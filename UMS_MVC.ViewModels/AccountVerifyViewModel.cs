﻿namespace UMS_MVC.ViewModels
{
    public class AccVerifyViewModel
    {
        public int AccountId { get; set; }
        public string EncodedVal { get; set; }
        public string MajorType { get; set; }
        public string MethodName { get; set; }
    }
}
