﻿namespace UMS_MVC.ViewModels.ViewModels.GradingForSection
{
    public class SectionBlockSubmit
    {
        public int DepartmentId { set; get; }
        public string DepartmentName { get; set; }
        public double TotalSections { get; set; }
        public double UmsCoreSections { get; set; } //30-8-2016 Added
        public double UmsLabSections { get; set; } //30-8-2016 Added
        public double UmsOtherSections { get; set; } //30-8-2016 Added
        public double PauCoreSections { get; set; }//30-8-2016 Added
        public double PharmaCoreNew { set; get; }//30-8-2016 Added
        public double PharmaCoreOld { get; set; }//30-8-2016 Added
        public double FacultySubmitted { get; set; }
        public double HighLighted { set; get; }
    }
}
