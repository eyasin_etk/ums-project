﻿
namespace UMS_MVC.ViewModels.ViewModels.GradingForSection
{
    public class GradePontViewModel
    {
        public GradePontViewModel(double gradep = 0, string letter = "")
        {
            GradeP = gradep;
            GradeLetter = letter;
        }
        public double GradeP { set; get; }
        public string GradeLetter { set; get; }
    }
}