﻿namespace UMS_MVC.ViewModels.ViewModels.GradingForSection
{
    public class SectionBlockStatusViewModel
    {
        public int DeptId { get; set; }
        public int SemesterId { set; get; }
        public bool TotalDo { get; set; }
        public bool CoreDo { get; set; }
        public bool LabDo { get; set; }
        public bool OtherDo { get; set; }
        public bool PauCoreDo { get; set; }
        public bool PharmaNew { get; set; }
        public bool PharmaOld { get; set; }
        public bool SubmitedDo { get; set; }
        public bool HighlightedDo { get; set; }

            
    }
}
