﻿

namespace UMS_MVC.ViewModels.ViewModels.GradingForSection
{
    public class MinifiedGradingViewModel
    {
        public MinifiedGradingViewModel(double att = 0, double ct = 0, double mid = 0, double final = 0)
        {
            Att = att;
            Ct = ct;
            Mid = mid;
            Final = final;
        }

        public double Att { get; set; }
        public double Ct { get; set; }
        public double Mid { get; set; }
        public double Final { set; get; }
    }
}