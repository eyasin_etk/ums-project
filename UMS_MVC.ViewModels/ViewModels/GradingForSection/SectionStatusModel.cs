﻿using System;
using System.Collections.Generic;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.ViewModels.ViewModels.GradingForSection
{
    public class SectionStatusModel
    {
        public SectionStatusModel(double totalEnrollment = 0,GradingSystem gradingSystem=null)
        {
            TotalEnrollment = totalEnrollment;
            GradingSystem = gradingSystem;
        }

        public int SectionId { get; set; }
        public string SectionName { get; set; }
        public int CourseForDepartmentId { get; set; } // added 7-2-2016
        public string ProgramName { set; get; }
        public int? TeacherId { get; set; }
        public string TeacherName { set; get; }// added 7-2-2016
        public int SemesterId { get; set; }
        public double TotalEnrollment { set; get; }
        public DateTime? CreatedDate { get; set; }
        public bool EvaluationPossible { get; set; }
        public Semester Semester { set; get; }
       // public Teacher Teacher { set; get; }
        public GradeUploadViewModel GradeUploadViewModel { set; get; }
        public CourseForDepartment CourseForDepartment { set; get; }
        public GradingSystem GradingSystem { set; get; }
        public List<SectionAdvisingViewModel> Advisings { set; get; }


    }
}