﻿


namespace UMS_MVC.ViewModels.ViewModels.GradingForSection
{
    public class SectionAdvisingViewModel
    {
        public SectionAdvisingViewModel(int gradingSystem = 0,double atten=0, double classte=0, double midt=0, double finalT=0.0, double total=0.0, double gradePOi=0.0)
        {
            GradingSystemId = gradingSystem;
            Att = atten;
            Ct = classte;
            Mid = midt;
            Final = finalT;
            Total = total;
            GradePoint = gradePOi;

        }

        public int CourseForStudentAcademicId { set; get; }
        public int CourseForDepartmentId { set; get; }
        public int SemesterId { set; get; }
        public int StudentIdentityId { set; get; }
        public string StudentId { set; get; }
        public string StudentName { set; get; }
        public double Att { set; get; }
        public double Ct { get; set; }
        public double Mid { get; set; }
        public double Final { get; set; }
        public double Total { get; set; }
        public double GradePoint { set; get; }
        public string LetterGrade { get; set; }
        public double TotalGrade { get; set; }
        public int GradingSystemId { set; get; }
        public int CourseStatusId { set; get; }
        public GradeUploadViewModel GradeUploadViewModel { set; get; }

    }
}