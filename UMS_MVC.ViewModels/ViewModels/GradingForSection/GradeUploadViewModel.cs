﻿

namespace UMS_MVC.ViewModels.ViewModels.GradingForSection
{
    public class GradeUploadViewModel
    {
        public GradeUploadViewModel(bool enableGradeUp = false, bool highLight = false, bool mid = false, bool final = false, string markupMessage = "General Mark Upload is Currently Deactivated")
        {
            Highlighted = highLight;
            EnableGradeUpload = enableGradeUp;
            MidTerm = mid;
            FinalTerm = final;
            MarkUploadMessage = markupMessage;
        }

        public int SectionId { set; get; }
        public int SemesterId { get; set; }
        public string ExpireDate { set; get; }
        public bool EnableGradeUpload { set; get; }
        public bool Highlighted { get; set; }
        public bool MidTerm { set; get; }
        public bool FinalTerm { get; set; }
        public string MarkUploadMessage { set; get; }
    }
}