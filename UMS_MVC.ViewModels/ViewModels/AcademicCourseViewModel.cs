﻿namespace UMS_MVC.ViewModels.ViewModels
{
    public class AcademicCourseViewModel
    {
        public int SemesterId { get; set; }
        public int StudentIdentiFictionId { get; set; }
        public string CourseCode { get; set; }
        public string CourseName { get; set; }
        public double Credits { get; set; }
        public double Mid { get; set; }
        public double Final { get; set; }
        public string LetterGrade { set; get; }
        public double Grade { get; set; }
        public double Tgp { get; set; }
        public string Status { get; set; }
        public int? SecId { get; set; }
        public int CourseStatusId { set; get; }
        public string SemesterAndYear { get; set; }
    }
}
