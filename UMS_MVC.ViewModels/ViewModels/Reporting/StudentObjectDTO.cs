﻿using System;

namespace UMS_MVC.ViewModels.ViewModels.Reporting
{
    
    public class StudentObjectDTO
    {
        
        public int StudentIdentityId { get; set; }
        public string StudentId { get; set; }
        public string Name { get; set; }
        public string FatherName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public DateTime AddedDate { get; set; }
        public int BatchNo { get; set; }
        public string Remarks { get; set; }
        public string DepartmentName { get; set; }
        public bool Advised { set; get; }

    }
}
