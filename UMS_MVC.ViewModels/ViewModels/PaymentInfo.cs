﻿namespace UMS_MVC.ViewModels.ViewModels
{
    public class PaymentInfo
    {
        public string AdvisedBy { set; get; }
        public string AdvisorId { set; get; }
        public string PrintedBy { set; get; }
    }
}