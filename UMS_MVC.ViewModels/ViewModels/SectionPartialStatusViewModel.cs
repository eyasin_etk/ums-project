﻿using System;

namespace UMS_MVC.ViewModels.ViewModels
{
    public class SectionPartialStatusViewModel
    {
        public string TeacherName { set; get; }
        public int TotalStudents { set; get; }
        public DateTime? DateCreated { set; get; }
    }
}