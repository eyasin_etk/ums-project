﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMS_MVC.ViewModels.ViewModels.Scrutiny
{
  public class SectionsScrutinyInfoViewModel
    {
        public int SectionId { set; get; }
        public string CourseName { set; get; }
        public string CountLevel1Status { set; get; }//Over all level 1 completed /total student
        public string CountLevel2Status { set; get; }//Over all level 2 completed /total student
    }
}
