﻿using System.Collections.Generic;

namespace UMS_MVC.ViewModels.ViewModels.Scrutiny
{
    public class ScrutintSectionViewModel
    {
        public int SectionId { get; set; }
        public string CourseName { get; set; }
        public string CourseCode { get; set; }
        public double CourseCredit { get; set; }
        public string AssignedTeacher { get; set; }
    }

    public class StudentsScrutinyInfoViewModel
    {
        public int CourseForStudentAcademicId { set; get; }
        public string StudentId { set; get; }
        public string StudentName { get; set; }
        public int SectionId { set; get; }
        public bool Lvl1Stat { set; get; }
        public bool Lvl2Stat { set; get; }
        public string ScrutinyBy { set; get; }

    }
    public class ScrutinyViewModel
    {
        public ScrutintSectionViewModel Section{ set; get; }
        public List<StudentsScrutinyInfoViewModel>  Students { get; set; }
    }

}
