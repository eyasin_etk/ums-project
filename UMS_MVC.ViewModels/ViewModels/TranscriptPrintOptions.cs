﻿using System;
using System.ComponentModel.DataAnnotations;

namespace UMS_MVC.ViewModels.ViewModels
{
    public class TranscriptPrintOptions
    {
        public int StudentIdenId { set; get; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? PublishingDate { set; get; }
    }
}