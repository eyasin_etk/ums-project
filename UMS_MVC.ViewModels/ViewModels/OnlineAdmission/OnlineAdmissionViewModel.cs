﻿using System;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.ViewModels.ViewModels.OnlineAdmission
{
    public class OnlineAdmStudentIdentityViewModel:StudentIdentification
    {
    }

    public class OnlineAdmStudentInfoViewMOdel : StudentInfo
    {
    }

    public class OnlineAdmAcademicViewModel : StudentAcademicInfo
    {
    }
    public class OnlineAdmIndexViewModel
    {
        public string StudentName { get; set; }
        public Guid PreAdmissionStudentInfoId { get; set; }
        public string StudentImage { get; set; }
        public string PersonalContactNo { get; set; }
        public DateTime EntryTime { get; set; }
        public string DepartmentName { get; set; }
        public int EnrolledDepartmentId { get; set; }
    }
}
