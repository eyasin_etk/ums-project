﻿namespace UMS_MVC.ViewModels.ViewModels
{
    public class TeacherAccountViewModel
    {
        public int  TeacherId { get; set; }
        public string Name { get; set; }
        public string Designation { set; get; }
        public string Department { set; get; }
        public string PhoneNo { set; get; }
        public string TeleExchange { set; get; }
        public string Pic { get; set; }
    }

    public class TeacherSectionFilterViewModel: TeacherAccountViewModel
    {
        public int CurrentSection { get; set; }
        public int TotalSections { set; get; }
    }
}