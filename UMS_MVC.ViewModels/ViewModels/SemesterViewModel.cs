﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMS_MVC.ViewModels.ViewModels
{
    public class SemesterViewModel
    {
        public int SemesterId { set; get; }
        public string AcademicYear { set; get; }
        public string SemesterNYear { set; get; }
        public int BatchNo { set; get; }
         public bool ActiveSemester { set; get; }
         public bool MidTerm { set; get; }
       public bool FinalTerm { set; get; }
        public bool MidAdmitCard { set; get; }
        public bool FinalAdmitCard { set; get; }
        public bool CourseAdvising { set; get; }
       public bool EvaluationActivate { set; get; }
        //public int? DepartmentId { set; get; }
        //public bool CourseAdvisingForSpecialDepartment { set; get; }
        //public bool MidGradeForSpecialDepartment { set; get; }
        //public bool FinalGradeForSpecialDepartment { set; get; }
        public DateTime? SpecialGradeuploadDeadLine { set; get; }
    }
}
