﻿using System;

namespace UMS_MVC.ViewModels.ViewModels
{
    public class NewRegisteredStudents
    {
        public int? StudentIdentityId { set; get; }
        public string StudentId { set; get; }
        public string DepartmentName { set; get; }
        public int NoOfRegisteredCourses { set; get; }
        public int PaymentRegistrationNumber { set; get; }
        public DateTime? Registrationtime { set; get; }
        public string RegisteredBy { set; get; }
    }
}