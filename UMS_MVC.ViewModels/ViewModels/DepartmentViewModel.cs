﻿namespace UMS_MVC.ViewModels.ViewModels
{
    public class DepartmentViewModel
    {
        public int DepartmentId { set; get; }
        public string SchoolName { set; get; }
        public string DepartmentName { set; get; }
        public int DepartmentCode { set; get; }
        public string DepartmentTitle { set; get; }
        public double TotalSemester { set; get; }
        public double TotalCredit { set; get; }
        public double AddmissionFee { set; get; }
        public double PerCreditCost { set; get; }
        public double LabFeePerSemester { set; get; }
        public double OtherFeePerSemister { set; get; }
        public double TotalCost { set; get; }
        public double RequiredCredit { set; get; }
    }
}