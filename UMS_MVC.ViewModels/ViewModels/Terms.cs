﻿namespace UMS_MVC.ViewModels.ViewModels
{
    public class Terms
    {
        public int TermId { set; get; }
        public string Term { set; get; }
    }

}