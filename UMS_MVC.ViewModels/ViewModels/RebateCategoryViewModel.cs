﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMS_MVC.ViewModels.ViewModels
{
    public class RebateCategoryViewModel
    {
        public int RebateCategoryId { set; get; }
        public string RebateName { set; get; }

        public double RebatePercent { set; get; }
    }
}
