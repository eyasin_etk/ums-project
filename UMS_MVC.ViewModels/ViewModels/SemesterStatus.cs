﻿namespace UMS_MVC.ViewModels.ViewModels
{
    public class SemesterStatus
    {
        public string ActiveSemester { set; get; }
        public string GradeUploadTerm { set; get; }
        public string AdvisingSemester { set; get; }
    }
}