﻿namespace UMS_MVC.ViewModels.ViewModels
{
    public class CumulativeScores
    {
        public double Credits { set; get; }
        public double Cgpa { set; get; }
        public double CreditEarned { set; get; }
        public double CourseCompleted { set; get; }
        public int? SemesterId { set; get; }
        public double? TotalGrade { set; get; }
        public double? TotalCredit { set; get; }
        public double SemesterGpa { set; get; }
        public int? StudentId { set; get; }
        public double CumulativeCredits { set; get; }
        public string Name { set; get; }
        public string DepartmentalId { set; get; }
    }
}