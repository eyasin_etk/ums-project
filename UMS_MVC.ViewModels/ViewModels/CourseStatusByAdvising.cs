﻿namespace UMS_MVC.ViewModels.ViewModels
{
    public class CourseStatusByAdvising
    {
        public int CourseForDeptId { set; get; }
        public string CourseCode { set; get; }
        public string CourseTitle { set; get; }
        public double TotalEnrolled { set; get; }
        public double SectionEnrolled { set; get; }
        public double SectionNotEnrolled { set; get; }
        public string SemesterSerial { set; get; }
    }
}