﻿using System.ComponentModel.DataAnnotations;

namespace UMS_MVC.ViewModels.ViewModels
{
    public class LoginInitial
    {
        [Required(ErrorMessage = "Please enter Login ID")]
        public string LoginId { set; get; }
        [Required(ErrorMessage = "Please enter Password")]
        public string Password { set; get; }
        public string RetypePassword { set; get; }
    }
}