﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using UMS_MVC.DataModel.Models.TeacherEvaluation;

namespace UMS_MVC.ViewModels.ViewModels
{
    public class TeacherEvaluationDisplay
    {
        public int Serial { set; get; }
        [Display(Name = "Teacher's Name")]
        public string TeacherName { get; set; }
        [Display(Name = "Course Code")]
        public string CourseCode { get; set; }
        public int CourseForStudentsAcademicId { set; get; }
        public List<SurveyQuestion> Questions { set; get; }
    }
}