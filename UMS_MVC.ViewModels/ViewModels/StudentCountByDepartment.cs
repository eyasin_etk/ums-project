﻿namespace UMS_MVC.ViewModels.ViewModels
{
    public class StudentCountByDepartment
    {
        public string DepartmentName { set; get; }
        public double CurrentSemesterCount { set; get; }
        public double TotalCount { set; get; }
        public int? DepartmentId { set; get; }
    }
}