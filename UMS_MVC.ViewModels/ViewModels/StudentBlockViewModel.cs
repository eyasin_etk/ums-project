﻿using System;

namespace UMS_MVC.ViewModels.ViewModels
{
    public class StudentBlockViewModel
    {
        public string ExtraMessage { set; get; }
        public string BlockReason { get; set; }
        public bool IsBlocked { set; get; }
        public DateTime? BlockExpireDate { set; get; }
    }
}
