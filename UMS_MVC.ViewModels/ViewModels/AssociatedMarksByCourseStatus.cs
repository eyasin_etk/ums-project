﻿namespace UMS_MVC.ViewModels.ViewModels
{
    public class AssociatedMarksByCourseStatus
    {
        public AssociatedMarksByCourseStatus(bool sec = false)
        {
            CancelSection = sec;
        }

        public int CustGradeId { set; get; }
        public string CourseStatus { set; get; }
        public int StatusId { set; get; }
        public string CustomGrade { set; get; }
        public string ShortName { get; set; }
        public bool CancelSection { set; get; }

    }
}