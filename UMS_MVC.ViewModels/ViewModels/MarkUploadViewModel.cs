﻿using System.ComponentModel.DataAnnotations;

namespace UMS_MVC.ViewModels.ViewModels
{
    public class MarkUploadViewModel
    {
        public int Serial { set; get; }
        public int CourseForStudentId { set; get; }
        public int CourseForDeptId { set; get; }
        [Display(Name = "Student's ID")]
        public string StudentId { set; get; }
        [Display(Name = "Student's Name")]
        public string StudentName { set; get; }
        [Display(Name = "Attendance")]
        public double Att { set; get; }
        [Display(Name = "Class Test")]
        public double ClassTest { set; get; }
        [Display(Name = "Mid Term")]
        public double MidTerm { set; get; }
        [Display(Name = "Final Term")]
        public double FinalTerm { set; get; }
        [Display(Name = "Total Mark")]
        public double TotalMark { set; get; }
        public double Grade { set; get; }
        [Display(Name = "Letter Grade")]
        public string LetterGrade { set; get; }
        public string CourseType { set; get; }
    }
}