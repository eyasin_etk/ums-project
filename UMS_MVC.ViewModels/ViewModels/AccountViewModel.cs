﻿using UMS_MVC.DataModel.Models.AccountExt;

namespace UMS_MVC.ViewModels.ViewModels
{
    public class AccountViewModel
    {
        public DataModel.Models.Essential_Types.Account Account { get; set; }
        public AccountMetaInformation AccountMetaInformation { get; set; }
        public AccountMetaProfessional AccountMetaProfessional { get; set; }
    }
}