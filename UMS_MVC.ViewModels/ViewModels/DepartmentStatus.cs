﻿namespace UMS_MVC.ViewModels.ViewModels
{
    public class DepartmentStatus
    {
        public string DepartmentName { set; get; }
        public int ExistingMale { set; get; }
        public int ExistingFemale { set; get; }
        public int AdvisedMale { set; get; }
        public int AdvisedFemale { set; get; }
        public int PassedMale { set; get; }
        public int PassedFemale { set; get; }
        public int FailedMale { set; get; }
        public int FailedFemale { set; get; }

        //Total Calculation
        public int ExistingTotal { set; get; }
        public int AdvisedTotal { set; get; }
        public int PassedTotal { set; get; }
        public int FailedTotal { set; get; }
        public int NonGraded { set; get; }
        public DepartmentStatus()
        {
            FailedMale = AdvisedMale - PassedMale;
            FailedFemale = AdvisedFemale - PassedFemale;
            ExistingTotal = ExistingMale + ExistingFemale;
            AdvisedTotal = AdvisedMale + AdvisedFemale;
            PassedTotal = PassedMale + PassedFemale;
            FailedTotal = FailedMale + FailedFemale;
        }
    }
}