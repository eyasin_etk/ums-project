﻿namespace UMS_MVC.ViewModels.ViewModels.SortingIndex
{
    public class StudentIndexSorting
    {
        public StudentIndexSorting(int genderid=3)
        {
            GenderId = genderid;
        }

        public int? Page { set; get; }
        public int SemesterId { set; get; }
        public string SemesterName { set; get; }
        public int DepartmentId { set; get; }
        public string DepartmentName { set; get; }
        public string IdSearch { set; get; }
        public string NameSearch { set; get; }
        public double TotalStudents { set; get; }
        public double TotalMaleStudents { set; get; }
        public double TotalFemaleStudents { get; set; }
        public int GenderId { set; get; } // Gender ID 1=Male, 2=Female, 3= All

    }

    public class StudentIndexApiSorting
    {
        public int SemesterId { set; get; }
        public int DepartmentId { set; get; }
        public int GenderId { set; get; } // Gender ID 1=Male, 2=Female, 3= All
        public string IdSearch { set; get; }
        public string NameSearch { set; get; }
       
    }

}
