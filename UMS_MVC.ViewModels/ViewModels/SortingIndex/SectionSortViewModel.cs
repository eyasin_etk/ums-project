﻿namespace UMS_MVC.ViewModels.ViewModels.SortingIndex
{
    public class SectionSortViewModel
    {
        public SectionSortViewModel(int deptId=0, int semId=0,int courseId=0, int teacherId=0, int secId=0)
        {
            DeptId = deptId;
            SemId = semId;
            CourseId = courseId;
            TeacherId = teacherId;
            SecId = secId;
        }
        public int DeptId { get; set; }
        public int SemId { get; set; }
        public int CourseId { get; set; }
        public int TeacherId { get; set; }
        public int SecId { get; set; }
    }
}
