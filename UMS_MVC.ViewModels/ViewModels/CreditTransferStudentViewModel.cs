﻿namespace UMS_MVC.ViewModels.ViewModels
{
    public class CreditTransferStudentViewModel
    {
        public int CreditTransferStudentsId { get; set; }
        public int StudentIdentificationId { set; get; }
        public string PreviousInstituteName { get; set; }
        public string InstituteAdmittedProgram { set; get; }
        public string YearAdmitted { get; set; }
        public double InstituteCreditCompleted { get; set; }
        public double CreditAccepted { get; set; }
        public double InstituteTotalCredit { get; set; }
        public string StudentName { set; get; }

    }
}
