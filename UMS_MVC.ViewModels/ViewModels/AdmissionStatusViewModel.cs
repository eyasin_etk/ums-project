﻿namespace UMS_MVC.ViewModels.ViewModels
{
    public class AdmissionStatusViewModel
    {
        public int DepartmentId { set; get; }
        public string DepartmentName { set; get; }
        public int SemesterId { set; get; }
        public string SemesterName { set; get; }
        public int CurrentAdmittedStudent { set; get; }
        public int TotalAdmittedStudent { set; get; }
    }
}