﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMS_MVC.ViewModels.ViewModels
{
    public class StudentRebateViewModel
    {
        public int StudentIdentificationId { get; set; }
        public int RebateId { set; get; }
        public int RebateCategoryId { set; get; }

        public string RebateName { get; set; }

        public double RebatePercentage { get; set; }

        public int SemesterId { get; set; }

        public string SemesterName { get; set; }
    }
}
