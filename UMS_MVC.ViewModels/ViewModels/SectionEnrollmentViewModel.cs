﻿namespace UMS_MVC.ViewModels.ViewModels
{
    public class SectionEnrollmentViewModel
    {
        public int? SectionId { get; set; }
        public string StudentId { get; set; }
        public string StatusName { get; set; }
        public int  CourseForStudentId { get; set; }
    }
}
