﻿namespace UMS_MVC.ViewModels.ViewModels
{
    public class AdvisedStudent
    {
        public int StudentIdentificationId { set; get; }
        public string StudentId { set; get; }
        public int BatchNo { set; get; }
        public int  GenderId { set; get; }
        public string GenderName { set; get; }
        public string StudentName { set; get; }
        public double TakenCredits { set; get; }

     }
}