﻿using System;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.ViewModels.ViewModels
{
    public class AdmissionViewModel
    {
        public StudentIdentification StudentIdentification { set; get; }
        public bool OldStudent { get; set; }
        public DateTime? AdmissionDate { get; set; }
    }
}
