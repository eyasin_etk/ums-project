﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMS_MVC.ViewModels.ViewModels
{
    public class EvaluationResultViewModel
    {
        public int EvaluationResultId { get; set; }
        public int SectionId { set; get; }
        public int QuestionId { get; set; }
        public string CourseName { set; get; }
        public double TotalStudent { get; set; }
        public double TotalPointYes { get; set; }
        public double TotalPointAverage { get; set; }
        public double TotalPointNo { get; set; }
        public double TotalScore { set; get; }
       
    }

    public class EvaluationProjectedResult
    {
       
        public EvaluationPartialInfo EvaluationPartialInfo { set; get; }
        public List<EvaluationResultViewModel> EvaluationResultViewModels { get; set; }
    }

    public class EvaluationPartialInfo
    {
        public bool MultipleFaculty { set; get; }
        public string DepartmentName { set; get; }
        public string FacultyName { set; get; }
        public string FacultyDesignation { set; get; }
        public string FacultyImage { set; get; }
    }
}
