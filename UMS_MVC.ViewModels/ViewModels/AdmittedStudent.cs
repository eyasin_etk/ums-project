﻿using System;

namespace UMS_MVC.ViewModels.ViewModels
{
    public class AdmittedStudent
    {
        public string StudentId { set; get; }
        public string DepartmentName { set; get; }
        public bool CreditTransfer { set; get; }
        public string Semester { set; get; }
        public bool Diploma { set; get; }
        public string Password { set; get; }
        public DateTime CreatedDate { set; get; }
    }
}