﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMS_MVC.ViewModels.ViewModels
{
    public class SectionDropDownViewModel
    {
        public int SectionId { get; set; }
        public string SectionName { get; set; }
        public int CourseForDeptId { set; get; }
        public string CourseNameWithCounter { set; get; }
        public string CourseCode { set; get; }
        public int  SemesterId { get; set; }
        public string SemesterName { set; get; }
        public string SemesterSerilizer { set; get; }

    }
}
