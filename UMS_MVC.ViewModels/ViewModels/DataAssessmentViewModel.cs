﻿namespace UMS_MVC.ViewModels.ViewModels
{
    public class DataAssessmentViewModel
    {
        //public DataAssessmentViewModel(int sectionId = 0, int total = 0, int comTotal = 0, int unComTotal = 0)
        //{
            
        //    SectionId = sectionId;
        //    TotalStudent = total;
        //    UncompletedSurvey = unComTotal;
        //    CompletedSurvey = comTotal;
        //}
        public int  SectionId { get; set; }
        public string SemesterName { get; set; }
        public string CourseName { get; set; }
        public int TotalStudent { set; get; }
        public int CompletedSurvey { get; set; }
        public int UncompletedSurvey { get; set; }
        public int AssesmentCounter { set; get; }
    }
}