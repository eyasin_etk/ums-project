﻿namespace UMS_MVC.ViewModels.ViewModels
{
    public class SectionIndexTeacherProfile
    {
        public int TeacherId { set; get; }
        public int AccountId { set; get; }
        public string Name { set; get; }
    }
}