﻿namespace UMS_MVC.ViewModels.ViewModels
{
    public class AdvisingPagesort
    {
        public int? Page { set; get; }
        public int SemesterId { set; get; }
        public int DepartmentId { set; get; }
        public int CourseId { set; get; }
        public int SecSort { set; get; }
        public int? TeacherId { set; get; }
        public bool PrintAccess { set; get; }
    }
}