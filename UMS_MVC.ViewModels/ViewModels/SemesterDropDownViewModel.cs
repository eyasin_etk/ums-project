﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMS_MVC.ViewModels.ViewModels
{
    public class SemesterDropDownViewModel
    {
       public int SemesterId { get; set; }
        public int BatchNo { get; set; }
        public bool ActiveSemester { get; set; }
        public string SemesterNYear { get; set; }
    }
}
