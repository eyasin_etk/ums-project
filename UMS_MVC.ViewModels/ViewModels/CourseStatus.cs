﻿namespace UMS_MVC.ViewModels.ViewModels
{
    public class CourseStatus
    {
        public int CourseStatusId { set; get; }
        public string Status { set; get; }
    }
}