﻿using System;

namespace UMS_MVC.ViewModels.ViewModels
{
    public class ResponseModel
    {
        public ResponseModel(object data = null, PaginationViewModel pagein = null, bool isSuccess = true, string message = "Success", Exception exception = null)
        {
            IsSuccess = isSuccess;
            Message = message;
            Data = data;
            Exception = exception;
            Pagination = pagein;
        }
        public Exception Exception { set; get; }
        public Object Data { set; get; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public PaginationViewModel Pagination { get; set; }

    }
}