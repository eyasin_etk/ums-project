﻿namespace UMS_MVC.ViewModels.ViewModels
{
    public class PaginationViewModel
    {
        public PaginationViewModel(int pageNo = 0, int pageSize = 10)
        {
            PageNo = pageNo;
            PageSize = pageSize;
        }
        public int TotalCount { get; set; }
        public int TotalPages { get; set; }
        public int PageNo { get; set; }
        public int PageSize { get; set; }
    }
}
