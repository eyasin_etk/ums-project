﻿namespace UMS_MVC.ViewModels.ViewModels
{
    public class AdvisedCourse
    {
        public string CourseCode { set; get; }
        public string CourseTitle { set; get; }
        public double Credits { set; get; }
    }
}