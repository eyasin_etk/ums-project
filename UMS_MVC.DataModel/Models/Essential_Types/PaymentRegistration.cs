﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class PaymentRegistration
    {
        public int PaymentRegistrationId { set; get; }
        [Index("IX_SemesterID")]
        public int SemesterId { set; get; }

        [DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime? RegistrationDate { set; get; }
        [DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime? PrintDate { set; get; }
        public string PrintedBy { set; get; } /////Based on Login Identity or Student Id
        public string RegistrationFor { set; get; }/////Student ID///////////////////
        public int RegistrationNo { set; get; }
        public int? StudentIdentificationId { set; get; }
        public virtual StudentIdentification StudentIdentification { set; get; }
        public virtual Semester Semester { set; get; }
        public virtual ICollection<Payment> Payments { set; get; }
        public virtual ICollection<CourseForStudentsAcademic> CourseForStudentsAcademics { set; get; }

    }
}