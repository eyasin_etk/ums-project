﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class Teacher
    {
        public int TeacherId { set; get; }
        public int? AccountId { set; get; }
        [Index("IX_DeptId")]
        public int DepartmentId { set; get; }
        public string LoginId { set; get; }
        //track
        public DateTime? EntryDate { set; get; }
        public string EntryBy { set; get; }
        public virtual Account Account { set; get; }
        public virtual Department Department { set; get; }
        public virtual ICollection<Section> Sections { set; get; }
    }
}