﻿namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class PicOfStudent
    {
        public int PicOfStudentId { set; get; }

        public string ImageName { set; get; }
    }
}