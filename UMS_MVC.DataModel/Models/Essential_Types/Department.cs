﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class Department
    {
        public int DepartmentId { set; get; }
        public int SchoolId { set; get; }

        [Required]
        //[Remote("CheckDepartmentName", "Check", ErrorMessage = "Department Name Cann't be same")]
        [Display(Name = "Department Name")]
        [StringLength(150)]
        [Index("IX_DeptName")]
        public string DepartmentName { set; get; }
        [Display(Name = "Short Name")]
        [StringLength(40)]
        [Index("IX_DeptShortName")]
        public string ShortName { get; set; }
        [Required]
        //[Remote("CheckDepartmentCode", "Check", ErrorMessage = "Department Code Cann't be same")]
        [Display(Name = "Department Code")]
        [Index("IX_DeptCode")]
        public int DepartmentCode { set; get; }
        public string DepartmentTitle { set; get; }
        [Display(Name = "Total Semister")]
        public double TotalSemester { set; get; }

        [Display(Name = "Required Credit to Complete")]
        public double TotalCredit { set; get; }
        [Display(Name = "Addmission Fee")]
        public double AddmissionFee { set; get; }
        [Display(Name = "Per Credit Cost")]
        public double PerCreditCost { set; get; }
        [Display(Name = "Lab Fee Per Semester")]
        public double LabFeePerSemester { set; get; }
        [Display(Name = "Other Fee Per Semester")]
        public double OtherFeePerSemister { set; get; }
        
        public double TotalCost { set; get; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DateCreated { set; get; }
        [Range(0.0,4.0)]
        [Display(Name = "Minimum CGPA")]
        public double RequiredCredit { set; get; }

        /// /////////// Foreign Keys/////////////////////////
      
        public virtual School ASchool { set; get; }
        public virtual ICollection<StudentIdentification> StudentIdentifications { set; get; }
        public virtual ICollection<CourseForDepartment> CourseForDepartments { set; get; }
        public virtual ICollection<Teacher> Teachers { set; get; }
        
    }
}