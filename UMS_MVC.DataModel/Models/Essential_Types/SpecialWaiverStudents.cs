﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMS_MVC.DataModel.Models.Essential_Types
{

    public  class SpecialWaiverStudents
    {
        [Key]
        public int specialwaiverID { get; set; }
        public string StudentId { get; set; }
        public int StudentIdentificationId { get; set; }
        public string description { get; set; }

        public int waiver { get; set; }
    }
}
