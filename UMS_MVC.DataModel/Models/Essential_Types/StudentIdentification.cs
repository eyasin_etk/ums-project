﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class StudentIdentification
    {
        public int StudentIdentificationId { set; get; }
        [Display(Name = " SelectSchool")]
        [Index("IX_SchoolID")]
        public int SchoolId { set; get; }
        [Display(Name = "Select Department")]
        [Index("IX_DeptID")]
        public int DepartmentId { set; get; }
        [Display(Name = "select Semester")]
        public int SemesterInfoId { set; get; }

        [Display(Name = "Student ID")]
        [StringLength(20)]
        [Index("IX_StudentID",1,IsUnique = true)]
        public string StudentId { set; get; }
        [Display(Name = "Semester")]
        public string SemesterAndYear { set; get; }
        public bool Validation { set; get; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime AddedDate { set; get; }
        public int? StudentInfoId { set; get; }
        // New added
        [DataType(DataType.Password)][MinLength(4)]
        public string Password { set; get; }
        [Display(Name = "Diploma Student")]
        public bool DiplomaStudent { set; get; }
        [Display(Name = "Pic of Student")]
        [Index("IX_StudentPic")]
        [StringLength(25)]
        public string StudentPicture { set; get; }
       // public string StudentInfofile { set; get; }
        public string Remark { set; get; }
        [Display(Name = "Credit Transfer")]
        public bool CreditTransfer { set; get; }
        [Index("IX_SemesterID")]
        public int SemesterId { set; get; }
        
        [Display(Name = "Block Student Login")]
        public bool BlockStudent { set; get; }// Added 21-3-2016
        [Display(Name = "Reason for block")]
        public string BlockReason { set; get; }// Added 21-3-2016
        public DateTime? BlockExpireDate { set; get; }
        public string EntryBy { set; get; } //New Added 08-11-2015
        public string LastPsswordChange { get; set; } //29-3-2016 will be in dateTime
        public int StudentGroupId { get; set; } // Default 1, {1,2,3}
        public bool SuspendedByProbation { get; set; }
        public Guid? StudentGuid { set; get; } //added 6-6-2016

        [Display(Name = "Regular Student")]
        public bool? RegularStudent { set; get; }

        [Display(Name = "Friday Student")]
        public bool? FridayStudent { set; get; }


        [Display(Name = "Evening Student")]
        public bool? EveningStudent { set; get; }


        [Display(Name = "Batch No")]
        public int? Batch { get; set; }

        

        ///////////////////////////////////////////////////////

        public virtual School School { set; get; }
        public virtual Department Department { set; get; }
        public virtual SemesterInfo SemesterInfo { set; get; }
        public virtual ICollection<CourseForStudentsAcademic> CourseForStudentsAcademics { set; get; }
        public virtual StudentInfo StudentInfo { set; get; }
        public virtual Semester Semester { set; get; }

    }
}