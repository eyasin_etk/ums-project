﻿using System;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class DepartmentChange
    {
        public int DepartmentChangeId { set; get; }
        public int StudentIdentificationId { set; get; }
        public string OldId { set; get; }
        public string NewId { set; get; }
        public DateTime? DateOfDeptChange { set; get; }
        public string ChangedBy { set; get; }
        public string PcIpAddress { set; get; }
        public string Remarks { get; set; }
        public virtual StudentIdentification StudentIdentification { set; get; }
    }
}