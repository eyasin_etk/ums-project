﻿namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class Rebate
    {
        public int RebateId { set; get; }
        public int StudentIdentificationId { set; get; }
        public int RebateCategoryId { set; get; }

        public int SemesterId { get; set; }

        public virtual RebateCategory RebateCategory { set; get; }
        public virtual StudentIdentification StudentIdentification { set; get; }
        public virtual Semester Semester { get; set; }

    }
}