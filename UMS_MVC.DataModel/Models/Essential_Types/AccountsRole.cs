﻿using System.ComponentModel.DataAnnotations;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class AccountsRole
    {
        public int AccountsRoleId { set; get; }
        [Display(Name = "Role")]
        public string AccountType { set; get; }

        
    }
}