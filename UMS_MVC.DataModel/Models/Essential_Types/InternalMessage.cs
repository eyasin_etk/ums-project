﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using UMS_MVC.DataModel.Models.UniversalTypes;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class InternalMessage
    {
        public int InternalMessageId { set;get; }
        public int MessageFlagId { set; get; }
        public int SenderAccountId { set; get; }
        public int ReceiverAccountId { set; get; }
        [Display(Name = "Subject")]
        public string MessageSubject { set; get; }
        [Required]
        [Display(Name = "Body")]
        public string MessageBody { set; get; }
        [Display(Name = "Attatched File")]
        public string AttatchedFile { set; get; }
        public DateTime SenderTime { set; get; }
        public DateTime? ReceiverViewTime { set; get; }
        public string SenderIp { set; get; }
        public string ReceiverIp { set; get; }
        public bool SenderDeleteStatus { set; get; }
        public bool ReceiverDeleteStatus { set; get; }
        public virtual MessageFlag MessageFlag { set; get; }
        public int OnreplyOfMessageId { set; get; }
        [ForeignKey("SenderAccountId")]
        public virtual Account SenderAccount { set; get; }
        [ForeignKey("ReceiverAccountId")]
        public virtual Account ReceiverAccount { set; get; }     

    }
}