﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class RebateCategory
    {
        public int RebateCategoryId { set; get; }
        public string RebateName { set; get; }

        [DataType(DataType.MultilineText)]
        public string RebateDetails { set; get; }
        public double RebatePercent { set; get; }
        public virtual ICollection<Rebate> Rebates { set; get; }
    }
}