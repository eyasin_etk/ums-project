﻿using System;
using System.ComponentModel.DataAnnotations;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class NoteUpload
    {
        public int NoteUploadId { set; get; }
        public int? AccountId { set; get; }
        [Display(Name = "Upload Date")]
        public DateTime? UploadDate { set; get; }
        [Display(Name = "Expire Date")]
        public DateTime? ExpireDate { set; get; }
        [Display(Name = "File Name")]
        public string FileName { set; get; }
        public string Description { set; get; }
        public int? SectionId { set; get; }
        public bool Status { set; get; }
        public virtual Section Section { set; get; }
        public virtual Account Account { set; get; }
    }
}