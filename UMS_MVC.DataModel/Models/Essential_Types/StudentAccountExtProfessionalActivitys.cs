﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class StudentAccountExtProfessionalActivitys
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int StudentAccountExtProfessionalActivityId { get; set; }
        public int? AccountMetaProfessionalId { get; set; }

        public int? AccountId { get; set; }

        public string EmploymentType { get; set; }

        public DateTime? employmentDate { get; set; }

        public string companyLocation { get; set; }

        public string Description { get; set; }
        

        public DateTime? EntryDate { get; set; }

        public string EntryBy { get; set; }

        public string company_name { get; set; }

        public string Designation { get; set; }

        public string Departmentname { get; set; }

        public DateTime? resignDate { get; set; }


    }
}
