﻿using System;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class PatchVersion
    {
        public int PatchVersionId { set; get; }
        public string VersionName { set; get; }
        public DateTime StartingDate { set; get; }
        public DateTime? FinishingDate { set; get; }
        public DateTime? PublishDate { set; get; }
        
    }
}