﻿namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class AdvisingCountByBatch
    {
        public int BatchNo { set; get; }
        public string BatchDetails { set; get; }
    }
}