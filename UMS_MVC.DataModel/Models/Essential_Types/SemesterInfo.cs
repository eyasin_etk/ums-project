﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class SemesterInfo
    {
        public int SemesterInfoId { set; get; }

        [Display(Name = "Semester Code")]
        public int SemesterCode { set; get; }

        [StringLength(20)]
        [Display (Name = "Semester")]
        public string SemesterName { set; get; }

        public virtual ICollection<StudentIdentification> StudentIdentifications { set; get; }

    }
}