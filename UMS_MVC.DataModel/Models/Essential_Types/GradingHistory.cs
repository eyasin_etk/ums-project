﻿using System;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class GradingHistory
    {
        public int GradingHistoryId { set; get; }
        public int CourseForStudentsAcademicId { set; get; } 
        public DateTime ChangedTime { set; get; }
        public string ChangedBy { set; get; }
        public string StudentId { set; get; }
        public double PreviousMark { set; get; }
        public string PreviousGrade { set; get; }
        public double ChangedMark { set; get; }
        public string ChangedGrade { set; get; }
        public string ComputerIp { set; get; }
    }
}