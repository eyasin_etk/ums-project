﻿using System;
using UMS_MVC.DataModel.Models.UniversalTypes;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class UserActivity
    {
        public int UserActivityId { set; get; }
        public int UserActivityTypeId { set; get; }
        public int AccountId { set; get; }
        public string Activity { set; get; }
        public string StudentId { set; get; }
        public DateTime? ActivityTime { set; get; }
        public bool Seen { set; get; }
        public string ComputerIp { set; get; }
       
        public virtual UserActivityType UserActivityType { set; get; }
        public virtual Account Account { set; get; }
        
    }
}