﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class Semester
    {
        public int SemesterId { set; get; }
        public string AcademicYear { set; get; }

        [Display(Name = "Semester with Year")]
        [StringLength(20)]
        [Index("IX_SemYear")]
        public string SemesterNYear { set; get; }
        [Display(Name = "Batch No")]
        public int BatchNo { set; get; }
        [Display(Name = "Active Semester for grading")]
        [Index("IX_Activated")]
        public bool ActiveSemester { set; get; }
        [Display(Name = "Activate Mid Term")]
        public bool MidTerm { set; get; }
        [Display(Name = "Activate Final Term")]
        public bool FinalTerm { set; get; }
        public bool MidAdmitCard { set; get; }
        public bool FinalAdmitCard { set; get; }
        
        [Display(Name = "Course Advising")]
        [Index("IX_AdvisingActivation")]
        public bool CourseAdvising { set; get; }
        [Display(Name = "Evaluation Activation")]
        [Index("IX_EvaluationActivation")]
        public bool EvaluationActivate { set; get; }
        public int? DepartmentId { set; get; }
        public bool OnlineAdmisssionStatus { set; get; }
        public DateTime? AdmissionStartDate { get; set; }
        public DateTime? AdmissionEndDate { get; set; }
        public bool PublishResult { set; get; }
        //public bool CourseAdvisingForSpecialDepartment { set; get; } //Disabled for created new class 27-7-2016
        //public bool MidGradeForSpecialDepartment { set; get; }
        //public bool FinalGradeForSpecialDepartment { set; get; }
        public DateTime? SpecialGradeuploadDeadLine { set; get; }
        public virtual Department Department { set; get; }
      //  public virtual UnfairStudents unfairstudents{ set; get; }

    }
    
   
}