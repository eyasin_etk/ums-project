﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class SerializedSemester
    {
        public int SerializedSemesterId { set; get; }
        [Index("IX_SemesterName")]
        [StringLength(20)]
        public String SemesterName { set; get; }
    }
}