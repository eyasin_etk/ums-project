﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using UMS_MVC.DataModel.Models.UniversalTypes;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class StudentInfoParial
    {
        //[Required(ErrorMessage = "District Required")]
        //[DataType(DataType.MultilineText)]
        //[Display(Name = "District")]
        public string PresentDistrict { get; set; } //new 11-6-2016
        //[Required(ErrorMessage = "Postal Code")]
        //[DataType(DataType.MultilineText)]
        //[Display(Name = "Postal Code")]
        public string PresentPostalCode { set; get; } //new 11-6-2016
        //[Required(ErrorMessage = "District Required")]
        //[DataType(DataType.MultilineText)]
        //[Display(Name = "District")]
        public string ParmanentDistrict { get; set; } //new 11-6-2016
        //[Required(ErrorMessage = "Postal Code")]
        //[DataType(DataType.MultilineText)]
        //[Display(Name = "Postal Code")]
        public string ParmanentPostalCode { set; get; } //new 11-6-2016
        public DateTime EntryTime { set; get; }
    }
    public class StudentInfo
    {

        public int StudentInfoId { set; get; }

        [Display(Name = "Student's ID")]
        [Index("IX_StudentId")]
        [StringLength(20)]
        public string StudentId { set; get; }

        [Required]
        [Index("IX_NameOfStudent")]
        [Display(Name = "Student's Name")]
        [StringLength(70,ErrorMessage = "Student's name cann't be more than 70 characters")]
        public string StudentName { set; get; }

        [StringLength(70, ErrorMessage = "Fathers name cann't be more than 70 characters")]
        [Display(Name = "Father's / Husband Name")]
        public string FathersName { set; get; }

        [StringLength(70, ErrorMessage = "Mothers name lengths cann't be more than 70 characters")]
        [Display(Name = "Mother's Name")]
        public string MothersName { set; get; }

       
        [DataType(DataType.MultilineText)]
        [Display(Name = "Present Address")]
        public string PresentAddress { set; get; }//new 11-6-2016
        //[Required(ErrorMessage = "District Required")]
        //[DataType(DataType.MultilineText)]
        //[Display(Name = "District")]
        public string PresentDistrict { get; set; } //new 11-6-2016
        //[Required(ErrorMessage = "Postal Code")]
        //[DataType(DataType.MultilineText)]
        //[Display(Name = "Postal Code")]
        public string PresentPostalCode { set; get; }
        [Required]
        [Display(Name = "Parmanent Address")]
        [DataType(DataType.MultilineText)]
        public string ParmanentAddress { set; get; }
        //[Required(ErrorMessage = "District Required")]
        //[DataType(DataType.MultilineText)]
        //[Display(Name = "District")]
        public string ParmanentDistrict { get; set; } //new 11-6-2016
        //[Required(ErrorMessage = "Postal Code")]
        //[DataType(DataType.MultilineText)]
        //[Display(Name = "Postal Code")]
        public string ParmanentPostalCode { set; get; } //new 11-6-2016
        [Display(Name = "Phone No.")]
        public string PhoneNo { set; get; }

        [Display(Name = "Mobile No.")]
        public string MobileNo { set; get; }


        [Display(Name = "Date Of Birth")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Please Insert Date")]
        public DateTime DateOfBirth { set; get; }

        //////////////////////-------------combo---------------------
        [Display(Name = "Blood Group")]
        public int BloodGroupsId { set; get; }

        [Display(Name = "Maritial Status")]
        public int MaritalStatusId { set; get; }

        [Required]
        [Display(Name = "Gender")]
        public int GenderId { set; get; }
        //////////////////////-------------combo---------------------
        [Display(Name = "Nationality")]
        [StringLength(20)]
        public string Nationality { set; get; }

        [Display(Name = "Skills On Others Field")]
        public string SkillInOtherfield { set; get; }
    
        [Display(Name = "Local Guardian Name")]
        public string LocalGuardianName { set; get; }
        [Required]
        [Display(Name = "Relation's With Guardian")]
        public string LocalGuardianRelationship { set; get; }

        [Required]
        [Display(Name = "Address Of Local Guardian")]
        [DataType(DataType.MultilineText)]
        public string LocalGuardianAddress { set; get; }
        
        [Display(Name = "Contact No Of Local Guardian")]
        public string LocalGuardianContact { set; get; }

        [Display(Name = "Email")][DataType(DataType.EmailAddress)]
        public string EmailAddress { set; get; }

        public DateTime? EntryTime { set; get; }

        public string Father_mobile { get; set; }
        public string Father_occupation { get; set; }
        public string Father_designation { get; set; }
        public string Father_organization { get; set; }
        public string Father_income { get; set; }
        public string Mother_mobile { get; set; }
        public string Mother_occupation { get; set; }
        public string Mother_designation { get; set; }
        public string Mother_organization { get; set; }
        public string mother_income { get; set; }
        public string Spouse_name { get; set; }
        public string Spouse_mobile { get; set; }
        public string Spouse_occupation { get; set; }
        public string Spouse_designation { get; set; }
        public string Spouse_organization { get; set; }
        public string FeePayer_name { get; set; }
        public string FeePAyer_mobile { get; set; }
        public string FeePayer_income { get; set; }
        public string FeePayer_Relation { get; set; }
        public string Advt_reference { get; set; }

        //////////////////////////////
        public virtual BloodGroups BloodGroups { set; get; }
        public virtual MaritalStatus MariaStatus { set; get; }
        public virtual Gender Genders { set; get; }
        //public virtual UnfairStudents UnfairStudents { set; get; }
        public int? Admission_waiver { get; set; }



    }
}