﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
   public class PaymentDue
    {

        public int paymentdueID { set; get; }
        public string StudentId { set; get; }
        public int SemesterId { set; get; }
        public int Totaldue { set; get; }
        public string description { set; get; }
        public virtual Semester semester { set; get; }
       // public virtual StudentIdentification StudentIdentification { set; get; }
    }
}
