﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class CourseForDepartment
    {
        public int CourseForDepartmentId { set; get; }

        [Display(Name = "Semester")]
        public int SerializedSemesterId { set; get; }

        [Display(Name = "Department")]
        [Index("IX_DeptID")]
        public int DepartmentId { set; get; }

        [Display(Name = "Course Type")]
        public string CourseType { set; get; }

        [Display(Name = "Course Code")]
        public string CourseCode { set; get; }
        [StringLength(180)]
        [Display(Name = "Course Title/Name")]
        [Index("IX_CoursName")]
        public string CourseName { set; get; }

        [Display(Name = "Prerequisite Course")]
        public string PrerequisiteCourse { set; get; }
        public double Credit { set; get; }
        public bool Deactivate { set; get; }
        public string Note { set; get; }
        public int GradingSystemId { set; get; } //Added 21-3-2016
        public string Remarks { set; get; }
        public virtual SerializedSemester SerializedSemester { set; get; }
        public virtual Department Department { set; get; }
        public virtual ICollection<CourseForStudentsAcademic> StudentsAcademics { set; get; }
        public virtual ICollection<Section> Sections { set; get; }
    }
}