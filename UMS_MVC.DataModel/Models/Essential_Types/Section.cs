﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class Section
    {
        public Section()
        {
            //CreatedDate=DateTime.Now;
        }

        public int SectionId { set; get; }
        public string SectionName { set; get; }
        [Index("IX_DeptCourseID")]
        public int CourseForDepartmentId { set; get; }
        public int? TeacherId { set; get; }
        public bool Status { set; get; }
        [Index("IX_SemesterID")]
        public int SemesterId { set; get; }
        [Display(Name = "Created Date")]
        public DateTime? CreatedDate { set; get; }
        public int? GradingSystemId { set; get; }
        [Display(Name = "Short Note")]
        [DataType(DataType.MultilineText)]
        public string ShortNote { set; get; }
        [Display(Name = "High light")]
        [Index("IX_Highlighted")]
        public bool HighLight { set; get; }
        //Newly Added
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? ExpireDateTime { set; get; }
        [Display(Name = "Grant Mid Term")]
        public bool MidTermEx { set; get; }
        [Display(Name = "Grant Final Term")]
        public bool FinalTermEx { set; get; }
        [Display(Name = "Evaluation Possible")]
        [Index("IXEvaluationPossbility")]
        public bool EvaluationPossible { get; set; } // added 5-11-2015
        [DisplayName("Faculty Submit Final")]
        public bool ConfirmSubmitByFaculty { set; get; } // Added 21-3-2016
        public DateTime? SumissionDate { set; get; } // Added 21-3-2016
        public bool RevisedStat { get; set; } // Added 24-4-2016
        public DateTime? ResubmitDate { get; set; } // Added 24-4-2016
        public bool NonCreditMarkSubmit { set; get; } // Added 27-7-2016
        public virtual CourseForDepartment CourseForDepartment { set; get; }
        public virtual Teacher Teacher { set; get; }
        public virtual Semester Semester { set; get; }
        public virtual ICollection<CourseForStudentsAcademic> CourseForStudentsAcademics { set; get; }
        public virtual GradingSystem GradingSystem { set; get; }

    }
}