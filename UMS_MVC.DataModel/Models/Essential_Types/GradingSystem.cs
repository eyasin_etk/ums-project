﻿using System;
using System.ComponentModel.DataAnnotations;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class GradingSystem
    {
        public int GradingSystemId { set; get; }
        public int GradingCode { set; get; }

        [Display(Name = "Grading Name")]
        public String GradingSystemName { set; get; }
        [Display(Name = "Att")]
        public double Attendance { set; get; }
        [Display(Name = "CT")]
        public double ClassTest { set; get; }
        [Display(Name = "Mid")]
        public double MidTerm { set; get; }
        [Display(Name = "Final")]
        public double FinalTerm { set; get; }
        [Display(Name ="ATT percent")]
        public double AttendancePercent { set; get; }
        [Display(Name = "CT percent")]
        public double ClassTestPercent { set; get; }
        [Display(Name = "MID percent")]
        public double MidTermPercent { set; get; }
        [Display(Name = "Final percent")]
        public double FinalTermPercent { set; get; }
        //-- New //Added 21-3-2016
        [Display(Name = "Special Grading")]
        public bool SpecialGrading { get; set; }
        [Display(Name = "Special Pass Grade")]
        public string SpecPassGrade { get; set; }
        [Display(Name = "Special Fail Grade")]
        public string SpecFailGrade { get; set; }
        [Display(Name = "Min Mark to Pass")]
        public double MinMarkToPass { set; get; }
        [Display(Name = "Impact On Result")]
        public bool ImpactOffResult { set; get; }
        //-- New //Added 21-3-2016
    }
}