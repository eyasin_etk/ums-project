﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class StudentAcademicInfo
    {
        public int StudentAcademicInfoId { set; get; }

        [Display(Name = "Student ID")]
        [StringLength(20)]
        [Index("IX_studentID")]
        public string StudentId { set; get; }

        [StringLength(45)]
        [Display(Name = "Name Of The Examination")]
        public string NameOfExamination { set; get; }

        
        [Display(Name = "Session")]
        public string StartingSession { set; get; }

        [StringLength(45)]
        [Display(Name = "University/Board")]
        public string UniversityBoard { set; get; }

        [Display(Name = "Passing Of Year")]
        public string PassingYear { set; get; }

        [Display(Name = "Result/Grade")]
        public string Result { set; get; }

        

        [StringLength(45)]
        [Display(Name = "Subject Studied")]
        public string Group { set; get; }


        public string DistrictBoard { get; set; }

        public string Scale { get; set; }

        public string ResultOptional { set; get; }

    }
}