﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class School
    {
        public int SchoolId { set; get; }
        [Required]
        [StringLength(70)]
        //[Remote("CheckSchoolName","Check",ErrorMessage = "School name Exists")]
        [Display(Name = "School Name")]
        public string SchoolName { set; get; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DateCreated { set; get; }
        public virtual ICollection<Department> Departments { set; get; }
        public virtual ICollection<StudentIdentification> StudentIdInfos{ set; get; }
    }
} 