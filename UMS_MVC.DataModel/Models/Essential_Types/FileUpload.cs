﻿using System;
using System.ComponentModel.DataAnnotations;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class FileUpload
    {
        public int FileUploadId { set; get; }
        public int? AccountId { set; get; }
        [Display(Name = "Upload Date")]
        public DateTime? UploadDate { set; get; }
        [Display(Name = "Expire Date")]
        public DateTime? ExpiredDate { set; get; }
        [Display(Name = "File Name")]
        public string FileName { set; get; }
        public string Description { set; get; }
        public bool Status { set; get; }
        public virtual Account Account { set; get; }
    }
}