﻿using System.ComponentModel.DataAnnotations;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class PowerWorkAccess
    {
        public int PowerWorkAccessId { set; get; }
        [Display(Name = "Account")]
        public int AccountId { set; get; }
        [Display(Name = "Power Work")]
        public int PowerWorkId { set; get; }
        public bool IpRestrictions { set; get; }
        public string PcIpAddress { set; get; }
        public virtual Account Account { set; get; }
        public virtual PowerWork PowerWork { set; get; }

    }
}