﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using UMS_MVC.DataModel.Models.AccountExt;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class Account
    {
        public int AccountId { set; get; }
        [StringLength(50,MinimumLength = 4)]
        [Index("IX_Name")]
        public string Name { set; get; }
        [EmailAddress]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email Address")]
        [MaxLength(50)]
        [RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}", ErrorMessage = "Please enter correct email")]
        //[Remote("CheckSameEmailForAccount", "Check", ErrorMessage = "Email address exists")]
        public string Email { set; get; }
        //[Remote("CheckPhoneForAccount", "Check", ErrorMessage = "This phone number is occupied by another User.")]
        [Display(Name = "Phone")]
        public string PhoneNo { set; get; }

        [Display(Name = "Star Tower/HBR exchange")]
        public string TeleExchange { set; get; }

        public string Designation { set; get; }

        [Display(Name = "Department/Section/Office")]
        public string DepartmentSection { set; get; }
        [StringLength(6)]
        [Display(Name = "Login ID")]
        [Index("IX_LoginId")]
        public string LoginIdentity { set; get; }
        
        [DataType(DataType.Password)]
        public string Password { set; get; }
        [Index("IX_AccountRole")]
        public int AccountsRoleId { set; get; }
        public string Pic { set; get; }
        public bool Status { set; get; }
        
        //---====New Added 31-10-2015
        public Guid? AccountHash { set; get; }
        public bool PartimeTeacher { set; get; }
        public DateTime? ExpireDate { set;get; }
        public DateTime? CreatedDate { set; get; }
        [Display(Name = "Log in Block")]
        public bool Deactivate { set; get; }

        [Display(Name = "Cause Of Deactivate")]
        public string CauseOfDeactivate { set; get; }

        public bool TeporaryBlock { set; get; } //29-3-2016
        public DateTime? TemporaryBlockExpireDate { set; get; } //29-3-2016
        public string LastPsswordChange { get; set; } //29-3-2016
        public virtual AccountsRole AccountsRole { set; get; }
        public virtual ICollection<AccountMetaInformation> AccountMetaInformations { set; get; }
        public virtual ICollection<AccountMetaProfessional> AccountMetaProfessionals { set; get; }
        public virtual ICollection<Teacher> Teachers { set; get; }


    }
}