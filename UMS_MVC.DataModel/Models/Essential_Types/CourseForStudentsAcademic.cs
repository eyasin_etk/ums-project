﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using UMS_MVC.DataModel.Models.UniversalTypes;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class CourseForStudentsAcademic
    {
        public int CourseForStudentsAcademicId { set; get; }
        [Display(Name = "Student ID")]
        [Index("IX_StudentID")]
        public int StudentIdentificationId { set; get; }
        [Display(Name = "Course Code")]
        [Index("IX_DeptCourseID")]
        public int CourseForDepartmentId { set; get; }
        [Display(Name = "Semester")]
        [Index("IX_SemesterID")]
        public int SemesterId { set; get; }
        public double Attendance { set; get; }
        [Display(Name = "Class Test")]
        public double ClassTest { set; get; }
        [Display(Name = "Mid Term")]
        public double Midterm { set; get; }
         [Display(Name = "Final Term")]
        public double FinalTerm { set; get; }
         [Display(Name = "Total Term")]
        public double TotalMark { set; get; }
        [Range(0.0, 4.0)]
        public double Grade { set; get; }
         [Display(Name = "Total Grade")]
        public double TotalGrade { set; get; }

        [Display(Name = "Letter Grade")]
        public string LetterGrade { set; get; }
        public int CourseStatusId { set; get; }
        public int? PreviousCourseStatus { get; set; } //Added 30-6-2016
        public DateTime AddedDate { set; get; }
        public int? SectionId { set; get; }
        public int? PaymentRegistrationId { set; get; }
        public int? GradingSystemId { set; get; }
        public int? ReferenceCourseId { get; set; }
       
        [Display(Name = "Retake Advising")]
        public bool RetakeAdv { get; set; } // Added 1-11-2015
        [Display(Name = "Improvement Advising")]
        public bool ImpAdv { get; set; } // Added 1-11-2015
        public bool EvaluationComplete { set; get; }// Added 5-11-2015
        public bool DeclareMajor { set; get; }
        //-- New
        [Index("IX_Withhelded")]
        public bool WithHelded { set; get; }
        public bool ReportedCase { set; get; }//Added 21-3-2016
        public string Remarks { set; get; }//Added 21-3-2016
        public bool FirstScrutinized { set; get; }//Added 21-3-2016 //updated 18-4-2016
        public bool SecondScrutinized { get; set; }
        public string ScrutinizedBy { set; get; }//Added 21-3-2016 === this option will save user name with date.
        public bool SuspendedForProbation { get; set; }
        //public bool FaultyMark { get; set; } //Added 20-4-2016 for further evaluation
        
        //-- New
        public virtual PaymentRegistration PaymentRegistration { set; get; }

        /// ///////////------------------------/////////////////////////
        public virtual Section Section { set; get; }
        public virtual CourseForDepartment CourseForDepartment { set; get; }
        public virtual Semester Semester { set; get; }
        public virtual StudentIdentification StudentIdentification { set; get; }
        public virtual CourseStatus CourseStatus { set; get; }
        public virtual GradingSystem GradingSystem { set; get; }


        //public CourseForStudentsAcademic()
        //{
        //    Attendance = 0;
        //    ClassTest = 0;
        //    Midterm = 0;
        //    FinalTerm = 0;
        //    TotalMark = 0;
        //    Grade = 0;
        //    TotalGrade = 0;
        //}

    }
}