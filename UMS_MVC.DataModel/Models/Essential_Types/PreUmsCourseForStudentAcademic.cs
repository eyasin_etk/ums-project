﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class PreUmsCourseForStudentAcademic
    {
        public int PreUmsCourseForStudentAcademicId { set; get; }
        [Display(Name = "Student ID")]
        [Index("IX_StudentID")]
        public int StudentIdentificationId { set; get; }
        [Display(Name = "Course Code")]
        [Index("IX_DeptCourseID")]
        public int CourseForDepartmentId { set; get; }
        [Display(Name = "Semester")]
        [Index("IX_SemesterID")]
        public int SemesterId { set; get; }
        public double Attendance { set; get; }
        [Display(Name = "Class Test")]
        public double ClassTest { set; get; }
        [Display(Name = "Mid Term")]
        public double Midterm { set; get; }
        [Display(Name = "Final Term")]
        public double FinalTerm { set; get; }
        [Display(Name = "Total Term")]
        public double TotalMark { set; get; }
        public bool FaultyMark { get; set; } //Added 20-4-2016
        [Display(Name = "Given mark")]
        public double FalutyGivenMark { get; set; }
        [Range(0.0, 4.0)]
        public double Grade { set; get; }
        [Display(Name = "Total Grade")]
        public double TotalGrade { set; get; }

        [Display(Name = "Letter Grade")]
        public string LetterGrade { set; get; }
        [Index("IX_CStat")]
        public int CourseStatusId { set; get; }
        public int? GradingSystemId { set; get; }
        public DateTime AddedDate { get; set; }
        public bool ReportedCase { set; get; }
        public string Remarks { set; get; }
        public bool FirstScrutinized { set; get; }
        public bool SecondScrutinized { get; set; }
        public string ScrutinizedBy { set; get; }
        
        
        public virtual CourseForDepartment CourseForDepartment { set; get; }
        public virtual Semester Semester { set; get; }
        public virtual StudentIdentification StudentIdentification { set; get; }
        public virtual GradingSystem GradingSystem { set; get; }
    }
}
