﻿using System;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class PatchNotes
    {
        public int PatchNotesId { set; get; }
        public int PatchVersionId { set; get; }
        public string PatchPoint { set; get; }
        public DateTime? DateSubmited { set; get; }
        public virtual PatchVersion PatchVersion { set; get; }
    }
}