﻿using System.ComponentModel.DataAnnotations;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class DocumentAdding
    {
        public int DocumentAddingId { set; get; }
        [Display(Name = "Student ID")]
        public string StudentId { set; get; }
        [Display(Name = "Certificate")]
        public bool SscCertificate { set; get; }
        [Display(Name = "Mark Sheet")]
        public bool SscMarkSheet { set; get; }
        [Display(Name = "Certificate")]
        public bool HscCertificate { set; get; }
        [Display(Name = "Mark Sheet")]
        public bool HscMarkSheet { set; get; }
        [Display(Name = "Certificate")]
        public bool DiplomaCertificate { set; get; }
        [Display(Name = "Mark Sheet")]
        public bool DiplomaMarkSheet { set; get; }
        [Display(Name = "O-Level")]
        public bool Gce5Olavel { set; get; }
        [Display(Name = "2 GCE A-Level")]
        public bool Gce5Alavel { set; get; }
        [Display(Name = "Certificate")]
        public bool BaBsCertificate { set; get; }
        [Display(Name = "Mark Sheet")]
        public bool BaBsMarkSheet { set; get; }
        [Display(Name = "Certificate")]
        public bool MaMsCertificate { set; get; }
        [Display(Name = "Mark Sheet")]
        public bool MaMsMarkSheet { set; get; }
        [Display(Name = "Two Letters of Recommendation")]
        public bool TwoLettersofRecommendation { set; get; }
        [Display(Name = "5 Stamp Size Photographs")]
        public bool TwoStampSizePhotoGraph { set; get; }
        [Display(Name = "SSC Testimonial")]
        public bool SscTestimonial { set; get; }
         [Display(Name = "HSC Testimonial")]
        public bool HscTestimonial { set; get; }
    }
}