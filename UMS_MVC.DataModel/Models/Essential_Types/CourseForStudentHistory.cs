﻿using System;
using System.ComponentModel.DataAnnotations;
using UMS_MVC.DataModel.Models.UniversalTypes;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class CourseForStudentHistory
    {
        public int CourseForStudentHistoryId { set; get; }
        [Display(Name = "Student ID")]
        public int StudentIdentificationId { set; get; }
        [Display(Name = "Course Code")]
        public int CourseForDepartmentId { set; get; }
        [Display(Name = "Semester")]
        public int SemesterId { set; get; }
        public double Attendance { set; get; }
        [Display(Name = "Class Test")]
        public double ClassTest { set; get; }
        [Display(Name = "Mid Term")]
        public double Midterm { set; get; }
        [Display(Name = "Final Term")]
        public double FinalTerm { set; get; }
        [Display(Name = "Total Term")]
        public double TotalMark { set; get; }
        [Range(0.0, 4.0)]
        public double Grade { set; get; }
        [Display(Name = "Total Grade")]
        public double TotalGrade { set; get; }

        [Display(Name = "Letter Grade")]
        public string LetterGrade { set; get; }
        public int CourseStatusId { set; get; }
        public DateTime AddedDate { set; get; }
        public int? SectionId { set; get; }
        public DateTime? DeletedTime { set; get; }
        /// ///////////------------------------/////////////////////////
        public virtual Section Section { set; get; }
        public virtual CourseForDepartment CourseForDepartment { set; get; }
        public virtual Semester Semester { set; get; }
        public virtual StudentIdentification StudentIdentification { set; get; }
        public virtual CourseStatus CourseStatus { set; get; }
        
    }
}