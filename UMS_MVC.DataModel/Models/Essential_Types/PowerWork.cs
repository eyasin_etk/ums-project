﻿using System.ComponentModel.DataAnnotations;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class PowerWork
    {
        public int PowerWorkId { set; get; }
        [Display(Name = "Controller")]
        public string ControllerName { set; get; }
        [Display(Name = "Method")]
        public string MethodName { set; get; }
        public bool Restricted { set; get; }
        [DataType(DataType.Password)]
        public string Password { set; get; }
    }
}