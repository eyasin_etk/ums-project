﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using UMS_MVC.DataModel.Models.UniversalTypes;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class Payment
    {
        [Display(Name = "Panyment Slip No")]
        public int PaymentId { set; get; }
        //////////PaymentRegistrations/////////////////////////////////////
       [Index("IX_PaymentRegiID")]
        public int PaymentRegistrationId { set; get; }
        [Index("IX_PaymentRegiNO")]
        public int RegistrationNo { set; get; }
        //////////PaymentRegistrations/////////////////////////////////////
        public int PaymentTypeId { set; get; }
        public DateTime PrintDate { set; get; }
        [Display(Name = "Printed By")]
        public string PrintedBy { set; get; } //////Can be From Account / Students
        ///////--Fees--//////////////////////////
        public string Subject { set; get; }
        [Range(0, 999999999, ErrorMessage = "Amount cann't be less than 0")]
        [Display(Name = "Admission")]
        public double AdmissionFee { set; get; }
        [Range(0, 999999999, ErrorMessage = "Amount cann't be less than 0")]
        [Display(Name = "Tuition")]
        public double TutionFee { set; get; }
        [Range(0, 999999999, ErrorMessage = "Amount cann't be less than 0")]
        [Display(Name = "Lab/Thesis Fee")]
        public double LabThesisFee { set; get; }
        [Range(0, 999999999, ErrorMessage = "Amount cann't be less than 0")]
        [Display(Name = "Other Fee")]
        public double OtherFee { set; get; }
        [Range(0, 999999999, ErrorMessage = "Amount cann't be less than 0")]
        [Display(Name = "Re-Reg./Imp. Fee")]
        public double ReRegiFee { set; get; }
        [Display(Name = "Late Advising Fee")]
        public double LateFee { set; get; }
        [Range(0, 999999999, ErrorMessage = "Amount cann't be less than 0")]
        [Display(Name = "Total")]
        public double Total { set; get; }
        [Range(0, 999999999, ErrorMessage = "Amount cann't be less than 0")]
        [Display(Name = "Rebate/Discount")]
        public double Rebate { set; get; }
        [Display(Name = "Previous Balance")]
        public double PreviousBalance { set; get; }
        [Display(Name = "Net Total Payable")]
        public double NetTotal { set; get; }
        [Range(0,999999999,ErrorMessage = "Amount cann't be less than 0")]
        [Display(Name = "Amount to be paid")]
        public double AmountWillPay { set; get; }
        [Range(0, 999999999, ErrorMessage = "Amount cann't be less than 0")]
        public double AmountPaid { set; get; }

        [Display(Name = "Balance")]
        public double? Balance { set; get; }
        public bool Status { set; get; }
        public virtual PaymentRegistration PaymentRegistration { set; get; }
        public virtual PaymentType PaymentType { set; get; }


        public double GetTotal(double admission = 0, double tutionFee=0,double labfee=0,double other=0,double rereg=0,double late=0)
        {
            AdmissionFee = admission;
            TutionFee = tutionFee;
            LabThesisFee = labfee;
            OtherFee = OtherFee;
            ReRegiFee = rereg;
            LateFee = late;
            Total=AdmissionFee + TutionFee + LabThesisFee + OtherFee + ReRegiFee + LateFee;
            Balance = Total;
            return Total;
        }

        public double GetNetTotal(double rebate=0,double previousBal=0)
        {
            double calTotal=0;
            if (rebate > 0)
            {
                calTotal =Total- ((Total*rebate)/100);
            }
            if (previousBal>0)
            {
                calTotal = calTotal + previousBal;
            }
            NetTotal = calTotal;
            return NetTotal;
        }
    }
}