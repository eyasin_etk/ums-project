﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class SpecifiedConfigurationOfDepartment //Added 27-7-2016 for the need of advising, markupload etc
    {
        public int SpecifiedConfigurationOfDepartmentId { get; set; }
        [Index("IX_DepartmentId", 2, IsUnique = true)]
        public int DepartmentId { set; get; }
        [Index("IX_SemesterId")]
        public int SemesterId { set; get; }
        public bool Advising { set; get; }
        public bool MarkUpload { get; set; }
        
        public bool ActivateMidTerm { set; get; }
        public bool ActivateFinalTerm { set; get; }

        public DateTime? AdvisingStartingDate { get; set; } 
        public DateTime? AdvisingExpireDate { get; set; } 

        public DateTime? MarkUploadStartDate { get; set; }
        public DateTime? MarkUploadExpiretDate { get; set; }

        [StringLength(65)]
        public string Remarks { set; get; }
        public DateTime EntryDate { set; get; } // will enter automatically

        public virtual Semester Semester { set; get; }
        public virtual Department Department { set; get; }

    }
}
