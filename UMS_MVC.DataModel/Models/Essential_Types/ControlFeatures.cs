﻿using System;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
    public class ControlFeatures
    {
        public int ControlFeaturesId { set; get; }
        public string FeatureName { set; get; }
        public bool ToggleFeature { set; get; }
        // New
        public string SpecialAttributes { get; set; }//Added 21-3-2016
        public string SpecialAccessTo { get; set; }//Added 21-3-2016
        public DateTime Date { get; set; }//Added 21-3-2016
        public string Path { get; set; }//Added 21-3-2016
        public string Remarks { get; set; }//Added 21-3-2016
        // New
    }
}