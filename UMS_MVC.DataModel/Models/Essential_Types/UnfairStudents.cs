﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMS_MVC.DataModel.Models.Essential_Types
{
   public class UnfairStudents
    {    [Key]
        public int StudentUnfairId { set; get; }
        public string StudentId { set; get; }
        public int SemesterId { set; get; }

       
        public string unfairdetails { set; get; }
        public int type { set; get; }
        public virtual Semester semester { set; get; }
    }
}
