﻿namespace UMS_MVC.DataModel.Models.MasterAdminProtocol
{
    public class UmsMasterSetting
    {
        public int UmsMasterSettingId { get; set; }
        public bool AccountLoginActivations { get; set; }
        public string StatusMessage { set; get; }
    }
}