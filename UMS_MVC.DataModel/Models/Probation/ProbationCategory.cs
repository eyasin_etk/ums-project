﻿using System.Collections.Generic;

namespace UMS_MVC.DataModel.Models.Probation
{
    public class ProbationCategory
    {
        public int ProbationCategoryId { get; set; }
        public string ProbationName { get; set; }
        public string Remarks { get; set; }
        public virtual ICollection<ProbationFallenStudent> ProbationFallenStudents { set; get; }

    }
}
