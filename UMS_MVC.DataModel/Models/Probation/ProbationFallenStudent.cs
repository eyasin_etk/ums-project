﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.DataModel.Models.Probation
{
    public class ProbationFallenStudent
    {
        public int ProbationFallenStudentId { get; set; }
        [Index("IX_StudentId")]
        public int StudentIdentificationId { set; get; }
        [Index("IX_SemesterId")]
        public int SemesterId { set; get; }
        [Index("IX_Category")]
        public int ProbationCategoryId { get; set; }
        public string Remarks { get; set; }
        [Index("IX_Cgpa")]
        public double Cgpa { get; set; }
        public double Sgpa { get; set; }
        public double SemesterCredits  { get; set; }
        public double CreditCompleted { get; set; }
        public bool ActivateSuspend { get; set; }
        public DateTime CreationDate { set; get; }

        public virtual Semester Semester { set; get; }
        public virtual ProbationCategory ProbationCategory { set; get; }
        public virtual StudentIdentification StudentIdentification { set; get; }

    }
}
