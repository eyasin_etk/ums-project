﻿using UMS_MVC.DataModel.Models.UniversalTypes;

namespace UMS_MVC.DataModel.Models.CreditTransfer
{
    public class CreditTransferAcceptedCourse:CommonDataForModels
    {
        public int CreditTransferAcceptedCourseId { get; set; }
        public string PreCourseNameCode { set; get; }
        public double PreCredit { get; set; }
        public string PreGrade { get; set; }
        public string EquivalentCourseNameCode { get; set; }
        public double EquiCredit { get; set; }
        public string EquiGrade { set; get; }
        public string LogicalGrade { get; set; }

    }
}
