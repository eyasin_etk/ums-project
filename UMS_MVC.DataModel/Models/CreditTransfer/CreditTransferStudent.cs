﻿using System.ComponentModel.DataAnnotations.Schema;
using UMS_MVC.DataModel.Models.UniversalTypes;

namespace UMS_MVC.DataModel.Models.CreditTransfer
{
    public class CreditTransferStudent:CommonDataForModels
    {
        public int CreditTransferStudentId { get; set; }
        [Index("IX_StudentId")]
        public int StudentIdentificationId { set; get; }
        public string PreviousInstituteName { get; set; }
        public string InstituteAdmittedProgram { set; get; }
        public string YearAdmitted { get; set; }
        public double InstituteCreditCompleted { get; set; }
        public double CreditAccepted { get; set; }
        public double InstituteTotalCredit { get; set; }
        public double Cgpa { set; get; }

    }
}
 