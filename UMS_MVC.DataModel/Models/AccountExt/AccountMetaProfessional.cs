﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.DataModel.Models.AccountExt
{
    public class AccountMetaProfessional
    {
        public int AccountMetaProfessionalId { get; set; }
        public int AccountId { set; get; }
        [Display(Name = "Background")]
        public string BackGround { get; set; }
        [Display(Name = "Research Interest")]
        public string ResearchInterest { get; set; }
        [Display(Name = "PHD Super Vision")]
        public string PhdSuperVision { get; set; }
        public string Publication { set; get; }
        [Display(Name = "Media ContriBution")]
        public int MediaContriBution { get; set; }
        [Display(Name = "Entry Date")]
        public DateTime? EntryDate { set; get; }
        [Display(Name = "Entry By")]
        public string EntryBy { set; get; }
        public virtual Account Account { set; get; }
        [NotMapped]
        public virtual ICollection<AccountExtEducation> AccountExtEducations { set; get; }
        public virtual ICollection<AccountExtProject> AccountExtProjects { set; get; }
        public virtual ICollection<AccountExtPublication> AccountExtPublications { set; get; }
        public virtual ICollection<AccountExtProfessionalActivity> AccountExtProfessionalActivities { set; get; }


    }
}