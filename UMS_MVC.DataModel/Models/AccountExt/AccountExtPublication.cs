﻿using System;
using System.ComponentModel.DataAnnotations;

namespace UMS_MVC.DataModel.Models.AccountExt
{
    public class AccountExtPublication
    {
        public int AccountExtPublicationId { get; set; }
        public int AccountMetaProfessionalId { get; set; }

        public string Type { get; set; }
        public string Subject { get; set; }
        public string Description { set; get; }
        [Display(Name = "Date Of Publication")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DateOfPublication { get; set; }
        [Display(Name = "Reference Link")]
        public string ReferenceLink { set; get; }

        // Track
        [Display(Name = "Entry Date")]
        public DateTime? EntryDate { set; get; }
        [Display(Name = "Entry By")]
        public string EntryBy { get; set; }
        public virtual AccountMetaProfessional AccountMetaProfessional { set; get; }

    }
}