﻿using System;
using System.ComponentModel.DataAnnotations;

namespace UMS_MVC.DataModel.Models.AccountExt
{
    public class AccountExtProfessionalActivity
    {
        public int AccountExtProfessionalActivityId { set; get; }
        public int AccountMetaProfessionalId { get; set; }
        [Display(Name = "Attend Type")]
        public string AttendType { set; get; }
        [Display(Name = "Date Of Activity")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public string DateOfActivity { set; get; }
        public string Location { set; get; }
        public string Description { get; set; }
        //Track
        [Display(Name = "Entry Date")]
        public DateTime? EntryDate { set; get; }
        [Display(Name = "Entry By")]
        public string EntryBy { set; get; }
        public virtual AccountMetaProfessional AccountMetaProfessional { set; get; }

    }
}