﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UMS_MVC.DataModel.Models.AccountExt
{
    public class AccountExtEducation
    {
        public int AccountExtEducationId { get; set; }
        public int AccountMetaProfessionalId { get; set; }
        [Display(Name = "Examination")]
        public string NameOfExamination { set; get; }

        [Display(Name = "Session")]
        public string StartingSession { set; get; }

        [StringLength(45)]
        [Display(Name = "University/Board")]
        public string UniversityBoard { set; get; }

        [Display(Name = "Year of passing")]
        public string PassingYear { set; get; }

        [Display(Name = "Result/Grade")]
        public string Result { set; get; }

        [StringLength(45)]
        [Display(Name = "Subject Studied")]
        public string SubjectStudied { set; get; }
        // Track
        [Display(Name = "Entry Date")]
        public DateTime? EntryDate { set; get; }
        [Display(Name = "Entry By")]
        public string EntryBy { get; set; }
        public virtual AccountMetaProfessional AccountMetaProfessional { set; get; }

    }
}