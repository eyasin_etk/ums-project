﻿using System;
using System.ComponentModel.DataAnnotations;

namespace UMS_MVC.DataModel.Models.AccountExt
{
    public class AccountExtProject
    {
        
        public int AccountExtProjectId { get; set; }
        public int AccountMetaProfessionalId { get; set; }
        [Display(Name = "Project Title")]
        public  string  ProjectTitle { get; set; }
        [Display(Name = "Short Description")]
        [DataType(DataType.MultilineText)]
        public string ShortDescription { set; get; }
        public string Location { get; set; }
        [Display(Name = "Project Strated Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? ProjectStratedDate { set; get; }
        [Display(Name = "Project Completed Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? ProjectCompletedDate { set; get; }
        [Display(Name = "Entry Date")]
        // Track
        public DateTime? EntryDate { set; get; }
        [Display(Name = "Entry By")]
        public string EntryBy { get; set; }
        public virtual AccountMetaProfessional AccountMetaProfessional { set; get; }


    }
}