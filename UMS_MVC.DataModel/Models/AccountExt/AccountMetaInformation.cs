﻿using System;
using System.ComponentModel.DataAnnotations;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.DataModel.Models.UniversalTypes;

namespace UMS_MVC.DataModel.Models.AccountExt
{
    public class AccountMetaInformation
    {
        public int AccountMetaInformationId { get; set; }
        public int AccountId { set; get; }
        [Display(Name = "Father's Name")]
        public string FatherName { set; get; }
        [Display(Name = "Mother's Name")]
        public string MothersName { set; get; }
        public int MaritalStatusId { set; get; }
        [Display(Name = "Spouce's Name")]
        public string SpouceName { set; get; }
        public int BloodGroupsId { set; get; }
        public int GenderId { set; get; }
        public string CurrentAddress { set; get; }
        public string PermanentAddress { set; get; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DateOfBirth { set; get; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? JoiningDateTime { set; get; }
        // Track
        public DateTime? EntryDate { set; get; }
        public string EntryBy { get; set; }

        public virtual Gender Gender { set; get; }
        public virtual BloodGroups BloodGroups { set; get; }
        public virtual MaritalStatus MaritalStatus { set; get; }
        public virtual Account Account { set; get; }
        

        

    }
}