﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UMS_MVC.DataModel.Models.Login_History
{
    public class LoginTrackCommon
    {
        [StringLength(25)]
        [Index("IX_EntryTime")]
        public string EntryTime { set; get; }
        public string LastVisitedPage { set; get; }
        public string PcAddress { set; get; }
        public string DeviceType { set; get; }
        public string Browser { set; get; }
    }
}