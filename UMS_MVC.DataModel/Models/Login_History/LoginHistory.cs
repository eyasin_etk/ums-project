﻿using System.ComponentModel.DataAnnotations.Schema;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.DataModel.Models.Login_History
{
    public class LoginHistory:LoginTrackCommon
    {
        public int LoginHistoryId { set; get; }
        [Index("IX_LoginID")]
        public int AccountId { set; get; }
       
        public virtual Account Account { set; get; }
    }
}