﻿using System.ComponentModel.DataAnnotations.Schema;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.DataModel.Models.Login_History
{
    public class LoginHistoryStudent:LoginTrackCommon
    {
        public int LoginHistoryStudentId { set; get; }
        [Index("IX_LoginStudentID")]
        public int StudentIdentificationId { set; get; }
        public virtual StudentIdentification StudentIdentification { set; get; }
    }
}