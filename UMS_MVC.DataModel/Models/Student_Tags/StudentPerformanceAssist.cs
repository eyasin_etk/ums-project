﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.DataModel.Models.Student_Tags
{
    public class StudentPerformanceAssist
    {
        public int StudentPerformanceAssistId { get; set; }
        [Index("IX_StudentID", 1, IsUnique = true)]
        public int StudentIdentificationId { get; set; }
        [Index("IX_Cgpa")]
        public double Cgpa { get; set; }
        public double Sgpa { get; set; }
        [Index("IX_CreditCompleted")]
        public double CreditCompleted { get; set; }
        public double CourseCompleted { get; set; }
        public int SemesterAttended { set; get; }
        public string LastSemesterAttended { set; get; }
       
        [Index("IX_ProbationFallen")]
        public bool ProbationFallenStat { get; set; }
        [Index("IX_ReportedFallen")]
        public bool ReportedCaseFallenStat { get; set; }
        [Index("IX_WithheldedFallen")]
        public bool WithHeldFallenStat { set; get; }
        [Index("IX_Discontinued")]
        public bool DiscontinuedStat { set; get; }// Added 22-3-2016 moved from Student Identification
        public bool GraduatedStat { set; get; }
        //[Index("IX_PassingYear")]
        //public string PassingYear { get; set; }
        public bool ExpelledStat { set; get; }
        public DateTime? LastUpdateTime { set; get; }
        public string Remarks { get; set; }
       
        public virtual StudentIdentification StudentIdentification { set; get; }
    }
}
