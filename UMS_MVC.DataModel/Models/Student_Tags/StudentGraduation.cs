﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.DataModel.Models.Student_Tags
{
    public class StudentGraduation
    {
        public int StudentGraduationId { get; set; }
        [Index("IX_StudentID", 1, IsUnique = true)]
        public int StudentIdentificationId { get; set; }
        public Guid RawGuidSerial { get; set; }  //Guid Number
        public string CertificateSerial { get; set; } // format: 0056/2016
        [Index("IX_ApplyGrads")]
        public bool ApplyForGraduationStat { set; get; }
        public DateTime? ApplyDate { set; get; }
        [Index("IX_ApprovedGrads")]
        public bool ApproveForGraduationStat { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public DateTime? MeetingDate { get; set; }
        public string MeetingPlace { get; set; }
        public bool StdentAttendedInMeetingStat { set; get; }
        public string TranscriptSerial { get; set; }
        public bool AllClearanceDoneStat { get; set; }
        public string PassingYear { get; set; }
        public string GraduationSerial { get; set; } // format: UG 00005 for Undergraudate and PG 00017 as Post graduate 
        [Index("IX_GraduationCmpl")]
        public bool GraduationCompletedStat { get; set; }// Added 22-3-2016 moved from Student Identification
        public DateTime? GraduationCompletionDate { set; get; }// Added 22-3-2016 moved from Student Identification
        public bool ProvisionalCertificateIssuedStat { get; set; } // Default false
        public DateTime? ProvisionalCerPrintDate { set; get; }
        public bool FinalCertificateIssuedStat { get; set; } // Default false
        public DateTime? FianlCerIssueDate { get; set; }
        public string FianlCertificateIssuedBy { get; set; }
        public string GraduationApproveBy { set; get; }
        public string Remarks { get; set; }

        public virtual StudentIdentification StudentIdentification { set; get; }
    }
}
