﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using UMS_MVC.DataModel.Models.PreAdmission;
using UMS_MVC.DataModel.Models.UniversalTypes;

namespace UMS_MVC.DataModel.Models.Student_Tags
{
    public class StudentAttatchedFile: CommonDataForModels
    {
        public int StudentAttatchedFileId { set; get; }
        public int StudentAttatchedFileCategoryId { set; get; }
        public int? StudentIdentificationId { set; get; }
        public Guid? PreAdmissionStudentInfoId { set; get; }
        public string FileName { set; get; } //File name will be like CategoryName +"-"+ Unique serial.pdf
        public bool RestrictedStat { get; set; }
        public bool AdmittedStatus { set; get; }  // will hit from student Side and removed by admission office programatically
        public virtual PreAdmissionStudentInfo PreAdmissionStudentInfo { set; get; }
        public virtual StudentAttatchedFileCategory AttatchedFileCategory { set; get; }
        //SSC
        //public string SscTCertificate { get; set; }
        //public string SscTMarkSheet { get; set; }
        //public string SscTTestimonial { get; set; }
        ////HSC
        //public string HscTCertificate { get; set; }
        //public string HscTMarkSheet { get; set; }
        //public string HscTTestimonial { get; set; }
        ////Diploma
        //public string DiplomaTCertificate { get; set; }
        //public string DiplomaTMarkSheet { get; set; }
        ////5 GCE
        //public string GecTolevel { get; set; }
        //public string GecTaLevel { get; set; }
        ////Ba/Bs
        //public string BaTCertificate { get; set; }
        //public string BaTMarkSheet { get; set; }
        ////Ma/Ms
        //public string MaTCertificate { get; set; }
        //public string MaTMarkSheet { get; set; }
        
    }
}
