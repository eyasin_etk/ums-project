﻿using System.Collections.Generic;

namespace UMS_MVC.DataModel.Models.Notice_Matter
{
    public class NoticeBoardCategory
    {
        public int NoticeBoardCategoryId { set; get; }
        public string CategoryName { set; get; }
        public string Remarks { get; set; }
        public virtual ICollection<NoticeBoardDetail> NoticeBoardDetails { set; get; }

    }
}