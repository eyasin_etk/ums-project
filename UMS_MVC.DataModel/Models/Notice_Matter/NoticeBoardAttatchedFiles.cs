﻿using System;

namespace UMS_MVC.DataModel.Models.Notice_Matter
{
    public class NoticeBoardAttatchedFiles
    {
        public int NoticeBoardAttatchedFilesId { set; get; }
        public DateTime UploadedTime { set; get; }
        public int NoticeBoardDetailId { set; get; }
        public int VisitorsCount { set; get; }
        public bool Visibility { set; get; }
        public string FileName { set; get; }
        public virtual NoticeBoardDetail NoticeBoardDetail { set; get; }

    }
}