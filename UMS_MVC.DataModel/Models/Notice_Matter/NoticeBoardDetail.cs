﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace UMS_MVC.DataModel.Models.Notice_Matter
{
    public class NoticeBoardDetail
    {
        public int NoticeBoardDetailId { set; get; }
        public int NoticeBoardCategoryId { set; get; }
        public string NoticeTitle { set; get; }
        public string NoticeHeading { set; get; }
        public string Description { get; set; }
        public bool TopNews { get; set; }
        public bool HasGoogleDoc { set; get; }
        public string GoogleDocCode { get; set; }
        public bool HasWebCode { get; set; }
        [AllowHtml]
        [StringLength(Int32.MaxValue)]
        public string HWebCode { get; set; }
        public DateTime EntryDate { get; set; }
        public DateTime PublishedDate { set; get; }
        public bool Visibility { set; get; }
        public int? ReadingCount { set; get; }
        public string PublishedBy { set; get; }
        public DateTime? StarTime { set; get; }
        public DateTime? EndTime { set; get; }
        public virtual NoticeBoardCategory NoticeBoardCategory { set; get; }
        public virtual ICollection<NoticeBoardAttatchedFiles> NoticeBoardAttatchedFileses { set; get; }
    }
}