﻿using System;
using System.ComponentModel.DataAnnotations;

namespace UMS_MVC.DataModel.Models.PreAdmission
{
    public class PreAdmissionFiles // Operation Cancelled --#--#--#--#--#--#--
    {
        //Notes
        //All Admission file Data will be processed in server 
        // EX. SscMarkSheet= 'ssc_marksheet_1654754.pdf'. This '1654754' number will come from token serial.
        public int PreAdmissionFilesId { get; set; }
        //[Required(ErrorMessage = "SSC Mark Sheet Is Required")]
        public string SscMarkSheet { get; set; }
        public string SscCertificate { get; set; }
       // [Required(ErrorMessage = "HSC Mark Sheet Is Required")]
        public string HscMarkSheet { get; set; }
        public Guid PreAdmissionStudentInfoId { get; set; }
        public virtual PreAdmissionStudentInfo PreAdmissionStudentInfo { set; get; }
    }
}
