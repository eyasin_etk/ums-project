﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using UMS_MVC.DataModel.Models.UniversalTypes;

namespace UMS_MVC.DataModel.Models.PreAdmission
{
    public class PreAdmissionOfferings
    {
        [Index("IX_SelectedSemester")]
        public int EnrolledSemsterId { set; get; }
        [Required(ErrorMessage = "Choose Departent")]
        [Index("IX_SelectedDept")]
        public int EnrolledDepartmentId { get; set; }
        public bool DiplomaStudent { set; get; }
        public string StudentImage { get; set; }
    }

    public class PreAdmissionStudentInfo: PreAdmissionOfferings
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid PreAdmissionStudentInfoId { get; set; }
        //identity: true, defaultValueSql: "newsequentialid()"

        [Required(ErrorMessage = "Name Is Required")]
        [Index("IX_PANameOfStudent")]
        [StringLength(70, ErrorMessage = "Student's name cann't be more than 70 characters")]
        public string StudentName { set; get; }
        [Required(ErrorMessage = "Father's / Husband Name Is Required")]
        [StringLength(70, ErrorMessage = "Fathers name cann't be more than 70 characters")]
        [Display(Name = "Father's / Husband Name")]
        public string FathersName { set; get; }
        [Required(ErrorMessage = "Mother's Name Is Required")]
        [StringLength(70, ErrorMessage = "Mothers name lengths cann't be more than 70 characters")]
        [Display(Name = "Mother's Name")]
        public string MothersName { set; get; }
        [Required(ErrorMessage = "Present Address Required")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Present Address")]
        public string PresentAddress { set; get; }
        [Required(ErrorMessage = "District Required")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "District")]
        public string PresentDistrict { get; set; } //new 11-6-2016
        [Required(ErrorMessage = "Postal Code")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Postal Code")]
        public string PresentPostalCode { set; get; } //new 11-6-2016
        [Required(ErrorMessage = "Parmanent Address Is Required")]
        [StringLength(200)]
        public string ParmanentAddress { set; get; }
        [Required(ErrorMessage = "District Required")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "District")]
        public string ParmanentDistrict { get; set; } //new 11-6-2016
        [Required(ErrorMessage = "Postal Code")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Postal Code")]
        public string ParmanentPostalCode { set; get; } //new 11-6-2016

        [Required(ErrorMessage = "First Contact No. Is Required")]
        [Index("IX_PhoneNoP", 1, IsUnique = true)]
        [Display(Name = "Phone No 1.")]
        [StringLength(45)]
        public string PersonalContactNo { set; get; }
       
        [Display(Name = "Phone No 2.")]
        [StringLength(45)]
        [Required(ErrorMessage = "Second Contact No. Is Required")]
        public string PhoneNoSecond { set; get; }
        
        [Display(Name = "Date Of Birth")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Please Insert Birth Date")]
        public DateTime DateOfBirth { set; get; }
        public int BloodGroupsId { set; get; }
        public int MaritalStatusId { set; get; }

        [Required(ErrorMessage = "Please Enter Gender")]
        [Display(Name = "Gender")]
        public int GenderId { set; get; }

        [Display(Name = "Nationality")]
        [StringLength(25)]
        public string Nationality { set; get; }
        
        [StringLength(150)]
        [Display(Name = "Skills On Others Field")]
        public string SkillInOtherfield { set; get; }
        [Required(ErrorMessage = "Local Guardian Name Is Required")]
        [Display(Name = "Local Guardian Name")]
        [StringLength(70)]
        public string LocalGuardianName { set; get; }
        [Required(ErrorMessage = "Relation's With Guardian Is Required")]
        [Display(Name = "Relation's With Guardian")]
        [StringLength(70)]
        public string LocalGuardianRelationship { set; get; }

        [Required(ErrorMessage = "Address Of Local Guardian Is Required")]
        [Display(Name = "Address Of Local Guardian")]
        [DataType(DataType.MultilineText)]
        [StringLength(150)]
        public string LocalGuardianAddress { set; get; }
        [Required(ErrorMessage = "Contact No Of Local Guardian Required")]
        [StringLength(85)]
        [Display(Name = "Contact No Of Local Guardian")]
        public string LocalGuardianContact { set; get; }
        
        [DataType(DataType.EmailAddress)]
        public string EmailAddress { set; get; }
        //Server Specified Data
        public string BarCodeSerial { get; set; }
        //public string TokenNumber { get; set; }
        public DateTime EntryTime { get; set; }
        public bool AdmissionOfficeConfirmed { get; set; }
        public bool InfoConfirmed { get; set; }
        //public bool AttatchedFileConfirmed { get; set; }
        //public bool AcademicRecordConfirmed { get; set; }
        //Server Specified Data
        public virtual BloodGroups BloodGroups { set; get; }
        public virtual MaritalStatus MariaStatus { set; get; }
        public virtual Gender Genders { set; get; }
    }
    
}
