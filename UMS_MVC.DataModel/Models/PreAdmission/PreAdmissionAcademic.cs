﻿using System;
using System.ComponentModel.DataAnnotations;

namespace UMS_MVC.DataModel.Models.PreAdmission
{
    public class PreAdmissionAcademic
    {
        public int PreAdmissionAcademicId { get; set; }
        [Required(ErrorMessage = "Name Of The Examination Is Required")]
        [StringLength(45)]
        [Display(Name = "Name Of The Examination")]
        public string NameOfExamination { set; get; }
        [StringLength(45)]
        [Display(Name = "Session")]
        public string StartingSession { set; get; }
        [Required(ErrorMessage = "University/Board Is Required")]
        [StringLength(45)]
        [Display(Name = "University/Board")]
        public string UniversityBoard { set; get; }
        [Required(ErrorMessage = "Passing Of Year Is Required")]
        [StringLength(45)]
        [Display(Name = "Passing Of Year")]
        public string PassingYear { set; get; }
        [Required(ErrorMessage = "Result/Grade Is Required")]
        [StringLength(45)]
        [Display(Name = "Result/Grade")]
        public string Result { set; get; }

        [StringLength(45)]
        [Display(Name = "Subject Studied")]
        public string Group { set; get; }
        public Guid PreAdmissionStudentInfoId { get; set; }
        public virtual PreAdmissionStudentInfo PreAdmissionStudentInfo { set; get; }
    }
}
