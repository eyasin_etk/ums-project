﻿using System;
using System.Collections.Generic;

namespace UMS_MVC.DataModel.Models.TeacherEvaluation
{
    public class SurveyQuestion
    {
        public int SurveyQuestionId { get; set; }
        public String QuestionDetials { get; set; }
        //[Required]
        public int? SelectedAnswer { set; get; }
        public virtual ICollection<SurveyAnswer> PossibleSAnswers { set; get; }

    }
}