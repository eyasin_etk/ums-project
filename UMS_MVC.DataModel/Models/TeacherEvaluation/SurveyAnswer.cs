﻿namespace UMS_MVC.DataModel.Models.TeacherEvaluation
{
    public class SurveyAnswer
    {
        public int SurveyAnswerId { get; set; }
        public string AnswerName { set; get; }
        public double AnswerPoint { get; set; }
    }
}