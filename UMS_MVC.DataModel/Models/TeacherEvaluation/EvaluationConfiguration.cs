﻿using System;
using System.ComponentModel.DataAnnotations;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.DataModel.Models.TeacherEvaluation
{
    public class EvaluationConfiguration
    {
        public int EvaluationConfigurationId { get; set; }
        [Display(Name = "Semester Name")]
        public int SemesterId { set; get; } //Updated from Semester Title 21-3-2016
        [Display(Name = "Start Date")]
        public DateTime? StartDate { set; get; }
        [Display(Name = "End Date")]
        public DateTime? EndDate { get; set; }
        [Display(Name = "Remove Evaluation Date")]
        public DateTime? RemoveEvaluationDate { get; set; }
        [Display(Name = "Removed By")]
        public string RemovedBy { set; get; }

        public string Remarks { get; set; }// 21-3-2016
        public virtual Semester Semester { set; get; }

    }
}