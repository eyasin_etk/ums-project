﻿using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.DataModel.Models.TeacherEvaluation
{
    public class EvaluationResult
    {
        public int EvaluationResultId { get; set; }
        public int SectionId { set; get; } // for projecting Teachers value
        public int QuestionId { set; get; }//Added 21-3-2016

        public double TotalPointYes { get; set; }
        public double TotalPointAverage { get; set; } 
        public double TotalPointNo { get; set; }
        public double TotalStudent { get; set; }//Added 21-3-2016

        public virtual Section Section { set; get; }
    }
}