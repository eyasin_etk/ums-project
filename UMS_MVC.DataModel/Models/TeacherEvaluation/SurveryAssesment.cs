﻿using System;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.DataModel.Models.TeacherEvaluation
{
    public class SurveryAssesment
    {
        public int SurveryAssesmentId { get; set; }
        public int CourseForStudentsAcademicId { set; get; }
        public int SurveyQuestionId { get; set; }
        public int SurveyAnswerId { get; set; }
        public DateTime EntryTime { get; set; }

        public virtual CourseForStudentsAcademic Enrollment { set; get; }
        public virtual SurveyQuestion SurveyQuestion  { set; get; }
        public virtual SurveyAnswer SurveyAnswer { set; get; }

    }
}