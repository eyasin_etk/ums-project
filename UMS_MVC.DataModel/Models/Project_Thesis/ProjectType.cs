﻿namespace UMS_MVC.DataModel.Models.Project_Thesis
{
    public class ProjectType
    {
        public int ProjectTypeId { set; get; }
        public string TypeName { set; get; }
    }
}