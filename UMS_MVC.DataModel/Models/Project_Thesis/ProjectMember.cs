﻿using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.DataModel.Models.Project_Thesis
{
    public class ProjectMember
    {
        public int ProjectMemberId { set; get; }
        public int ProjectId { set; get; }
        public int StudentIdentificationId { set; get; }
        public int CourseForStudentsAcademicId { set; get; }
        public virtual Project Project { set; get; }
        public virtual StudentIdentification StudentIdentity { set; get; }
        public virtual CourseForStudentsAcademic CourseForStudentsAcademic { set; get; }
    }
}