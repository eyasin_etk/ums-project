﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.DataModel.Models.Project_Thesis
{
    public class Project
    {
        public int ProjectId { set; get; }
        public int ProjectTypeId { set; get; }
        public int DepartmentId { set; get; }
        public int ProjectCurrentStatusId { set; get; }
        public int TeacherId { set; get; }
        public int CoTeacherId { set; get; }

        [Display(Name = "Project/Thesis/Intern Title")]
        public string Title { set; get; }

        [Display(Name = "Subject Area")]
        public string Subject { set; get; }
        [Display(Name = "Defination of all key terms")]
        public string KeyTerms { set; get; }
        [Display(Name = "Hypothesis or KeyPoints")]
        public string Hypothesis { set; get; }
        [Display(Name = "Detail Information")]
        public string Detail { set; get; }
        public string ProjectedResult { set; get; }
        [Display(Name = "Potential Conclusion/Implications")]
        public string Conclusion { set; get; }
        public DateTime CreatedDate { set; get; }
        public DateTime? StartDate { set; get; }
        public DateTime? EndDate { set; get; }

        // InternShip optional
        public string InternShipPlaceName { set; get; }
        public string InternAddress { set; get; }
        public string InternPhone { set; get; }

        public virtual ProjectType ProjectType { set; get; }
        public virtual Department Department { set; get; }
        public virtual ProjectCurrentStatus ProjectCurrentStatus { set; get; }
        public virtual Teacher Teacher { set; get; }
        public virtual ICollection<ProjectMember> ProjectMembers { set; get; }

        public Project()
        {
            CreatedDate=DateTime.Now;
        }
    }
}