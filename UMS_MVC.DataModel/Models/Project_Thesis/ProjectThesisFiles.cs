﻿using System;

namespace UMS_MVC.DataModel.Models.Project_Thesis
{
    public class ProjectThesisFiles
    {
        public int ProjectThesisFilesId { set; get; }
        public int ProjectId { set; get; }
        public string FileName { set; get; }
        public virtual Project Project { set; get; }
        public DateTime UploadTime { set; get; }

        public ProjectThesisFiles()
        {
            UploadTime=DateTime.Now;
        }
    }
}