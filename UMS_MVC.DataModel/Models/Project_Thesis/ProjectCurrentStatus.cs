﻿namespace UMS_MVC.DataModel.Models.Project_Thesis
{
    public class ProjectCurrentStatus
    {
        public int ProjectCurrentStatusId { set; get; }
        public string StatusName { set; get; }
        public string ShortName { set; get; }
    }
}