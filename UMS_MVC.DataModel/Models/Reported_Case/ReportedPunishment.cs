﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.DataModel.Models.UniversalTypes;

namespace UMS_MVC.DataModel.Models.Reported_Case
{
    public class ReportedPunishment:CommonDataForModels
    {
        public int ReportedPunishmentId { get; set; }
        [Index("IX_StudentId")]
        public int ReportedStudentId { set; get; }
        [Index("IX_SemesterId")]
        public int SemesterId { set; get; }
        [Index("IX_Status")]
        public bool Status { get; set; }
        public bool SpecialConsideration { get; set; }
        public string SpecialConsiderBy { get; set; }
        public DateTime? SpecialConsiderDate { get; set; }
        public virtual ReportedStudent ReportedStudent { set; get; }
        public virtual Semester Semester { set; get; }

    }
}
