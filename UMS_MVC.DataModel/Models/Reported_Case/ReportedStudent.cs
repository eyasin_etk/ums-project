﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using UMS_MVC.DataModel.Models.UniversalTypes;

namespace UMS_MVC.DataModel.Models.Reported_Case
{
    public class ReportedStudent: CommonDataForModels
    {
        public int ReportedStudentId { set; get; }
        [Index("IX_StudentId")]
        public int StudentIdentificationId { set; get; }
        [StringLength(40)]
        public string ReportedBy { set; get; }
        [StringLength(40)]
        public string CategoryTitle { set; get; }
        public string OffenceDetail { set; get; }
        public string RecommandationDetail { set; get; }
        public DateTime SubmitDate { set; get; }
        [Index("IX_Semster")]
        public int SemesterId { set; get; }
       
        //public DateTime? PunishmentStaringDate { set; get; }// disabled 18-4-2016
        //public DateTime? PunishmentEndingDate { set; get; }// disabled 18-4-2016
        public bool ApprovalFromVc { set; get; }
        public bool ApprovalFromCe { set; get; }
        public bool ActivePunishment { set; get; }
        public string MeetingMemebers { set; get; }
        public DateTime? MeetingTime { set; get; }
       // public virtual StudentIdentification StudentIdentification { set; get; }
        
        //public virtual Semester Semester { set; get; }
       
    }

   
}