﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.DataModel.Models.UniversalTypes
{
    public class BloodGroups
    {
        public int BloodGroupsId { set; get; }

        [Display(Name="Blood Group")]
        public string Name { set; get; }

        public virtual ICollection<StudentInfo> StudentInfos { set; get; }
    }
}