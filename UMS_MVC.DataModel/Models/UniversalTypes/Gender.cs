﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using UMS_MVC.DataModel.Models.AccountExt;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.DataModel.Models.UniversalTypes
{
    public class Gender
    {
        public int GenderId { set; get; }

        [Display(Name = "Gender")]
        public string GenderName { set; get; }

        public virtual ICollection<StudentInfo> StudentInfos { set; get; }
        public virtual ICollection<AccountMetaInformation> Teachers { set; get; }
    }
}