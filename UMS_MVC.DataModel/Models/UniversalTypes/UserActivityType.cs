﻿using System.Collections.Generic;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.DataModel.Models.UniversalTypes
{
    public class UserActivityType
    {
        public int UserActivityTypeId { set; get; }
        public string ActivityType { set; get; }
        public virtual ICollection<UserActivity> UserActivities { set; get; }
    }
}