﻿namespace UMS_MVC.DataModel.Models.UniversalTypes
{
    public class CourseStatus
    {
        public int CourseStatusId { set; get; }
        public string Status { set; get; }
        public string ShortName { set; get; }
        public string ImageName { get; set; }
    }
}