﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.DataModel.Models.UniversalTypes
{
    public class MaritalStatus
    {
        public int MaritalStatusId { set; get; }
        [Display(Name = "Marital Status")]

        public string MaritalStat { set; get; }

        public virtual ICollection<StudentInfo> StudentInfos { set; get; }
    }
}