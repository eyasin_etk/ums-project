﻿namespace UMS_MVC.DataModel.Models.UniversalTypes
{
    public class UniversityName
    {
        public int UniversityNameId { get; set; }
        public string InstituteName { get; set; }
        public string ShortName { get; set; }
        public string Location { get; set; }
        public string FirstPhoneNo { get; set; }
        public string SecondPhoneNo { get; set; }
        public string Remarks { get; set; }
        public string GoogleMapUrl { get; set; }
    }
}
