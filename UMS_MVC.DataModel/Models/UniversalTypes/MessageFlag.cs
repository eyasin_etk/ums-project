﻿namespace UMS_MVC.DataModel.Models.UniversalTypes
{
    public class MessageFlag
    {
        public int MessageFlagId { set; get; }
        public string FlagName { set; get; }
        public string FlagDescription { set; get; }
    }
}