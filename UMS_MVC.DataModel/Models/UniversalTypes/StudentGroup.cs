﻿namespace UMS_MVC.DataModel.Models.UniversalTypes
{
    public class StudentGroup
    {
        public int StudentGroupId { get; set; }
        public string Title { get; set; }
        public string Remarks { get; set; }
        public string OtherProperty { get; set; }
    }
}
