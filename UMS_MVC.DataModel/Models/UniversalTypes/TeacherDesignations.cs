﻿namespace UMS_MVC.DataModel.Models.UniversalTypes
{
    public class TeacherDesignations
    {
        public int TeacherDesignationsId { set; get; }
        public string DesignationTitle { set; get; }
        
    }
}