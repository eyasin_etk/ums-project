﻿namespace UMS_MVC.DataModel.Models.UniversalTypes
{
    public class Year
    {
        public int YearId { set; get; }
        public string Years { set; get; }
    }
}