﻿using System;

namespace UMS_MVC.DataModel.Models.UniversalTypes
{
    public class CommonDataForModels
    {
        public string Remarks { get; set; }
        public string EntryBy { set; get; }
        public string UpdateBy { get; set; }
        public DateTime EntryDate { set; get; }
        public DateTime? UpdateTime { set; get; }
    }
}
