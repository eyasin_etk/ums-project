﻿namespace UMS_MVC.DataModel.Models.UniversalTypes
{
    public class StudentAttatchedFileCategory
    {
        public int StudentAttatchedFileCategoryId { set; get; }
        public string FirstName { set; get; }
        public string CategoryName { set; get; }
        public bool VisibleStudent { set; get; }
        public bool VisibleCustom { set; get; }
        public bool VisibleAdmin { set; get; }
        public string Remarks { set; get; }

    }
}
