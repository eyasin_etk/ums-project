﻿namespace UMS_MVC.DataModel.Models.UniversalTypes
{
    public class PaymentType
    {
        public int PaymentTypeId { set; get; }
        public string TypeName { set; get; }

    }
}