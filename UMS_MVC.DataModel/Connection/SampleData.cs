﻿using System.Data.Entity;

namespace UMS_MVC.DataModel.Connection
{
    public class SampleData : DropCreateDatabaseIfModelChanges<UmsDbContext>
    {

        protected override void Seed(UmsDbContext context)
        {
            //var yeari =new List<string>();
            //for (int i = 2003; i < 2031; i++)
            //{
            //    yeari.Add(Convert.ToString(i));
            //}
            //foreach (var y in yeari)
            //{
            //    var yeas = new Year {Years = y};
            //    context.Years.Add(yeas);
            //}
           
            //new List<SemesterInfo>
            //{
            //    new SemesterInfo {SemesterCode = 1, SemesterName = "Spring"},
            //    new SemesterInfo {SemesterCode = 2, SemesterName = "Summer"},
            //    new SemesterInfo {SemesterCode = 3, SemesterName = "Fall"},
            //    new SemesterInfo {SemesterCode = 4, SemesterName = "Empty"}

            //}.ForEach(i => context.SemesterInfos.Add(i));

            //new List<School>
            //{
            //    new School {SchoolName = "Business", DateCreated = Convert.ToDateTime("10/12/2013")},
            //    new School {SchoolName = "Engineering And Technology", DateCreated = Convert.ToDateTime("5/6/2013")},
            //    new School {SchoolName = "Science", DateCreated = Convert.ToDateTime("3/2/2013")},
            //    new School {SchoolName = "Law", DateCreated = Convert.ToDateTime("10/12/2013")},
            //     new School {SchoolName = "No", DateCreated = Convert.ToDateTime("10/12/2013")},

            //}.ForEach(j => context.Schools.Add(j));

            //new List<BloodGroups>
            //{
            //    new BloodGroups {Name = "Nil"},
            //    new BloodGroups {Name = "AB+"},
            //    new BloodGroups {Name = "AB-"},
            //    new BloodGroups {Name = "A+"},
            //    new BloodGroups {Name = "A-"},
            //    new BloodGroups {Name = "B+"},
            //    new BloodGroups {Name = "B-"},
            //    new BloodGroups {Name = "O+"},
            //    new BloodGroups {Name = "O-"}
                
            //}.ForEach(j => context.BloodGroupses.Add(j));

            //new List<MaritalStatus>
            //{
            //    new MaritalStatus {MaritalStat = "Unmarried"},
            //    new MaritalStatus {MaritalStat = "Married"}
            //}.ForEach(s=>context.MaritalStatuses.Add(s));

            //new List<Gender>
            //{
            //    new Gender{GenderName = "Male"},
            //    new Gender{GenderName = "Female"},
            //}.ForEach(j=>context.Genders.Add(j));

            //new List<Semester>
            //{
            //    new Semester {SemesterNYear = "Spring-2003",CourseAdvising = false,BatchNo = 031},
            //    new Semester {SemesterNYear = "Summer-2003",CourseAdvising = false,BatchNo = 032},
            //    new Semester {SemesterNYear = "Fall-2003",CourseAdvising = false,BatchNo = 033},
            //    new Semester {SemesterNYear = "Spring-2004",CourseAdvising = false,BatchNo = 041},
            //    new Semester {SemesterNYear = "Summer-2004",CourseAdvising = false,BatchNo = 042},
            //    new Semester {SemesterNYear = "Fall-2004",CourseAdvising = false,BatchNo = 043},
            //    new Semester {SemesterNYear = "Spring-2005",CourseAdvising = false,BatchNo = 051},
            //    new Semester {SemesterNYear = "Summer-2005",CourseAdvising = false,BatchNo = 052},
            //    new Semester {SemesterNYear = "Fall-2005",CourseAdvising = false,BatchNo = 053},
            //    new Semester {SemesterNYear = "Spring-2006",CourseAdvising = false,BatchNo = 061},
            //    new Semester {SemesterNYear = "Summer-2006",CourseAdvising = false,BatchNo = 062},
            //    new Semester {SemesterNYear = "Fall-2006",CourseAdvising = false,BatchNo = 063},
            //    new Semester {SemesterNYear = "Spring-2007",CourseAdvising = false,BatchNo = 071},
            //    new Semester {SemesterNYear = "Summer-2007",CourseAdvising = false,BatchNo = 072},
            //    new Semester {SemesterNYear = "Fall-2007",CourseAdvising = false,BatchNo = 073},
            //    new Semester {SemesterNYear = "Spring-2008",CourseAdvising = false,BatchNo = 081},
            //    new Semester {SemesterNYear = "Summer-2008",CourseAdvising = false,BatchNo = 082},
            //    new Semester {SemesterNYear = "Fall-2008",CourseAdvising = false,BatchNo = 083},
            //    new Semester {SemesterNYear = "Spring-2009",CourseAdvising = false,BatchNo = 091},
            //    new Semester {SemesterNYear = "Summer-2009",CourseAdvising = false,BatchNo = 092},
            //    new Semester {SemesterNYear = "Fall-2009",CourseAdvising = false,BatchNo = 093},
            //    new Semester {SemesterNYear = "Spring-2010",CourseAdvising = false,BatchNo = 101},
            //    new Semester {SemesterNYear = "Summer-2010",CourseAdvising = false,BatchNo = 102},
            //    new Semester {SemesterNYear = "Fall-2010",CourseAdvising = false,BatchNo = 103},
            //    new Semester {SemesterNYear = "Spring-2011",CourseAdvising = false,BatchNo = 111},
            //    new Semester {SemesterNYear = "Summer-2011",CourseAdvising = false,BatchNo = 112},
            //    new Semester {SemesterNYear = "Fall-2011",CourseAdvising = false,BatchNo = 113},
            //    new Semester {SemesterNYear = "Spring-2012",CourseAdvising = false,BatchNo = 121},
            //    new Semester {SemesterNYear = "Summer-2012",CourseAdvising = false,BatchNo = 122},
            //    new Semester {SemesterNYear = "Fall-2012",CourseAdvising = false,BatchNo = 123},
            //    new Semester {SemesterNYear = "Spring-2013",CourseAdvising = false,BatchNo = 131},
            //    new Semester {SemesterNYear = "Summer-2013",CourseAdvising = false,BatchNo = 132},
            //    new Semester {SemesterNYear = "Fall-2013",CourseAdvising = false,BatchNo = 133},
            //    new Semester {SemesterNYear = "Spring-2014",CourseAdvising = false,BatchNo = 141},
            //    new Semester {SemesterNYear = "Summer-2014",CourseAdvising = false,BatchNo = 142},
            //    new Semester {SemesterNYear = "Fall-2014", ActiveSemester = true,CourseAdvising = false,BatchNo = 143},
            //    new Semester {SemesterNYear = "Spring-2015",CourseAdvising = false,BatchNo = 151},
            //    new Semester {SemesterNYear = "Summer-2015",CourseAdvising = false,BatchNo = 152},
            //    new Semester {SemesterNYear = "Fall-2015",CourseAdvising = false,BatchNo = 153},
            //    new Semester {SemesterNYear = "Spring-2016",CourseAdvising = false,BatchNo = 161},
            //    new Semester {SemesterNYear = "Summer-2016",CourseAdvising = false,BatchNo = 162},
            //    new Semester {SemesterNYear = "Fall-2016",CourseAdvising = false,BatchNo = 163},
            //    new Semester {SemesterNYear = "Spring-2017",CourseAdvising = false,BatchNo = 171},
            //    new Semester {SemesterNYear = "Summer-2017",CourseAdvising = false,BatchNo = 172},
            //    new Semester {SemesterNYear = "Fall-2017",CourseAdvising = false,BatchNo = 173},
            //    new Semester {SemesterNYear = "Spring-2018",CourseAdvising = false,BatchNo = 181},
            //    new Semester {SemesterNYear = "Summer-2018",CourseAdvising = false,BatchNo = 182},
            //    new Semester {SemesterNYear = "Fall-2018",CourseAdvising = false,BatchNo = 183},
            //}.ForEach(j => context.Semesters.Add(j));

           
            //new List<SerializedSemester>
            //{
            //    new SerializedSemester{SemesterName = "Semester 1"},new SerializedSemester{SemesterName = "Semester 2"},
            //    new SerializedSemester{SemesterName = "Semester 3"},new SerializedSemester{SemesterName = "Semester 4"},
            //    new SerializedSemester{SemesterName = "Semester 5"},new SerializedSemester{SemesterName = "Semester 6"},
            //    new SerializedSemester{SemesterName = "Semester 7"},new SerializedSemester{SemesterName = "Semester 8"},
            //    new SerializedSemester{SemesterName = "Semester 9"},new SerializedSemester{SemesterName = "Semester 10"},
            //    new SerializedSemester{SemesterName = "Semester 11"},new SerializedSemester{SemesterName = "Semester 12"},
            //    new SerializedSemester{SemesterName = "Semester 13"},new SerializedSemester{SemesterName = "Semester 14"},
            //    new SerializedSemester{SemesterName = "Semester 15"},new SerializedSemester{SemesterName = "Semester 16"},
            //    new SerializedSemester{SemesterName = "Semester 17"},new SerializedSemester{SemesterName = "Semester 18"},
            //    new SerializedSemester{SemesterName = "Semester 19"},new SerializedSemester{SemesterName = "Semester 20"},
            //    new SerializedSemester{SemesterName = "Semester 0"}
            //}.ForEach(j => context.SerializedSemesters.Add(j));
            //new List<CourseStatus>
            //{
            //   new CourseStatus{Status = "Invalid",ShortName = "INVAL"},
            //   new CourseStatus{Status = "Valid",ShortName = "VAL"},
            //   new CourseStatus{Status = "Pending",ShortName = "PN"},
            //   new CourseStatus{Status = "Drop",ShortName = "DRP"},
            //   new CourseStatus{Status = "Retake",ShortName = "RT"},
            //   new CourseStatus{Status = "Improve",ShortName = "IPV"},
            //   new CourseStatus{Status = "Non-Credit",ShortName = "NC"},
            //   new CourseStatus{Status = "Incomplete",ShortName = "INC"},
            //}.ForEach(k=>context.CourseStatuses.Add(k));

            //new List<PaymentType>
            //{
            //    new PaymentType{TypeName = "Invoice 1st Receipt"},
            //    new PaymentType{TypeName = "Invoice Nth Receipt"},
            //    new PaymentType{TypeName = "Invoice Other Receipt"},
            //    new PaymentType{TypeName = "Payment"}
            //}.ForEach(s=>context.PaymentTypes.Add(s));
          
            //new List<GradingSystem>
            //{

            //    new GradingSystem{GradingCode = 10,GradingSystemName ="Ums Core", Attendance = 10,AttendancePercent = 5, ClassTest = 50,ClassTestPercent = 25, MidTerm = 100, MidTermPercent = 20,FinalTerm = 100,FinalTermPercent = 50},
            //    new GradingSystem{GradingCode = 11,GradingSystemName ="Ums Lab", Attendance = 0,AttendancePercent = 0, ClassTest = 0,ClassTestPercent = 0, MidTerm = 100, MidTermPercent = 40,FinalTerm = 100,FinalTermPercent = 60},
            //    new GradingSystem{GradingCode = 12,GradingSystemName ="Ums Other", Attendance = 0,AttendancePercent = 0, ClassTest = 0,ClassTestPercent = 0, MidTerm = 0, MidTermPercent = 0,FinalTerm = 100,FinalTermPercent = 100},

            //    new GradingSystem{GradingCode = 20,GradingSystemName ="Pau Core", Attendance = 5,AttendancePercent = 5, ClassTest = 25,ClassTestPercent = 25, MidTerm = 20, MidTermPercent = 20,FinalTerm = 50,FinalTermPercent = 50},
            //    //new GradingSystem{GradingCode = 21,GradingSystemName ="Pau Lab", Attendance = 0,AttendancePercent = 0, ClassTest = 0,ClassTestPercent = 0, MidTerm = 100, MidTermPercent = 40,FinalTerm = 100,FinalTermPercent = 60},
            //    //new GradingSystem{GradingCode = 22,GradingSystemName ="Pau Other", Attendance = 0,AttendancePercent = 0, ClassTest = 0,ClassTestPercent = 0, MidTerm = 0, MidTermPercent = 0,FinalTerm = 100,FinalTermPercent = 100},
                
            //    new GradingSystem{GradingCode = 30,GradingSystemName ="MBA Core", Attendance = 5,AttendancePercent = 5, ClassTest = 30,ClassTestPercent = 30, MidTerm = 25, MidTermPercent = 25,FinalTerm = 40,FinalTermPercent = 40},
            //    new GradingSystem{GradingCode = 40,GradingSystemName ="Pharma Core", Attendance = 5,AttendancePercent = 5, ClassTest = 30,ClassTestPercent = 30, MidTerm = 15, MidTermPercent = 15,FinalTerm = 50,FinalTermPercent = 50}
            //}.ForEach(s=>context.GradingSystems.Add(s));
            //new List<UserActivityType>
            //{
            //    new UserActivityType{UserActivityTypeId = 1,ActivityType = "Admssion"},
            //    new UserActivityType{UserActivityTypeId = 2,ActivityType = "Advising"},
            //    new UserActivityType{UserActivityTypeId = 3,ActivityType = "Print Payment Slip"},
            //    new UserActivityType{UserActivityTypeId = 4,ActivityType = "Print Students by Section"},
            //    new UserActivityType{UserActivityTypeId = 5,ActivityType = "Print Students by Department"},
            //    new UserActivityType{UserActivityTypeId = 6, ActivityType = "Print Students by Course"},
            //    new UserActivityType{UserActivityTypeId = 7,ActivityType = "Print Section Grade Sheet"},
            //    new UserActivityType{UserActivityTypeId = 8,ActivityType = "Print Admit Card"},
            //    new UserActivityType{UserActivityTypeId = 9,ActivityType = "Print Tabulation Sheet"},
            //    new UserActivityType{UserActivityTypeId = 10,ActivityType = "Print Student Profile"},
            //    new UserActivityType{UserActivityTypeId = 11,ActivityType = "Print Single Student Grade Sheet"},
            //    new UserActivityType{UserActivityTypeId = 12,ActivityType = "Manual Grade History"},
            //    new UserActivityType{UserActivityTypeId = 13,ActivityType = "Messaging Activity"},
            //    new UserActivityType{UserActivityTypeId = 14,ActivityType = "Print Registrations"},
            //    new UserActivityType{UserActivityTypeId = 15,ActivityType = "Print Section proposal"},
            //    new UserActivityType{UserActivityTypeId = 16,ActivityType = "Print CGPA Overall"},
            //    new UserActivityType{UserActivityTypeId = 17,ActivityType = "Print Transcript"},
            //    new UserActivityType{UserActivityTypeId = 18,ActivityType = "System support"},
            //}.ForEach(s=>context.UserActivityTypes.Add(s));

            //new List<MessageFlag>
            //{
            //    new MessageFlag{FlagName = "Unread", FlagDescription = "Not Yet Read"},
            //    new MessageFlag{FlagName = "Read", FlagDescription = "Viewed"},
            //    new MessageFlag{FlagName = "Favourite", FlagDescription = "Liked"},
            //    new MessageFlag{FlagName = "Spam", FlagDescription = "Without Subject"},
            //    new MessageFlag{FlagName = "Deleted", FlagDescription = "Removed but not cleaned"}
            //}.ForEach(s=>context.MessageFlags.Add(s));

            //new List<SurveyQuestion>
            //{
            //    new SurveyQuestion{QuestionDetials = "Did the teacher provided you with Lecture Plan and course contents?"},
            //    new SurveyQuestion{QuestionDetials = "Did the teacher follow the course syllabus fully?"},
            //    new SurveyQuestion{QuestionDetials = "Did the teacher deliver his lecture in English?"},
            //    new SurveyQuestion{QuestionDetials = "Did the teacher remain in each of the classes for the total duration of the class?"},
            //    new SurveyQuestion{QuestionDetials = "Did the teacher come to the class in time?"},
            //    new SurveyQuestion{QuestionDetials = "Do you think that the teacher had sufficient depth in his subject?"},
            //    new SurveyQuestion{QuestionDetials = "Do you think that the teacher came to the classes after getting prepared for the lecture?"},
            //    new SurveyQuestion{QuestionDetials = "Did the teacher give a master copy of notes/ hand outs to support his lecture?"},
            //    new SurveyQuestion{QuestionDetials = "Did the teacher give adequate homework?"},
            //    new SurveyQuestion{QuestionDetials = "Did the teacher take four/three class tests during the semester (for 3 credit-hr/  2 credit-hr course)?"},
            //    new SurveyQuestion{QuestionDetials = "Did the teacher give adequate effort to make lecture topics understandable to you?"},
            //    new SurveyQuestion{QuestionDetials = "Did the teacher give you sheets of some of the worked out (solved) problems?"},
            //    new SurveyQuestion{QuestionDetials = "Did the teacher give chance to the students to ask questions on the topic of the lecture at the end of the lectures?"},
            //    new SurveyQuestion{QuestionDetials = "Did the teacher display consultation hours on his door?"},
            //    new SurveyQuestion{QuestionDetials = "Was the teacher available during consultation hour?"},
            //    new SurveyQuestion{QuestionDetials = "Do you think that the teacher was friendly in the class and during consultation hours? "},
            //    new SurveyQuestion{QuestionDetials = "The content specified in the course outline was actually covered in class"}
            //}.ForEach(s=>context.SurveyQuestions.Add(s));

            //new List<SurveyAnswer>
            //{
            //    new SurveyAnswer{AnswerName = "Yes", AnswerPoint = 5},
            //    new SurveyAnswer{AnswerName = "Average", AnswerPoint = 2},
            //    new SurveyAnswer{AnswerName = "No", AnswerPoint = 0}
            //}.ForEach(s=>context.SurveyAnswers.Add(s));
        }

    }
}
    
