﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using UMS_MVC.DataModel.Models.AccountExt;
using UMS_MVC.DataModel.Models.CreditTransfer;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.DataModel.Models.Login_History;
using UMS_MVC.DataModel.Models.MasterAdminProtocol;
using UMS_MVC.DataModel.Models.Notice_Matter;
using UMS_MVC.DataModel.Models.PreAdmission;
using UMS_MVC.DataModel.Models.Probation;
using UMS_MVC.DataModel.Models.Project_Thesis;
using UMS_MVC.DataModel.Models.Reported_Case;
using UMS_MVC.DataModel.Models.Student_Tags;
using UMS_MVC.DataModel.Models.TeacherEvaluation;
using UMS_MVC.DataModel.Models.UniversalTypes;


namespace UMS_MVC.DataModel.Connection
{
    public class UmsDbContext : DbContext
    {
        public UmsDbContext() : base("UmsDbContext")
        {
            //Thread.Sleep(5000);// for removing migrations
            Database.SetInitializer<UmsDbContext>(null); 

        }

        // new added
        public DbSet<UnfairStudents> UnfairStudents { set; get; }
        public DbSet<Department> Departments { set; get; }
        public DbSet<School> Schools { set; get; }
        public DbSet<SemesterInfo> SemesterInfos { set; get; }
        public DbSet<StudentAcademicInfo> StudentAcademicInfos { set; get; }
        public DbSet<StudentIdentification> StudentIdentifications { set; get; }
        public DbSet<DocumentAdding> DocumentsaddAddings { set; get; }
        public DbSet<StudentInfo> StudentInfos { set; get; }
        public DbSet<Gender> Genders { set; get; }
        public DbSet<MaritalStatus> MaritalStatuses { set; get; }
        public DbSet<BloodGroups> BloodGroupses { set; get; }
        public DbSet<Semester> Semesters { set; get; }
        //public DbSet<PicOfStudent> PicOfStudents { set; get; }
        public DbSet<SerializedSemester> SerializedSemesters { set; get; }
        public DbSet<CourseForDepartment> CourseForDepartments { set; get; }
        public DbSet<CourseForStudentsAcademic> CourseForStudentsAcademics { set; get; }
        public DbSet<CourseStatus> CourseStatuses { set; get; }
        public DbSet<Account> Accounts { set; get; }
        public DbSet<AccountsRole> AccountsRoles { set; get; }
        public DbSet<Year> Years { set; get; }
        public DbSet<Section> Sections { set; get; }
        public DbSet<Teacher> Teachers { set; get; }
        public DbSet<TeacherDesignations> TeacherDesignations { set; get; }
        public DbSet<LoginHistory> LoginHistories { set; get; }

        public DbSet<PaymentDue> PaymentDues { get; set; }
        public DbSet<PaymentRegistration> PaymentRegistrations { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<Rebate> Rebates { set; get; }
        public DbSet<RebateCategory> RebateCategories { set; get; }
        public DbSet<PaymentType> PaymentTypes { set; get; }
        public DbSet<GradingHistory> GradingHistories { set; get; }
        //..... Added new Models 12-2-2015
        public DbSet<ControlFeatures> ControlFeatureses { set; get; }
        public DbSet<DepartmentChange> DepartmentChanges { set; get; }
        public DbSet<CourseForStudentHistory> CourseForStudentHistories { set; get; }
        public DbSet<NoteUpload> NoteUploads { set; get; }
        public DbSet<FileUpload> FileUploads { set; get; }
        public DbSet<GradingSystem> GradingSystems { set; get; }
        public DbSet<UserActivityType> UserActivityTypes { set; get; }
        public DbSet<UserActivity> UserActivities { set; get; }
        public DbSet<LoginHistoryStudent> LoginHistoryStudents { set; get; }
        public DbSet<MessageFlag> MessageFlags { set; get; }
        public DbSet<InternalMessage> InternalMessages { set; get; }
        public DbSet<CreditTransferStudent> CreditTransferStudents { set; get; }
        //Patch Version
        public DbSet<PatchVersion> PatchVersions { set; get; }
        public DbSet<PatchNotes> PatchNoteses { set; get; }
        //Reported Student
        public DbSet<ReportedStudent> ReportedStudents { set; get; }

        //NOtices
        public DbSet<NoticeBoardCategory> NoticeBoardCategories { set; get; }
        public DbSet<NoticeBoardDetail> NoticeBoardDetails { set; get; }
        public DbSet<NoticeBoardAttatchedFiles> NoticeBoardAttatchedFileses { set; get; }
        //Work Access
        public DbSet<PowerWork> PowerWorks { set; get; }
        public DbSet<PowerWorkAccess> PowerWorkAccesses { set; get; }
        //new project management Added 24-8-2015
        public DbSet<Project> Projects { set; get; }
        public DbSet<ProjectCurrentStatus> ProjectCurrentStatuses { set; get; }
        public DbSet<ProjectMember> ProjectMembers { set; get; }
        public DbSet<ProjectType> ProjectTypes { set; get; }
        public DbSet<ProjectThesisFiles> ProjectThesisFileses { set; get; }
        //new Account Extended Added 5-11-2015
        public DbSet<AccountExtEducation> AccountExtEducations { set; get; }
        public DbSet<AccountExtProfessionalActivity> AccountExtProfessionalActivities { set; get; }
        public DbSet<AccountExtProject> AccountExtProjects { set; get; }
        public DbSet<AccountExtPublication> AccountExtPublications { set; get; }
        public DbSet<AccountMetaInformation> AccountMetaInformations { set; get; }
        public DbSet<AccountMetaProfessional> AccountMetaProfessionals { set; get; }

        //new Teacher's Evaluation Added 8-11-2015
        public DbSet<SurveyQuestion> SurveyQuestions { set; get; }
        public DbSet<SurveyAnswer> SurveyAnswers { set; get; }
        public DbSet<SurveryAssesment> SurveryAssesments { set; get; }
        public DbSet<EvaluationConfiguration> EvaluationConfigurations { set; get; }
        public DbSet<EvaluationResult> EvaluationResults { set; get; }
        public DbSet<UmsMasterSetting> UmsMasterSettings { set; get; }

        //added 19-4-2016
        public DbSet<ProbationCategory> ProbationCategories { get; set; }
        public DbSet<ProbationFallenStudent> ProbationFallenStudents { get; set; }
        public DbSet<CreditTransferAcceptedCourse> CreditTransferAcceptedCourses { get; set; }
        public DbSet<ReportedPunishment> ReportedPunishments { get; set; }
        public DbSet<UniversityName> UniversityNames { get; set; }
        public DbSet<StudentGroup> StudentGroups { get; set; }
        public DbSet<PreUmsCourseForStudentAcademic> PreUmsCourseForStudentAcademics { get; set; }
        public DbSet<StudentPerformanceAssist> StudentPerformanceAssist { set; get; }
        public DbSet<StudentGraduation> StudentGraduations { set; get; }
        //Online admission 
        public DbSet<PreAdmissionStudentInfo> PreAdmissionStudentInfos { get; set; }
        public DbSet<PreAdmissionAcademic> PreAdmissionAcademics { get; set; }
        //public DbSet<PreAdmissionFiles> PreAdmissionFileses { set; get; }
        public DbSet<StudentAttatchedFileCategory> StudentAttatchedFileCategories { set; get; }
        public DbSet<StudentAttatchedFile> StudentAttatchedFiles { set; get; }
        public DbSet<SpecifiedConfigurationOfDepartment> SpecifiedConfigurationOfDepartments { set; get; } // Added 27-7-2016

        public DbSet<StudentAccountExtProfessionalActivitys> StudentAccountExtProfessionalActivitys { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<InternalMessage>()
                .HasRequired(s => s.SenderAccount)
                .WithMany()
                .HasForeignKey(d => d.SenderAccountId);
            modelBuilder.Entity<InternalMessage>()
              .HasRequired(s => s.ReceiverAccount)
              .WithMany()
              .HasForeignKey(d => d.ReceiverAccountId);
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        public System.Data.Entity.DbSet<UMS_MVC.DataModel.Models.Essential_Types.SpecialWaiverStudents> SpecialWaiverStudents { get; set; }
    }

}