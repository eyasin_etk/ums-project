﻿servApp.service('rebateCategoryservice', ['$resource', '$q', 'baseUrl',
    function ($resource, $q, baseUrl) {
        var workUrl = baseUrl + 'RebateCategoryService';
        var getAllRebateCategoryById = function () {
            var defer = $q.defer();
            var resource = $resource(workUrl);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }



        return {
            getAllRebateCategoryById: getAllRebateCategoryById
        };

    }]);