﻿
servApp.service('sectionservice', ['$resource', '$q', 'baseUrl',
    function ($resource, $q, baseUrl) {

        var getSectionsByFaculty = function (tId) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'SectionService/FindSectionByTeacherId', {
                id: tId
            });
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }

        var getSectionsByDept = function (tid) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'SectionService?id=' + tid);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }


        var getAllSectionsforScruBySemDept = function (sorObj) {
            var defer = $q.defer();

            var resource = $resource(baseUrl + 'SectionService/SectionForScruBySemesterDep', { semId: sorObj.SemId, courseforDepId: sorObj.CourseforDeptId, teacherId: sorObj.TeacherId, secId: sorObj.SecId });
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        
        return {
            getSectionsByFaculty: getSectionsByFaculty,
            getSectionsByDept: getSectionsByDept,
            getAllSectionsforScruBySemDept: getAllSectionsforScruBySemDept
        };

    }]);