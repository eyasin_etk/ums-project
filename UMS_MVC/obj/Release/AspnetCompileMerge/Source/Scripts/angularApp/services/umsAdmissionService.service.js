﻿servApp.service('umsAdmissionService', ['$resource', '$q', 'baseUrl',
    function ($resource, $q, baseUrl) {

        //var getAllCourses = function (depId) {
        //    var defer = $q.defer();
        //    var resource = $resource(baseUrl + 'CourseForDepartmentService?deptId=' + depId);
        //    resource.get(function (response) {
        //        defer.resolve(response);
        //    }, function (error) {
        //        return defer.reject(error);
        //    });
        //    return defer.promise;
        //}
   
        var generateIdForUms= function(guidId) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'PostUmsAdmissionService?guidId=' + guidId);
                resource.get(function (response) {
                    defer.resolve(response);
                }, function (error) {
                    return defer.reject(error);
                });
                return defer.promise;
        }
        var specificPostAdmStudent= function(id) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'PostAdmissionInfoService?id=' + id);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        var getspecificStudentAcaWithGuid = function (guidId) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'PostAdmissionStudentAcademic?id=' + guidId + '&acaId=' + 0+"&type="+true);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        var getDataToEdit= function(acaId) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'PostAdmissionStudentAcademic?id=' + null + '&acaId=' + acaId + "&type=" + false);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        var insertOnlineStudentToUms = function (specificPostStudent) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'PostOnlineAdmInfoService');
            resource.save(specificPostStudent, function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        return {
            generateIdForUms: generateIdForUms,
            specificPostAdmStudent: specificPostAdmStudent,
            getspecificStudentAcaWithGuid: getspecificStudentAcaWithGuid,
            getDataToEdit: getDataToEdit,
            insertOnlineStudentToUms: insertOnlineStudentToUms
        };

    }]);