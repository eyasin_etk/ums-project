﻿servApp.service('studentIdentityService', ['$resource', '$q', 'baseUrl',
    function ($resource, $q, baseUrl) {

        var specificPostAdmIdenStudent = function (id) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'FinalOnlineAdmService?id=' + id + "&type=" + 1 + "&studentId=" + null);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        var generateNonOnlineIdForUms = function (studentIdenObj) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'StudentIdentityService');
            resource.save(studentIdenObj, function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        var getSpecificStudent = function (id) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'StudentIdentityService?id=' + id);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        var updateStudentIdenInfo = function (studentIdenObj) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'StudentIdentityService');
            resource.save(studentIdenObj, function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        var deleteStudentsIdenInfo = function (id) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'StudentIdentityService?id=' + id);
            resource.delete(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }

        return {
            specificPostAdmIdenStudent: specificPostAdmIdenStudent,
            generateNonOnlineIdForUms: generateNonOnlineIdForUms,
            getSpecificStudent: getSpecificStudent,
            updateStudentIdenInfo: updateStudentIdenInfo,
            deleteStudentsIdenInfo: deleteStudentsIdenInfo

        };

    }]);