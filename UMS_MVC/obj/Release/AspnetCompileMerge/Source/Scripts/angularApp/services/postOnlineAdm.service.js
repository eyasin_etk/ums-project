﻿
servApp.service('postOnlineAdmService', ['$resource', '$q', 'baseUrl',
    function ($resource, $q, baseUrl) {

        var getAllGenders = function () {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'CommonService?id=' + 1);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        var getAllBloods = function () {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'CommonService?id=' + 2);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        var getAllMaritals = function () {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'CommonService?id=' + 3);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }

        var getAllStudent = function (pageObj,filterObj) {
            var defer = $q.defer();
            var url2 = baseUrl + 'PostAdmissionInfoService';
            var resource = $resource(url2 + "?name=" + filterObj.name + "&tokenId=" + filterObj.tokenId + "&departmentId=" + filterObj.departmentId + "&admitted="+filterObj.admitted + "&phone=" + filterObj.phone + "&gender=" + filterObj.gender,
            {
                sorting:  pageObj 
            });
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }

        var getspecificStudentWithGuid = function (guidId) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'PostAdmissionInfoService?id=' + guidId);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        var updateInfo = function (student) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'PostAdmissionInfoService');
            resource.save(student, function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        var getspecificStudentAcaWithGuid = function (guidId) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'PostAdmissionStudentAcademic?id=' + guidId);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
          var getDataToEdit= function(acaId) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'PostAdmissionStudentAcademic?id=' + null + '&acaId=' + acaId + "&type=" + false);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        var updateStudentAcaInfo = function (studentAca) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'PostAdmissionStudentAcademic');
            resource.save(studentAca, function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        var deleteStudentInformation = function (studentGuidId) {
            console.log(studentGuidId);
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'PostAdmissionInfoService/Delete?id=' + studentGuidId);
            resource.delete(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        var deleteStudentsAcaInfo = function (acaInfoId) {
            console.log(acaInfoId);
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'PostAdmissionStudentAcademic/Delete?id=' + acaInfoId);
            resource.delete(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        var getPreFetchToken = function (val) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'PostAdmNameLoader?nameParam=' + val);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }

      var getImageUrl=  function(id, type,stamp) {
          var uploadUrl =baseUrl+ "ProfileImageTransferService";
          return uploadUrl + "?imageName=" + id + "&type=" + type + "&stamp=" + stamp;
        }

        return {
            getAllBloods: getAllBloods,
            getAllMaritals: getAllMaritals,
            getAllGenders: getAllGenders,
            getAllStudent: getAllStudent,
            getspecificStudentWithGuid: getspecificStudentWithGuid,
            updateInfo: updateInfo,
            getspecificStudentAcaWithGuid: getspecificStudentAcaWithGuid,
            getDataToEdit:getDataToEdit,
            updateStudentAcaInfo: updateStudentAcaInfo,
            deleteStudentInformation: deleteStudentInformation,
            deleteStudentsAcaInfo: deleteStudentsAcaInfo,
            getPreFetchToken: getPreFetchToken,
            getImageUrl: getImageUrl

        };

    }]);