﻿


servApp.service('universityService', ['$resource', '$q', 'baseUrl',
    function ($resource, $q, baseUrl) {
        var getUniversityNames = function () {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'UniversityNameService');
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        var getUniversityById = function (uniId) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'UniversityNameService?id=' + uniId);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        var saveUniversity = function (uniObj) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'UniversityNameService');
            resource.save(uniObj, function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        var removeUniversity = function (uniId) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'UniversityNameService?id=' + uniId);
            resource.delete(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        return {
            getUniversityNames: getUniversityNames,
            getUniversityById: getUniversityById,
            saveUniversity: saveUniversity,
            removeUniversity: removeUniversity
        };

    }]);