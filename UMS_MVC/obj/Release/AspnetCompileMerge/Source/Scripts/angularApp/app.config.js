﻿
var servApp = angular.module('servApp', []);

//servApp.value('baseUrl', 'http://localhost:55091/api/');

servApp.value('baseUrl', 'http://123.136.27.58/admissionapi/api/');
//angular.module('umspInitial').value('baseUrl', 'http://192.168.1.100/umsapi/api/');
//angular.module('umspInitial').value('baseUrl', 'http://localhost/umsapi/api/');
//angular.module('umspInitial').value('baseUrl', 'http://123.136.27.60/umsapi/api/');


var dirApp = angular.module('directivesApp', []);
var app = angular.module('umspInitial', [
    'ngRoute',
    'ngResource',
    'angular.filter',
    'googlechart',
    'ngMaterial',
    'ngMessages',
    'ui.bootstrap',
    'servApp',
    'directivesApp',
    'LocalStorageModule'
]);
servApp.directive('ncgRequestVerificationToken', [
        '$http', function ($http) {
            return function (scope, element, attrs) {
                $http.defaults.headers.common['RequestVerificationToken'] = attrs.ncgRequestVerificationToken || "no request verification token";
            }
        }
]);
app.factory('userService', function () {
    return {
        AccountId: 0,
        Name: '',
        AccountRole:0
    };
});
app.factory('studentInitializer', function () {
    var studentInitObj = {
        StudentIdentificationId: 0,
        StudentId: '',
        StudentGuidId: undefined,
        studentPicture: ''
    };
    return studentInitObj;
});

app.config([
    '$routeProvider', function ($routeProvider) {

        $routeProvider.
             when('/master',
            {
                templateUrl: '../scripts/angularapp/views/masterIndex.tpl.html',
                controller: 'masterAdmController'
            }).
             when('/university',
            {
                templateUrl: '../scripts/angularapp/views/universityIndex.tpl.html',
                controller: 'universityController'
            }).
            when('/notice',
            {
                templateUrl: '../scripts/angularapp/views/noticeIndex.tpl.html',
                controller: 'noticeDetailController'
            }).
            when('/evalIndex', {
                templateUrl: '../scripts/angularapp/views/evaluationIndex.tpl.html',
                controller: 'evalController'
            }).
            when('/evalResult',
            {
                templateUrl: '../scripts/angularapp/views/evalResult.tpl.html',
                controller: 'evaluationController'
            }).
            when('/scrutinyAdm',
            {
                templateUrl: '../scripts/angularapp/views/scrutinyIndex.tpl.html',
                controller: 'scrutinyController'
            }).
             when('/postOnlineAdmIndex',
            {
                templateUrl: '../scripts/angularapp/views/postOnlineAdmissionViews/postOnlineAdmIndex.tpl.html',
                controller: 'postOnlineAdmIndexController'
            }).
            when('/postOnlineAdmEntry/:id',
            {
                templateUrl: '../scripts/angularapp/views/postOnlineAdmissionViews/postOnlineAdmEntry.tpl.html',
                controller: 'postOnlineAdmEntryController'
            }).
             when('/studentIndex',
            {
                templateUrl: '../Scripts/angularApp/views/umsStudent/umsStudentIndex.tpl.html',
                controller: 'umsStudentIndexController'
            }).
            when('/umsAdmission/:id',
            {
                templateUrl: '../scripts/angularapp/views/umsAdmission.tpl.html',
                controller: 'postOnlineAdmissionController'
            }).
            when('/updatedumsAdmission',
            {
                templateUrl: '../scripts/angularapp/views/updatedUmsAdmission.tpl.html'

            }).
             when('/studentIdentity', {
                 templateUrl: '../scripts/angularapp/views/admissionViews/studentIdentity.tpl.html',
                 controller: 'studentIdentityController'
             }).
            when('/personalInfo', {
                templateUrl: '../scripts/angularapp/views/admissionViews/personalInfo.tpl.html',
                controller: 'personalInfoController'
            }).
            when('/academicInfo', {
                templateUrl: '../scripts/angularapp/views/admissionViews/academicInfo.tpl.html',
                controller: 'academicInfoController'
            }).
            when('/submittedDocument', {
                templateUrl: '../scripts/angularapp/views/admissionViews/submittedDocument.tpl.html',
                controller: 'submittedDocumentController'
            }).
            when('/uploadImage', {
                templateUrl: '../scripts/angularapp/views/admissionViews/uploadImage.tpl.html',
                controller: 'uploadImageController'
            }).
            when('/jobExperience', {
                templateUrl: '../scripts/angularapp/views/admissionViews/jobExperience.tpl.html',
                controller: 'jobExperienceController'
            }).
            when('/studentRebate', {
                templateUrl: '../scripts/angularapp/views/admissionViews/studentRebate.tpl.html',
                controller: 'studentRebateController'
            }).
        when('/semester',
        {
            templateUrl: '../scripts/angularapp/views/semester.tpl.html',
            controller: 'semesterController'
        }).
        when('/noAccess',
        {
            templateUrl: '../scripts/angularapp/views/noAccess.tpl.html'
        }).
             when('/courseMaster',
        {
            templateUrl: '../scripts/angularapp/views/courseManage/courseMaster.tpl.html',
            controller: 'courseMasterController'
        }).
            otherwise({ redirectTo: '/postOnlineAdmIndex' });


    }
]);
app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptorService');
});
