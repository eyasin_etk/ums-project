﻿angular.module('umspInitial').controller('universityController', [
    '$scope','universityService', function($scope, universityService) {
        $scope.pageName = 'University';


        function  init() {
            $scope.universities = [];
            $scope.university = {};
        }
       


        $scope.getUnis = function () {
            $scope.universities = [];
            universityService.getUniversityNames().then(function(response) {
                if (response.IsSuccess) {
                    $scope.universities = response.Data;
                } else {
                    alert(response.Message);
                    if (response.Exception != null)
                        console.log(response.Exception);
                }
            });
        }

        $scope.getSingleUni = function (id) {
            universityService.getUniversityById(id).then(function (response) {
                if (response.IsSuccess) {
                    $scope.university = response.Data;
                } else {
                    alert(response.Message);
                    if (response.Exception != null)
                        console.log(response.Exception);
                }
            });
        }
        $scope.saveUniversity = function () {
            
            universityService.saveUniversity($scope.university).then(function (response) {
                if (response.IsSuccess) {
                    $scope.getUnis();
                } else {
                    alert(response.Message);
                    if (response.Exception != null)
                        console.log(response.Exception);
                }
            });
        }
        $scope.removeUni = function (uniId) {

            universityService.removeUniversity(uniId).then(function (response) {
                if (response.IsSuccess) {
                    $scope.getUnis();
                } else {
                    alert(response.Message);
                    if (response.Exception != null)
                        console.log(response.Exception);
                }
            });
        }
        $scope.cancel= function() {
            $scope.university = {};
        }
        init();
        $scope.getUnis();
    }]);