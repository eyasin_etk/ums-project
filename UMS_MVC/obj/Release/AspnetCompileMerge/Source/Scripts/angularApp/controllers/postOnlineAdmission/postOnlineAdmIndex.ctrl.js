﻿
angular.module('umspInitial').controller('postOnlineAdmIndexController', [
    '$scope', '$location', 'userService', 'semesterservice', 'departmentservice', 'postOnlineAdmService', 'studentInitializer', '$mdDialog', '$mdMedia',
    function ($scope, $location, userService, semesterservice, departmentservice, postOnlineAdmService, studentInitializer, $mdDialog, $mdMedia) {

        var errorFunction = function (error) {
            console.log(error);
        }
        function init() {
            $scope.userObj = userService;
            console.log($scope.userObj);
            var accountRole = $scope.userObj.AccountRole;
            console.log(accountRole);
            if (accountRole !== "1" && accountRole !== "3") {
                $location.path('/noAccess');
            } 

            $scope.pageObj = {
                "PageNo": 0,
                "PageSize": 10
            };

            $scope.filterObj = {
                name: '', tokenId: '', sortDate: '', phone: "", gender: 0, departmentId: 0, admitted: false

            }
        }
        $scope.pageSizeSet = function () {
            var pageMove = $scope.pageObj.PageNo;
            var pagesize = $scope.pageObj.PageSize;
            $scope.pageObj = { PageNo: 0, PageSize: pagesize, TotalPages: 0, TotalCount: 0 };
            $scope.getAllStudent();
        }
        $scope.navPage = function (pageVal) {
            var pageMove = $scope.pageObj.PageNo + pageVal;
            var pagesize = $scope.pageObj.PageSize;
            $scope.pageObj = { PageNo: pageMove, PageSize: pagesize, TotalPages: 0, TotalCount: 0 };

            $scope.getAllStudent();
        }

        $scope.pageName = 'online Adm Index';
        $scope.msg = 'sss';
        $scope.semesters = [];
        $scope.departments = [];

        $scope.getSemesters = function () {
            $scope.semesters = [];
            semesterservice.getAllSemesters().then(function (response) {
                if (response.IsSuccess) {
                    $scope.semesters = response.Data;
                } else {
                    console.log(response);
                }
            }, errorFunction);
        }
       
        $scope.getDepartments = function () {
            departmentservice.getAllDepartments().then(function (response) {
                $scope.departments = [];
                if (response.IsSuccess) {
                   $scope.departments = response.Data;
                    } else {
                  console.log(response);
                }

            }, errorFunction);
        }
        $scope.getAllStudent = function () {
            console.log($scope.filterObj);
            //$scope.filterObj.sortDate = $filter('date')($scope.filterDate);
            if ($scope.filterObj.departmentId === null) {
                $scope.filterObj.departmentId = 0;
            }
            postOnlineAdmService.getAllStudent($scope.pageObj, $scope.filterObj).then(function (response) {
                $scope.student = [];
                if (response.IsSuccess) {
                    $scope.pageObj = null;
                    $scope.pageObj = response.Pagination;
                    $scope.allStudentList = response.Data;

                } else {
                    alert('No Data');
                }

            },errorFunction);
        }

        function resetStudentInitial() {
            studentInitializer.StudentGuidId = undefined;
            studentInitializer.studentPicture = "";
            studentInitializer.StudentId = 0;
            studentInitializer.StudentIdentificationId = 0;
        }
        $scope.setAdmId = function (id, image) {
            resetStudentInitial();
          
            studentInitializer.StudentGuidId = id;
            studentInitializer.studentPicture = image;
            console.log(studentInitializer);
            $location.path('/studentIdentity');
        }
        $scope.filterDo = function () {
            if ($scope.filterObj.name === undefined) {
                $scope.filterObj.name = '';
            }
            if ($scope.filterObj.phone === undefined) {
                $scope.filterObj.phone = '';
            }
            if ($scope.filterObj.departmentId === null) {
                $scope.filterObj.departmentId = 0;
            }

            $scope.getAllStudent();
        }
        $scope.resetFilter = function () {
            init();
            $scope.getAllStudent();
        }
        $scope.getPreFetchNames = function (val) {
            console.log('Fetching Data');
            return postOnlineAdmService.getPreFetchToken(val)
                .then(function (response) {
                    var names = [];
                    angular.forEach(response.Data, function (item) {
                        names.push(item.StudentName);
                    });
                    return names;
                });
        }
        $scope.showAdd = function ($event) {
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;

            $mdDialog.show({
                controller: DialogController,
                //parent: parentEl,
                targetEvent: $event,
                templateUrl: '../Scripts/angularApp/views/postOnlineAdmissionViews/StudentInitalView.tpl.html',
                clickOutsideToClose: true,
                fullscreen: useFullScreen,
                openFrom: {
                    top: -50,
                    width: 30,
                    height: 80
                }
            }).then(function (countryObj) {

                console.log(countryObj);
            });
        };


        $scope.getImage = function (id) {
            var stamp = new Date().getTime();
            return postOnlineAdmService.getImageUrl(id, 2, 0);

        }


        init();
        $scope.getSemesters();
        $scope.getDepartments();
        $scope.getAllStudent();
        $scope.getPreFetchNames();
    }
]);

function DialogController($scope, $mdDialog, postOnlineAdmService) {
    $scope.studentObj = {};
    $scope.cancel = function () {
        $mdDialog.cancel();
        console.log("Method Triggered");
    };

    // $scope.studentObj = postOnlineAdmService.[0];

    $scope.save = function () {
        $mdDialog.hide($scope.country);
    }

    $scope.closeDialog = function () {
        $mdDialog.cancel();
        //$mdDialog.hide($scope.country);
    };
};
