﻿

$(function () {
    $("#courseEdit").on("submit", function (e) {
        e.preventDefault();
        $.ajax({
            url: this.action,
            type: this.method,
            data: $(this).serialize(),
            success: function (data) {
                alert("Update Successful !!");
                $("#courseListbySemester2").html(data);
            }
        });
    });
});

function ConfirmDelete() {
    if (confirm("Do You want to Delete ?")) {
        return true;
    } else {
        return false;
    }
}