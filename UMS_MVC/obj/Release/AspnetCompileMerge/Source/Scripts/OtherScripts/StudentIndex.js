﻿

$(document).ready(function () {
    var url = "..//Student/StudentInfo";
    var url2 = "..//Student/DocumentInfo";
    var url3 = "..//Student/AcademicInfo";
    
    //var url4 = "..//Student/GetCumulativeScoresByStudent";
    //updateCumulativeData();
    $.get(url, function (data) {
        $("#studentInfoPartial").html(data);
    });
    $.get(url2, function (data) {
        $("#DocumentInfoPartial").html(data);
    });
    $.get(url3, function (data) {
        $("#AcademicInfoPartial").html(data);
    });
   
    //$.get(url4, function (data) {
    //    $("#cgpaLbl").html(data);
    //});
});
//function updateCumulativeData() {
//    var url = '..//Student/GetCumulativeScoresByStudent';
//    $.get(url, null, function (data) {
//        $("#creditCompletedLbl").html(data.CreditEarned);
//        $("#cgpaLbl").html(data.Cgpa);
//    });

//}
function LoadCumulativeInfo() {
    $.ajax({
        type: 'GET',
        url: '..//Student/GetCumulativeScoresByStudent',
        success: function (json) {
            $("#creditCompletedLbl").html(json.Data.CreditEarned);
            $("#cgpaLbl").html(json.Data.Cgpa);
        }
    });
}
function LoadCurGradeInfo(id) {
    $.ajax({
        type: 'GET',
        url: '..//Student/GradeCurWise',
        data: { identificationId: id },
        success: function (data) {
            $('#loadCourseDetails').html(data);
        }
    });
}
function LoadSemGradeInfo(id) {
    $.ajax({
        type: 'GET',
        url: '..//Student/GradeSemWise',
        data: { identificationId: id },
        success: function (data) {
            $('#loadCourseDetails').html(data);
        },
    });
}
function LoadComingSoon() {
    $.ajax({
        type: 'GET',
        url: '..//Faculty/ComingSoonPartial',
        data: { },
        success: function (data) {
            $('#loadCourseDetails').html(data);
        },
    });
}
function ChangePassword() {
    $.ajax({
        type: 'GET',
        url: '..//Student/ChangePassword',
        data: {},
        success: function (data) {
            $('#loadCourseDetails').html(data);
        },
    });
}
function LoadNoteDown(id) {
    $.ajax({
        type: 'GET',
        url: '..//Student/NoteDownLoad',
        data: { studentId: id },
        success: function (data) {
            $('#loadCourseDetails').html(data);
        },
    });
}

function loadNoteForStudent(secId) {
    $.ajax({
        type: "GET",
            data: { secId: secId },
            url: '..//Student/GetNotesBySection',
            success: function (response) {
                $('#notesDiv').html(response);
            }
        });
}

(function ($) {
    $(".ripple-effect").click(function (e) {
        var rippler = $(this);
        // create .ink element if it doesn't exist
        if (rippler.find(".ink").length == 0) {
            rippler.append("<span class='ink'></span>");
        }
        var ink = rippler.find(".ink");
        // prevent quick double clicks
        ink.removeClass("animate");
        // set .ink diametr
        if (!ink.height() && !ink.width()) {
            var d = Math.max(rippler.outerWidth(), rippler.outerHeight());
            ink.css({ height: d, width: d });
        }
        // get click coordinates
        var x = e.pageX - rippler.offset().left - ink.width() / 2;
        var y = e.pageY - rippler.offset().top - ink.height() / 2;

        // set .ink position and add class .animate
        ink.css({
            top: y + 'px',
            left: x + 'px'
        }).addClass("animate");
    });
})(jQuery);