﻿
angular.module('umspInitial').controller('studentIdentityController', [
    '$scope', '$location', 'userService', 'departmentservice', 'semesterservice', 'studentIdentityService', 'studentInitializer', 'onlineStudentsUmsInsertService', 'imgUploadService', function ($scope, $location, userService, departmentservice, semesterservice, studentIdentityService, studentInitializer, onlineStudentsUmsInsertService, imgUploadService) {

        $scope.pageName = "Identity";
        $scope.btnOperationName = "Admit";
        $scope.cnfmId = false;
        $scope.selectedProgram = undefined;
        $scope.lockEditing = false;
        var initialObject = studentInitializer;
        
       $scope.emptyStudentObj= function() {
           $scope.studenIdentityObj = {
               StudentIdentificationId: 0,
               StudentGuid: undefined,
               StudentId: '',
               DepartmentId: 0,
               SemesterId: 0,
               CreditTransfer: false,
               DiplomaStudent: false,
               Remark: '',
               StudentPicture: '',
               StudentInfoId: '',
               RegularStudent: false,
               FridayStudent: false,
               EveningStudent: false,
               Batch:''

           };
       }
        $scope.diplayStudentInitials= function() {
            $scope.displayGuidId = initialObject.StudentGuidId;
            $scope.displayIdentity = initialObject.StudentIdentificationId;
            $scope.displayId = initialObject.StudentId;
        }
        $scope.init = function () {

            $scope.userObj = userService;
            var accountRole = $scope.userObj.AccountRole;
           
            if (accountRole !== "1" && accountRole !== "3") {
                $location.path('/noAccess');
            }
           //console.log(initialObject);
           $scope.emptyStudentObj();
           
           if (initialObject.StudentIdentificationId !== 0 && initialObject.StudentGuidId !== undefined) {
               $scope.lockEditing = true;
               $scope.getSingleStudentIdentity(initialObject.StudentIdentificationId);
               $scope.loadStudentImage(initialObject.studentPicture, 1);
           } 
           if (initialObject.StudentIdentificationId === 0 && initialObject.StudentGuidId !== undefined) {
               $scope.lockEditing = false;
               $scope.getSingleStudentIdenByGuid(initialObject.StudentGuidId);
               $scope.loadStudentImage(initialObject.studentPicture, 2);
           }
           if (initialObject.StudentIdentificationId !== 0 && initialObject.StudentGuidId === undefined) {
               $scope.lockEditing = true;
               $scope.getSingleStudentIdentity(initialObject.StudentGuidId);
               $scope.loadStudentImage(initialObject.studentPicture, 1);

           }
           if (initialObject.StudentIdentificationId === 0 && initialObject.StudentGuidId === undefined) {
               $scope.lockEditing = false;
            }
        }
        
        var errorFunction = function (error) {
            console.log(error);
        }
        $scope.clear= function() {
            $scope.studenIdentityObj = {};
        }
        //$scope.getOnlindeAdmData =

        $scope.loadDepartments = function () {
            $scope.departments = {};
            departmentservice.getAllDepartments().then(function (response) {
                if (response.IsSuccess) {
                    $scope.departments = response.Data;
                } else {
                    console.log(response);
                }
            },errorFunction);
       }

     $scope.loadSemesters= function() {
            $scope.semesters = {};
            semesterservice.getAllSemesters().then(function(response) {
                if (response.IsSuccess) {
                    $scope.semesters = response.Data;
                } else {
                    console.log(response);
                }
            },errorFunction);
        }

        function changeBtnName() {
            $scope.btnOperationName = "Update";
            $scope.cnfmId = true;
        }
       $scope.createNUpdate = function () {
           studentIdentityService.generateNonOnlineIdForUms($scope.studenIdentityObj).then(function (response) {
               $scope.serverMsg = response.Message;
               if (response.IsSuccess) {
                   $scope.studenIdentityObj = {};
                   var identityObj = response.Data;
                   $scope.studenIdentityObj = identityObj;
                    studentInitializer.StudentIdentificationId = identityObj.StudentIdentificationId;
                    studentInitializer.StudentId = identityObj.StudentId;
                    $scope.diplayStudentInitials();
                   $scope.lockEditing = true;
                   console.log('test: ' + response.Data);
                    changeBtnName();
               } else {
                   alert(response.Message);
                    console.log(response);
                }
            },errorFunction);
        }
        $scope.getSingleStudentIdentity= function() {
            $scope.studenIdentityObj = {};
            studentIdentityService.getSpecificStudent(initialObject.StudentIdentificationId).then(function (response) {
                console.log(response);
                if (response.IsSuccess) {
                    $scope.studenIdentityObj = response.Data;
                  } else {
                    console.log(response);
                }
            },errorFunction);
        }
        $scope.getSingleStudentIdenByGuid = function (id) {
            $scope.studenIdentityObj = {};
            onlineStudentsUmsInsertService.specificPostAdmIdenStudent(id).then(function (response) {
                $scope.studenIdentityObj = response.Data;

                console.log($scope.studenIdentityObj);
            }, errorFunction);
        }
        
       
        $scope.delete = function () {
            var studentIdentity = $scope.studenIdentityObj.StudentIdentificationId;
            console.log(studentIdentity);
            studentIdentityService.deleteStudentsIdenInfo(studentIdentity).then(function (response) {
                $scope.serverMsg = response.Message;
                if (response.IsSuccess) {
                   } else {
                    console.log(response);
                }
            },errorFunction);
        }

        $scope.unlock = function () {

            studentInitializer.StudentGuidId = undefined;
            studentInitializer.StudentId = '';
            studentInitializer.StudentIdentificationId = 0;
            studentInitializer.studentPicture = '';
            $scope.diplayStudentInitials();
            $scope.init();
            $scope.loadStudentImage('', 1);
            $location.path('/studentIdentity');
        }

        $scope.loadStudentImage= function(imageName,type) {
            var stamp = new Date().getTime();
            $scope.image_source = "";
            console.log("Image...");
            $scope.image_source = imgUploadService.getImageFinalUrl(imageName, type, stamp);
        }
        //$scope.getImageFinal = function () {
        //    $scope.image_source = "";
        //    $scope.image_source = imgUploadService.getImageFinalUrl()
            
        //}
        //$scope.loadEmptyImage = function () {
        //    $scope.image_source =
        //        imgUploadService.getEmptyImageUrl();
        //}
        //$scope.getImage = function () {
        //    var stamp = new Date().getTime();
        //    console.log('Getting image');
        //    $scope.image_source = "";
        //    $scope.image_source = imgUploadService.getImageUrl(initialObject.StudentGuidId, stamp);
        //}
        //$scope.checkStudentImage = function () {
        //    $scope.image_source = "";
        //    imgUploadService.checkImageExiatance(initialObject.StudentGuidId)
        //        .then(function (response) {
        //            if (response.IsSuccess) {
        //                $scope.getImage();
        //            } else {
        //                $scope.loadEmptyImage();
        //            }
        //        });
        //}
        $scope.init();
        $scope.diplayStudentInitials();
        $scope.loadSemesters();
        $scope.loadDepartments();

    }]);


