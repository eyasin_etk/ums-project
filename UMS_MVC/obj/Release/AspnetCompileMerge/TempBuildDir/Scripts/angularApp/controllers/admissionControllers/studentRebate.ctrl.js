﻿angular.module('umspInitial').controller('studentRebateController', ['$scope', '$location', 'studentRebateService', 'rebateCategoryservice', 'studentInitializer', function ($scope, $location, studentRebateService, rebateCategoryservice, studentInitializer) {

    $scope.pageName = "Student Rebate Information";
    var errorFunction = function (error) {
        console.log(error);
    };

    var dropId = 0;
    var initialObject = studentInitializer;

    $scope.init = function () {
        $scope.academics = [];
        console.log(studentInitializer);
        $scope.emptyStudentObj();
        if (initialObject.StudentId !== undefined) {
            $scope.getAllRebates();

        }
    };
    $scope.diplayStudentInitials = function () {
        $scope.displayGuidId = initialObject.StudentGuidId;
        $scope.displayIdentity = initialObject.StudentIdentificationId;
        $scope.displayId = initialObject.StudentId;
    };

    $scope.emptyStudentObj = function () {
        $scope.clearForm();
    }

    $scope.clearForm = function () {
        $scope.btnOperationName = "Add";
        $scope.studentRebateObj = {
            RebateId: 0,
            StudentIdentificationId: initialObject.StudentIdentificationId,
            RebateCategoryId: 0,
            SemesterId:0
        };
    }

    function changeBtnName() {
        $scope.btnOperationName = "Update";
        $scope.cnfmId = true;
    }

    $scope.loadRebateCategories = function () {
        $scope.rebateCategories = [];
        rebateCategoryservice.getAllRebateCategoryById().then(function (response) {
            if (response.IsSuccess) {
                $scope.rebateCategories = response.Data;
                console.log(response.Data);
            } else {
                console.log(response);
            }
        }, errorFunction);
    }

    $scope.getAllRebates = function () {
        $scope.rebates = [];
        studentRebateService.getAllById(initialObject.StudentIdentificationId)
            .then(function (response) {
                if (response.IsSuccess) {
                    $scope.rebates = response.Data;
                    console.log(response.Data);
                } else {

                    console.log(response);
                }
            }, errorFunction);
    }

    $scope.save = function () {
        console.log($scope.studentRebateObj);
  
        studentRebateService.saveRebateInfo($scope.studentRebateObj)
            .then(function (response) {
                $scope.serverMsg = response.Message;
                if (response.IsSuccess) {
                    console.log(response);
                    $scope.getAllRebates();
                    $scope.clearForm();

                } else {
                    console.log(response);
                }
            }, errorFunction);

    }

    $scope.getDetailById = function (id) {
        console.log(id);
        studentRebateService.getSingleByInfoId(initialObject.StudentIdentificationId,id)
            .then(function (response) {
                $scope.serverMsg = response.Message;
                if (response.IsSuccess) {

                    $scope.studentRebateObj = response.Data;
                    changeBtnName();
                } else {
                    console.log(response);
                }
            }, errorFunction);

    }

    $scope.delete = function (id) {

        if (confirm("Are you sure?")) {
            console.log(id);
            studentRebateService.deleteStudentsRebateInfo(id).then(function (response) {
                $scope.serverMsg = response.Message;
                if (response.IsSuccess) {
                    $scope.studentRebateObj = {};
                    $scope.getAllRebates();
                } else {
                    console.log(response);
                }
            }, errorFunction);

        }

    }

    $scope.unlock = function () {
        studentInitializer.StudentGuidId = undefined;
        studentInitializer.StudentId = '';
        studentInitializer.StudentIdentificationId = 0;
        $scope.diplayStudentInitials();
        console.log(studentInitializer);
        $location.path('/studentIdentity');
    }

    $scope.init();
    $scope.loadRebateCategories();
    $scope.diplayStudentInitials();
}]);