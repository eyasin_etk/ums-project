﻿

angular.module('umspInitial').controller('evaluationController', [
    '$scope', 'semesterservice', 'facultyservice', 'evaluationResultService', 'sectionservice', function ($scope, semesterservice, facultyservice, evaluationResultService, sectionservice) {

        var errorFunction = function (error) {
            console.log(error);
        }
        $scope.semesters = [];
        $scope.faculties = [];
        $scope.chartData = [];
        $scope.sections = [];
        $scope.getFaculties = function () {
            facultyservice.getAllFaculty().then(function (response) {
                if (response.IsSuccess) {
                    $scope.faculties = response.Data;
                } else {
                    console.log(response.Message);
                    console.log(response.Exception);
                }
            });
        }

        $scope.getSectionsForFaculty = function () {
            var tid = $scope.selectedfaculty.TeacherId;
            console.log("Faculty: " + tid);
            sectionservice.getSectionsByFaculty(tid).then(function (response) {
                if (response.IsSuccess) {
                    $scope.sections = response.Data;
                    console.log(response.Data);
                }
            }, errorFunction);
        }

        $scope.getSemesters = function () {
            semesterservice.getAllSemesters().then(function (response) {
                if (response.IsSuccess) {
                    $scope.semesters = response.Data;
                } else {
                    console.log(response.Message);
                    console.log(response.Exception);
                }
            }, errorFunction);
        }
      
        $scope.loadSecondFilter = function () {
            var semesterId = $scope.selectedSemester.SemesterId;
            var facultyId = $scope.selectedfaculty.TeacherId;
            console.log("sem :" + semesterId);
            console.log("Faculty :" + facultyId);
            evaluationResultService.getResultofEvaluationByTeacher(facultyId, semesterId).then(function (response) {
                if (response.IsSuccess) {
                    if (response.Data.length > 0) {
                        console.log(response);
                    }
                } else {
                    console.log('Error');
                    console.log(response.Exception);
                }

            });
        }

        $scope.pageName = "Teacher's Evaluation Result";

        function init() {
            $scope.getSemesters();
            $scope.getFaculties();
        }

        init();

        var chart1 = {};
        chart1.type = "BarChart";
        //chart1.cssStyle = "height:800px; width:300px;";
        //chart1.cssStyle = "height:400px; width:550px;";
        chart1.data = [['Question', 'Yes', 'Average', 'No', { role: 'annotation' }]];
        chart1.data.push(['0', 0, 0, 0, '']);
        //chart1.data.push(['Question 2', 20, 65, 15, '']);
        //chart1.data = $scope.chartData;
        chart1.options = {

            "title": "Teacher's Performance",
            "subtitle": 'Points per question',
            "isStacked": "percent",
            "fill": 40,
            //"bars": 'horizontal',
            "legend": { position: 'top', maxLines: 3 },
            "displayExactValues": true,
            "colors": ['#1b9e77', '#4181ec', '#c0382d'],
            "hAxis": {
                minValue: 0,
                ticks: [0, .3, .6, .9, 1]
            }
            //"hAxis": {
            //    viewWindowMode: 'explicit',
            //    "gridlines": { "count": 10 },
            //    viewWindow: {
            //        max: 100,
            //        min: 0
            //    },
            //    format: '#\'%\''
            //},
        };

        chart1.formatters = {
            number: [{
                columnNum: 1,
                pattern: "#,###"
            }]
        };
        $scope.loadFirstFilter = function () {
            var secId = $scope.selectedSection.SectionId;
            console.log("SecId :" + secId);

            evaluationResultService.getResultofEvaluationBySection(secId).then(function (response) {
                if (response.IsSuccess) {
                    if (response.Data.length > 0) {
                        console.log(response);
                        angular.forEach(response.Data, function (evaResult) {
                            chart1.data.push(['Question ' + evaResult.QuestionId,
                                 evaResult.TotalPointYes,
                                evaResult.TotalPointAverage,
                                evaResult.TotalPointNo, '']);
                        });

                    }
                } else {
                    console.log('Error');
                    console.log(response.Exception);
                }

            });
        }
        $scope.chart = chart1;
    }]);