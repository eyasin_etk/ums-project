﻿

angular.module('umspInitial').controller('postOnlineAdmEntryController', [
    '$scope', '$routeParams', 'semesterservice', 'departmentservice', 'postOnlineAdmService', 'umsAdmissionService',
    function ($scope, $routeParams, semesterservice, departmentservice, postOnlineAdmService, umsAdmissionService) {
        function init() {
            var paramId = $routeParams.id;
            console.log(paramId);
            if (paramId !== '0') {
                $scope.specifiedUserId = paramId;
                $scope.getSpecificStudent(paramId);
                $scope.getSpecificStudentAcaInfo(paramId);
            } else {
                $scope.student = {};
                $scope.studentAca = {};
            }
        }
        var errorFunction = function (error) {
            console.log(error);
        }
        $scope.pageName = 'Online Admission Entry';
        $scope.msg = 'sss';
        $scope.semesters = {};
        $scope.departments = {};
        $scope.blood = {};
        $scope.gender = {};
        $scope.marital = {};
        //$scope.sectionSortObj = { SemId: 0, DeptId: 0, CourseId: 0, TeacherId: 0, SecId: 0 };

        //load all semesters to the dropdown
        $scope.getSemesters = function () {
            $scope.semesters = {};
            semesterservice.getAllSemesters().then(function (response) {
                $scope.semesters = response.Data;
            });
        }
        //load all departments to the dropdown
        $scope.getDepartments = function () {
            departmentservice.getAllDepartments().then(function (response) {
                $scope.departments = {};
                if (response.IsSuccess) {
                    $scope.departments = response.Data;

                } else {
                    alert('No Data');
                }

            });
        }
        //load all Blood to the dropdown
        $scope.getBloods = function () {
            postOnlineAdmService.getAllBloods().then(function (response) {
                $scope.blood = {};
                if (response.IsSuccess) {
                    $scope.blood = response.Data;

                } else {
                    alert('No Data');
                }
            });
        }
        //load all gender to the dropdown
        $scope.getAllGender = function () {
            postOnlineAdmService.getAllGenders().then(function (response) {
                $scope.gender = {};
                if (response.IsSuccess) {
                    $scope.gender = response.Data;
                    console.log($scope.gender);
                } else {
                    alert('No Data');
                }
            });
        }
        //load all marital to the drop down
        $scope.getAllMarital = function () {
            postOnlineAdmService.getAllMaritals().then(function (response) {
                $scope.marital = {};
                if (response.IsSuccess) {
                    $scope.marital = response.Data;
                } else {
                    alert('No Data');
                }
            });
        }
        $scope.getSpecificStudent = function (paramId) {
            console.log(paramId);
            postOnlineAdmService.getspecificStudentWithGuid(paramId).then(function (response) {
                $scope.student = {};
                if (response.IsSuccess) {
                   $scope.student = response.Data;
                } else {
                    alert('No Data');
                }

            });
        }
        $scope.updateStudentInfo = function () {
            console.log($scope.student);
            postOnlineAdmService.updateInfo($scope.student).then(function (response) {
                if (response.IsSuccess) {
                    $scope.student = response.Data;
                } else {
                    alert('No Data');
                    console.log(response);
                }

            });
        }
        $scope.getSpecificStudentAcaInfo = function (paramId) {
            console.log(paramId);
            umsAdmissionService.getspecificStudentAcaWithGuid(paramId).then(function (response) {
                if (response.IsSuccess) {
                    console.log($scope.studentAca);
                    console.log(response.Data);
                    $scope.studentAca = response.Data;
                   } else {
                    alert('No Data');
                }

            });
        }
        $scope.getSingleForEdit = function ( acaId) {
            console.log( acaId);
            console.log($scope.studentAcaObj);
            postOnlineAdmService.getDataToEdit(acaId).then(function (response) {
                if (response.IsSuccess) {
                    $scope.studentAcaObj = response.Data;
                  } else {
                    alert('No Data');
                }

            },errorFunction);
        }
        $scope.updateStudentAcademicInfo = function () {
            var academicObj = $scope.studentAcaObj;
            academicObj.PreAdmissionStudentInfoId = $routeParams.id;
            postOnlineAdmService.updateStudentAcaInfo(academicObj).then(function (response) {
                console.log(academicObj);
                if (response.IsSuccess) {
                    console.log(response.Data);
                    $scope.getSpecificStudentAcaInfo();
                    alert("Success");
                } else {
                    alert('No Data');
                }

            });
        }
        $scope.deleteStudentInfo = function () {
            console.log($scope.student.PreAdmissionStudentInfoId);
            postOnlineAdmService.deleteStudentInformation($scope.student.PreAdmissionStudentInfoId).then(function (response) {
                if (response.IsSuccess) {
                    $scope.student = {};
                    $scope.student = response.Data;
                    alert("Success");
                } else {
                    alert('No Data');
                    console.log(response);
                }

            });
        }
        $scope.deleteStudentsAca = function (preAdmissionStudentacaId) {
            console.log(preAdmissionStudentacaId);
            postOnlineAdmService.deleteStudentsAcaInfo(preAdmissionStudentacaId).then(function (response) {
                if (response.IsSuccess) {
                    $scope.studentAca = {};
                    $scope.studentAca = response.Data;
                    alert("Success");
                } else {
                    alert('No Data');
                }

            });
        }
     

        $scope.getSemesters();
        $scope.getDepartments();
        $scope.getBloods();
        $scope.getAllGender();
        $scope.getAllMarital();
        init();
    }
]);