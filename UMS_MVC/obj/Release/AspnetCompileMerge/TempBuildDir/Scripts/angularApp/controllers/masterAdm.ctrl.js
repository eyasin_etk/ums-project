﻿
angular.module('umspInitial').controller('masterAdmController', [
    '$scope', 'masterAdmService', '$mdDialog', '$mdMedia', function ($scope, masterAdmService, $mdDialog, $mdMedia) {
        $scope.pageName = 'Master Admin';
        $scope.sectionCountBtn = "Show Section Count";
        $scope.indexVisibility = false;
        $scope.selectedType = '';
        function init() {
            $scope.sectionStatus = [];
        }
        $scope.getSectionStatuses = function () {
            init();
            masterAdmService.countSectionsByDept().then(function(response) {
                if (response.IsSuccess) {
                    $scope.sectionStatus = response.Data;
                    
                } else {
                    console.log(response);
                }
                if ($scope.indexVisibility) {
                    $scope.indexVisibility = false;
                    $scope.sectionCountBtn = "Show Section Count";
                } else {
                    $scope.indexVisibility = true;
                    $scope.sectionCountBtn = "Hide Section Count";
                }
            });
        }

        // operation
        $scope.overallChange = function (name, typeId) {
            $scope.selectedType = name;
            console.log(typeId);
        }
        $scope.overallChangeAfter = function (name, typeId) {
            $scope.selectedType = name;
            console.log(typeId);
        }
        $scope.specificChange = function (name, deptId, typeId) {
            $scope.selectedType = name;
            console.log(deptId,typeId);
        }
        $scope.specificChangeAfter = function (name, deptId, typeId) {
            $scope.selectedType = name;
            console.log(deptId,typeId);
        }
        $scope.status = '  ';
        $scope.showConfirm = function (ev) {
            var confirm = $mdDialog.confirm()
                   .title('Would you like to alter all section statuses?')
                   .textContent('This will alter all section in current semester contains teacher submit.')
                   .ariaLabel('Lucky day')
                   .targetEvent(ev)
                   .ok('Confirm')
                   .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                masterAdmService.unlockSections(false).then(function (response) {
                    $scope.status = response.Message;
                    if (response.IsSuccess === false) {
                        console.log(response);
                        
                    }
                });
            }, function () {
                $scope.status = 'Operation Cancelled.';
            });
        };
        
        $scope.showPrerenderedDialog = function (ev) {
            $mdDialog.show({
                controller: DialogController,
                contentElement: '#myDialog',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true
            });
        };
        
        //$scope.getSectionStatuses();

    }]);
function DialogController($scope, $mdDialog) {
    $scope.hide = function () {
        $mdDialog.hide();
    };
    $scope.cancel = function () {
        $mdDialog.cancel();
    };
    $scope.answer = function (answer) {
        $mdDialog.hide(answer);
    };
}
