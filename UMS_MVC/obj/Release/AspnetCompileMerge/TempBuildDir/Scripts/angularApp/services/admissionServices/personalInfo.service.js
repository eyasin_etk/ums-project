﻿
servApp.service('personalInfoService', ['$resource', '$q', 'baseUrl',
    function ($resource, $q, baseUrl) {

        var getDetailById = function (id) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'PersonalInfoService?studentId=' + id);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        var saveInfo = function (studentObj) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'PersonalInfoService');
            resource.save(studentObj, function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        var deleteStudentsPersonalInfo = function (id) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'PersonalInfoService?id=' + id);
            resource.delete(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }

        return {
            getDetailById: getDetailById,
            saveInfo: saveInfo,
            deleteStudentsPersonalInfo: deleteStudentsPersonalInfo
        };

    }]);