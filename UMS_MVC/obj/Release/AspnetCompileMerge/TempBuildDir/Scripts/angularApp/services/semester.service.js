﻿

servApp.service('semesterservice', ['$resource', '$q', 'baseUrl',
    function ($resource, $q, baseUrl) {
        baseUrl += 'SemesterService';


        var getAllSemesters = function () {
          var defer = $q.defer();
            var resource = $resource(baseUrl);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }

        var findSingleById = function (id) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + '?id=' + id);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }

        var save = function (studentObj) {
            var defer = $q.defer();
            var resource = $resource(baseUrl );
            resource.save(studentObj, function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        var deleteSemester= function (id) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + '?id=' + id);
            resource.delete(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        return {
            getAllSemesters: getAllSemesters,
            findSingleById: findSingleById,
            save: save,
            deleteSemester: deleteSemester

        };

    }]);