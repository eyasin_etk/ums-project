﻿

servApp.service('imgUploadService', ['$http', 'baseUrl', '$resource', '$q',
function ($http, baseUrl, $resource, $q) {
    var umsUrl = baseUrl + "UmsImageUploadService";
    var filecheck = baseUrl + "FileCheck";
    var imageUrl = baseUrl + "ProfileImageTransferService";
    var secondaryUrl = baseUrl + "CheckStudentImageService";
    baseUrl = baseUrl + "ImageUploadService";
   
    var getImageUrl = function (studentToken, stamp) {
        var imgurl = "";
        imgurl = baseUrl + "?studentToken=" + studentToken + "&stamp=" + stamp;
        return imgurl;
    }

    //image_source

    var getImageFinalUrl = function (imageName,type, stamp) {
      
        return imageUrl + "?imageName=" + imageName+"&type="+type + "&stamp=" + stamp;
        
    }



    var checkImageExiatance = function (studentToken) {
        var defer = $q.defer();
        var resource = $resource(secondaryUrl + "?studentToken=" + studentToken);
        resource.get(function (response) {
            defer.resolve(response);
        }, function (error) {
            return defer.reject(error);
        });
        return defer.promise;
    }
    var getEmptyImageUrl = function () {
        return secondaryUrl;
    }



    
    var uploadFileToUrl = function (submitBy, caption, file) {
        var uploadUrl = baseUrl;
        var fd = new FormData();
        fd.append('file', file);
        fd.append('sumittedBy', submitBy);
        fd.append('newFileName', caption);
        return $http({
            method: 'Post', url: uploadUrl, data: fd, transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        }).
        then(function (response) {
            return response;
        });
    }




    var getUmsImageUrl = function (studentToken, stamp) {
        var imgurl = "";
        imgurl = umsUrl + "?studentIdentity=" + studentToken + "&stamp=" + stamp;
        return imgurl;
    }


    //upload file

    var uploadFileToUmsUrl = function (submitBy, caption, file) {
        var fd = new FormData();
        fd.append('file', file);
        fd.append('sumittedBy', submitBy);
        fd.append('newFileName', caption);
        return $http({
            method: 'Post', url: umsUrl, data: fd,transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        }).
        then(function (response) {
            return response;
        });
    }



    // check file existing
    var getfilecheck=function(studentPicture){
        var fd = new FormData();
        fd.append('imageName', studentPicture);
      //  fd.append('type', type);
       // fd.append('stamp', stamp);
        return $http({
            method: 'Get',
            url: filecheck + "?imageName="+studentPicture,
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        }).
        then(function (response) {
            return response;
        });
    }


    return {
        getImageUrl: getImageUrl,
        getImageFinalUrl:getImageFinalUrl,
        checkImageExiatance: checkImageExiatance,
        getEmptyImageUrl: getEmptyImageUrl,
        uploadFileToUrl: uploadFileToUrl,
        getUmsImageUrl: getUmsImageUrl,
        uploadFileToUmsUrl: uploadFileToUmsUrl,
        getfilecheck: getfilecheck
     }




}]);