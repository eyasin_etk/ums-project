﻿


angular.module('umspInitial').controller('umsStudentIndexController', [
    '$scope', '$location', 'semesterservice', 'departmentservice', 'umsStudentService', 'studentInitializer',
    function ($scope, $location, semesterservice, departmentservice, umsStudentService, studentInitializer) {

        var errorFunction = function (error) {
            console.log(error);
        }
        function init() {
            $scope.pageObj = {
                "PageNo": 0,
                "PageSize": 10
            };

            $scope.filterObj = {
                SemesterId: 0, DepartmentId: 0, GenderId: 0, IdSearch: '', NameSearch: ''
            }
        }
        $scope.pageSizeSet = function () {
            var pageMove = $scope.pageObj.PageNo;
            var pagesize = $scope.pageObj.PageSize;
            $scope.pageObj = { PageNo: 0, PageSize: pagesize, TotalPages: 0, TotalCount: 0 };
            $scope.getAllStudent();
        }
        $scope.navPage = function (pageVal) {
            var pageMove = $scope.pageObj.PageNo + pageVal;
            var pagesize = $scope.pageObj.PageSize;
            $scope.pageObj = { PageNo: pageMove, PageSize: pagesize, TotalPages: 0, TotalCount: 0 };
            $scope.getAllStudent();
        }

        $scope.pageName = 'Student Index';
        $scope.semesters = [];
        $scope.departments = [];

        $scope.getSemesters = function () {
            $scope.semesters = [];
            semesterservice.getAllSemesters().then(function (response) {
                if (response.IsSuccess) {
                    $scope.semesters = response.Data;

                } else {
                    console.log(response);
                }
            }, errorFunction);
        }

        $scope.getDepartments = function () {
            departmentservice.getAllDepartments().then(function (response) {
                $scope.departments = [];
                if (response.IsSuccess) {
                    $scope.departments = response.Data;
                } else {
                    console.log(response);
                }

            }, errorFunction);
        }
        $scope.getAllStudent = function () {
            console.log($scope.filterObj);
            if ($scope.filterObj.SemesterId === null) {
                $scope.filterObj.SemesterId = 0;
            }
            if ($scope.filterObj.DepartmentId === null) {
                $scope.filterObj.DepartmentId = 0;
            }
            umsStudentService.getAllStudent($scope.pageObj, $scope.filterObj).then(function (response) {
                $scope.student = [];
                if (response.IsSuccess) {
                    $scope.pageObj = null;
                    $scope.pageObj = response.Pagination;
                    $scope.allStudentList = response.Data;
                   
                } else {
                    alert('No Data');
                }
            }, errorFunction);
        }
        function resetStudentInitial() {
            studentInitializer.StudentGuidId = undefined;
            studentInitializer.studentPicture = "";
            studentInitializer.StudentId = 0;
            studentInitializer.StudentIdentificationId = 0;
        }
        $scope.setAdmId = function (identity, id, image) {
            resetStudentInitial();
            studentInitializer.StudentIdentificationId = identity;
            studentInitializer.studentPicture = image;
            studentInitializer.StudentId = id;
            console.log(studentInitializer);
            $location.path('/studentIdentity');
        }
        $scope.filterDo = function () {
            if ($scope.filterObj.IdSearch === undefined) {
                $scope.filterObj.IdSearch = '';
            }
            if ($scope.filterObj.NameSearch === undefined) {
                $scope.filterObj.NameSearch = '';
            }
            if ($scope.filterObj.SemesterId === null) {
                $scope.filterObj.SemesterId = 0;
            }
            if ($scope.filterObj.DepartmentId === null) {
                $scope.filterObj.DepartmentId = 0;
            }
            
            $scope.getAllStudent();
        }
        $scope.resetFilter = function () {
            init();
            $scope.getAllStudent();
        }
        $scope.getPreFetchNames = function (nameVal) {
            console.log('Fetching Data');

            return umsStudentService.getPreFetchData(nameVal, '', true)
                .then(function (response) {
                    var names = [];
                    angular.forEach(response.Data, function (item) {
                       names.push(item.StudentName);
                        
                    });
                   return names;
                },errorFunction);
        }
        $scope.getPreFetchIds = function (idVal) {
            console.log('Fetching Data');

            return umsStudentService.getPreFetchData('', idVal, false)
                .then(function (response) {
                    var ids = [];
                    angular.forEach(response.Data, function (item) {
                        ids.push(item.StudentId);
                    });
                    return ids;
                }, errorFunction);
        }
        $scope.getImage = function (id) {
            return umsStudentService.getImageUrl(id, 1, 0);

        }

        init();
        $scope.getSemesters();
        $scope.getDepartments();
        $scope.getAllStudent();
        $scope.getPreFetchNames();
    }
]);

