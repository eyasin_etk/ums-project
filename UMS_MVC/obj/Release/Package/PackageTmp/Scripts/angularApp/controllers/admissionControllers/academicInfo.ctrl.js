﻿


angular.module('umspInitial').controller('academicInfoController', [
    '$scope', '$location', 'academicInfoService', 'studentInitializer', function ($scope, $location, academicInfoService, studentInitializer) {

        $scope.pageName = "Academic Information";

        var errorFunction = function (error) {
            console.log(error);
        }




          $scope.chk = "ok";
        $scope.test = function () {
            console.log($scope.studentAcaObj.NameOfExamination + "hi");
            if ($scope.studentAcaObj.NameOfExamination == 'other') {

                $scope.chk = "no";
                
            }
            else

                $scope.chk = "hihi";

        }





        var initialObject = studentInitializer;
        $scope.init = function () {
            $scope.academics = [];
            console.log(studentInitializer);
            $scope.emptyStudentObj();
            if (initialObject.StudentId !== undefined) {
                $scope.getAllAcademicRecords();
                // $scope.getAllAcademicRecordsByStudentId();
            }
        }
        $scope.diplayStudentInitials = function () {
            $scope.displayGuidId = initialObject.StudentGuidId;
            $scope.displayIdentity = initialObject.StudentIdentificationId;
            $scope.displayId = initialObject.StudentId;
        }
        $scope.emptyStudentObj = function () {
           $scope.clearForm();
        }
        $scope.gteAllYears = function () {
            $scope.years = academicInfoService.getYears();
        }
        function changeBtnName() {
            $scope.btnOperationName = "Update";
            $scope.cnfmId = true;
        }
        $scope.clearForm = function () {
            $scope.btnOperationName = "Add";
            $scope.studentAcaObj = {
                StudentId: initialObject.StudentId,
                StudentAcademicInfoId: 0,
                NameOfExamination: '',
                StartingSession: '',
                UniversityBoard: '',
                PassingYear: '',
                Result: '',
                Group: '',
                DistrictBoard: '',
                Scale: '',
                ResultOptional :''
            };
        }

        $scope.getAllAcademicRecords = function () {
            $scope.academics = [];
            academicInfoService.getAllByInfoId(initialObject.StudentId)
                .then(function (response) {
                    if (response.IsSuccess) {
                        $scope.academics = response.Data;
                        console.log(response.Data);
                    } else {

                        console.log(response);
                    }
                }, errorFunction);
        }
        //$scope.getAllAcademicRecordsByStudentId = function () {
        //    $scope.academics = [];
        //    academicInfoService.getAllByInfoId(initialObject.StudentId)
        //        .then(function (response) {
        //            if (response.IsSuccess) {
        //                $scope.academics = response.Data;
        //                console.log(response.Data);
        //            } else {

        //                console.log(response);
        //            }
        //        }, errorFunction);
        //}

        $scope.save = function () {
           // console.log($scope.studentAcaObj.cgpa + "hihihihihi");
           
            if ($scope.studentAcaObj.NameOfExamination=='other') {
             $scope.studentAcaObj.NameOfExamination = $scope.studentAcaObj.otherExam_name;
            }
             if ($scope.studentAcaObj.Result == 'gpa') {
                $scope.studentAcaObj.Result = $scope.studentAcaObj.cgpa;
            }
             if ($scope.studentAcaObj.ResultOptional == 'gpa') {
                $scope.studentAcaObj.ResultOptional = $scope.studentAcaObj.cgpaOptional;
            }

            console.log($scope.studentAcaObj.cgpa + "hihihihihi");
            console.log($scope.studentAcaObj);
           academicInfoService.saveAcaInfo($scope.studentAcaObj)
                    .then(function (response) {
                        $scope.serverMsg = response.Message;
                        if (response.IsSuccess) {
                            console.log(response);
                            $scope.getAllAcademicRecords();
                           // $scope.clearForm();
                          
                        } else {
                            console.log(response);
                        }
                    }, errorFunction);





        }
        $scope.getDetailById = function (id) {
            console.log(id);
            academicInfoService.getSingleByInfoId(id)
                    .then(function (response) {
                        $scope.serverMsg = response.Message;
                        if (response.IsSuccess) {

                            $scope.studentAcaObj = response.Data;
                            changeBtnName();
                        } else {
                            console.log(response);
                        }
                    }, errorFunction);

        }
        $scope.delete = function (id) {

            if (confirm("Are you sure?")) {
                console.log(id);
                academicInfoService.deleteStudentsAcaInfo(id).then(function (response) {
                    $scope.serverMsg = response.Message;
                    if (response.IsSuccess) {
                        $scope.studentAcaObj = {};
                        $scope.getAllAcademicRecords();
                    } else {
                        console.log(response);
                    }
                }, errorFunction);

            }
            
        }
        $scope.unlock = function () {
            studentInitializer.StudentGuidId = undefined;
            studentInitializer.StudentId = '';
            studentInitializer.StudentIdentificationId = 0;
            $scope.diplayStudentInitials();
            console.log(studentInitializer);
            $location.path('/studentIdentity');
        }

        $scope.init();
        $scope.diplayStudentInitials();
        $scope.gteAllYears();
       }]);