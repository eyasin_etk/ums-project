﻿
angular.module('umspInitial').controller('jobExperienceController', [
    '$scope', '$location', 'jobExperienceService', 'studentInitializer', function ($scope, $location, jobExperienceService, studentInitializer) {

        $scope.pageName = "Job Experience";
       
        var errorFunction = function (error) {
            console.log(error);
        }
        var initialObject = studentInitializer;
        $scope.init = function () {
            $scope.jobs = [];
            console.log(studentInitializer);
            $scope.emptyJobObj();
             if(initialObject.StudentIdentificationId !== undefined) {
                 $scope.getAllJobsRecords();
                // $scope.getAllAcademicRecordsByStudentId();
            }
        }
        $scope.diplayStudentInitials = function () {
            $scope.displayGuidId = initialObject.StudentGuidId;
            $scope.displayIdentity = initialObject.StudentIdentificationId;
            $scope.displayId = initialObject.StudentId;
        }

        $scope.emptyJobObj = function () {
            $scope.clearForm();
        }
        
        function changeBtnName() {
            $scope.btnOperationName = "Update";
            $scope.cnfmId = true;
        }

        $scope.clearForm = function () {
            $scope.btnOperationName = "Add";
            $scope.jobs = {
                AccountId: initialObject.StudentIdentificationId,
                StudentAccountExtProfessionalActivityId: 0,
                AccountMetaProfessionalId:0,
                EmploymentType: '',
                employmentDate: new Date(),
                companyLocation: '',
                Description: '',
                EntryDate: new Date(),
                EntryBy:'',
                company_name: '',
                Designation:'',
                Departmentname: '',
                resignDate: new Date()
            };
        }

        // date conversion

      



        $scope.getAllJobsRecords = function () {
            $scope.academics = [];
            jobExperienceService.getAllByInfoId(initialObject.StudentIdentificationId)
                .then(function (response) {
                    if (response.IsSuccess) {
                        $scope.academics = response.Data;
                        console.log(response.Data);
                    } else {

                        console.log(response);
                    }
                }, errorFunction);
        }
        
        $scope.save = function () {
            console.log($scope.jobs);
            jobExperienceService.saveInfo($scope.jobs)
                .then(function (response) {
                    $scope.serverMsg = response.Message;
                    if (response.IsSuccess) {
                        console.log(response);
                      $scope.getAllJobsRecords();
                        $scope.clearForm();

                    } else {
                        console.log(response);
                    }
                }, errorFunction);

        }


        $scope.getDetailById = function (id) {
            console.log(id);
            jobExperienceService.getSingleByInfoId(id)
                .then(function (response) {
                    $scope.serverMsg = response.Message;
                    if (response.IsSuccess) {

                        $scope.jobs = response.Data;
                        $scope.jobs.employmentDate = new Date(response.Data.employmentDate);
                        $scope.jobs.resignDate = new Date(response.Data.resignDate);

                        changeBtnName();
                    } else {
                        console.log(response);
                    }
                }, errorFunction);

        }


        $scope.delete = function (id) {

            if (confirm("Are you sure?")) {
                

                console.log(id);
                jobExperienceService.deleteJobInfo(id)
                    .then(function (response) {
                        $scope.serverMsg = response.Message;
                        if (response.IsSuccess) {
                            $scope.jobs = {};
                            $scope.getAllJobsRecords();
                        } else {
                            console.log(response);
                        }
                    }, errorFunction);
            }
        }

        $scope.unlock = function () {
            studentInitializer.StudentGuidId = undefined;
            studentInitializer.StudentId = '';
            studentInitializer.StudentIdentificationId = 0;
            $scope.diplayStudentInitials();
            console.log(studentInitializer);
            $location.path('/studentIdentity');
        }

         $scope.init();
        $scope.diplayStudentInitials();
        
    }]);

