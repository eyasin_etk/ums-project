﻿

angular.module('umspInitial').controller('personalInfoController', [
    '$scope', '$location', 'personalInfoService', 'postOnlineAdmService', 'studentInitializer', function ($scope, $location, personalInfoService, postOnlineAdmService, studentInitializer) {

        $scope.pageName = "Personal Information";
        $scope.btnOperationName = "Create";
        $scope.cnfmId = false;
        var initialObject = studentInitializer;


        $scope.init = function () {
            console.log(studentInitializer);
            $scope.emptyStudentObj();
            if (initialObject.StudentId !== '') {
                $scope.getSingleStudentInfoByStudentId();
            }
        }
        $scope.diplayStudentInitials = function () {
            $scope.displayGuidId = initialObject.StudentGuidId;
            $scope.displayIdentity = initialObject.StudentIdentificationId;
            $scope.displayId = initialObject.StudentId;
        }
        $scope.emptyStudentObj = function () {
            $scope.student = {
                StudentIdentificationId: initialObject.StudentIdentificationId,
                StudentId: initialObject.StudentId,
                StudentInfoId: 0,
                DateOfBirth: new Date(),
                EnrolledDepartmentId: 1,
                StudentName: '',
                FathersName: '',
                MothersName: '',
                PersonalContactNo: '',
                PhoneNoSecond: '',
                EmailAddress: '',
                Nationality: '',
                PresentAddress: '',
                ParmanentAddress: '',
                PresentDistrict: '',
                ParmanentDistrict: '',
                PresentPostalCode: '',
                ParmanentPostalCode: '',
                LocalGuardianName: '',
                LocalGuardianAddress: '',
                LocalGuardianContact: '',
                LocalGuardianRelationship: '',
                Father_mobile: '',
                Father_occupation: '',
                Father_designation: '',
                Father_organization: '',
                Father_income: '',
                Mother_mobile: '',
                Mother_occupation: '',
                Mother_designation: '',
                Mother_organization: '',
                mother_income: '',
                Spouse_name: '',
                Spouse_mobile: '',
                Spouse_occupation: '',
                Spouse_designation: '',
                Spouse_organization: '',
                FeePayer_name: '',
                FeePAyer_mobile: '',
                FeePayer_income: '',
                FeePayer_Relation: '',
                Advt_reference: '',
                Admission_waiver: 0
            };
        }

        var errorFunction = function (error) {
            console.log(error);
        }

        $scope.addressCopy = function () {
            $scope.student.ParmanentAddress = $scope.student.PresentAddress;
            $scope.student.ParmanentDistrict = $scope.student.PresentDistrict;
            $scope.student.ParmanentPostalCode = $scope.student.PresentPostalCode;
            $scope.successMsg = 'Address Copied';
        }
        $scope.getAllGenders = function () {
            $scope.genders = [];
            postOnlineAdmService.getAllGenders()
            .then(function (response) {
                if (response.IsSuccess) {
                    $scope.genders = response.Data;
                } else {
                    console.log(response);
                }
            }, errorFunction);
        }
        $scope.getAllBloodGroups = function () {
            $scope.bloodGroups = [];
            postOnlineAdmService.getAllBloods()
            .then(function (response) {
                if (response.IsSuccess) {
                    $scope.bloodGroups = response.Data;

                } else {
                    console.log(response);
                }
            }, errorFunction);
        }

        $scope.getAllMaritalStatus = function () {
            $scope.maritalStatuses = [];
            postOnlineAdmService.getAllMaritals()
            .then(function (response) {
                if (response.IsSuccess) {
                    $scope.maritalStatuses = response.Data;
                } else {
                    console.log(response);
                }
            }, errorFunction);
        }
        function changeBtnName() {
            $scope.btnOperationName = "Update";
            $scope.cnfmId = true;
        }

        $scope.getSingleStudentInfoByStudentId = function () {
            $scope.student = {};
            personalInfoService.getDetailById(initialObject.StudentId).then(function (response) {
                console.log(response);
                if (response.IsSuccess) {
                    $scope.student = response.Data;
                    $scope.student.DateOfBirth = new Date(response.Data.DateOfBirth);
                    changeBtnName();
                } else {
                    $scope.emptyStudentObj();
                    console.log(response.Data);
                }

            }, errorFunction);
        }

        $scope.saveNUpdate = function () {
            var studentInfoObj = $scope.student;
            personalInfoService.saveInfo(studentInfoObj)
		        .then(function (response) {
		            $scope.serverMsg = response.Message;
		            console.log(response);
		            console.log(studentInfoObj);
		            if (response.IsSuccess) {
		                $scope.student = response.Data;
		                $scope.student.DateOfBirth = new Date(response.Data.DateOfBirth);
		                
		            } else {
		                console.log(response);
		            }
		        }, errorFunction);
        }
        $scope.StudentId = studentInitializer.StudentId;



        $scope.delete = function () {
            if (confirm("Are you sure?")) {

                var personalInfoObj = $scope.student;
                console.log("Deleting Student Info Id" + personalInfoObj.StudentId);
                personalInfoService.deleteStudentsPersonalInfo(personalInfoObj.StudentInfoId).then(function (response) {
                    $scope.serverMsg = response.Message;
                    if (response.IsSuccess) {
                    } else {
                        console.log(response);
                    }

                });

            }

        }



        $scope.unlock = function () {
            studentInitializer.StudentGuidId = undefined;
            studentInitializer.StudentId = '';
            studentInitializer.StudentIdentificationId = 0;
            $scope.diplayStudentInitials();
            console.log(studentInitializer);
            $location.path('/studentIdentity');
        }
        $scope.init();
        $scope.diplayStudentInitials();
        $scope.getAllGenders();
        $scope.getAllBloodGroups();
        $scope.getAllMaritalStatus();


    }]);