﻿
angular.module('umspInitial').controller('scrutinyController', [
    '$scope', 'semesterservice', 'departmentservice', 'sectionservice', 'scrutinyservice', 'courseservice', 'facultyservice',
    function ($scope, semesterservice, departmentservice, sectionservice, scrutinyservice, courseservice, facultyservice) {
        function init() {
           
        }

        //$scope.sem = {};
        //$scope.selectedCourse = {};
        $scope.pageName = 'scrutiny Category';
        $scope.msg = 'sss';
        $scope.semesters = [];
        $scope.departments = [];
        $scope.sectionSortObj = { SemId: 0, DeptId: 0, CourseId: 0, TeacherId: 0, SecId: 0 };

        //load all semesters to the dropdown
        $scope.getSemesters = function () {
            $scope.semesters = [];
            semesterservice.getAllSemesters().then(function (response) {
                $scope.semesters = response.Data;
            });
        }
        //load all departments to the dropdown
        $scope.getDepartments = function () {
            departmentservice.getAllDepartments().then(function (response) {
                $scope.departments = [];
                if (response.IsSuccess) {
                    $scope.departments = response.Data;

                } else {
                    alert('No Data');
                }

            });
        }
        $scope.getSectionsByTeacher= function(teacherId) {
            if (angular.isUndefined($scope.sem)) {
                $scope.sem.SemesterId = 0;
                alert('Choose Semester');
            }
           $scope.sectionSortObj = { SemId: $scope.sem.SemesterId, CourseforDeptId: 0, TeacherId: teacherId, SecId: 0 };
          
          sectionservice.getAllSectionsforScruBySemDept($scope.sectionSortObj).then(function (response) {
              console.log(response);
              if (response.IsSuccess) {
                  $scope.secDetails = response.Data;
                  $scope.selectedsecId = response.Data[0].SectionId;
              } else {
                  alert('No Data Found');
                  $scope.selectedsecId = 0;
              }

          });
            
        }
        //submitted values from scrutiny index
        $scope.getAllSecWithScruStat = function () {
            var semId = 0;
            var courseforDepId = 0;
            var teacherId = 0;
            var sectionId = 0;
            console.log(semId, courseforDepId, teacherId, sectionId);
            init();
            if (!angular.isUndefined($scope.sem)) {
                semId = $scope.sem.SemesterId;
            }
            if (!angular.isUndefined($scope.selectedCourse)) {
                courseforDepId = $scope.selectedCourse.CourseForDepartmentId;
            }
            if (!angular.isUndefined($scope.selectedSection)) {
                sectionId = $scope.selectedSection;
            }
            if (angular.isObject(teacherId)) {
                teacherId = 0;
            }
            
            $scope.sectionSortObj = { SemId: semId, CourseforDeptId: courseforDepId, TeacherId: teacherId, SecId: sectionId };
            console.log(semId, courseforDepId, teacherId, sectionId);
            sectionservice.getAllSectionsforScruBySemDept($scope.sectionSortObj).then(function (response) {
                console.log(response);
                if (response.IsSuccess) {
                    $scope.secDetails = response.Data;
                    $scope.selectedsecId = response.Data[0].SectionId;
                    console.log($scope.secDetails);
                } else {
                    alert('No Data Found');
                    $scope.selectedsecId = 0;
                }

            });

        }
        //submited value from scrutiny index row
        $scope.showAllStudent = function (secId) {
           
            $scope.studentList = [];
            $scope.sectionObject = {};
            if (secId !== 0) {
                console.log(secId);
                scrutinyservice.getAllStudentForScrutiny(secId).then(function (response) {
                    console.log(response);
                    $scope.studentList = response.Data.Students;
                    $scope.sectionObject = response.Data.Section;
                });

            }
        }
        //for completing all scrutiny Stat
        $scope.overAllScrutinyStatus = function (secId, category) {
            console.log(secId);
            $scope.overAllScrutinyStat = [];
            scrutinyservice.overAllScruSubmit(secId, category).then(function (response) {
                $scope.overAllScrutinyStat = response.Data;
                if (response.IsSuccess) {
                    $scope.showAllStudent(secId);
                    alert(response.Message);

                }
            });
        }
        //get Department Wise Course List
        $scope.getDeptIdWiseCourseList = function () {
            console.log($scope.selectedDep);
            courseservice.getAllCourses($scope.selectedDep.DepartmentId).then(function (response) {
                $scope.coursesList = response.Data;

            });
        }
        //get teacherList
        $scope.getTeracherList = function () {
            $scope.teacherList = [];
            facultyservice.allFacultycourseWise().then(function (response) {
                $scope.teacherList = response.Data;
            });
        }
        //disable lvl1
        $scope.disableFunc = function (stuId, secId, a) {
            console.log(stuId);
            scrutinyservice.singleScruSubmit(stuId, secId, a).then(function (response) {
                $scope.singleScruSub = response.Data;
                if (response.IsSuccess) {
                    //need for 50 % data checked
                    if (a === 1) {
                        alert(response.Message + " " + ("Checked: " + $('.cb1:checked').length) + " " + ("UnChecked: " + $('.cb1:not(:checked)').length));
                    }
                    if (a === 2) {
                        alert(response.Message + " " + ("Checked: " + $('.cb2:checked').length) + " " + ("UnChecked: " + $('.cb2:not(:checked)').length));

                    }
                }
              
                $scope.isDisabled = response.IsSuccess;
                return false;
            });
        }
        //sample function
        $scope.SampleFunc = function () {

            //alert(val, a);
            //$scope.disable1 = true;
            //$scope.disable2 = false;
            //if (val===true&&a===1) {
            //    $scope.disable1 = true;
            //    $scope.disable2 = false;
            //    return false;
            //} else if (val === true && a === 2) {
            //    $scope.disable1 = true;
            //    $scope.disable2 = true;
            //    return false;
            //}


            //if ($('#cb1').is(':checked')) {
            //    alert($('#cb1').val());
            //}


            //need for 50 % data checked
            //alert("Checked: " + $('.cb1:checked').length);
            //alert("UnChecked: " + $('.cb1:not(:checked)').length);


            //var checked = $("#cb1 input:checked").length > 0;
            //if (!checked) {
            //    alert("Please check at least one checkbox");
            //    return false;
            //} else {
            //    alert("checked");
            //    return true;
            //}


            //if (('#cb1:checked').length>=1) {
            //    alert("checked");
            //} else if (('#cb1:checked').length ===0) {
            //    alert("unchecked");
            //}


            //var checkbox = document.getElementsByName('#cb1');
            //var ln = 0;
            //for (var i = 0; i < checkbox.length; i++) {
            //    if (checkbox[i].checked)
            //        ln++;
            //}
            //alert(ln);


            //$('input[type="checkbox"]').click(function () {
            //    if ($('#cb1').prop("checked") === true) {
            //        alert("Checkbox is checked.");
            //    }
            //    else if ($('#cb1').prop("checked") === false) {
            //        alert("Checkbox is unchecked.");
            //    }
            //});

        }

        $scope.getSemesters();
        $scope.getDepartments();
        $scope.getTeracherList();
    }
]);