﻿
servApp.service('masterAdmService', ['$resource', '$q', 'baseUrl',
    function ($resource, $q, baseUrl) {

        var countSectionsByDept = function () {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'SectionBlockService');
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        var unlockSections = function (unlockStat) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'SectionBlockService?submitAcc=' + unlockStat);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        
        return {
            countSectionsByDept:countSectionsByDept,
            unlockSections: unlockSections
          };

    }]);