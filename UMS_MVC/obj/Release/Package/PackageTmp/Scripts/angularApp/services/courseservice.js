﻿
servApp.service('courseservice', ['$resource', '$q', 'baseUrl',
    function ($resource, $q, baseUrl) {

        var getAllCourses = function (depId) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'CourseForDepartmentService?deptId='+depId);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }

     
        return {
            getAllCourses: getAllCourses
        };

    }]);