﻿servApp.service('facultyservice', ['$resource', '$q', 'baseUrl',
    function ($resource, $q, baseUrl) {

        var getAllFaculty = function () {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'TeacherService');
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }

        var getSingleFaculty = function (tid) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'TeacherService?id=' + tid);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        var allFacultycourseWise = function () {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'FilteredTeacherService');
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        return {
            getAllFaculty: getAllFaculty,
            getSingleFaculty: getSingleFaculty,
            allFacultycourseWise: allFacultycourseWise
        };

    }]);