﻿
servApp.service('academicInfoService', ['$resource', '$q', 'baseUrl',
    function ($resource, $q, baseUrl) {
        var workUrl = baseUrl + 'AcademicInfoService';

        var getYears = function () {
            var currentYear = new Date().getFullYear();
            var years = [];
            for (var i = 1970; i <= currentYear ; i++) {
                years.push(i.toString());
            }
            return years;
        }

        var saveAcaInfo = function (studentObj) {
            var defer = $q.defer();
            var resource = $resource(workUrl);
            resource.save(studentObj, function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        var updateAcaInfo = function (id) {
            var defer = $q.defer();
            var resource = $resource(workUrl + "?id=" + id);
            resource.save(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }

        var getAllByInfoId = function (studentId) {
            var defer = $q.defer();
            var resource = $resource(workUrl + "?id=" + studentId + "&academicId=" + 0 + "&fullType=" + true);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }

        var getSingleByInfoId = function (infoId) {
            var defer = $q.defer();
            var resource = $resource(workUrl + "?id=" + null + "&academicId=" + infoId + "&fullType=" + false);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        var deleteStudentsAcaInfo = function (id) {
            var defer = $q.defer();
            var resource = $resource(workUrl + "?id=" + id);
            resource.delete(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        return {
            getYears: getYears,
            saveAcaInfo: saveAcaInfo,
            updateAcaInfo: updateAcaInfo,
            getAllByInfoId: getAllByInfoId,
            getSingleByInfoId: getSingleByInfoId,
            deleteStudentsAcaInfo: deleteStudentsAcaInfo

        };

    }]);