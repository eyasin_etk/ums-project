﻿servApp.service('submittedDocumentService', ['$resource', '$q', 'baseUrl',
    function ($resource, $q, baseUrl) {


        var findSingleByStudentId = function (id) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'DocumentalInfoService?id=' + id);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        var addSubmittedDocuments = function (studentdocuObj) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'DocumentalInfoService');
            resource.save(studentdocuObj, function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        var updateDocumentInfo = function (studentdocuObj) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'DocumentalInfoService');
            resource.save(studentdocuObj, function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        var deleteSubmittedDocument= function(id) {
            var defer = $q.defer();
            var resource = $resource(baseUrl+'DocumentalInfoService?id=' + id);
            resource.delete(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        return {
            findSingleByStudentId: findSingleByStudentId,
            addSubmittedDocuments: addSubmittedDocuments,
            updateDocumentInfo: updateDocumentInfo,
            deleteSubmittedDocument: deleteSubmittedDocument
       
        };

    }]);