﻿
servApp.service('scrutinyservice', ['$resource', '$q', 'baseUrl',
    function ($resource, $q, baseUrl) {

        var getAllStudentForScrutiny = function (secId) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'ScrutinyService/AllStudentsBySection?sectionId='+secId);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
     
        var overAllScruSubmit = function (secId, category) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'ScrutinyService/OverAllScrutinySubmit', { sectionId: secId, a: category});
            resource.save(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        var singleScruSubmit= function(stuId,secId,a) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'ScrutinyService/SingleScruSubmit', { studentAcaId: stuId,sectionId:secId,e:a });
            resource.save(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        return {
            getAllStudentForScrutiny: getAllStudentForScrutiny,
            overAllScruSubmit: overAllScruSubmit,
            singleScruSubmit: singleScruSubmit
        };

    }]);