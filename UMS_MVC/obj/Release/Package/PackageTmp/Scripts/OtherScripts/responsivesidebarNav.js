﻿function multiMenu() {
    var trigger = $("#trigger");
    var theBigDoc = $(".wrapper");

    trigger.on("click", function (e) {
        e.stopPropagation();
        $(this).toggleClass("open");
        theBigDoc.toggleClass("nav-open");
    });

    $("main").on("click", function () {
        if (theBigDoc.hasClass("nav-open")) {
            trigger.removeClass("open");
            theBigDoc.removeClass("nav-open");
        }
    });
}
multiMenu();



$.expr[":"].contains = $.expr.createPseudo(function (arg) {
    return function (elem) {
        return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
    };
});
//SEARCH
$('.nav-search').on("keyup", function () {
    var sField = $(this);
    $(".nav-lvl2").addClass("for-search");
    console.log("!!!");
    if (sField.val() === '') {
        $(".nav-lvl2").removeClass("for-search");
        $('.nav-lvl2>li').removeClass("hidden");
    }
    else {
        $('.nav-lvl2>li').removeClass("hidden");
        $('.nav-lvl2>li>a:not(:contains("' + sField.val() + '"))').parent().addClass("hidden");
    }
});





