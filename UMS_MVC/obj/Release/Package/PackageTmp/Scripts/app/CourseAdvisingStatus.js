﻿
function noStudentLblFunc() {
    document.getElementById("noStudentLbl").value = "No Student";
    document.getElementById("noStudentLbl").style.color = "orange";
}
function noStudentLblFunc2() {

    document.getElementById("noStudentLbl2").value = "No Student";
    document.getElementById("noStudentLbl2").style.color = "orange";
}

$("#SemesterId").change(function () {
    loadCourseStatus();
});
$("#departmentId").change(function () {
    loadCourseStatus();
});
$("#CourseForDepartmentId").change(function () {
    var courseId = $('#CourseForDepartmentId :selected').val();
    loadAdvisings(courseId);

});

function loadCourseStatus() {
    $("#studentListLoadAnim").show();
    var departmentId = $('#departmentId :selected').val();
    var semesterId = $('#SemesterId :selected').val();
    $.ajax({
        type: 'GET',
        url: '..//CourseForStudent/GetAdvisingStatusByCourse/',
        data: { departmentId: departmentId, semesterId: semesterId },
        success: function (data) {
            $("#studentListLoadAnim").hide();
            $("#courseStatusList").delay(700).html(data);

        },
    });
}
function loadAdvisings(id) {

    var url = '@Url.Action("LoadAdvisingsOfStudents", "CourseForStudent")';
    var departmentId = $('#departmentId :selected').val();
    var semesterId = $('#SemesterId :selected').val();
    var courseId = id;
    $.get(url, { page: 1, SemesterId: semesterId, DepartmentId: departmentId, CourseForDepartmentId: courseId, secSort: 0 }, function (data) {
        $("#advisingListByCourse").html(data);
    });
}

function loadAdvisings2(id) {
    $("#studentListLoadAnim").show();
    var courseId = id;
    var departmentId = $('#departmentId :selected').val();
    var semesterId = $('#SemesterId :selected').val();
    $.ajax({
        type: 'GET',
        url: '..//CourseForStudent/LoadAdvisingsOfStudents/',
        data: { page: 1, SemesterId: semesterId, DepartmentId: departmentId, CourseForDepartmentId: courseId, secSort: 0 },
        success: function (data) {
            $("#studentListLoadAnim").hide();
            $("#advisingListByCourse").delay(700).html(data);

        },
    });
}

