﻿

function openSectionBlock() {
    var url = "..//Home/CountTotalStudentAdmitted";
    $.get(url, null, function(data) {
        $("#totalAdmittedStudent").html(data);
    });
}

function UpdateModalLoad() {
    var url = "..//Admin/UpdateInvaildModalLoad";
    $('#adminModal1').load(url);
}

function CourseAdvisingForSpecificDepartment() {
    var url = "..//Admin/ActivateOtherSemesterCourseAdvising";
    $('#adminModal1').load(url);
}

function updateLoginCounter() {
    var url = "..//Home/CountLoginInUms";
    $.get(url, null, function (data) {
        $("#loginCount").html(data);
        window.setTimeout(updateLoginCounter, 30000);
    });
}

// New
function getAllAdvisingCounter() {
    var url = "..//Home/CountAllPaymentRegistration";
    $.get(url, null, function (data) {
        $("#registartionCount").html(data);
    });
}

function getAllInvalidAdvisingAllSemseterCounter() {
    var url = "..//Home/CountAllInvalidAdvisingInAllSemester";
    $.get(url, null, function (data) {
        $("#CurrentlyInvalidRegistrationAllSemester").html(data);
    });
}

function getAllInvalidAdvisingCurrentSemseterCounter() {
    var url = "..//Home/CountAllInvalidAdvisingInCurrentSemester";
    $.get(url, null, function (data) {
        $("#CurrentlyInvalidRegistrationThisSemester").html(data);
    });
}

function getAllRebatedStudentCounter() {
    var url = "..//Home/CountAllRebatedStudent";

    $.get(url, null, function (data) {
        $("#totalStudentGotRebate").html(data);
    });
}

function getAllAccountcounter() {

    var url = "..//Home/CountAllAccount";
    $.get(url, null, function (data) {
        $("#totalAccounts").html(data);
    });
}

function getTeacherCounter() {
    var url = "..//Home/CountAllTeachers";
    $.get(url, null, function (data) {
        $("#totalTeacher").html(data);
    });
}

function updateTotalStudentBySemester() {
    var url = "..//Home/CountTotalStudentBySemester";
    $.get(url, null, function (data) {
        $("#studentCountInthisSemsester").html(data);
    });
}

// New
function updateTotalAdmittedStudent() {
    var url = "..//Home/CountTotalStudentAdmitted";
    $.get(url, null, function (data) {
        $("#totalAdmittedStudent").html(data);
       // window.setTimeout(updateTotalAdmittedStudent, 40500);
    });

}

function updateAdmittedToday() {
    var url = "..//Home/CountAdmittedToday";
    $.get(url, null, function (data) {
        $("#admittedToday").html(data);
        window.setTimeout(updateAdmittedToday, 20000);
    });

}
function update1() {
    $("#totalSections").html('Loading..');
    $.ajax({
        type: 'GET',
        url: '..//Home/CountTotalSections',
        timeout: 50000,
        success: function (data) {
            $("#totalSections").html(data);
           // window.setTimeout(update1, 10000);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $("#totalSections").html('Timeout contacting server..');
            //window.setTimeout(update1, 60000);
        }
    });
}

$(document).ready(function () {
    $("#adminLogo").slideDown("slow");
    //$("#activityDiv").slideDown("slow");
    //$("#middlePortion").slideDown("slow");
    updateLoginCounter();
    updateTotalAdmittedStudent();
    updateAdmittedToday();
    update1();
    getAllAdvisingCounter();
    getAllInvalidAdvisingAllSemseterCounter();
    getAllInvalidAdvisingCurrentSemseterCounter();
    getAllRebatedStudentCounter();
    getAllAccountcounter();
    getTeacherCounter();
    updateTotalStudentBySemester();
});