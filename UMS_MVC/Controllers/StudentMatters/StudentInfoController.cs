﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO;

namespace UMS_MVC.Controllers.StudentMatters
{
    public class StudentInfoController : Controller
    {
        private UmsDbContext db ;
        private readonly StudentManager _studentManager;

        public StudentInfoController()
        {
            db=new UmsDbContext();
            _studentManager=new StudentManager();
        }

        public ActionResult Index()
        {
            var studentinfos = db.StudentInfos.Include(s => s.BloodGroups).Include(s => s.Genders).Include(s => s.MariaStatus);
            return View(studentinfos.ToList());
        }

        
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
          
///////////////////////////// corrections needed////////////////////////////////////////////////////////////
            StudentInfo studentinfo = db.StudentInfos.Find(id);

         if (studentinfo == null)
            {
                return HttpNotFound();
            }
            return View(studentinfo);
        }

        public ActionResult DetailInfo(int? id)
        {
           
            return View();
        }

        public ActionResult DetailsOfAcademicInfo(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            int x = (db.StudentAcademicInfos.Where(i => i.StudentId == id)).Count();
            if (x > 0)
            {
                var studentinfos = from studentAcademicInfo in db.StudentAcademicInfos
                                   where studentAcademicInfo.StudentId == id
                                   select studentAcademicInfo;
                return View(studentinfos.ToList());
            }
            Session.Add("StudentId", id);
            Session.Add("DocAdd","Doc");
            ViewBag.sig = 1;
            return View();
        }
       

        public ActionResult DetailsOfDocuments(String id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            int x = (db.DocumentsaddAddings.Where(j => j.StudentId == id)).Count();
            if (x > 0)
            {
                var documentadding = (db.DocumentsaddAddings.Where(j => j.StudentId == id)).First();

                return View(documentadding);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Create()
        {
            if (Session["Role"] != null && Session["User"] != null)
            {
                string role = Session["Role"].ToString();
                if (role == "Admin" || role == "Admission" || role=="Support")
                {
                    //////////////// main code start /////////////////////////////
                    if (Session["StudentId"] != null)
                    {
                        var student = new StudentInfo { 
                            StudentId = Session["StudentId"].ToString(), 
                            Nationality = "Bangladeshi",
                            DateOfBirth = DateTime.Now
                        };
                        DropdownsLoad();
                        return View(student);
                    }
                    DropdownsLoad();
                    return View();
                    ////////////////main code end////////////////////////////////
                }
                return RedirectToAction("NotPermitted", "Home");
            }
            return RedirectToAction("LogInresultSubmit", "Home");
        }

        private void DropdownsLoad()
        {
            ViewBag.BloodGroupsId = new SelectList(db.BloodGroupses, "BloodGroupsId", "Name");
            ViewBag.GenderId = new SelectList(db.Genders, "GenderId", "GenderName");
            ViewBag.MaritalStatusId = new SelectList(db.MaritalStatuses, "MaritalStatusId", "MaritalStat");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( StudentInfo studentinfo)
        {
            try
            {
                string msgOfEmail="";
                bool check = _studentManager.GetAllStudentInfos().Count(s=>s.StudentId==studentinfo.StudentId)==0;
                if (check)
                {
                    var mailCheck=_studentManager.GetAllStudentInfos().Count(s=>s.StudentId==studentinfo.EmailAddress)==0;
                    if (mailCheck)
                    {
                        if (ModelState.IsValid)
                        {
                            db.StudentInfos.Add(studentinfo);
                            db.SaveChanges();
                            StudentIdentification findStudent = db.StudentIdentifications.FirstOrDefault(s => s.StudentId == studentinfo.StudentId);
                            if (findStudent != null)
                            {
                                findStudent.StudentInfoId = studentinfo.StudentInfoId;
                                db.Entry(findStudent).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                            Session.Add("studentId", studentinfo.StudentId);
                            return RedirectToAction("StudentAcademicInfoShow");
                        }
                    }
                    msgOfEmail = "Email address used already";
                }
                ViewBag.msg = "Student Info Exists" + msgOfEmail;
                ViewBag.BloodGroupsId = new SelectList(db.BloodGroupses, "BloodGroupsId", "Name", studentinfo.BloodGroupsId);
                ViewBag.GenderId = new SelectList(db.Genders, "GenderId", "GenderName", studentinfo.GenderId);
                ViewBag.MaritalStatusId = new SelectList(db.MaritalStatuses, "MaritalStatusId", "MaritalStat", studentinfo.MaritalStatusId);
                return View(studentinfo);
            }
            catch (Exception)
            {

                return RedirectToAction("ErrorPage", "Home");
            }
           
        }

        public ActionResult StudentAcademicInfoShow()
        {
            if (Session["Role"] != null && Session["User"] != null)
            {
                string role = Session["Role"].ToString();
                if (role == "Admin" || role == "Admission"||role=="Support")
                {
                    //////////////// main code start /////////////////////////////
                    if (Session["DocAdd"] != null)
                    {
                        var student = new StudentAcademicInfo { StudentId = Session["studentId"].ToString() };
                        ViewBag.signal = 1;
                        //Session.Add("DocAdd",string.Empty);
                        return View(student);
                    }

                    if (Session["studentId"] != null)
                    {
                        var student = new StudentAcademicInfo { StudentId = Session["studentId"].ToString() };
                        return View(student);
                    }
                    return RedirectToAction("Create");

                    ////////////////main code end////////////////////////////////
                }
                return RedirectToAction("NotPermitted", "Home");
            }
            return RedirectToAction("LogInresultSubmit", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult StudentAcademicInfoShow(StudentAcademicInfo studentAcademicInfo, string submitButton)
        {
            var checkAcademic =
              db.StudentAcademicInfos.Count(x=>x.StudentId==studentAcademicInfo.StudentId && x.NameOfExamination == studentAcademicInfo.NameOfExamination) == 0;
            switch (submitButton)
            {
                case "Save":
                    if (checkAcademic)
                    {
                        if (ModelState.IsValid)
                        {
                            db.StudentAcademicInfos.Add(studentAcademicInfo);
                            db.SaveChanges();
                            return RedirectToAction("StudentDocumentAddings");
                        }
                    }
                    ViewBag.msg = "Same Examination exists ! ";
                    return View(studentAcademicInfo);
               case "Save and reload":
                    if (checkAcademic)
                    {
                        if (ModelState.IsValid)
                        {
                            db.StudentAcademicInfos.Add(studentAcademicInfo);
                            db.SaveChanges();
                            ModelState.Clear();
                            var student = new StudentAcademicInfo {StudentId = Session["studentId"].ToString()};
                            return View(student);

                        }
                    }
                    ViewBag.msg = "Same Examination exists ! ";
                    return View(studentAcademicInfo);
                case "Skip":
                    return RedirectToAction("StudentDocumentAddings");
                case "Add":
                    if (checkAcademic)
                    {
                        if (ModelState.IsValid)
                        {
                            db.StudentAcademicInfos.Add(studentAcademicInfo);
                            db.SaveChanges();
                            Session.Add("StudentId", "");
                        }
                    }
                    ViewBag.msg = "Same Examination exists ! ";
                    return RedirectToAction("Index2", "StudentAllDetails");

                case "Save and reload2":
                    if (checkAcademic)
                    {
                        if (ModelState.IsValid)
                        {
                            db.StudentAcademicInfos.Add(studentAcademicInfo);
                            db.SaveChanges();
                            ModelState.Clear();
                            var student = new StudentAcademicInfo {StudentId = Session["studentId"].ToString()};
                            ViewBag.signal = 1;
                            return View(student);
                        }
                    }
                    ViewBag.msg = "Same Examination exists ! ";
                    return RedirectToAction("Index", "StudentIdentity");

                default:
                    return RedirectToAction("Index","StudentIdentity");

            }

        }
        
        public ActionResult StudentDocumentAddings()
        {
            if (Session["Role"] != null && Session["User"] != null)
            {
                string role = Session["Role"].ToString();
                if (role == "Admin" || role == "Admission"||role=="Support")
                {
                    //////////////// main code start /////////////////////////////
                    if (Session["studentId"] != null)
                    {
                        var student = new DocumentAdding { StudentId = Session["studentId"].ToString() };
                        return View(student);
                    }
                    return RedirectToAction("StudentAcademicInfoShow");
                    ////////////////main code end////////////////////////////////
                }
                return RedirectToAction("NotPermitted", "Home");
            }
            return RedirectToAction("LogInresultSubmit", "Home");

          
            
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult StudentDocumentAddings(DocumentAdding adding, string submitButton)
        {
            Session.Add("studentId", "");
            switch (submitButton)
            {
                case "Save":
                    if (ModelState.IsValid)
                    {
                        db.DocumentsaddAddings.Add(adding);
                        db.SaveChanges();
                        return RedirectToAction("Index","StudentIdentity");
                    }
                    return View(adding);
                case "Skip":

                    return RedirectToAction("Index", "StudentIdentity");
                case "Enter another student":
                   
                    return RedirectToAction("Create");

                default:
                    return RedirectToAction("Index", "StudentIdentity");

            }
        }

        public ActionResult Edit(int? id)
        {
            if (Session["Role"] != null && Session["User"] != null)
            {
                string role = Session["Role"].ToString();
                if (role == "Admin" || role == "Admission" || role == "Support")
                {
                    //////////////// main code start /////////////////////////////
                    if (id == null)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    StudentInfo studentinfo = db.StudentInfos.Find(id);
                    if (studentinfo == null)
                    {
                        return HttpNotFound();
                    }
                    ViewBag.BloodGroupsId = new SelectList(db.BloodGroupses, "BloodGroupsId", "Name", studentinfo.BloodGroupsId);
                    ViewBag.GenderId = new SelectList(db.Genders, "GenderId", "GenderName", studentinfo.GenderId);
                    ViewBag.MaritalStatusId = new SelectList(db.MaritalStatuses, "MaritalStatusId", "MaritalStat", studentinfo.MaritalStatusId);
                    return View(studentinfo);
                    ////////////////main code end////////////////////////////////
                }
                return RedirectToAction("NotPermitted", "Home");
            }
            return RedirectToAction("LogInresultSubmit", "Home");

           
        }

       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( StudentInfo studentinfo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(studentinfo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BloodGroupsId = new SelectList(db.BloodGroupses, "BloodGroupsId", "Name", studentinfo.BloodGroupsId);
            ViewBag.GenderId = new SelectList(db.Genders, "GenderId", "GenderName", studentinfo.GenderId);
            ViewBag.MaritalStatusId = new SelectList(db.MaritalStatuses, "MaritalStatusId", "MaritalStat", studentinfo.MaritalStatusId);
            return View(studentinfo);
        }

        
        public ActionResult Delete(int? id)
        {
            if (Session["Role"] != null && Session["User"] != null)
            {
                string role = Session["Role"].ToString();
                if (role == "Admin")
                {
                    //////////////// main code start /////////////////////////////
                    if (id == null)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    StudentInfo studentinfo = db.StudentInfos.Find(id);
                    if (studentinfo == null)
                    {
                        return HttpNotFound();
                    }
                    return View(studentinfo);
                    ////////////////main code end////////////////////////////////
                }
                return RedirectToAction("NotPermitted", "Home");
            }
            return RedirectToAction("LogInresultSubmit", "Home");

           
        }

        
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            StudentInfo studentinfo = db.StudentInfos.Find(id);
            db.StudentInfos.Remove(studentinfo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
