﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Mvc;
using UMS_MVC.Controllers.OtherSpecialControllers;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO;

namespace UMS_MVC.Controllers.StudentMatters
{
    public class StudentIdentityController : Controller
    {
        private UmsDbContext db;
        private readonly DepartmentManager _departmentManager;
        //private readonly CommonAttributeManager _commonAttributeManager=new CommonAttributeManager();
        private readonly SemesterManager _semesterManager;
        private readonly StudentManager _studentManager;
        private readonly StudentExtraManager _studentExtraManager;
        private readonly UserActivityManager _userActivityManager;

        public StudentIdentityController()
        {
            db = new UmsDbContext();
            _departmentManager = new DepartmentManager();
            _semesterManager = new SemesterManager();
            _studentManager = new StudentManager();
            _studentExtraManager = new StudentExtraManager();
            _userActivityManager = new UserActivityManager();
        }

        // GET: /StudentIdentity/
        public ActionResult Index()
        {
            if (Session["Role"] != null && Session["User"] != null)
            {
                string role = Session["Role"].ToString();
                if (role == "Admin" || role == "Admission" || role == "Support")
                {
                    //////////////// main code start /////////////////////////////
                    var studentidentifications = db.StudentIdentifications.Include(s => s.Department).Include(s => s.School).Include(s => s.SemesterInfo);
                    return View(studentidentifications.ToList());
                    ////////////////main code end////////////////////////////////
                }
                return RedirectToAction("NotPermitted", "Home");
            }
            return RedirectToAction("LogInresultSubmit", "Home");

          
        }

        // GET: /StudentIdentity/Details/5
        public ActionResult Details(int? id)
        {
            if (Session["Role"] != null && Session["User"] != null)
            {
                string role = Session["Role"].ToString();
                if (role == "Admin" || role == "Support")
                {
                    //////////////// main code start /////////////////////////////
                    if (id == null)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    StudentIdentification studentidentification = db.StudentIdentifications.Find(id);
                    if (studentidentification == null)
                    {
                        return HttpNotFound();
                    }
                    return View(studentidentification);
                    ////////////////main code end////////////////////////////////
                }
                return RedirectToAction("NotPermitted", "Home");
            }
            return RedirectToAction("LogInresultSubmit", "Home");
        }
        public ActionResult Create()
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Admission" && account.AccountsRole.AccountType!="Support") return RedirectToAction("NotPermitted", "Home");
            {
               DropDownsForCreateStudents();
               return View();
            }
        }

        public void DropDownsForCreateStudents()
        {
            ViewBag.DepartmentId = new SelectList(_departmentManager.GetAllDepartmentForDropdown(), "DepartmentId", "DepartmentName", "ASchool.SchoolName", 0);
            if (_semesterManager.GetAdvisingActivatedSemester() != null)
            {
                ViewBag.SemesterId = new SelectList(_semesterManager.GetAllSemesterForDropdown(), "SemesterId", "SemesterNYear", _semesterManager.GetAdvisingActivatedSemester().SemesterId);
            }
            ViewBag.SemesterId = new SelectList(_semesterManager.GetAllSemesterForDropdown(), "SemesterId", "SemesterNYear", _semesterManager.GetActiveSemester().SemesterId);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DepartmentId,SemesterId,CreditTransfer,DiplomaStudent,Remark")] StudentIdentification studentidentification, string submitButton)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Admission" && account.AccountsRole.AccountType!="Support") return RedirectToAction("NotPermitted", "Home");
            switch (submitButton)
            {
                case "CreateId":
                {
                    DropDownsForCreateStudents();
                    if (studentidentification.DepartmentId > 0 && studentidentification.SemesterId > 0)
                    {
                        string studentId = GenerateResult(studentidentification);
                        if (studentId != null)
                        {
                           string password= Passgenerator.GeneratePassword(3);
                            var aIdentification = new StudentIdentification
                            {
                                SchoolId = 5,
                                DepartmentId = studentidentification.DepartmentId,
                                SemesterInfoId = 4,
                                SemesterAndYear = "",
                                Validation = true,
                                StudentId = studentId,
                                SemesterId = studentidentification.SemesterId,
                                AddedDate = DateTime.Now,
                                CreditTransfer = studentidentification.CreditTransfer,
                                Remark = studentidentification.Remark,
                                DiplomaStudent = studentidentification.DiplomaStudent,
                                Password = password,
                                EntryBy = "ENT /" + DateTime.Now.Date.ToString("d") + " /" + account.LoginIdentity
                            };
                            
                            db.StudentIdentifications.Add(aIdentification);
                            db.SaveChanges();
                            Session.Add("StudentId", studentId);
                            string msg = "STUDENT ID : " + studentId;
                            //string msg = "STUDENT ID : " + studentId+" & PASSWORD : "+password;
                            ViewBag.StudentIdentity =msg ;
                            TempData["admittedStudentdata"]=_studentExtraManager.GenerateViewStudent(aIdentification);
                            _userActivityManager.InsertActivity(new UserActivity
                            {
                                UserActivityTypeId = 1,
                                AccountId = account.AccountId,
                                ActivityTime = DateTime.Now,
                                StudentId = studentId,
                                Activity =account.LoginIdentity +" admitted Student ID "+ studentId
                            });
                            ModelState.Clear();
                            return View();
                        }
                        ViewBag.StudentExist = "Student already Exists !";
                        return View(studentidentification);
                    }
                    ViewBag.StudentExist = "Please enter all informations !";
                    return View();
                }
                case "StartAddmission":
                    return RedirectToAction("Create", "StudentInfo");
                default:
                    return View();
            }
        }

        public string GenerateResult(StudentIdentification aIdentification)
        {
            if (aIdentification.DepartmentId > 0 && aIdentification.SemesterId>0)
            {
                int countStudent =
                    _studentManager.GetAllStudentByDepartment(aIdentification.DepartmentId)
                    .Count(s => s.SemesterId == aIdentification.SemesterId);
                var getSemesterInfo =
                    _semesterManager.GetAllSemester().
                    FirstOrDefault(s => s.SemesterId == aIdentification.SemesterId);
                var departmentCode = _departmentManager.GetSingleDepartment(aIdentification.DepartmentId).DepartmentCode;
                if (countStudent > 0)
                {
                    string identity;
                    int nextId = countStudent;
                    do
                    {
                        nextId++;
                        identity = getSemesterInfo.BatchNo + nextId.ToString("D3") + departmentCode.ToString("D3");
                    } while (_studentManager.StudentExistanceByStudentId(identity));

                    return identity;
                }
                if (countStudent == 0)
                {
                    const int studentSerial = 1;
                    string identity = getSemesterInfo.BatchNo + studentSerial.ToString("D3") + departmentCode.ToString("D3");
                    return identity;
                }
            }
            return null;
        }
        public bool CheckExistingId(string identity)
        {
            return _studentManager.StudentExistanceByStudentId(identity);
        }
        //public ActionResult Create()
        //{
        //    if (Session["Role"] != null && Session["User"] != null)
        //    {
        //        string role = Session["Role"].ToString();
        //        if (role == "Admin" || role == "Admission")
        //        {
        //            //////////////// main code start /////////////////////////////
        //            //ViewBag.SchoolId = new SelectList(_schoolManager.GetAllSchools(), "SchoolId", "SchoolName");
        //            ViewBag.DepartmentId = new SelectList(_departmentManager.GetAllDepartment(), "DepartmentId", "DepartmentName", "ASchool.SchoolName", 0);
                    
        //            ViewBag.SemesterInfoId = new SelectList(db.SemesterInfos, "SemesterInfoId", "SemesterName");
        //            //ViewBag.YearId = (DateTime.Now.Year).ToString();
        //            ViewBag.YearId = new SelectList(_commonAttributeManager.GetAllYears(), "Years", "Years", DateTime.Now.Year.ToString());
        //            return View();
        //            ////////////////main code end////////////////////////////////
        //        }
        //        return RedirectToAction("NotPermitted", "Home");
        //    }
        //    return RedirectToAction("LogInresultSubmit", "Home");
        //}

        //public string  GenerateResult(StudentIdentification aIdentification)
        //{
        //    if (aIdentification.SchoolId >0 && aIdentification.DepartmentId >0 && aIdentification.SemesterInfoId >0)
        //    {
        //        string year = DateTime.Now.ToString("yy");
               
        //        int students = (from studentIdentification in db.StudentIdentifications.AsParallel()
        //            where
        //                studentIdentification.DepartmentId == aIdentification.DepartmentId &&
        //                studentIdentification.SemesterInfoId == aIdentification.SemesterInfoId
        //            select studentIdentification).Count();
        //        var departmentCode = (from department in db.Departments.AsParallel()
        //                              where department.DepartmentId == aIdentification.DepartmentId
        //                              select department.DepartmentCode).First();            
        //        if (students > 0)
        //        {
        //            string identity;
        //            int nextId = students;
        //            do
        //            {
        //                nextId++;
        //                identity = year + aIdentification.SemesterInfoId + nextId.ToString("D3") + departmentCode.ToString("D3");
        //            } while (CheckExistingId(identity));

        //            return identity;
        //        }
        //        if (students==0)
        //        {
        //            const int studentSerial = 1;
        //            string identity = year + aIdentification.SemesterInfoId + studentSerial.ToString("D3") + departmentCode.ToString("D3");
        //            return identity;
        //        }           
               
        //    }

        //    return null;
            
        //}
       
        public ActionResult SortStudentAndDeptJs3(int? schoolId)
        {
            if (schoolId > 0)
            {
                List<Department> departments = db.Departments.Include(y=>y.ASchool).Where(s => s.SchoolId == schoolId).ToList();
                ViewBag.DepartmentId = new SelectList(departments, "DepartmentId", "DepartmentName");
                return PartialView("~/Views/StudentIdentity/_DepartmentSortPartial.cshtml");
            }
            ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "DepartmentName");
            return PartialView("~/Views/StudentIdentity/_DepartmentSortPartial.cshtml");
        }
        
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "StudentIdentificationId,SchoolId,DepartmentId,SemesterInfoId,StudentId")] StudentIdentification studentidentification, string submitButton, string YearId)
        //{
        //    switch (submitButton)
        //    {
        //        case "CreateId":
        //        {
        //            bool checkExistance =
        //                db.StudentIdentifications.Count(s => s.StudentId == studentidentification.StudentId) == 0;
        //            if (checkExistance && ModelState.IsValid )
        //            {
        //                string studentId = GenerateResult(studentidentification);
        //                if (studentId != null)
        //                {
        //                    var aIdentification = new StudentIdentification
        //                    {
        //                        SchoolId = studentidentification.SchoolId,
        //                        DepartmentId = studentidentification.DepartmentId,
        //                        SemesterInfoId = studentidentification.SemesterInfoId,
        //                        SemesterAndYear = db.SemesterInfos.Find(studentidentification.SemesterInfoId).SemesterName+"-"+YearId,
        //                        Validation = false,
        //                        StudentId = studentId,
        //                        AddedDate = DateTime.Now
        //                    };

        //                    db.StudentIdentifications.Add(aIdentification);
        //                    db.SaveChanges();
        //                    Session.Add("StudentId",studentId);
        //                    ViewBag.StudentIdentity = studentId;
        //                    ModelState.Clear();
        //                    ViewBag.SchoolId = new SelectList(db.Schools, "SchoolId", "SchoolName", studentidentification.SchoolId = 0);
        //                    ViewBag.SemesterInfoId = new SelectList(db.SemesterInfos, "SemesterInfoId", "SemesterName");
        //                    ViewBag.YearId = DateTime.Now.Year;
        //                    return View();
        //                }

        //            }
        //            ViewBag.SchoolId = new SelectList(db.Schools, "SchoolId", "SchoolName");
        //            ViewBag.SemesterInfoId = new SelectList(db.SemesterInfos, "SemesterInfoId", "SemesterName");
        //            ViewBag.YearId = DateTime.Now.Year;
        //            ViewBag.StudentExist = "Student already Exists !";
        //            return View(studentidentification);
        //        }
        //        case "StartAddmission":
        //            return RedirectToAction("Create","StudentInfo");
        //        default:
        //            return View();
        //    }  
        //}

        public ActionResult Edit(int? id)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account) Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Admission" &&
                account.AccountsRole.AccountType != "Support") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StudentIdentification studentidentification = db.StudentIdentifications.Find(id);
            if (studentidentification == null)
            {
                return HttpNotFound();
            }
            ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "DepartmentName",
                studentidentification.DepartmentId);
            ViewBag.SchoolId = new SelectList(db.Schools, "SchoolId", "SchoolName", studentidentification.SchoolId);
            ViewBag.SemesterInfoId = new SelectList(db.SemesterInfos, "SemesterInfoId", "SemesterName",
                studentidentification.SemesterInfoId);

            return View(studentidentification);
        }

        public bool CheckOwnId(StudentIdentification st)
        {
            int checkOwn = (from stu in db.StudentIdentifications.AsParallel()
                              where stu.StudentIdentificationId == st.StudentIdentificationId &&
                                    stu.StudentId == st.StudentId
                              select stu).Count();
            if (checkOwn > 0)
            {
                return true;
            }
            return false;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "StudentIdentificationId,SchoolId,DepartmentId,SemesterInfoId,StudentId,SemesterAndYear,Validation,AddedDate")] StudentIdentification studentidentification, string YearId)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Admission" && account.AccountsRole.AccountType != "Support") return RedirectToAction("NotPermitted", "Home");
            try
            {
                if (studentidentification.SchoolId > 0 && studentidentification.DepartmentId > 0 && studentidentification.SemesterInfoId > 0)
                {
                    if (ModelState.IsValid)
                    {
                       studentidentification.EntryBy = account.Name + " - " + account.LoginIdentity;
                       db.Entry(studentidentification).State = EntityState.Modified;
                       db.SaveChanges();
                       return RedirectToAction("Index");
                     }
                }
               ViewBag.Msg = "Select All fields properly";
               ViewBags(studentidentification);
               return View(studentidentification);
            }
            catch (DataException)
            {
                return RedirectToAction("ErrorPage", "Home");
            }          
           
        }

        public void ViewBags(StudentIdentification aIdentification)
        {
            ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "DepartmentName", aIdentification.DepartmentId);
            ViewBag.SchoolId = new SelectList(db.Schools, "SchoolId", "SchoolName", aIdentification.SchoolId);
            ViewBag.SemesterInfoId = new SelectList(db.SemesterInfos, "SemesterInfoId", "SemesterName", aIdentification.SemesterInfoId);
            
        }

        // GET: /StudentIdentity/Delete/5
        public ActionResult Delete(int? id)
        {
            if (Session["Role"] != null && Session["User"] != null)
            {
                string role = Session["Role"].ToString();
                if (role == "Admin")
                {
                    //////////////// main code start /////////////////////////////
                    if (id == null)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    StudentIdentification studentidentification = db.StudentIdentifications.Find(id);
                    if (studentidentification == null)
                    {
                        return HttpNotFound();
                    }
                    return View(studentidentification);
                    ////////////////main code end////////////////////////////////
                }
                return RedirectToAction("NotPermitted", "Home");
            }
            return RedirectToAction("LogInresultSubmit", "Home");          
        }

        // POST: /StudentIdentity/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                StudentIdentification studentidentification = db.StudentIdentifications.Find(id);
                db.StudentIdentifications.Remove(studentidentification);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception)
            {

                return RedirectToAction("ErrorPage","Home");
            }
            
        }

        public ActionResult CreateOldStudent()
        {
            if (Session["Role"] != null && Session["User"] != null)
            {
                string role = Session["Role"].ToString();
                if (role == "Admin" || role == "Admission" || role=="Support")
                {
                    //////////////// main code start /////////////////////////////
                    DropDownsForCreateStudents();
                    var student=new StudentIdentification()
                    {
                        AddedDate = DateTime.Now
                    };
                    return View(student);
                    ////////////////main code end////////////////////////////////
                }
                return RedirectToAction("NotPermitted", "Home");
            }
            return RedirectToAction("LogInresultSubmit", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateOldStudent(StudentIdentification studentidentification)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Admission" && account.AccountsRole.AccountType != "Support") return RedirectToAction("NotPermitted", "Home");
            DropDownsForCreateStudents();
            bool checkExistance =
                db.StudentIdentifications.Count(s => s.StudentId == studentidentification.StudentId) == 0;
            if (studentidentification.StudentId!=null)
            {
                if (ModelState.IsValid && checkExistance)
                {
                    string password = Passgenerator.GeneratePassword(3);
                    var aIdentification = new StudentIdentification
                    {
                        SchoolId = 5,
                        DepartmentId = studentidentification.DepartmentId,
                        SemesterInfoId = 4,
                        SemesterAndYear = "",
                        Validation = true,
                        StudentId = studentidentification.StudentId,
                        AddedDate = studentidentification.AddedDate,
                        SemesterId = studentidentification.SemesterId,
                        CreditTransfer = studentidentification.CreditTransfer,
                        Remark = studentidentification.Remark,
                        DiplomaStudent = studentidentification.DiplomaStudent,
                        Password = password,
                        EntryBy = "ENT /" + DateTime.Now.Date.ToString("d") + " /" + account.LoginIdentity
                    };
                    TempData["admittedStudentdata"] = _studentExtraManager.GenerateViewStudent(aIdentification);
                    db.StudentIdentifications.Add(aIdentification);
                    db.SaveChanges();
                    Session.Add("StudentId", studentidentification.StudentId);
                    string msg = "Student ID : " +studentidentification.StudentId;
                    //string msg = "STUDENT ID : " + studentidentification.SemesterId + " & PASSWORD : " + password;
                    ViewBag.StudentIdentity = msg;
                    _userActivityManager.InsertActivity(new UserActivity
                    {
                        UserActivityTypeId = 1,
                        AccountId = account.AccountId,
                        ActivityTime = DateTime.Now,
                        StudentId = aIdentification.StudentId,
                        Activity = account.LoginIdentity + " admitted Old Student ID " + aIdentification.StudentId
                    });
                    ModelState.Clear();
                    return View();
                }
                ViewBag.StudentExist = "Student already Exists !";
                return View(studentidentification);
            }
            ViewBag.StudentExist = "Student Id Field cann't be empty !!";
            return View(studentidentification);
        }

        public string CheckStudentIdValidity( int semId,string studentIdentityId,int deptId=0)
        {
            var sb = new StringBuilder();
            var studentIdentityLength = studentIdentityId.Length;
            if (studentIdentityLength!=9 || deptId==0)
            {
                sb.Append("<font color='#ff5252'>Type Full Id or select Department properly.</font> </br>");
                
            }
            else if (studentIdentityLength > 9)
            {
                sb.Append("<font color='#ff5252'>Student ID is more than 9 character.</font> </br>");
            }
            else
            {
                var getDeptCode = _departmentManager.GetSingleDepartment(deptId).DepartmentCode.ToString("D3");
                var getBatchNo = _semesterManager.GetSingleSemester(semId).BatchNo.ToString("D3");
                var checkExistingStudent = _studentManager.StudentExistanceByStudentId(studentIdentityId);
                var deptCode = studentIdentityId.Substring(studentIdentityId.Length - 3);
                var batchNo = studentIdentityId.Substring(0, 3);
                if (getDeptCode != deptCode || deptId == 0)
                {
                    sb.Append("<font color='#ff5252'><b>Type or select Department Correctly. <b></font></br>");
                }
                if (getBatchNo != batchNo)
                {
                    sb.Append("<font color='#ff5252'><b>Type or select batch / year+semester Correctly. <b></font></br>");
                }
                if (checkExistingStudent)
                {
                    sb.Append(" <font color='#ff5252'><b>Student already Exists !!<b></font></br>");
                }
                if (getDeptCode == deptCode && getBatchNo == batchNo && !checkExistingStudent)
                {
                    sb.Append("<font color='#00bb39'><b>Student ID is Valid.<b></font></br>");
                }
            }
            return sb.ToString();
        }

       
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
