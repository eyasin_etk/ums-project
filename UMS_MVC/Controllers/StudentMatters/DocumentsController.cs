﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.Controllers.StudentMatters
{
    public class DocumentsController : Controller
    {
        private UmsDbContext db = new UmsDbContext();

        // GET: /Documents/
        public ActionResult Index()
        {
            return View(db.DocumentsaddAddings.ToList());
        }

        // GET: /Documents/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DocumentAdding documentadding = db.DocumentsaddAddings.Find(id);
            if (documentadding == null)
            {
                return HttpNotFound();
            }
            return View(documentadding);
        }

        // GET: /Documents/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DocumentAdding documentadding)
        {
             bool check = db.DocumentsaddAddings.Count(s => s.StudentId == documentadding.StudentId) == 0;
            if (check)
            {
                if (ModelState.IsValid)
                {
                    db.DocumentsaddAddings.Add(documentadding);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            ViewBag.msg = "Document Exists !";
            return View(documentadding);
        }

        // GET: /Documents/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DocumentAdding documentadding = db.DocumentsaddAddings.Find(id);
            if (documentadding == null)
            {
                return HttpNotFound();
            }
            return View(documentadding);
        }

     
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( DocumentAdding documentadding)
        {
            if (ModelState.IsValid)
            {
                db.Entry(documentadding).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(documentadding);
        }

        // GET: /Documents/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DocumentAdding documentadding = db.DocumentsaddAddings.Find(id);
            if (documentadding == null)
            {
                return HttpNotFound();
            }
            return View(documentadding);
        }

        // POST: /Documents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DocumentAdding documentadding = db.DocumentsaddAddings.Find(id);
            db.DocumentsaddAddings.Remove(documentadding);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
