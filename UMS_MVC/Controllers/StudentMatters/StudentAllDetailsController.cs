﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using PagedList;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.Manager.DAL.DAO.Intefaces;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IAdmission;
using UMS_MVC.Manager.DAL.DAO.Repository;
using UMS_MVC.Manager.DAL.DAO.Repository.RAdmission;
using UMS_MVC.ViewModels.ViewModels;
using UMS_MVC.ViewModels.ViewModels.SortingIndex;

namespace UMS_MVC.Controllers.StudentMatters
{
    public class StudentAllDetailsController : Controller
    {
        private UmsDbContext db;
        private readonly CourseForStudentManger _courseForStudentManger;
        private readonly StudentManager _studentManager;
        private readonly SemesterManager _semesterManager;
        private readonly DepartmentManager _departmentManager;
        private readonly CourseForDepartmentManager _courseForDepartmentManager;
        private readonly CommonAttributeManager _commonAttributeManager;
        private readonly GradeManager _gradeManager;
        private readonly Stopwatch _stopwatch = new Stopwatch();
        private readonly IStudentIdentityRepository _identityRepository;
        private readonly ICourseForStudentRepository _courseForStudentRepository;
        private readonly ISemesterRepository _semesterRepository;

        public StudentAllDetailsController()
        {
            db = new UmsDbContext();
            _courseForStudentManger = new CourseForStudentManger();
            _studentManager = new StudentManager();
            _semesterManager = new SemesterManager();
            _departmentManager = new DepartmentManager();
            _courseForDepartmentManager = new CourseForDepartmentManager();
            _commonAttributeManager = new CommonAttributeManager();
            _gradeManager = new GradeManager();
            _identityRepository=new StudentIdentityRepository();
            _courseForStudentRepository=new CourseForStudentRepository();
            _semesterRepository=new SemesterRepository();
        }

        public ActionResult Index2()
        {
            try
            {
                if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
                ViewBag.SemesterId = new SelectList(_semesterManager.GetAllSemesterForDropdown(), "SemesterId", "SemesterNYear",
                _semesterManager.GetActiveSemester().SemesterId);
                ViewBag.DepartmentList = new SelectList(_departmentManager.GetAllDepartmentForDropdown(), "departmentId",
                    "DepartmentName", "ASchool.SchoolName", 0);
                return View();
            }
            catch (Exception)
            {
                return RedirectToAction("ErrorPage", "Home");
            }

        }

        public PartialViewResult StudentIndexFiltered(int? Page, string IdSearch,
            string NameSearch, int SemesterId = 0, int DepartmentId = 0, int GenderId = 3)
        {
            int pageSize;
            int pageNumber;
            int totalStudent = 0;
            int totalFemale = 0;
            int totalMale = 0;
            ////////////for Admission print

            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType == "Admission" || account.AccountsRole.AccountType == "Admin")
            {
                ViewData["admissionPrint"] = account;
            }
            ///////////////
            var students = _studentManager.GetAllStudentIdentityLimited();
            if (!string.IsNullOrEmpty(NameSearch))
            {
                var newlist =
                    students
                        .Where(s => s.StudentInfo.StudentName.Contains(NameSearch))
                        .OrderByDescending(s => s.AddedDate);
                if (newlist.Any())
                {
                    totalStudent = newlist.Count();
                }
                ViewBag.StudentCount = totalStudent;
                pageSize = newlist.Count();
                pageNumber = (Page ?? 1);
                var studentindexSorting = new StudentIndexSorting
                {
                    SemesterId = SemesterId,
                    DepartmentId = DepartmentId,
                    IdSearch = IdSearch,
                    NameSearch = NameSearch,
                    TotalStudents = totalStudent,
                    Page = Page
                };
                ViewBag.studentIndexFilterObj = studentindexSorting;
                return PartialView("_StudentIndexFilteredPartial", newlist.ToPagedList(pageNumber, pageSize));
            }
            if (!String.IsNullOrEmpty(IdSearch))
            {
                students = students.Where(s => s.StudentId.Contains(IdSearch));
            }
            var studentindexSorting2 = new StudentIndexSorting();

            if (SemesterId > 0 && DepartmentId > 0)
            {
                var getSemester = _semesterManager.GetSingleSemester(SemesterId);
                Department department = _departmentManager.GetSingleDepartment(DepartmentId);
                studentindexSorting2.DepartmentName = department.DepartmentName;
                studentindexSorting2.SemesterName = ", " + getSemester.SemesterNYear;
                students = students.Where(x => x.DepartmentId == DepartmentId && x.SemesterId == SemesterId);
                switch (GenderId)
                {
                    case 1:
                        students = students.Where(s => s.StudentInfo.GenderId == 1);
                        break;
                    case 2:
                        students = students.Where(s => s.StudentInfo.GenderId == 2);
                        break;
                }
            }
            else
            {
                students = students.Take(350);
            }
            if (students.Any())
            {
                totalStudent = students.Count();
                totalMale = students.Count(s => s.StudentInfo.GenderId == 1);
                totalFemale = students.Count(s => s.StudentInfo.GenderId == 2);
            }
            studentindexSorting2.SemesterId = SemesterId;
            studentindexSorting2.DepartmentId = DepartmentId;
            studentindexSorting2.IdSearch = IdSearch;
            studentindexSorting2.NameSearch = NameSearch;
            studentindexSorting2.TotalStudents = totalStudent;
            studentindexSorting2.Page = Page;
            studentindexSorting2.TotalMaleStudents = totalMale;
            studentindexSorting2.TotalFemaleStudents = totalFemale;

            ViewBag.studentIndexFilterObj = studentindexSorting2;
            students = from s in students
                orderby s.StudentId.Length, s.StudentId.Length
                select s;
            
            //students = students.OrderByDescending(s => s.StudentIdentificationId);
            pageSize = 30;
            pageNumber = (Page ?? 1);

            return PartialView("_StudentIndexFilteredPartial", students.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult ResdirectToIndex2()
        {
            if (Session["RequestUrlToIndex2"] != null)
            {
                return Redirect(Session["RequestUrlToIndex2"].ToString());
            }
            return RedirectToAction("Index2");
        }

        public PartialViewResult TodaysAdmissionList()
        {
            if (Session["UserObj"] == null)
                return PartialView("_notPermittedPartial");
            string role = Session["Role"].ToString();
            if (role != "Admin" && role != "Admission" && role != "Ps" && role != "Support") return PartialView("_notPermittedPartial");
            //////////////// main code start /////////////////////////////

            var studentList =
                _studentManager.GetAllStudentIdentifications()
                    .Where(s => DbFunctions.TruncateTime(s.AddedDate) == DateTime.Today)
                    .Include(s => s.StudentInfo);
            return PartialView("_todaysAdmissionList", studentList.ToList());

            ////////////////main code end////////////////////////////////
        }

        public PartialViewResult GetStudentCountByDepartment(int semesterStatusId = 0)
        {
            int semesterFinder;
            string semseterName;
            if (semesterStatusId != 0)
            {
                var getspecificSemester = _semesterManager.GetSingleSemester(semesterStatusId);
                semseterName = getspecificSemester.SemesterNYear;
                semesterFinder = semesterStatusId;
            }
            else
            {
                var getActiveSemester = _semesterManager.GetActiveSemester();
                semseterName = getActiveSemester.SemesterNYear;
                semesterFinder = getActiveSemester.SemesterId;
            }
            ViewBag.currentSemesterName = semseterName;
            ViewBag.semesterStatusId = new SelectList(_semesterManager.GetAllSemesterForDropdown(), "SemesterId", "SemesterNYear", semesterFinder);

            return PartialView("_StudentCountByDepartmentPartial",
                _departmentManager.GetStudentCountByDepartments(semesterFinder));
        }

        public void LoadDropDowns(string selectedYear)
        {
            string currentYear = Convert.ToString(DateTime.Now.Year);
            ViewBag.SemesterNYear = !string.IsNullOrEmpty(selectedYear)
                ? new SelectList(
                    _semesterManager.GetAllSemestersDefault()
                        .Where(s => s.SemesterNYear.EndsWith(selectedYear))
                        .OrderByDescending(s => s.SemesterId), "SemesterNYear", "SemesterNYear")
                : new SelectList(
                    db.Semesters.Where(s => s.SemesterNYear.EndsWith(currentYear)).OrderByDescending(s => s.SemesterId),
                    "SemesterNYear", "SemesterNYear");
            ViewBag.Year = new SelectList(_semesterManager.GetallYears(), "Years", "Years", DateTime.Now.Year);


        }

        public ActionResult StudentDetailAdmin(int id = 0)
        {

            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var role = Session["Role"].ToString();
            var account = (Account)Session["UserObj"];

            //////////////// main code start /////////////////////////////
            ViewBag.access = true;
            if (account.AccountId == 8)
            {
                ViewBag.access = false;
            }
            StudentIdentification studentidentification = _studentManager.GetSingleStudent(id);

            if (studentidentification == null)
            {
                return HttpNotFound();
            }

            ViewBag.Role = role;
            return View(studentidentification);
            ////////////////main code end////////////////////////////////
        }



        //public ActionResult StudentDetailLoad(string studentId)
        //{
        //    if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
        //    string role = Session["Role"].ToString();

        //    //////////////// main code start /////////////////////////////
        //    try
        //    {
        //        var getAllStudentInfos = _studentManager.GetAllStudentInfos().Where(s => s.StudentId == studentId);
        //        if (getAllStudentInfos.Any())
        //        {
        //            StudentInfo student = getAllStudentInfos.FirstOrDefault();
        //            ViewBag.Role = role;
        //            return PartialView("~/Views/StudentAllDetails/_StdudentDetailsPartial.cshtml", student);
        //        }
        //        ViewBag.sid = studentId;
        //        ViewBag.Role = role;
        //        return PartialView("~/Views/StudentAllDetails/_StdudentDetailsPartial.cshtml", null);
        //    }
        //    catch (Exception)
        //    {
        //        return RedirectToAction("ErrorPage", "Home");
        //    }

        //    ////////////////main code end////////////////////////////////

        //}
        public ActionResult StudentDetailLoad(int studentInfId, string studentId)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account2 = (Account)Session["UserObj"];
            try
            {
                var studentInformation = new StudentInfo
                {
                    StudentId = studentId,
                    StudentInfoId = 0
                };
                if (studentInfId > 0 && !string.IsNullOrEmpty(studentId))
                {
                    var dbstudentInfo = _studentManager.GetSingleStudentInfo(studentInfId);
                    if (dbstudentInfo != null)
                    {
                        studentInformation = dbstudentInfo;
                    }
                }
                else if (studentInfId == 0 && !string.IsNullOrEmpty(studentId))
                {
                    var dbstudentInfo = _studentManager.GetStudentInfoByStudentId(studentId);
                    if (dbstudentInfo != null)
                    {
                        studentInformation = dbstudentInfo;
                    }
                }
                //ViewBag.sid = studentId;
                bool updateAccess = account2.AccountsRole.AccountType == "Admin" || account2.AccountsRole.AccountType == "Support" || account2.AccountsRole.AccountType == "Admission";
                ViewBag.updateAccess = updateAccess;
                return PartialView("~/Views/StudentAllDetails/_StdudentDetailsPartial.cshtml", studentInformation);
            }
            catch (Exception)
            {
                return RedirectToAction("ErrorPage", "Home");
            }

            ////////////////main code end////////////////////////////////

        }
        public ActionResult DetailsOfAcademicInfo(string studentId)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();

            //////////////// main code start /////////////////////////////
            try
            {
                var getAllStudentInfo = _studentManager.GetAllAcademicInfos().Where(s => s.StudentId == studentId);

                if (getAllStudentInfo.Any())
                {
                    ViewBag.Role = role;
                    return PartialView("~/Views/StudentAllDetails/_StudentAcademicInfoPartial.cshtml", getAllStudentInfo);
                }
                ViewBag.sid = studentId;
                ViewBag.Role = role;
                return PartialView("~/Views/StudentAllDetails/_StudentAcademicInfoPartial.cshtml", null);
            }
            catch (Exception)
            {
                return RedirectToAction("ErrorPage", "Home");
            }
            ////////////////main code end////////////////////////////////
        }

        public ActionResult DetailOfDocumentAttatchedInfo(string studentId)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();

            //////////////// main code start /////////////////////////////

            try
            {
                var getallDocumentInfo = _studentManager.GetAllStudentDocument().Where(s => s.StudentId == studentId);
                if (getallDocumentInfo.Any())
                {
                    var student = getallDocumentInfo.FirstOrDefault();
                    ViewBag.Role = role;
                    return PartialView("~/Views/StudentAllDetails/_StudentDocumentAttatchedPartial.cshtml", student);
                }
                ViewBag.sid = studentId;
                ViewBag.Role = role;
                return PartialView("~/Views/StudentAllDetails/_StudentDocumentAttatchedPartial.cshtml", null);
            }
            catch (Exception)
            {
                return RedirectToAction("ErrorPage", "Home");
            }
            ////////////////main code end////////////////////////////////

        }

        public ActionResult ReloadPage()
        {
            return RedirectToAction("Index2");
        }

        ///////////////////////////////////////////////Profile 

        public ActionResult EditProfileInformation(int id, string studentId)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Admission")
                return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            StudentInfo studentInfo = new StudentInfo();
            if (id == 0)
            {
                studentInfo.DateOfBirth=DateTime.Now;
                studentInfo.StudentId = studentId;
                studentInfo.BloodGroupsId = 1;
                studentInfo.GenderId = 1;
                studentInfo.MaritalStatusId = 1;
            }
            else
            {
                var getStudentInfoes = _studentManager.GetSingleStudentInfo(id);
                if (getStudentInfoes != null)
                {
                    studentInfo = getStudentInfoes;
                }
            }

          TempData["BloodGroupsId"] = new SelectList(_studentManager.GetAllBloodGroups(), "BloodGroupsId", "Name",
                studentInfo.BloodGroupsId);
          TempData["GenderId"] = new SelectList(_studentManager.GetAllGender(), "GenderId", "GenderName",
                studentInfo.GenderId);
           TempData["MaritalStatusId"] = new SelectList(_studentManager.GetAllmMaritalStatuses(), "MaritalStatusId",
                "MaritalStat", studentInfo.MaritalStatusId);
            return View("~/Views/StudentAllDetails/EditProfileInformation.cshtml", studentInfo);

            ////////////////main code end////////////////////////////////
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProfileInformation(StudentInfo studentinfo)

        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Admission")
                return RedirectToAction("NotPermitted", "Home");
            string msgs = "";

            try
            {
               
                if (!string.IsNullOrEmpty(studentinfo.StudentId))
                {
                    if (studentinfo.StudentInfoId > 0)
                    {
                        db.Entry(studentinfo).State = EntityState.Modified;
                    }
                    else
                    {
                        db.StudentInfos.Add(studentinfo);
                    }
                    db.SaveChanges();
                    StudentIdentification findStudent = db.StudentIdentifications.FirstOrDefault(s => s.StudentId == studentinfo.StudentId);
                    if (findStudent != null)
                    {
                        findStudent.StudentInfoId = studentinfo.StudentInfoId;
                        db.Entry(findStudent).State=EntityState.Modified;
                        db.SaveChanges();
                    }
                  
                }
            }
            catch (Exception exception)
            {
                msgs = exception.ToString();
            }
           
            if (Session["RequestUrl"] != null)
            {
                return Redirect(Session["RequestUrl"].ToString());
            }
            ViewBag.message = msgs;
            TempData["BloodGroupsId"] = new SelectList(_studentManager.GetAllBloodGroups(), "BloodGroupsId", "Name",
                studentinfo.BloodGroupsId);
            TempData["GenderId"] = new SelectList(_studentManager.GetAllGender(), "GenderId", "GenderName",
                studentinfo.GenderId);
            TempData["MaritalStatusId"] = new SelectList(_studentManager.GetAllmMaritalStatuses(), "MaritalStatusId",
                "MaritalStat", studentinfo.MaritalStatusId);
            return View(studentinfo);
        }

        ///////////////////////////////////////////////Document customization////////////////////////////////////////////////

        public ActionResult CreateDocumentInformation(string id)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Admission")
                return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            var student = new DocumentAdding { StudentId = id };
            return View(student);
            ////////////////main code end////////////////////////////////
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateDocumentInformation(
            DocumentAdding documentadding)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Admission")
                return RedirectToAction("NotPermitted", "Home");
            if (ModelState.IsValid)
            {
                db.DocumentsaddAddings.Add(documentadding);
                db.SaveChanges();
                if (Session["RequestUrl"] != null)
                {
                    return Redirect(Session["RequestUrl"].ToString());
                }
            }

            return View(documentadding);
        }

        public ActionResult EditDocumentInformation(string id)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Admission")
                return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            var getAllDocumnent = _studentManager.GetAllStudentDocument().Where(s => s.StudentId == id);
            if (getAllDocumnent.Any())
            {
                DocumentAdding dc = getAllDocumnent.FirstOrDefault();
                return View(dc);
            }
            return View("~/Views/StudentAllDetails/EditDocumentInformation.cshtml", null);
            ////////////////main code end////////////////////////////////
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditDocumentInformation(
          DocumentAdding documentadding)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Admission")
                return RedirectToAction("NotPermitted", "Home");
            if (ModelState.IsValid)
            {
                db.Entry(documentadding).State = EntityState.Modified;
                db.SaveChanges();
                if (Session["RequestUrl"] != null)
                {
                    return Redirect(Session["RequestUrl"].ToString());
                }
            }
            return View(documentadding);
        }

        //////////////////////////----Student Academic Info---------------///////////////////
        public ActionResult CreateAcademicInformation(string id)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Admission")
                return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            var student = new StudentAcademicInfo { StudentId = id };
            return View(student);
            ////////////////main code end////////////////////////////////
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateAcademicInformation(
          StudentAcademicInfo studentacademicinfo)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Admission")
                return RedirectToAction("NotPermitted", "Home");

            int countSameAcademicInfo =
                (_studentManager.GetAllAcademicInfos()
                    .Count(
                        s =>
                            s.NameOfExamination == studentacademicinfo.NameOfExamination &&
                            s.StudentId == studentacademicinfo.StudentId));
            if (countSameAcademicInfo == 0)
            {
                if (ModelState.IsValid)
                {
                    db.StudentAcademicInfos.Add(studentacademicinfo);
                    db.SaveChanges();
                    if (Session["RequestUrl"] != null)
                    {
                        return Redirect(Session["RequestUrl"].ToString());
                    }
                }
            }
            ViewBag.msg = "Same Examination Exists !";
            return View(studentacademicinfo);
        }

        public ActionResult EditAcademicInformation(int? id)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Admission")
                return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StudentAcademicInfo studentacademicinfo = db.StudentAcademicInfos.Find(id);
            if (studentacademicinfo == null)
            {
                return HttpNotFound();
            }
            return View(studentacademicinfo);
            ////////////////main code end////////////////////////////////
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditAcademicInformation(
            [Bind(
                Include =
                    "StudentAcademicInfoId,StudentId,NameOfExamination,StartingSession,UniversityBoard,PassingYear,Result,Group"
                )] StudentAcademicInfo studentacademicinfo)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Admission")
                return RedirectToAction("NotPermitted", "Home");
            if (ModelState.IsValid)
            {
                db.Entry(studentacademicinfo).State = EntityState.Modified;
                db.SaveChanges();
                if (Session["RequestUrl"] != null)
                {
                    return Redirect(Session["RequestUrl"].ToString());
                }
            }
            return View(studentacademicinfo);
        }

        ///////////////////////////////////////////-------------Student Deleting---------///////////////////////////////////
        public ActionResult DeleteStudentAcademicInfo(int? id)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Admission")
                return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            var studentAcademic = db.StudentAcademicInfos.Find(id);
            db.StudentAcademicInfos.Remove(studentAcademic);
            db.SaveChanges();
            TempData["msg"] = "Student Academic info Deleted.";
            if (Session["RequestUrl"] != null)
            {
                return Redirect(Session["RequestUrl"].ToString());
            }
            return RedirectToAction("Index2");
            ////////////////main code end////////////////////////////////
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteStudent(int deleteStudentId)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin") return RedirectToAction("NotPermitted", "Home");
            try
            {
                StudentIdentification studentIdentification = db.StudentIdentifications.Find(deleteStudentId);
                var studentInfo =
                    (_studentManager.GetAllStudentInfos().Count(s => s.StudentId == studentIdentification.StudentId));
                var studentdocument =
                    (_studentManager.GetAllStudentDocument().Count(s => s.StudentId == studentIdentification.StudentId));
                var studentAcademic =
                    (_studentManager.GetAllAcademicInfos().Count(s => s.StudentId == studentIdentification.StudentId));
                var courseForStudentCount =
                    _courseForStudentManger.GetAllCourseForStudentsAcademics()
                        .Count(s => s.StudentIdentification.StudentId == studentIdentification.StudentId);
                if (courseForStudentCount > 0)
                {
                    TempData["ConfirmDeleteMsg"] =
                        "Student can not be deleted, Student registered some courses. Delete from course Registration & payment registration first";
                    return RedirectToAction("Index2");
                }
                db.Database.ExecuteSqlCommand("Delete from [StudentIdentifications] where StudentId={0}",
                    studentIdentification.StudentId);
                if (studentInfo > 0 && studentdocument > 0 && studentAcademic > 0)
                {
                    db.Database.ExecuteSqlCommand("Delete from [DocumentAddings] where StudentId={0}",
                        studentIdentification.StudentId);
                    db.Database.ExecuteSqlCommand("Delete from [StudentAcademicInfoes] where StudentId={0}",
                        studentIdentification.StudentId);
                    db.Database.ExecuteSqlCommand("Delete from [StudentInfoes] where StudentId={0}",
                        studentIdentification.StudentId);
                }
                else if (studentInfo > 0 && studentdocument > 0 && studentAcademic == 0)
                {
                    db.Database.ExecuteSqlCommand("Delete from [DocumentAddings] where StudentId={0}",
                        studentIdentification.StudentId);
                    db.Database.ExecuteSqlCommand("Delete from [StudentInfoes] where StudentId={0}",
                        studentIdentification.StudentId);
                }
                else if (studentInfo > 0 && studentdocument == 0 && studentAcademic == 0)
                {
                    db.Database.ExecuteSqlCommand("Delete from [StudentInfoes] where StudentId={0}",
                        studentIdentification.StudentId);
                }
                TempData["ConfirmDeleteMsg"] = "Student Deleted Successfully";
                return RedirectToAction("Index2");
            }
            catch (Exception)
            {
                throw new Exception();
            }
        }

        public ActionResult AdmitCardPrintOut(string term, int SemesterId = 0, int department = 0)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Exam" &&
                account.AccountsRole.AccountType != "ExamCtrl" && account.AccountsRole.AccountType != "Support") return RedirectToAction("NotPermitted", "Home");
            TempData["terms"] = new SelectList(_commonAttributeManager.GetAllTerms(), "Term", "Term");
            TempData["SemesterId"] = new SelectList(_semesterManager.GetAllSemester(), "SemesterId", "BatchNo",
                _semesterManager.GetActiveSemester().SemesterId);
            TempData["departments"] = new SelectList(_departmentManager.GetAllDepartment(), "departmentId",
                "DepartmentName");
            var getActiveSemester = _semesterManager.GetActiveSemester();
            ViewBag.activeSemester = getActiveSemester.SemesterNYear;

            if (SemesterId > 0 || department > 0)
            {
                var countStudents = _studentManager.GetAllStudentForAdmitCard(department, SemesterId).Count();
                if (countStudents > 0)
                {
                    ViewBag.selectedTerm = term;
                    ViewBag.selectedSemester = SemesterId;
                    ViewBag.selectedDepartment = department;

                    ViewBag.printConfirm = countStudents + "  Students found !";
                    return View();
                }

                ViewBag.noStudent = "No Student found.";
                return View();
            }

            return View();
        }


        public ActionResult StudentsCourseListByCurriculam(int studentId)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");


            var findStudent = _studentManager.GetSingleStudentByStudentIdentification(studentId);
            var takenCourses = _courseForStudentManger.GetAllCourseByStudent(studentId);
            ///////////////// Course List/////////////////////
            var studentWiseCourseList =
                _courseForDepartmentManager.GetAllCourseForDepartmentsByDepartment(findStudent.DepartmentId);
            var data = new List<dynamic>();
            foreach (var courselist in studentWiseCourseList)
            {
                if (takenCourses.Count(s => s.CourseForDepartmentId == courselist.CourseForDepartmentId) > 0)
                {
                    CourseForDepartment courselist1 = courselist;
                    foreach (
                        var courseForStudentsAcademic in
                            takenCourses.Where(s => s.CourseForDepartmentId == courselist1.CourseForDepartmentId))
                    {
                        dynamic courses = new ExpandoObject();
                        courses.StudentID = studentId;
                        courses.Semester = courselist.SerializedSemester.SemesterName;
                        courses.CourseForDepartmentId = courselist.CourseForDepartmentId;
                        courses.CourseCode = courselist.CourseCode;
                        courses.CourseName = courselist.CourseName;
                        courses.Credit = courselist.Credit;
                        courses.Prerequisit = courselist.PrerequisiteCourse;
                        courses.Status = courseForStudentsAcademic.CourseStatus.Status;
                        courses.completeSemester = courseForStudentsAcademic.Semester.SemesterNYear;
                        courses.Grade = courseForStudentsAcademic.LetterGrade;
                        data.Add(courses);
                    }
                }
                else
                {
                    dynamic courses = new ExpandoObject();
                    courses.StudentID = studentId;
                    courses.Semester = courselist.SerializedSemester.SemesterName;
                    courses.CourseForDepartmentId = courselist.CourseForDepartmentId;
                    courses.CourseCode = courselist.CourseCode;
                    courses.CourseName = courselist.CourseName;
                    courses.Credit = courselist.Credit;
                    courses.Prerequisit = courselist.PrerequisiteCourse;
                    courses.Status = "";
                    courses.completeSemester = "";
                    courses.Grade = "";
                    data.Add(courses);
                }
            }
            ViewBag.studentiIdentity = studentId;
            return View(data);
        }

        public ActionResult StudentsCourseListBySemester(int studentId)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");

            ViewData["accessObj"] = GetAccess();
            ViewBag.studentiIdentity = studentId;
            var takenCourses = _courseForStudentManger.GetAllCourseByStudent(studentId);
            if (takenCourses.Any())
            {
                return View(takenCourses.ToList());
            }
            ViewBag.EmptyCourseListmsg = "This student has not taken any courses.";

            return View(new List<CourseForStudentsAcademic>());

        }

        public bool GetAccess()
        {
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType == "Admin")
            {
                return true;
            }
            return false;
        }

        public ActionResult RedirectToStudentPage(int studentIdentityId = 0)
        {
            return RedirectToAction("StudentDetailAdmin", new { id = studentIdentityId });
        }
        public List<CumulativeScores> TabulationCreation(IEnumerable<int> studentList, int semesterId = 0)
        {
            int getActiveSemester = semesterId > 0 ? semesterId : _semesterManager.GetActiveSemester().SemesterId;

            return (from i in studentList
                    let getCumulatives = _gradeManager.CalculateCgpaSingleSemester(i, getActiveSemester)
                    select new CumulativeScores
                    {
                        StudentId = i,
                        Credits = getCumulatives.Credits,
                        CreditEarned = getCumulatives.CreditEarned,
                        SemesterGpa = getCumulatives.SemesterGpa
                    }).ToList();
        }

        //public List<CumulativeScores> TabulationCreation(IEnumerable<int> studentList, int semesterId = 0)
        //{
        //    int getActiveSemester;
        //    if (semesterId > 0)
        //    {
        //        getActiveSemester = semesterId;
        //    }
        //    else
        //    {
        //        getActiveSemester = _semesterManager.GetActiveSemester().SemesterId;
        //    }

        //    var cumulativesList = new List<CumulativeScores>();
        //    foreach (var i in studentList)
        //    {
        //        var getCumulatives = _gradeManager.CalculateCgpaSingleSemester(i, getActiveSemester);
        //        var cumulativeForStudent = new CumulativeScores
        //        {
        //            StudentId = i,
        //            Credits = getCumulatives.Credits,
        //            CreditEarned = getCumulatives.CreditEarned,
        //            SemesterGpa = getCumulatives.SemesterGpa
        //        };
        //        cumulativesList.Add(cumulativeForStudent);
        //    }
        //    return cumulativesList;
        //}

        public ActionResult TabulationPage(int departmentId = 0, int batchno = 0, int semesterId = 35, bool print = false)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Exam" &&
                account.AccountsRole.AccountType != "ExamCtrl") return RedirectToAction("NotPermitted", "Home");
            ViewBag.DepartmentId = new SelectList(_departmentManager.GetAllDepartmentFiltered().Include(s=>s.ASchool).AsNoTracking(), "departmentId",
                "DepartmentName", "ASchool.SchoolName", departmentId);
            ViewBag.Batch = new SelectList("", "SemesterId", "BatchNo", batchno);
            var getSemesters =_semesterRepository.GetAll().AsNoTracking().Where(s => s.SemesterId >= 35).OrderBy(s => s.SemesterId).ToList();
            ViewBag.semesterId = new SelectList(getSemesters, "SemesterId", "SemesterNYear", semesterId);
            _stopwatch.Reset();
            _stopwatch.Restart();
            if (departmentId > 0)
            {
                int getActiveSemester = semesterId > 0 ? semesterId : _semesterManager.GetActiveSemester().SemesterId;
                //var coursesForsAcademic =
                //    _courseForStudentManger.GetAllCourseForStudentsAcademicsByDeptAndSemester(departmentId,
                //        getActiveSemester);
                var coursesForsAcademic =
                  _courseForStudentRepository.GetAll().Include(s=>s.CourseForDepartment.Department).Include(s=>s.StudentIdentification).Include(s=>s.CourseForDepartment).Where(s=>s.SemesterId==semesterId && s.CourseForDepartment.DepartmentId== departmentId).AsNoTracking();
                //coursesForsAcademic = coursesForsAcademic.OrderBy(s => s.CourseForDepartmentId);
               // coursesForsAcademic = coursesForsAcademic;
                if (coursesForsAcademic.Any())
                {
                    //var separateStudents =
                    //    coursesForsAcademic
                    //        .DistinctBy(s => s.StudentIdentificationId)
                    //        .Select(s => s.StudentIdentificationId);
                    var separateStudents = (from courseForStu in coursesForsAcademic
                            .DistinctBy(s => s.StudentIdentificationId)
                        select courseForStu.StudentIdentificationId).ToList();
                    var batchSorting = coursesForsAcademic.Include(s => s.StudentIdentification.Semester).DistinctBy(s => s.StudentIdentification.SemesterId).
                   Select(s => s.StudentIdentification.Semester);
                    if (batchno > 0)
                    {
                        coursesForsAcademic = coursesForsAcademic.Where(s => s.StudentIdentification.SemesterId == batchno);
                    }
                    var orderedQueryable = (from student in coursesForsAcademic.Include(s => s.StudentIdentification.StudentInfo)
                                            orderby student.StudentIdentification.StudentId.Length, student.StudentIdentification.StudentId
                                            select student).ToList();

                    var getTabulationData = TabulationCreation(separateStudents, semesterId);
                    ViewBag.selectedBatch = batchno;
                    ViewBag.selectedSemesterId = getActiveSemester;
                    ViewData["createTabulation"] = getTabulationData;
                    ViewBag.Batch = new SelectList(batchSorting, "SemesterId", "BatchNo", batchno);
                    _stopwatch.Stop();
                    ViewBag.timeTaken = "Time taken to generate tabulation " +
                                        _stopwatch.Elapsed.ToString("mm\\:ss\\.ff") + " (min:sec.msec)";
                    if (print)
                    {
                        TempData["deptid"] = departmentId;
                        TempData["tabulationData"] = orderedQueryable;
                        TempData["cumulativeData"] = getTabulationData;
                        TempData["semesterData"] = _semesterManager.GetSingleSemester(getActiveSemester).SemesterNYear;
                        return RedirectToAction("TabulationPrint", "DocumentPrint");
                    }
                    return View(orderedQueryable);
                }
            }
            ViewBag.msg = "You have to select Department first !";
            return View();
        }

        public ActionResult StudentIdenUpdate(int id)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Admission" && account.AccountsRole.AccountType != "Support") return RedirectToAction("NotPermitted", "Home");
            if (id > 0)
            {
                var stu = _studentManager.GetSingleStudentByStudentIdentification(id);
                return View(stu);
            }
            return View();
        }

        public string StudentIdenEdit(StudentIdentification studentIdentification)
        {
            if (Session["UserObj"] == null) return "Session expire. Login first.";
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Admission" && account.AccountsRole.AccountType != "Support") return "You are not permitted to do changes.";
            try
            {
                var stu = db.StudentIdentifications.Find(studentIdentification.StudentIdentificationId);
                if (stu.StudentIdentificationId > 0)
                {
                    stu.CreditTransfer = studentIdentification.CreditTransfer;
                    stu.Remark = studentIdentification.Remark;
                    stu.DiplomaStudent = studentIdentification.DiplomaStudent;
                    stu.AddedDate = studentIdentification.AddedDate;
                    stu.EntryBy = stu.EntryBy + ", UPD /" + DateTime.Now.Date.ToString("d") + " /" + account.LoginIdentity;
                    db.Entry(stu).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return "Student Updated Successfully";

            }
            catch (DbException)
            {

                return "Student Update Failed";
            }
            //return RedirectToAction("StudentIdenUpdate", "StudentAllDetails",
            //    new {id = studentIdentification.StudentIdentificationId});

        }

        public ActionResult GetCgpaOfAllStudentsByDepartment(int departmentId = 0, int sortBy = 0)
        {
            var getDepartmentInfo = "";
            if (departmentId > 0)
            {
                getDepartmentInfo = _departmentManager.GetSingleDepartment(departmentId).DepartmentName;
            }

            var getActiveSemester = _semesterManager.GetActiveSemester();
            var allStudent = _studentManager.GetAllStudentByDepartment(departmentId).OrderBy(s => s.StudentId);
            var cgpaList = new List<CumulativeScores>();
            foreach (var studentIdentification in allStudent)
            {
                var getCgpa = _gradeManager.CalculateCgpaofAStudent(studentIdentification.StudentIdentificationId);
                getCgpa.Name = studentIdentification.StudentInfoId == null
                    ? ""
                    : studentIdentification.StudentInfo.StudentName;
                getCgpa.DepartmentalId = studentIdentification.StudentId;
                cgpaList.Add(getCgpa);
            }
            if (departmentId == 0)
            {
                ViewBag.message = "Please Select Department First !";
            }
            ViewBag.departmentId = departmentId;
            if (sortBy > 0)
            {
                cgpaList = sortBy == 1
                    ? cgpaList.OrderBy(s => s.DepartmentalId).ToList()
                    : cgpaList.OrderByDescending(s => s.Cgpa).ToList();
            }
            ViewBag.sortby = sortBy;
            ViewBag.semesterData = getActiveSemester.SemesterNYear;
            ViewBag.departmentInfo = getDepartmentInfo;
            return PartialView("_CGPACalculationAll", cgpaList);
        }
        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //        _courseForStudentManger.Dispose();
        //        _studentManager.Dispose();
        //        _semesterManager.Dispose();
        //        _departmentManager.Dispose();
        //        _courseForDepartmentManager.Dispose();
        //        _commonAttributeManager.Dispose();
        //        _gradeManager.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
        public string DeletePic(int stdId)
        {
            if (Session["UserObj"] == null) return "You are not logged in. Log in first";
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin")
                return "Sorry you are not permitted to do this action.";

            var studentDetails = db.StudentIdentifications.Find(stdId);

            string path = Request.MapPath("~/Image/" + studentDetails.StudentPicture);
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
                studentDetails.StudentPicture = null;
                db.Entry(studentDetails).State = EntityState.Modified;
                db.SaveChanges();

            }
            return "Image removed successfully.";
        }
    }
}