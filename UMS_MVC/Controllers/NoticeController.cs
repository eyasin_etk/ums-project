﻿using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.DataModel.Models.Notice_Matter;
using UMS_MVC.Manager.DAL.DAO;

namespace UMS_MVC.Controllers
{
    public class NoticeController : Controller
    {
        private readonly UmsDbContext _db;
        private readonly NoticeBoardDetailsManager _detailsManager;
        private readonly NoticeBoardCategoryManager _categoryManager;
        private readonly NoticeBoardAttachFileManager _fileManager;

        public NoticeController()
        {
            _db = new UmsDbContext();
            _detailsManager = new NoticeBoardDetailsManager();
            _categoryManager = new NoticeBoardCategoryManager();
            _fileManager = new NoticeBoardAttachFileManager();
        }
        // GET: Notice

        public ActionResult Index()
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin") return RedirectToAction("NotPermitted", "Home");
            return View();
        }
        public ActionResult IndexUpdated()
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin") return RedirectToAction("NotPermitted", "Home");
            return View();
        }
        //starting of category of notice 
        public PartialViewResult NoticeCategoryPartial()
        {
            var category = _categoryManager.GetAllCategories();
            return PartialView("_noticeCategoryPartial", category);
        }

        public PartialViewResult NewCategoryPartial(int id = 0)
        {

            if (id != 0)
            {
                var cat = _categoryManager.GetSinglecCategory(id);
                return PartialView("_noticeCategoryUpdatePartial", cat);
            }
            else
            {
                return PartialView("_noticeCategoryUpdatePartial");
            }
        }

        public string AddOrUpdateCategory(NoticeBoardCategory category)
        {
           
                _categoryManager.InsertUpdate(category);
               return "Added Successfully";
           
            
        }

        public string DeleteNoticeCategory(int id)
        {
            if (id != 0)
            {
                var detailObj = _detailsManager.GetAllDetails().Count(s => s.NoticeBoardCategoryId == id);
                if (detailObj==0)
                {
                    _categoryManager.Delete(id);
                    _categoryManager.Save();
                    return "Deleted Successfully";
                }
                else
                {
                    return "This Category have dependency";
                }
            }
            return "An Error Occured";
        }
        /// <summary>
        /// /////strating of Details of notice 
        /// </summary>
        /// <returns></returns>

        public PartialViewResult NoticeDetailsPartial()
        {
            var details = _detailsManager.GetAllDetails();
            return PartialView("_noticeDetailsPartial", details);
        }

        public PartialViewResult NewDetailsPartial(int id = 0)
        {
            if (id != 0)
            {
                var details = _detailsManager.GetSingleDetail(id);
                TempData["noticeBoardCategoryId"] = new SelectList(_categoryManager.GetAllCategories(), "NoticeBoardCategoryId", "CategoryName", details.NoticeBoardCategoryId);

                return PartialView("_noticeDetailsUpdatePartial", details);
            }
            else
            {
                TempData["noticeBoardCategoryId"] = new SelectList(_categoryManager.GetAllCategories(), "NoticeBoardCategoryId", "CategoryName");
                return PartialView("_noticeDetailsUpdatePartial");
            }
        }

        public string AddOrUpdateDetails(NoticeBoardDetail detail)
        {
           _detailsManager.InsertUpdate(detail);
            return "Success";
        }
        public string DeleteNoticeDetails(int id)
        {
            if (id != 0)
            {
                var fileObj = _fileManager.GetAllAttatchedFiles().Count(s => s.NoticeBoardDetailId == id);
                if (fileObj==0)
                {
                    _detailsManager.Delete(id);
                    _detailsManager.Save();
                    return "Deleted Successfully";
                }
                else
                {
                    return "This Details have dependency";
                }
            }
            return "An Error Occured";
        }
        //starting of file attachment of notice
        public PartialViewResult NoticeFileAttachPartial(int id = 0)
        {
            if (id != 0)
            {
                var file = _fileManager.GetAllAttatchedFiles().Where(s => s.NoticeBoardDetailId == id);
                return PartialView("_noticeFileAttachPartial", file.ToList());
            }
            else
            {
                var file = _fileManager.GetAllAttatchedFiles();
                return PartialView("_noticeFileAttachPartial", file.ToList());
            }

        }

        public PartialViewResult NewFilePartial(int did = 0, int id = 0)
        {
            var noticeBoardAttatchedFiles = new NoticeBoardAttatchedFiles();
            if (did != 0 && id != 0)
            {
                noticeBoardAttatchedFiles = _fileManager.GetSingleFile(id);
                noticeBoardAttatchedFiles.NoticeBoardDetailId = did;
            }
            else
            {
                noticeBoardAttatchedFiles.NoticeBoardDetailId = did;
                noticeBoardAttatchedFiles.NoticeBoardAttatchedFilesId = id;
            }
            return PartialView("_noticeFileAttachUpdatePartial", noticeBoardAttatchedFiles);
        }

        public void AttachFile(string fileToken, HttpPostedFileBase attatchedfile)
        {
            if (attatchedfile != null)
            {
                var getFileExtension = Path.GetExtension(attatchedfile.FileName);
                var fileNameFinal = fileToken + getFileExtension;
                var path = Path.Combine(Server.MapPath("~/Content/NoticeContent/"), fileNameFinal);
                attatchedfile.SaveAs(path);
            }
        }

        public JsonResult AddOrUpdateFile(NoticeBoardAttatchedFiles noticefiles, HttpPostedFileBase attatchedfile)
        {
            string msg = "Updated Successfully";
            try
            {
                if (attatchedfile != null && noticefiles.NoticeBoardAttatchedFilesId == 0)
                {
                    
                    _fileManager.Insert(noticefiles);
                    _fileManager.Save();
                    string fileToken =noticefiles.NoticeBoardAttatchedFilesId+"-"+ noticefiles.FileName;
                    AttachFile(fileToken, attatchedfile);

                }
                else
                {
                    if (attatchedfile != null && noticefiles.NoticeBoardAttatchedFilesId != 0)
                    {
                        _fileManager.Update(noticefiles);
                        _fileManager.Save();
                        string fileToken = noticefiles.NoticeBoardAttatchedFilesId + "-" + noticefiles.FileName;
                        AttachFile(fileToken, attatchedfile);

                    }
                    else
                    {
                        msg = "Choose New File To Update Existing File";
                    }

                }
            }
            catch (Exception e)
            {
                msg = "An Error Occured !!!" + e.ToString();
            }
            return new JsonResult()
                    {
                        Data = msg
                    };

        }
        public string DeleteNoticeFile(int id)
        {
            if (id != 0)
            {
                _fileManager.Delete(id);
                _fileManager.Save();
                return "Deleted Successfully";
            }
            return "An Error Occured";
        }
    }
}