﻿using System.Web.Mvc;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO;

namespace UMS_MVC.Controllers.RoleWiseController
{
	public class AdmissionController : Controller
	{
		private readonly AccountManager _accountManager;
		private readonly StudentManager _studentManager;

		public AdmissionController()
		{
			_accountManager=new AccountManager();
			_studentManager=new StudentManager();

		}
		
		public ActionResult Index()
		{
			if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
			var account = (Account)Session["UserObj"];
			if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Admission") return RedirectToAction("NotPermitted", "Home");
			var getUserProfile = _accountManager.GetSingleAccount(account.AccountId);
			
			return View(getUserProfile);
			
		}
		public ActionResult StudentProfilePicture()
		{

			return View();
		}

	}
}