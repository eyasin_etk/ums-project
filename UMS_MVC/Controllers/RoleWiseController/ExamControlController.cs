﻿using System.Web.Mvc;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.Controllers.RoleWiseController
{
    public class ExamControlController : Controller
    {
        // GET: ExamControl
        public ActionResult Index()
        {
            if (Session["UserObj"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "ExamCtrl") return RedirectToAction("NotPermitted", "Home");

            return View(account);
        }
    }
}