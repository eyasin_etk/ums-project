﻿using System.Web.Mvc;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.Controllers.RoleWiseController
{
	public class CoordinatorController : Controller
	{
		public ActionResult Index()
		{
			if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
			var account = (Account)Session["UserObj"];
			if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Coordinator") return RedirectToAction("NotPermitted", "Home");
			//////////////// main code start /////////////////////////////
			return View(account);
			////////////////main code end////////////////////////////////
		}
	}
}