﻿using System;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.BLL;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.OtherSpecialService;

namespace UMS_MVC.Controllers.RoleWiseController
{
    public class ExamSectionController : Controller
    {
        private readonly GradingSystemManager _gradingSystemManager;
        private readonly CourseForStudentManger _courseForStudentManger;
        private readonly CommonAttributeManager _commonAttributeManager;
        private readonly StudentManager _studentManager;
        private readonly MarkSubmitLogicManager _markSubmitLogic;
        private readonly UmsDbContext _db;
        private readonly CourseForDepartmentManager _courseForDepartmentManager;
        private readonly SectionManager _sectionManager;

        public ExamSectionController()
        {
            _gradingSystemManager = new GradingSystemManager();
            _courseForStudentManger = new CourseForStudentManger();
            _commonAttributeManager = new CommonAttributeManager();
            _studentManager = new StudentManager();
            _markSubmitLogic = new MarkSubmitLogicManager();
            _courseForDepartmentManager = new CourseForDepartmentManager();
            _sectionManager = new SectionManager();
            _db = new UmsDbContext();

        }

        public ActionResult SomeImage(string imageName)
        {

            var image = Server.MapPath("~/Image/" + imageName);
            return File(image, "image/jpeg");
        }
        public ActionResult Index()
        {
            if (Session["UserObj"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "ExamCtrl" && account.AccountsRole.AccountType != "Exam") return RedirectToAction("NotPermitted", "Home");

            return View(account);
        }

        public ActionResult ControlCourse(int advId = 0)
        {
            if (Session["UserObj"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "ExamCtrl") return RedirectToAction("NotPermitted", "Home");

            var findAdvising = _courseForStudentManger.GetAdvisingFull(advId);
            var getAllCourseStatuses = _commonAttributeManager.GetallCourseStatuses();
            var getCustomGrades = _commonAttributeManager.GetAllGradesForExtraCourseStatus();
            if (findAdvising != null)
            {
                GradingSystem findGradingSystem = _gradingSystemManager.GetGradingSystemOfSpecificAdvising(findAdvising);

                var getStudentInfo = _studentManager.GetSingleStudentFilteredFnD(findAdvising.StudentIdentificationId);
                TempData["courseStatusId"] = new SelectList(getAllCourseStatuses, "CourseStatusId", "Status", findAdvising.CourseStatusId);
                ViewBag.gradingSystem = findGradingSystem;
                TempData["customGradings"] = new SelectList(getCustomGrades, "CustGradeId", "CustomGrade");
                ViewBag.studentInfo = getStudentInfo;
                ViewBag.courseStats = findAdvising.CourseForDepartment;
                return View(findAdvising);
            }
            return View(new CourseForStudentsAcademic());
        }
        [HttpPost]
        public string CourseAlterCommit(CourseForStudentsAcademic courseForStudentsAcademic, bool custGrade = false, int customGradeId = 0, int courseStatId = 0)
        {
            var sb = new StringBuilder();
            if (Session["UserObj"] == null)
                return sb.Append("<font color='#962b3d'>Session Expired, Log in to continue.</font> </br>").ToString();
            //return "Session Expired, Log in to continue";

            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "ExamCtrl")
                return sb.Append("<font color='#962b3d'>Not Permitted.</font> </br>").ToString();

            var getStudentInfo = _studentManager.GetSingleStudentFilteredFnD(courseForStudentsAcademic.StudentIdentificationId);
            var previousMark = courseForStudentsAcademic.TotalMark;
            var preViousGrade = courseForStudentsAcademic.LetterGrade;
            var gradingSystem = _gradingSystemManager.GetGradingSystemOfSpecificAdvising(courseForStudentsAcademic);
            var checkObj = _markSubmitLogic.CheckBeforeSave(courseForStudentsAcademic, gradingSystem);
            try
            {
                if (checkObj.IsSuccess)
                {
                    var verifiedMarks = _markSubmitLogic.CalculateGradeForAdvisedCourseUpdated(courseForStudentsAcademic, gradingSystem);
                    switch (custGrade)
                    {
                        case true:
                            var getCustomGrades = _commonAttributeManager.GetAllGradesForExtraCourseStatus()
                                   .First(s => s.CustGradeId == customGradeId);
                            if (customGradeId > 0 && (courseStatId == getCustomGrades.StatusId))
                            {
                               if (getCustomGrades.CancelSection)
                                {
                                    verifiedMarks.SectionId = null;
                                }
                                verifiedMarks.CourseStatusId = courseStatId;
                                verifiedMarks.LetterGrade = getCustomGrades.CustomGrade;
                                _markSubmitLogic.UpdateMarkModified(verifiedMarks);
                                if (account.AccountsRole.AccountType == "ExamCtrl")
                                {
                                    SaveGradeHistory(verifiedMarks, previousMark, preViousGrade, getStudentInfo.StudentInfo.StudentId);
                                }


                                sb.Append("<font color='#00bb39'>Mark Updated Successfully.</font> </br>");
                            }
                            else
                            {

                                sb.Append("<font color='#962b3d'>Cutom Grade and Course Status does not match. Please Insert properly!</font> </br>").ToString();
                            }
                            break;
                        case false:
                            _markSubmitLogic.UpdateMarkModified(verifiedMarks);
                            if (account.AccountsRole.AccountType == "ExamCtrl")
                            {
                                SaveGradeHistory(verifiedMarks, previousMark, preViousGrade, getStudentInfo.StudentInfo.StudentId);
                            }
                            sb.Append("<font color='#00bb39'>Mark Updated Successfully.</font> </br>");
                            break;
                    }
                }
                else
                {
                    sb.Append("<font color='#962b3d'>" + checkObj.Message + "</font> </br>");
                }
            }
            catch (DbException)
            {

                sb.Append("<font color='#962b3d'>Internal Error, Contact to Software Engineer about this problem!</font> </br>");
            }
            return sb.ToString();

        }

        public PartialViewResult CalculateResults(double att, double ct, double mid, double finall, int gradingId, int courseId)
        {
            if (att >= 0 && ct >= 0 && mid >= 0 && finall >= 0 && gradingId >= 0)
            {
                var aCourse = new CourseForStudentsAcademic
                {
                    Attendance = att,
                    ClassTest = ct,
                    Midterm = mid,
                    FinalTerm = finall,
                    GradingSystemId = gradingId,
                    CourseForDepartmentId = courseId

                };
                var findGradingSystem = _gradingSystemManager.FindGradingSystem(gradingId);
                var calculatedMarks = _markSubmitLogic.CalculateGradeForAdvisedCourseUpdated(aCourse, findGradingSystem);
                return PartialView("_calculateMarkPartial", calculatedMarks);
            }
            return PartialView("_calculateMarkPartial", new CourseForStudentsAcademic());
        }
        private void SaveGradeHistory(CourseForStudentsAcademic findCourseForStudent, double previousMark, string preViousGrade, string studentId)
        {
            var account = (Account)Session["UserObj"];

            var gradeUploadByExam = new GradingHistory
            {
                CourseForStudentsAcademicId = findCourseForStudent.CourseForStudentsAcademicId,
                ChangedBy = account.LoginIdentity,
                ChangedTime = DateTime.Now,
                PreviousMark = previousMark,
                PreviousGrade = preViousGrade,
                StudentId = studentId,
                ChangedMark = findCourseForStudent.TotalMark,
                ChangedGrade = findCourseForStudent.LetterGrade,
                ComputerIp = ClassifiedServices.GetUserIp()
            };
            _markSubmitLogic.SaveMarkSubmitBySpecifiedUser(gradeUploadByExam);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}