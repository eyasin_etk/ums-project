﻿using System.Linq;
using System.Web.Mvc;
using UMS_MVC.DataModel.Connection;

namespace UMS_MVC.Controllers.RoleWiseController
{
    public class AccountOfficerController : Controller
    {
        private UmsDbContext db = new UmsDbContext();
        public ActionResult Index()
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string user = Session["User"].ToString();
            string role = Session["Role"].ToString();
            if (role != "Admin" && role != "Accounts Officer") return RedirectToAction("NotPermitted", "Home");
            var getAdminProfile = db.Accounts.Single(s => s.LoginIdentity == user);
            //////////////// main code start /////////////////////////////
            return View(getAdminProfile);
            ////////////////main code end////////////////////////////////
        }
	}
}