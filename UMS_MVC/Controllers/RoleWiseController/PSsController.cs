﻿using System.Web.Mvc;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO;

namespace UMS_MVC.Controllers.RoleWiseController
{
    public class PSsController : Controller
    {
        private readonly AccountManager _accountManager=new AccountManager();
        
        public ActionResult Index()
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account) Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Ps") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            return View(account);
            ////////////////main code end////////////////////////////////
        }
	}
}