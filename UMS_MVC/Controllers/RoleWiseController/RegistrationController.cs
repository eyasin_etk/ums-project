﻿using System.Linq;
using System.Web.Mvc;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.Controllers.RoleWiseController
{
    public class RegistrationController : Controller
    {
        private UmsDbContext db = new UmsDbContext();
      
        public ActionResult Index()
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Advisor") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            var findSpecific = db.Teachers.Where(s => s.LoginId == account.LoginIdentity);
            if (findSpecific.Any())
            {
                var findTeacher = db.Teachers.Single(s => s.LoginId == account.LoginIdentity);
                var getTakencourses = db.Sections.Where(s => s.TeacherId == findTeacher.TeacherId);
                ViewData["TakenCourses"] = getTakencourses;
                return View(findTeacher);
            }
            TempData["msgTeacherNotFound"] = "Sorry ! your Teacher(Advisor) Profile may not be created please contact to Software section";
            return RedirectToAction("LogInresultSubmit", "Home");
            ////////////////main code end////////////////////////////////
        }
	}
}