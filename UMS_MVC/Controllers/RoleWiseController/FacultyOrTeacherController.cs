﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using UMS_MVC.Controllers.OtherSpecialControllers;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO;

namespace UMS_MVC.Controllers.RoleWiseController
{
    public class FacultyOrTeacherController : Controller
    {
        private UmsDbContext db = new UmsDbContext();
        private OtherController otherCtrl=new OtherController();
        private readonly CourseForStudentManger _courseForStudentManger=new CourseForStudentManger();
        private readonly SemesterManager _semesterManager=new SemesterManager();
        private readonly SectionManager _sectionManager=new SectionManager();

        public ActionResult Index()
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Teacher" && account.AccountsRole.AccountType != "Advisor" && account.AccountsRole.AccountType != "Support") return RedirectToAction("NotPermitted", "Home");

            //////////////// main code start /////////////////////////////
            var findSpecific = otherCtrl.GetTeachers().Where(s => s.LoginId == account.LoginIdentity);
            if (findSpecific.Any())
            {
                var findTeacher = otherCtrl.GetTeachers().Single(s => s.LoginId == account.LoginIdentity);
                //var getTakencourses =_sectionManager.GetAllSections().Where(s => s.TeacherId == findTeacher.TeacherId);
                //ViewData["TakenCourses"] = getTakencourses;
                return View(findTeacher);
            }
            TempData["msgTeacherNotFound"] = "Sorry ! your Teacher Profile may not be created please contact to Software section";
            return RedirectToAction("LogInresultSubmit", "Home");

        }

        public ActionResult SectionIndex(int? SemesterNYear)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Teacher" && account.AccountsRole.AccountType != "Advisor" && account.AccountsRole.AccountType != "Support") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            
            if (account.AccountsRole.AccountType == "Admin")
            {
                SortSemesterForAdmin(SemesterNYear);
            }
            else
            {
                if (_sectionManager.GetAllSectionForTeachers(account.LoginIdentity).Count > 0)
                {
                    SortSemesterForTeacher(SemesterNYear);
                }
                else
                {
                    TempData["msg"] = "No Section";
                    return RedirectToAction("Index");
                }
                

            }
            return View();
        }

        public void SortSemesterForAdmin(int? SemesterNYear)
        {
            var getAllsectionsCustom = _sectionManager.GetAllSectionsByCourseCodewise();
            var findActiveSemester = _semesterManager.GetActiveSemester();
            if (SemesterNYear > 0)
            {

                TempData["SemesterNYear"] = new SelectList(_semesterManager.GetAllSemester(), "SemesterId", "SemesterNYear", SemesterNYear);
                TempData["Sections"] = new SelectList(getAllsectionsCustom.Where(s => s.SemesterId == SemesterNYear), "SectionId", "SectionName");
            }
            else
            {
                TempData["SemesterNYear"] = new SelectList(_semesterManager.GetAllSemester(), "SemesterId", "SemesterNYear", findActiveSemester.SemesterId);
                TempData["Sections"] = new SelectList(getAllsectionsCustom.Where(s => s.SemesterId == findActiveSemester.SemesterId), "SectionId", "SectionName");
            }
        }

        public void SortSemesterForTeacher(int? SemesterNYear)
        {
            var account = (Account)Session["UserObj"];
            var getSectionByTeacher = _sectionManager.GetAllSectionForTeachers(account.LoginIdentity);
            var selectedSemester = new List<Semester>();
            var findActiveSemester = _semesterManager.GetActiveSemester();
           
            foreach (var section in getSectionByTeacher.DistinctBy(s => s.SemesterId))
            {
                var semester = new Semester { SemesterId = section.SemesterId, SemesterNYear = section.Semester.SemesterNYear, ActiveSemester = section.Semester.ActiveSemester };
                selectedSemester.Add(semester);
            }
            if (SemesterNYear > 0)
            {

                TempData["SemesterNYear"] = new SelectList(selectedSemester, "SemesterId", "SemesterNYear", SemesterNYear);
                TempData["Sections"] = new SelectList(getSectionByTeacher.Where(s => s.SemesterId == SemesterNYear), "SectionId", "SectionName");
            }
            else
            {
                if (getSectionByTeacher.Count(s => s.SemesterId == findActiveSemester.SemesterId) > 0)
                {
                    TempData["SemesterNYear"] = new SelectList(selectedSemester, "SemesterId", "SemesterNYear", findActiveSemester.SemesterId);
                    TempData["Sections"] = new SelectList(getSectionByTeacher.Where(s => s.SemesterId == findActiveSemester.SemesterId), "SectionId", "SectionName");
                }
                var getLastSemeserHeattented = getSectionByTeacher.OrderByDescending(s=>s.SemesterId).First().SemesterId;
                TempData["SemesterNYear"] = new SelectList(selectedSemester, "SemesterId", "SemesterNYear", getLastSemeserHeattented);
                TempData["Sections"] = new SelectList(getSectionByTeacher.Where(s => s.SemesterId == getLastSemeserHeattented), "SectionId", "SectionName");
            }
        }

        public string GradeUplodadNotification()
        {
            var semesterInfo = _semesterManager.GetActiveSemester();
            var finalNotification = "deactivated.";
            if (semesterInfo.MidTerm)
            {
                finalNotification = "activated for Mid Term, " + semesterInfo.SemesterNYear + ".";
            }
            if (semesterInfo.FinalTerm)
            {
                finalNotification = "activated for FinalTerm, " + semesterInfo.SemesterNYear + ".";
            }
            return finalNotification;
        }

        public PartialViewResult StudentListBySection(int sectionId=0)
        {
            var account = (Account) Session["UserObj"];
            var getStudentListBySection = _courseForStudentManger.GetAllCourseForStudentsAcademicsBysection(sectionId);
            return PartialView("_redirectToPortalNotifiy");
           // ViewBag.gradeuploadNoti = GradeUplodadNotification();
            //if (account.AccountsRole.AccountType == "Admin")
            //{
            //    return PartialView("_StudentListOfaSectionDefault", getStudentListBySection);
            //}
            //if (CheckTeacherWiseSection(sectionId, account.LoginIdentity))
            //{
            //    var getCourseType = _sectionManager.GetSingleSection(sectionId).CourseForDepartment.CourseType.ToUpper();
            //    //var getCourseType = getStudentListBySection.First().CourseForDepartment.CourseType;
            //    if (getCourseType == "LAB")
            //    {
            //        return PartialView("_studentListOfaSectionLab", getStudentListBySection);
            //    }
            //    if (getCourseType == "OTHER")
            //    {
            //        return PartialView("_studentListOfSectionOther", getStudentListBySection);
            //    }
            //    else
            //    {
            //        return PartialView("_StudentListOfaSectionDefault", getStudentListBySection);
            //    }
                
            //}
            //var demoData = new List<CourseForStudentsAcademic>();
            //return PartialView("_StudentListOfaSectionDefault",demoData);
        }

        public ActionResult GradeUpload(int sectionId=0)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
           // if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Teacher" && account.AccountsRole.AccountType != "Advisor" && account.AccountsRole.AccountType != "ExamCtrl") return RedirectToAction("NotPermitted", "Home");
            if (account.AccountsRole.AccountType != "Admin" ) return RedirectToAction("NotPermitted", "Home");
            //...Main Code
           
            if (sectionId > 0)
            {
                TempData["sectionId"] = sectionId;
                var studentList =
                    _courseForStudentManger.GetAllCourseForStudentsAcademicsBysection(sectionId);

                if (account.AccountsRole.AccountType == "Admin")
                {
                    GetAccessForAdmins(sectionId);
                    return View(studentList.ToList());
                }
                if (account.AccountsRole.AccountType == "ExamCtrl")
                {
                    GetAccessForExamController(sectionId);
                    return View(studentList.ToList());
                }
                if (account.AccountsRole.AccountType == "Teacher" || account.AccountsRole.AccountType == "Advisor")
                {
                    if (AccessToHighLightedSection(sectionId))
                    {
                        if (CheckTeacherWiseSection(sectionId, account.LoginIdentity))
                        {
                            return View(studentList.ToList());
                        }
                        return RedirectToAction("NotPermitted", "Home");
                    }
                    if (AccessTograde(sectionId))
                    {
                        if (CheckTeacherWiseSection(sectionId, account.LoginIdentity))
                        {
                            return View(studentList.ToList());
                        }
                        return RedirectToAction("NotPermitted", "Home");
                    }
                }
               return View("~/Views/Home/ErrorPage.cshtml");
                //return RedirectToAction("GradeUploadDisabled","Home");
            }
            return View();
        }

        public bool AccessToHighLightedSection(int sectionId)
        {
            var findSection = db.Sections.Find(sectionId);
            var today =DateTime.Now;
            if (findSection.HighLight)
            {
                if (findSection.ExpireDateTime == null)
                {
                    TempData["GradeUploadStatus"] = "Grade upload option for this section will be open soon...";
                }
                if (findSection.ExpireDateTime != null && findSection.ExpireDateTime < today.Date)
                {
                    TempData["GradeUploadStatus"] = "Sorry, Grade upload for this section expired on " + findSection.ExpireDateTime;
                }
                if (findSection.ExpireDateTime >= today.Date)
                {
                    string termAccess = String.Empty;
                    string courseType = "CORE/Elective";
                    if (findSection.MidTermEx || findSection.FinalTermEx)
                    {
                        
                        if (findSection.MidTermEx && findSection.FinalTermEx)
                        {
                            termAccess = "admin";
                        }
                        if (findSection.MidTermEx && !findSection.FinalTermEx)
                        {
                            termAccess = "mid";
                        }
                        if (!findSection.MidTermEx && findSection.FinalTermEx)
                        {
                            termAccess = "final";
                        }

                        if (findSection.CourseForDepartment.CourseType.ToUpper() == "LAB")
                        {
                            courseType = "LAB";
                        }
                        if (findSection.CourseForDepartment.CourseType.ToUpper() == "OTHER")
                        {
                            courseType = "OTHER";
                        }
                        TempData["GradeControl"] = termAccess;
                        TempData["CourseType"] = courseType;
                        return true;
                    }
                    return false;
                }
                return false;
            }
            return false;
        }

        public void GetAccessForExamController(int sectionId)
        {
            var findSection = db.Sections.Find(sectionId);
            TempData["GradeControl"] = "admin";
            TempData["CourseType"] = "CORE/Elective";
            if (findSection.CourseForDepartment.CourseType.ToUpper() == "LAB")
            {
                TempData["CourseType"] = "LAB";
            }
            if (findSection.CourseForDepartment.CourseType.ToUpper() == "OTHER")
            {
                TempData["CourseType"] = "OTHER";
            }
        }

        public void GetAccessForAdmins(int sectionId)
        {
            var findSection = db.Sections.Find(sectionId);
            TempData["GradeControl"] = "admin";
            TempData["CourseType"] = "CORE/Elective";
            if (findSection.CourseForDepartment.CourseType.ToUpper() == "LAB")
            {
                TempData["CourseType"] = "LAB";
            }
            if (findSection.CourseForDepartment.CourseType.ToUpper() == "OTHER")
            {
                TempData["CourseType"] = "OTHER";
            }
        }

        public bool AccessTograde(int sectionId)
        {
            var findSection = db.Sections.Find(sectionId);
            var gradeUploadStatus = _semesterManager.GetActiveSemester();
            string msg = "Grade upload is currently disabled or try current semester sections.";
            if (findSection.SemesterId != gradeUploadStatus.SemesterId)
            {
                TempData["GradeUploadStatus"] = msg;
                return false;
            }
            if (!gradeUploadStatus.MidTerm && !gradeUploadStatus.FinalTerm)
            {
                TempData["GradeUploadStatus"] = msg;
                return false;
            }
            
            if (gradeUploadStatus.MidTerm)
            {
                 TempData["GradeControl"] = "mid";
            }
            else
            {
                TempData["GradeControl"] = "final";
            }
            TempData["CourseType"] = "CORE/Elective";
            if (findSection.CourseForDepartment.CourseType.ToUpper() == "LAB")
            {
                TempData["CourseType"] = "LAB";
            }
            if (findSection.CourseForDepartment.CourseType.ToUpper() == "OTHER")
            {
                TempData["CourseType"] = "OTHER";
            }
            return true;
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitGrade(CourseForStudentsAcademic findCourseForStudent)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            //if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Teacher" && account.AccountsRole.AccountType != "Advisor" && account.AccountsRole.AccountType != "ExamCtrl") return RedirectToAction("NotPermitted", "Home");
            //.....Main Code
            if (account.AccountsRole.AccountType != "Admin" ) return RedirectToAction("NotPermitted", "Home");
            if (CheckGradeValidation(findCourseForStudent))
            {
                var findCourse = db.CourseForStudentsAcademics.Find(findCourseForStudent.CourseForStudentsAcademicId);
                var previousMark = findCourse.TotalMark;
                var preViousGrade = findCourse.LetterGrade;
                findCourse.Attendance = findCourseForStudent.Attendance;
                findCourse.ClassTest = findCourseForStudent.ClassTest;
                findCourse.Midterm = findCourseForStudent.Midterm;
                findCourse.FinalTerm = findCourseForStudent.FinalTerm;
                var getGrade= otherCtrl.CalculateGrade(findCourse);
                findCourse.Grade = getGrade.Grade;
                findCourse.TotalMark = getGrade.TotalMark;
                findCourse.LetterGrade = getGrade.LetterGrade;
                findCourse.TotalGrade = getGrade.TotalGrade;
                db.Entry(findCourse).State = EntityState.Modified;
                db.SaveChanges();
                TempData["gradeForCourseId"] = findCourseForStudent.CourseForStudentsAcademicId;
                TempData["gradeUploadMsg"] = "1";
                if (account.AccountsRole.AccountType == "ExamCtrl")
                {
                    SaveGradeHistory(findCourse,previousMark,preViousGrade);
                   
                }
            }
            else
            {
                TempData["gradeForCourseId"] = findCourseForStudent.CourseForStudentsAcademicId;
                TempData["gradeUploadMsg"] = "2";
            }
            if (Session["RequestUrlToGrageUpload"] != null)
            {
                return Redirect(Session["RequestUrlToGrageUpload"].ToString());
            }
            return RedirectToAction("Index", "FacultyOrTeacher");          
        }
        private string GetUserIp()
        {
            HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }
            return context.Request.ServerVariables["REMOTE_ADDR"];
        }
        private void SaveGradeHistory(CourseForStudentsAcademic findCourseForStudent, double previousMark, string preViousGrade)
        {
            var account = (Account)Session["UserObj"];

            var gradeUploadByExam = new GradingHistory
            {
                CourseForStudentsAcademicId = findCourseForStudent.CourseForStudentsAcademicId,
                ChangedBy = account.LoginIdentity,
                ChangedTime = DateTime.Now,
                PreviousMark = previousMark,
                PreviousGrade = preViousGrade,
                StudentId = findCourseForStudent.StudentIdentification.StudentId,
                ChangedMark = findCourseForStudent.TotalMark,
                ChangedGrade = findCourseForStudent.LetterGrade,
                ComputerIp = GetUserIp()
            };
            db.GradingHistories.Add(gradeUploadByExam);
            db.SaveChanges();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitGradeForLab(CourseForStudentsAcademic findCourseForStudent)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            //if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Teacher" && account.AccountsRole.AccountType != "Advisor" && account.AccountsRole.AccountType != "Advisor" && account.AccountsRole.AccountType != "ExamCtrl") return RedirectToAction("NotPermitted", "Home");
            if (account.AccountsRole.AccountType != "Admin") return RedirectToAction("NotPermitted", "Home");
            //.....Main Code
            if (CheckGradeValidation(findCourseForStudent))
            {
                var findCourse = db.CourseForStudentsAcademics.Find(findCourseForStudent.CourseForStudentsAcademicId);
                var previousMark = findCourse.TotalMark;
                var preViousGrade = findCourse.LetterGrade;
                findCourse.Attendance = 0;
                findCourse.ClassTest = 0;
                findCourse.Midterm = findCourseForStudent.Midterm;
                findCourse.FinalTerm = findCourseForStudent.FinalTerm;
                var getGrade = otherCtrl.CalculateGrade(findCourse);
                findCourse.Grade = getGrade.Grade;
                findCourse.TotalMark = getGrade.TotalMark;
                findCourse.LetterGrade = getGrade.LetterGrade;
                findCourse.TotalGrade = getGrade.TotalGrade;

                db.Entry(findCourse).State = EntityState.Modified;
                db.SaveChanges();
                TempData["gradeForCourseId"] = findCourseForStudent.CourseForStudentsAcademicId;
                TempData["gradeUploadMsg"] = "1";
                if (account.AccountsRole.AccountType == "ExamCtrl")
                {
                    SaveGradeHistory(findCourse, previousMark, preViousGrade);

                }
            }
            else
            {
                TempData["gradeForCourseId"] = findCourseForStudent.CourseForStudentsAcademicId;
                TempData["gradeUploadMsg"] = "2";
            }
            if (Session["RequestUrlToGrageUpload"] != null)
            {
                return Redirect(Session["RequestUrlToGrageUpload"].ToString());
            }
            return RedirectToAction("Index", "FacultyOrTeacher");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitGradeForOther(CourseForStudentsAcademic findCourseForStudent)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            //if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Teacher" && account.AccountsRole.AccountType != "Advisor" && account.AccountsRole.AccountType != "Advisor" && account.AccountsRole.AccountType != "ExamCtrl") return RedirectToAction("NotPermitted", "Home");
            if (account.AccountsRole.AccountType != "Admin" ) return RedirectToAction("NotPermitted", "Home");
            //.....Main Code
            if (CheckGradeValidation(findCourseForStudent))
            {
                var findCourse = db.CourseForStudentsAcademics.Find(findCourseForStudent.CourseForStudentsAcademicId);
                var previousMark = findCourse.TotalMark;
                var preViousGrade = findCourse.LetterGrade;
                findCourse.Attendance = 0;
                findCourse.ClassTest = 0;
                findCourse.Midterm = 0;
                findCourse.FinalTerm = findCourseForStudent.FinalTerm;
                var getGrade = otherCtrl.CalculateGrade(findCourse);
                findCourse.Grade = getGrade.Grade;
                findCourse.TotalMark = getGrade.TotalMark;
                findCourse.LetterGrade = getGrade.LetterGrade;
                findCourse.TotalGrade = getGrade.TotalGrade;

                db.Entry(findCourse).State = EntityState.Modified;
                db.SaveChanges();
                TempData["gradeForCourseId"] = findCourseForStudent.CourseForStudentsAcademicId;
                TempData["gradeUploadMsg"] = "1";
                if (account.AccountsRole.AccountType == "ExamCtrl")
                {
                    SaveGradeHistory(findCourse, previousMark, preViousGrade);

                }
            }
            else
            {
                TempData["gradeForCourseId"] = findCourseForStudent.CourseForStudentsAcademicId;
                TempData["gradeUploadMsg"] = "2";
            }
            if (Session["RequestUrlToGrageUpload"] != null)
            {
                return Redirect(Session["RequestUrlToGrageUpload"].ToString());
            }
            return RedirectToAction("Index", "FacultyOrTeacher");
        }

        public bool CheckGradeValidation(CourseForStudentsAcademic cfsa)
        {
            if((cfsa.Attendance >= 0 && cfsa.Attendance < 11) && (cfsa.ClassTest >= 0 && cfsa.ClassTest < 51) &&
                (cfsa.Midterm >= 0 && cfsa.Midterm < 101) && (cfsa.FinalTerm >= 0 && cfsa.FinalTerm < 101))
            {
                return true;
            }
            return false;
        }

        public bool CheckTeacherWiseSection(int? sectionId, string logindentity)
        {
            var getAllsections = _sectionManager.GetAllSections().Count(s => s.SectionId == sectionId && s.Teacher.LoginId == logindentity);
            if (getAllsections == 1)
            {
                return true;
            }
            return false;
        }

    }
}