﻿using System.Web.Mvc;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO;

namespace UMS_MVC.Controllers.RoleWiseController
{

    public class SystemSupportController : Controller
    {
        private readonly AccountManager _accountManager;

        public SystemSupportController()
		{
			_accountManager=new AccountManager();

		}
        // GET: SystemSupport
        public ActionResult Index()
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Support") return RedirectToAction("NotPermitted", "Home");
            var getUserProfile = _accountManager.GetSingleAccount(account.AccountId);
            return View(getUserProfile);
        }
        public ActionResult ActivityLog()
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Support") return RedirectToAction("NotPermitted", "Home");
            return View();
        }
    }
}