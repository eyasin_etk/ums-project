﻿using System.Web.Mvc;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.Controllers
{
    public class AdvancedMasterController : Controller
    {
        // GET: AdvancedMaster
        public ActionResult Index()
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType == "Admin")
            {
                ViewBag.msg = true;
            }
            if (account.AccountsRole.AccountType == "Admission")
            {
                ViewBag.msg = false;
            }
            if (account.AccountsRole.AccountType != "Admin"&& account.AccountsRole.AccountType != "Admission")
                return RedirectToAction("NotPermitted", "Home");
            return View();
        }
        public PartialViewResult NoticeboardCatManager()
        {
            return PartialView("~/views/advancedmaster/notice/views/_noticeBoardCategory.cshtml");
        }
        public PartialViewResult NoticeboardDetaManager()
        {
            return PartialView("~/views/advancedmaster/notice/views/_noticeBoardDetail.cshtml");
        }
        public PartialViewResult EvaluationFilteredResult()
        {
            return PartialView("~/views/advancedmaster/evaluation/views/_evaluationFilteredResultPartial.cshtml");
        }

       
    }
}