﻿using System;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using UMS_MVC.Controllers.OtherSpecialControllers;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.DataModel.Models.Login_History;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.Manager.DAL.DAO.Intefaces;
using UMS_MVC.Manager.DAL.DAO.Repository;
using UMS_MVC.OtherSpecialService;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.Controllers
{
    public class HomeController : Controller
    {
        
        private readonly StudentManager _studentManger;
        private readonly TeacherManager _teacherManager;
        private readonly AccountManager _accountManager;
        private readonly SectionManager _sectionManager;
        private readonly RebateManager _rebateManager;
        private readonly SemesterManager _semesterManager;
        private readonly CourseForDepartmentManager _courseForDepartmentManager;
        private readonly CourseForStudentManger _courseForStudentManger;
        private readonly LoginHistoryManager _loginHistoryManager;
        private readonly OtherStatus _otherStatus;
        private readonly UserActivityManager _activityManager;
        private readonly DepartmentManager _departmentManager;
        private readonly PaymentManager _paymentManager;
        private readonly IControlFeatureRepository _featureRepository;

        public HomeController()
        {
            _studentManger = new StudentManager();
            _teacherManager = new TeacherManager();
            _accountManager = new AccountManager();
            _sectionManager = new SectionManager();
            _rebateManager = new RebateManager();
            _semesterManager = new SemesterManager();
            _courseForDepartmentManager = new CourseForDepartmentManager();
            _courseForStudentManger = new CourseForStudentManger();
            _loginHistoryManager = new LoginHistoryManager();
            _otherStatus = new OtherStatus();
            _activityManager = new UserActivityManager();
            _departmentManager=new DepartmentManager();
            _paymentManager=new PaymentManager();
            _featureRepository=new ControlFeatureRepository();
            
        }

        public ActionResult Index(int? departmentId)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin") return RedirectToAction("NotPermitted", "Home");
            
            //////////////// main code start /////////////////////////////
            int nameIndex = account.Name.LastIndexOf(' ');
            ViewBag.UserName = account.Name.Substring(nameIndex + 1)+"'s";
            var getActiveSemester = _semesterManager.GetActiveSemesterFiltered();
            TempData["departmentId"] = new SelectList(_departmentManager.GetAllDepartmentForDropdown(), "DepartmentId", "DepartmentName");
            var findActiveSemester = _semesterManager.GetActiveSemester();
            TempData["semester"] = new SelectList(_semesterManager.GetAllSemesterForDropdown(), "SemesterId", "SemesterNYear",findActiveSemester.SemesterId);

            string termActivatiomMsg = "";
            TempData["currentSemester"] = getActiveSemester.SemesterNYear;
            ViewBag.courseAdvisingSemester = "Disabled";
            Semester semester = _semesterManager.GetAdvisingActivatedSemester();
            if (semester != null)
            {
                ViewBag.courseAdvisingSemester = semester.SemesterNYear+" - Activated";
            }
            
            ViewBag.Access = false;
            if (account.AccountId == 1)
            {
                ViewBag.Access = true;
            }
            if (getActiveSemester.FinalTerm)
            {
                 termActivatiomMsg= "Final Term ";
            }
            else if(getActiveSemester.MidTerm)
            {
                termActivatiomMsg = "Mid Term ";
            }
            else
            {
                termActivatiomMsg= "Grade upload Disabled";
            }
            if (getActiveSemester.SpecialGradeuploadDeadLine != null)
            {
                DateTime expireDate = (DateTime)getActiveSemester.SpecialGradeuploadDeadLine;
                termActivatiomMsg += " Expire Date (" + expireDate.Date.ToString("d") + ")";
            }
            TempData["currentTerm"] = termActivatiomMsg;
            return View();
            ////////////////main code end////////////////////////////////
        }

        public int CountAllInvalidAdvisingInCurrentSemester()
        {
            return Enumerable.Count<CourseForStudentsAcademic>(_courseForStudentManger.GetAllCourseForStudentsAcademicsFiltered()
                    .DistinctBy(s => s.StudentIdentificationId), s => s.CourseStatusId == 1 && s.SemesterId == _semesterManager.GetActiveSemesterFiltered().SemesterId);
        }

        public int CountAllInvalidAdvisingInAllSemester()
        {
            return Enumerable.Count<CourseForStudentsAcademic>(_courseForStudentManger.GetAllCourseForStudentsAcademicsFiltered()
                  .DistinctBy(s => s.StudentIdentificationId), s => s.CourseStatusId == 1);
        }

        public int CountAllRebatedStudent()
        {
            return Enumerable.Count<Rebate>(_rebateManager.GetAllRebatesFiltered().DistinctBy(s => s.StudentIdentificationId));
        }

        public int CountAllAccount()
        {
            return _accountManager.GetAllAccountsFiltered().Count();
        }

        public int CountAllTeachers()
        {
            return _teacherManager.GetAllTeacherFiltered().Count();
        }

        public int CountAllPaymentRegistration()
        {
            var getSemesterId = _semesterManager.GetActiveSemesterFiltered().SemesterId;
            return _paymentManager.GetAllPaymentRegistration().Count(s => s.SemesterId == getSemesterId);
        }

        public int CountTotalStudentBySemester()
        {
            var getSemesterId = _semesterManager.GetActiveSemesterFiltered().SemesterId;
            return _studentManger.GetAllStudentBySemesterFiltered(getSemesterId)
                .Count();
        }

        public int CountTotalSections()
        {
            return _sectionManager.GetAllSectionsFiltered().Count();
        }
        
        public int CountAdmittedToday()
        {
            return _studentManger.GetAllStudentIdentifications().Count(s => DbFunctions.TruncateTime(s.AddedDate) == DateTime.Today);
        }

        public int CountTotalStudentAdmitted()
        {
            var countStudent = _studentManger.GetAllStudentIdentifications().Count();
            return countStudent;
        }

        public int CountLoginInUms()
        {
            var currentTime = DateTime.Now.ToString("d");
            var count= _loginHistoryManager.GetAllLoginHistoriesByTime(currentTime).Count();
            return count;
        }

        public PartialViewResult GetRegisteredStudents(int departmentId=0,int semester=0)
        {
            //TempData["departmentId"] = new SelectList(db.Departments, "DepartmentId", "DepartmentName");
            
           ViewData["Sectionlist"] = _sectionManager.GetAllSections().Where(s => s.CourseForDepartment.DepartmentId == departmentId);
           ViewData["CourseList"] = _courseForDepartmentManager.GetAllCourseForDepartmentsByDepartment(departmentId);
            if (departmentId > 0)
            {
                var getCourseList = _courseForStudentManger.GetAllCourseForStudentsAcademicsByDeptAndSemester(departmentId,semester);
                int countSt = getCourseList.Count();
                ViewBag.csa = countSt;
                return PartialView("_CourseListWithCalculation2", getCourseList.ToList());
            }
            return PartialView("_CourseListWithCalculation2");
        }
        public ActionResult Download(string fn)
        {
            return File(Path.Combine("~/Downloadables/", "ChromeStandaloneSetup.exe"), "application/octet-stream", "ChromeStable35.exe");
        }
       
        public ActionResult GetTotalStudentCount(int courseId=0)
        {
            if (courseId > 0)
            {
                var getTotal = _courseForStudentManger.GetAllCourseForStudentsAcademicsByCourseId(courseId).Count();
                ViewBag.total = getTotal;
                return View();
            }
            ViewBag.total = 0;
            return View();
        }

        public ActionResult LogInresultSubmit()
        {
            Session.Abandon();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> LogInresultSubmit(LoginInitial aaccount)
        {
            if (Session["Role"] != null && Session["User"] != null)
            {
                Session.Abandon();
            }
            //var account = _accountManager.GetAllAccountsFiltered().Where(s=>s.LoginIdentity==aaccount.LoginId && s.Password==aaccount.Password);
            var account =await _accountManager.FindAccountByLogInId(aaccount);
            if (account!=null)
            {
                //var getPerson = account.FirstOrDefault();
                if (account.Deactivate)
                {
                    ViewBag.msg = "Sorry, You Account has been Disabled. Contact UMS Section for Account Activation.";
                    return View();
                }

                if (account.Status)
                {
                    var urlTogg = "http://123.136.27.58/umsapi/api/";
                    string role = account.AccountsRole.AccountType;
                    string userName = account.Name;
                    Session.Add("UserName", userName);
                    Session.Add("User", account.LoginIdentity);
                    Session.Add("Role", role);
                    Session.Add("UserObj", account);
                    Session.Add("LayoutMgr", OtherController.LayoutManager(role));
                    Session.Add("ServiceUrl",urlTogg);

                    LoginHistorySaver(account);
                    switch (role)
                    {
                        case "Admin":
                            return RedirectToAction("Index", "Home");
                        case "Admission":
                            return RedirectToAction("Index", "Admission");
                        case "Advisor":
                            return RedirectToAction("Index", "FacultyOrTeacher");
                        case "Teacher":
                            return RedirectToAction("Index", "FacultyOrTeacher");
                        case  "Accounts Officer":
                            return RedirectToAction("Index", "AccountOfficer");
                        case "Ps":
                            return RedirectToAction("Index", "PSs");
                        case "Coordinator":
                            return RedirectToAction("Index", "Coordinator");
                        case "Exam":
                            return RedirectToAction("Index", "ExamSection");
                        case "ExamCtrl":
                            return RedirectToAction("Index", "ExamControl");
                        case "Support":
                            return RedirectToAction("Index", "SystemSupport");
                    }
                }
                ViewBag.msg = "UMS is doing some crucial improvement, log In is disabled right now. Try again later. Thank You";
                return View();
            }
            ViewBag.msg = "Id or Password is Incorrect";
            return View();
        }

        public void LoginHistorySaver(Account account)
        {
            
            string currentTime = Convert.ToString(DateTime.Now);
            var browserInfo = Request.Browser;
            string deviceType = "Desktop";
            if (browserInfo.IsMobileDevice)
            {
                deviceType = "Mobile :: " + browserInfo.MobileDeviceModel;
            }
            var loga = new LoginHistory
            {
                AccountId = account.AccountId,EntryTime = currentTime,
                LastVisitedPage = "",
                PcAddress = ClassifiedServices.GetUser_IP(),
                Browser = browserInfo.Browser+" v"+browserInfo.Version,
                DeviceType = deviceType
            };
            _loginHistoryManager.InsertLoginHistory(loga);
        }
        
        public ActionResult LogoutAction()
        {
            Session.Abandon();
            return RedirectToAction("LogInresultSubmit");
        }

        public ActionResult GradeUploadDisabled()
        {
            TempData["GradeUploadMsg"] = "Grade upload is currently disabled or try current semester sections.";
            return RedirectToAction("ErrorPage");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult PatchNotes()
        {
            return View();
        }
        public ActionResult NotPermitted()
        {
            return View();
        }
        public ActionResult TeacherResult()
        {
            return View();
        }
        public ActionResult ErrorPage()
        {
            return View();
        }
        public string GetActivedSemesterName()
        {
            return _otherStatus.GetSemesterStatus().ActiveSemester;
        }
        public string GetTermName()
        {
            return _otherStatus.GetSemesterStatus().GradeUploadTerm;
        }
        public string GetAdvisingStatus()
        {
            return _otherStatus.GetSemesterStatus().AdvisingSemester;
        }
        public int GetTotalLoginCounter(int userId = 0)
        {
            return _loginHistoryManager.GetTotalLogins(userId);
        }
        public ActionResult ComingSoon()
        {
            return View();
        }
        
    }
}