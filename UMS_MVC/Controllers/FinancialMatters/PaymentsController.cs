﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using PagedList;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.OtherSpecialService;

namespace UMS_MVC.Controllers.FinancialMatters
{
    public class PaymentsController : Controller
    {
        private UmsDbContext db;
        private readonly PaymentManager _paymentManager;
        private readonly SemesterManager _semesterManager;
        private readonly UserActivityManager _userActivityManager;
        private readonly  DepartmentManager _departmentManager;
        private readonly CourseForStudentManger _courseForStudentManger;

        public PaymentsController()
        {
            db = new UmsDbContext();
            _paymentManager = new PaymentManager();
            _semesterManager = new SemesterManager();
            _userActivityManager = new UserActivityManager();
            _departmentManager = new DepartmentManager();
            _courseForStudentManger = new CourseForStudentManger();
        }

        public ActionResult RegistrationPaymentPrint(string studentId, bool admissionSignal = false, bool admissionSignalFair=false)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Advisor" && account.AccountsRole.AccountType != "Accounts Officer" && account.AccountsRole.AccountType != "Support") return RedirectToAction("NotPermitted", "Home");
          
            //////////////// main code start /////////////////////////////
            
            _userActivityManager.InsertActivity(new UserActivity
            {
                ComputerIp = ClassifiedServices.GetUserIp(),
                UserActivityTypeId = 2,
                AccountId = account.AccountId,
                ActivityTime = DateTime.Now,
                StudentId = studentId,
                Activity = account.LoginIdentity + " Advised Student ID " + studentId
            });
            // ..............Added for bypassing activated semester..................
            var selectedSemester=new Semester();
            var activeSemester = _semesterManager.GetAdvisingActivatedSemester();
            if (activeSemester != null)
            {
                selectedSemester = activeSemester;
            }
            else
            {
                selectedSemester = _semesterManager.GetActiveSemester();
            }
            //var activeSemester = db.Semesters.Single(s => s.ActiveSemester).SemesterNYear;
            var findStudent = db.StudentIdentifications;
            if (!string.IsNullOrEmpty(studentId) && findStudent.Count(s => s.StudentId == studentId) > 0)
            {
                //var findStudent1 = findStudent.Single(s => s.StudentId == studentId);
                var findStudent1 = findStudent.FirstOrDefault(s => s.StudentId == studentId);
                var registrationslip = RegistrationForMewSemesterConfirm(studentId);
                // ..............Added for bypassing activated semester..................
                var takenCoursesByStudent =
                    db.CourseForStudentsAcademics.Include(s => s.CourseForDepartment).Where(
                        s =>
                            s.StudentIdentification.StudentId == studentId && s.Semester.CourseAdvising &&
                            s.CourseStatusId == 1).ToList();
               
                if (takenCoursesByStudent.Any())
                {
                    foreach (var courseForStudentsAcademic in takenCoursesByStudent.Where(s=> s.PaymentRegistrationId == null))
                    {
                        courseForStudentsAcademic.PaymentRegistrationId = registrationslip.PaymentRegistrationId;
                        db.Entry(courseForStudentsAcademic).State = EntityState.Modified;
                    }
                    db.SaveChanges();
                    var totalCredit =
                        takenCoursesByStudent.Sum(s => s.CourseForDepartment.Credit);
                    var payment = new Payment();

                    if (registrationslip != null)
                    {
                        double tutionfees = findStudent1.Department.PerCreditCost * totalCredit;
                        double labfees = findStudent1.Department.LabFeePerSemester;
                        double otherfees = findStudent1.Department.OtherFeePerSemister;
                        double admissionfees = findStudent1.Department.AddmissionFee;
                        var rebateCatagoryId = (db.Rebates.Where(x => x.StudentIdentificationId == findStudent1.StudentIdentificationId)
                            ).FirstOrDefault() == null ?
                           0: (db.Rebates.Where(x => x.StudentIdentificationId == findStudent1.StudentIdentificationId)
                        .FirstOrDefault().RebateCategoryId);
                        //Select(x => x.RebateCategoryId).FirstOrDefault();
                        // if (rebateCatagoryId !=null)
                        //  {
                        // rebateCatagoryId= db.Rebates.Where(x => x.StudentIdentificationId == findStudent1.StudentIdentificationId).FirstOrDefault().RebateCategoryId);
                        //.FirstOrDefault().RebateCategoryId);
                        // }
                        //     
                    //    if (rebateCatagoryId != 0)
                      //  {
                            double rebatePercentage = (db.RebateCategories
                                .Where(x => x.RebateCategoryId == rebateCatagoryId)).FirstOrDefault() == null ? 0
                                : (db.RebateCategories.Where(x => x.RebateCategoryId == rebateCatagoryId).FirstOrDefault().RebatePercent);
                      //  }

                        // ****All payment null nby sir order 11-10-2015
                        //if (admissionSignal)
                        //{
                        //    payment.AdmissionFee = 10000;
                        //    admissionfees = 10000;
                        //}
                        //if (admissionSignalFair)
                        //{
                        //    payment.AdmissionFee = 5000;
                        //    admissionfees = 5000;
                        //}
                        // ****All payment null by sir order 11-10-2015
                        payment.AdmissionFee = 0;
                        payment.Subject = "Fee of " + studentId + " of " + selectedSemester.SemesterNYear;
                        payment.RegistrationNo = registrationslip.RegistrationNo;
                        payment.PaymentRegistrationId = registrationslip.PaymentRegistrationId;
                        payment.Rebate = rebatePercentage;

                        // ****All payment null nby sir order 11-10-2015
                        //payment.TutionFee = tutionfees;
                        //payment.LabThesisFee = labfees;
                        //payment.OtherFee = otherfees;
                        //payment.Total = admissionfees + tutionfees + labfees + otherfees;
                        //payment.NetTotal = payment.Total;
                        //payment.AmountWillPay = 0;
                        // ****All payment null nby sir order 11-10-2015

                        payment.TutionFee = tutionfees;
                        payment.LabThesisFee = labfees;
                        payment.OtherFee = otherfees;
                        payment.Total = 0;
                        payment.NetTotal = 0;
                        payment.AmountWillPay = 0;

                        ViewBag.PerCreditCost = findStudent1.Department.PerCreditCost;
                        ViewBag.TakenCredit = totalCredit;
                        ViewBag.StudentId = studentId;
                        ViewBag.RegistrationSerial = registrationslip.PaymentRegistrationId;
                        ViewBag.RegistrationNo = registrationslip.RegistrationNo;
                        ViewBag.PaymentTypeId = new SelectList(db.PaymentTypes, "PaymentTypeId", "TypeName");
                        return View(payment);
                    }
                }
            }
            ViewBag.PaymentTypeId = new SelectList(db.PaymentTypes, "PaymentTypeId", "TypeName");
            return View();
            ////////////////main code end////////////////////////////////
        }
        
        
        public ActionResult PaymentConfirmSubmit()
        {
            return View();
        }

        [ValidateAntiForgeryToken][HttpPost]
        public ActionResult PaymentConfirmSubmit(Payment payment)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string userId = Session["User"].ToString();
            string role = Session["Role"].ToString();
            if (role != "Admin" && role != "Advisor" && role != "Accounts Officer" &&role!="Support")
                return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            var checkExistSlip = db.Payments.Where(s => s.PaymentRegistrationId == payment.PaymentRegistrationId && s.PaymentTypeId == 1);
            if (checkExistSlip.Any())
            {
                var findStudent =
                    db.PaymentRegistrations.First(s => s.PaymentRegistrationId == payment.PaymentRegistrationId);
                if (checkExistSlip.Count() > 1)
                {
                    TempData["paymentExists"] = "Multiple Registration Detected, delete unnecessery registration first.";

                    return RedirectToAction("RegistrationPaymentPrint",
                        new { studentId = findStudent.RegistrationFor });
                }
                TempData["paymentExists"] = "Payment Slip No : " + checkExistSlip.First().PaymentId + " is already made in " + checkExistSlip.First().PrintDate;
                TempData["PaymentSlip"] = checkExistSlip.First().PaymentId;
                return RedirectToAction("RegistrationPaymentPrint",
                    new { studentId = findStudent.RegistrationFor });
            }
                  
            var payment1 = new Payment
            {
                PaymentRegistrationId = payment.PaymentRegistrationId,
                RegistrationNo = payment.RegistrationNo,
                PrintedBy = userId,
                PaymentTypeId = 1,
                Subject = payment.Subject,
                AdmissionFee = payment.AdmissionFee,
                TutionFee = payment.TutionFee,
                LabThesisFee = payment.LabThesisFee,
                OtherFee = payment.OtherFee,
                ReRegiFee = payment.ReRegiFee,
                LateFee = payment.LateFee,
                Total = payment.Total,
                Rebate = payment.Rebate,
                PreviousBalance = 0,
                NetTotal = payment.NetTotal,
                AmountWillPay = 0,
                Balance = payment.NetTotal,
                PrintDate = DateTime.Now,
                AmountPaid = 0

            };
            db.Payments.Add(payment1);
            db.SaveChanges();
            //return View(payment1);
            return RedirectToAction("CheckOrPrintPayment", new { slipId = payment1.PaymentId });
            ////////////////main code end////////////////////////////////
        }

        public ActionResult PaymentConfirmFinal(int paymentSerial)
        {
            if (Session["Role"] != null && Session["User"] != null)
            {
                string role = Session["Role"].ToString();
                if (role == "Admin"||role=="Advisor"|| role == "Accounts Officer"||role=="Support")
                {
                    //////////////// main code start /////////////////////////////
                    var findPayment = db.Payments.Find(paymentSerial);
                    return View(findPayment);
                    ////////////////main code end////////////////////////////////
                }
                return RedirectToAction("NotPermitted", "Home");
            }
            return RedirectToAction("LogInresultSubmit", "Home");
           
        }

        public PartialViewResult RegistrationForNewSemester(string studentId)
        {
            if (!string.IsNullOrEmpty(studentId))
            {
                
                var selectedStudentsCourseList =
                db.CourseForStudentsAcademics.Where(s => s.StudentIdentification.StudentId == studentId && s.CourseStatusId == 1)
                  .Include(s => s.CourseForDepartment);

                ViewBag.TotalCredits = selectedStudentsCourseList.Sum(s => s.CourseForDepartment.Credit);
                return PartialView("_PaymentMainPartial",selectedStudentsCourseList);
            }
            return PartialView("_PaymentMainPartial");
           
        }

        public PartialViewResult RebateResult(string studentId)
        {
            if (!string.IsNullOrEmpty(studentId))
            {
                var findstudent = db.Rebates.Where(s => s.StudentIdentification.StudentId == studentId);
                if (findstudent.Any())
                {
                    ViewBag.totalRebate = findstudent.Sum(s => s.RebateCategory.RebatePercent);
                    return PartialView("_RebateForStudent", findstudent);
                }
               
            }
            return PartialView("_RebateForStudent");
        }

        public PaymentRegistration GetLastRegistration(string studentId)
        {
            var lastRegistration =
                db.PaymentRegistrations.Where(s => s.RegistrationFor == studentId)
                        .OrderByDescending(s => s.RegistrationDate).FirstOrDefault();
            return lastRegistration;
        }

        public bool CheckForFirstRegistration(string studentId)
        {
            // ..............Added for bypassing activated semester..................
            var checkRegistrationSerial =
                db.PaymentRegistrations.Count(s => s.RegistrationFor == studentId && s.Semester.CourseAdvising) == 1;
            //var checkRegistrationSerial =
            //    db.PaymentRegistrations.Count(s => s.RegistrationFor == studentId && s.Semester.ActiveSemester) ==1;
            return checkRegistrationSerial;
        }

        public bool CheckForTakenCourses(string studentId)
        {
            // ..............Added for bypassing activated semester..................
            var takenCoursesByStudent =
                    db.CourseForStudentsAcademics.Count(
                        s =>
                            s.StudentIdentification.StudentId == studentId && s.Semester.CourseAdvising &&
                            s.CourseStatusId == 1);
            //var takenCoursesByStudent =
            //        db.CourseForStudentsAcademics.Count(
            //            s =>
            //                s.StudentIdentification.StudentId == studentId && s.Semester.ActiveSemester &&
            //                s.CourseStatusId == 1);

            return takenCoursesByStudent>0;
        }

        public PaymentRegistration RegistrationForMewSemesterConfirm(string studentId)
        {
            string userId;
            if (Session["Role"] != null && Session["User"] != null)
            {
               userId= Session["User"].ToString();
            }
            else
            {
                userId = "";
            }
            var findStudent = db.StudentIdentifications;
            if (CheckForTakenCourses(studentId) && !string.IsNullOrEmpty(studentId) && findStudent.Count(s => s.StudentId == studentId) > 0)
            {
                if (CheckForFirstRegistration(studentId) == false)
                {
                    var randomNumber = new Random();
                    // ..............Added for bypassing activated semester..................
                    var firstOrDefault = _semesterManager.GetAdvisingActivatedSemester();
                    //var firstOrDefault = db.Semesters.FirstOrDefault(s => s.ActiveSemester);
                    if (firstOrDefault != null)
                    {
                        var newRegistration = new PaymentRegistration
                        {
                            SemesterId = firstOrDefault.SemesterId,
                            PrintDate = DateTime.Now,
                            PrintedBy =userId,
                            RegistrationDate = DateTime.Now,
                            RegistrationFor = studentId,
                            StudentIdentificationId = findStudent.First(s=>s.StudentId==studentId).StudentIdentificationId,
                            RegistrationNo = randomNumber.Next(100, 999999999)
                        };
                        db.PaymentRegistrations.Add(newRegistration);
                        db.SaveChanges();
                        return newRegistration;
                    }
                }
            }
            return GetLastRegistration(studentId);
        }

        public ActionResult ForNthPayment()
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role == "Admin" || role == "Accounts Officer")
            {
                //////////////// main code start /////////////////////////////
                return View();
                ////////////////main code end////////////////////////////////
            }
            return RedirectToAction("NotPermitted", "Home");
        }

        public ActionResult PaymentReceived(int paymentSlipId=0)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin" && role != "Accounts Officer") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            if (paymentSlipId > 0)
            {
                var findpayment = db.Payments.Count(s => s.PaymentId == paymentSlipId) > 0;
                if (findpayment)
                {
                    var findPaymentSlip = db.Payments.Find(paymentSlipId);

                    var findRegistration =
                        db.PaymentRegistrations.Single(
                            s =>
                                s.PaymentRegistrationId == findPaymentSlip.PaymentRegistrationId &&
                                s.RegistrationNo == findPaymentSlip.RegistrationNo);
                    findPaymentSlip.Subject = "Payment of " + findRegistration.RegistrationFor + " of " + findRegistration.Semester.SemesterNYear;
                    ViewBag.AmountPaid = findPaymentSlip.AmountPaid;
                    findPaymentSlip.AmountPaid = findPaymentSlip.AmountWillPay;
                    var getTotalSum =
                        db.Payments.Where(s => s.PaymentRegistrationId == findPaymentSlip.PaymentRegistrationId);
                    findPaymentSlip.Balance = getTotalSum.Sum(s => s.NetTotal) - getTotalSum.Sum(s => s.AmountPaid);

                    return View(findPaymentSlip);
                }
            }
            return View();
            ////////////////main code end////////////////////////////////
        }
        [ValidateAntiForgeryToken][HttpPost]
        public ActionResult PaymentReceivedFinal(Payment payment, int? paymentFor)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string userId = Session["User"].ToString();
            string role = Session["Role"].ToString();
            if (role != "Admin" && role != "Accounts Officer") return RedirectToAction("NotPermitted", "Home");

            //////////////// main code start /////////////////////////////
            var findPaymentslip = db.Payments.Find(paymentFor);
            if (findPaymentslip.Status == false)
            {
                findPaymentslip.Status = true;
                db.Entry(findPaymentslip).State = EntityState.Modified;

                ////////////////Previous payment Confirmation/////////////////

                payment.PrintDate = DateTime.Now;
                payment.PrintedBy = userId;
                payment.Balance = payment.Balance - payment.AmountPaid;
                payment.PaymentTypeId = 4;
                payment.Status = true;
                db.Payments.Add(payment);
                db.SaveChanges();
                TempData["ConfirmationOfPayment"] = "Payment Received";
                return RedirectToAction("PaymentReceived");
            }
            TempData["paymentExists"] = "Payment Already received";
            return RedirectToAction("PaymentReceived");
            ////////////////main code end////////////////////////////////
        }

        public PartialViewResult DebitCreditResult(int regiId=0)
        {
            if (regiId > 0)
            {
                var findStudent = db.PaymentRegistrations.Single(s => s.PaymentRegistrationId == regiId);
                var getAllPayments = db.Payments.Where(s => s.PaymentRegistrationId == regiId);
                double totalDebit = getAllPayments.Sum(s => s.NetTotal);
                double totalCredit = getAllPayments.Sum(s => s.AmountPaid);
                ViewBag.TotalDebit = totalDebit;
                ViewBag.TotalCredit = totalCredit;
                ViewBag.TotalBalance =  totalDebit-totalCredit ; 
                TempData["studentId"] = findStudent.RegistrationFor;
                return PartialView("_AllPaymentHistoryPartial",getAllPayments);
            }
            TempData["studentId"] = "";
            return PartialView("_AllPaymentHistoryPartial");
        }

        public ActionResult SecondOrNthPayment(string studentId)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin" && role != "Accounts Officer") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            if (!string.IsNullOrEmpty(studentId))
            {
                var findInRegistration = db.PaymentRegistrations.OrderByDescending(s => s.PaymentRegistrationId);
                if (findInRegistration.Count(s => s.RegistrationFor == studentId && s.Semester.ActiveSemester) > 0)
                {
                    var getlastRegistration = findInRegistration.First(s => s.RegistrationFor == studentId && s.Semester.ActiveSemester);
                    var makePayment = new Payment
                    {
                        PaymentRegistrationId = getlastRegistration.PaymentRegistrationId,
                        RegistrationNo = getlastRegistration.RegistrationNo,
                        Subject = "Fee of " + studentId + " " + getlastRegistration.Semester.SemesterNYear

                    };
                    return View(makePayment);
                }
            }
            return View();
            ////////////////main code end////////////////////////////////
        }

        [HttpPost][ValidateAntiForgeryToken]
        public ActionResult SecondOrNthPaymentConfirmSubmit(Payment payment)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string userId = Session["User"].ToString();
            string role = Session["Role"].ToString();
            if (role != "Admin" && role != "Accounts Officer") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            payment.PrintDate = DateTime.Now;
            payment.PrintedBy = userId;
            payment.PaymentTypeId = 2;
            payment.Balance = payment.NetTotal;
            payment.AmountWillPay = payment.NetTotal + payment.AmountWillPay;
            db.Payments.Add(payment);
            db.SaveChanges();
            TempData["PaymentReceived"] = "Payment Received";
            return RedirectToAction("SecondOrNthPayment");
            ////////////////main code end////////////////////////////////
        }

        public ActionResult IndexPayment(int? page)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin" && role != "Accounts Officer" && role != "Ps"&&role!="Support") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            if (Request.HttpMethod != "GET")
            {
                page = 1;
            }
            int pageSize = 50;
            int pageNumber = (page ?? 1);
            var getAllPayment = db.Payments.OrderByDescending(s=>s.PaymentId);
            return View(getAllPayment.ToPagedList(pageNumber, pageSize));
            ////////////////main code end////////////////////////////////
        }

        [HttpGet]
        public ActionResult EditPayement(int? id)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin" && role != "Accounts Officer"&&role!="Support") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            if (id != null)
            {
                bool masterAccess = role == "Admin" || role == "Support";

                ViewBag.masterAccess = masterAccess;
                Payment payment = db.Payments.Find(id);
                if (payment == null) return HttpNotFound();
                
                ViewBag.PaymentRegistrationId = new SelectList(db.Payments, "PaymentRegistrationId", "PaymentRegistrationId", payment.PaymentRegistrationId);
                ViewBag.PaymentTypeId = new SelectList(db.PaymentTypes, "PaymentTypeId", "TypeName", payment.PaymentTypeId);
                return View(payment);
            }
            return HttpNotFound();
            ////////////////main code end////////////////////////////////
        }
        [HttpPost]
        public ActionResult EditPayement(Payment payment)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var role = Session["Role"].ToString();
            var userId = Session["User"].ToString();
            if (role != "Admin"&&role!="Support") return RedirectToAction("NotPermitted", "Home");
           
            //////////////// main code start /////////////////////////////
            if (payment.Rebate < 101 && payment.AmountWillPay<=payment.NetTotal)
            {
                if (ModelState.IsValid)
                {
                    payment.PrintedBy = userId;
                    payment.Balance = payment.NetTotal;
                    db.Entry(payment).State = EntityState.Modified;
                    db.SaveChanges();
                    ViewBag.msg = "Payment Successfully Updated.";
                    if (Session["UrlPaymentConfirm"] != null)
                    {
                        string urlRequested = Session["UrlPaymentConfirm"].ToString();
                        Session["UrlPaymentConfirm"] = null;
                        return Redirect(urlRequested);
                    }
                    return RedirectToAction("IndexPayment");
                }
                        
            }
            
            ViewBag.msg = "Payment updateLoginCounter fail !!!";
            ViewBag.PaymentRegistrationId = new SelectList(db.Payments, "PaymentRegistrationId", "PaymentRegistrationId", payment.PaymentRegistrationId);
            ViewBag.PaymentTypeId = new SelectList(db.PaymentTypes, "PaymentTypeId", "TypeName", payment.PaymentTypeId);
            return View(payment);
            ////////////////main code end////////////////////////////////
        }

        public ActionResult CheckOrPrintPayment(string idStudent, int? slipId)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin" && role != "Accounts Officer" && role != "Advisor" && role != "Ps"&&role!="Support")
                return RedirectToAction("NotPermitted", "Home");
            TempData["userRole"] = role;
            //////////////// main code start /////////////////////////////
            if (slipId != null)
            {
                Payment payment = db.Payments.Find(slipId);
                if (payment != null)
                {
                    return View(payment);
                }
            }
            if (!string.IsNullOrEmpty(idStudent))
            {

                var findActiveSemester = _semesterManager.GetActiveSemester();
                var findAdvisingSemester = _semesterManager.GetAdvisingActivatedSemester();
                int filteredSemesterId = findActiveSemester.SemesterId;

                if (findAdvisingSemester != null)
                {
                    filteredSemesterId = findAdvisingSemester.SemesterId;
                }

                var findRegiId = db.PaymentRegistrations.FirstOrDefault(s => s.RegistrationFor == idStudent && s.SemesterId==filteredSemesterId);
                if (findRegiId!=null) return View();
                var findPaymentSlip =
                    db.Payments.FirstOrDefault(s => s.PaymentRegistrationId == findRegiId.PaymentRegistrationId);
                return View(findPaymentSlip);
            }
            return View();
            ////////////////main code end////////////////////////////////
        }
        //////////////////Accounts Can see this part////////////////////////////////

        public ActionResult CheckRegistrationofStudents(string idStudent, int? regiId,int? page, int SemesterId=0, int DepartmentId=0)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin" && role != "Accounts Officer" && role != "Ps" && role!="Support") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            
            ViewBag.SchoolId = new SelectList(db.Schools, "SchoolId", "SchoolName");
            var getAllPaymentRegistration = _paymentManager.GetAllPaymentRegistration();
            var getActiveSemester = _semesterManager.GetActiveSemester();
            TempData["DepartmentId"] = new SelectList(_departmentManager.GetAllDepartment(), "DepartmentId", "DepartmentName", "ASchool.SchoolName", DepartmentId);
           
            if (SemesterId > 0)
            {
                TempData["SemesterId"] = new SelectList(_semesterManager.GetAllSemester(), "SemesterId", "SemesterNYear", SemesterId);
                TempData["selectedSemester"] = SemesterId;
            }
            else
            {
                TempData["SemesterId"] = new SelectList(_semesterManager.GetAllSemester(), "SemesterId", "SemesterNYear", getActiveSemester.SemesterId);
            }
            if (DepartmentId>0)
            {
                //ViewBag.SemesterId = new SelectList(getAllSemesters, "SemesterId", "SemesterNYear", SemesterId);
                getAllPaymentRegistration = getAllPaymentRegistration.Where(s => s.SemesterId == SemesterId && s.StudentIdentification.DepartmentId==DepartmentId);
                TempData["deptname"] = db.Departments.Find(DepartmentId).DepartmentName;
                TempData["selectedDepartment"] = DepartmentId;
            }
           
            if (!string.IsNullOrEmpty(idStudent))
            {
                var getpaymentsAll = getAllPaymentRegistration.Where(s => s.RegistrationFor == idStudent);
                getAllPaymentRegistration = getpaymentsAll;
            }
            if (regiId > 0)
            {
                var getallPayment2 = getAllPaymentRegistration.Where(s => s.PaymentRegistrationId == regiId);
                getAllPaymentRegistration = getallPayment2;
            }
            ViewBag.TotalRegistrations = getAllPaymentRegistration.Count();
               
            if (Request.HttpMethod != "GET")
            {
                page = 1;
            }
            int pageSize = 50;
            int pageNumber = (page ?? 1);
            getAllPaymentRegistration =
                getAllPaymentRegistration.OrderByDescending(s => s.PaymentRegistrationId).Include(s => s.Payments);
            return View(getAllPaymentRegistration.ToPagedList(pageNumber, pageSize));
            ////////////////main code end////////////////////////////////
        }

        
        public PartialViewResult GetAllPayments(int? id)
        {
            if (id == null) return PartialView("_GetAllPaymentsPartial");
            var getpaymentlist = db.Payments.Where(s => s.PaymentRegistrationId == id);
            return PartialView("_GetAllPaymentsPartial", getpaymentlist);
        }

        public ActionResult DeletePayment(int payId = 0)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var role = Session["Role"].ToString();
            if (role != "Admin"&& role!="Support") return RedirectToAction("NotPermitted", "Home");

            // Main Code

            var findPayment = db.Payments.Find(payId);

            if (findPayment != null)
            {
                TempData["msg"] = "Payment slip no " + payId + " of Id: "
                                  + findPayment.PaymentRegistration.RegistrationFor + " deleted.";
                var findAssociatedCourses =
                    db.CourseForStudentsAcademics.Where(
                        s => s.PaymentRegistrationId == findPayment.PaymentRegistrationId);
                    // findAssociatedCourses.ForEach(a => a.PaymentRegistrationId = null);
                foreach (var courseForStudentsAcademic in findAssociatedCourses)
                {
                    courseForStudentsAcademic.PaymentRegistrationId = null;
                    db.Entry(courseForStudentsAcademic).State = EntityState.Modified;
                }
                //int totalCourses = findAssociatedCourses.Count();
                //ViewBag.countedddd = totalCourses;

                db.Payments.Remove(findPayment);
                db.SaveChanges();
                return RedirectToAction("CheckRegistrationofStudents");
            }
            TempData["msg"] = "Payment can be deleted due  to some exception.";
            return RedirectToAction("EditPayement", new {id = payId});
        }
    }
}
