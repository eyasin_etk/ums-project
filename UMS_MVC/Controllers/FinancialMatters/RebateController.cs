﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PagedList;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.Controllers.FinancialMatters
{
    public class RebateController : Controller
    {
        private UmsDbContext db = new UmsDbContext();

        // GET: /Rebate/
        public ActionResult Index(string studentid, int? page)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin" && role != "Accounts Officer" && role != "Ps") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            var rebates = db.Rebates.Include(r => r.RebateCategory).Include(r => r.StudentIdentification).AsParallel();
            ViewBag.RebatedStudentCount = rebates.Count();
            if (Request.HttpMethod != "GET")
            {
                page = 1;
            }
            int pageSize = 50;
            int pageNumber = (page ?? 1);
            if (!string.IsNullOrEmpty(studentid))
            {
                rebates = rebates.Where(s => s.StudentIdentification.StudentId == studentid);
                if (rebates.Any())
                    return View(rebates.ToPagedList(pageNumber, pageSize));
            }
            return View(rebates.ToPagedList(pageNumber, pageSize));
            ////////////////main code end////////////////////////////////
        }

        public PartialViewResult LoadRebatedStudentlist(string studentid)
        {
            var rebates = db.Rebates.Include(r => r.RebateCategory).Include(r => r.StudentIdentification).AsParallel();
            if (!string.IsNullOrEmpty(studentid))
            {
                rebates = rebates.Where(s => s.StudentIdentification.StudentId == studentid);
                if (rebates.Any())
                    return PartialView("~/Views/Rebate/_RebatedStudentlist.cshtml", rebates.ToList());
            }
            return PartialView("~/Views/Rebate/_RebatedStudentlist.cshtml", rebates.ToList());
        }

        // GET: /Rebate/Details/5
        public ActionResult Details(int? id)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin" && role != "Accounts Officer" && role != "Ps") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rebate rebate = db.Rebates.Find(id);
            if (rebate == null)
            {
                return HttpNotFound();
            }
            return View(rebate);
            ////////////////main code end////////////////////////////////
        }

        public ActionResult FindStudenttoAddRebate(string studentid)
        {
            if (!string.IsNullOrEmpty(studentid))
            {
                var findStudent = db.StudentIdentifications;
                if (findStudent.Count(s => s.StudentId == studentid)>0)
                {
                    var findSpecific = findStudent.Single(s => s.StudentId == studentid);
                    TempData["SelectedStudent"] = findSpecific.StudentIdentificationId;
                   return RedirectToAction("Create");
                }
                
            }
            return RedirectToAction("Create");
        }

        // GET: /Rebate/Create
        public ActionResult Create()
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin" && role != "Accounts Officer") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            if (TempData["SelectedStudent"]!=null)
            {
                ViewBag.RebateCategoryId = new SelectList(db.RebateCategories, "RebateCategoryId", "RebateName");
                ViewBag.StudentIdentificationId = new SelectList(db.StudentIdentifications, "StudentIdentificationId", "StudentId", TempData["SelectedStudent"].ToString());
                return View();
            }
                    
            ViewBag.RebateCategoryId = new SelectList(db.RebateCategories, "RebateCategoryId", "RebateName");
            ViewBag.StudentIdentificationId = new SelectList(db.StudentIdentifications, "StudentIdentificationId", "StudentId");
            return View();
            ////////////////main code end////////////////////////////////
        }

        public PartialViewResult LoadStudentPartial(int? studentId)
        {
            if (studentId > 0)
            {
                var findstudent = db.StudentIdentifications.Find(studentId);
                var getThisStudentRebates =
                    db.Rebates.Where(s => s.StudentIdentificationId == findstudent.StudentIdentificationId);
                TempData["rebateListOfthisStudent"] = getThisStudentRebates;
                if (getThisStudentRebates.Any())
                {
                    TempData["totalRebatesForthisStudent"] = getThisStudentRebates.Sum(s => s.RebateCategory.RebatePercent);
                }
                else
                {
                    TempData["totalRebatesForthisStudent"] = 0;
                }
                
                if (findstudent != null)
                {
                    return PartialView("~/Views/Rebate/_LoadStudentPartial.cshtml", findstudent);
                }
            }
            return PartialView("~/Views/Rebate/_LoadStudentPartial.cshtml");

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="RebateId,StudentIdentificationId,RebateCategoryId")] Rebate rebate)
        {
            var findrebatename = db.RebateCategories.Find(rebate.RebateCategoryId);
            var findStudent = db.StudentIdentifications.Find(rebate.StudentIdentificationId);
            var checkSameReabtetwice =
                db.Rebates.Where(s => s.StudentIdentificationId == rebate.StudentIdentificationId);   
            if (checkSameReabtetwice.Count(s=>s.RebateCategoryId == rebate.RebateCategoryId) >= 1)
            {
                TempData["Msg"] =findStudent.StudentId +" has already got "+findrebatename.RebateName+" cann't add twice.";
            }  
            else
            {
                if (ModelState.IsValid)
                {
                    TempData["Msg"] = findrebatename.RebateName + " is added to " + findStudent.StudentId;
                    db.Rebates.Add(rebate);
                    db.SaveChanges();
                    ModelState.Clear();
                    ViewBag.RebateCategoryId = new SelectList(db.RebateCategories, "RebateCategoryId", "RebateName");
                    ViewBag.StudentIdentificationId = new SelectList(db.StudentIdentifications, "StudentIdentificationId", "StudentId");
                    return View();
                }
            }
            ViewBag.RebateCategoryId = new SelectList(db.RebateCategories, "RebateCategoryId", "RebateName", rebate.RebateCategoryId);
            ViewBag.StudentIdentificationId = new SelectList(db.StudentIdentifications, "StudentIdentificationId", "StudentId", rebate.StudentIdentificationId);
            return View(rebate);
        }

        // GET: /Rebate/Edit/5
        public ActionResult Edit(int? id)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin" && role != "Accounts Officer") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rebate rebate = db.Rebates.Find(id);
            if (rebate == null)
            {
                return HttpNotFound();
            }
            ViewBag.RebateCategoryId = new SelectList(db.RebateCategories, "RebateCategoryId", "RebateName", rebate.RebateCategoryId);
            ViewBag.StudentIdentificationId = new SelectList(db.StudentIdentifications, "StudentIdentificationId", "StudentId", rebate.StudentIdentificationId);
            return View(rebate);
            ////////////////main code end////////////////////////////////
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="RebateId,StudentIdentificationId,RebateCategoryId")] Rebate rebate)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rebate).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.RebateCategoryId = new SelectList(db.RebateCategories, "RebateCategoryId", "RebateName", rebate.RebateCategoryId);
            ViewBag.StudentIdentificationId = new SelectList(db.StudentIdentifications, "StudentIdentificationId", "StudentId", rebate.StudentIdentificationId);
            return View(rebate);
        }

        // GET: /Rebate/Delete/5
        public ActionResult Delete(int? id)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin" && role != "Accounts Officer") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rebate rebate = db.Rebates.Find(id);
            if (rebate == null)
            {
                return HttpNotFound();
            }
            return View(rebate);
            ////////////////main code end////////////////////////////////
        }

        // POST: /Rebate/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            Rebate rebate = db.Rebates.Find(id);
            db.Rebates.Remove(rebate);
            db.SaveChanges();
            return RedirectToAction("Index");
            ////////////////main code end////////////////////////////////
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
