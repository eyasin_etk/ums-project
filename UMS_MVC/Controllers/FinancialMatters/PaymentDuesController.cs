﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.Controllers.FinancialMatters
{
    public class PaymentDuesController : Controller
    {
        private UmsDbContext db = new UmsDbContext();

        // GET: PaymentDues
        public ActionResult Index()
        {
            var paymentDues = db.PaymentDues.Include(p => p.semester);
            return View(paymentDues.ToList());
        }

        // GET: PaymentDues/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PaymentDue paymentDue = db.PaymentDues.Find(id);
            if (paymentDue == null)
            {
                return HttpNotFound();
            }
            return View(paymentDue);
        }

        // GET: PaymentDues/Create
        public ActionResult Create()
        {
            ViewBag.SemesterId = new SelectList(db.Semesters, "SemesterId", "SemesterNYear");
           // ViewBag.StudentIdentificationId = new SelectList(db.StudentIdentifications, "StudentIdentificationId", "StudentId");
            return View();
        }

        // POST: PaymentDues/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "paymentdueID,StudentId,SemesterId,Totaldue,description")] PaymentDue paymentDue)
        {
            if (ModelState.IsValid)
            {
               // var identity = db.StudentIdentifications.Where(a => a.StudentId == paymentDue.StudentId).Select(a => a.StudentIdentificationId).FirstOrDefault();
                
                db.PaymentDues.Add(paymentDue);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SemesterId = new SelectList(db.Semesters, "SemesterId", "SemesterNYear", paymentDue.SemesterId);
           // ViewBag.StudentIdentificationId = new SelectList(db.StudentIdentifications, "StudentIdentificationId", "StudentId", paymentDue.StudentIdentificationId);
            return View(paymentDue);
        }

        // GET: PaymentDues/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PaymentDue paymentDue = db.PaymentDues.Find(id);
            if (paymentDue == null)
            {
                return HttpNotFound();
            }
            ViewBag.SemesterId = new SelectList(db.Semesters, "SemesterId", "SemesterNYear", paymentDue.SemesterId);
           // ViewBag.StudentIdentificationId = new SelectList(db.StudentIdentifications, "StudentIdentificationId", "StudentId", paymentDue.StudentIdentificationId);
            return View(paymentDue);
        }

        // POST: PaymentDues/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "paymentdueID,StudentId,SemesterId,Totaldue,description")] PaymentDue paymentDue)
        {
            if (ModelState.IsValid)
            {
                db.Entry(paymentDue).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SemesterId = new SelectList(db.Semesters, "SemesterId", "SemesterNYear", paymentDue.SemesterId);
           // ViewBag.StudentIdentificationId = new SelectList(db.StudentIdentifications, "StudentIdentificationId", "StudentId", paymentDue.StudentIdentificationId);
            return View(paymentDue);
        }

        // GET: PaymentDues/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PaymentDue paymentDue = db.PaymentDues.Find(id);
            if (paymentDue == null)
            {
                return HttpNotFound();
            }
            return View(paymentDue);
        }

        // POST: PaymentDues/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PaymentDue paymentDue = db.PaymentDues.Find(id);
            db.PaymentDues.Remove(paymentDue);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
