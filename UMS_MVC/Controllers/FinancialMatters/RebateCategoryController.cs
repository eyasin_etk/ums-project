﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.Controllers.FinancialMatters
{
    public class RebateCategoryController : Controller
    {
        private UmsDbContext db = new UmsDbContext();

        // GET: /RebateCategory/
        public ActionResult Index()
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role == "Admin" && role != "Ps")
            {
                //////////////// main code start /////////////////////////////
                return View(db.RebateCategories.ToList());
                ////////////////main code end////////////////////////////////
            }
            return RedirectToAction("NotPermitted", "Home");
        }

        // GET: /RebateCategory/Details/5
        public ActionResult Details(int? id)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin" && role != "Ps") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RebateCategory rebatecategory = db.RebateCategories.Find(id);
            if (rebatecategory == null)
            {
                return HttpNotFound();
            }
            return View(rebatecategory);
            ////////////////main code end////////////////////////////////
        }

        // GET: /RebateCategory/Create
        public ActionResult Create()
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role == "Admin")
            {
                //////////////// main code start /////////////////////////////
                return View();
                ////////////////main code end////////////////////////////////
            }
            return RedirectToAction("NotPermitted", "Home");
        }

    
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="RebateCategoryId,RebateName,RebateDetails,RebatePercent")] RebateCategory rebatecategory)
        {
            if (ModelState.IsValid)
            {
                db.RebateCategories.Add(rebatecategory);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(rebatecategory);
        }

        // GET: /RebateCategory/Edit/5
        public ActionResult Edit(int? id)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RebateCategory rebatecategory = db.RebateCategories.Find(id);
            if (rebatecategory == null)
            {
                return HttpNotFound();
            }
            return View(rebatecategory);
            ////////////////main code end////////////////////////////////
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="RebateCategoryId,RebateName,RebateDetails,RebatePercent")] RebateCategory rebatecategory)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rebatecategory).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(rebatecategory);
        }

        // GET: /RebateCategory/Delete/5
        public ActionResult Delete(int? id)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RebateCategory rebatecategory = db.RebateCategories.Find(id);
            if (rebatecategory == null)
            {
                return HttpNotFound();
            }
            return View(rebatecategory);
            ////////////////main code end////////////////////////////////
        }

       
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            RebateCategory rebatecategory = db.RebateCategories.Find(id);
            db.RebateCategories.Remove(rebatecategory);
            db.SaveChanges();
            return RedirectToAction("Index");
            ////////////////main code end////////////////////////////////
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
