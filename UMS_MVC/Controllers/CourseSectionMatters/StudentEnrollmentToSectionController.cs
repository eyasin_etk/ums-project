﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.Manager.DAL.DAO.Intefaces;
using UMS_MVC.Manager.DAL.DAO.Repository;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.Controllers.CourseSectionMatters
{
    public class StudentEnrollmentToSectionController : Controller
    {
        private UmsDbContext db = new UmsDbContext();
        private  readonly CourseForDepartmentManager _courseForDepartmentManager=new CourseForDepartmentManager();
        private readonly DepartmentManager _departmentManager=new DepartmentManager();
        private readonly CourseForStudentManger _courseForStudentManger=new CourseForStudentManger();
        private readonly SemesterManager _semesterManager=new SemesterManager();
        private readonly SectionManager _sectionManager=new SectionManager();
        private readonly ICourseForStudentRepository _studentRepository=new CourseForStudentRepository();
        //private readonly 

        public ActionResult EnrollToSections(int semesterId=0, int SectionId=0, int DepartmentId = 0,int CourseForDepartmentId =0)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType!="Support") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            ViewBag.semesterId = new SelectList(_semesterManager.GetAllSemester(), "SemesterId", "SemesterNYear", _semesterManager.GetActiveSemester().SemesterId);
            ViewBag.DepartmentId = new SelectList(_departmentManager.GetAllDepartment(), "DepartmentId", "DepartmentName", "ASchool.SchoolName",0);
            var getAllCourse = _courseForDepartmentManager.GetAllCourseForDepartmentsWithCode(DepartmentId);
            ViewBag.CourseForDepartmentId = new SelectList(getAllCourse, "CourseForDepartmentId",
                "CourseName", "SerializedSemester.SemesterName", 0);
            int selectedSec = 0;
            if (semesterId > 0)
            {
                if (DepartmentId > 0)
                {
                    if (CourseForDepartmentId > 0)
                    {
                        //var studentList =
                        //    _courseForStudentManger.GetAllCoursesByCourseCodeandSemester(CourseForDepartmentId, semesterId).Where(s=>s.CourseStatusId !=4);
                        var studentList =
                    _studentRepository.GetAll().Include(s=>s.StudentIdentification).Include(s=>s.CourseStatus).Where(s => s.CourseStatusId != 4 && s.CourseForDepartmentId==CourseForDepartmentId && s.SemesterId==semesterId);
                        var seclist = _sectionManager.GetAllSectionByCourseCode(CourseForDepartmentId);
                        if(seclist.Any())
                        {
                            ViewBag.SectionId =
                                new SelectList(seclist, "SectionId", "SectionName", "Semester.SemesterNYear", 0);                                 
                        }
                        else
                        {
                            ViewBag.SectionId = new SelectList("", "SectionId", "SectionName", "Semester.SemesterNYear", 0);
                               
                        }
                        if (SectionId > 0 && seclist.Count(s => s.SectionId == SectionId) == 1)
                            {
                            var getSectionInfo = GetSectionInfo(SectionId);
                            if (getSectionInfo.Teacher != null)
                            {
                                ViewBag.AssignedTeacherName = getSectionInfo.Teacher.Account.Name;
                            }
                            else
                            {
                                ViewBag.AssignedTeacherName = "No Faculty Assigned";
                            }
                            Session.Add("SelectedSection", SectionId);
                            ViewBag.SectionName = getSectionInfo.SectionName;
                            ViewBag.SelectedSectionID = SectionId;
                            ViewBag.TotalUnassignedStudents = studentList.Count(s => s.SectionId == null);
                            ViewBag.TotalAssigned = studentList.Count(s => s.SectionId == SectionId);
                            selectedSec = SectionId;
                        }
                        if (studentList.Any())
                        {
                            ViewBag.studentCount = studentList.Count();
                            var getSelectedCourseInfo =
                                _courseForDepartmentManager.GetSingleCourseForDepartment(CourseForDepartmentId);
                            ViewBag.selectedCourse = "[" + getSelectedCourseInfo.CourseCode + "] " +
                                                     getSelectedCourseInfo.CourseName;
                            ViewBag.selectedSection = selectedSec;
                            var filteredList = (from acaStu in studentList.AsNoTracking()
                                select new SectionEnrollmentViewModel
                                {
                                    SectionId = acaStu.SectionId,
                                    StudentId = acaStu.StudentIdentification.StudentId,
                                    StatusName = acaStu.CourseStatus.ShortName,
                                    CourseForStudentId = acaStu.CourseForStudentsAcademicId
                                }).OrderBy(s => s.StudentId).ToList();
                            
                            return View(filteredList);
                        }
                    }
                }
            }
            ViewBag.selectedSection = selectedSec;
            ViewBag.SectionId = new SelectList("", "SectionId", "SectionName", "Semester.SemesterNYear", 0);
            return View();
            ////////////////main code end////////////////////////////////
        }

        public Section GetSectionInfo(int sectionId)
        {
            var sectionInfo = _sectionManager.GetSingleSection(sectionId);
            return sectionInfo;
        }

        public ActionResult EnrollTosectionConfirm(FormCollection collection)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin" && role!="Support") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            if (Session["SelectedSection"] != null)
            {
                int selectedSection = Convert.ToInt32(Session["SelectedSection"]);
                foreach (var key in collection.Keys)
                {
                    if (key.ToString().StartsWith("Member"))
                    {
                        int itemId = int.Parse(key.ToString().Replace("Member", ""));
                        if (collection[key.ToString()].Contains("true"))
                        {
                            CourseForStudentsAcademic findStudent = db.CourseForStudentsAcademics.Find(itemId);
                            findStudent.SectionId = selectedSection;
                            db.Entry(findStudent).State = EntityState.Modified;
                           
                        }
                    }
                }
                db.SaveChanges();
            }
            if (Session["RediretToSectionEnrollment"] != null)
            {
                return Redirect(Session["RediretToSectionEnrollment"].ToString());
            }
            return RedirectToAction("EnrollToSections");
            ////////////////main code end////////////////////////////////
        }

        public ActionResult EnrollSectionRemove(FormCollection collection)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin" && role!="Support") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            if (Session["SelectedSection"] != null)
            {
                int selectedSection = Convert.ToInt32(Session["SelectedSection"]);
                foreach (var key in collection.Keys)
                {
                    if (key.ToString().StartsWith("Member"))
                    {
                        int itemId = int.Parse(key.ToString().Replace("Member", ""));
                        if (collection[key.ToString()].Contains("true"))
                        {
                            CourseForStudentsAcademic findStudent = db.CourseForStudentsAcademics.Find(itemId);
                            findStudent.SectionId = null;
                            db.Entry(findStudent).State = EntityState.Modified;
                            
                        }
                    }
                }
                db.SaveChanges();
            }
            if (Session["RediretToSectionEnrollment"] != null)
            {
                return Redirect(Session["RediretToSectionEnrollment"].ToString());
            }
            return RedirectToAction("EnrollToSections");

            ////////////////main code end////////////////////////////////
        }
    }
}