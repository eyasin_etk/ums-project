﻿using System.Linq;
using System.Web.Mvc;
using UMS_MVC.Controllers.OtherSpecialControllers;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.BLL;
using UMS_MVC.Manager.DAL.DAO;

namespace UMS_MVC.Controllers.CourseSectionMatters
{
    public class CourseOtherMatterController : Controller
    {
        private readonly CourseBusinessLogic _courseBusinessLogicController=new CourseBusinessLogic();
        private readonly SemesterManager _semesterManager = new SemesterManager();

        public CourseOtherMatterController()
        {

        }

        public ActionResult Index()
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (!AccessibilitiesOverMethod.AccessForCourseOtherMatter("Index", account.AccountsRole.AccountType)) return RedirectToAction("NotPermitted", "Home");
            /////////code start//////
            var getSemesters = _semesterManager.GetAllSemester().Where(s => s.SemesterId >= 35).OrderBy(s => s.SemesterId);
            ViewBag.fromBatch = new SelectList(getSemesters, "SemesterId", "BatchNo",35);
            ViewBag.toBatch = new SelectList(getSemesters, "SemesterId", "BatchNo");
            var semesterInfo = _semesterManager.GetActiveSemester();
            ViewBag.sem = semesterInfo.SemesterNYear;
          return View();
        }
        
        public PartialViewResult GetAllStatictics(int fromBatchId, int toBatchId=0)
        {
            ViewBag.formBatchId = fromBatchId;
            ViewBag.tobatchId = toBatchId;
            var semesters = _semesterManager.GetAllSemester().Where(s=>s.SemesterId>=fromBatchId && s.SemesterId<=toBatchId)
                .OrderByDescending(s => s.SemesterId).ToList();
            string batches = semesters.Aggregate("", (current, semester) => current + semester.BatchNo + " ");
            ViewBag.batches = batches;
            var getData = _courseBusinessLogicController.GenerateStatisticalData(fromBatchId, toBatchId);
            return PartialView(getData);
        }
    }
}