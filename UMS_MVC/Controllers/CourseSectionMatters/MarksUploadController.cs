﻿using System.Linq;
using System.Web.Mvc;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.Controllers.CourseSectionMatters
{
    public class MarksUploadController : Controller
    {
        private readonly MarkUploadManager _markUploadManager;
        private readonly SectionManager _sectionManager;
        public MarksUploadController()
        {
            _markUploadManager=new MarkUploadManager();
            _sectionManager=new SectionManager();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SectionForUploadMarks(int secId=86)
        {
            int counter = 0;
            var findSectionObj = _sectionManager.GetSingleSection(secId);
            var findsection = _markUploadManager.GetAllCourseForStudent().Where(s => s.SectionId == secId).AsEnumerable();
            var sortList = findsection.Select((s, ixc) => new MarkUploadViewModel()
            {
                Serial = ixc,
                CourseForStudentId = s.CourseForStudentsAcademicId,
                CourseForDeptId = s.CourseForDepartmentId,
                StudentId = s.StudentIdentification.StudentId,
                StudentName = s.StudentIdentification.StudentInfo.StudentName,
                Att = s.Attendance,
                ClassTest =s.ClassTest,
                MidTerm = s.Midterm,
                FinalTerm = s.FinalTerm,
                TotalMark = s.TotalMark,
                LetterGrade = s.LetterGrade,
                Grade = s.Grade,
                CourseType = s.CourseForDepartment.CourseType
            });
            return View(sortList.OrderBy(s=>s.StudentId));
        }

        public PartialViewResult LoadCourseForUploadModal(int id=0)
        {
            if (id > 0)
            {
                var findCourse = _markUploadManager.GetSingleCourseForStudentsAcademic(id);
                MarkUploadViewModel courseForStudentsAcademic=new MarkUploadViewModel()
                {
                   CourseForStudentId = findCourse.CourseForStudentsAcademicId,
                   CourseForDeptId = findCourse.CourseForDepartmentId,
                   StudentId = findCourse.StudentIdentification.StudentId,
                   StudentName = findCourse.StudentIdentification.StudentInfo.StudentName,
                   Att = findCourse.Attendance,
                   ClassTest = findCourse.ClassTest,
                   MidTerm = findCourse.Midterm,
                   FinalTerm = findCourse.FinalTerm,
                   TotalMark = findCourse.TotalMark,
                   LetterGrade = findCourse.LetterGrade,
                   Grade = findCourse.Grade,
                   CourseType = findCourse.CourseForDepartment.CourseType
                    
                };
                return PartialView(courseForStudentsAcademic);
            }
            return PartialView(new MarkUploadViewModel());
        }

        public JsonResult SubmitMarks(MarkUploadViewModel courseForStudentsAcademic)
        {
            var findCourseForStudent =
                _markUploadManager.GetSingleCourseForStudentsAcademic(courseForStudentsAcademic.CourseForStudentId);
            findCourseForStudent.Attendance = courseForStudentsAcademic.Att;
            findCourseForStudent.ClassTest = courseForStudentsAcademic.ClassTest;
            findCourseForStudent.Midterm = courseForStudentsAcademic.MidTerm;
            findCourseForStudent.FinalTerm = courseForStudentsAcademic.FinalTerm;
            return Json(new
            {
                CourseForStudentId = courseForStudentsAcademic.CourseForStudentId,
                CourseForDeptId = courseForStudentsAcademic.CourseForDeptId,
                StudentId = findCourseForStudent.StudentIdentification.StudentId,
                StudentName = findCourseForStudent.StudentIdentification.StudentInfo.StudentName,
                Att = courseForStudentsAcademic.Att,
                ClassTest = courseForStudentsAcademic.ClassTest,
                MidTerm = courseForStudentsAcademic.MidTerm,
                FinalTerm = courseForStudentsAcademic.FinalTerm,
                TotalMark = courseForStudentsAcademic.TotalMark,
                LetterGrade = courseForStudentsAcademic.LetterGrade,
                Grade = courseForStudentsAcademic.Grade,
                CourseType = courseForStudentsAcademic.CourseType,
                msg = "Updated Successfully"
            });
        }
       

    }
}