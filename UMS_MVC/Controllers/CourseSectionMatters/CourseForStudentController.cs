﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using PagedList;
using UMS_MVC.Controllers.OtherSpecialControllers;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.BLL;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.Controllers.CourseSectionMatters
{
    public class CourseForStudentController : Controller
    {
        private UmsDbContext db;
        private readonly SemesterManager _semesterManager;
        private readonly CourseForStudentManger _courseForStudentManger;
        private readonly StudentManager _studentManager;
        private readonly CourseForDepartmentManager _courseForDepartmentManager;
        private readonly DepartmentManager _departmentManager;
        private readonly CommonAttributeManager _commonAttributeManager;
        private readonly TeacherManager _teacherManager;
        private readonly UserActivity _userActivity;
        private readonly SectionBusinessLogic _sectionBusinessLogic;

        public CourseForStudentController()
        {
            db = new UmsDbContext();
            _semesterManager = new SemesterManager();
            _courseForStudentManger = new CourseForStudentManger();
            _studentManager = new StudentManager();
            _courseForDepartmentManager = new CourseForDepartmentManager();
            _departmentManager = new DepartmentManager();
            _commonAttributeManager = new CommonAttributeManager();
            _teacherManager = new TeacherManager();
            _userActivity = new UserActivity();
            _sectionBusinessLogic = new SectionBusinessLogic();
        }

        public ActionResult Index(int SemesterId = 0, int DepartmentId = 0, int CourseForDepartmentId = 0)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (!AccessibilitiesOverMethod.AccountAccessRoleStudentAdvisingList("Index", account.AccountsRole.AccountType)) return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////

            var findActiveSemester = _semesterManager.GetActiveSemester();
            var findAdvisingSemester = _semesterManager.GetAdvisingActivatedSemester();
            int filteredSemesterId = findActiveSemester.SemesterId;

            if (findAdvisingSemester != null)
            {
                filteredSemesterId = findAdvisingSemester.SemesterId;
            }

            ViewBag.selectedSemester = filteredSemesterId;

            TempData["SemesterId"] = new SelectList(_semesterManager.GetAllSemester(), "SemesterId", "SemesterNYear", filteredSemesterId);
            TempData["DepartmentId"] = new SelectList(_departmentManager.GetAllDepartment(), "DepartmentId", "DepartmentName", "ASchool.SchoolName", DepartmentId);
            TempData["CourseForDepartmentId"] = new SelectList(_courseForDepartmentManager.GetAllCourseForDepartmentsWithCode(DepartmentId), "CourseForDepartmentId", "CourseName", "SerializedSemester.SemesterName", CourseForDepartmentId);
            ViewBag.selectedSemester = SemesterId;
            ViewBag.selectedDeptId = DepartmentId;
            ViewBag.SelectedCourseId = CourseForDepartmentId;
            ViewBag.selectedSemesterId = SemesterId;
            return
                 View();
            ////////////////main code end////////////////////////////////
        }
        public PartialViewResult LoadAdvisingsOfStudents(int? page, int SemesterId = 0, int DepartmentId = 0, int CourseForDepartmentId = 0, int secSort = 0)
        {
            var account = (Account)Session["UserObj"];
            var courseforstudentsacademics =
                _courseForStudentManger.GetAllCourseForStudentsAcademicsByCourseId(CourseForDepartmentId)
                .Where(s => s.SemesterId == SemesterId);

            var getCourseInfo = _courseForDepartmentManager.GetSingleCourseForDepartment(CourseForDepartmentId);
            ViewBag.AllAdvised = courseforstudentsacademics.Count();
            ViewBag.assignedStudents = courseforstudentsacademics.Count(s => s.SectionId != null);
            ViewBag.notAssignedStudents = courseforstudentsacademics.Count(s => s.SectionId == null);
            switch (secSort)
            {
                case 0:
                    break;
                case 1:
                    courseforstudentsacademics = courseforstudentsacademics.Where(s => s.SectionId != null);
                    break;
                case 2:
                    courseforstudentsacademics = courseforstudentsacademics.Where(s => s.SectionId == null);
                    break;
            }
            var batches = courseforstudentsacademics.DistinctBy(s => s.StudentIdentification.Semester.BatchNo)
             .Select(s => s.StudentIdentification.Semester.BatchNo);
            string selectedBatches = batches.Aggregate("", (current, batch) => current + (batch + " "));
            ViewBag.batches = selectedBatches;
            bool printAccess;
            if (account.AccountsRoleId == 1 || account.AccountsRoleId == 7 || account.AccountsRoleId == 11)
            {
                printAccess = true;
            }
            else
            {
                printAccess = false;
            }
            ViewBag.PageSortObj = new AdvisingPagesort
            {
                SemesterId = SemesterId,
                DepartmentId = DepartmentId,
                CourseId = CourseForDepartmentId,
                SecSort = secSort,
                PrintAccess = printAccess
            };
            ViewBag.courseInfo = getCourseInfo;
            if (Request.HttpMethod != "GET")
            {
                page = 1;
            }
            int pageSize = 30;
            int pageNumber = (page ?? 1);
            //var sortedList =
            //    courseforstudentsacademics.OrderByDescending(s => s.CourseForDepartmentId)
            //        .ToPagedList(pageNumber, pageSize);
            var sortedList = (from stud in courseforstudentsacademics
                              orderby stud.StudentIdentification.StudentId, stud.StudentIdentification.StudentId
                              select stud)
                .ToPagedList(pageNumber, pageSize);
            return PartialView("_StudentListPartial", sortedList);
        }
        public PartialViewResult SectionInitialProposal(int SemesterId = 0, int DepartmentId = 0, int CourseForDepartmentId = 0, int secSort = 0, int noOfStudent = 0)
        {
            ViewBag.teacherId = new SelectList(_sectionBusinessLogic.FilteredTeachers(), "TeacherId", "Account.Name", "Department.DepartmentTitle", 0);
            ViewBag.courseInfo = _courseForDepartmentManager.GetSingleCourseForDepartment(CourseForDepartmentId);
            var advisingSortObj = new AdvisingPagesort
            {
                SemesterId = SemesterId,
                DepartmentId = DepartmentId,
                CourseId = CourseForDepartmentId,
                SecSort = secSort
            };
            ViewBag.noOfStudent = noOfStudent;
            return PartialView("_sectionProposalPartial", advisingSortObj);
        }
        //public string FindBatches(int SemesterId = 0,int CourseForDepartmentId = 0)
        //{
        //    var getBatches =
        //        _courseForStudentManger.GetAllCourseForStudentsAcademicsByCourseId(CourseForDepartmentId)
        //      .Where(s => s.SemesterId == SemesterId)
        //      .DistinctBy(s => s.StudentIdentification.Semester.BatchNo)
        //      .Select(s=>s.StudentIdentification.Semester.BatchNo);
        //    return getBatches.Aggregate("", (current, batch) => current + (batch + " "));
        //}
        public CourseStatusByAdvising GetSpecificCourseStatus(int courseForDeptId = 0)
        {
            var getCourseInfo = _courseForDepartmentManager.GetSingleCourseForDepartment(courseForDeptId);
            var getSpecificCourseStatus =
                _courseForStudentManger.GetAllCourseForStudentsAcademicsByCourseId(courseForDeptId);
            var courseStatus = new CourseStatusByAdvising
            {
                CourseCode = getCourseInfo.CourseCode,
                CourseTitle = getCourseInfo.CourseName,
                CourseForDeptId = getCourseInfo.CourseForDepartmentId,
                TotalEnrolled = getSpecificCourseStatus.Count(),
                SectionEnrolled = getSpecificCourseStatus.Count(s => s.SectionId != null),
                SectionNotEnrolled = getSpecificCourseStatus.Count(s => s.SectionId == null),
            };
            return courseStatus;
        }
        public PartialViewResult GetAdvisingStatusByCourse(int departmentId = 0, int semesterId = 0)
        {
            var getAllCourses = _courseForDepartmentManager.GetAllCourseForDepartmentsByDepartment(departmentId);
            var mergeCourses = new List<CourseStatusByAdvising>();
            if (departmentId > 0)
            {
                ViewBag.deptInfo = _departmentManager.GetSingleDepartment(departmentId).DepartmentName;
                var getAdvisedList = _courseForStudentManger.GetAllCourseForStudentsAcademicsByDeptAndSemester(departmentId, semesterId);
                foreach (var courseForDepartment in getAllCourses)
                {
                    var getAllAdvisedCounter =
                        getAdvisedList.Where(s => s.CourseForDepartmentId == courseForDepartment.CourseForDepartmentId);
                    var courseStatus = new CourseStatusByAdvising
                    {
                        SemesterSerial = courseForDepartment.SerializedSemester.SemesterName,
                        CourseForDeptId = courseForDepartment.CourseForDepartmentId,
                        CourseCode = courseForDepartment.CourseCode,
                        CourseTitle = courseForDepartment.CourseName,
                        TotalEnrolled = getAllAdvisedCounter.Count(),
                        SectionEnrolled = getAllAdvisedCounter.Count(s => s.SectionId != null),
                        SectionNotEnrolled = getAllAdvisedCounter.Count(s => s.SectionId == null),

                    };
                    mergeCourses.Add(courseStatus);
                }
            }
            return PartialView("_CourseStatus", mergeCourses);
        }
        public ActionResult Details(int? id)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CourseForStudentsAcademic courseforstudentsacademic = db.CourseForStudentsAcademics.Find(id);
            if (courseforstudentsacademic == null)
            {
                return HttpNotFound();
            }
            return View(courseforstudentsacademic);
            ////////////////main code end////////////////////////////////
        }
        public PartialViewResult GetNewAdvisedCoursesVersionOne(string studentid)
        {
            var getAdvisedActivatedSemester = _semesterManager.GetAdvisingActivatedSemester().SemesterId;
            var getStudentId = _studentManager.GetSingleStudentByStudentId(studentid);
            var getcourses = _courseForStudentManger.GetAllNewAdvisedCoursesOfAStudent(getStudentId.StudentIdentificationId, getAdvisedActivatedSemester);
            ViewBag.TakenCredits = 0;
            if (getcourses.Any())
            {
                ViewBag.TakenCredits = getcourses.Sum(s => s.CourseForDepartment.Credit);
            }
            //ViewBag.TakenCredits=
            return PartialView("_NewAdvisedForVersionOne", getcourses);
        }
        public ActionResult AddCourses2(string studentIdentity)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (!AccessibilitiesOverMethod.AccountAccessRoleStudentAdvisingList("AddCourses2", account.AccountsRole.AccountType)) return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            var getAdvisingSemester = _semesterManager.GetAdvisingActivatedSemester();
            // ..............Added for bypassing activated semester..................
            if (getAdvisingSemester == null)
            {
                return View("AdvisingDisabled");
            }
            if (string.IsNullOrEmpty(studentIdentity) || !_studentManager.StudentExistanceByStudentId(studentIdentity))
            {
                return View();
            }
            var findStudent = _studentManager.GetSingleStudentByStudentId(studentIdentity);
            if (findStudent.BlockStudent)
            {
                ViewData["blockStudentMsg"] = _studentManager.StudentBlockCheck(findStudent.StudentIdentificationId);
                return View();
            }

            var takenCourses = _courseForStudentManger.GetAllCourseByStudent(findStudent.StudentIdentificationId).Where(s => s.CourseStatusId != 1 && s.CourseStatusId != 4);
            ViewBag.StudentObj = findStudent;

            // ..............Added for bypassing activated semester..................
            //var totalStudentEnrollment =
            //    _courseForStudentManger.GetAllCourseForStudentsAcademics()
            //        .Where(s => s.Semester.CourseAdvising && s.CourseStatusId == 1);
            var studentWiseCourseList =
                _courseForDepartmentManager.GetAllCourseForDepartmentsByDepartment(findStudent.DepartmentId);
            var data = new List<dynamic>();
            foreach (var courselist in studentWiseCourseList)
            {
                //var getTotalEnrollNumber =
                //    totalStudentEnrollment.Count(s => s.CourseForDepartmentId == courselist.CourseForDepartmentId);
                if (takenCourses.Count(s => s.CourseForDepartmentId == courselist.CourseForDepartmentId) > 0)
                {
                    //All duplicate logic should start be here
                    foreach (var courseForStudentsAcademic in takenCourses.Where(s => s.CourseForDepartmentId == courselist.CourseForDepartmentId))
                    {
                        var sortedTakenCourses =
                            takenCourses.Where(s => s.CourseForDepartmentId == courseForStudentsAcademic.CourseForDepartmentId);
                        if (sortedTakenCourses.Count() > 1)
                        {
                            if (sortedTakenCourses.Any(s => s.Grade > 2.75))
                            {
                                var selectProperCourse = sortedTakenCourses.First(s => s.Grade > 2.75);
                                if (data.Count(s => s.CourseForDepartmentId == courseForStudentsAcademic.CourseForDepartmentId) == 0)
                                {
                                    dynamic courses = new ExpandoObject();
                                    courses.StudentID = studentIdentity;
                                    courses.Semester = courselist.SerializedSemester.SemesterName;
                                    courses.CourseForDepartmentId = selectProperCourse.CourseForDepartmentId;
                                    courses.CourseCode = courselist.CourseCode;
                                    courses.CourseName = courselist.CourseName;
                                    courses.Credit = courselist.Credit;
                                    courses.Prerequisit = courselist.PrerequisiteCourse;
                                    courses.Status = selectProperCourse.CourseStatus.Status;
                                    // courses.TotalEnrolled = getTotalEnrollNumber;
                                    courses.completeSemester = selectProperCourse.Semester.SemesterNYear;
                                    courses.Grade = selectProperCourse.LetterGrade;
                                    data.Add(courses);
                                }
                            }
                            if (sortedTakenCourses.Any(d => d.LetterGrade == "F" && d.Grade <= 2.75))
                            {
                                if (data.Count(s => s.CourseForDepartmentId == courseForStudentsAcademic.CourseForDepartmentId) == 0)
                                {
                                    var selectProperCourse = sortedTakenCourses.Where(s => s.Grade >= 0.0 && s.Grade <= 2.75).OrderByDescending(s => s.CourseForStudentsAcademicId).First();
                                    dynamic courses = new ExpandoObject();
                                    courses.StudentID = studentIdentity;
                                    courses.Semester = courselist.SerializedSemester.SemesterName;
                                    courses.CourseForDepartmentId = selectProperCourse.CourseForDepartmentId;
                                    courses.CourseCode = courselist.CourseCode;
                                    courses.CourseName = courselist.CourseName;
                                    courses.Credit = courselist.Credit;
                                    courses.Prerequisit = courselist.PrerequisiteCourse;
                                    courses.Status = selectProperCourse.CourseStatus.Status;
                                    // courses.TotalEnrolled = getTotalEnrollNumber;
                                    courses.completeSemester = selectProperCourse.Semester.SemesterNYear;
                                    courses.Grade = selectProperCourse.LetterGrade;
                                    data.Add(courses);
                                }
                            }
                            if (sortedTakenCourses.Count(s => s.LetterGrade == "F") > 1)
                            {

                                if (data.Count(s => s.CourseForDepartmentId == courseForStudentsAcademic.CourseForDepartmentId) == 0)
                                {
                                    var selectProperCourse = sortedTakenCourses.Where(s => s.LetterGrade == "F").OrderByDescending(s => s.CourseForStudentsAcademicId).First();
                                    dynamic courses = new ExpandoObject();
                                    courses.StudentID = studentIdentity;
                                    courses.Semester = courselist.SerializedSemester.SemesterName;
                                    courses.CourseForDepartmentId = selectProperCourse.CourseForDepartmentId;
                                    courses.CourseCode = courselist.CourseCode;
                                    courses.CourseName = courselist.CourseName;
                                    courses.Credit = courselist.Credit;
                                    courses.Prerequisit = courselist.PrerequisiteCourse;
                                    courses.Status = selectProperCourse.CourseStatus.Status;
                                    //courses.TotalEnrolled = getTotalEnrollNumber;
                                    courses.completeSemester = selectProperCourse.Semester.SemesterNYear;
                                    courses.Grade = selectProperCourse.LetterGrade;
                                    data.Add(courses);
                                }
                            }
                        }
                        else
                        {
                            dynamic courses = new ExpandoObject();
                            courses.StudentID = studentIdentity;
                            courses.Semester = courselist.SerializedSemester.SemesterName;
                            courses.CourseForDepartmentId = courselist.CourseForDepartmentId;
                            courses.CourseCode = courselist.CourseCode;
                            courses.CourseName = courselist.CourseName;
                            courses.Credit = courselist.Credit;
                            courses.Prerequisit = courselist.PrerequisiteCourse;
                            courses.Status = courseForStudentsAcademic.CourseStatus.Status;
                            //courses.TotalEnrolled = getTotalEnrollNumber;
                            courses.completeSemester = courseForStudentsAcademic.Semester.SemesterNYear;
                            courses.Grade = courseForStudentsAcademic.LetterGrade;
                            data.Add(courses);
                        }
                    }
                    //All duplicate logic should end be here
                }
                else
                {
                    dynamic courses = new ExpandoObject();
                    courses.StudentID = studentIdentity;
                    courses.Semester = courselist.SerializedSemester.SemesterName;
                    courses.CourseForDepartmentId = courselist.CourseForDepartmentId;
                    courses.CourseCode = courselist.CourseCode;
                    courses.CourseName = courselist.CourseName;
                    courses.Credit = courselist.Credit;
                    courses.Prerequisit = courselist.PrerequisiteCourse;
                    courses.Status = "";
                    //courses.TotalEnrolled = getTotalEnrollNumber;
                    courses.completeSemester = "";
                    courses.Grade = "";
                    data.Add(courses);
                }
            }
            if (_studentManager.GetAllStudentInfos().Count(s => s.StudentId == studentIdentity) > 0)
            {
                ViewBag.studentName =
                    _studentManager.GetAllStudentInfos().Single(s => s.StudentId == studentIdentity).StudentName;
            }
            ViewBag.Department = findStudent.Department.DepartmentName;
            ViewBag.School = findStudent.School.SchoolName;
            return View(data);
        }
        //public ActionResult AddCourses2(string studentIdentity)
        //{
        //    if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
        //    var account = (Account)Session["UserObj"];
        //    if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Advisor") return RedirectToAction("NotPermitted", "Home");
        //    var getAdvisingSemester = _semesterManager.GetAdvisingActivatedSemester();
        //    // ..............Added for bypassing activated semester..................
        //    if (getAdvisingSemester == null)
        //    {
        //        return View("AdvisingDisabled");
        //    }
        //    if (string.IsNullOrEmpty(studentIdentity) || !_studentManager.StudentExistanceByStudentId(studentIdentity))
        //    {
        //        return View();
        //    }
        //    var findStudent = _studentManager.GetSingleStudentByStudentId(studentIdentity);
        //    var takenCourses = _courseForStudentManger.GetAllCourseByStudent(findStudent.StudentIdentificationId).Where(s => s.CourseStatusId != 1);

        //    // ..............Added for bypassing activated semester..................
        //    var totalStudentEnrollment =
        //        _courseForStudentManger.GetAllCourseForStudentsAcademics()
        //            .Where(s => s.Semester.CourseAdvising && s.CourseStatusId == 1);
        //    var studentWiseCourseList =
        //        _courseForDepartmentManager.GetAllCourseForDepartmentsByDepartment(findStudent.DepartmentId);
        //    var data = new List<dynamic>();
        //    foreach (var courselist in studentWiseCourseList)
        //    {
        //        var getTotalEnrollNumber =
        //            totalStudentEnrollment.Count(s => s.CourseForDepartmentId == courselist.CourseForDepartmentId);
        //        if (takenCourses.Count(s => s.CourseForDepartmentId == courselist.CourseForDepartmentId) > 0)
        //        {
        //            //All duplicate logic should start be here
        //            foreach (var courseForStudentsAcademic in takenCourses.Where(s => s.CourseForDepartmentId == courselist.CourseForDepartmentId))
        //            {
        //                var sortedTakenCourses =
        //                    takenCourses.Where(s => s.CourseForDepartmentId == courseForStudentsAcademic.CourseForDepartmentId && s.CourseStatusId != 1);
        //                if (sortedTakenCourses.Count() > 1)
        //                {
        //                    if (sortedTakenCourses.Any(s => s.Grade > 2.75))
        //                    {
        //                        var selectProperCourse = sortedTakenCourses.First(s => s.Grade > 2.75);
        //                        if (data.Count(s => s.CourseForDepartmentId == selectProperCourse.CourseForDepartmentId) == 0)
        //                        {
        //                            dynamic courses = new ExpandoObject();
        //                            courses.StudentID = studentIdentity;
        //                            courses.Semester = courselist.SerializedSemester.SemesterName;
        //                            courses.CourseForDepartmentId = selectProperCourse.CourseForDepartmentId;
        //                            courses.CourseCode = courselist.CourseCode;
        //                            courses.CourseName = courselist.CourseName;
        //                            courses.Credit = courselist.Credit;
        //                            courses.Prerequisit = courselist.PrerequisiteCourse;
        //                            courses.Status = selectProperCourse.CourseStatus.Status;
        //                            courses.TotalEnrolled = getTotalEnrollNumber;
        //                            courses.completeSemester = selectProperCourse.Semester.SemesterNYear;
        //                            courses.Grade = selectProperCourse.LetterGrade;
        //                            data.Add(courses);
        //                        }
        //                    }
        //                    if (sortedTakenCourses.Count(s => s.LetterGrade == "F") > 1)
        //                    {
        //                        var selectProperCourse = sortedTakenCourses.Where(s => s.LetterGrade == "F").OrderByDescending(s => s.CourseForStudentsAcademicId).First();
        //                        if (data.Count(s => s.CourseForDepartmentId == selectProperCourse.CourseForDepartmentId) == 0)
        //                        {
        //                            dynamic courses = new ExpandoObject();
        //                            courses.StudentID = studentIdentity;
        //                            courses.Semester = courselist.SerializedSemester.SemesterName;
        //                            courses.CourseForDepartmentId = selectProperCourse.CourseForDepartmentId;
        //                            courses.CourseCode = courselist.CourseCode;
        //                            courses.CourseName = courselist.CourseName;
        //                            courses.Credit = courselist.Credit;
        //                            courses.Prerequisit = courselist.PrerequisiteCourse;
        //                            courses.Status = selectProperCourse.CourseStatus.Status;
        //                            courses.TotalEnrolled = getTotalEnrollNumber;
        //                            courses.completeSemester = selectProperCourse.Semester.SemesterNYear;
        //                            courses.Grade = selectProperCourse.LetterGrade;
        //                            data.Add(courses);
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    dynamic courses = new ExpandoObject();
        //                    courses.StudentID = studentIdentity;
        //                    courses.Semester = courselist.SerializedSemester.SemesterName;
        //                    courses.CourseForDepartmentId = courselist.CourseForDepartmentId;
        //                    courses.CourseCode = courselist.CourseCode;
        //                    courses.CourseName = courselist.CourseName;
        //                    courses.Credit = courselist.Credit;
        //                    courses.Prerequisit = courselist.PrerequisiteCourse;
        //                    courses.Status = courseForStudentsAcademic.CourseStatus.Status;
        //                    courses.TotalEnrolled = getTotalEnrollNumber;
        //                    courses.completeSemester = courseForStudentsAcademic.Semester.SemesterNYear;
        //                    courses.Grade = courseForStudentsAcademic.LetterGrade;
        //                    data.Add(courses);
        //                }
        //            }
        //            //All duplicate logic should end be here
        //        }
        //        else
        //        {
        //            dynamic courses = new ExpandoObject();
        //            courses.StudentID = studentIdentity;
        //            courses.Semester = courselist.SerializedSemester.SemesterName;
        //            courses.CourseForDepartmentId = courselist.CourseForDepartmentId;
        //            courses.CourseCode = courselist.CourseCode;
        //            courses.CourseName = courselist.CourseName;
        //            courses.Credit = courselist.Credit;
        //            courses.Prerequisit = courselist.PrerequisiteCourse;
        //            courses.Status = "";
        //            courses.TotalEnrolled = getTotalEnrollNumber;
        //            courses.completeSemester = "";
        //            courses.Grade = "";
        //            data.Add(courses);
        //        }
        //    }
        //    if (_studentManager.GetAllStudentInfos().Count(s => s.StudentId == studentIdentity) > 0)
        //    {
        //        ViewBag.studentName =
        //            _studentManager.GetAllStudentInfos().Single(s => s.StudentId == studentIdentity).StudentName;
        //    }
        //    ViewBag.Department = findStudent.Department.DepartmentName;
        //    ViewBag.School = findStudent.School.SchoolName;
        //    return View(data);
        //}
        //public ActionResult AddCourses2(string studentIdentity)
        //{
        //    if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
        //    var account = (Account)Session["UserObj"];
        //    if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Advisor") return RedirectToAction("NotPermitted", "Home");
        //    var getAdvisingSemester = _semesterManager.GetAdvisingActivatedSemester();
        //    // ..............Added for bypassing activated semester..................
        //    if (getAdvisingSemester == null)
        //    {
        //        return View("AdvisingDisabled");
        //    }
        //    if (string.IsNullOrEmpty(studentIdentity) || !_studentManager.StudentExistanceByStudentId(studentIdentity))
        //    {
        //        return View();
        //    }
        //    var findStudent = _studentManager.GetSingleStudentByStudentId(studentIdentity);
        //    var takenCourses = _courseForStudentManger.GetAllCourseByStudent(findStudent.StudentIdentificationId);

        //    // ..............Added for bypassing activated semester..................
        //    var totalStudentEnrollment =
        //        _courseForStudentManger.GetAllCourseForStudentsAcademics()
        //            .Where(s => s.Semester.CourseAdvising && s.CourseStatusId == 1);
        //    var studentWiseCourseList =
        //        _courseForDepartmentManager.GetAllCourseForDepartmentsByDepartment(findStudent.DepartmentId);
        //    var data = new List<dynamic>();

        //    foreach (var courselist in studentWiseCourseList)
        //    {
        //        var getTotalEnrollNumber =
        //            totalStudentEnrollment.Count(s => s.CourseForDepartmentId == courselist.CourseForDepartmentId);
        //        if (takenCourses.Count(s => s.CourseForDepartmentId == courselist.CourseForDepartmentId) > 0)
        //        {
        //            //All sorted logic should be here
        //            foreach (var courseForStudentsAcademic in takenCourses.Where(s => s.CourseForDepartmentId == courselist.CourseForDepartmentId))
        //               {
        //                dynamic courses = new ExpandoObject();
        //                courses.StudentID = studentIdentity;
        //                courses.Semester = courselist.SerializedSemester.SemesterName;
        //                courses.CourseForDepartmentId = courselist.CourseForDepartmentId;
        //                courses.CourseCode = courselist.CourseCode;
        //                courses.CourseName = courselist.CourseName;
        //                courses.Credit = courselist.Credit;
        //                courses.Prerequisit = courselist.PrerequisiteCourse;
        //                courses.Status = courseForStudentsAcademic.CourseStatus.Status;
        //                courses.TotalEnrolled = getTotalEnrollNumber;
        //                courses.completeSemester = courseForStudentsAcademic.Semester.SemesterNYear;
        //                courses.Grade = courseForStudentsAcademic.LetterGrade;
        //                data.Add(courses);
        //            }
        //        }
        //        else
        //        {
        //            dynamic courses = new ExpandoObject();
        //            courses.StudentID = studentIdentity;
        //            courses.Semester = courselist.SerializedSemester.SemesterName;
        //            courses.CourseForDepartmentId = courselist.CourseForDepartmentId;
        //            courses.CourseCode = courselist.CourseCode;
        //            courses.CourseName = courselist.CourseName;
        //            courses.Credit = courselist.Credit;
        //            courses.Prerequisit = courselist.PrerequisiteCourse;
        //            courses.Status = "";
        //            courses.TotalEnrolled = getTotalEnrollNumber;
        //            courses.completeSemester = "";
        //            courses.Grade = "";
        //            data.Add(courses);
        //        }
        //    }
        //    if (_studentManager.GetAllStudentInfos().Count(s => s.StudentId == studentIdentity) > 0)
        //    {
        //        ViewBag.studentName =
        //            _studentManager.GetAllStudentInfos().Single(s => s.StudentId == studentIdentity).StudentName;
        //    }
        //    ViewBag.Department = findStudent.Department.DepartmentName;
        //    ViewBag.School = findStudent.School.SchoolName;
        //    return View(data);
        //}
        public ActionResult AddCourses(string studentIdentity)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin" && role != "Advisor" && role != "Support") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            //var studentExixtance = db.StudentIdentifications.Count(s => s.StudentId == studentIdentity) == 0;
            if (string.IsNullOrEmpty(studentIdentity) || !_studentManager.StudentExistanceByStudentId(studentIdentity))
            {
                return View();
            }
            var findStudent = _studentManager.GetSingleStudentByStudentId(studentIdentity);
            var takenCourses = _courseForStudentManger.GetAllCourseByStudent(findStudent.StudentIdentificationId);
            var totalStudentEnrollment = _courseForStudentManger.GetAllCourseForStudentsAcademics().Where(s => s.Semester.ActiveSemester && s.CourseStatusId == 1);
            var data = new List<dynamic>();
            foreach (var courselist in db.CourseForDepartments.Where(s => s.DepartmentId == findStudent.DepartmentId))
            {
                var takencourseExist =
                    takenCourses.Count(s => s.CourseForDepartmentId == courselist.CourseForDepartmentId) == 1;
                if (takencourseExist)
                {
                    dynamic courses = new ExpandoObject();
                    courses.StudentID = studentIdentity;
                    courses.Semester = courselist.SerializedSemester.SemesterName;
                    courses.CourseForDepartmentId = courselist.CourseForDepartmentId;
                    courses.CourseCode = courselist.CourseCode;
                    courses.CourseName = courselist.CourseName;
                    courses.Credit = courselist.Credit;
                    courses.Prerequisit = courselist.PrerequisiteCourse;
                    courses.Status = (takenCourses.Where(s => s.CourseForDepartmentId == courselist.CourseForDepartmentId)).Single().CourseStatus.Status;
                    courses.TotalEnrolled =
                        totalStudentEnrollment.Count(s => s.CourseForDepartmentId == courselist.CourseForDepartmentId);
                    data.Add(courses);
                }
                else
                {
                    dynamic courses2 = new ExpandoObject();
                    courses2.StudentID = studentIdentity;
                    courses2.Semester = courselist.SerializedSemester.SemesterName;
                    courses2.CourseForDepartmentId = courselist.CourseForDepartmentId;
                    courses2.CourseCode = courselist.CourseCode;
                    courses2.CourseName = courselist.CourseName;
                    courses2.Credit = courselist.Credit;
                    courses2.Prerequisit = courselist.PrerequisiteCourse;
                    courses2.Status = "";
                    courses2.TotalEnrolled =
                        totalStudentEnrollment.Count(s => s.CourseForDepartmentId == courselist.CourseForDepartmentId);
                    data.Add(courses2);
                }

            }
            if (db.StudentInfos.Count(s => s.StudentId == studentIdentity) > 0)
            {
                ViewBag.studentName = db.StudentInfos.Single(s => s.StudentId == studentIdentity).StudentName;
            }
            ViewBag.Department = findStudent.Department.DepartmentName;
            ViewBag.School = findStudent.School.SchoolName;
            return View(data);
            ////////////////main code end////////////////////////////////
        }
        public bool CheckCourseStatusForEnroll(int courseCode, int student, string forWho)
        {
            // ..............Added for bypassing activated semester..................
            var getCurrentSemester = _semesterManager.GetAdvisingActivatedSemester();
            //var getCurrentSemester = _semesterManager.GetActiveSemester();
            var x = false;
            var checkExistsCours = db.CourseForStudentsAcademics.Where(s => s.CourseForDepartmentId == courseCode && s.StudentIdentificationId == student);
            //var courseForStudentsAcademic = checkExistsCours.FirstOrDefault();

            if (forWho == "new")
            {

                if (checkExistsCours.Count(s => s.CourseStatusId == 1 && s.SemesterId == getCurrentSemester.SemesterId) > 0)
                {
                    x = false;
                }
                else
                {
                    x = true;
                }
                return x;
            }
            if (forWho == "retake")
            {
                if (checkExistsCours.Count(s => s.CourseStatusId == 4) > 0)
                {
                    if (checkExistsCours.Count(s => s.CourseStatusId == 1 && s.SemesterId == getCurrentSemester.SemesterId) > 0)
                    {
                        x = false;
                    }
                    else
                    {
                        x = true;
                    }

                }
                else
                {
                    x = false;
                }
            }

            return x;
        }
        public void AddCourseForStudentAcademic(int courseCode, int student, int semesterId, int courseStatusCode)
        {
            var courseforStudent = new CourseForStudentsAcademic
            {
                CourseForDepartmentId = courseCode,
                StudentIdentificationId = student,
                SemesterId = semesterId,
                Attendance = 0.0,
                ClassTest = 0.0,
                Midterm = 0.0,
                FinalTerm = 0.0,
                TotalMark = 0.0,
                Grade = 0,
                TotalGrade = 0.0,
                CourseStatusId = courseStatusCode,
                AddedDate = DateTime.Now
            };
            db.CourseForStudentsAcademics.Add(courseforStudent);
            db.SaveChanges();
        }
        public ActionResult AddCoursesForStudent(int courseCode, string studentId)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Advisor" && account.AccountsRole.AccountType != "Support") return RedirectToAction("NotPermitted", "Home");

            //////////////// main code start /////////////////////////////

            int student = db.StudentIdentifications.Single(s => s.StudentId == studentId).StudentIdentificationId;
            // ..............Added for bypassing activated semester..................
            int semesterId = _semesterManager.GetAdvisingActivatedSemester().SemesterId;
            //int semesterId = _semesterManager.GetActiveSemester().SemesterId;
            string courseName = db.CourseForDepartments.Find(courseCode).CourseName;


            if (CheckCourseStatusForEnroll(courseCode, student, "new"))
            {
                AddCourseForStudentAcademic(courseCode, student, semesterId, 1);

                TempData["CourseExistanceAlert"] = " " + courseName + " Added";
                return RedirectToAction("AddCourses2", new { studentIdentity = studentId });
            }
            TempData["CourseExistanceAlert"] = " " + courseName + " Already Taken";
            return RedirectToAction("AddCourses2", new { studentIdentity = studentId });
            ////////////////main code end////////////////////////////////
        }
        public ActionResult RetakeCourse(int courseCode, string studentId)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin" && role != "Advisor" && role != "Support") return RedirectToAction("NotPermitted", "Home");

            int student = db.StudentIdentifications.Single(s => s.StudentId == studentId).StudentIdentificationId;
            // ..............Added for bypassing activated semester..................
            int semesterId = _semesterManager.GetAdvisingActivatedSemester().SemesterId;
            //int semesterId = _semesterManager.GetActiveSemester().SemesterId;
            string courseName = db.CourseForDepartments.Find(courseCode).CourseName;
            if (CheckCourseStatusForEnroll(courseCode, student, "retake"))
            {
                AddCourseForStudentAcademic(courseCode, student, semesterId, 1);

                TempData["CourseExistanceAlert"] = " " + courseName + " Added";
                return RedirectToAction("AddCourses2", new { studentIdentity = studentId });
            }
            TempData["CourseExistanceAlert"] = " " + courseName + " Already Taken";
            return RedirectToAction("AddCourses2", new { studentIdentity = studentId });
        }
        public ActionResult RetakeFailedCourses(int courseCode, string studentId)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin" && role != "Advisor" && role != "Support") return RedirectToAction("NotPermitted", "Home");

            string courseName = db.CourseForDepartments.Find(courseCode).CourseName;
            int student = db.StudentIdentifications.Single(s => s.StudentId == studentId).StudentIdentificationId;
            // ..............Added for bypassing activated semester..................
            int semesterId = _semesterManager.GetAdvisingActivatedSemester().SemesterId;
            //int semesterId = _semesterManager.GetActiveSemester().SemesterId;
            if (CheckExistingCourse(student, courseCode))
            {
                var findSpecificCourse = _courseForStudentManger.GetAllCourseByStudent(student).Where(d => d.CourseForDepartmentId == courseCode && d.LetterGrade == "F");
                if (findSpecificCourse.Any())
                {
                    UpdatePreviousFailedCourses(courseCode, student, 5);
                    AddCourseForStudentAcademic(courseCode, student, semesterId, 1);
                    TempData["CourseExistanceAlert"] = " " + courseName + " Added";
                    return RedirectToAction("AddCourses2", new { studentIdentity = studentId });
                }
            }

            TempData["CourseExistanceAlert"] = " " + courseName + " Already Taken";
            return RedirectToAction("AddCourses2", new { studentIdentity = studentId });
        }
        public ActionResult ImproveCourses(int courseCode, string studentId)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin" && role != "Advisor" && role != "Support") return RedirectToAction("NotPermitted", "Home");


            string courseName = db.CourseForDepartments.Find(courseCode).CourseName;
            int student = db.StudentIdentifications.Single(s => s.StudentId == studentId).StudentIdentificationId;
            // ..............Added for bypassing activated semester..................
            int semesterId = _semesterManager.GetAdvisingActivatedSemester().SemesterId;
            //int semesterId = _semesterManager.GetActiveSemester().SemesterId;
            if (CheckExistingCourse(student, courseCode) && CountTotalImprovedCourses(student))
            {
                var findSpecificCourse = _courseForStudentManger.GetAllCourseByStudent(student).Where(d => d.CourseForDepartmentId == courseCode && d.Grade >= 2.0 && d.Grade <= 2.75);
                if (findSpecificCourse.Any())
                {
                    UpdatePreviousCorsesForImprovement(courseCode, student, 6);
                    AddCourseForStudentAcademic(courseCode, student, semesterId, 1);
                    TempData["CourseExistanceAlert"] = " " + courseName + " Added";
                    return RedirectToAction("AddCourses2", new { studentIdentity = studentId });
                }
            }

            TempData["CourseExistanceAlert"] = " " + courseName + " Already Taken";
            return RedirectToAction("AddCourses2", new { studentIdentity = studentId });
        }
        public bool CountTotalImprovedCourses(int student)
        {
            var countAllCoutse =
               _courseForStudentManger.GetAllCourseByStudent(student).Count(s => s.CourseStatusId == 6);

            if (countAllCoutse >= 8)
            {
                return false;
            }
            return true;
        }
        public bool CheckExistingCourse(int student, int courseCode)
        {
            // ..............Added for bypassing activated semester..................
            var getCurrentSester = _semesterManager.GetAdvisingActivatedSemester();
            //var getCurrentSester = _semesterManager.GetActiveSemester();
            var countAllCoutse =
                _courseForStudentManger.GetAllCourseByStudent(student).Count(s => s.CourseForDepartmentId == courseCode
                    && s.SemesterId == getCurrentSester.SemesterId
                    && s.CourseStatusId == 1);
            if (countAllCoutse > 0)
            {
                return false;
            }
            return true;
        }
        private void UpdatePreviousCorsesForImprovement(int courseCode, int student, int courseStatusCode)
        {
            var findLowGradedCourses =
               db.CourseForStudentsAcademics.Where(
                   s =>
                       s.StudentIdentificationId == student && s.CourseForDepartmentId == courseCode &&
                       s.Grade >= 2.0 && s.Grade <= 2.75 && s.CourseStatusId == 2);
            foreach (var courseForStudentsAcademic in findLowGradedCourses)
            {
                courseForStudentsAcademic.CourseStatusId = courseStatusCode;
                db.Entry(courseForStudentsAcademic).State = EntityState.Modified;
            }
            db.SaveChanges();
        }
        private void UpdatePreviousFailedCourses(int courseCode, int student, int courseStatusCode)
        {
            var findFailedCourses =
                db.CourseForStudentsAcademics.Where(
                    s =>
                        s.StudentIdentificationId == student && s.CourseForDepartmentId == courseCode &&
                        s.LetterGrade == "F" && s.CourseStatusId == 2);
            foreach (var courseForStudentsAcademic in findFailedCourses)
            {
                courseForStudentsAcademic.CourseStatusId = courseStatusCode;
                db.Entry(courseForStudentsAcademic).State = EntityState.Modified;
            }
            db.SaveChanges();
        }
        public ActionResult RemoveCoursesFromStudent(int studentId, int takenCourseId = 0)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin" && role != "Advisor" && role != "Support") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            var studentAndCourseCheck =
                db.CourseForStudentsAcademics.Find(takenCourseId);
            if (studentAndCourseCheck != null)
            {
                if (studentAndCourseCheck.PaymentRegistrationId == null)
                {
                    TempData["paymentDone"] = studentAndCourseCheck.CourseForDepartment.CourseCode + " removed Successfully.";
                    db.CourseForStudentsAcademics.Remove(studentAndCourseCheck);
                    db.SaveChanges();
                }
                else
                {
                    TempData["paymentDone"] = "Sorry, Cannot remove course because payment has been created!";
                }

            }
            return RedirectToAction("AddCourses2", new { studentIdentity = studentId });
            ////////////////main code end////////////////////////////////
        }
        public ActionResult Create()
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            ViewBag.CourseForDepartmentId = new SelectList(db.CourseForDepartments, "CourseForDepartmentId", "CourseType");
            ViewBag.CourseStatusId = new SelectList(db.CourseStatuses, "CourseStatusId", "Status");
            ViewBag.SemesterId = new SelectList(db.Semesters, "SemesterId", "SemesterNYear");
            ViewBag.StudentIdentificationId = new SelectList(db.StudentIdentifications, "StudentIdentificationId", "StudentId");
            return View();
            ////////////////main code end////////////////////////////////
        }
        public ActionResult RegistrationConfirm(string studentIdentity)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin" && role != "Advisor" && role != "Support") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            var studentexists = db.StudentIdentifications.Count(s => s.StudentId == studentIdentity) > 0;
            if (!string.IsNullOrEmpty(studentIdentity))
            {
                if (studentexists)
                {
                    var findStudent = db.StudentIdentifications.Single(s => s.StudentId == studentIdentity);
                    ViewBag.CourseStatusId = new SelectList(db.CourseStatuses, "CourseStatusId", "Status", 2);
                    var countRegisteredCourses =
                        db.CourseForStudentsAcademics.Count(
                            s =>
                                s.StudentIdentificationId == findStudent.StudentIdentificationId &&
                                s.Semester.CourseAdvising) > 0;
                    // ...........Update ^ this formula s.Semester.ActiveSemester................
                    if (countRegisteredCourses)
                    {
                        var studentCourselist =
                            db.CourseForStudentsAcademics.Where(
                                s => s.StudentIdentificationId == findStudent.StudentIdentificationId && s.Semester.CourseAdvising);
                        // ...........Update ^ this formula s.Semester.ActiveSemester................
                        return View(studentCourselist);
                    }
                    ViewBag.Notice = "Student's advising not done.";
                    return View();
                }
                ViewBag.Notice = "student not found.";
                return View();
            }
            return View();
            ////////////////main code end////////////////////////////////
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RegistrationConfirmApproval(CourseForStudentsAcademic ca)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin" && role != "Advisor" && role != "Support") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            if (ca.CourseStatusId > 0)
            {
                var findCourse = db.CourseForStudentsAcademics.Find(ca.CourseForStudentsAcademicId);
                findCourse.CourseStatusId = ca.CourseStatusId;
                db.Entry(findCourse).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("RegistrationConfirm", new { studentIdentity = ca.StudentIdentification.StudentId });
            }
            return RedirectToAction("RegistrationConfirm");
            ////////////////main code end////////////////////////////////
        }
        public ActionResult RegistrationConfirmApprovalAll(int studentId)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin" && role != "Support") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            var findStudent = db.StudentIdentifications.Find(studentId);
            var findCoursesForStudent =
                db.CourseForStudentsAcademics.Where(
                    s => s.StudentIdentificationId == studentId && s.Semester.CourseAdvising);
            // ...........Update ^ this formula s.Semester.ActiveSemester................
            foreach (var courseForStudentsAcademic in findCoursesForStudent)
            {
                courseForStudentsAcademic.CourseStatusId = 2;
                db.Entry(courseForStudentsAcademic).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("RegistrationConfirm", new { studentIdentity = findStudent.StudentId });
            ////////////////main code end////////////////////////////////
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CourseForStudentsAcademicId,StudentIdentificationId,CourseForDepartmentId,SemesterId,Attendance,ClassTest,Midterm,FinalTerm,TotalMark,Grade,TotalGrade,LetterGrade,CourseStatusId")] CourseForStudentsAcademic courseforstudentsacademic)
        {

            if (ModelState.IsValid)
            {
                courseforstudentsacademic.AddedDate = DateTime.Now;
                db.CourseForStudentsAcademics.Add(courseforstudentsacademic);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CourseForDepartmentId = new SelectList(db.CourseForDepartments, "CourseForDepartmentId", "CourseType", courseforstudentsacademic.CourseForDepartmentId);
            ViewBag.CourseStatusId = new SelectList(db.CourseStatuses, "CourseStatusId", "Status", courseforstudentsacademic.CourseStatusId);
            ViewBag.SemesterId = new SelectList(db.Semesters, "SemesterId", "SemesterNYear", courseforstudentsacademic.SemesterId);
            ViewBag.StudentIdentificationId = new SelectList(db.StudentIdentifications, "StudentIdentificationId", "StudentId", courseforstudentsacademic.StudentIdentificationId);
            return View(courseforstudentsacademic);
        }
        public ActionResult Edit(int? id)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CourseForStudentsAcademic courseforstudentsacademic = db.CourseForStudentsAcademics.Find(id);
            if (courseforstudentsacademic == null)
            {
                return HttpNotFound();
            }
            ViewBag.CourseForDepartmentId = new SelectList(db.CourseForDepartments, "CourseForDepartmentId", "CourseType", courseforstudentsacademic.CourseForDepartmentId);
            ViewBag.CourseStatusId = new SelectList(db.CourseStatuses, "CourseStatusId", "Status", courseforstudentsacademic.CourseStatusId);
            ViewBag.SemesterId = new SelectList(db.Semesters, "SemesterId", "SemesterNYear", courseforstudentsacademic.SemesterId);
            ViewBag.StudentIdentificationId = new SelectList(db.StudentIdentifications, "StudentIdentificationId", "StudentId", courseforstudentsacademic.StudentIdentificationId);
            return View(courseforstudentsacademic);
            ////////////////main code end////////////////////////////////
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CourseForStudentsAcademicId,StudentIdentificationId,CourseForDepartmentId,SemesterId,Attendance,ClassTest,Midterm,FinalTerm,TotalMark,Grade,TotalGrade,LetterGrade,CourseStatusId,SectionId")] CourseForStudentsAcademic courseforstudentsacademic)
        {
            if (ModelState.IsValid)
            {
                courseforstudentsacademic.AddedDate = DateTime.Now;
                db.Entry(courseforstudentsacademic).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CourseForDepartmentId = new SelectList(db.CourseForDepartments, "CourseForDepartmentId", "CourseType", courseforstudentsacademic.CourseForDepartmentId);
            ViewBag.CourseStatusId = new SelectList(db.CourseStatuses, "CourseStatusId", "Status", courseforstudentsacademic.CourseStatusId);
            ViewBag.SemesterId = new SelectList(db.Semesters, "SemesterId", "SemesterNYear", courseforstudentsacademic.SemesterId);
            ViewBag.StudentIdentificationId = new SelectList(db.StudentIdentifications, "StudentIdentificationId", "StudentId", courseforstudentsacademic.StudentIdentificationId);
            return View(courseforstudentsacademic);
        }
        public ActionResult Delete(int? id)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CourseForStudentsAcademic courseforstudentsacademic = db.CourseForStudentsAcademics.Find(id);
            if (courseforstudentsacademic == null)
            {
                return HttpNotFound();
            }
            return View(courseforstudentsacademic);
            ////////////////main code end////////////////////////////////
        }
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CourseForStudentsAcademic courseforstudentsacademic = db.CourseForStudentsAcademics.Find(id);
            db.CourseForStudentsAcademics.Remove(courseforstudentsacademic);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult AdvisedStudentList(int departmentId = 0, int semesterId = 0, int batchNo = 0, int genderId = 0, bool dropStat = false)
        {

            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (!AccessibilitiesOverMethod.AccountAccessRoleStudentAdvisingList("AdvisedStudentList", account.AccountsRole.AccountType)) return RedirectToAction("NotPermitted", "Home");

            ViewBag.departmentId = new SelectList(_departmentManager.GetAllDepartment(), "DepartmentId",
                "DepartmentName", "ASchool.SchoolName", departmentId);
            ViewBag.semesterId = new SelectList(_semesterManager.GetAllSemester(), "SemesterId", "SemesterNYear",
                     _semesterManager.GetActiveSemester().SemesterId);
            ViewBag.genderId = new SelectList(_commonAttributeManager.GetAllGenders(), "GenderId", "GenderName");

            if (departmentId > 0 && semesterId > 0)
            {
                var getAllStudent =
                    _courseForStudentManger.GetAllCourseForStudentsAcademicsByDeptAndSemester(departmentId,
                        semesterId).Include(s => s.StudentIdentification);
                if (dropStat)
                {
                    ViewBag.checkbox = "true";
                    getAllStudent = getAllStudent.Where(s => s.CourseStatusId == 4);

                }
                var getAllStudent2 = Enumerable.ToList(getAllStudent.DistinctBy(s => s.StudentIdentificationId).Select(s => s.StudentIdentification));
                ViewBag.totalStudentCount = Enumerable.Count<CourseForStudentsAcademic>(getAllStudent.DistinctBy(s => s.StudentIdentificationId));
                ViewBag.totalMaleStudentCount = getAllStudent2.Count(s => s.StudentInfo.GenderId == 1);
                ViewBag.TotalFemaleStudentCount = getAllStudent2.Count(s => s.StudentInfo.GenderId == 2);
                if (Queryable.Any<CourseForStudentsAcademic>(getAllStudent))
                {
                    var sortlist = from studentIdentification in getAllStudent2
                                   where studentIdentification.StudentInfo != null
                                   select new AdvisedStudent
                                   {
                                       StudentIdentificationId = studentIdentification.StudentIdentificationId,
                                       StudentId = studentIdentification.StudentId,
                                       GenderId = studentIdentification.StudentInfo.GenderId,
                                       GenderName = studentIdentification.StudentInfo.Genders.GenderName,
                                       StudentName = studentIdentification.StudentInfo.StudentName,
                                       BatchNo = studentIdentification.Semester.BatchNo,
                                       TakenCredits = GetTakenCreditByStudent(departmentId, semesterId, studentIdentification.StudentIdentificationId)
                                   };
                    var batchSorting = Enumerable.ToList(sortlist.DistinctBy(s => s.BatchNo));
                    string sortingBasedOn = _departmentManager.GetSingleDepartment(departmentId).DepartmentName + ", " +
                        _semesterManager.GetSingleSemester(semesterId).SemesterNYear;
                    var batchList = batchSorting.Select(advisedStudent => new AdvisingCountByBatch
                    {
                        BatchNo = advisedStudent.BatchNo,
                        BatchDetails = advisedStudent.BatchNo + " (" + sortlist.Count(s => s.BatchNo == advisedStudent.BatchNo) + " Students)"
                    }).ToList();

                    if (batchNo > 0)
                    {
                        sortlist = sortlist.Where(s => s.BatchNo == batchNo);
                        sortingBasedOn = sortingBasedOn + " and " + batchNo + " batch.";
                        ViewBag.totalStudentCount = sortlist.Count();
                        ViewBag.totalMaleStudentCount = sortlist.Count(s => s.GenderId == 1);
                        ViewBag.TotalFemaleStudentCount = sortlist.Count(s => s.GenderId == 2);
                    }

                    if (genderId > 0)
                    {
                        sortlist = sortlist.Where(s => s.GenderId == genderId);
                        sortingBasedOn = sortingBasedOn + ".";

                    }
                    int countBatches = batchSorting.Count();
                    ViewBag.batchCount = countBatches;
                    ViewBag.batchNo = new SelectList(batchList, "BatchNo", "BatchDetails", batchNo);
                    ViewBag.sortingBasedOn = sortingBasedOn;
                    ViewBag.SelectedSemester = _semesterManager.GetSingleSemester(semesterId);
                    ViewBag.showingStudent = "Showing " + sortlist.Count() + " results.";
                    return View(sortlist);
                }
            }
            ViewBag.batchNo = new SelectList("", "BatchNo", "BatchDetails", batchNo);
            return View(new List<AdvisedStudent>());
        }
        public PartialViewResult _GetAdvisedPartial(int studentIdentityId = 0, int semesterId = 0)
        {
            if (studentIdentityId > 0 && semesterId > 0)
            {
                var getStudent = _studentManager.GetSingleStudentByStudentIdentification(studentIdentityId);
                ViewBag.studentObj = getStudent;
                var getSortCourses = from course in _courseForStudentManger.GetAllCourseByStudent(studentIdentityId).Where(s => s.SemesterId == semesterId)
                                     select new AdvisedCourse
                                     {
                                         CourseCode = course.CourseForDepartment.CourseCode,
                                         CourseTitle = course.CourseForDepartment.CourseName,
                                         Credits = course.CourseForDepartment.Credit
                                     };
                if (getSortCourses.Any())
                {
                    ViewBag.totalCredits = getSortCourses.Sum(s => s.Credits);
                    ViewBag.sem = semesterId;
                    return PartialView("_GetAdvicedPartial", getSortCourses.ToList());
                }

            }
            ViewBag.msg = "No Advised Course";
            return PartialView("_GetAdvicedPartial", new List<AdvisedCourse>());

        }
        public double GetTakenCreditByStudent(int deptId = 0, int semesterId = 0, int studentIdentity = 0)
        {
            return
                _courseForStudentManger.GetAllCourseForStudentsAcademicsByDeptAndSemester(deptId, semesterId)
                    .Where(s => s.StudentIdentificationId == studentIdentity)
                    .Sum(s => s.CourseForDepartment.Credit);
        }
        public ActionResult AddCoursesModified()
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin") return RedirectToAction("NotPermitted", "Home");

            return View();
        }
        [HttpPost]
        public ActionResult AddCoursesModified(string studentIdentity)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin") return RedirectToAction("NotPermitted", "Home");

            var getAdvisingSemester = _semesterManager.GetAdvisingActivatedSemester();
            var findStudent = _studentManager.GetSingleStudentByStudentId(studentIdentity);
            if (findStudent != null)
            {
                var getOfferCourses = _courseForStudentManger.GetAllOfferedCoursesList(findStudent.StudentIdentificationId);
                ViewBag.pendingCourses =
                    _courseForStudentManger.GetAllOtherAndFailedCourses(findStudent.StudentIdentificationId);
                ViewBag.ImporveCourses =
                    _courseForStudentManger.GetCoursesForImprovement(findStudent.StudentIdentificationId);
                ViewBag.studentinfo = findStudent;
                return View(getOfferCourses);
            }
            return View();

        }
        public string AddCoursesForStudent2(int courseCode, int studentId)
        {
            int semesterId = _semesterManager.GetAdvisingActivatedSemester().SemesterId;
            string courseName = db.CourseForDepartments.Find(courseCode).CourseName;
            string msg;
            var sb = new StringBuilder();
            try
            {
                if (CheckCourseStatusForEnroll(courseCode, studentId, "new"))
                {
                    AddCourseForStudentAcademic(courseCode, studentId, semesterId, 1);
                    msg = " " + courseName + " Added";
                    sb.Append("<font color='Green'><b>" + msg + "<b></font>");
                }
                else
                {
                    msg = " " + courseName + " Already Taken";
                    sb.Append("<font color='orange'><b>" + msg + "<b></font>");
                }
            }
            catch (DbException)
            {
                msg = "Something went wrong.";
                sb.Append("<font color='red'><b>" + msg + "<b></font>");

            }
            return sb.ToString();
        }
        public string RemoveCourseFromAdvising2(int courseId)
        {
            //if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            //string role = Session["Role"].ToString();
            //if (role != "Admin" && role != "Advisor") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            var studentAndCourseCheck = _courseForStudentManger.GetSingleCourseForStudentAcademic(courseId);
            var sb = new StringBuilder();
            if (studentAndCourseCheck != null)
            {
                if (studentAndCourseCheck.PaymentRegistrationId == null)
                {
                    try
                    {
                        CourseForStudentsAcademic courseforstudentsacademic =
                        db.CourseForStudentsAcademics.Find(courseId);
                        db.CourseForStudentsAcademics.Remove(courseforstudentsacademic);
                        db.SaveChanges();
                        sb.Append("<font color='Green'><b>Successfully Removed Course!<b></font>");
                        //message = "";
                    }
                    catch (DbException)
                    {
                        sb.Append("<font color='Red'><b>Operation Failed!<b></font>");

                    }
                }
                else
                {
                    sb.Append("<font color='orange'><b>Sorry, Cannot remove course because payment has been created!<b></font>");
                }

            }
            return sb.ToString();
        }
        public PartialViewResult AdvisedCourses(int studentId)
        {
            var getAdvisingSemester = _semesterManager.GetAdvisingActivatedSemester();
            var getAllAdvisedCourses = _courseForStudentManger.GetAllNewAdvisedCoursesOfAStudent(studentId,
                getAdvisingSemester.SemesterId);
            double credits = 0;
            ViewBag.TotalCreditTaken = 0;
            if (getAllAdvisedCourses.Any())
            {
                credits = getAllAdvisedCourses.Sum(s => s.CourseForDepartment.Credit);
                ViewBag.TotalCreditTaken = credits;
            }
            return PartialView("_advisedCourses", getAllAdvisedCourses);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
