﻿using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO;

namespace UMS_MVC.Controllers.CourseSectionMatters
{
    public class CourseController : Controller
    {
        private UmsDbContext db ;
        private readonly CourseForDepartmentManager _courseForDepartmentManager;
        private readonly SchoolManager _schoolManager=new SchoolManager();
        private readonly DepartmentManager _departmentManager;
        private readonly SemesterManager _semesterManager;
        private readonly SectionManager _sectionManager;
        private readonly CourseForStudentManger _courseForStudentManger;

        public CourseController()
        {
            db = new UmsDbContext();
            _courseForDepartmentManager = new CourseForDepartmentManager();
            _departmentManager = new DepartmentManager();
            _semesterManager = new SemesterManager();
            _sectionManager=new SectionManager();
            _courseForStudentManger=new CourseForStudentManger();
        }



        public ActionResult Index()
        {
            if (Session["Role"] == null || Session["User"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            ViewBag.DepartmentId = new SelectList(_departmentManager.GetAllDepartment(), "DepartmentId",
                "DepartmentName", "ASchool.SchoolName", 0);
            return View();
        }

        public PartialViewResult CourseIndexpartial(int id)
        {
            var account2 = (Account)Session["UserObj"];
            if (account2 == null)
                return PartialView("_notPermittedPartial");
            ViewBag.Role = false;
            if (account2.AccountsRole.AccountsRoleId == 1)
            {
                ViewBag.Role = true;
            }
            if (id > 0)
            {
                var getAllCourseForDepartment = _courseForDepartmentManager.GetCourseforDeptBydeptIdFiltered(id);
                var checkDept = getAllCourseForDepartment.Count(s => s.DepartmentId == id) >= 1;
                if (checkDept)
                {
                    ViewBag.TotalCredit =
                         getAllCourseForDepartment.Where(s => s.SerializedSemester.SemesterName != "Semester 0").Sum(s => s.Credit);
                    ViewBag.deptObj = _departmentManager.GetSingleDeptFiltered(id);

                    
                }
                return PartialView("_CourseListByDepartment", getAllCourseForDepartment.ToList());
            }
            return PartialView("_CourseListByDepartment", null);
        }

        public ActionResult Edit(int id = 0)
        {
            if (Session["Role"] == null || Session["User"] == null) return PartialView("_SessionOutPartial");
            var account2 = (Account) Session["UserObj"];
            if (account2.AccountsRole.AccountType != "Admin")
                return PartialView("_notPermittedPartial");

            //////////////// main code start /////////////////////////////
            var courseForDept=new CourseForDepartment();
            if (id != 0)
            {
                 courseForDept = _courseForDepartmentManager.GetSingleCourseFiltered(id);
                ViewBag.includedCourses = _sectionManager.GetAllSectionByCourseFiltered(id).Count();
            }
            TempData["advising"]=
            TempData["DepartmentId"] = new SelectList(_departmentManager.GetAllDepartment(), "DepartmentId",
                "DepartmentName", courseForDept.DepartmentId);
            TempData["SerializedSemesterId"] = new SelectList(_semesterManager.GetAllSerializedSemesters(),
                "SerializedSemesterId", "SemesterName", courseForDept.SerializedSemesterId);
            return PartialView("_EditPartial", courseForDept);
            ////////////////main code end////////////////////////////////
        }

        public string AdvisingStudentCountForThisCourse(int id)
        {
            var getAdvisings =
                   _courseForStudentManger.GetAllCourseForStudentsAcademicsFiltered().Include(s => s.StudentIdentification)
                       .Where(s => s.CourseForDepartmentId == id);
            string studentList = "Total " + getAdvisings.Count()+". Students :";
            foreach (var course in getAdvisings)
            {
                studentList += course.StudentIdentification.StudentId + ", ";
            }
            return studentList;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public string EditSubmit(CourseForDepartment coursefordepartment)
        {
            if (Session["Role"] == null || Session["User"] == null) return "Log in First";
            var account2 = (Account)Session["UserObj"];
            if (account2.AccountsRole.AccountType != "Admin")
                return "Not Permitted";
            string msg = null;
            try
            {
                if (ModelState.IsValid)
                {
                    if (coursefordepartment.CourseForDepartmentId != 0)
                    {
                        db.Entry(coursefordepartment).State = EntityState.Modified;
                    }
                    else
                    {
                        db.CourseForDepartments.Add(coursefordepartment);
                    }
                    db.SaveChanges();
                    msg = "Update Success";

                }
            }
            catch (DbException)
            {
                msg = "Update Failed";

            }
           
            //ViewBag.DepartmentId = new SelectList(_departmentManager.GetAllDepartment(), "DepartmentId", "DepartmentName", coursefordepartment.DepartmentId);
            //ViewBag.SerializedSemesterId = new SelectList(_semesterManager.GetAllSerializedSemesters(), "SerializedSemesterId", "SemesterName", coursefordepartment.SerializedSemesterId);
            return msg;
        }


        public ActionResult Delete(int? id)
        {
            if (Session["Role"] == null || Session["User"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account2 = (Account) Session["UserObj"];
            if (account2.AccountsRole.AccountType != "Admin")
                return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CourseForDepartment coursefordepartment = db.CourseForDepartments.Find(id);
            if (coursefordepartment == null)
            {
                return HttpNotFound();
            }
            return View(coursefordepartment);
            ////////////////main code end////////////////////////////////
        }

        // POST: /Course/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account2 = (Account)Session["UserObj"];
            if (account2.AccountsRole.AccountType != "Admin" )
                return RedirectToAction("NotPermitted", "Home");
            CourseForDepartment coursefordepartment = db.CourseForDepartments.Find(id);
            db.CourseForDepartments.Remove(coursefordepartment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
