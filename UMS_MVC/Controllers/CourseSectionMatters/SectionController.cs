﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PagedList;
using UMS_MVC.Controllers.OtherSpecialControllers;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.BLL;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.ViewModels.ViewModels;
using UMS_MVC.ViewModels.ViewModels.GradingForSection;

namespace UMS_MVC.Controllers.CourseSectionMatters
{
    public class SectionController : Controller
    {
        private UmsDbContext db;
        private readonly TeacherManager _teacherManager;
        private readonly SectionManager _sectionManager;
        private readonly SemesterManager _semesterManager;
        private readonly CourseForDepartmentManager _courseForDepartmentManager;
        private readonly CourseForStudentManger _courseForStudentManger;
        private readonly DepartmentManager _departmentManager;
        private readonly UserActivityManager _userActivityManager;
        private readonly SemesterBusinessLogic _semesterBusinessLogic;
        private readonly SectionBusinessLogic _sectionBusinessLogic;
        private readonly GradingSystemManager _gradingSystemManager;

        public SectionController()
        {

            db = new UmsDbContext();
            _teacherManager = new TeacherManager();
            _sectionManager = new SectionManager();
            _semesterManager = new SemesterManager();
            _courseForDepartmentManager = new CourseForDepartmentManager();
            _courseForStudentManger = new CourseForStudentManger();
            _departmentManager = new DepartmentManager();
            _userActivityManager = new UserActivityManager();
            _semesterBusinessLogic = new SemesterBusinessLogic();
            _sectionBusinessLogic = new SectionBusinessLogic();
            _gradingSystemManager = new GradingSystemManager();
        }

        public ActionResult Index(int? page, int semesterId = 0, int departmentId = 0, int courseId = 0, int teacherId = 0, int searchSecId = 0)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (!AccessibilitiesOverMethod.AccountAccessRoleSectionCon("SectionIndex", account.AccountsRole.AccountType)) return RedirectToAction("NotPermitted", "Home");

            //////////////// main code start /////////////////////////////
            int pageNumber;
            int pageSize;
            ViewBag.UserId = account.AccountId;
            if (account.AccountsRole.AccountType == "Admin" || account.AccountsRole.AccountType == "Support")
            {
                TempData["accessStatus"] = true;
            }
            else
            {
                TempData["accessStatus"] = false;
            }
            var getAllCourse = _courseForDepartmentManager.GetAllCourseForDepartmentsWithCode(departmentId);
            var getAllSemester = _semesterManager.GetAllSemesterForDropdown();
            TempData["semesterId"] = new SelectList(getAllSemester, "SemesterId", "SemesterNYear");
            TempData["departmentId"] = new SelectList(_departmentManager.GetAllDepartmentForDropdownSimple(), "DepartmentId", "DepartmentName", "SchoolName", 0);
            var sections = _sectionManager.GetAllSections().OrderBy(d => d.SectionId);
            var findCurrentSemester = _semesterManager.GetActiveSemester();
            var getAllTeacher = FilteredTeachers();
            TempData["teacherList"] = new SelectList(_sectionBusinessLogic.FilteredTeachers(), "TeacherId", "Account.Name", "Department.DepartmentTitle", 0);
            TempData["courseId"] = new SelectList(getAllCourse, "CourseForDepartmentId", "CourseName", "SerializedSemester.SemesterName", 0);
            if (searchSecId > 0)
            {
                sections = sections.Where(s => s.SectionId.Equals(searchSecId)).OrderBy(d => d.SectionId);
                if (sections.Any())
                {
                    pageSize = 45;
                    pageNumber = (page ?? 1);
                }
                else
                {
                    pageSize = 1;
                    pageNumber = (page ?? 1);
                }

                return View(sections.ToPagedList(pageNumber, pageSize));
            }
            if (semesterId > 0)
            {
                sections = sections.Where(s => s.SemesterId == semesterId).OrderBy(d => d.SectionId);
                TempData["semesterId"] = new SelectList(getAllSemester, "SemesterId", "SemesterNYear", semesterId);
                ViewBag.selectedSemester = semesterId;
            }
            else
            {
                sections = sections.Where(s => s.SemesterId == findCurrentSemester.SemesterId).OrderBy(d => d.SectionId);
                TempData["semesterId"] = new SelectList(getAllSemester, "SemesterId", "SemesterNYear", findCurrentSemester.SemesterId);
            }
            if (departmentId > 0)
            {
                sections = sections.Where(s => s.CourseForDepartment.DepartmentId == departmentId).OrderBy(d => d.SectionId);
                TempData["departmentId"] = new SelectList(_departmentManager.GetAllDepartmentForDropdownSimple(), "DepartmentId", "DepartmentName", "SchoolName", departmentId);
                ViewBag.selectedDepartment = departmentId;
            }
            if (courseId > 0)
            {
                sections = sections.Where(s => s.CourseForDepartment.CourseForDepartmentId == courseId).OrderBy(d => d.SectionId);
                //TempData["courseId"] = new SelectList(getAllCourse, "CourseForDepartmentId", "CourseName", "SerializedSemester.SemesterName", courseId);
                TempData["courseId"] = new SelectList(getAllCourse, "CourseForDepartmentId", "CourseName", "SerializedSemester.SemesterName", courseId);
                ViewBag.selectedCourse = courseId;
            }
            if (teacherId > 0)
            {
                sections = sections.Where(s => s.TeacherId == teacherId).OrderBy(d => d.SectionId);
            }
            if (!sections.Any())
            {
                ViewBag.EmptyMsg = "No result";
            }
            else
            {
                ViewBag.TotalSections = sections.Count();
            }
            if (sections.Count() > 0)
            {
                pageSize = 45;
                pageNumber = (page ?? 1);
            }
            else
            {
                pageSize = 1;
                pageNumber = (page ?? 1);
            }

            return View(sections.ToPagedList(pageNumber, pageSize));
            ////////////////main code end////////////////////////////////
        }

        public IEnumerable<Teacher> FilteredTeachers()
        {
            var getCurretSemester = _semesterManager.GetActiveSemester();
            var getAllTeacher = _teacherManager.GetAllTeacher().Where(s => s.AccountId != null).AsEnumerable();
            var getAllSections = _sectionManager.GetAllSections().AsEnumerable().ToList();
            var sortedTeacher = new List<Teacher>();
            foreach (var teach in getAllTeacher.ToList())
            {
                var countSections = getAllSections.Count(s => s.TeacherId == teach.TeacherId);
                var countSectionsCurrentSemester = getAllSections.Count(s => s.TeacherId == teach.TeacherId && s.SemesterId == getCurretSemester.SemesterId);
                var teacher = new Teacher
                {
                    TeacherId = teach.TeacherId,
                    Account = new Account()
                    {
                        Name = teach.Account.Name + " - [ " + countSectionsCurrentSemester + "/" + countSections + " ]"
                    }

                };
                sortedTeacher.Add(teacher);
            }
            return sortedTeacher;
        }
        //public List<TeacherAccountViewModel> FilteredTeachers()
        //{
        //    var getCurretSemester = _semesterManager.GetActiveSemester();
        //    var getAllTeacher = _teacherManager.GetAllTeacherFiltered();
        //    var getAllSections = _sectionManager.GetAllSectionsFiltered();
        //    var sortedTeacher = new List<TeacherAccountViewModel>();
        //    foreach (var teach in getAllTeacher.ToList())
        //    {
        //        var countSections = getAllSections.Count(s => s.TeacherId == teach.TeacherId);
        //        var countSectionsCurrentSemester = getAllSections.Count(s => s.TeacherId == teach.TeacherId && s.SemesterId == getCurretSemester.SemesterId);
        //        var teacher = new TeacherAccountViewModel
        //        {
        //            TeacherId = teach.TeacherId,
        //            AccountId = (int) teach.AccountId,
        //            Name = teach.Account.Name + " - [ " + countSectionsCurrentSemester + "/" + countSections + " ]"
        //        };
        //        sortedTeacher.Add(teacher);
        //    }
        //    return sortedTeacher;
        //}

        public JsonResult GetSortedCourseList(int departmentId = 0)
        {
            var sortCourseList = _courseForDepartmentManager.GetAllCourseForDepartmentsByDepartment(departmentId);
            return Json(new SelectList(sortCourseList, "CourseForDepartmentId", "CourseName"), JsonRequestBehavior.AllowGet);
        }
        
        public PartialViewResult GetStudentListModalView(int sectionId = 0)
        {
            string msg = "";
            if (Session["UserObj"] != null)
            {

                var account = (Account)Session["UserObj"];
                bool access = account.AccountsRole.AccountType == "Admin" || account.AccountsRole.AccountType == "Exam" || account.AccountsRole.AccountType == "ExamCtrl" || account.AccountsRole.AccountType == "Support";
                bool secondAccess = account.AccountsRoleId==1 ||
                                    account.AccountsRoleId==13;
                ViewBag.accessLvl = access;
                ViewBag.accessLvlSecondary = secondAccess;
                var getSectionStatusView = _sectionBusinessLogic.ManageSectionStatusModel(sectionId);
                if (getSectionStatusView != null)
                {
                    if (getSectionStatusView.GradingSystem != null)
                    {


                        return PartialView("StudentListofaSectionModalView", getSectionStatusView);
                    }
                    else
                    {
                        msg = "Grading System not Defined for this Section!";

                    }

                }
                else
                {
                    msg = "No student in this section.";
                }
            }

            ViewBag.emptyStudentListmsg = msg;
            return PartialView("StudentListofaSectionModalView", new SectionStatusModel());
        }
        
        public ActionResult GetCountOfStudentInSection(int sectionId = 0)
        {
            if (sectionId > 0)
            {
                var countTotalStudent = _courseForStudentManger.GetAllCourseForStudentsAcademicsBysection(sectionId).Count();
                ViewBag.rst = countTotalStudent;
                return View();
            }
            ViewBag.rst = "No Student";
            return View();
        }

        public PartialViewResult GetCoutOfGradedStudentInSection(int sectionId = 0)
        {
            if (sectionId > 0)
            {
                // var countTotalStudent = _courseForStudentManger.GetAllCourseForStudentsAcademicsBysection(sectionId).Count(s=>s.);
                var countTotalStudent = (
                    from secCount in _courseForStudentManger.GetAllCourseForStudentsAcademicsBysection(sectionId)
                    where !string.IsNullOrEmpty(secCount.LetterGrade)
                    select secCount).Count();
                ViewBag.gcr = countTotalStudent;
                return PartialView();
            }
            return PartialView();
        }

        public ActionResult Details(int? id)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (!AccessibilitiesOverMethod.AccountAccessRoleSectionCon("SectionDetails", account.AccountsRole.AccountType)) return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Section section = db.Sections.Find(id);
            if (section == null)
            {
                return HttpNotFound();
            }
            return View(section);
            ////////////////main code end////////////////////////////////
        }

        // GET: /Section/Create
        public ActionResult CreateUpdate(int secId = 0)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (!AccessibilitiesOverMethod.AccountAccessRoleSectionCon("SectionCreate", account.AccountsRole.AccountType)) return RedirectToAction("NotPermitted", "Home");
            var secObj = new Section
            {
                SectionId = 0,
                TeacherId = 0,
                SemesterId = 0,
                GradingSystemId = 0,
                CourseForDepartmentId = 0,
                EvaluationPossible = false,
                CourseForDepartment = new CourseForDepartment
                {
                    DepartmentId = 0
                }
            };
            if (secId > 0)
            {
                var getSection = _sectionManager.GetSingleSection(secId);
                secObj = getSection;
            }
            TempData["DepartmentId"] = new SelectList(_departmentManager.GetAllDepartmentFiltered(), "DepartmentId", "DepartmentName", secObj.CourseForDepartment.DepartmentId);
            TempData["TeacherId"] = new SelectList(_teacherManager.GetAllTeacherFiltered().Include(s => s.Account).Include(s => s.Department), "TeacherId", "Account.Name", "Department.DepartmentName", secObj.TeacherId);
            TempData["SemesterId"] = new SelectList(_semesterManager.GetAllSemesterForDropdown(), "SemesterId", "SemesterNYear", secObj.SemesterId);
            TempData["gradingSystems"] = new SelectList(_gradingSystemManager.GetAllGradingSystems(), "GradingSystemId", "GradingSystemName", secObj.GradingSystemId);
            return View(secObj);
            ////////////////main code end////////////////////////////////
        }
        public int GetGradingSystems(int courseId)
        {
            int selectedVal = 0;
            var getCourseType = _courseForDepartmentManager.GetSingleCourseFiltered(courseId).CourseType.ToLower();
            switch (getCourseType)
            {
                case "core":
                    selectedVal = 1;
                    break;
                case "lab":
                    selectedVal = 2;
                    break;
                case "other":
                    selectedVal = 3;
                    break;
            }
            return selectedVal;
        }

        public PartialViewResult GetAdvisngStatusByCourseSemester(int courseId, int semId)
        {
            if (courseId > 0 && semId > 0)
            {
                var getAdvising = _courseForStudentManger.GetAllCourseForStudentsAcademicsFiltered()
                    .Where(s => s.CourseForDepartmentId == courseId && s.SemesterId == semId);
                var getCourseInfo = _courseForDepartmentManager.GetSingleCourseFiltered(courseId);
                var getSemesterInfo = _semesterManager.GetSingleSemester(semId);

                var getSections = Enumerable.AsEnumerable<Section>(_sectionManager.GetAllSectionByCourseCodeAndSemesterFiltered(courseId, semId).Include(s => s.Teacher.Account)).Select(s => new SectionPartialStatusViewModel
                {
                    TeacherName = s.Teacher.Account.Name,
                    TotalStudents = getAdvising.Count(c => c.SectionId == s.SectionId),
                    DateCreated = s.CreatedDate
                }).ToList();
                var asectionStat = new CourseStatusByAdvising
                {
                    CourseCode = getCourseInfo.CourseCode,
                    CourseTitle = getCourseInfo.CourseName,
                    SemesterSerial = getSemesterInfo.SemesterNYear,

                    TotalEnrolled = getAdvising.Count(),
                    SectionEnrolled = getAdvising.Count(s => s.SectionId != null),
                    SectionNotEnrolled = getAdvising.Count(s => s.SectionId == null)
                };
                ViewBag.sectionStat = asectionStat;
                ViewBag.sectionLists = getSections;

            }
            else
            {
                ViewBag.sectionStat = null;
                ViewBag.sectionLists = null;
            }

            return PartialView("_SectionAdvisingDetailPartial");
        }

        public string GetCourseTypeName(int courseId)
        {
            var courseTypeName = _courseForDepartmentManager.GetSingleCourseFiltered(courseId).CourseType;
            return "'" + courseTypeName + "' Type Course.";
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public string CreateCommit(Section section, string save)
        {
            string msg = "";
            if (Session["UserObj"] == null) return "Session out, Log In to continue.";
            var account = (Account)Session["UserObj"];
            if (!AccessibilitiesOverMethod.AccountAccessRoleSectionCon("SectionCreate", account.AccountsRole.AccountType)) return "Not permitted";
            try
            {
                if (section.GradingSystemId == null)
                    return "Choose Grading system properly";
                switch (save)
                {
                    case "Alter":
                        if (section.SectionId > 0)
                        {
                            msg = EditCommit(section);
                        }
                        else
                        {
                            msg = AddCommit(section);
                        }
                        break;
                    case "Delete":
                        msg = DeleteConfirmed(section.SectionId);
                        break;
                }

            }
            catch (DbException)
            {

                msg = "Internal server error. Contact to Admin User.";
            }
            return msg;
        }

        public string AddCommit(Section section)
        {
            string msg;
            if (CheckForSameSection(section))
            {
                section.CreatedDate = DateTime.Now;
                db.Sections.Add(section);
                db.SaveChanges();
                msg = "Section Created Successfully";
            }
            else
            {
                msg = "Section name '" + section.SectionName + "' already exists for this course!!";
            }
            return msg;
        }

        public string EditCommit(Section section)
        {

            try
            {
                db.Entry(section).State = EntityState.Modified;
                db.SaveChanges();
                var getStudentListinThisSection =
                    db.CourseForStudentsAcademics.Where(s => s.SectionId == section.SectionId);
                if (getStudentListinThisSection.Count(s => s.CourseForDepartmentId != section.CourseForDepartmentId) > 0)
                {
                    foreach (var courseForStudentsAcademic in getStudentListinThisSection)
                    {
                        courseForStudentsAcademic.SectionId = null;
                        db.Entry(courseForStudentsAcademic).State = EntityState.Modified;
                    }
                    db.SaveChanges();
                }
                return "Section Updated Successfully.";
            }
            catch (DbException)
            {
                return "Update Failed! Contact Admin User";
            }
        }
        public string DeleteConfirmed(int id)
        {
            string msg = "";
            try
            {
                Section section = db.Sections.Find(id);
                db.Sections.Remove(section);
                var getStudentListinThisSection =
                    db.CourseForStudentsAcademics.Where(s => s.SectionId == section.SectionId);
                foreach (var courseForStudentsAcademic in getStudentListinThisSection)
                {
                    courseForStudentsAcademic.SectionId = null;
                    db.Entry(courseForStudentsAcademic).State = EntityState.Modified;
                }
                db.SaveChanges();
                msg = "Delete Successfull.";
            }
            catch (DbException)
            {
                msg = "Internal Server Error. Contact to Admin User.";
            }
            return msg;
        }
        public PartialViewResult SortCourseList(int deptId = 0, int selectedCourse = 0)
        {
            ViewBag.CourseForDepartmentId = new SelectList(_courseForDepartmentManager.GetAllCourseForDepartmentsWithCode(deptId), "CourseForDepartmentId", "CourseName", "SerializedSemester.SemesterName", selectedCourse);
            return PartialView("~/Views/Section/_SortCourseList.cshtml");
            //ViewBag.CourseForDepartmentId = new SelectList(courses, "CourseForDepartmentId", "CourseName","SerializedSemester.SemesterName",0);
            //return PartialView("~/Views/Section/_SortCourseList.cshtml");
        }

        public bool CheckForSameSection(Section section)
        {
            var checkResult =
                _sectionManager.GetAllSectionsFiltered()
                    .Count(
                        s =>
                            s.SectionName == section.SectionName &&
                            s.CourseForDepartmentId == section.CourseForDepartmentId && s.SemesterId == section.SemesterId) == 0;
            if (checkResult && !string.IsNullOrEmpty(section.SectionName))
            {
                return true;
            }
            return false;
        }



        // GET: /Section/Edit/5
        public ActionResult Edit(int? id)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (!AccessibilitiesOverMethod.AccountAccessRoleSectionCon("SectionEdit", account.AccountsRole.AccountType)) return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Section section = db.Sections.Find(id);
            if (section == null)
            {
                return HttpNotFound();
            }
            ViewBag.CourseForDepartmentId = new SelectList(_courseForDepartmentManager.GetAllCourseForDepartmentFiltered().OrderBy(s => s.CourseForDepartmentId), "CourseForDepartmentId", "CourseName", "Department.DepartmentName", section.CourseForDepartmentId);
            ViewBag.SemesterId = new SelectList(_semesterManager.GetAllSemester(), "SemesterId", "SemesterNYear", section.SemesterId);
            ViewBag.DepartmentId = new SelectList(_departmentManager.GetAllDepartmentFiltered(), "DepartmentId", "DepartmentName");
            ViewBag.TeacherId = new SelectList(_teacherManager.GetAllTeacherFiltered(), "TeacherId", "Account.Name", section.TeacherId);
            return View(section);
            ////////////////main code end////////////////////////////////
        }



        // GET: /Section/Delete/5
        public ActionResult Delete(int? id)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (!AccessibilitiesOverMethod.AccountAccessRoleSectionCon("SectionDelete", account.AccountsRole.AccountType)) return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Section section = db.Sections.Find(id);
            if (section == null)
            {
                return HttpNotFound();
            }
            return View(section);
            ////////////////main code end////////////////////////////////
        }



        public ActionResult HighlightedSectionIndex()
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (!AccessibilitiesOverMethod.AccountAccessRoleSectionCon("HilightSectionIndex", account.AccountsRole.AccountType)) return RedirectToAction("NotPermitted", "Home");
            TempData["semesterId"] = new SelectList(_semesterBusinessLogic.GetAllHighlightedSectionDropDown(), "SemesterId", "SemesterNYear");
            return View();
        }

        public PartialViewResult HighLightedSectionPartialIndex(int id = 0)
        {
            if (id > 0)
            {
                var getAllSections = _sectionManager.GetAllHighLightedSection().Where(s => s.SemesterId == id);
                return PartialView("_HighLightedSectionPartialIndex", getAllSections.ToList());
            }
            return PartialView("_HighLightedSectionPartialIndex", new List<Section>());
        }

        public ActionResult UpdateHighlightedSection(int id = 0)
        {

            var findSection = _sectionManager.GetSingleSection(id);
            return PartialView("_UpdateHighlightedSection", findSection);
        }
        [ValidateAntiForgeryToken]
        public string UpdateHighlightedConfirm(Section section)
        {
            if (Session["UserObj"] == null) return "You are not logged in. Log in first";
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Support")
                return "Sorry you are not permitted to do this action.";
            string msgfromServer;
            var findSection = db.Sections.Find(section.SectionId);
            try
            {
                findSection.ShortNote = section.ShortNote;
                findSection.HighLight = section.HighLight;
                findSection.MidTermEx = section.MidTermEx;
                findSection.FinalTermEx = section.FinalTermEx;
                findSection.ExpireDateTime = section.ExpireDateTime;
                db.Entry(findSection).State = EntityState.Modified;
                db.SaveChanges();
                msgfromServer = "Update Done.";
            }
            catch (DbEntityValidationException es)
            {
                var msg = string.Empty;
                foreach (var validationErrors in es.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        msg += Environment.NewLine + string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                //var fail = new Exception(msg, es);
                //throw fail
                msgfromServer = msg;
            }

            return msgfromServer;
        }

        public string DisableHighlightedConfirm(int id = 0)
        {
            if (Session["UserObj"] == null) return "You are not logged in. Log in first";
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Support")
                return "Sorry you are not permitted to do this action.";
            string msgfromServer = "Section Invalid or Empty.";
            if (id > 0)
            {
                try
                {
                    var findSectionData = db.Sections.Find(id);
                    findSectionData.HighLight = false;
                    db.Entry(findSectionData).State = EntityState.Modified;
                    db.SaveChanges();
                    msgfromServer = "Section Highlight disable done.";
                }
                catch (DbEntityValidationException es)
                {
                    var msg = string.Empty;
                    foreach (var validationErrors in es.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            msg += Environment.NewLine + string.Format("Property: {0} Error: {1}",
                                validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                    //var fail = new Exception(msg, es);
                    //throw fail
                    msgfromServer = msg;
                }
            }
            return msgfromServer;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
