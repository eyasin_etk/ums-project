﻿using System.Linq;
using System.Web.Mvc;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.Manager.DAL.DAO;

namespace UMS_MVC.Controllers.CheckControllers
{
	public class CheckController : Controller
	{
		private UmsDbContext db=new UmsDbContext();
		private readonly SectionManager _sectionManager=new SectionManager();
		private readonly AccountManager _accountManager=new AccountManager();
		private readonly  TeacherManager _teacherManager=new TeacherManager();
		public JsonResult CheckSectionName(string SectionName, int CourseForDepartmentId)
		{
			var result = _sectionManager.GetAllSections().Count(s => s.SectionName == SectionName && s.CourseForDepartmentId==CourseForDepartmentId) == 1;
			return Json(result, JsonRequestBehavior.AllowGet);
		}
		//public JsonResult CheckSameEmailForTeacher(string EmailAddress)
		//{
		//    var result = _teacherManager.GetAllTeacher().Count(s => s.EmailAddress == EmailAddress) == 0;
		//    return Json(result, JsonRequestBehavior.AllowGet);
		//}
		public JsonResult CheckSameLoginId(string LoginId)
		{
			var result = _teacherManager.GetAllTeacher().Count(s => s.LoginId == LoginId) == 0;
			return Json(result, JsonRequestBehavior.AllowGet);
		}

		public JsonResult CheckPhoneForAccount(string PhoneNo)
		{
			var result = _accountManager.GetAllAccounts().Count(s => s.PhoneNo==PhoneNo) == 0;
			return Json(result, JsonRequestBehavior.AllowGet);
		}

		//public JsonResult CheckSamePhoneNumberForTeacher(string Phone)
		//{
		//    var result = _teacherManager.GetAllTeacher().Count(s => s.Phone==Phone) == 0;
		//    return Json(result, JsonRequestBehavior.AllowGet);
		//}

		public JsonResult CheckSameEmailForAccount(string Email)
		{
			var result = _accountManager.GetAllAccounts().Count(s => s.Email == Email) == 0;
			return Json(result, JsonRequestBehavior.AllowGet);
		}

		public JsonResult CheckDepartmentName(string DepartmentName)
		{
			var result = db.Departments.Count(x => x.DepartmentName == DepartmentName) == 0;
			return Json(result, JsonRequestBehavior.AllowGet);
		}
		public JsonResult CheckDepartmentCode(int DepartmentCode)
		{
			var result = db.Departments.Count(x => x.DepartmentCode == DepartmentCode) == 0;
			return Json(result, JsonRequestBehavior.AllowGet);
		}
		public JsonResult CheckSchoolName(string SchoolName)
		{
			var result = db.Schools.Count(x => x.SchoolName == SchoolName) == 0;
			return Json(result, JsonRequestBehavior.AllowGet);
		}
	}
}