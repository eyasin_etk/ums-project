﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.Controllers.OtherSpecialControllers
{
    public class SpecialWaiverStudentsController : Controller
    {
        private UmsDbContext db = new UmsDbContext();

        // GET: SpecialWaiverStudents
        public ActionResult Index()
        {
            return View(db.SpecialWaiverStudents.ToList());
        }

        // GET: SpecialWaiverStudents/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SpecialWaiverStudents specialWaiverStudents = db.SpecialWaiverStudents.Find(id);
            if (specialWaiverStudents == null)
            {


                return HttpNotFound();
            }
            return View(specialWaiverStudents);
        }

        // GET: SpecialWaiverStudents/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SpecialWaiverStudents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "specialwaiverID,StudentId,StudentIdentificationId,,waiver,description")] SpecialWaiverStudents specialWaiverStudents)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");

            var account = (Account)Session["UserObj"];

            if (account.AccountsRoleId != 1 && account.AccountsRoleId != 12 && account.AccountsRoleId != 4 && account.AccountsRoleId != 3)
                //|| account.AccountsRoleId != 4 || account.AccountsRoleId != 12)
                return RedirectToAction("NotPermitted", "Home");


            if (ModelState.IsValid)
            {
                specialWaiverStudents.StudentIdentificationId = db.StudentIdentifications.Where(a => a.StudentId == specialWaiverStudents.StudentId).Select(b => b.StudentIdentificationId).FirstOrDefault();

                var studentcheck = db.StudentIdentifications.Find(specialWaiverStudents.StudentIdentificationId);

                if (studentcheck == null || specialWaiverStudents.StudentIdentificationId==0)
                {
                    ViewBag.msg = "Student not found";
                    return View(specialWaiverStudents);
                }
                else if(specialWaiverStudents.StudentIdentificationId!=0 && specialWaiverStudents.waiver!=0)
                {
                    //db.SpecialWaiverStudents.Add(identificationID);
                    db.SpecialWaiverStudents.Add(specialWaiverStudents);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }

            return View(specialWaiverStudents);
        }

        // GET: SpecialWaiverStudents/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SpecialWaiverStudents specialWaiverStudents = db.SpecialWaiverStudents.Find(id);
            if (specialWaiverStudents == null)
            {
                return HttpNotFound();
            }
            return View(specialWaiverStudents);
        }

        // POST: SpecialWaiverStudents/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "specialwaiverID,StudentId,StudentIdentificationId,description")] SpecialWaiverStudents specialWaiverStudents)
        {
            if (ModelState.IsValid)
            {
                db.Entry(specialWaiverStudents).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(specialWaiverStudents);
        }

        // GET: SpecialWaiverStudents/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SpecialWaiverStudents specialWaiverStudents = db.SpecialWaiverStudents.Find(id);
            if (specialWaiverStudents == null)
            {
                return HttpNotFound();
            }
            return View(specialWaiverStudents);
        }

        // POST: SpecialWaiverStudents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SpecialWaiverStudents specialWaiverStudents = db.SpecialWaiverStudents.Find(id);
            db.SpecialWaiverStudents.Remove(specialWaiverStudents);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
