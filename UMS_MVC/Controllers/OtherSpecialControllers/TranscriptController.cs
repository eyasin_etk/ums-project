﻿using System;
using System.Drawing;
using System.Linq;
using System.Web.Mvc;
using Novacode;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.BLL;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.Controllers.OtherSpecialControllers
{
    public class TranscriptController : Controller
    {
        private readonly CourseForStudentManger _courseForStudentManger;
        private readonly GradeManager _gradeManager;
        private readonly StudentManager _studentManager;
        private readonly PowerWorkBusinessLogic _powerWorkBusinessLogic;
        
        public TranscriptController()
        {
            _courseForStudentManger=new CourseForStudentManger();
            _gradeManager=new GradeManager();
            _studentManager=new StudentManager();
            _powerWorkBusinessLogic=new PowerWorkBusinessLogic();
        }

       

        // Finally Updated on 1-8-2016 for Future copy
        public void GenrateTranscriptFinal(int studentId = 0, int type = 1)
        {
            if (Session["UserObj"] != null)
            {
                var account = (Account)Session["UserObj"];
                if (account.AccountsRole.AccountType == "Admin" || !_powerWorkBusinessLogic.CheckMethodForAccess("PrintTransCript", account.AccountId))
                {
                    var getStudent = _studentManager.GetSingleStudent(studentId);
                    var takencourses = _courseForStudentManger.GetAllCourseByStudent(studentId).ToList();
                    var getAllCgpa = _gradeManager.CalculateCgpaAllSemester(studentId).ToList();

                    string physicalPath = Server.MapPath("~/Downloadables/" + "Tes.doc");
                    string physicalPath2 = type == 1
                        ? Server.MapPath("~/Downloadables/" + "TranscriptTemp.docx")
                        : Server.MapPath("~/Downloadables/" + "Transcript-Demo.docx");

                    using (DocX document = DocX.Create(physicalPath))
                    {
                        document.AddHeaders();
                        document.AddFooters();
                        document.ApplyTemplate(physicalPath2);
                        Formatting darkFormatting = new Formatting
                        {
                            Bold = true,
                            Size = 8,
                            FontColor = Color.Brown
                        };
                        Formatting studentInfoFormat = new Formatting
                        {
                            Bold = true,
                            Size = 11,
                            FontFamily = new FontFamily("Calibri Light")
                        };
                        Formatting studentInfoDetailFormat = new Formatting
                        {
                            Size = 11,
                            FontFamily = new FontFamily("Calibri Light"),
                            Bold = true
                        };

                        #region Transcript Title
                        //Paragraph lineParagraph = document.InsertParagraph();
                        //lineParagraph.InsertText("-----------------------------------------------------------------------------------------------------------------------------", false);
                        string studentStatVal = getStudent.StudentId + " " + getStudent.StudentInfo.StudentName;
                        document.ReplaceText("StuNa", studentStatVal);
                        document.ReplaceText("datePri", DateTime.Now.ToShortDateString());
                        Paragraph transcriptTitle = document.InsertParagraph();
                        transcriptTitle.Append("Transcript of Academic Record")
                            .FontSize(20)
                            .Font(new FontFamily("Calibri"))
                            .Bold();
                        transcriptTitle.UnderlineStyle(UnderlineStyle.thick);
                        transcriptTitle.InsertText("\n", false);
                        transcriptTitle.Alignment = Alignment.center;
                        #endregion

                        #region Student_Detail_Table
                        Table studenrInfoTable = document.AddTable(3, 2);
                        studenrInfoTable.Design = TableDesign.TableNormal;
                        studenrInfoTable.AutoFit = AutoFit.Contents;
                        studenrInfoTable.Rows[0].Cells[0].Paragraphs.First()
                            .InsertText("Name", false, studentInfoFormat);
                        studenrInfoTable.Rows[0].Cells[1].Paragraphs.First()
                            .InsertText(": " + getStudent.StudentInfo.StudentName, false, studentInfoDetailFormat);
                        studenrInfoTable.Rows[1].Cells[0].Paragraphs.First()
                            .InsertText("Student's ID", false, studentInfoFormat);
                        studenrInfoTable.Rows[1].Cells[1].Paragraphs.First()
                            .InsertText(": " + getStudent.StudentId, false, studentInfoDetailFormat);
                        studenrInfoTable.Rows[2].Cells[0].Paragraphs.First()
                            .InsertText("Program", false, studentInfoFormat);
                        studenrInfoTable.Rows[2].Cells[1].Paragraphs.First()
                            .InsertText(": " + getStudent.Department.DepartmentName, false, studentInfoDetailFormat);
                        document.InsertTable(studenrInfoTable);

                        #endregion

                        #region Table_Setup

                        var semesterCounter = 1;
                        //var getAllDepartments = db.Departments;
                        foreach (var semester in takencourses.Select(s => s.SemesterId).Distinct())
                        {

                            var semesterOrdinalName = NumericCordianal.AddOrdinal(semesterCounter);
                            var semester1 = semester;
                            var getStudentList = takencourses.Where(s => s.SemesterId == semester1);
                            var getCumulativeScores = getAllCgpa.FirstOrDefault(s => s.SemesterId == semester1);
                            Table table1 = document.AddTable(getStudentList.ToList().Count, 7);
                            //table1.Design = TableDesign.LightListAccent1;

                            //table1.Design = TableDesign.LightShading;
                            table1.Design = TableDesign.LightShading;
                            table1.AutoFit = AutoFit.Contents;
                            table1.Alignment = Alignment.both;

                            Formatting tableTop = new Formatting
                            {
                                Bold = true,
                                Size = 11,
                                FontFamily = new FontFamily("Calibri"),
                                FontColor = Color.Black
                            };
                            Formatting tableTitle = new Formatting
                            {
                                Bold = true,
                                Size = 9,
                                FontFamily = new FontFamily("Calibri Light")
                            };
                            Formatting tableContent = new Formatting
                            {
                                Size = 9,
                                FontFamily = new FontFamily("Calibri Light")
                            };
                            Formatting tableFooter = new Formatting
                            {
                                Bold = true,
                                Size = 8,
                                FontFamily = new FontFamily("Calibri Light")
                            };
                            //Table Header
                            Row headingRowTop = table1.InsertRow(0);
                            headingRowTop.TableHeader = true;
                            headingRowTop.Cells[0].Paragraphs.First().InsertText(semesterOrdinalName, false, tableTop);
                            headingRowTop.Cells[1].Paragraphs.First().InsertText("Semester", false, tableTop);
                            headingRowTop.Cells[2].Paragraphs.First()
                                .InsertText(getStudentList.First().Semester.SemesterNYear + "", false, tableTop);
                            //headingRowTop.Cells[2].Width=30;
                            headingRowTop.Cells[2].Paragraphs.First().Alignment = Alignment.left;
                            headingRowTop.Cells[3].Paragraphs.First().InsertText("", false, tableTop);
                            headingRowTop.Cells[4].Paragraphs.First().InsertText("", false, tableTop);
                            headingRowTop.Cells[5].Paragraphs.First().InsertText("", false, tableTop);
                            headingRowTop.Cells[6].Paragraphs.First().InsertText("", false, tableTop);

                            Row headingRow = table1.InsertRow(1);
                            headingRow.TableHeader = true;
                            //headingRow.BreakAcrossPages = false;
                            headingRow.Cells[0].Paragraphs.First().InsertText("", false, tableTitle);
                            headingRow.Cells[0].Paragraphs.First().Alignment = Alignment.center;
                            headingRow.Cells[1].Paragraphs.First().InsertText("", false, tableTitle);
                            headingRow.Cells[1].Paragraphs.First().Alignment = Alignment.center;
                            headingRow.Cells[2].Paragraphs.First().InsertText("Course Title", false, tableTitle);
                            headingRow.Cells[2].Paragraphs.First().Alignment = Alignment.center;
                            headingRow.Cells[3].Paragraphs.First().InsertText("Credit", false, tableTitle);
                            headingRow.Cells[3].Paragraphs.First().Alignment = Alignment.center;
                            headingRow.Cells[4].Paragraphs.First().InsertText("Letter", false, tableTitle);
                            headingRow.Cells[4].Paragraphs.First().Alignment = Alignment.center;
                            headingRow.Cells[5].Paragraphs.First().InsertText("Grade", false, tableTitle);
                            headingRow.Cells[5].Paragraphs.First().Alignment = Alignment.center;
                            headingRow.Cells[6].Paragraphs.First().InsertText("Cr.Hr.", false, tableTitle);
                            headingRow.Cells[6].Paragraphs.First().Alignment = Alignment.center;

                            Row headingRow2 = table1.InsertRow(2);
                            headingRow2.TableHeader = true;
                            //headingRow.BreakAcrossPages = false;
                            headingRow2.Cells[0].Paragraphs.First().InsertText("Sl.", false, tableTitle);
                            headingRow2.Cells[0].Paragraphs.First().Alignment = Alignment.center;
                            headingRow2.Cells[1].Paragraphs.First().InsertText("Course Code", false, tableTitle);
                            headingRow2.Cells[1].Paragraphs.First().Alignment = Alignment.center;
                            headingRow2.Cells[2].Paragraphs.First().InsertText("___________________________________________________________", false, tableTitle);
                            headingRow2.Cells[2].Paragraphs.First().Alignment = Alignment.left;
                            headingRow2.Cells[3].Paragraphs.First().InsertText("Hours", false, tableTitle);
                            headingRow2.Cells[3].Paragraphs.First().Alignment = Alignment.center;
                            headingRow2.Cells[4].Paragraphs.First().InsertText("Grade", false, tableTitle);
                            headingRow2.Cells[4].Paragraphs.First().Alignment = Alignment.center;
                            headingRow2.Cells[5].Paragraphs.First().InsertText("Point", false, tableTitle);
                            headingRow2.Cells[5].Paragraphs.First().Alignment = Alignment.center;
                            headingRow2.Cells[6].Paragraphs.First().InsertText("*Gr.Pt.", false, tableTitle);
                            headingRow2.Cells[6].Paragraphs.First().Alignment = Alignment.center;
                            //Table Footer
                            //Row cgpafooterRow = table1.InsertRow();
                            //cgpafooterRow

                            int i = 3;
                            int courseCounter = 1;
                            foreach (var student in getStudentList.ToList())
                            {
                                table1.Rows[i].Cells[0].Paragraphs.First()
                                    .InsertText(courseCounter.ToString(), false, tableContent);
                                table1.Rows[i].Cells[0].Paragraphs.First().Alignment = Alignment.center;
                                table1.Rows[i].Cells[1].Paragraphs.First()
                                    .InsertText(student.CourseForDepartment.CourseCode, false, tableContent);
                                table1.Rows[i].Cells[1].Paragraphs.First().Alignment = Alignment.left;
                                table1.Rows[i].Cells[2].Paragraphs.First()
                                    .InsertText(student.CourseForDepartment.CourseName, false, tableContent);
                                table1.Rows[i].Cells[2].Paragraphs.First().Alignment = Alignment.left;
                                //table1.Rows[i].Cells[2].Width = .68;
                                if (student.CourseStatusId != 2)
                                {
                                    table1.Rows[i].Cells[3].Paragraphs.First().InsertText("(0.0)", false, tableContent);
                                }
                                else
                                {
                                    table1.Rows[i].Cells[3].Paragraphs.First()
                                        .InsertText(student.CourseForDepartment.Credit.ToString("0.0"), false,
                                            tableContent);
                                }
                                table1.Rows[i].Cells[3].Paragraphs.First().Alignment = Alignment.center;
                                table1.Rows[i].Cells[4].Paragraphs.First()
                                    .InsertText(student.LetterGrade, false, tableContent);
                                table1.Rows[i].Cells[4].Paragraphs.First().Alignment = Alignment.center;
                                table1.Rows[i].Cells[5].Paragraphs.First()
                                    .InsertText(student.Grade.ToString("0.00"), false, tableContent);
                                table1.Rows[i].Cells[5].Paragraphs.First().Alignment = Alignment.center;
                                table1.Rows[i].Cells[6].Paragraphs.First()
                                    .InsertText(student.TotalGrade.ToString("0.00"), false, tableContent);
                                table1.Rows[i].Cells[6].Paragraphs.First().Alignment = Alignment.center;
                                //table1.AutoFit=AutoFit.ColumnWidth;
                                i++;
                                courseCounter++;
                            }

                            table1.Rows[0].BreakAcrossPages = false;
                            Row footerRow = table1.InsertRow();

                            // Table Style Start
                            //table1.Design = TableDesign.LightShading;
                            //table1.Design = TableDesign.LightShadingAccent2;
                            //table1.AutoFit = AutoFit.Contents;
                            //table1.Alignment = Alignment.both;
                            // Table Style End

                            footerRow.Cells[0].Paragraphs.First().InsertText("Total ", false, tableFooter);
                            footerRow.Cells[0].Paragraphs.First().Alignment = Alignment.right;
                            footerRow.Cells[1].Paragraphs.First()
                                .InsertText("Credit: " + getCumulativeScores.CreditEarned, false, tableFooter);
                            footerRow.Cells[1].Paragraphs.First().Alignment = Alignment.left;
                            footerRow.Cells[2].Paragraphs.First()
                                .InsertText("Total Grade Point: " + getCumulativeScores.TotalGrade, false, tableFooter);
                            footerRow.Cells[2].Paragraphs.First().Alignment = Alignment.left;
                            footerRow.Cells[3].Paragraphs.First().InsertText("GPA: ", false, tableFooter);
                            footerRow.Cells[3].Paragraphs.First().Alignment = Alignment.right;
                            footerRow.Cells[4].Paragraphs.First()
                                .InsertText(getCumulativeScores.SemesterGpa.ToString("0.00"), false, tableFooter);
                            footerRow.Cells[4].Paragraphs.First().Alignment = Alignment.left;
                            footerRow.Cells[5].Paragraphs.First().InsertText("CGPA: ", false, tableFooter);
                            footerRow.Cells[5].Paragraphs.First().Alignment = Alignment.right;
                            footerRow.Cells[6].Paragraphs.First()
                                .InsertText(getCumulativeScores.Cgpa.ToString("0.00"), false, tableFooter);
                            footerRow.Cells[6].Paragraphs.First().Alignment = Alignment.left;
                            Paragraph p = document.InsertParagraph();

                            ////p.SetLineSpacing(LineSpacingType.Line, 1.5f);
                            //p.Append("\n");
                            //p.Append("Semester Details of : " + getStudentList.First().Semester.SemesterNYear + "")
                            //    .Font(new FontFamily("Calibri")).FontSize(11).Bold();
                            #endregion
                            document.InsertTable(table1);
                            semesterCounter++;
                            //document.InsertTable(table1);
                        }
                        //Paragraph p2 = document.InsertParagraph();
                        ////p.SetLineSpacing(LineSpacingType.Line, 1.5f);
                        //p2.Append("\n\n\n");
                        //Table signingTable = document.AddTable(3, 3);
                        //signingTable.Design = TableDesign.TableNormal;
                        //signingTable.AutoFit = AutoFit.Contents;
                        //signingTable.Alignment=Alignment.both;
                        //signingTable.Rows[0].Cells[0].Paragraphs.First().InsertText("Adnan mahmud", false, studentInfoFormat);
                        //signingTable.Rows[0].Cells[1].Paragraphs.First().InsertText("                               ");
                        //signingTable.Rows[0].Cells[2].Paragraphs.First().InsertText("                               ");
                        //signingTable.Rows[1].Cells[0].Paragraphs.First().InsertText("--------------------");
                        //signingTable.Rows[1].Cells[1].Paragraphs.First().InsertText("");
                        //signingTable.Rows[1].Cells[2].Paragraphs.First().InsertText("------------------------------");

                        //signingTable.Rows[2].Cells[0].Paragraphs.First().InsertText("Verified By", false, studentInfoFormat);
                        //signingTable.Rows[2].Cells[1].Paragraphs.First().InsertText("");
                        //signingTable.Rows[2].Cells[2].Paragraphs.First().InsertText("Controller of Examinations",false, studentInfoFormat);

                        //document.InsertTable(signingTable);
                        document.ReplaceText("veriName", account.Name);
                        document.ReplaceText("desigName", "Senior Officer, Exam");
                        string fileNameTemplate = @"C:\Users\Adnan mahmud\Desktop\Transcript-{0}-{1}.docx";
                        Response.Clear();
                        document.SaveAs(Response.OutputStream);
                        Response.AddHeader("content-disposition",
                            "attachment; filename=\"" + getStudent.StudentId + ".doc\"");
                        Response.ContentType = "application/msword";
                    }
                }
            }
        }
        //public void GenrateTranscriptFinal(int studentId = 0, int type = 1)
        //{
        //    if (Session["UserObj"] != null)
        //    {
        //        var account = (Account)Session["UserObj"];
        //        if (account.AccountsRole.AccountType == "Admin" || !_powerWorkBusinessLogic.CheckMethodForAccess("PrintTransCript", account.AccountId))
        //        {
        //            var getStudent = _studentManager.GetSingleStudent(studentId);
        //            var takencourses = _courseForStudentManger.GetAllCourseByStudent(studentId).ToList();
        //            var getAllCgpa = _gradeManager.CalculateCgpaAllSemester(studentId).ToList();

        //            string physicalPath = Server.MapPath("~/Downloadables/" + "Tes.doc");
        //            string physicalPath2 = type == 1
        //                ? Server.MapPath("~/Downloadables/" + "TranscriptTemp.docx")
        //                : Server.MapPath("~/Downloadables/" + "Transcript-Demo.docx");

        //            using (DocX document = DocX.Create(physicalPath))
        //            {
        //                document.AddHeaders();
        //                document.AddFooters();
        //                document.ApplyTemplate(physicalPath2);
        //                Formatting darkFormatting = new Formatting
        //                {
        //                    Bold = true,
        //                    Size = 8,
        //                    FontColor = Color.Brown
        //                };
        //                Formatting studentInfoFormat = new Formatting
        //                {
        //                    Bold = true,
        //                    Size = 11,
        //                    FontFamily = new FontFamily("Calibri Light")
        //                };
        //                Formatting studentInfoDetailFormat = new Formatting
        //                {
        //                    Size = 11,
        //                    FontFamily = new FontFamily("Calibri Light"),
        //                    Bold = true
        //                };
        //                #region Transcript Title
        //               string studentStatVal = getStudent.StudentId + " " + getStudent.StudentInfo.StudentName;
        //                document.ReplaceText("StuNa", studentStatVal);
        //                document.ReplaceText("datePri", DateTime.Now.ToShortDateString());
        //                Paragraph transcriptTitle = document.InsertParagraph();
        //                transcriptTitle.Append("Transcript of Academic Record")
        //                    .FontSize(20)
        //                    .Font(new FontFamily("Calibri"))
        //                    .Bold();
        //                transcriptTitle.UnderlineStyle(UnderlineStyle.thick);
        //                transcriptTitle.InsertText("\n", false);
        //                transcriptTitle.Alignment = Alignment.center;
        //                #endregion

        //                #region Student_Detail_Table
        //                Table studenrInfoTable = document.AddTable(3, 2);
        //                studenrInfoTable.Design = TableDesign.TableNormal;
        //                studenrInfoTable.AutoFit = AutoFit.Contents;
        //                studenrInfoTable.Rows[0].Cells[0].Paragraphs.First()
        //                    .InsertText("Name", false, studentInfoFormat);
        //                studenrInfoTable.Rows[0].Cells[1].Paragraphs.First()
        //                    .InsertText(": " + getStudent.StudentInfo.StudentName, false, studentInfoDetailFormat);
        //                studenrInfoTable.Rows[1].Cells[0].Paragraphs.First()
        //                    .InsertText("Student's ID", false, studentInfoFormat);
        //                studenrInfoTable.Rows[1].Cells[1].Paragraphs.First()
        //                    .InsertText(": " + getStudent.StudentId, false, studentInfoDetailFormat);
        //                studenrInfoTable.Rows[2].Cells[0].Paragraphs.First()
        //                    .InsertText("Program", false, studentInfoFormat);
        //                studenrInfoTable.Rows[2].Cells[1].Paragraphs.First()
        //                    .InsertText(": " + getStudent.Department.DepartmentName, false, studentInfoDetailFormat);
        //                document.InsertTable(studenrInfoTable);

        //                #endregion

        //                #region Table_Setup

        //                var semesterCounter = 1;
                        
        //                foreach (var semester in takencourses.Select(s => s.SemesterId).Distinct())
        //                {

        //                    var semesterOrdinalName = NumericCordianal.AddOrdinal(semesterCounter);
        //                    var semester1 = semester;
        //                    var getStudentList = takencourses.Where(s => s.SemesterId == semester1);
        //                    var getCumulativeScores = getAllCgpa.FirstOrDefault(s => s.SemesterId == semester1);
        //                    Table table1 = document.AddTable(getStudentList.ToList().Count, 7);
        //                    table1.Design = TableDesign.LightShading;
        //                    table1.AutoFit = AutoFit.Contents;
        //                    table1.Alignment = Alignment.both;

        //                    Formatting tableTop = new Formatting
        //                    {
        //                        Bold = true,
        //                        Size = 11,
        //                        FontFamily = new FontFamily("Calibri"),
        //                        FontColor = Color.Black
        //                    };
        //                    Formatting tableTitle = new Formatting
        //                    {
        //                        Bold = true,
        //                        Size = 9,
        //                        FontFamily = new FontFamily("Calibri Light")
        //                    };
        //                    Formatting tableContent = new Formatting
        //                    {
        //                        Size = 9,
        //                        FontFamily = new FontFamily("Calibri Light")
        //                    };
        //                    Formatting tableFooter = new Formatting
        //                    {
        //                        Bold = true,
        //                        Size = 8,
        //                        FontFamily = new FontFamily("Calibri Light")
        //                    };
        //                    //Table Header
        //                    Row headingRowTop = table1.InsertRow(0);
        //                    headingRowTop.TableHeader = true;
        //                    headingRowTop.Cells[0].Paragraphs.First().InsertText(semesterOrdinalName, false, tableTop);
        //                    headingRowTop.Cells[1].Paragraphs.First().InsertText("Semester", false, tableTop);
        //                    headingRowTop.Cells[2].Paragraphs.First()
        //                        .InsertText(getStudentList.First().Semester.SemesterNYear + "", false, tableTop);
        //                    //headingRowTop.Cells[2].Width=30;
        //                    headingRowTop.Cells[2].Paragraphs.First().Alignment = Alignment.left;
        //                    headingRowTop.Cells[3].Paragraphs.First().InsertText("", false, tableTop);
        //                    headingRowTop.Cells[4].Paragraphs.First().InsertText("", false, tableTop);
        //                    headingRowTop.Cells[5].Paragraphs.First().InsertText("", false, tableTop);
        //                    headingRowTop.Cells[6].Paragraphs.First().InsertText("", false, tableTop);

        //                    Row headingRow = table1.InsertRow(1);
        //                    headingRow.TableHeader = true;
        //                    //headingRow.BreakAcrossPages = false;
        //                    headingRow.Cells[0].Paragraphs.First().InsertText("Sl.", false, tableTitle);
        //                    headingRow.Cells[0].Paragraphs.First().Alignment = Alignment.center;
        //                    headingRow.Cells[1].Paragraphs.First().InsertText("Course Code", false, tableTitle);
        //                    headingRow.Cells[1].Paragraphs.First().Alignment = Alignment.center;
        //                    headingRow.Cells[2].Paragraphs.First().InsertText("Course Title", false, tableTitle);
        //                    headingRow.Cells[2].Paragraphs.First().Alignment = Alignment.center;
        //                    headingRow.Cells[3].Paragraphs.First().InsertText("Credit Hours", false, tableTitle);
        //                    headingRow.Cells[3].Paragraphs.First().Alignment = Alignment.center;
        //                    headingRow.Cells[4].Paragraphs.First().InsertText("Letter Grade", false, tableTitle);
        //                    headingRow.Cells[4].Paragraphs.First().Alignment = Alignment.center;
        //                    headingRow.Cells[5].Paragraphs.First().InsertText("Grade Point", false, tableTitle);
        //                    headingRow.Cells[5].Paragraphs.First().Alignment = Alignment.center;
        //                    headingRow.Cells[6].Paragraphs.First().InsertText("Cr.Hr.*Gr.Pt.", false, tableTitle);
        //                    headingRow.Cells[6].Paragraphs.First().Alignment = Alignment.center;

                           

        //                    int i = 2;
        //                    int courseCounter = 1;
        //                    foreach (var student in getStudentList.ToList())
        //                    {
        //                        table1.Rows[i].Cells[0].Paragraphs.First()
        //                            .InsertText(courseCounter.ToString(), false, tableContent);
        //                        table1.Rows[i].Cells[0].Paragraphs.First().Alignment = Alignment.center;
        //                        table1.Rows[i].Cells[1].Paragraphs.First()
        //                            .InsertText(student.CourseForDepartment.CourseCode, false, tableContent);
        //                        table1.Rows[i].Cells[1].Paragraphs.First().Alignment = Alignment.left;
        //                        table1.Rows[i].Cells[2].Paragraphs.First()
        //                            .InsertText(student.CourseForDepartment.CourseName, false, tableContent);
        //                        table1.Rows[i].Cells[2].Paragraphs.First().Alignment = Alignment.left;
        //                        //table1.Rows[i].Cells[2].Width = .68;
        //                        if (student.CourseStatusId != 2)
        //                        {
        //                            table1.Rows[i].Cells[3].Paragraphs.First().InsertText("(0.0)", false, tableContent);
        //                        }
        //                        else
        //                        {
        //                            table1.Rows[i].Cells[3].Paragraphs.First()
        //                                .InsertText(student.CourseForDepartment.Credit.ToString("0.0"), false,
        //                                    tableContent);
        //                        }
        //                        table1.Rows[i].Cells[3].Paragraphs.First().Alignment = Alignment.center;
        //                        table1.Rows[i].Cells[4].Paragraphs.First()
        //                            .InsertText(student.LetterGrade, false, tableContent);
        //                        table1.Rows[i].Cells[4].Paragraphs.First().Alignment = Alignment.center;
        //                        table1.Rows[i].Cells[5].Paragraphs.First()
        //                            .InsertText(student.Grade.ToString("0.00"), false, tableContent);
        //                        table1.Rows[i].Cells[5].Paragraphs.First().Alignment = Alignment.center;
        //                        table1.Rows[i].Cells[6].Paragraphs.First()
        //                            .InsertText(student.TotalGrade.ToString("0.00"), false, tableContent);
        //                        table1.Rows[i].Cells[6].Paragraphs.First().Alignment = Alignment.center;
        //                        //table1.AutoFit=AutoFit.ColumnWidth;
        //                        i++;
        //                        courseCounter++;
        //                    }

        //                    table1.Rows[0].BreakAcrossPages = false;
        //                    Row footerRow = table1.InsertRow();

        //                    footerRow.Cells[0].Paragraphs.First().InsertText("Total ", false, tableFooter);
        //                    footerRow.Cells[0].Paragraphs.First().Alignment = Alignment.right;
        //                    footerRow.Cells[1].Paragraphs.First()
        //                        .InsertText("Credit: " + getCumulativeScores.CreditEarned, false, tableFooter);
        //                    footerRow.Cells[1].Paragraphs.First().Alignment = Alignment.left;
        //                    footerRow.Cells[2].Paragraphs.First()
        //                        .InsertText("Total Grade Point: " + getCumulativeScores.TotalGrade, false, tableFooter);
        //                    footerRow.Cells[2].Paragraphs.First().Alignment = Alignment.left;
        //                    footerRow.Cells[3].Paragraphs.First().InsertText("GPA: ", false, tableFooter);
        //                    footerRow.Cells[3].Paragraphs.First().Alignment = Alignment.right;
        //                    footerRow.Cells[4].Paragraphs.First()
        //                        .InsertText(getCumulativeScores.SemesterGpa.ToString("0.00"), false, tableFooter);
        //                    footerRow.Cells[4].Paragraphs.First().Alignment = Alignment.left;
        //                    footerRow.Cells[5].Paragraphs.First().InsertText("CGPA: ", false, tableFooter);
        //                    footerRow.Cells[5].Paragraphs.First().Alignment = Alignment.right;
        //                    footerRow.Cells[6].Paragraphs.First()
        //                        .InsertText(getCumulativeScores.Cgpa.ToString("0.00"), false, tableFooter);
        //                    footerRow.Cells[6].Paragraphs.First().Alignment = Alignment.left;
        //                    Paragraph p = document.InsertParagraph();

        //                    ////p.SetLineSpacing(LineSpacingType.Line, 1.5f);
        //                    //p.Append("\n");
        //                    //p.Append("Semester Details of : " + getStudentList.First().Semester.SemesterNYear + "")
        //                    //    .Font(new FontFamily("Calibri")).FontSize(11).Bold();
        //                    #endregion
        //                    document.InsertTable(table1);
        //                    semesterCounter++;
        //                    //document.InsertTable(table1);
        //                }
        //                //Paragraph p2 = document.InsertParagraph();
        //                ////p.SetLineSpacing(LineSpacingType.Line, 1.5f);
        //                //p2.Append("\n\n\n");
        //                //Table signingTable = document.AddTable(3, 3);
        //                //signingTable.Design = TableDesign.TableNormal;
        //                //signingTable.AutoFit = AutoFit.Contents;
        //                //signingTable.Alignment=Alignment.both;
        //                //signingTable.Rows[0].Cells[0].Paragraphs.First().InsertText("Adnan mahmud", false, studentInfoFormat);
        //                //signingTable.Rows[0].Cells[1].Paragraphs.First().InsertText("                               ");
        //                //signingTable.Rows[0].Cells[2].Paragraphs.First().InsertText("                               ");
        //                //signingTable.Rows[1].Cells[0].Paragraphs.First().InsertText("--------------------");
        //                //signingTable.Rows[1].Cells[1].Paragraphs.First().InsertText("");
        //                //signingTable.Rows[1].Cells[2].Paragraphs.First().InsertText("------------------------------");

        //                //signingTable.Rows[2].Cells[0].Paragraphs.First().InsertText("Verified By", false, studentInfoFormat);
        //                //signingTable.Rows[2].Cells[1].Paragraphs.First().InsertText("");
        //                //signingTable.Rows[2].Cells[2].Paragraphs.First().InsertText("Controller of Examinations",false, studentInfoFormat);

        //                //document.InsertTable(signingTable);
        //                document.ReplaceText("veriName", account.Name);
        //                document.ReplaceText("desigName", "Senior Officer, Exam");
        //                string fileNameTemplate = @"C:\Users\Adnan mahmud\Desktop\Transcript-{0}-{1}.docx";
        //                Response.Clear();
        //                document.SaveAs(Response.OutputStream);
        //                Response.AddHeader("content-disposition",
        //                    "attachment; filename=\"" + getStudent.StudentId + ".doc\"");
        //                Response.ContentType = "application/msword";
        //            }
        //        }
        //    }
        //}
        public void GenerateTransCriptFrontPage(TranscriptPrintOptions printOptions) // No tracking Added
        {
            if (Session["UserObj"] != null)
            {
                var account = (Account) Session["UserObj"];
                if (account.AccountsRole.AccountType == "Admin" || !_powerWorkBusinessLogic.CheckMethodForAccess("PrintTransCript", account.AccountId))
                {
                    string majorSubjects = "N/A";
                    var getStudent = _studentManager.GetSingleStudent(printOptions.StudentIdenId);
                    var allCgpa = _gradeManager.CalculateCgpaofAStudent(printOptions.StudentIdenId);
                    var sem = _courseForStudentManger.GetAllCompletedCourseOfAStudent(printOptions.StudentIdenId).AsEnumerable()
                        .Select(s => new Semester
                        {
                            SemesterId = s.SemesterId,
                            SemesterNYear = s.Semester.SemesterNYear
                        }).ToList();
                    var getAllCoursesBySyudent = _courseForStudentManger.GetAllCourseForStudentsAcademics().Where(s => s.StudentIdentificationId == printOptions.StudentIdenId && s.DeclareMajor).ToList();
                    if (getAllCoursesBySyudent.Any())
                    {
                        majorSubjects = getAllCoursesBySyudent.Aggregate(majorSubjects, (current, c) => current + "(" + c.CourseForDepartment.CourseName + ")" + Environment.NewLine);
                    }

                    string physicalPath = Server.MapPath("~/Downloadables/" + "Tes.doc");
                    string physicalPath2 = Server.MapPath("~/Downloadables/" + "TranscriptFront.docx");
                    using (DocX document = DocX.Create(physicalPath))
                    {
                        document.AddHeaders();
                        document.AddFooters();
                        document.ApplyTemplate(physicalPath2);
                        #region Student Portion
                        document.ReplaceText("nameSp", getStudent.StudentInfo.StudentName);
                        document.ReplaceText("idSp", getStudent.StudentId);
                        document.ReplaceText("progSp", getStudent.Department.DepartmentName);
                        if (sem.Any())
                        {
                            document.ReplaceText("firstSp", sem.FirstOrDefault().SemesterNYear);
                            document.ReplaceText("lastSp", sem.OrderByDescending(s => s.SemesterId).FirstOrDefault().SemesterNYear);
                        }
                        else
                        {
                            document.ReplaceText("firstSp", "");
                            document.ReplaceText("lastSp", "---");
                        }
                        document.ReplaceText("resPubSp", printOptions.PublishingDate.ToString());
                        document.ReplaceText("minCreSp", getStudent.Department.RequiredCredit.ToString());
                        document.ReplaceText("stuCreSp", allCgpa.CreditEarned.ToString("F"));
                        document.ReplaceText("minCgpaSp", getStudent.Department.RequiredCredit.ToString("F"));
                        document.ReplaceText("stuCgpaSp", allCgpa.Cgpa.ToString("F"));
                        document.ReplaceText("majSp", majorSubjects);
                        #endregion

                        document.ReplaceText("datePri", DateTime.Now.ToShortDateString());
                        document.ReplaceText("veriName", account.Name);
                        document.ReplaceText("desigName", "Senior Officer, Exam");
                        string fileNameTemplate = @"C:\Users\Adnan mahmud\Desktop\Transcript_Cover-{0}-{1}.docx";

                        Response.Clear();
                        document.SaveAs(Response.OutputStream);
                        Response.AddHeader("content-disposition",
                            "attachment; filename=\"" + getStudent.StudentId + ".doc\"");
                        Response.ContentType = "application/msword";

                    }
                }
            }
        }
    }
}