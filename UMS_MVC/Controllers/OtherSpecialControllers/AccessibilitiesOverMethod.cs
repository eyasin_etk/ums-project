﻿using System.Linq;

namespace UMS_MVC.Controllers.OtherSpecialControllers
{
    public static class AccessibilitiesOverMethod
    {
        public static bool AccountAccessRoleSectionCon(string actionName, string account)
        {
            var accounts = new[] { "Admin", "Ps", "Coordinator", "Exam", "ExamCtrl","Support"};
            var accounts2 = new[] { "Admin", "Coordinator" };
            var accounts3 = new[] { "Admin" };
            var account4 = new[] {"Admin", "Support"};
            switch (actionName)
            {
                //.... Section ......
                //.. Sample
                //if (Session["UserObj"] == null)  return RedirectToAction("LogInresultSubmit", "Home");
                //var account =(Account) Session["UserObj"];
                //if (!AccessibilitiesOverMethod.AccountAccessRoleSectionCon("SectionDetails", account.AccountsRole.AccountType)) return RedirectToAction("NotPermitted", "Home");
                case "SectionIndex":
                    
                    if (accounts.Contains(account)) return true;
                    break;
                case "SectionDetails":
                    
                    if (accounts2.Contains(account)) return true;
                    break;
                case "SectionCreate":

                    if (account4.Contains(account)) return true;
                    break;
                case "SectionEdit":

                    if (account4.Contains(account)) return true;
                    break;
                case "SectionDelete":
                    
                    if (accounts3.Contains(account)) return true;
                    break;
                case "HilightSectionIndex":
                   
                    if (account4.Contains(account)) return true;
                    break;
            }
            return false;
        }

        public static bool AccountAccessRoleStudentAdvisingList(string actionName, string account)
        {
            switch (actionName)
            {
                case "Index":
                    var accounts1 = new[] {"Admin", "Advisor", "Coordinator","Exam","ExamCtrl","Support"};
                    if(accounts1.Contains(account))return true;
                    break;
                case "AddCourses2":
                    var accounts2 = new[] {"Admin", "Advisor","Support"};
                    if(accounts2.Contains(account))return true;
                    break;
                case "AdvisedStudentList":
                    var accounts3 = new[] { "Admin", "Advisor", "Exam", "ExamCtrl","Support", "Coordinator" };
                    if (accounts3.Contains(account)) return true;
                    break;
            }
            return false;
        }

        public static bool AccessForCourseOtherMatter(string actionName, string account)
        {
            switch (actionName)
            {
                case "Index":
                    var accounts1 = new[] { "Admin", "Exam", "ExamCtrl" };
                    if (accounts1.Contains(account)) return true;
                    break;
            }
            return false;
        }
    }
}