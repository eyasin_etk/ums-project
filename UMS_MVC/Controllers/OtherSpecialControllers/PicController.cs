﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO;

namespace UMS_MVC.Controllers.OtherSpecialControllers
{
    public class PicController : Controller
    {

        private UmsDbContext db;
        private readonly StudentManager _studentManager;
        private readonly AccountManager _accountManager ;
        private readonly DepartmentManager _departmentManager;

        public PicController()
        {
            db = new UmsDbContext();
            _studentManager = new StudentManager();
            _accountManager = new AccountManager();
            _departmentManager=new DepartmentManager();

        }
        public ActionResult SomeImage(string imageName,int type)
        {
            var root = @"C:\inetpub\wwwroot\pauums\Image\AccountPic\" + imageName;
            String RelativePath = root.Replace(Request.ServerVariables["APPL_PHYSICAL_PATH"], String.Empty);
            return File(RelativePath, "image/jpeg");
        }
        public bool CheckImageType(HttpPostedFileBase file)
        {
            string extension = Path.GetExtension(file.FileName);
            if (extension == ".jpg" || extension == ".JPG")
            {
                return true;
            }
            return false;
        }
        //public ActionResult FileUpload(HttpPostedFileBase file, string studentId)
        //{
        //    if (file != null && CheckImageType(file))
        //    {
        //        string imageName = studentId + ".jpg";
        //        var checkImage = (db.PicOfStudents.Where(s => s.ImageName == imageName)).Count();
        //        if (checkImage == 0)
        //        {
        //            ResizeImageAndSave(file, studentId);
        //            var pic = new PicOfStudent
        //            {
        //                ImageName = imageName
        //            };

        //            db.PicOfStudents.Add(pic);
        //            db.SaveChanges();
        //        }

        //    }
        //    if (Session["RequestUrl"] != null)
        //    {
        //        return Redirect(Session["RequestUrl"].ToString());
        //    }
        //    return RedirectToAction("Index2", "StudentAllDetails");
        //}
        public ActionResult FileUpload(HttpPostedFileBase file, int StudentIdentificationId)
        {
            if (file != null && CheckImageType(file))
            {
                var checkImage = db.StudentIdentifications.Find(StudentIdentificationId);
                string imageName = checkImage.StudentId + ".jpg";
                
                if (file.ContentLength>0)
                {
                    ResizeImageAndSave(file, imageName);
                    checkImage.StudentPicture = imageName;

                    db.Entry(checkImage).State = EntityState.Modified;
                    db.SaveChanges();
                }
              
                // need to change  refferenc with stident identification id instead od student id//
            }
            if (Session["RequestUrl"] != null)
            {
                return Redirect(Session["RequestUrl"].ToString());
            }
            return RedirectToAction("Index2", "StudentAllDetails");
        }

        public void ResizeImageAndSave(HttpPostedFileBase file, string imageName)
        {
            WebImage img = new WebImage(file.InputStream);
            if (img.Height > 450)
                img.Resize(350, 450);
            string physicalPath = Server.MapPath("~/Image/" + imageName);
            //string physicalPath2 = @"C:\inetpub\wwwroot\portal.ums\Image\" + imageName;
            img.Save(physicalPath);
            //img.Save(physicalPath2);
        }

        public ActionResult ChangePicofStudent(HttpPostedFileBase file, int StudentIdentificationId)
        {
            if (file != null && CheckImageType(file))
            {
                var findStudent = db.StudentIdentifications.Find(StudentIdentificationId);
                var imageName =findStudent.StudentId+ ".jpg";
                ResizeImageAndSave(file, imageName);
            }
            if (Session["RequestUrl"] != null)
            {
                return Redirect(Session["RequestUrl"].ToString());
            }
            return RedirectToAction("Index2", "StudentAllDetails");
        }

        public ActionResult StudentImageLookups()
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Admission" && account.AccountsRole.AccountType != "Support") return RedirectToAction("NotPermitted", "Home");

            ViewBag.departmentId = new SelectList(_departmentManager.GetAllDepartmentWithEmptyPicCalculation(), "DepartmentId", "DepartmentName");
            return View();
        }

        public PartialViewResult EmptyImageCalculation(int departmentId = 0)
        {
            if (Session["UserObj"] != null)
            {
                var account = (Account) Session["UserObj"];
                if (account.AccountsRole.AccountType == "Admin" || account.AccountsRole.AccountType == "Admission" ||
                    account.AccountsRole.AccountType == "Support")
                {
                    int totalstudents = 0;
                    var studentIdentifications =
                        from student in
                            _studentManager.GetAllStudentIdentificationsFiltered().Include(s => s.StudentInfo)
                        where student.DepartmentId == departmentId && string.IsNullOrEmpty(student.StudentPicture)
                        select student;

                    if (studentIdentifications.Any())
                    {
                        ViewBag.totalStudents = studentIdentifications.Count();
                        return PartialView("_EmptyImageCalculationPartial", studentIdentifications.ToList());
                    }
                    ViewBag.totalStudents = totalstudents;
                }
            }
            return PartialView("_EmptyImageCalculationPartial", new List<StudentIdentification>()
            {
                new StudentIdentification()
                {
                    StudentId = "No result",
                }
            });
        }
    }
}