﻿using System;
using System.Linq;
using System.Web.Mvc;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO;

namespace UMS_MVC.Controllers.OtherSpecialControllers
{
    public class LoginMonitorController : Controller
    {
        private readonly UmsDbContext db;
        private readonly LoginHistoryManager _loginHistoryManager;
        private readonly AccountManager _accountManager;

        public LoginMonitorController()
        {
          db  = new UmsDbContext();
            _loginHistoryManager=new LoginHistoryManager();
            _accountManager=new AccountManager();

        }
        [HttpGet]
        public ActionResult ShowLogins()
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRoleId != 1 && account.AccountsRoleId != 14) return RedirectToAction("NotPermitted", "Home");
            var getAllLoginHistory = _loginHistoryManager.GetAlLoginHistories();
            string currentTime = DateTime.Now.ToString("d");
            //////////////// main code start /////////////////////////////
            ViewBag.TotalHistory = getAllLoginHistory.Count();
            ViewBag.access = false;
            ViewBag.roleId = new SelectList(_accountManager.GetAllAccountsRoles(), "AccountsRoleId", "AccountType");
            if (account.AccountId == 1)
            {
                ViewBag.access = true;
            }
            return View(getAllLoginHistory.Where(s=>s.EntryTime.Contains(currentTime)).OrderByDescending(s => s.LoginHistoryId).Take(400).ToList());
            ////////////////main code end////////////////////////////////
        }
        [HttpPost]
        public ActionResult ShowLogins(DateTime? userId, string reset, string searchId, int roleId = 0)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRoleId != 1 && account.AccountsRoleId != 14) return RedirectToAction("NotPermitted", "Home");
            ViewBag.access = false;
            ViewBag.roleId = new SelectList(_accountManager.GetAllAccountsRoles(), "AccountsRoleId", "AccountType");
            if (account.AccountId == 1)
            {
                ViewBag.access = true;
            }
            //////////////// main code start /////////////////////////////
            var loadLogins = _loginHistoryManager.GetAlLoginHistories();
            if (string.IsNullOrEmpty(reset))
            {
                var loginHistories = loadLogins;
                if (!string.IsNullOrEmpty(searchId))
                {
                    var accounts = _accountManager.GetAllAccounts();
                    if (accounts.Count(s => s.LoginIdentity == searchId) > 0)
                    {
                        var findAccount = accounts.Single(s => s.LoginIdentity == searchId);
                        TempData["LoginId"] = searchId;
                        loginHistories = loginHistories.Where(s => s.AccountId == findAccount.AccountId);
                    }  
                }
                if (userId != null && loginHistories.Any())
                {
                    if (loginHistories.Any())
                    {
                        string selectedDate = userId.Value.ToString("d");
                        ViewBag.SelectedDate = "at " + selectedDate;
                        var loginHistoryWithDate = loginHistories.Where(s => s.EntryTime.Contains(selectedDate));
                        TempData["SelectedDate"] = userId;
                        ViewBag.allLoginCount = loginHistoryWithDate.Count();
                        return View(loginHistoryWithDate.ToList());
                    }
                    return View();
                }
                if (roleId > 0)
                {
                    ViewBag.allLoginCount = loginHistories.Count(s => s.Account.AccountsRoleId == roleId);
                    var getNotAllLogins = loginHistories.Where(s => s.Account.AccountsRoleId == roleId).Take(200);
                    return View(getNotAllLogins.ToList());
                }
                if (userId != null)
                {
                    string selectedDate = userId.Value.ToString("d");
                    ViewBag.SelectedDate = "at " + selectedDate;
                    var loginHistoryWithDate = loadLogins.Where(s => s.EntryTime.Contains(selectedDate));
                    TempData["SelectedDate"] = userId;
                    ViewBag.allLoginCount = loginHistoryWithDate.Count();
                    return View(loginHistoryWithDate.ToList());
                }
                ViewBag.allLoginCount = loginHistories.Count();
                return View(loginHistories.ToList());
            }
            return View(loadLogins.OrderByDescending(s => s.LoginHistoryId).Take(200).ToList());
            ////////////////main code end////////////////////////////////
        }
        public ActionResult DeleteLogins()
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin") return RedirectToAction("NotPermitted", "Home");
            string currentTime = DateTime.Now.ToString("d");
            foreach (var logs in _loginHistoryManager.GetAlLoginHistories().Where(s=>!s.EntryTime.Contains(currentTime)))
            {
                _loginHistoryManager.DeleteLoginHistory(logs.LoginHistoryId);
            }
            return RedirectToAction("ShowLogins");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
                _accountManager.Dispose();
                _loginHistoryManager.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}