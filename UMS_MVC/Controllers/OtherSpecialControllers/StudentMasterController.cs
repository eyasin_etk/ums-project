﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.Manager.DAL.DAO.Intefaces;
using UMS_MVC.Manager.DAL.DAO.Repository;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.Controllers.OtherSpecialControllers
{
    public class StudentMasterController : Controller
    {
        private readonly UmsDbContext db;
        private readonly StudentManager _studentManager;
        private readonly CourseForStudentManger _courseForStudentManger;
        private readonly PaymentManager _paymentManager;
        private readonly RebateManager _rebateManager;
        private readonly SemesterManager _semesterManager;
        private readonly DepartmentManager _departmentManager;
        private readonly GradeManager _gradeManager;
        private readonly ICourseForStudentRepository _courseForStudentRepository;

        public StudentMasterController()
        {
            db = new UmsDbContext();
            _studentManager = new StudentManager();
            _courseForStudentManger = new CourseForStudentManger();
            _paymentManager = new PaymentManager();
            _rebateManager = new RebateManager();
            _semesterManager = new SemesterManager();
            _departmentManager = new DepartmentManager();
            _gradeManager = new GradeManager();
            _courseForStudentRepository = new CourseForStudentRepository();


        }

        public ActionResult StudentMastersControllers(string searchstudentId)
        {
            if (Session["UserObj"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin") return RedirectToAction("NotPermitted", "Home");
            if (!string.IsNullOrEmpty(searchstudentId))
            {
                var studentIdentity =
                    _studentManager.GetAllStudentIdentifications().FirstOrDefault(s => s.StudentId == searchstudentId);
                if (studentIdentity != null)
                {
                    var studentPaymetRegis =
                       _paymentManager.GetAllPaymentRegistration()
                           .Where(s => s.StudentIdentification.StudentId == searchstudentId);
                    var studentPayment = _paymentManager.GetAllPayments()
                        .Where(s => s.PaymentRegistration.StudentIdentification.StudentId == searchstudentId);
                    var registeredCourses = _courseForStudentManger.GetAllCourseForStudentsAcademics()
                        .Where(s => s.StudentIdentification.StudentId == searchstudentId);
                    var rebateSearch =
                        _rebateManager.GetAllRebate()
                            .Where(s => s.StudentIdentificationId == studentIdentity.StudentIdentificationId);
                    ViewData["StudentIdentity"] = studentIdentity;
                    if (studentPaymetRegis.Any())
                    {
                        ViewData["studentPaymetRegis"] = studentPaymetRegis;
                    }
                    if (studentPayment.Any())
                    {
                        ViewData["StudentPayment"] = studentPayment;
                    }
                    if (rebateSearch.Any())
                    {
                        ViewData["rebate"] = rebateSearch;
                    }
                    if (registeredCourses.Any())
                    {
                        ViewData["registeredCourses"] = registeredCourses;
                    }
                }
                var studentInfo = _studentManager.GetAllStudentInfos().Where(s => s.StudentId == searchstudentId);
                var studentAcademic =
                    _studentManager.GetAllAcademicInfos().Where(s => s.StudentId == searchstudentId);
                var studentDocument =
                    _studentManager.GetAllStudentDocument().Where(s => s.StudentId == searchstudentId);

                if (studentInfo.Any())
                {
                    ViewData["StudentInfo"] = studentInfo.First();
                }
                if (studentAcademic.Any())
                {
                    ViewData["StudentAcademic"] = studentAcademic;
                }
                if (studentDocument.Any())
                {
                    ViewData["StudentDocument"] = studentDocument.First();
                }

            }
            ViewBag.StudentId = searchstudentId;



            return View();
        }
        public ActionResult DeleteStudentIdentity(int? studentidentityId, string searchsVal)
        {
            if (Session["UserObj"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin") return RedirectToAction("NotPermitted", "Home");
            if (studentidentityId > 0)
            {
                var findStudentIdentity = db.StudentIdentifications.Find(studentidentityId);
                db.StudentIdentifications.Remove(findStudentIdentity);
                db.SaveChanges();
            }
            return RedirectToAction("StudentMastersControllers", new { searchstudentId = searchsVal });
        }
        public ActionResult DeleteStudentInformation(int? sudentIanfoId, string searchsVal)
        {
            var findStudent = db.StudentInfos.Find(sudentIanfoId);
            if (findStudent != null)
            {
                db.StudentInfos.Remove(findStudent);
                db.SaveChanges();
            }
            return RedirectToAction("StudentMastersControllers", new { searchstudentId = searchsVal });
        }
        public ActionResult DeleteStudentAcademic(string searchsVal, string studentId = "")
        {
            if (Session["UserObj"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin") return RedirectToAction("NotPermitted", "Home");
            var findStudent = db.StudentAcademicInfos.Where(s => s.StudentId == studentId);
            if (findStudent.Any())
            {
                foreach (var studentAcademicInfo in findStudent)
                {
                    db.StudentAcademicInfos.Remove(studentAcademicInfo);
                }
                db.SaveChanges();
            }
            return RedirectToAction("StudentMastersControllers", new { searchstudentId = searchsVal });
        }
        public ActionResult DeleteStudentDocument(int? documentId, string searchsVal)
        {
            if (Session["UserObj"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin") return RedirectToAction("NotPermitted", "Home");
            if (documentId > 0)
            {
                var findStudent = db.DocumentsaddAddings.Find(documentId);
                db.DocumentsaddAddings.Remove(findStudent);
                db.SaveChanges();
            }
            return RedirectToAction("StudentMastersControllers", new { searchstudentId = searchsVal });
        }
        public ActionResult DeletePaymentRegis(int? studentId, string searchsVal)
        {
            if (Session["UserObj"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin") return RedirectToAction("NotPermitted", "Home");
            var findPayemtnRegis = db.PaymentRegistrations.Where(s => s.StudentIdentificationId == studentId);
            var findPayments = db.Payments;
            if (findPayemtnRegis.Any())
            {
                foreach (var paymentRegistration in findPayemtnRegis)
                {
                    PaymentRegistration registration = paymentRegistration;
                    foreach (var findPayment in findPayments.Where(s => s.PaymentRegistrationId == registration.PaymentRegistrationId))
                    {
                        db.Payments.Remove(findPayment);

                    }
                    db.SaveChanges();
                    db.PaymentRegistrations.Remove(paymentRegistration);
                }
                db.SaveChanges();
            }
            return RedirectToAction("StudentMastersControllers", new { searchstudentId = searchsVal });
        }
        public ActionResult DeleteRebates(int? studentId, string searchsVal)
        {
            if (Session["UserObj"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin") return RedirectToAction("NotPermitted", "Home");

            var findPayemtnRegis = db.Rebates.Where(s => s.StudentIdentificationId == studentId);
            if (findPayemtnRegis.Any())
            {
                foreach (var rebates in findPayemtnRegis)
                {
                    db.Rebates.Remove(rebates);
                }
                db.SaveChanges();
            }
            return RedirectToAction("StudentMastersControllers", new { searchstudentId = searchsVal });
        }
        public ActionResult DeleteRegisteredCourses(int? studentId, string searchsVal)
        {
            if (Session["UserObj"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin") return RedirectToAction("NotPermitted", "Home");
            var findStudent = db.CourseForStudentsAcademics.Where(s => s.StudentIdentificationId == studentId);
            if (findStudent.Any())
            {
                foreach (var courseForStudentsAcademic in findStudent)
                {
                    db.CourseForStudentsAcademics.Remove(courseForStudentsAcademic);
                }
                db.SaveChanges();
            }
            return RedirectToAction("StudentMastersControllers", new { searchstudentId = searchsVal });
        }
        //////Student grade Printout////////////
        public ActionResult GradeSheetPrintForSingleSemester(string studentId, int? DepartmentId, int SemesterId = 0)
        {
            if (Session["UserObj"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "ExamCtrl" && account.AccountsRole.AccountType != "Exam") return RedirectToAction("NotPermitted", "Home");
            try
            {
                //main code start from here/////////
                var getAllDepartment = _departmentManager.GetAllDepartment();
                TempData["SemesterId"] = new SelectList(_semesterManager.GetAllSemester(), "SemesterId", "BatchNo", _semesterManager.GetActiveSemester().SemesterId);
                ViewBag.DepartmentId = new SelectList(getAllDepartment, "DepartmentId", "DepartmentName");
                TempData["currentSemester"] = _semesterManager.GetActiveSemester();

                var filteredSemStudent = _studentManager.GetAllStudentIdentifications().Include(s => s.StudentInfo);

                if (!string.IsNullOrEmpty(studentId))
                {
                    filteredSemStudent = filteredSemStudent.Where(s => s.StudentId.Contains(studentId));
                    if (!Queryable.Any<StudentIdentification>(filteredSemStudent))
                    {
                        ViewBag.StudentFoundMgs = "Student not found.";
                    }
                    return View(filteredSemStudent.ToList());
                }
                if (SemesterId == 0 && !(DepartmentId > 0))
                    return View(filteredSemStudent.Take(100).ToList());

                if (SemesterId > 0)
                {
                    TempData["SemesterId"] = new SelectList(_semesterManager.GetAllSemester(), "SemesterId", "BatchNo", SemesterId);
                    filteredSemStudent = filteredSemStudent.Where(s => s.SemesterId == SemesterId);
                    //return View(filteredSemStudent.ToList());
                }
                if (DepartmentId > 0)
                {
                    ViewBag.DepartmentId = new SelectList(getAllDepartment, "DepartmentId", "DepartmentName",
                        DepartmentId);
                    filteredSemStudent = filteredSemStudent.Where(s => s.DepartmentId == DepartmentId);
                    //return View(filteredSemStudent.ToList());
                }
                ViewBag.totalStudentCount = filteredSemStudent.Count();
                return View(filteredSemStudent.ToList());
            }
            catch (Exception)
            {
                return RedirectToAction("ErrorPage", "Home");
            }

        }

        public PartialViewResult ShowTranscriptPrintOptions(int id)
        {
            var student =
                _studentManager.GetAllStudentIdentifications().FirstOrDefault(s => s.StudentIdentificationId == id);
            if (student != null)
            {
                var options = new TranscriptPrintOptions
                {
                    StudentIdenId = student.StudentIdentificationId
                };
                return PartialView("_ShowTranscriptPrintPartial", options);
            }
            return PartialView("_ShowTranscriptPrintPartial");
        }

        public ActionResult StudentGradeSheetDetails(int sIdForStudentDetails)
        {
            var getActiveSemester = _semesterManager.GetActiveSemester();
            ViewBag.semester = getActiveSemester;
            if (sIdForStudentDetails > 0)
            {
                var findStudent = _studentManager.GetSingleStudent(sIdForStudentDetails);
                if (findStudent != null)
                {
                    ViewBag.studentObj = findStudent;
                }
                var takencourses = (_courseForStudentManger.GetAllCourseByStudent(sIdForStudentDetails)).Where(s => s.SemesterId == getActiveSemester.SemesterId);

                if (takencourses.Any())
                {
                    _gradeManager.CalculateCgpaAllSemester(sIdForStudentDetails);
                    ViewBag.cgpaOfStudent = _gradeManager.CalulateScoresBySemester(sIdForStudentDetails, getActiveSemester.SemesterId);
                    return PartialView("_gradeSheetDetails", takencourses.ToList());
                }

            }
            ViewBag.Alert = "No Match Found !!!";
            return PartialView("_gradeSheetDetails", new List<CourseForStudentsAcademic>());

        }
        public PartialViewResult AllDetailsOfSemester(int sIId)
        {

            if (sIId > 0)
            {
                //var takencourses = (_courseForStudentManger.GetAllCourseByStudent(sIId));
                var takencourses =
               (from course in _courseForStudentRepository.GetAll().Where(s=>s.StudentIdentificationId==sIId).Include(s => s.CourseForDepartment).Include(s => s.CourseStatus).Include(s=>s.Semester).AsNoTracking()
                select new AcademicCourseViewModel
                {
                    CourseCode = course.CourseForDepartment.CourseCode,
                    CourseName = course.CourseForDepartment.CourseName,
                    SemesterId = course.SemesterId,
                    StudentIdentiFictionId = course.StudentIdentificationId,
                    Credits = course.CourseForDepartment.Credit,
                    Mid = course.Midterm,
                    Final = course.FinalTerm,
                    LetterGrade = course.LetterGrade,
                    Grade = course.Grade,
                    Tgp = course.TotalGrade,
                    Status = course.CourseStatus.ShortName,
                    CourseStatusId = course.CourseStatusId,
                    SecId = course.SectionId,
                    SemesterAndYear = course.Semester.SemesterNYear,
                    
                }).ToList();
                var getActiveSemester = _semesterManager.GetActiveSemester().SemesterId;
                if (takencourses.Any())
                {
                    ViewBag.activeSemester = getActiveSemester;
                    ViewBag.sIId = sIId;
                    ViewBag.cgpas = _gradeManager.CalculateCgpaAllSemester(sIId);
                    return PartialView("_allCourseListBySemester", takencourses);
                }
            }
            return PartialView("_allCourseListBySemester", new List<AcademicCourseViewModel>());
        }



    }
}