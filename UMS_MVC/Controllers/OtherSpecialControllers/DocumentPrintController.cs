﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Dynamic;
using System.Linq;
using System.Web.Mvc;
using RazorPDF;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.BLL;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.Manager.DAL.DAO.Intefaces;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IAdmission;
using UMS_MVC.Manager.DAL.DAO.Repository;
using UMS_MVC.Manager.DAL.DAO.Repository.RAdmission;
using UMS_MVC.OtherSpecialService;
using UMS_MVC.ViewModels.ViewModels;
using UMS_MVC.ViewModels.ViewModels.Reporting;


namespace UMS_MVC.Controllers.OtherSpecialControllers
{
    public class DocumentPrintController : Controller
    {
        private UmsDbContext db;
        private readonly SectionManager _sectionManager;
        private readonly CourseForStudentManger _courseForStudentManger;
        private readonly SemesterManager _semesterManager;
        private readonly StudentManager _studentManager;
        private readonly GradeManager _gradeManager;
        private readonly DepartmentManager _departmentManager;
        private readonly PaymentManager _paymentManager;
        private readonly AccountManager _accountManager;
        private readonly UserActivityManager _userActivityManager;
        private readonly CourseForDepartmentManager _courseForDepartmentManager;
        private readonly TeacherManager _teacherManager;
        private readonly CourseBusinessLogic _courseBusinessLogicController;
        private readonly PowerWorkBusinessLogic _powerWorkBusinessLogic;
        private readonly IStudentIdentityRepository _identityRepository;
        private readonly IPersonalInfoRepository _infoRepository;
        private readonly ISemesterRepository _semesterRepository;
        private readonly ICourseForStudentRepository _courseForStudentRepository;

        public DocumentPrintController()
        {
            db = new UmsDbContext();
            _sectionManager = new SectionManager();
            _courseForStudentManger = new CourseForStudentManger();
            _semesterManager = new SemesterManager();
            _studentManager = new StudentManager();
            _gradeManager = new GradeManager();
            _departmentManager = new DepartmentManager();
            _paymentManager = new PaymentManager();
            _accountManager = new AccountManager();
            _userActivityManager = new UserActivityManager();
            _courseForDepartmentManager = new CourseForDepartmentManager();
            _teacherManager = new TeacherManager();
            _courseBusinessLogicController = new CourseBusinessLogic();
            _powerWorkBusinessLogic = new PowerWorkBusinessLogic();
            _identityRepository = new StudentIdentityRepository();
            _infoRepository = new PersonalInfoRepository();
            _semesterRepository = new SemesterRepository();
            _courseForStudentRepository = new CourseForStudentRepository();

        }

        public ActionResult PdfForStudents(int dept = 0, int sem = 0)
        {
            if (Session["UserObj"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            var sortedlist = _studentManager.GetAllStudentIdentificationsFiltered()
                .Include(s => s.Semester)
                .Include(s => s.StudentInfo);
            string departmentName = _departmentManager.GetSingleDepartment(dept).DepartmentName;
            string semester = _semesterManager.GetSingleSemester(sem).SemesterNYear;
            if (sem > 0 && dept > 0)
            {
                sortedlist = sortedlist.Where(s => s.SemesterId == sem && s.DepartmentId == dept);
            }
            else
            {
                return RedirectToAction("Index2", "StudentAllDetails");
            }
            sortedlist = (from stud in sortedlist
                          orderby stud.StudentId.Length, stud.StudentId
                          select stud);
            _userActivityManager.InsertActivity(new UserActivity
            {
                ComputerIp = ClassifiedServices.GetUserIp(),
                UserActivityTypeId = 5,
                Activity = account.LoginIdentity + " Printed Student list of " + departmentName + "-" + semester,
                AccountId = account.AccountId,
                StudentId = "All",
                ActivityTime = DateTime.Now,

            });
            Session.Add("SelectedDepartment", departmentName);
            Session.Add("SelectedSemester", semester);
            return new PdfResult(sortedlist.ToList(), "PdfForStudents");
        }

        public StudentInfo GetStudentInfoEssential(int stdId)
        {
            var studentInfoId = _identityRepository.FindSingle(stdId);
            if (studentInfoId.StudentInfoId != null)
            {
                return studentInfoId.StudentInfo;
            }
            return new StudentInfo()
            ;
        }

        //Print For Admission
        public ActionResult XclForStudentsAdmissionPart(int dept = 0, int sem = 0)
        {
            if (Session["UserObj"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            var sortedlist = _studentManager.GetAllStudentIdentifications();

            string departmentName = _departmentManager.GetSingleDepartment(dept).DepartmentName;
            string semester = _semesterManager.GetSingleSemester(sem).SemesterNYear;
            if (sem > 0 && dept > 0)
            {
                sortedlist = sortedlist.Where(s => s.SemesterId == sem && s.DepartmentId == dept);
            }
            else
            {
                return RedirectToAction("Index2", "StudentAllDetails");
            }
            _userActivityManager.InsertActivity(new UserActivity
            {
                ComputerIp = ClassifiedServices.GetUserIp(),
                UserActivityTypeId = 5,
                Activity = account.LoginIdentity + " Printed Student list of " + departmentName + "-" + semester,
                AccountId = account.AccountId,
                StudentId = "All",
                ActivityTime = DateTime.Now,

            });
            Session.Add("SelectedDepartment", departmentName);
            Session.Add("SelectedSemester", semester);

            HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Students_of_" + departmentName + "" + (semester) + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";

            return View("PdfForStudentsAdmissionPart", sortedlist.OrderBy(s => s.StudentId));

        }

        public ActionResult XclForAdmissionPartSecond(int dept = 0, int sem = 0)
        {
            if (Session["UserObj"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            var sortedlist = _identityRepository.GetAll().Include(s => s.Semester).Include(s => s.StudentInfo);

            string departmentName = _departmentManager.GetSingleDepartment(dept).DepartmentName;
            var semester = _semesterRepository.FindSingle(sem);
            if (sem > 0 && dept > 0)
            {
                sortedlist = sortedlist.Where(s => s.SemesterId == sem && s.DepartmentId == dept);
            }
            else
            {
                return RedirectToAction("Index2", "StudentAllDetails");
            }
            _userActivityManager.InsertActivity(new UserActivity
            {
                ComputerIp = ClassifiedServices.GetUserIp(),
                UserActivityTypeId = 5,
                Activity = account.LoginIdentity + " Printed Student list of " + departmentName + "-" + semester,
                AccountId = account.AccountId,
                StudentId = "All",
                ActivityTime = DateTime.Now,

            });
            Session.Add("SelectedDepartment", departmentName);
            Session.Add("SelectedSemester", semester);
            var finalList = (from iden in sortedlist
                             let dt2 = iden.StudentInfo.DateOfBirth

                             select new StudentObjectDTO
                             {
                                 StudentIdentityId = iden.StudentIdentificationId,
                                 StudentId = iden.StudentId,
                                 Name = iden.StudentInfo.StudentName,
                                 FatherName = iden.StudentInfo.FathersName,
                                 DateOfBirth = iden.StudentInfo.DateOfBirth,
                                 AddedDate = iden.AddedDate,
                                 BatchNo = semester.BatchNo,
                                 DepartmentName = departmentName,
                                 Remarks = iden.Remark,
                                 // Advised = true

                             }).AsNoTracking().OrderBy(s => s.StudentId).ToList();
            foreach (var studentObjectDto in finalList)
            {
                studentObjectDto.Advised = _courseForStudentRepository.GetAll().AsNoTracking()
                                               .Count(
                                                   s =>
                                                       s.SemesterId == sem &&
                                                       s.StudentIdentificationId == studentObjectDto.StudentIdentityId) >
                                           0;
            }
            HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Students_of_" + departmentName + "" + (semester) + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";

            return View("XclForAdmissionPartSecond", finalList);
        }

        public ActionResult PrintSudentProfile(string id)
        {
            if (Session["UserObj"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];

            StudentInfo stduentProfile = (db.StudentInfos.Where(s => s.StudentId == id)).Single();
            string imageName = id + ".jpg";
            string physicalPath = Server.MapPath("~/Image/" + imageName);
            if (System.IO.File.Exists(physicalPath))
            {
                var pdf = new PdfResult(stduentProfile, "PrintSudentProfile");
                pdf.ViewBag.Title = id + "'s Profile";
                return pdf;
            }
            _userActivityManager.InsertActivity(new UserActivity
            {
                ComputerIp = ClassifiedServices.GetUserIp(),
                UserActivityTypeId = 10,
                Activity = account.LoginIdentity + " Printed Student Profile",
                AccountId = account.AccountId,
                StudentId = stduentProfile.StudentId,
                ActivityTime = DateTime.Now,

            });
            return new PdfResult(stduentProfile, "PrintSudentProfile");

        }
        public ActionResult PrintStudentSectionWise(int? id)
        {
            if (Session["UserObj"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];

            var findSection = db.Sections.Find(id);
            var findStudent = db.CourseForStudentsAcademics.Where(s => s.SectionId == id);
            int serial = 1;
            var data2 = new List<dynamic>();
            foreach (var courseForStudentsAcademic in findStudent)
            {
                dynamic student1 = new ExpandoObject();
                student1.serial = serial;
                student1.SectionName = findSection.SectionName;
                student1.Teacher = findSection.Teacher.Account.Name;
                student1.Department = findSection.CourseForDepartment.Department.DepartmentName;
                student1.CourseName = findSection.CourseForDepartment.CourseName;
                student1.CourseCode = findSection.CourseForDepartment.CourseCode;
                student1.Semester = findSection.Semester.SemesterNYear;
                student1.StudentId = courseForStudentsAcademic.StudentIdentification.StudentId;
                data2.Add(student1);
                serial++;
            }
            _userActivityManager.InsertActivity(new UserActivity
            {
                ComputerIp = ClassifiedServices.GetUserIp(),
                UserActivityTypeId = 4,
                Activity = account.LoginIdentity + " Printed Section Index of : " + findSection.SectionId,
                AccountId = account.AccountId,
                StudentId = "All",
                ActivityTime = DateTime.Now,

            });
            return new PdfResult(data2, "PrintStudentSectionWise");
        }
        public ActionResult PrintStudentBySection(int? id)
        {
            if (Session["UserObj"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];

            var findSection = db.Sections.Find(id);
            if (findSection == null) return RedirectToAction("Index", "CourseForStudent");
            var findStudents = db.CourseForStudentsAcademics.Where(s => s.SectionId == findSection.SectionId);
            if (findStudents.Any())
            {
                _userActivityManager.InsertActivity(new UserActivity
                {
                    UserActivityTypeId = 4,
                    Activity = account.LoginIdentity + " Printed Section Index of : " + findSection.SectionId,
                    AccountId = account.AccountId,
                    StudentId = "All",
                    ActivityTime = DateTime.Now,

                });
                return new PdfResult(findStudents, "PrintStudentSectionWise");
            }
            return RedirectToAction("Index", "Home");
        }
        public ActionResult PrintPayment(int paymentSlipNo = 0)
        {
            if (Session["UserObj"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];

            if (_paymentManager.GetAllPayments().Count(s => s.PaymentId == paymentSlipNo) > 0)
            {
                var findPayment = _paymentManager
                    .GetSinglePayment(paymentSlipNo);
                var findRegidstartion = _paymentManager
                     .GetSinglePaymentRegistration(findPayment.PaymentRegistrationId);
                var findStudentInfo = _studentManager
                    .GetStudentInfoByStudentId(findRegidstartion.RegistrationFor);
                var findStudentDept = _studentManager.GetSingleStudentByStudentId(findRegidstartion.RegistrationFor);
                var findTakenCourses = _courseForStudentManger
                    .GetallAdvisedCoursesByPaymentRegis(findPayment.PaymentRegistrationId);

                var paymenInfoes = new PaymentInfo
                {
                    AdvisedBy = _accountManager.GetAllAccounts()
                    .FirstOrDefault(s => s.LoginIdentity == findPayment.PrintedBy).Name,
                    AdvisorId = findPayment.PrintedBy,
                    PrintedBy = account.LoginIdentity
                };
                Session.Add("PaymentOtherInfo", paymenInfoes);
                Session.Add("StudentVar", findStudentInfo);
                Session.Add("StudentDeptInfo", findStudentDept);
                Session.Add("findTakenCourses", findTakenCourses);
                // Activity Monitor
                _userActivityManager.InsertActivity(new UserActivity
                {
                    UserActivityTypeId = 3,
                    Activity = account.LoginIdentity + " Printed Payment Slip : " + paymentSlipNo,
                    AccountId = account.AccountId,
                    StudentId = findStudentDept.StudentId,
                    ActivityTime = DateTime.Now,

                });
                // Activity Monitor
                return new PdfResult(findPayment, "PrintPayment");
            }
            return new PdfResult("PrintPayment");
        }
        public ActionResult PrintRegistrations(int? DepartmentId, int? SemesterId)
        {
            if (Session["UserObj"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];

            var getAllRegistrations = db.PaymentRegistrations;
            var sortedRegis = getAllRegistrations.Where(s => s.StudentIdentification.DepartmentId == DepartmentId && s.SemesterId == SemesterId);
            // Activity Monitor
            _userActivityManager.InsertActivity(new UserActivity
            {
                ComputerIp = ClassifiedServices.GetUserIp(),
                UserActivityTypeId = 14,
                Activity = account.LoginIdentity + " Printed Registrations",
                AccountId = account.AccountId,
                StudentId = "All",
                ActivityTime = DateTime.Now,

            });
            // Activity Monitor
            return new PdfResult(sortedRegis, "PrintRegistrations");
        }
        public ActionResult PrintStudentByCourseTaken(int semesterId = 0, int courseForDepartmentId = 0)
        {
            if (Session["UserObj"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (semesterId > 0 && courseForDepartmentId > 0)
            {
                var getallStudentList =
                    _courseForStudentManger.GetAllCourseForStudentsAcademicsByCourseId(courseForDepartmentId)
                        .Where(s => s.SemesterId == semesterId);
                var getCourseInfo = _courseForDepartmentManager.GetSingleCourseForDepartment(courseForDepartmentId);
                if (getallStudentList.Any())
                    // Activity Monitor
                    _userActivityManager.InsertActivity(new UserActivity
                    {
                        UserActivityTypeId = 6,
                        Activity = account.LoginIdentity + " Printed Advised Student list of : " + getCourseInfo.CourseName + "-" + getCourseInfo.CourseCode,
                        AccountId = account.AccountId,
                        StudentId = "All",
                        ActivityTime = DateTime.Now,

                    });
                // Activity Monitor
                return new PdfResult(getallStudentList, "PrintStudentListByCourseTaken");
            }
            return RedirectToAction("Index", "CourseForStudent");
        }
        [HttpGet]
        public ActionResult GenerateSectionsXcelReport(int id)
        {
            if (Session["UserObj"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];

            var findSection = db.Sections.Find(id);
            TempData["sectionInfo"] = findSection;
            if (findSection == null) return RedirectToAction("Index", "CourseForStudent");
            var findStudents = _courseForStudentManger.GetAllCourseForStudentsAcademicsBysection(id);
            if (!findStudents.Any()) return RedirectToAction("ErrorPage", "Home");
            // Activity Monitor
            _userActivityManager.InsertActivity(new UserActivity
            {
                UserActivityTypeId = 6,
                Activity = account.LoginIdentity + " Printed EXCEL Report of : " + id,
                AccountId = account.AccountId,
                StudentId = "All",
                ActivityTime = DateTime.Now,

            });
            // Activity Monitor
            HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Students_of_" + findSection.SectionName + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            //this.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            return View("_XlsFileForSection", findStudents);
        }
        public Section GetSectionInfo(int sectionId)
        {
            var findSection = db.Sections.Find(sectionId);
            return findSection;
        }
        public ActionResult PrintGradePdfReport(int sectionId = 0, int actionId = 0, int diretctDownload = 0)
        {
            if (Session["UserObj"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (sectionId > 0)
            {
                string specifiedTerms = "Final Term";
                var termStatus = _semesterManager.GetActiveSemester();
                if (termStatus.MidTerm)
                {
                    specifiedTerms = "Mid Term";
                }
                var getSectiondata = GetSectionInfo(sectionId);

                TempData["termStatus"] = specifiedTerms;
                TempData["sectionInfo"] = getSectiondata;


                var findSectionLoad = _courseForStudentManger.GetAllCourseForStudentsAcademicsBysection(sectionId);
                var findCourseType = findSectionLoad.First().CourseForDepartment.CourseType;
                Session["coursetype"] = findCourseType;
                if (findSectionLoad.Any())
                {
                    // Activity Monitor
                    _userActivityManager.InsertActivity(new UserActivity
                    {
                        UserActivityTypeId = 7,
                        Activity = account.LoginIdentity + " Printed " + specifiedTerms + " Grade Report Of " + sectionId,
                        AccountId = account.AccountId,
                        StudentId = "All",
                        ActivityTime = DateTime.Now,

                    });
                    // Activity Monitor
                    if (diretctDownload > 0)
                    {
                        Response.AddHeader("content-disposition", "attachment; filename=" + findSectionLoad.First().CourseForDepartment.CourseCode + "- Grade Report.pdf");
                        Response.ContentType = "application/pdf";
                    }
                    var pdf = new PdfResult(findSectionLoad, "PrintGradePdfReport");
                    pdf.ViewBag.Title = "Mark Sheet";
                    return pdf;
                    //return new RazorPDF.PdfResult(findSectionLoad, "PrintGradePdfReport");
                }
            }
            if (actionId == 3)
            {
                return RedirectToAction("Index", "Section");
            }
            return actionId == 1 ? RedirectToAction("Index", "Section") : RedirectToAction("GradeUpload", "FacultyOrTeacher");
        }
        public ActionResult PrintStudentsAdmitCard(string term, int sem, int dept = 0)
        {
            if (Session["UserObj"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "ExamCtrl" && account.AccountsRole.AccountType != "Exam" && account.AccountsRole.AccountType != "Support") return RedirectToAction("NotPermitted", "Home");
            if (sem > 0 && dept > 0)
            {

                var findStudents =
                    from stud in _studentManager.GetAllStudentByDepartment(dept).Where(s => s.SemesterId == sem)
                    orderby stud.StudentId.Length, stud.StudentId
                    select stud;
                var findSemester = _semesterManager.GetActiveSemester();
                var courseList = new List<CourseForStudentsAcademic>();
                var getAllTakenCourse = _courseForStudentManger.GetAllCourseForStudentsAcademicsByDeptAndSemester(dept,
                    findSemester.SemesterId).Where(s => s.CourseStatusId != 4);
                var getDepartmentName = _departmentManager.GetSingleDepartment(dept).DepartmentName;
                foreach (var studentIdentification in findStudents)
                {
                    StudentIdentification identification = studentIdentification;
                    foreach (var courseForStudentsAcademic in getAllTakenCourse.Where(s => s.StudentIdentificationId == identification.StudentIdentificationId).Take(9))
                    {
                        if (courseForStudentsAcademic.CourseForDepartment.CourseName.Length > 44)
                        {
                            courseForStudentsAcademic.CourseForDepartment.CourseName = courseForStudentsAcademic.CourseForDepartment.CourseName.Substring(0, 42) + "...";
                            courseList.Add(courseForStudentsAcademic);
                        }
                        else
                        {
                            courseList.Add(courseForStudentsAcademic);
                        }
                    }
                }
                // Activity Monitor
                _userActivityManager.InsertActivity(new UserActivity
                {
                    ComputerIp = ClassifiedServices.GetUserIp(),
                    UserActivityTypeId = 8,
                    Activity = account.LoginIdentity + " Printed Admit Card Of " + term + " in " + findSemester.SemesterNYear + "-" + getDepartmentName,
                    AccountId = account.AccountId,
                    StudentId = "All",
                    ActivityTime = DateTime.Now,

                });
                // Activity Monitor
                Session.Add("termName", term);
                Session.Add("studentObj", findStudents);
                Session.Add("semesterInfo", findSemester);
                return new PdfResult(courseList, "PrintStudentsAdmitCard");
            }
            return RedirectToAction("Index2", "StudentAllDetails");
        }
        public ActionResult SemesterWiseGrade(int studenIdenId, int semesterId, int diretctDownload = 0)
        {
            if (Session["UserObj"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "ExamCtrl" && account.AccountsRole.AccountType != "Exam" && account.AccountsRole.AccountType != "Support") return RedirectToAction("NotPermitted", "Home");
            /////Main Code Start
            var findStudent = _studentManager.GetSingleStudentByStudentIdentification(studenIdenId);
            var takenCourses = _courseForStudentManger.GetAllCourseByStudent(studenIdenId).Where(s => s.SemesterId == semesterId);
            var semesterWiseCalculation = _gradeManager.CalulateScoresBySemester(studenIdenId, semesterId);
            var findSemester = _semesterManager.GetSingleSemester(semesterId);
            if (takenCourses.Any())
            {
                // Activity Monitor
                _userActivityManager.InsertActivity(new UserActivity
                {
                    ComputerIp = ClassifiedServices.GetUserIp(),
                    UserActivityTypeId = 11,
                    Activity = account.LoginIdentity + " Printed Actual " + findSemester.SemesterNYear + " Grade Sheet",
                    AccountId = account.AccountId,
                    StudentId = findStudent.StudentId,
                    ActivityTime = DateTime.Now,
                });
                // Activity Monitor
                Session.Add("semesterScore", semesterWiseCalculation);
                Session.Add("studentObj", findStudent);
                Session.Add("verifiedBy", account);
                if (diretctDownload > 0)
                {
                    Response.AddHeader("content-disposition", "attachment; filename=" + findStudent.StudentId + "-" + findSemester.SemesterNYear + "-Grade Report.pdf");
                    Response.ContentType = "application/pdf";

                }
                return new PdfResult(takenCourses.ToList(), "PdfForSemesterWiseGrade");
            }
            return RedirectToAction("StudentsCourseListBySemester", "StudentAllDetails");
        }
        public ActionResult SemesterWiseGradeInitial(int studenIdenId, int semesterId, int diretctDownload = 0)
        {
            if (Session["UserObj"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "ExamCtrl" && account.AccountsRole.AccountType != "Exam" && account.AccountsRole.AccountType != "Support") return RedirectToAction("NotPermitted", "Home");
            /////Main Code Start
            var findStudent = _studentManager.GetSingleStudentByStudentIdentification(studenIdenId);
            var takenCourses = _courseForStudentManger.GetAllCourseByStudent(studenIdenId).Where(s => s.SemesterId == semesterId);
            var semesterWiseCalculation = _gradeManager.GetCgpaSingleSemesterInitial(studenIdenId, semesterId);
            var getOnlyActualCgpa = _gradeManager.CalulateScoresBySemester(studenIdenId, semesterId);
            semesterWiseCalculation.Cgpa = getOnlyActualCgpa.Cgpa;
            var findSemester = _semesterManager.GetSingleSemester(semesterId);
            if (takenCourses.Any())
            {
                //
                // Activity Monitor
                _userActivityManager.InsertActivity(new UserActivity
                {
                    ComputerIp = ClassifiedServices.GetUserIp(),
                    UserActivityTypeId = 11,
                    Activity = account.LoginIdentity + " Printed Initial " + findSemester.SemesterNYear + " Grade Sheet",
                    AccountId = account.AccountId,
                    StudentId = findStudent.StudentId,
                    ActivityTime = DateTime.Now,

                });
                // Activity Monitor
                Session.Add("semesterScore", semesterWiseCalculation);
                Session.Add("studentObj", findStudent);
                Session.Add("verifiedBy", account);
                if (diretctDownload > 0)
                {
                    Response.AddHeader("content-disposition", "attachment; filename=" + findStudent.StudentId + "-" + findSemester.SemesterNYear + "-Grade Report.pdf");
                    Response.ContentType = "application/pdf";

                }
                return new PdfResult(takenCourses.ToList(), "PdfForSemesterWiseGradeInitial");
            }
            return RedirectToAction("StudentsCourseListBySemester", "StudentAllDetails");
        }
        public ActionResult TabulationPrint()
        {
            if (Session["UserObj"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "ExamCtrl" && account.AccountsRole.AccountType != "Exam")
                return RedirectToAction("NotPermitted", "Home");
            ViewBag.semester = TempData["semesterData"];
            var deptId = (int)TempData["deptid"];
            var tabulationfData = TempData["tabulationData"];
            var getDepartment = _departmentManager.GetSingleDepartment(deptId).DepartmentName;
            ViewData["createTabulation"] = TempData["cumulativeData"];
            ViewData["meritListA"] = TempData["meritListA"];
            ViewData["meritListB"] = TempData["meritListB"];
            ViewBag.deptName = getDepartment;
            string fileName = "TabulationSheet";

            HttpContext.Response.AddHeader("content-disposition", "attachment; filename=" + fileName + ".xls");
            Response.ContentType = "application/vnd.ms-excel";
            // Activity Monitor
            _userActivityManager.InsertActivity(new UserActivity
            {
                ComputerIp = ClassifiedServices.GetUserIp(),
                UserActivityTypeId = 9,
                Activity = account.LoginIdentity + " Printed Tabulation Sheet of " + getDepartment,
                AccountId = account.AccountId,
                StudentId = "All",
                ActivityTime = DateTime.Now,

            });
            // Activity Monitor
            return View(tabulationfData);

        }
        //public PdfResult PdfForSemesterWiseGrade(int studenIdenId, int semesterId)
        //{
        //   var account = (Account)Session["UserObj"];
        //    /////Main Code Start
        //    var findStudent = _studentManager.GetSingleStudentByStudentIdentification(studenIdenId);
        //    var takenCourses = _courseForStudentManger.GetAllCourseByStudent(studenIdenId).Where(s => s.SemesterId == semesterId);
        //    var semesterWiseCalculation = _gradeManager.CalulateScoresBySemester(studenIdenId, semesterId);
        //    var findSemester = _semesterManager.GetSingleSemester(semesterId);
        //    if (takenCourses.Any())
        //    {
        //        Session.Add("semesterScore", semesterWiseCalculation);
        //        Session.Add("studentObj", findStudent);
        //        Session.Add("verifiedBy", account);
        //        Response.AddHeader("content-disposition", "attachment; filename=" + findStudent.StudentId + "-" + findSemester.SemesterNYear + "-Grade Report.pdf");
        //        Response.ContentType = "application/pdf";
        //    }
        //    return new RazorPDF.PdfResult(takenCourses.ToList(), "PdfForSemesterWiseGrade");
        //}
        public ActionResult PrintGradingHistory(DateTime? starTime, DateTime? endTime)
        {
            try
            {
                if (starTime != null && endTime != null)
                {
                    var getallGradingHistory =
                    db.GradingHistories.Where(
                        s =>
                            EntityFunctions.TruncateTime(s.ChangedTime) >= starTime &&
                            EntityFunctions.TruncateTime(s.ChangedTime) <= endTime);
                    return new PdfResult(getallGradingHistory, "PrintGradingHistory");
                }
            }
            catch (DbException)
            {

                throw;
            }
            return RedirectToAction("GradingHistory", "Admin");
        }
        public ActionResult PrintSectionProposal(AdvisingPagesort advisingPagesort, int diretctDownload = 0)
        {
            if (Session["UserObj"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];

            string teacherName = "---";
            if (advisingPagesort.TeacherId != null)
            {
                teacherName = _teacherManager.GetSingleTeacher(advisingPagesort.TeacherId).Account.Name;
            }
            var courseforstudentsacademics =
                 _courseForStudentManger.GetAllCourseForStudentsAcademicsByCourseId(advisingPagesort.CourseId)
                 .Where(s => s.SemesterId == advisingPagesort.SemesterId);
            var getCourseInfo = _courseForDepartmentManager.GetSingleCourseForDepartment(advisingPagesort.CourseId);
            switch (advisingPagesort.SecSort)
            {
                case 0:
                    break;
                case 1:
                    courseforstudentsacademics = courseforstudentsacademics.Where(s => s.SectionId != null);
                    break;
                case 2:
                    courseforstudentsacademics = courseforstudentsacademics.Where(s => s.SectionId == null);
                    break;
            }
            if (courseforstudentsacademics.Any())
            {
                // Activity Monitor
                Random r = new Random();
                int n = r.Next(10000000, 99999999);
                _userActivityManager.InsertActivity(new UserActivity
                {
                    ComputerIp = ClassifiedServices.GetUserIp(),
                    UserActivityTypeId = 15,
                    Activity =
                        account.LoginIdentity + "Printed Section Proposal, Code " + n + " of " +
                        getCourseInfo.CourseCode + "-" + getCourseInfo.Department.DepartmentName,
                    AccountId = account.AccountId,
                    StudentId = "All",
                    ActivityTime = DateTime.Now,
                });
                if (diretctDownload > 0)
                {
                    Response.AddHeader("content-disposition",
                        "attachment; filename=Section_proposal-" + getCourseInfo.CourseCode + ".pdf");
                    Response.ContentType = "application/pdf";
                }
                var pdf = new PdfResult(courseforstudentsacademics.ToList(), "PrintStudentListByCourseTaken");
                pdf.ViewBag.Title = "Section Proposal";
                pdf.ViewBag.teacherName = teacherName;
                pdf.ViewBag.courseObj = getCourseInfo;
                pdf.ViewBag.userObj = account;
                pdf.ViewBag.IdentityCode = n;
                pdf.ViewBag.printCategory = diretctDownload;
                return pdf;
            }
            return RedirectToAction("Index", "CourseForStudent");
        }
        public ActionResult StatisticalDataPrint(int formBatch, int toBatch)
        {
            if (Session["UserObj"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "ExamCtrl" && account.AccountsRole.AccountType != "Exam")
                return RedirectToAction("NotPermitted", "Home");

            var semesterInfo = _semesterManager.GetActiveSemester();
            var getData = _courseBusinessLogicController.GenerateStatisticalData(formBatch, toBatch);
            var pdf = new PdfResult(getData, "PrintStatisticalData");

            //Batch Calculator
            var semesters = _semesterManager.GetAllSemester().Where(s => s.SemesterId >= formBatch && s.SemesterId <= toBatch)
                .OrderByDescending(s => s.SemesterId).ToList();
            string batches = semesters.Aggregate("", (current, semester) => current + semester.BatchNo + " ");
            //Batch Calculator

            pdf.ViewBag.batches = batches;
            pdf.ViewBag.sem = semesterInfo.SemesterNYear;

            return pdf;
        }
        public ActionResult PrintAdvisedDetails(int studentId = 0, int semesterId = 0)
        {
            if (Session["UserObj"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (studentId > 0 && semesterId > 0)
            {
                var clculatedQuery =
                    _courseForStudentManger.GetAllCoursesByIdentificationIdAndSemester(studentId, semesterId)
                        .Include(s => s.Semester)
                        .Include(s => s.CourseForDepartment);
                var getSemesterWiseCourses = (from course in clculatedQuery
                                              select new AdvisedCourse
                                              {
                                                  CourseCode = course.CourseForDepartment.CourseCode,
                                                  CourseTitle = course.CourseForDepartment.CourseName,
                                                  Credits = course.CourseForDepartment.Credit
                                              }).ToList();

                var pdf = new PdfResult(getSemesterWiseCourses, "GetAdvicedPDF");
                var semester = _semesterManager.GetSingleSemester(semesterId);
                var student = _studentManager.GetSingleStudentByStudentIdentification(studentId);
                var payment =
                    _paymentManager.GetAllPayments()
                        .FirstOrDefault(
                            s =>
                                s.PaymentRegistration.StudentIdentificationId == studentId &&
                                s.PaymentRegistration.SemesterId == semesterId);
                pdf.ViewBag.sem = semester.SemesterNYear;
                pdf.ViewBag.studentId = student.StudentId;
                pdf.ViewBag.name = student.StudentInfo.StudentName;
                pdf.ViewBag.department = student.Department.DepartmentName;
                if (payment != null)
                {
                    pdf.ViewBag.admission = payment.AdmissionFee = 0;
                    pdf.ViewBag.tution = payment.TutionFee = 0;
                    pdf.ViewBag.lab = payment.LabThesisFee = 0;
                    pdf.ViewBag.other = payment.OtherFee = 0;
                    pdf.ViewBag.reregi = payment.ReRegiFee = 0;
                    pdf.ViewBag.late = payment.LateFee = 0;
                    pdf.ViewBag.total = payment.Total = 0;
                }
                _userActivityManager.InsertActivity(new UserActivity
                {
                    UserActivityTypeId = 3,
                    Activity = account.LoginIdentity + " Printed Advising ",
                    AccountId = account.AccountId,
                    StudentId = student.StudentId,
                    ActivityTime = DateTime.Now,
                });
                return pdf;
            }
            return RedirectToAction("AdvisedStudentList", "CourseForStudent");
        }
        //public ActionResult PrintAdvisedDetails(int studentId = 0, int semesterId = 0)
        //{
        //    if (Session["UserObj"] == null)
        //        return RedirectToAction("LogInresultSubmit", "Home");
        //    var account = (Account)Session["UserObj"];
        //    if (studentId > 0 && semesterId > 0)
        //    {
        //        var getSemesterWiseCourses =
        //            _courseForStudentManger.GetAllCoursesByIdentificationIdAndSemester(studentId, semesterId);
        //        var advisedCourses = new List<AdvisedCourse>();
        //        foreach (var course in getSemesterWiseCourses)
        //        {
        //            var advisedCourseObj = new AdvisedCourse()
        //            {
        //                CourseCode = course.CourseForDepartment.CourseCode,
        //                CourseTitle = course.CourseForDepartment.CourseName,
        //                Credits = course.CourseForDepartment.Credit
        //            };
        //            advisedCourses.Add(advisedCourseObj);
        //        }
        //        var pdf = new PdfResult(advisedCourses, "GetAdvicedPDF");
        //        var semester = _semesterManager.GetSingleSemester(semesterId);
        //        var student = _studentManager.GetSingleStudentByStudentIdentification(studentId);
        //        var payment =
        //            _paymentManager.GetAllPayments()
        //                .FirstOrDefault(
        //                    s =>
        //                        s.PaymentRegistration.StudentIdentificationId == studentId &&
        //                        s.PaymentRegistration.SemesterId == semesterId);
        //        pdf.ViewBag.sem = semester.SemesterNYear;
        //        pdf.ViewBag.studentId = student.StudentId;
        //        pdf.ViewBag.name = student.StudentInfo.StudentName;
        //        pdf.ViewBag.department = student.Department.DepartmentName;
        //        if (payment.AdmissionFee != null && payment.TutionFee != null && payment.LabThesisFee != null && payment.OtherFee != null && payment.ReRegiFee != null && payment.LateFee != null)
        //        {
        //            pdf.ViewBag.admission = payment.AdmissionFee=0;
        //            pdf.ViewBag.tution = payment.TutionFee=0;
        //            pdf.ViewBag.lab = payment.LabThesisFee=0;
        //            pdf.ViewBag.other = payment.OtherFee=0;
        //            pdf.ViewBag.reregi = payment.ReRegiFee=0;
        //            pdf.ViewBag.late = payment.LateFee=0;
        //            pdf.ViewBag.total = payment.Total=0;
        //        }
        //        _userActivityManager.InsertActivity(new UserActivity
        //        {
        //            UserActivityTypeId = 3,
        //            Activity = account.LoginIdentity + " Printed Advising ",
        //            AccountId = account.AccountId,
        //            StudentId = student.StudentId,
        //            ActivityTime = DateTime.Now,
        //        });
        //        return pdf;
        //    }
        //    return RedirectToAction("AdvisedStudentList", "CourseForStudent");
        //}
        public ActionResult PrintSortedCgpa(int departmentId = 0, int sortBy = 0)
        {
            if (Session["UserObj"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "ExamCtrl" && account.AccountsRole.AccountType != "Exam")
                return RedirectToAction("NotPermitted", "Home");
            var getDepartmentInfo = "";
            if (departmentId > 1)
            {
                getDepartmentInfo = _departmentManager.GetSingleDepartment(departmentId).DepartmentName;
            }
            var getSemesterInfo = _semesterManager.GetActiveSemester().SemesterNYear;
            var allStudent = _studentManager.GetAllStudentByDepartment(departmentId).OrderBy(s => s.StudentId);
            var cgpaList = new List<CumulativeScores>();
            foreach (var studentIdentification in allStudent)
            {
                var getCgpa = _gradeManager.CalculateCgpaofAStudent(studentIdentification.StudentIdentificationId);
                getCgpa.Name = studentIdentification.StudentInfoId == null ? "" : studentIdentification.StudentInfo.StudentName;
                getCgpa.DepartmentalId = studentIdentification.StudentId;
                cgpaList.Add(getCgpa);
            }
            if (sortBy > 0)
            {
                cgpaList = sortBy == 1 ? cgpaList.OrderBy(s => s.DepartmentalId).ToList() : cgpaList.OrderByDescending(s => s.Cgpa).ToList();
            }
            ViewBag.departmentId = departmentId;
            var pdf = new PdfResult(cgpaList, "PrintSortedCgpa");
            pdf.ViewBag.DepartmentName = getDepartmentInfo;
            pdf.ViewBag.SemesterName = getSemesterInfo;
            pdf.ViewBag.acc = account.LoginIdentity;
            _userActivityManager.InsertActivity(new UserActivity
            {
                ComputerIp = ClassifiedServices.GetUserIp(),
                UserActivityTypeId = 16,
                Activity = account.LoginIdentity + " Printed CGPA, PDF result  of " + getDepartmentInfo + ", " + getSemesterInfo,
                AccountId = account.AccountId,
                StudentId = "All",
                ActivityTime = DateTime.Now,
            });
            return pdf;
        }
        public ActionResult ExcelForSortedCgpa(int departmentId = 0, int sortBy = 0)
        {
            if (Session["UserObj"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "ExamCtrl" && account.AccountsRole.AccountType != "Exam")
                return RedirectToAction("NotPermitted", "Home");

            var getDepartmentInfo = "";
            if (departmentId > 1)
            {
                getDepartmentInfo = _departmentManager.GetSingleDepartment(departmentId).DepartmentName;
            }
            var getSemesterInfo = _semesterManager.GetActiveSemester().SemesterNYear;
            var allStudent = _studentManager.GetAllStudentByDepartment(departmentId).OrderBy(s => s.StudentId);
            var cgpaList = new List<CumulativeScores>();
            foreach (var studentIdentification in allStudent)
            {
                var getCgpa = _gradeManager.CalculateCgpaofAStudent(studentIdentification.StudentIdentificationId);
                getCgpa.Name = studentIdentification.StudentInfoId == null ? "" : studentIdentification.StudentInfo.StudentName;
                getCgpa.DepartmentalId = studentIdentification.StudentId;
                cgpaList.Add(getCgpa);
            }
            if (sortBy > 0)
            {
                cgpaList = sortBy == 1 ? cgpaList.OrderBy(s => s.DepartmentalId).ToList() : cgpaList.OrderByDescending(s => s.Cgpa).ToList();
            }
            ViewBag.departmentId = departmentId;
            ViewBag.SemesterName = getSemesterInfo;
            ViewBag.acc = account.LoginIdentity;
            ViewBag.DepartmentName = getDepartmentInfo;

            string fileName = "CGPA_Sheet";
            HttpContext.Response.AddHeader("content-disposition", "attachment; filename=" + fileName + ".xls");
            Response.ContentType = "application/vnd.ms-excel";
            _userActivityManager.InsertActivity(new UserActivity
            {
                UserActivityTypeId = 16,
                Activity = account.LoginIdentity + " Printed CGPA, Excel result  of " + getDepartmentInfo + ", " + getSemesterInfo,
                AccountId = account.AccountId,
                StudentId = "All",
                ActivityTime = DateTime.Now,
            });
            return View(cgpaList);
        }
        //Activity type Trans1
        public ActionResult PrintTransCript(int studentId = 0, int type = 1)
        {
            // CR date 15-sep-2015
            if (Session["UserObj"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && _powerWorkBusinessLogic.CheckMethodForAccess("PrintTransCript", account.AccountId))
                return RedirectToAction("NotPermitted", "Home");
            var userData = (Account)Session["UserObj"];
            if (studentId > 0)
            {
                var getStudent = _studentManager.GetSingleStudent(studentId);
                var takencourses = _courseForStudentManger.GetAllCourseByStudent(studentId);
                var getAllCgpa = _gradeManager.CalculateCgpaAllSemester(studentId);
                var getCumulativeScore = _gradeManager.CalculateCgpaofAStudent(studentId);
                var pdf = new PdfResult(takencourses, "PrintTranscriptUnoff");
                pdf.ViewBag.Gpa = getAllCgpa;
                pdf.ViewBag.Student = getStudent;
                pdf.ViewBag.CumulativeScore = getCumulativeScore;
                pdf.ViewBag.PrintedBy = userData;
                pdf.ViewBag.printType = type;
                //=======Track Activity=======================
                string activityType = "Printed Official Transcript.";
                if (type == 2)
                {
                    activityType = "Printed Unofficial Transcript.";
                }
                TrackActivity(17, activityType, getStudent.StudentId);
                //=======Track Activity=======================
                return pdf;
            }
            return RedirectToAction("ErrorPage", "Home");

        }

        public ActionResult PrintTransCriptCover(TranscriptPrintOptions options)
        {
            if (Session["UserObj"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && _powerWorkBusinessLogic.CheckMethodForAccess("PrintTrCover", account.AccountId))
                return RedirectToAction("NotPermitted", "Home");


            if (options.StudentIdenId > 0)
            {
                var student = _studentManager.GetSingleStudent(options.StudentIdenId);
                var allCgpa = _gradeManager.CalculateCgpaofAStudent(options.StudentIdenId);
                var sem = _courseForStudentManger.GetAllCompletedCourseOfAStudent(options.StudentIdenId).AsEnumerable()
                    .Select(s => new Semester
                    {
                        SemesterId = s.SemesterId,
                        SemesterNYear = s.Semester.SemesterNYear
                    }).ToList();
                //var getAllCoursesBySyudent = _courseForStudentManger.GetAllCourseByStudent(options.StudentIdenId);
                var getAllCoursesBySyudent = _courseForStudentManger.GetAllCourseForStudentsAcademics().Where(s => s.StudentIdentificationId == options.StudentIdenId && s.DeclareMajor);
                var pdf = new PdfResult(student, "PrintTransCriptCover");
                if (getAllCoursesBySyudent.Any())
                {
                    string majorSubjects = "";
                    foreach (var c in getAllCoursesBySyudent)
                    {
                        majorSubjects = majorSubjects +
                                        "(" + c.CourseForDepartment.CourseName + ")" + Environment.NewLine;
                    }
                    pdf.ViewBag.major = majorSubjects;
                }
                pdf.ViewBag.allCGPA = allCgpa;
                pdf.ViewBag.printedBy = account;
                pdf.ViewBag.firstSem = sem.FirstOrDefault();
                pdf.ViewBag.lastSem = sem.OrderByDescending(s => s.SemesterId).FirstOrDefault();
                pdf.ViewBag.date = options.PublishingDate;
                //=======Track Activity=======================
                string activityType = "Printed Transcript Front Page.";
                TrackActivity(17, activityType, student.StudentId);
                //=======Track Activity=======================
                Response.AddHeader("content-disposition", "attachment; filename=" + student.StudentId + "-" + "-Cover Page.pdf");
                Response.ContentType = "application/pdf";
                return pdf;

            }
            return RedirectToAction("ErrorPage", "Home");
        }

        public void TrackActivity(int type, string activityName, string studentId)
        {
            if (Session["UserObj"] != null)
            {
                var account = (Account)Session["UserObj"];
                _userActivityManager.InsertActivity(new UserActivity
                {
                    ComputerIp = ClassifiedServices.GetUserIp(),
                    UserActivityTypeId = type,
                    Activity = account.LoginIdentity + " " + activityName,
                    AccountId = account.AccountId,
                    StudentId = studentId,
                    ActivityTime = DateTime.Now,
                });
            }

        }


    }
}