﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.OtherSpecialService;

namespace UMS_MVC.Controllers.OtherSpecialControllers
{
    public class DepartmentChangeController : Controller
    {
        private UmsDbContext db = new UmsDbContext();
        private readonly StudentManager _studentManager = new StudentManager();
        private readonly SemesterManager _semesterManager = new SemesterManager();
        private readonly DepartmentManager _departmentManager = new DepartmentManager();
        private readonly SchoolManager _schoolManager = new SchoolManager();
        private readonly DepartmentChangingManager _departmentChanging=new DepartmentChangingManager();
        private readonly UserActivityManager _userActivityManager=new UserActivityManager();

        public string GenerateStudentId(int departmentId = 0, int semesterId = 0)
        {
            if (departmentId > 0 && semesterId > 0)
            {
                int countStudent =
                    _studentManager.GetAllStudentByDepartment(departmentId)
                    .Count(s => s.SemesterId == semesterId);
                var getSemesterInfo =
                    _semesterManager.GetSingleSemester(semesterId);                  
                var departmentCode = _departmentManager.GetSingleDepartment(departmentId).DepartmentCode;
                if (countStudent > 0)
                {
                    string identity;
                    int nextId = countStudent;
                    do
                    {
                        nextId++;
                        identity = getSemesterInfo.BatchNo + nextId.ToString("D3") + departmentCode.ToString("D3");
                    } while (_studentManager.StudentExistanceByStudentId(identity));

                    return identity;
                }
                if (countStudent == 0)
                {
                    const int studentSerial = 1;
                    string identity = getSemesterInfo.BatchNo + studentSerial.ToString("D3") + departmentCode.ToString("D3");
                    return identity;
                }
            }
            return "-----";
        }

        public string GeneratePassword()
        {
          return  Passgenerator.GeneratePassword(3);
        }

        public ActionResult StudentDepartmentChange(string studentId, int SemesterId = 0, int DepartmentId = 0)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Support") return RedirectToAction("NotPermitted", "Home");
            try
            {
                var getAllSemester = _semesterManager.GetAllSemester().ToList();
                var getAllDepartment = _departmentManager.GetAllDepartment();
                ViewBag.SemesterId = new SelectList(getAllSemester, "SemesterId", "SemesterNYear",
                    SemesterId);
                ViewBag.DepartmentId = new SelectList(getAllDepartment, "DepartmentId", "DepartmentName", DepartmentId);
                var filteredSemStudent = _studentManager.GetAllStudentIdentifications().Include(s => s.StudentInfo);
                if (!string.IsNullOrEmpty(studentId))
                {
                    filteredSemStudent = filteredSemStudent.Where(s => s.StudentId.Contains(studentId));
                    if (!Queryable.Any<StudentIdentification>(filteredSemStudent))
                    {
                        ViewBag.StudentFoundMgs = "Student not found.";
                    }
                    return View(filteredSemStudent);
                }
                if (SemesterId > 0 && DepartmentId > 0)
                {
                    filteredSemStudent =
                        filteredSemStudent.Where(s => s.SemesterId == SemesterId && s.DepartmentId == DepartmentId);
                }
                else
                {
                    filteredSemStudent = Queryable.Take<StudentIdentification>(filteredSemStudent, 200);
                }
                return View(filteredSemStudent);
            }
            catch (Exception)
            {

                return RedirectToAction("ErrorPage", "Home");
            }
        }

        public ActionResult InfoDetails(int sIID)
        {
            try
            {
                var getAllschool = _schoolManager.GetAllSchools().ToList();
                var getAllDepartment = _departmentManager.GetAllDepartment();
                if (sIID > 0)
                {
                    var details = _studentManager.GetSingleStudentByStudentIdentification(sIID);
                    ViewBag.SchoolId = new SelectList(getAllschool, "SchoolId", "SchoolName");
                    ViewBag.DepartmentId = new SelectList(getAllDepartment, "DepartmentId", "DepartmentName");
                    //ViewBag.pic =
                    //    db.PicOfStudents.Where(s => s.ImageName == details.StudentId + ".jpg").Select(p => p.ImageName);
                    var getAcademic = _studentManager.GetAllAcademicInfos().Where(s => s.StudentId == details.StudentId);
                    var getDocument =
                        _studentManager.GetAllStudentDocument().Where(s => s.StudentId == details.StudentId);

                    if (getAcademic.Any() && getDocument.Any())
                    {
                        string acad = "";
                        foreach (var sai in getAcademic)
                        {
                            acad = acad + sai.StudentAcademicInfoId + "\t";
                        }
                        ViewBag.Academic = acad;
                        //ViewBag.Academic = getAcademic.First(s => s.StudentId==details.StudentId).StudentAcademicInfoId;
                        ViewBag.Document = getDocument.First(s => s.StudentId == details.StudentId).DocumentAddingId;
                    }
                    //
                    return View(details);
                }
                return View();
            }
            catch (Exception)
            {
                return RedirectToAction("ErrorPage", "Home");
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public string ImmediateChange(StudentIdentification studentIdentification, string nsid)
        {
            if (Session["UserObj"] == null)
                return "No Access";
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin"&& account.AccountsRole.AccountType!="Support") return "No Access";
            string msg;
            if (studentIdentification != null && !string.IsNullOrEmpty(nsid))
            {
                try
                {
                    var identification =
                        db.StudentIdentifications.Find(studentIdentification.StudentIdentificationId);
                    if (identification != null)
                    {
                        var departmentChangedHistory = new DepartmentChange
                        {
                            OldId = identification.StudentId,
                            NewId =nsid,
                            StudentIdentificationId = identification.StudentIdentificationId,
                            ChangedBy = account.LoginIdentity+"-"+account.Name,
                            PcIpAddress = ClassifiedServices.GetUserIp(),
                            DateOfDeptChange = DateTime.Now
                         };
                        db.DepartmentChanges.Add(departmentChangedHistory);
                        identification.SchoolId = 5;
                        identification.DepartmentId = studentIdentification.DepartmentId;
                        identification.StudentId = nsid;
                        db.Entry(identification).State = EntityState.Modified;
                        
                    }
                    var studentInfo = db.StudentInfos.FirstOrDefault(s => s.StudentId == studentIdentification.StudentId);
                    if (studentInfo != null)
                    {
                        studentInfo.StudentId = nsid;
                        db.Entry(studentInfo).State = EntityState.Modified;
                    }
                    var studentAcademicInfo =
                        db.StudentAcademicInfos.Where(s => s.StudentId == studentIdentification.StudentId);
                    if (studentAcademicInfo.Any())
                    {
                        foreach (var academicInfo in studentAcademicInfo)
                        {
                            academicInfo.StudentId = nsid;
                            db.Entry(academicInfo).State = EntityState.Modified;
                        }
                    }
                    var documentAdding =
                        db.DocumentsaddAddings.FirstOrDefault(s => s.StudentId == studentIdentification.StudentId);
                    if (documentAdding != null)
                    {
                        documentAdding.StudentId = nsid;
                        db.Entry(documentAdding).State = EntityState.Modified;
                    }
                   
                    db.SaveChanges();
                    msg = "All information updated";
                    return msg;
                }
                catch (DbException ds)
                {
                    msg = "Error : " + ds;
                    return msg;
                }
            }
            msg = "Department or Student Id not inserted !!";
            return msg;
        }

        public ActionResult DepartmentChangingHistory()
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" || account.AccountId != 1) return RedirectToAction("NotPermitted", "Home");
            return View(_departmentChanging.GetAllDepartmentChanges());
        }
        //alter student 
        public ActionResult AlterStudent(int sIID)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Support") return RedirectToAction("NotPermitted", "Home");
            try
            {
                var student = _studentManager.GetSingleStudentByStudentIdentification(sIID);
                var findEmail = _studentManager.GetAllStudentInfos().FirstOrDefault(s => s.StudentId == student.StudentId);
                ViewBag.email = findEmail;
                return PartialView("~/Views/DepartmentChange/AlterStudent.cshtml", student);
            }
            catch (Exception)
            {

                return RedirectToAction("ErrorPage", "Home");
            }
           
        }

        public string AddPassword(StudentIdentification studentIdentification, string email)
        {
            //var student = db.StudentIdentifications.Find(studentIdentification.StudentIdentificationId);
            var student = _studentManager.FindSingleStudentFiltered(studentIdentification.StudentIdentificationId);
            var errorMessages = "";
            string msg = "";
            if (student.StudentInfoId != null)
            {
                var studentInfo = _studentManager.FindSingleStudentInfoFiltered((int)student.StudentInfoId);
                try
                {
                    if (!string.IsNullOrEmpty(studentIdentification.Password))
                    {
                        student.Password = studentIdentification.Password;
                        msg = "Password";
                    }
                    if (studentIdentification.Validation)
                    {
                        student.Validation = studentIdentification.Validation;
                        msg = msg + ", Validation ";
                    }
                    //if (studentIdentification.BlockStudent && studentIdentification.BlockExpireDate!=null)
                    //{
                        student.BlockStudent = studentIdentification.BlockStudent;
                        student.BlockReason = studentIdentification.BlockReason;
                        student.BlockExpireDate = studentIdentification.BlockExpireDate;
                    //}
                    db.Entry(student).State = EntityState.Modified;
                    if (studentInfo != null)
                    {
                        studentInfo.EmailAddress = email;
                        db.Entry(studentInfo).State = EntityState.Modified;
                        msg =msg+" ,"+ studentInfo.EmailAddress;
                    }
                    db.SaveChanges();
                    return msg + " updated to the student profile successfully.";
                }
                catch (DbEntityValidationException es)
                {
                    foreach (DbEntityValidationResult validationResult in es.EntityValidationErrors)
                    {
                        string entityName = validationResult.Entry.Entity.GetType().Name;
                        foreach (DbValidationError error in validationResult.ValidationErrors)
                        {
                            errorMessages+=entityName + "." + error.PropertyName + ": " + error.ErrorMessage;
                        }
                    }
                    msg = errorMessages;
                    //msg = msg + " An error occured during updateLoginCounter !!";
                }

            }
            return msg;
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}