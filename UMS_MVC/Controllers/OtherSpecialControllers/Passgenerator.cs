﻿using System;
using System.Text;

namespace UMS_MVC.Controllers.OtherSpecialControllers
{
    public static class Passgenerator
    {
        private static int RandomNumber(int min, int max)
        {
            var random = new Random();
            return random.Next(min, max);
        }
        private static string RandomString(int size, bool lowerCase)
        {
            var builder = new StringBuilder();
            var random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }
       public static string GeneratePassword(int passChar=2)
        {
            var bd = new StringBuilder();
            bd.Append(RandomString(passChar, true));
            bd.Append(RandomNumber(100, 9999));
            //bd.Append(RandomString(2, false));
            return bd.ToString();

        }
    }
}