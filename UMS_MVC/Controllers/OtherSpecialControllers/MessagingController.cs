﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using PagedList;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.BLL;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.OtherSpecialService;

namespace UMS_MVC.Controllers.OtherSpecialControllers
{
    public class MessagingController : Controller
    {
        // GET: Messaging
        private UmsDbContext db = new UmsDbContext();
        private readonly AccountManager _accountManager = new AccountManager();
        private readonly UserActivityManager _activityManager = new UserActivityManager();
        private readonly MessageManager _messageManager = new MessageManager();
        public ActionResult Index(string msg)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin") return RedirectToAction("NotPermitted", "Home");
            ViewBag.account = account;
            if (msg != null)
            {
                ViewBag.msg = msg;
            }
            var accountHolders = db.Accounts.Where(s => s.AccountId != account.AccountId);
            return View(accountHolders);
        }
        public ActionResult ShowMessagingPart(int acc)
        {
            if (acc > 0)
            {
                var vUser = _accountManager.GetSingleAccount(acc);
                var newInternalMsg = new InternalMessage
                {
                    ReceiverAccount = vUser,
                    ReceiverAccountId = vUser.AccountId
                };
                return PartialView("~/Views/Messaging/_accountDetails.cshtml", newInternalMsg);
            }
            return RedirectToAction("Index", "Messaging");
        }
        //public ActionResult ShowMessagingPart(int acc)
        //{
        //    var account = (Account)Session["UserObj"];
        //    if (acc > 0)
        //    {
        //        var vUser = _accountManager.GetSingleAccount(acc);
        //        ViewBag.owner = account.AccountId;
        //        ViewBag.reciever = acc;
        //        return PartialView("~/Views/Messaging/_accountDetails.cshtml", vUser);
        //    }
        //    return RedirectToAction("Index", "Messaging");
        //}
        public bool CheckUploadedFileType(HttpPostedFileBase file)
        {
            var fileExtentions = new List<string> { ".doc", ".docx", ".xls", ".xlsx", ".pdf", ".ppt", ".txt", ".jpg", ".jpeg" };
            string extension = Path.GetExtension(file.FileName);
            if (fileExtentions.Contains(extension))
            {
                return true;
            }
            return false;
        }
        public string InsertNewMessage(InternalMessage message, HttpPostedFileBase attatchedfile)
        {
            var sb = new StringBuilder();
            var account = (Account)Session["UserObj"];
            if (account == null) return sb.Append("<font color='#ff5d16'>Your account is invalid to send message.</font> </br>").ToString();

            if (string.IsNullOrEmpty(message.MessageBody))
            {
                sb.Append("<font color='#ff5d16'>Sending failed. Message Body is empty !</font> </br>");
                return sb.ToString();
            }

            var fileName = "";
            if (attatchedfile != null)
            {
                if (attatchedfile.ContentLength > 0 && attatchedfile.ContentLength < 3100000)
                {
                    if (CheckUploadedFileType(attatchedfile))
                    {
                        //string getFileName = Path.GetFileName(attatchedfile.FileName);
                        //fileName = getFileName;
                        try
                        {
                            fileName = SaveAttatchments(attatchedfile);
                        }
                        catch (Exception)
                        {
                            sb.Append("<font color='#ff5d16'>Sending failed. Attatched file can not be saved.</font> </br>");
                            return sb.ToString();
                        }
                    }
                    else
                    {
                        sb.Append("<font color='#ff5d16'>Sending failed. Attatched File type not supported.</font> </br>");
                        return sb.ToString();
                    }
                }
                else
                {
                    sb.Append("<font color='#ff5d16'>Sending failed. Attatched file size should be less than 3 MB.</font> </br>");
                    return sb.ToString();
                }             
            }
           
            var findReceiverName = _accountManager.GetSingleAccount(message.ReceiverAccountId).Name;
            try
            {
                var ainternalMessage = new InternalMessage
                {
                    SenderAccountId = account.AccountId,
                    ReceiverAccountId = message.ReceiverAccountId,
                    MessageSubject = message.MessageSubject,
                    MessageBody = message.MessageBody,
                    MessageFlagId = 1,
                    SenderTime = DateTime.Now,
                    SenderDeleteStatus = false,
                    ReceiverDeleteStatus = false,
                    SenderIp = ClassifiedServices.GetUserIp(),
                    AttatchedFile = fileName
                };
                db.InternalMessages.Add(ainternalMessage);
                db.SaveChanges();
                sb.Append("<font color='#007b77'>Message send successfully to " + findReceiverName + "</font> </br>");

            }
            catch (DbException es)
            {
                sb.Append("<font color='#ff5d16'>Message sending failed!</font> </br>");
            }
            return sb.ToString();
        }

        public string SaveAttatchments(HttpPostedFileBase attatchedfile)
        {
            var getFileExtension = Path.GetExtension(attatchedfile.FileName);
            var getOriginalFileName = Path.GetFileNameWithoutExtension(attatchedfile.FileName);
            var getFileName = "";
            var fileNameFinal =getOriginalFileName+ "_"+getFileName + getFileExtension;
            var path = Path.Combine(Server.MapPath("~/Content/MessagingAttachments/"), fileNameFinal);
            attatchedfile.SaveAs(path);
            return fileNameFinal;
        }

        public ActionResult SaveHistoryOfMessage(HttpPostedFileBase file, int Sender, int Reciever, string title, string Message, int flag = 1, bool sDelStatus = false, bool rDelstatus = false)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin") return RedirectToAction("NotPermitted", "Home");
            if (file != null && file.ContentLength > 0)
            {
                var fileName = Path.GetFileName(file.FileName);
                //var ext = Path.GetExtension(fileName);
                //if (ext==".doc"||ext==".docx"||ext==".jpg"||ext==".jpeg"||ext==".ppt"||ext==".xls"||ext==".xlsx")
                //{

                //}
                var path = Path.Combine(Server.MapPath("~/Content/MessagingAttachments/"), fileName);
                file.SaveAs(path);

            }

            if (Sender > 0 && Reciever > 0 && file != null)
            {
                var fileName = Path.GetFileName(file.FileName);
                var ip = ClassifiedServices.GetUserIp();
                var mesSaver = new InternalMessage()
                {

                    SenderAccountId = Sender,
                    ReceiverAccountId = Reciever,
                    MessageSubject = title,
                    MessageBody = Message,
                    MessageFlagId = flag,
                    SenderTime = DateTime.Now,
                    SenderDeleteStatus = sDelStatus,
                    ReceiverDeleteStatus = rDelstatus,
                    SenderIp = ip,
                    AttatchedFile = fileName
                    //ReceiverIp = ,
                    //ReceiverAccount = ,


                };
                db.InternalMessages.Add(mesSaver);
                db.SaveChanges();

            }
            if (Sender > 0 && Reciever > 0)
            {
                var ip = ClassifiedServices.GetUserIp();
                var mesSaver = new InternalMessage()
                {
                    SenderAccountId = Sender,
                    ReceiverAccountId = Reciever,
                    MessageSubject = title,
                    MessageBody = Message,
                    MessageFlagId = flag,
                    SenderTime = DateTime.Now,
                    SenderDeleteStatus = sDelStatus,
                    ReceiverDeleteStatus = rDelstatus,
                    SenderIp = ip,
                    AttatchedFile = null
                    //ReceiverIp = ,
                    //ReceiverAccount = ,
                };
                db.InternalMessages.Add(mesSaver);
                db.SaveChanges();
            }
            ViewBag.msg = "Message Sent Successfully";
            var msg = ViewBag.msg;
            return RedirectToAction("Index", "Messaging", new { msg });
        }

        public int CountInboxUnreadMessages()
        {
            var account = (Account)Session["UserObj"];
            var countInboxMessages = _messageManager.GetReceivedMessageForSingleUser(account.AccountId).Count(s => s.MessageFlagId == 1);
            return countInboxMessages;
        }

        public int CountInboxMesssages()
        {
            var account = (Account)Session["UserObj"];
            var countInboxMessages = _messageManager.GetReceivedMessageForSingleUser(account.AccountId).Count();
            return countInboxMessages;
        }

        public int CountOutBoxMessages()
        {
            var account = (Account)Session["UserObj"];
            var countOutBoxMessages = _messageManager.GetSentMessagesForSingleUser(account.AccountId).Count();
            return countOutBoxMessages;
        }
        public PartialViewResult Inbox(int? page)
        {
            var account = (Account)Session["UserObj"];
            var receivedMsg = _messageManager.GetReceivedMessageForSingleUser(account.AccountId);
            var countUnread = receivedMsg.Count(s => s.MessageFlagId == 1);
            int pageSize;
            int pageNumber;
            if (receivedMsg.Any())
            {
                pageSize = 5;
                pageNumber = (page ?? 1);
            }
            else
            {
                pageSize = 1;
                pageNumber = (page ?? 1);
            }
            ViewBag.countInbox = receivedMsg.Count();
            ViewBag.CountUnread = countUnread;
            //ViewBag.accId = account.AccountId;
            return PartialView("_inboxPartial", receivedMsg.ToPagedList(pageNumber, pageSize));
        }

        public PartialViewResult Outbox(int? page)
        {
            var account = (Account)Session["UserObj"];
            var sentMessage = _messageManager.GetSentMessagesForSingleUser(account.AccountId);
            int pageSize;
            int pageNumber;
            if (sentMessage.Any())
            {
                pageSize = 5;
                pageNumber = (page ?? 1);
            }
            else
            {
                pageSize = 1;
                pageNumber = (page ?? 1);
            }
            ViewBag.CountOutBox = sentMessage.Count();
            //ViewBag.accId = account.AccountId;
            return PartialView("_outboxPartial", sentMessage.ToPagedList(pageNumber, pageSize));
        }

        public PartialViewResult GetMessageDetails(int acc)
        {
            var ip = ClassifiedServices.GetUserIp();
            var msg = db.InternalMessages.Find(acc);
            if (msg.MessageFlagId == 1 && msg.ReceiverViewTime == null && msg.ReceiverIp == null)
            {
                msg.ReceiverIp = ip;
                msg.MessageFlagId = 2;
                msg.ReceiverViewTime = DateTime.Now;

                db.Entry(msg).State = EntityState.Modified;
                db.SaveChanges();
            }



            ViewBag.interId = msg.InternalMessageId;
            ViewBag.senId = msg.SenderAccountId;
            ViewBag.recId = msg.ReceiverAccountId;
            return PartialView("_messageDetails", msg);
        }

        public PartialViewResult GetOwnMessageDetails(int acc)
        {
            var msg = db.InternalMessages.Find(acc);
            return PartialView("_ownMessageDetails", msg);
        }

        public ActionResult RepliedMessages(InternalMessage message, HttpPostedFileBase attatchedfile)
        {
            if (message.InternalMessageId > 0)
            {
                var account = (Account)Session["UserObj"];
                var msg = db.InternalMessages.Find(message.InternalMessageId);
                msg.OnreplyOfMessageId = message.InternalMessageId;
                db.Entry(msg).State = EntityState.Modified;
                var fileName = "";
                if (attatchedfile != null && attatchedfile.ContentLength > 0)
                {
                    if (CheckUploadedFileType(attatchedfile))
                    {
                        string getFileName = Path.GetFileName(attatchedfile.FileName);
                        fileName = getFileName;
                        var path = Path.Combine(Server.MapPath("~/Content/MessagingAttachments/"), fileName);
                        attatchedfile.SaveAs(path);
                    }

                }
                if (message.MessageBody != null)
                {
                    var ainternalMessage = new InternalMessage
                    {
                        SenderAccountId = account.AccountId,
                        ReceiverAccountId = message.SenderAccountId,
                        MessageSubject = message.MessageSubject,
                        MessageBody = message.MessageBody,
                        MessageFlagId = 1,
                        SenderTime = DateTime.Now,
                        SenderDeleteStatus = false,
                        ReceiverDeleteStatus = false,
                        SenderIp = ClassifiedServices.GetUserIp(),
                        AttatchedFile = fileName
                    };
                    db.InternalMessages.Add(ainternalMessage);
                    db.SaveChanges();
                    ViewBag.msg = "Message Sent Successfully";
                }

            }
            return RedirectToAction("Index", "Messaging");
        }

        public ActionResult SaveOnreply(HttpPostedFileBase file, int InternalId, int Sender, int Reciever, string title, string Message)
        {
            if (InternalId > 0)
            {
                var msg = db.InternalMessages.Find(InternalId);
                msg.OnreplyOfMessageId = InternalId;
                db.Entry(msg).State = EntityState.Modified;

                if (title != null || Message != null)
                {
                    var senId = Sender;
                    var recId = Reciever;
                    string title1 = title;
                    string message1 = Message;
                    SaveHistoryOfMessage(file, senId, recId, title1, message1);
                }

            }
            ViewBag.msg = "Message Sent Successfully";
            var msg1 = ViewBag.msg;
            return RedirectToAction("Inbox", "Messaging", new { msg1 });
        }

        public ActionResult DownloadFile(string file1)
        {
            if (Path.GetExtension(file1) != null)
            {
                string fullPath = Path.Combine(Server.MapPath("~/Content/MessagingAttachments/"), file1);
                return File(fullPath, "~/Content/MessagingAttachments/", file1);
            }
            return RedirectToAction("Inbox", "Messaging");
        }

        public string DeleteInboxMessage(int inMessId)
        {
            var msg = db.InternalMessages.Find(inMessId);
            msg.ReceiverDeleteStatus = true;
            db.Entry(msg).State = EntityState.Modified;
            db.SaveChanges();
            var sb = new StringBuilder();
            sb.Append("<font color='white'>Deleted Sucessfully</font>");
            return sb.ToString();
        }

        public string DeleteOutboxMessage(int inMessId)
        {
            var msg = db.InternalMessages.Find(inMessId);
            msg.SenderDeleteStatus = true;
            db.Entry(msg).State = EntityState.Modified;
            db.SaveChanges();
            var sb = new StringBuilder();
            sb.Append("<font color='white'>Deleted Sucessfully</font>");
            return sb.ToString();
        }
    }
}