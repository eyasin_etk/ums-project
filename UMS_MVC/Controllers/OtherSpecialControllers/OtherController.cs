﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO;

namespace UMS_MVC.Controllers.OtherSpecialControllers
{
   
    public class OtherController : Controller
    {
        private UmsDbContext db = new UmsDbContext();
        private readonly StudentManager _studentManager=new StudentManager();
        private  readonly  CourseForStudentManger _courseForStudentManger=new CourseForStudentManger();
        private readonly SemesterManager _semesterManager=new SemesterManager();
        private readonly Stopwatch _stopwatch=new Stopwatch();
        
        public ActionResult ModifyAdmin()
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin" ) return RedirectToAction("NotPermitted", "Home");
            var getActivesemester = _semesterManager.GetActiveSemester();
            var getAllNotSyncStudentList=_studentManager.GetAllStudentIdentifications().Count(s => s.StudentInfoId == null);
            var getAllFailedStudent = _courseForStudentManger.GetAllCourseForStudentsAcademics().Where(s => s.LetterGrade == "F");
            ViewBag.notSynclist = getAllNotSyncStudentList;
            ViewBag.totalFailed = getAllFailedStudent.Count();
            ViewBag.failedStudentAllSemester = getAllFailedStudent.Count(s=>s.CourseStatusId!=7);
            ViewBag.failedStudentCurrentSemester = getAllFailedStudent.Count(s => s.SemesterId == getActivesemester.SemesterId && s.CourseStatusId != 7);
            ViewBag.EmptyStudentSem = _studentManager.GetAllStudentIdentifications().Count(s => s.SemesterId == null);
            return View();
        }
        
        public string UpdateStudentSemesters()
        {
            int x = 0;
            try
            {
                _stopwatch.Reset();
                _stopwatch.Restart();
                var getAllEmptySemes = db.StudentIdentifications.Where(s => s.SemesterId == null);
                var getAllSemester = _semesterManager.GetAllSemester();
                
                foreach (var studentIdentification in getAllEmptySemes)
                {
                    var verifyStudents = getAllSemester.FirstOrDefault(s => s.SemesterNYear == studentIdentification.SemesterAndYear);
                    if (verifyStudents != null)
                        studentIdentification.SemesterId = verifyStudents.SemesterId;
                           // _semesterManager.GetSepecificSemester(studentIdentification.SemesterAndYear).SemesterId;
                    db.Entry(studentIdentification).State = EntityState.Modified;
                    x ++;
                }
                db.SaveChanges();
                _stopwatch.Stop();
            }
            catch (Exception)
            {

                return "Student Update Failed !!";
            }

            return x + "Students Status updated. Time taken " + _stopwatch.Elapsed.ToString("mm\\:ss\\.ff") + " (min:sec.msec)";
        }

        public string UpdateStudentsIdentity(bool notfy = false)
        {
            string msg = "You are not Admin !";
            if (Session["UserObj"] != null)
            {
                var account = (Account) Session["UserObj"];
                if (account.AccountsRole.AccountType == "Admin" && notfy)
                {
                    int count = 0;
                    try
                    {
                        _stopwatch.Reset();
                        _stopwatch.Restart();
                        db.Configuration.AutoDetectChangesEnabled = false;
                        var getAllstudentId = db.StudentIdentifications.Where(s => s.StudentInfoId == null);
                        var getAllStudentInfo = db.StudentInfos;
                        if (getAllstudentId.Any())
                        {
                            foreach (var studentInfo in getAllStudentInfo)
                            {
                                foreach (var studentIdentification in getAllstudentId)
                                {
                                    if (studentInfo.StudentId == studentIdentification.StudentId)
                                    {
                                        studentIdentification.StudentInfoId = studentInfo.StudentInfoId;
                                        db.Entry(studentIdentification).State = EntityState.Modified;
                                        count++;
                                    }
                                }
                            }
                            db.SaveChanges();
                            _stopwatch.Stop();
                            msg = count + " Student Identification Updated & Time taken " +
                                  _stopwatch.Elapsed.ToString("mm\\:ss\\.ff") + " (min:sec.msec)";
                        }
                    }
                    catch (Exception)
                    {
                        msg = "Something is wrong !!";
                    }
                    finally
                    {
                        db.Configuration.AutoDetectChangesEnabled = true;
                    }
                }
                
            }
            //ViewBag.MSg = msg;
            return msg;
        }
        
        public ActionResult UpdateRegistrations(bool notfy = false)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin") return RedirectToAction("NotPermitted", "Home"); 

            if (!notfy) return RedirectToAction("ModifyAdmin");

            try
            {
                _stopwatch.Reset();
                _stopwatch.Restart();
                int count = 0;
                var getAllregistrations = db.PaymentRegistrations.Where(s => s.StudentIdentificationId == null);
                var getAllStudentIds = db.StudentIdentifications;
                foreach (var studentIdentification in getAllStudentIds)
                {
                    foreach (var paymentRegistration in getAllregistrations)
                    {
                        if (studentIdentification.StudentId != paymentRegistration.RegistrationFor) continue;
                        paymentRegistration.StudentIdentificationId = studentIdentification.StudentIdentificationId;
                        db.Entry(paymentRegistration).State = EntityState.Modified;
                        
                        count++;
                    }
                }
                _stopwatch.Stop();
                db.SaveChanges();
                TempData["msg"] = count + " registration Updated.Time taken " + _stopwatch.Elapsed.ToString("mm\\:ss\\.ff") + " (min:sec.msec)";
            }
            catch (Exception)
            {

                TempData["msg"] = "Something error.";
            }
            return RedirectToAction("ModifyAdmin");
        }

        public static int CheckAccount(string role1)
        {
            if (!string.IsNullOrEmpty(role1))
            {
                
                switch (role1)
                {
                    case "Admin":
                        return 2;
                    case "SuperAdmin":
                       
                        return 2;
                    case "Admission":
                        
                        return 3;
                    case "Accounts":
                       
                        return 4;
                    case "Teacher":
                        
                        return 5;
                    case "Examination":
                        
                        return 6;
                    case "Advising":
                       
                        return 7;
                }
            }
            return 0;
        }

        public static string LayoutManager(string role)
        {
            string layout;
                switch (role)
                {
                    case "Admin":
                        layout = "~/Views/Shared/_LayoutAdmin.cshtml";
                        break;
                    case "SuperAdmin":
                        layout = "~/Views/Shared/_LayoutAdmin.cshtml";
                        break;
                    case "Admission":
                        layout = "~/Views/Shared/_LayoutAdmission.cshtml";
                        break;
                    case "Accounts Officer":
                        layout = "~/Views/Shared/_LayoutAccountsOfficer.cshtml";
                        break;
                    case "Teacher":
                        layout = "~/Views/Shared/_TeacherLayout.cshtml";
                        break;
                    case "Examination":
                        layout = "~/Views/Shared/_LayoutAdmin.cshtml";
                        break;
                    case "Advisor":
                        layout = "~/Views/Shared/_LayoutCourseRegistration.cshtml";
                        break;
                    case "Ps":
                        layout = "~/Views/Shared/_LayoutPSs.cshtml";
                        break;
                    case "Coordinator":
                        layout = "~/Views/Shared/_LayoutCoordinator.cshtml";
                        break;
                    case "Exam":
                        layout = "~/Views/Shared/_LayoutExamSection.cshtml";
                        break;
                    case "ExamCtrl":
                        layout = "~/Views/Shared/_LayoutExamCtrl.cshtml";
                        break;
                    case "Support":
                        layout = "~/Views/Shared/_LayoutSystemSupport.cshtml";
                        break;
                    default:
                        layout = "~/Views/Shared/_Layout.cshtml";
                        break;
                }

            return  layout;
        }

        public IEnumerable<Teacher> GetTeachers()
        {
            return db.Teachers;
        }
        
        public CourseForStudentsAcademic CalculateGrade(CourseForStudentsAcademic courseForStudentsAcademic)
        {
            double calculateTotal;
            switch (courseForStudentsAcademic.CourseForDepartment.CourseType.ToUpper())
            {
                case "LAB":
                    calculateTotal = LabActualMark(courseForStudentsAcademic);
                    break;
                case "OTHER":
                    calculateTotal = OtherActualMark(courseForStudentsAcademic);
                    break;
                default:
                    calculateTotal = ActualMark(courseForStudentsAcademic);
                    break;
            }
            calculateTotal = Math.Round(calculateTotal, 0,MidpointRounding.AwayFromZero);
            double grade = 0;
            string gradeLetter = string.Empty;
            if (calculateTotal >= 0 && calculateTotal < 40)
            {
                grade = 0.0;
                gradeLetter = "F";
            }
            else if (calculateTotal >=40  && calculateTotal < 45)
            {
                grade = 2.0;
                gradeLetter = "D";
            }
            else if (calculateTotal >= 45 && calculateTotal < 50)
            {
                grade = 2.25;
                gradeLetter = "C";
            }
            else if (calculateTotal >= 50 && calculateTotal <55)
            {
                grade = 2.5;
                gradeLetter = "C+";
            }
            else if (calculateTotal >= 55 && calculateTotal <60)
            {
                grade = 2.75;
                gradeLetter = "B-";
            }
            else if (calculateTotal >= 60 && calculateTotal <65)
            {
                grade = 3.0;
                gradeLetter = "B";
            }
            else if (calculateTotal >= 65 && calculateTotal <70)
            {
                grade = 3.25;
                gradeLetter = "B+";
            }
            else if (calculateTotal >= 70 && calculateTotal < 75)
            {
                grade = 3.5;
                gradeLetter = "A-";
            }
            else if (calculateTotal >= 75 && calculateTotal < 80)
            {
                grade = 3.75;
                gradeLetter = "A";
            }
            else if (calculateTotal >= 80 )
            {
                grade = 4.0;
                gradeLetter = "A+";
            }
           
            var getCourseInfo = db.CourseForDepartments.Find(courseForStudentsAcademic.CourseForDepartmentId).Credit;
            courseForStudentsAcademic.TotalMark = calculateTotal;
            courseForStudentsAcademic.Grade = grade;
            courseForStudentsAcademic.LetterGrade=gradeLetter;
            courseForStudentsAcademic.TotalGrade = grade*getCourseInfo;
            //Course Status Changed
            courseForStudentsAcademic.CourseStatusId = 2;
            var countSameCourses =
                _courseForStudentManger.GetAllCourseByStudent(courseForStudentsAcademic.StudentIdentificationId)
                    .Count(s => s.CourseForDepartmentId == courseForStudentsAcademic.CourseForDepartmentId && s.LetterGrade=="F");
            if(countSameCourses>0 && grade>0)
            {
                ChangeAllFailedCoursesStatusToNonCredit(courseForStudentsAcademic);
            }
            return courseForStudentsAcademic;
        }
        public void ChangeAllFailedCoursesStatusToNonCredit(CourseForStudentsAcademic cFSa)
        {
            var getAllSamecoursesofThisStudent =
                db.CourseForStudentsAcademics.Where(
                    s =>
                        s.StudentIdentificationId == cFSa.StudentIdentificationId &&
                        s.CourseForDepartmentId == cFSa.CourseForDepartmentId &&
                        s.CourseForStudentsAcademicId != cFSa.CourseForStudentsAcademicId 
                        && s.CourseStatusId==5 && s.LetterGrade=="F");
            foreach (var courseForStudentsAcademic in getAllSamecoursesofThisStudent)
            {
                courseForStudentsAcademic.CourseStatusId =7;
                db.Entry(courseForStudentsAcademic).State = EntityState.Modified;
            }
            db.SaveChanges();
        }
       
        public double ActualMark(CourseForStudentsAcademic courseForStudentsAcademic)
        {
            double att = 0;
            double classTest = 0;
            double mid = 0;
            double final = 0;
            if (courseForStudentsAcademic.Attendance>0)
            {
                att = (courseForStudentsAcademic.Attendance/10)*5;
            }
            if (courseForStudentsAcademic.ClassTest > 0)
            {
                classTest = (courseForStudentsAcademic.ClassTest/50)*25;
            }
            if (courseForStudentsAcademic.Midterm > 0)
            {
                mid = (courseForStudentsAcademic.Midterm/100)*20;
            }
            if (courseForStudentsAcademic.FinalTerm > 0)
            {
                final = (courseForStudentsAcademic.FinalTerm/100)*50;
            }
            return att + classTest + mid + final;
        }

        public double LabActualMark(CourseForStudentsAcademic courseForStudentsAcademic)
        {
            double mid = 0;
            double final = 0;
            
            if (courseForStudentsAcademic.Midterm > 0)
            {
                mid = (courseForStudentsAcademic.Midterm / 100) * 40;
            }
            if (courseForStudentsAcademic.FinalTerm > 0)
            {
                final = (courseForStudentsAcademic.FinalTerm / 100) * 60;
            }
            return mid + final;
        }

        public double OtherActualMark(CourseForStudentsAcademic courseForStudentsAcademic)
        {
            double final = 0;
           
            if (courseForStudentsAcademic.FinalTerm > 0)
            {
                final = courseForStudentsAcademic.FinalTerm;
            }
            return final;
        }

        //public PartialViewResult GetAllFailedStudents()
        //{
        //    var getAllFailedStudent = _courseForStudentManger.GetAllCourseForStudentsAcademics().Where(s => s.LetterGrade == "F");

        //}
        public string  UpdateFailedStatus(string sem)
        {
            var getActiveSemester = _semesterManager.GetActiveSemester();
            var getAllFailedStudent = db.CourseForStudentsAcademics.Where(s => s.LetterGrade == "F");
            int count=0;
            if (sem == "Cur")
            {
                getAllFailedStudent = getAllFailedStudent.Where(s => s.SemesterId == getActiveSemester.SemesterId);
                if (getAllFailedStudent.Any())
                {
                    foreach (var courseForStudentsAcademic in getAllFailedStudent)
                    {
                        courseForStudentsAcademic.CourseStatusId = 7;
                        db.Entry(courseForStudentsAcademic).State = EntityState.Modified;
                        count++;
                    }
                    db.SaveChanges();
                }
            }
            if (sem == "All" && getAllFailedStudent.Any())
            {
                foreach (var courseForStudentsAcademic in getAllFailedStudent)
                {
                    courseForStudentsAcademic.CourseStatusId = 7;
                    db.Entry(courseForStudentsAcademic).State = EntityState.Modified;
                    
                    count++;
                }
                db.SaveChanges();
            }
           return count + " Course status Updated";
        }

        
    }
}