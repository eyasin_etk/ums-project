﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.Controllers.OtherSpecialControllers
{
    public class UnfairStudentsController : Controller
    {
        private UmsDbContext db = new UmsDbContext();

        // GET: UnfairStudents
        public ActionResult Index()
        {

            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];

            if (account.AccountsRoleId == 1)

               return View(db.UnfairStudents.ToList());


            else if (account.AccountsRoleId == 12)
            {
                var exmview = db.UnfairStudents.Where(a => a.type == 1).ToList();
                return View(exmview);
            }


            else
            {
                var exmview = db.UnfairStudents.Where(a => a.type == 2).ToList();
                return View(exmview);
            }
        }

        // GET: UnfairStudents/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UnfairStudents unfairStudents = db.UnfairStudents.Find(id);
            if (unfairStudents == null)
            {
                return HttpNotFound();
            }
            return View(unfairStudents);
        }

        // GET: UnfairStudents/Create
        public ActionResult Create(string msg)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];

            if (account.AccountsRoleId!=1 && account.AccountsRoleId!=12 && account.AccountsRoleId!=4)
                //|| account.AccountsRoleId != 4 || account.AccountsRoleId != 12)
                return RedirectToAction("NotPermitted", "Home");
            ViewBag.ErrorMessage = msg;
            var activesem = db.Semesters.Where(a => a.ActiveSemester == true).Select(p => p.SemesterId).FirstOrDefault();
            ViewBag.SemesterId = new SelectList(db.Semesters.ToList(), "SemesterId", "SemesterNYear",selectedValue:activesem);
            return View();
        }

        // POST: UnfairStudents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "StudentUnfairId,StudentId,SemesterId,unfairdetails,type")] UnfairStudents unfairStudents)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];

            if (account.AccountsRoleId != 1 && account.AccountsRoleId != 12 && account.AccountsRoleId != 4) return RedirectToAction("NotPermitted", "Home");
            var message1 = "";
           // if (ModelState.IsValid)
          //  {

                //unfairStudents.SemesterId = db.Semesters.Where(a => a.ActiveSemester == true).Select(a => a.SemesterId).FirstOrDefault();
                var check = db.StudentIdentifications.Where(a => a.StudentId == unfairStudents.StudentId).Count();
            var identyID = db.StudentIdentifications.Where(b => b.StudentId == unfairStudents.StudentId).Select(a => a.StudentIdentificationId).FirstOrDefault();

            var semesterNyear = db.Semesters.Where(a => a.SemesterId == unfairStudents.SemesterId).Select(b => b.SemesterNYear).FirstOrDefault();

            if (check == 0)
            {
                message1 = "Student ID is not Exists";
                return RedirectToAction("Create/", new { msg = message1 });
            }

            else
             {
                StudentIdentification stu = db.StudentIdentifications.Find(identyID);
                stu.BlockStudent = true;
                if (stu.BlockReason ==null)
                {
                    stu.BlockReason =unfairStudents.unfairdetails+"in"+ semesterNyear;
                }
                else
                {
                    stu.BlockReason = stu.BlockReason + "," + unfairStudents.unfairdetails + "in" + semesterNyear;
                }

                //db.StudentIdentifications.Add(stu); 

                    if (account.AccountsRoleId == 12)
                      {
                       unfairStudents.type = 1;
                      }

                    else
                     {
                       unfairStudents.type = 2;
                     }
                db.UnfairStudents.Add(unfairStudents);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
           // }

           // return View(unfairStudents);
        }

        // GET: UnfairStudents/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UnfairStudents unfairStudents = db.UnfairStudents.Find(id);
            if (unfairStudents == null)
            {
                return HttpNotFound();
            }


            ViewBag.SemesterId = new SelectList(db.Semesters.ToList(), "SemesterId", "SemesterNYear", unfairStudents.SemesterId);
            return View(unfairStudents);
        }

        // POST: UnfairStudents/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "StudentUnfairId,StudentId,SemesterId,unfairdetails")] UnfairStudents unfairStudents)
        {



            var check = db.StudentIdentifications.Where(a => a.StudentId == unfairStudents.StudentId).Count();
            var identyID = db.StudentIdentifications.Where(b => b.StudentId == unfairStudents.StudentId).Select(a => a.StudentIdentificationId).FirstOrDefault();
            //if (check == 0)
            //{
            //    message1 = "Student ID is not Exists";
            //    return RedirectToAction("Create/", new { msg = message1 });
            //}
           // else
          //  {
                StudentIdentification stu = db.StudentIdentifications.Find(identyID);
                stu.BlockStudent = true;

           

                if (stu.BlockReason == null)
                {
                    stu.BlockReason = unfairStudents.unfairdetails;
                }
                else
                {
                   var count = stu.BlockReason.LastIndexOf(',');
                if (count > 0) { 
                   var t = stu.BlockReason.Substring(0, count);

                     stu.BlockReason = t + "," + unfairStudents.unfairdetails;
                     }

                else
                {
                    stu.BlockReason = unfairStudents.unfairdetails;
                }
            }

            //db.StudentIdentifications.Add(stu); 
            // db.UnfairStudents.Add(unfairStudents);
            db.Entry(unfairStudents).State = EntityState.Modified;
            db.Entry(stu).State = EntityState.Modified;
            db.SaveChanges();
                return RedirectToAction("Index");
          //  }



            //if (ModelState.IsValid)
            //{
            //    db.Entry(unfairStudents).State = EntityState.Modified;
            //    db.SaveChanges();
            //    return RedirectToAction("Index");
            //}
            //return View(unfairStudents);
        }

        // GET: UnfairStudents/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UnfairStudents unfairStudents = db.UnfairStudents.Find(id);
            if (unfairStudents == null)
            {
                return HttpNotFound();
            }
            return View(unfairStudents);
        }

        // POST: UnfairStudents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UnfairStudents unfairStudents = db.UnfairStudents.Find(id);
            db.UnfairStudents.Remove(unfairStudents);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
