﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.BLL;
using UMS_MVC.Manager.DAL.DAO;

namespace UMS_MVC.Controllers.AdminMatter
{
    public class AdminController : Controller
    {
        private UmsDbContext db;
        private readonly Stopwatch _stopwatch = new Stopwatch();
        private readonly SemesterManager _semesterManager;
        private readonly AccountManager _accountManager;
        private readonly CourseForDepartmentManager _courseForDepartmentManager;
        private readonly CommonAttributeManager _commonAttributeManager;
        private readonly GradeManager _gradeManager;
        private readonly CourseForStudentManger _courseForStudentManger;
        private readonly UserActivityManager _userActivityManager;
        private readonly DepartmentManager _departmentManager;
        private readonly StudentManager _studentManager;
        private readonly LoginHistoryManager _historyManager;
        private readonly PowerWorkManager _powerWorkManager;


        public AdminController()
        {
            db = new UmsDbContext();
            _semesterManager = new SemesterManager();
            _accountManager = new AccountManager();
            _courseForDepartmentManager = new CourseForDepartmentManager();
            _commonAttributeManager = new CommonAttributeManager();
            _gradeManager = new GradeManager();
            _courseForStudentManger = new CourseForStudentManger();
            _userActivityManager = new UserActivityManager();
            _departmentManager = new DepartmentManager();
            _studentManager = new StudentManager();
            _historyManager = new LoginHistoryManager();
            _powerWorkManager = new PowerWorkManager();
        }

        public ActionResult AdminMain()
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin") return RedirectToAction("NotPermitted", "Home");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult TermController(string termValue, DateTime? expireDate)
        {
            if (Session["UserObj"] == null) return new JsonResult() { Data = "U are not logged In.", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" || account.AccountId != 1) return new JsonResult() { Data = "Not Allowed !", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            string msgFromServer = "Term activation Failed";
            var getActiveSemester = db.Semesters.FirstOrDefault(s => s.ActiveSemester);
            if (!string.IsNullOrEmpty(termValue) && getActiveSemester != null)
            {
                string msg = string.Empty;
                switch (termValue)
                {
                    case "mid":
                        msg = "Mid term Activated";
                        getActiveSemester.MidTerm = true;
                        getActiveSemester.FinalTerm = false;
                        break;
                    case "final":
                        msg = "Final term Activated";
                        getActiveSemester.MidTerm = false;
                        getActiveSemester.FinalTerm = true;
                        break;
                    case "diactive":
                        msg = "Grade upload disabled";
                        getActiveSemester.MidTerm = false;
                        getActiveSemester.FinalTerm = false;
                        break;
                }
                getActiveSemester.SpecialGradeuploadDeadLine = expireDate;
                db.Entry(getActiveSemester).State = EntityState.Modified;
                db.SaveChanges();
                msgFromServer = msg;
                //TempData["msgmsgfromtermController"] = msg;

            }
            return new JsonResult { Data = msgFromServer, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ChangeSemester(int semesterId = 0)
        {
            if (Session["UserObj"] == null) return new JsonResult() { Data = "U are not logged In.", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" || account.AccountId != 1) return new JsonResult() { Data = "Not Allowed !", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            string msgFromServer = "Semester Cann't be Activated.";
            if (semesterId > 0)
            {
                //var semester = _semesterManager.GetSingleSemesterFiltered(semesterId);
                //semester.ActiveSemester = true;
                //bool updateSemester = _semesterManager.UpdateSemester(semester);
                foreach (var semester1 in _semesterManager.GetAllSemesterFiltered().ToList())
                {
                    semester1.MidTerm = false;
                    semester1.FinalTerm = false;
                    semester1.SpecialGradeuploadDeadLine = null;
                    if (semester1.SemesterId != semesterId)
                    {
                        semester1.ActiveSemester = false;
                    }
                    else
                    {
                        semester1.ActiveSemester = true;
                       msgFromServer=semester1.SemesterNYear+ " activated successfully.";
                    }
                    _semesterManager.UpdateSemester(semester1);
                    
                }
                _semesterManager.SaveAll();
            }
            return new JsonResult { Data = msgFromServer, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult ChangeCourseAdvising(string adviceValue)
        {
            if (Session["UserObj"] == null) return new JsonResult() { Data = "U are not logged In.", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" || account.AccountId != 1) return new JsonResult() { Data = "Not Allowed !", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            string message = "Advising operation Failed !!";
            var getActiveSemester = _semesterManager.GetActiveSemester();
            if (!string.IsNullOrEmpty(adviceValue))
            {
                if (adviceValue == "active")
                {
                    CourseAdvisingOperation(getActiveSemester.SemesterId);
                    message = "Advising Activated";
                }
                else
                {
                    CourAdvisingDisable();
                    message = "Advising Disabled";
                }

            }
            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ValidateAllCoursesInCurrentSemester(int sectorId = 0)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" || account.AccountId != 1) return RedirectToAction("NotPermitted", "Home");
            var getCurrentSemseter = _semesterManager.GetActiveSemester();
            if (sectorId > 0)
            {
                var getAllCourseforStudent = db.CourseForStudentsAcademics.Where(s => s.CourseStatusId == 1);
                if (sectorId == 2)
                {
                    getAllCourseforStudent =
                        getAllCourseforStudent.Where(s => s.SemesterId == getCurrentSemseter.SemesterId);
                }
                if (sectorId == 3)
                {
                    getAllCourseforStudent =
                        getAllCourseforStudent.Where(s => s.SemesterId != getCurrentSemseter.SemesterId);
                }
                TempData["modalMsg"] = "Successfully Updated " + getAllCourseforStudent.DistinctBy(s => s.CourseForStudentsAcademicId).Count() + " Students course Status";
                if (getAllCourseforStudent.Any())
                {
                    foreach (var courseForStudentsAcademic in getAllCourseforStudent)
                    {
                        courseForStudentsAcademic.CourseStatusId = 2;
                        db.Entry(courseForStudentsAcademic).State = EntityState.Modified;
                    }
                    db.SaveChanges();
                }

            }
            return RedirectToAction("Index", "Home");
        }
        public ActionResult AccountListWithPic(string acc)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Support") return RedirectToAction("NotPermitted", "Home");
            if (!string.IsNullOrEmpty(acc))
            {
                var vUser = _accountManager.GetAllAccounts().Where(s => s.LoginIdentity.Contains(acc));
                return View(vUser);
            }
            if (TempData["AccountId"] != null)
            {
                int x = (int)TempData["AccountId"];
                return View(_accountManager.GetAllAccounts().Where(s => s.AccountId == x));
            }
            var nVUser = _accountManager.GetAllAccounts();
            return View(nVUser);
        }
        public ActionResult UserDetails(int acc)
        {
            if (acc > 0)
            {
                var vUser = _accountManager.GetSingleAccount(acc);

                return PartialView("~/Views/Admin/_detailsOfUser.cshtml", vUser);
            }
            return RedirectToAction("AccountListWithPic", "Admin");
        }

        public ActionResult MasterUploadOfPicture(HttpPostedFileBase file, int accId)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Support") return RedirectToAction("NotPermitted", "Home");
            //if (Session["RequestUrl"] != null)
            //{
            //    return Redirect(Session["RequestUrl"].ToString());
            //}


            if (file != null && CheckImageType(file))
            {
                var searchAccountImage = db.Accounts.Find(accId);
                string picture = searchAccountImage.LoginIdentity;
                var findImage = (db.Accounts.Where(s => s.Pic == picture + ".jpg")).Count();
                if (findImage == 0)
                {
                    ResizeImageAndSave(file, picture);
                    searchAccountImage.Pic = picture + ".jpg";
                    db.Entry(searchAccountImage).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    ResizeImageAndSave(file, picture);
                }

                TempData["AccountId"] = accId;
                return RedirectToAction("AccountListWithPic", "Admin");
            }
            return RedirectToAction("AccountListWithPic", "Admin");
        }


        public bool CheckImageType(HttpPostedFileBase file)
        {
            string extension = Path.GetExtension(file.FileName);
            if (extension == ".jpg" || extension == ".JPG")
            {
                return true;
            }
            return false;
        }
        public void ResizeImageAndSave(HttpPostedFileBase file, string accId)
        {
            var img = new WebImage(file.InputStream);
            if (img.Height > 450)
                img.Resize(350, 450);
            string imageName = accId + ".jpg";
            string physicalPath = FileCheckingManager.AccountImageFileLocation() + imageName;
            img.Save(physicalPath);
        }

        public ActionResult DeletePic(int accId)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" || account.AccountId != 1) return RedirectToAction("NotPermitted", "Home");
            var personDetais = db.Accounts.Find(accId);
            string logId = personDetais.LoginIdentity;
            string imageName = logId + ".jpg";
            string path = Server.MapPath("~/Image/AccountPic/" + imageName);
            if (System.IO.File.Exists(path))
            {
                Account personsPic = db.Accounts.Find(accId);
                personsPic.Pic = null;
                db.SaveChanges();
                System.IO.File.Delete(path);
            }
            TempData["AccountId"] = accId;
            return RedirectToAction("AccountListWithPic", "Admin");
        }

        public ActionResult ChangeStudentPic()
        {
            return View();
        }

        public ActionResult CourseManagementAdmin(string searchId)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin") return RedirectToAction("NotPermitted", "Home");
            ViewBag.courseTypes = new SelectList(_commonAttributeManager.GetAllGradingSystems(), "GradingSystemId", "GradingSystemName");
            ViewBag.CourseForDepartmentId = new SelectList(_courseForDepartmentManager.GetAllCourseForDepartmentsByDepartment(0), "CourseForDepartmentId", "CourseName");
            ViewBag.CourseStatusId = new SelectList(_commonAttributeManager.GetallCourseStatuses(), "CourseStatusId", "Status");
            ViewBag.SemesterId = new SelectList(_semesterManager.GetAllSemester(), "SemesterId", "SemesterNYear");
            TempData["buttonStatus"] = false;

            if (!string.IsNullOrEmpty(searchId))
            {
                var findStudent = db.StudentIdentifications.FirstOrDefault(s => s.StudentId == searchId);
                if (findStudent != null)
                {
                    ViewBag.studentObj = findStudent;
                    var sortCourseForStudent = _courseForDepartmentManager.GetAllCourseForDepartmentsWithCode(findStudent.DepartmentId);
                    ViewBag.CourseForDepartmentId = new SelectList(sortCourseForStudent, "CourseForDepartmentId", "CourseName", "SerializedSemester.SemesterName", 0);
                }

                TempData["buttonStatus"] = true;
            }
            var demoCourse = new CourseForStudentsAcademic
            {
                Attendance = 0,
                ClassTest = 0,
                CourseStatusId = 0,
                FinalTerm = 0,

            };
            return View(demoCourse);

        }

        //[HttpPost]
        //public ActionResult CourseManagementAdmin(string searchId)
        //{
        //    ViewBag.courseTypes = new SelectList(_commonAttributeManager.GetAllCourseStatuses(), "Status", "Status");
        //    ViewBag.CourseForDepartmentId = new SelectList(_courseForDepartmentManager.GetAllCourseForDepartmentsByDepartment(0), "CourseForDepartmentId", "CourseName");
        //    ViewBag.CourseStatusId = new SelectList(_commonAttributeManager.GetallCourseStatuses(), "CourseStatusId", "Status");
        //    ViewBag.SemesterId = new SelectList(_semesterManager.GetAllSemester(), "SemesterId", "SemesterNYear");
        //    var findStudent = db.StudentIdentifications.FirstOrDefault(s => s.StudentId == searchId);
        //    if (findStudent != null)
        //    {
        //        ViewBag.studentObj = findStudent;
        //        var sortCourseForStudent = _courseForDepartmentManager.GetAllCourseForDepartmentsByDepartment(findStudent.DepartmentId);
        //        ViewBag.CourseForDepartmentId = new SelectList(sortCourseForStudent, "CourseForDepartmentId", "CourseName", "SerializedSemester.SemesterName",0);
        //    }
        //    var demoCourse = new CourseForStudentsAcademic
        //    {
        //        Attendance = 0,
        //        ClassTest = 0,
        //        CourseStatusId = 0,
        //        FinalTerm = 0,

        //    };
        //    TempData["buttonStatus"] = true;



        //    return View(demoCourse);
        //}

        public string CheckCourseType(int courseId, int studentIdentityId)
        {
            var sb = new StringBuilder();
            var getType = _courseForDepartmentManager.GetSingleCourseForDepartment(courseId).CourseType;
            sb.Append("Course Type : <font color='red'><b>" + getType + "<b></font> </br>");
            if (studentIdentityId > 0)
            {
                var getTakenCourses = _courseForStudentManger.GetAllCourseByStudent(studentIdentityId).Count(s => s.CourseForDepartmentId == courseId);
                if (getTakenCourses == 0)
                {
                    sb.Append("Course not <font color='green'><b>Taken<b></font>");
                }
                if (getTakenCourses == 1)
                {
                    sb.Append("Course is already <font color='orange'><b>Taken<b></font>");
                }
                if (getTakenCourses > 1)
                {
                    sb.Append("Course is <font color='red'><b>Taken " + getTakenCourses + " <b></font>times");
                }

            }
            return sb.ToString();

        }

        public PartialViewResult GeneratePartialforCourseType(int courseId)
        {

            var getType = _courseForDepartmentManager.GetSingleCourseForDepartment(courseId).CourseType;
            ViewBag.courseType = getType;
            return PartialView("_CoreGradeUpload");
            //return PartialView("_TestPartialView");
        }

        public PartialViewResult CalculateForFinalResultUpdated(int courseId, double att, double ct, double mid,
            double finall, int gradingCode = 1)
        {
            if (gradingCode > 0)
            {
                var getGradingSystem = _commonAttributeManager.GetSpecificGradingSystem(gradingCode);
                if (getGradingSystem.Attendance >= att && getGradingSystem.ClassTest >= ct &&
                    getGradingSystem.MidTerm >= mid && getGradingSystem.FinalTerm >= finall)
                {
                    var gradeObj = new CourseForStudentsAcademic
                    {
                        Attendance = att,
                        ClassTest = ct,
                        Midterm = mid,
                        FinalTerm = finall,
                        CourseForDepartmentId = courseId
                    };
                    var getCalculatedGrade = _gradeManager.CalculateGradeUpdated(gradeObj, gradingCode);
                    return PartialView("_LabGradeUpload", getCalculatedGrade);
                }
                ViewBag.msg = "Insert marks in range !!";
            }
            return PartialView("_LabGradeUpload", new CourseForStudentsAcademic());
        }

        public PartialViewResult CalculateForFinalGrade(int courseId, string courseType, double att, double ct, double mid, double finall)
        {
            if (!string.IsNullOrEmpty(courseType))
            {
                var gradeObj = new CourseForStudentsAcademic
                {
                    Attendance = att,
                    ClassTest = ct,
                    Midterm = mid,
                    FinalTerm = finall,
                    CourseForDepartmentId = courseId
                };
                var getCalculatedGrade = _gradeManager.CalculateGrade(gradeObj, courseType);
                return PartialView("_LabGradeUpload", getCalculatedGrade);
            }
            return PartialView("_LabGradeUpload", new CourseForStudentsAcademic());
        }

        public string TellMeDate(int sasd)
        {
            return DateTime.Now.ToString();
        }
        public PartialViewResult GetCoursesOfStudent(int studentIdentityId)
        {
            //var findStudent = _studentManager.GetSingleStudentByStudentId(studentIdentityId);
            var getAllCourses =
               _courseForStudentManger.GetAllCourseByStudent(studentIdentityId);
            if (getAllCourses.Any())
            {
                return PartialView("_courseListBySemester", getAllCourses.ToList());
            }
            return PartialView("_courseListBySemester", new List<CourseForStudentsAcademic>());
        }

        public PartialViewResult LoadGradingInfoes(int gradingId)
        {
            var getGradingnfo = _commonAttributeManager.GetSpecificGradingSystem(gradingId);
            return PartialView("_GradingInfoPartial", getGradingnfo);
        }

        [HttpPost]
        public PartialViewResult CourseAddings2(CourseForStudentsAcademic courseForStudentsAcademic, int gradingCode = 1, bool emptyGrade = false)
        {
            UpdateCourseManageMentCourseOptimized(courseForStudentsAcademic, true, gradingCode, emptyGrade);
            //if (courseForStudentsAcademic.CourseStatusId == 8)
            //{
            //    courseForStudentsAcademic.AddedDate = DateTime.Now.Date;
            //    courseForStudentsAcademic.CourseStatusId = 8;
            //    courseForStudentsAcademic.LetterGrade = "I";
            //    courseForStudentsAcademic.Grade = 0;
            //    courseForStudentsAcademic.TotalGrade = 0;
            //    courseForStudentsAcademic.TotalMark = 0;
            //    db.CourseForStudentsAcademics.Add(courseForStudentsAcademic);
            //}
            //if (courseForStudentsAcademic.CourseStatusId == 4)
            //{
            //    courseForStudentsAcademic.AddedDate = DateTime.Now.Date;
            //    //courseForStudentsAcademic.CourseStatusId = 4;
            //    courseForStudentsAcademic.LetterGrade = "F";
            //    courseForStudentsAcademic.Grade = 0;
            //    courseForStudentsAcademic.TotalGrade = 0;
            //    courseForStudentsAcademic.TotalMark = 0;
            //    courseForStudentsAcademic.SectionId = null;
            //    db.CourseForStudentsAcademics.Add(courseForStudentsAcademic);
            //}  
            //else
            //{
            //    if (emptyGrade)
            //    {
            //        courseForStudentsAcademic.LetterGrade = "";
            //        courseForStudentsAcademic.Grade = 0;
            //        courseForStudentsAcademic.TotalGrade = 0;
            //        courseForStudentsAcademic.AddedDate = DateTime.Now.Date;
            //        db.CourseForStudentsAcademics.Add(courseForStudentsAcademic);
            //    }
            //    else
            //    {
            //        var calCulatedGrade = _gradeManager.CalculateGradeUpdated(courseForStudentsAcademic, gradingCode);
            //        calCulatedGrade.AddedDate = DateTime.Now.Date;
            //        db.CourseForStudentsAcademics.Add(calCulatedGrade);
            //    }
            //}

            //db.SaveChanges();
            var getAllCourses =
                _courseForStudentManger.GetAllCourseByStudent(courseForStudentsAcademic.StudentIdentificationId);
            if (getAllCourses.Any())
            {
                return PartialView("_courseListBySemester", getAllCourses.ToList());
            }
            return PartialView("_courseListBySemester", new List<CourseForStudentsAcademic>());
        }

        [HttpPost]
        public PartialViewResult CourseAddings(CourseForStudentsAcademic courseForStudentsAcademic, string courseTypes)
        {
            var calCulatedGrade = _gradeManager.CalculateGrade(courseForStudentsAcademic, courseTypes);
            courseForStudentsAcademic.AddedDate = DateTime.Now.Date;
            db.CourseForStudentsAcademics.Add(calCulatedGrade);
            db.SaveChanges();
            var getAllCourses =
                _courseForStudentManger.GetAllCourseByStudent(courseForStudentsAcademic.StudentIdentificationId);

            if (getAllCourses.Any())
            {
                return PartialView("_courseListBySemester", getAllCourses.ToList());
            }
            return PartialView("_courseListBySemester", new List<CourseForStudentsAcademic>());
        }

        public ActionResult CourseManagementEdit(int takenCourseId = 0)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin") return RedirectToAction("NotPermitted", "Home");

            ViewBag.courseTypes = new SelectList(_commonAttributeManager.GetAllGradingSystems(), "GradingSystemId", "GradingSystemName", 0);
            ViewBag.CourseForDepartmentId = new SelectList(_courseForDepartmentManager.GetAllCourseForDepartmentsWithCode(0), "CourseForDepartmentId", "CourseName");
            ViewBag.CourseStatusId = new SelectList(_commonAttributeManager.GetallCourseStatuses(), "CourseStatusId", "Status");
            ViewBag.SemesterId = new SelectList(_semesterManager.GetAllSemester(), "SemesterId", "SemesterNYear");

            if (takenCourseId > 0)
            {

                var findTakenCourse = db.CourseForStudentsAcademics.Find(takenCourseId);
                var findStudent = db.StudentIdentifications.Find(findTakenCourse.StudentIdentificationId);
                if (findTakenCourse.GradingSystemId > 0)
                {
                    ViewBag.courseTypes = new SelectList(_commonAttributeManager.GetAllGradingSystems(), "GradingSystemId", "GradingSystemName", findTakenCourse.GradingSystemId);
                }
                if (findStudent != null)
                {
                    ViewBag.studentObj = findStudent;
                    ViewBag.CourseForDepartmentId = new SelectList(_courseForDepartmentManager.GetAllCourseForDepartmentsWithCode(findStudent.DepartmentId), "CourseForDepartmentId", "CourseName", findTakenCourse.CourseForDepartmentId);
                    ViewBag.CourseStatusId = new SelectList(_commonAttributeManager.GetallCourseStatuses(), "CourseStatusId", "Status", findTakenCourse.CourseStatusId);
                    ViewBag.SemesterId = new SelectList(_semesterManager.GetAllSemester(), "SemesterId", "SemesterNYear", findTakenCourse.SemesterId);

                }
                return View(findTakenCourse);
            }

            return View(new CourseForStudentsAcademic());
        }
        [HttpPost]
        public PartialViewResult CourseManagementEdit(CourseForStudentsAcademic courseForStudentsAcademic, int gradingCode = 1, bool emptyGrade = false)
        {

            UpdateCourseManageMentCourseOptimized(courseForStudentsAcademic, false, gradingCode, emptyGrade);
            var getAllCourses =
            _courseForStudentManger.GetAllCourseByStudent(courseForStudentsAcademic.StudentIdentificationId);
            if (getAllCourses.Any())
            {
                return PartialView("_courseListBySemester", getAllCourses.ToList());
            }
            return PartialView("_courseListBySemester", new List<CourseForStudentsAcademic>());

        }

        public void UpdateCourseManageMentCourseOptimized(CourseForStudentsAcademic courseForStudentsAcademic, bool courseAddOption,
            int gradingCode = 1, bool emptyGrade = false)
        {
            var courseStat = courseForStudentsAcademic.CourseStatusId;
            switch (courseStat)
            {
                case 8:
                    courseForStudentsAcademic.AddedDate = DateTime.Now.Date;
                    courseForStudentsAcademic.CourseStatusId = 8;
                    courseForStudentsAcademic.LetterGrade = "I";
                    courseForStudentsAcademic.Grade = 0;
                    courseForStudentsAcademic.TotalGrade = 0;
                    courseForStudentsAcademic.TotalMark = 0;
                    break;
                case 4:
                    courseForStudentsAcademic.AddedDate = DateTime.Now.Date;
                    //courseForStudentsAcademic.CourseStatusId = 4;
                    courseForStudentsAcademic.LetterGrade = "W";
                    courseForStudentsAcademic.Grade = 0;
                    courseForStudentsAcademic.TotalGrade = 0;
                    courseForStudentsAcademic.TotalMark = 0;
                    courseForStudentsAcademic.SectionId = null;
                    break;
                default:
                    if (emptyGrade)
                    {
                        courseForStudentsAcademic.LetterGrade = "";
                        courseForStudentsAcademic.Grade = 0;
                        courseForStudentsAcademic.TotalGrade = 0;
                        courseForStudentsAcademic.AddedDate = DateTime.Now.Date;
                    }
                    else
                    {
                        var calCulatedGrade = _gradeManager.CalculateGradeUpdated(courseForStudentsAcademic, gradingCode);
                        var takenCourseId = courseForStudentsAcademic.CourseForStudentsAcademicId;
                        calCulatedGrade.AddedDate = DateTime.Now.Date;
                        calCulatedGrade.DeclareMajor = courseForStudentsAcademic.DeclareMajor;
                        courseForStudentsAcademic = calCulatedGrade;
                    }
                    break;
            }
            if (courseAddOption)
            {
                db.CourseForStudentsAcademics.Add(courseForStudentsAcademic);
            }
            else
            {
                db.Entry(courseForStudentsAcademic).State = EntityState.Modified;
            }
            db.SaveChanges();
        }
        
        public ActionResult CourseDelete(int courseForStudentId)
        {

            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin") return RedirectToAction("NotPermitted", "Home");

            var findCourse = db.CourseForStudentsAcademics.Find(courseForStudentId);
            var findStudent = findCourse.StudentIdentification.StudentId;
            db.CourseForStudentsAcademics.Remove(findCourse);
            db.SaveChanges();
            TempData["deleteMsg"] = "Successfully deleted.";
            return RedirectToAction("CourseManagementAdmin", new { searchId = findStudent });
        }
        [HttpGet]
        public ActionResult CreateFeatures()
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" || account.AccountId != 1) return RedirectToAction("NotPermitted", "Home");
            return View();
        }

        [HttpPost]
        public ActionResult CreateFeatures(ControlFeatures controlFeatures)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" || account.AccountId != 1) return RedirectToAction("NotPermitted", "Home");
            db.ControlFeatureses.Add(controlFeatures);
            db.SaveChanges();
            return View();
        }

        public ActionResult FeaturesController(ControlFeatures controlFeatures)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin") return RedirectToAction("NotPermitted", "Home");


            return View();
        }

        public PartialViewResult UpdateInvaildModalLoad()
        {
            return PartialView("~/Views/Home/_UpdateInvalidCourses.cshtml");
        }

        public PartialViewResult ActivateOtherSemesterCourseAdvising()
        {
            var findAdvisingSemester = _semesterManager.GetAdvisingActivatedSemester();
            ViewBag.Heading = "Course advising is currently deactivated.";
            if (findAdvisingSemester != null)
            {
                ViewBag.Heading = findAdvisingSemester.SemesterNYear + " is currently activated as Advising Semsester";
            }
            ViewBag.SemesterId = new SelectList(_semesterManager.GetAllSemestersDefault().OrderByDescending(s => s.SemesterId), "SemesterId", "SemesterNYear", 0);
            return PartialView("~/Views/Home/_SemesterAdvisingControl.cshtml");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public string ActivateSpecificSemesterAdvising(int semesterId = 0)
        {
            //string msg="Nothing to do !!!";
            //switch (submitButton)
            //{
            //    case "updateLoginCounter":
            //        msg= CourseAdvisingOperation(semesterId);
            //        break;
            //    case "deactivate":
            //        msg= CourAdvisingDisable();
            //        break;
            //}
            //return msg;
            return CourseAdvisingOperation(semesterId);
        }
        public string CourseAdvisingOperation(int semesterId)
        {
            string msg = "";
            try
            {
                foreach (var semester in db.Semesters)
                {
                    semester.CourseAdvising = false;
                    if (semester.SemesterId == semesterId)
                    {
                        semester.CourseAdvising = true;
                        msg = semester.SemesterNYear + " has been set for Course Advising.";
                    }
                    db.Entry(semester).State = EntityState.Modified;
                }
                db.SaveChanges();
            }
            catch (Exception)
            {
                msg = "Error Found.";
            }
            return msg;
        }
        public string CourAdvisingDisable()
        {
            string msgs = "";
            try
            {
                foreach (var semester in db.Semesters.Where(s => s.CourseAdvising))
                {
                    semester.CourseAdvising = false;
                    db.Entry(semester).State = EntityState.Modified;
                }
                db.SaveChanges();
                msgs = "Course advising has been disabled successfully.";
            }
            catch (Exception)
            {
                msgs = "Error found while disabling course advising.";
            }
            return msgs;
        }
        public ActionResult ControlCourseAdvising()
        {
            return View();
        }

        public PartialViewResult _Settings()
        {
            if (Session["UserObj"] == null)
            {
                ViewBag.msg = "You are Not logged in.";
            }
            bool power = Session["specialPower"] != null;
            ViewBag.power = power;
            return PartialView();
        }

        public string AccessToSpecial(string passId)
        {
            string grantMsg = "Access Denied";
            string specialPassCode = "Al@Nisa";
            if (passId == specialPassCode)
            {
                Session["specialPower"] = true;
                grantMsg = "Access Granted";
            }
            return grantMsg;
        }

        public string DestroySpecial()
        {
            Session["specialPower"] = null;
            return "Done !";
        }

        public PartialViewResult ActivityLogView(DateTime? userId)
        {
            string msg = "";
            var newlist = new List<UserActivity>();
            var account = (Account)Session["UserObj"];
            if (Session["UserObj"] == null)
            {
                ViewBag.msg = "You are Not logged in.";
                return PartialView(newlist);
            }
            if (account.AccountsRoleId != 1 && account.AccountsRoleId != 14)
            {
                ViewBag.msg = "NotPermitted";
                return PartialView(newlist);
            }
           //if (Session["specialPower"] == null)
            //{
            //    ViewBag.msg = "Access Denied.";
            //    return PartialView(newlist);
            //}
            if (userId == null)
            {
                var currentTime = DateTime.Now.Date;
                var getAllActivities = _userActivityManager.GetAllUserActivity().Where(s => EntityFunctions.TruncateTime(s.ActivityTime) == currentTime).OrderByDescending(s => s.UserActivityId);
                return PartialView(getAllActivities.Take(450).ToList());
            }
            var getAllActivitiesSorted = _userActivityManager.GetAllUserActivity().Where(s => EntityFunctions.TruncateTime(s.ActivityTime) == userId).OrderByDescending(s => s.UserActivityId);
            return PartialView(getAllActivitiesSorted.Take(450).ToList());
        }

        public string DeleteActivityLog(int activityId = 0)
        {
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" || account.AccountId != 1) return "ja vag...";
            if (activityId > 0)
            {
                _userActivityManager.DeleteUserActivity(activityId);
                return "Activity Deleted.";
            }
            return "Delete failed.";
        }

        public ActionResult GradingHistory()
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" || account.AccountId != 1) return RedirectToAction("NotPermitted", "Home");
            var getAllGradingHistory = db.GradingHistories;
            return View(getAllGradingHistory.Take(200));
        }
        [HttpPost]
        public ActionResult GradingHistory(DateTime? starTime, DateTime? endTime)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" || account.AccountId != 1) return RedirectToAction("NotPermitted", "Home");
            if (starTime != null && endTime != null)
            {
                var getallGradingHistory =
                db.GradingHistories.Where(
                    s =>
                        EntityFunctions.TruncateTime(s.ChangedTime) >= starTime &&
                        EntityFunctions.TruncateTime(s.ChangedTime) <= endTime);
                ViewBag.starTime = starTime;
                ViewBag.endTime = endTime;
                return View(getallGradingHistory);
            }
            var getAllGradingHistory = db.GradingHistories;
            return View(getAllGradingHistory.Take(200));
        }

        public PartialViewResult _UpdatePharmacyAdvisings()
        {
            if (Session["UserObj"] == null)
            {
                ViewBag.msg = "You are Not logged in.";
                return PartialView();
            }
            if (Session["specialPower"] == null)
            {
                ViewBag.msg = "Access Denied";
                return PartialView();
            }
            var getSemesters = _semesterManager.GetAllSemester();
            ViewBag.selectedBatchId = new SelectList(getSemesters, "SemesterId", "BatchNo");
            ViewBag.advisedSemesterId = new SelectList(getSemesters, "SemesterId", "SemesterNYear");
            ViewBag.convertedSemesterId = new SelectList(getSemesters, "SemesterId", "SemesterNYear");
            ViewBag.PhramacyId = _departmentManager.GetSingleDepartment(17).DepartmentName;
            return PartialView();
        }

        public string SearchForAlterCounter(int selectedBatchId = 0, int advisedSemesterId = 0)
        {
            var countCourses =
                _courseForStudentManger.GetAllCourseForStudentsAcademicsByDeptAndSemester(17, advisedSemesterId).Where(s => s.StudentIdentification.SemesterId == selectedBatchId);
            var countStudent = countCourses.DistinctBy(s => s.StudentIdentificationId).Count();

            return countStudent + " Student & " + countCourses.Count() + "  Courses Found.";
        }

        [HttpPost]
        public string UpdatePharmacyAdvisingsConfirm(int selectedBatchId = 0, int advisedSemesterId = 0,
            int convertedSemesterId = 0)
        {
            var findCourses = db.CourseForStudentsAcademics.Where(s => s.CourseForDepartment.DepartmentId == 17
                && s.SemesterId == advisedSemesterId && s.StudentIdentification.SemesterId == selectedBatchId);
            var countChanges = findCourses.Count();
            var separateStudentIds = findCourses.DistinctBy(s => s.StudentIdentificationId);
            var courseForStudentsAcademics = separateStudentIds as CourseForStudentsAcademic[] ?? separateStudentIds.ToArray();
            string studentIds = courseForStudentsAcademics.Aggregate("", (current, students) => current + (students.StudentIdentification.StudentId + " ,"));
            foreach (var course in findCourses)
            {
                course.SemesterId = convertedSemesterId;
                db.Entry(course).State = EntityState.Modified;
            }
            db.SaveChanges();
            return countChanges + " AlterCompleted for " + courseForStudentsAcademics.Count() + " Students, ID's :" + studentIds;
        }

        public PartialViewResult _SyncStudentIdentity()
        {
            if (Session["UserObj"] == null)
            {
                ViewBag.msg = "You are Not logged in.";
                return PartialView();
            }
            if (Session["specialPower"] == null)
            {
                ViewBag.msg = "Access Denied";
                return PartialView();
            }
            var getAllNotSyncStudentList = _studentManager.GetAllStudentIdentifications().Count(s => s.StudentInfoId == null);
            ViewBag.notSynclist = getAllNotSyncStudentList;
            return PartialView();
        }

        public double NotSyncedStudents()
        {
            return _studentManager.GetAllStudentIdentifications().Count(s => s.StudentInfoId == null);
        }

        public string GetNotSyncedList()
        {
            if (Session["UserObj"] == null)
            {
                return "You are Not logged in.";
            }
            if (Session["specialPower"] == null)
            {
                return "Access Denied";
            }
            int counter = 1;
            string studentIds = "";
            var getAllStudentListOfNotSynced =
                _studentManager.GetAllStudentIdentifications().Where(s => s.StudentInfoId == null);
            foreach (var studentIdentification in getAllStudentListOfNotSynced)
            {
                studentIds += counter + ". " + studentIdentification.StudentId + Environment.NewLine;
                counter++;
            }
            return studentIds;
        }

        public PartialViewResult GetLogInOfStudent(DateTime? userId)
        {
            if (Session["UserObj"] == null)
            {
                ViewBag.msg = "You are Not logged in.";
                return PartialView();
            }
            if (Session["specialPower"] == null)
            {
                ViewBag.msg = "Access Denied";
                return PartialView();
            }
            string selectedDate;
            if (userId == null)
            {
                var getToday = DateTime.Now;
                selectedDate = getToday.ToString("d");
            }
            else
            {
                selectedDate = userId.Value.ToString("d");
            }
            ViewBag.selectedDate = selectedDate;
            var getLoginSorted = _historyManager.GetLoginHistoryStudentsByTime(selectedDate);
            return PartialView(getLoginSorted);
        }

        public string UpdateSectionStatus()
        {
            if (Session["UserObj"] == null)
            {
                return "You are Not logged in.";
            }
            if (Session["specialPower"] == null)
            {
                return "Access Denied";
            }
            _stopwatch.Restart();
            try
            {
                db.Configuration.AutoDetectChangesEnabled = false;
                var getAllsections = db.Sections.Where(s => s.GradingSystemId == null);
                var coreCourses = getAllsections.Where(s => s.CourseForDepartment.CourseType.ToLower() == "core");
                var labCourses = getAllsections.Where(s => s.CourseForDepartment.CourseType.ToLower() == "lab");
                var otherCourses = getAllsections.Where(s => s.CourseForDepartment.CourseType.ToLower() == "other");
                foreach (var sec in coreCourses)
                {
                    sec.GradingSystemId = 1;
                    db.Entry(sec).State = EntityState.Modified;
                }
                foreach (var sec in labCourses)
                {
                    sec.GradingSystemId = 2;
                    db.Entry(sec).State = EntityState.Modified;
                }
                foreach (var sec in otherCourses)
                {
                    sec.GradingSystemId = 3;
                    db.Entry(sec).State = EntityState.Modified;
                }
                db.SaveChanges();
            }
            catch (DbException)
            {
                return "Exception";

            }
            finally
            {
                db.Configuration.AutoDetectChangesEnabled = true;
            }
            _stopwatch.Stop();
            return "SectionUpdated Upadated, Time taken to complete " + _stopwatch.Elapsed;
        }

        public string UpdateFailedStudentGrades()
        {
            _stopwatch.Restart();
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                db.Configuration.LazyLoadingEnabled = false;
                db.Configuration.AutoDetectChangesEnabled = false;

                var getAllAdvisings =
                    db.CourseForStudentsAcademics.Where(s => s.CourseStatusId == 4);

                int total = 1;

                foreach (var adv in getAllAdvisings.ToList())
                {
                    adv.LetterGrade = "W";
                    db.Entry(adv).State = EntityState.Modified;
                    total++;
                }
                db.SaveChanges();
                _stopwatch.Stop();
                return "Operation Successfull on " + total + " advisings. Time taken " +
                       _stopwatch.Elapsed;
            }
            catch (DbException)
            {

                return "Exception";
            }
            finally
            {
                db.Configuration.AutoDetectChangesEnabled = true;
            }
        }

        public string UpdateStudentAdvisingAccoringtoSecId()
        {
            _stopwatch.Restart();
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                db.Configuration.LazyLoadingEnabled = false;
                db.Configuration.AutoDetectChangesEnabled = false;
                var getAllSections = db.Sections.Where(s => s.GradingSystemId != null);
                var getAllAdvisings =
                    db.CourseForStudentsAcademics.Where(s => s.GradingSystemId == null && s.SectionId != null)
                        .AsQueryable();
                int total = 1;
                foreach (var sec in getAllSections)
                {
                    var sec1 = sec;

                    foreach (var adv in getAllAdvisings.Where(s => s.SectionId == sec1.SectionId).ToList())
                    {
                        adv.GradingSystemId = sec1.GradingSystemId;
                        db.Entry(adv).State = EntityState.Modified;
                        total++;
                    }
                    db.SaveChanges();
                }
                _stopwatch.Stop();
                return "Operation Successfull on " + total + " advisings. Time taken " +
                       _stopwatch.Elapsed;
            }
            catch (DbException)
            {

                return "Exception";
            }
            finally
            {
                db.Configuration.AutoDetectChangesEnabled = true;
            }

        }

        //public string UpdatePicInStudentIdentification() // Discontinued in 2-2-2016
        //{
        //    if (Session["UserObj"] == null)
        //    {
        //        return "You are Not logged in.";
        //    }
        //    if (Session["specialPower"] == null)
        //    {
        //        return "Access Denied";
        //    }
        //   _stopwatch.Restart();
        //    var identification = from identification1 in db.StudentIdentifications 
        //                             where  string.IsNullOrEmpty(identification1.StudentPicture)
        //                             select identification1;
        //    foreach (var studentIdentification in identification)
        //    {
        //        studentIdentification.StudentPicture = studentIdentification.StudentId + ".jpg";
        //        db.Entry(studentIdentification).State = EntityState.Modified;
        //    }
        //    db.SaveChanges();
        //    _stopwatch.Stop();
        //    return "Students profile Upadated "+identification.Count()+", Time taken to complete "+_stopwatch.Elapsed;
        //}
        public string UpdateTeachersInformation() // Added 31-10-2015
        {
            if (Session["UserObj"] == null)
            {
                return "You are Not logged in.";
            }
            if (Session["specialPower"] == null)
            {
                return "Access Denied";
            }
            _stopwatch.Restart();
            var identification = from identification1 in db.Teachers
                                 select identification1;
            var accounts = db.Accounts.Where(s => s.AccountsRoleId == 5 || s.AccountsRoleId == 7).AsEnumerable().ToList();
            int updateCounter = 0;
            foreach (var teacherObj in identification)
            {
                var findAccountOfTeacher = accounts.FirstOrDefault(s => s.LoginIdentity == teacherObj.LoginId);
                if (findAccountOfTeacher != null)
                {
                    teacherObj.AccountId = findAccountOfTeacher.AccountId;
                    db.Entry(teacherObj).State = EntityState.Modified;
                    updateCounter++;
                }

            }
            db.SaveChanges();
            _stopwatch.Stop();
            return "Teachers Profile Updated :: " + updateCounter + ", Time taken to complete " + _stopwatch.Elapsed;
        }
        public PartialViewResult PowerMethodIndex()
        {
            if (Session["UserObj"] == null)
            {
                ViewBag.msg = "You are Not logged in.";
                return PartialView("~/Views/PowerWork/_PowerMethodIndexPartial.cshtml");
            }
            if (Session["specialPower"] == null)
            {
                ViewBag.msg = "Access Denied";
                return PartialView("~/Views/PowerWork/_PowerMethodIndexPartial.cshtml");
            }
            var getAllPowerWork = _powerWorkManager.GetAllPowerWorks();
            return PartialView("~/Views/PowerWork/_PowerMethodIndexPartial.cshtml", getAllPowerWork.ToList());
        }
        public PartialViewResult PowerWorkAccessIndex()
        {
            if (Session["UserObj"] == null)
            {
                ViewBag.msg = "You are Not logged in.";
                return PartialView("~/Views/PowerWork/_PowerWorkAccessIndexPartial.cshtml");
            }
            if (Session["specialPower"] == null)
            {
                ViewBag.msg = "Access Denied";
                return PartialView("~/Views/PowerWork/_PowerWorkAccessIndexPartial.cshtml");
            }
            var getAllPowerWork = _powerWorkManager.GetAllPowerWorkAccesses();
            return PartialView("~/Views/PowerWork/_PowerWorkAccessIndexPartial.cshtml", getAllPowerWork);
        }



        private bool _disposed = false;
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                db.Dispose();
            }
            _disposed = true;
        }
        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}