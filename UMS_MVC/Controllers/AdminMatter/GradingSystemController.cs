﻿using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web.Mvc;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO;

namespace UMS_MVC.Controllers.AdminMatter
{
    public class GradingSystemController : Controller
    {
        private readonly GradingSystemManager _gradingSystemManager;

        public GradingSystemController()
        {
            _gradingSystemManager=new GradingSystemManager();
        }
        public PartialViewResult GradingSystemIndexPartial()
        {
            if (Session["UserObj"] == null)
            {
                ViewBag.msg = "You are Not logged in.";
                return PartialView("_GradingSystemPartial");
            }
            if (Session["specialPower"] == null)
            {
                ViewBag.msg = "Access Denied";
                return PartialView("_GradingSystemPartial");
            }
            return PartialView("_GradingSystemPartial", _gradingSystemManager.GetAllGradingSystems().ToList());
        }
        public PartialViewResult EditGradingSystemPartial(int id)
        {
            if (Session["UserObj"] == null)
            {
                ViewBag.msg = "You are Not logged in.";
                return PartialView("_GradingSystemPartial");
            }
            if (Session["specialPower"] == null)
            {
                ViewBag.msg = "Access Denied";
                return PartialView("_GradingSystemPartial");
            }
            var aGradingSystem=new GradingSystem();
            if (id > 0)
            {
                aGradingSystem = _gradingSystemManager.FindGradingSystem(id);
            }
           
            return PartialView("_EditGradingSystemPartial", aGradingSystem);
        }
        public string UpdateGradingSystemCommit(GradingSystem gradingSystem, string save)
        {
            string msg = "";
            try
            {
                if (ModelState.IsValid)
                {
                    switch (save)
                    {
                        case "Add":
                           _gradingSystemManager.Insert(gradingSystem);
                            
                            msg = "Insert Completed";
                            break;
                        case "Update":
                            _gradingSystemManager.Update(gradingSystem);
                            msg = "Update Completed";
                            break;
                        case "Delete":
                            _gradingSystemManager.Delete(gradingSystem.GradingSystemId);
                            msg = "Delete Completed";
                            break;
                    }
                    _gradingSystemManager.Save();
                }
                else
                {
                    msg = "Update Failed";
                }
            }
            catch (DbUpdateException)
            {

                msg = "Internal Entity exception occured.";
            }

            return msg;
        }
    }
}