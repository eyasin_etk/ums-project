﻿using System.Data.Entity.Infrastructure;
using System.Web.Mvc;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.BLL;
using UMS_MVC.Manager.DAL.DAO;

namespace UMS_MVC.Controllers.AdminMatter
{
    public class PowerWorkController : Controller
    {

        private readonly PowerWorkBusinessLogic _powerWorkBusinessLogic;
        private readonly PowerWorkManager _powerWorkManager;
        private readonly AccountManager _accountManager;

        public PowerWorkController()
        {
            _powerWorkBusinessLogic=new PowerWorkBusinessLogic();
            _powerWorkManager=new PowerWorkManager();
            _accountManager=new AccountManager();
        }

        //-------Power Work ----------------------
        //public PartialViewResult PowerMethodIndex()
        //{
        //    if (Session["UserObj"] == null)
        //    {
        //        ViewBag.msg = "You are Not logged in.";
        //        return PartialView("~/Views/PowerWork/_PowerWorkIndexPartial.cshtml");
        //    }
        //    if (Session["specialPower"] == null)
        //    {
        //        ViewBag.msg = "Access Denied";
        //        return PartialView("~/Views/PowerWork/_PowerWorkIndexPartial.cshtml");
        //    }
        //    var getAllPowerWork = _powerWorkManager.GetAllPowerWorks();
        //    return PartialView("~/Views/PowerWork/_PowerWorkIndexPartial.cshtml", getAllPowerWork);
        //}

        //public PartialViewResult PowerWorkIndex()
        //{
        //    if (Session["UserObj"] == null)
        //    {
        //        ViewBag.msg = "You are Not logged in.";
        //        return PartialView("~/Views/PowerWork/_PowerWorkIndexPartial.cshtml");
        //    }
        //    if (Session["specialPower"] == null)
        //    {
        //        ViewBag.msg = "Access Denied";
        //        return PartialView("~/Views/PowerWork/_PowerWorkIndexPartial.cshtml");
        //    }
        //    var getAllPowerWork = _powerWorkManager.GetAllPowerWorks();
        //    return PartialView("~/Views/PowerWork/_PowerWorkIndexPartial.cshtml", getAllPowerWork);
        //}
        public PartialViewResult UpdatePartialViewResult(int id=0)
        {
            if (Session["UserObj"] == null)
            {
                ViewBag.msg = "You are Not logged in.";
                return PartialView();
            }
            if (Session["specialPower"] == null)
            {
                ViewBag.msg = "Access Denied";
                return PartialView();
            }
            if (id > 0)
            {
                var findPowerwork = _powerWorkManager.GetSinglePowerWork(id);
                return PartialView(findPowerwork);
            }
            var newPowerWork=new PowerWork()
            {
                PowerWorkId = 0,MethodName = "--",ControllerName = "--",Password = "--",Restricted = false
            };
            return PartialView(newPowerWork);
        }
        public string InsertPowerWorkCommit(PowerWork powerWork)
        {
            string msg;
            try
            {
                if (ModelState.IsValid)
                {
                    _powerWorkManager.InsertPowerWork(powerWork);
                    msg = "Insert Completed";
                }
                else
                {
                    msg = "Insert Failed";
                }
            }
            catch (DbUpdateException)
            {

                msg = "Internal Entity exception occured.";
            }

            return msg;
        }
        public string UpdatePowerWorkCommit(PowerWork powerWork, string save)
        {
            string msg = "";
            try
            {
                if (ModelState.IsValid)
                {
                    switch (save)
                    {
                        case "Add":
                            _powerWorkManager.InsertPowerWork(powerWork);
                            msg = "Insert Completed";
                            break;
                        case "Update":
                            _powerWorkManager.UpdatePowerWork(powerWork);
                            msg = "Update Completed";
                            break;
                        case "Delete":
                            _powerWorkManager.DeletePowerwork(powerWork.PowerWorkId);
                            msg = "Delete Completed";
                            break;
                    }        
                }
                else
                {
                    msg = "Operarion Failed";
                }
            }
            catch (DbUpdateException)
            {

                msg = "Internal Entity exception occured.";
            }
            
            return msg;
        }
        public string DeletePowerWorkCommit(int id)
        {
            string msg;
            try
            {
                if (id>0)
                {
                    _powerWorkManager.DeletePowerwork(id);
                    msg = "Delete Completed";
                }
                else
                {
                    msg = "Delete Failed";
                }
            }
            catch (DbUpdateException)
            {

                msg = "Internal Entity exception occured.";
            }

            return msg;
        }
        //-------Power Work ----------------------


        //-------Power Work Access----------------

        //public PartialViewResult PowerWorkAccessIndex()
        //{
        //    if (Session["UserObj"] == null)
        //    {
        //        ViewBag.msg = "You are Not logged in.";
        //        return PartialView();
        //    }
        //    if (Session["specialPower"] == null)
        //    {
        //        ViewBag.msg = "Access Denied";
        //        return PartialView();
        //    }
        //    var getAllPowerWork = _powerWorkManager.GetAllPowerWorkAccesses();
        //    return PartialView(getAllPowerWork);
        //}
        public PartialViewResult UpdateAccessPartialViewResult(int id = 0)
        {
            if (Session["UserObj"] == null)
            {
                ViewBag.msg = "You are Not logged in.";
                return PartialView();
            }
            if (Session["specialPower"] == null)
            {
                ViewBag.msg = "Access Denied";
                return PartialView();
            }
            var getAccounts = _accountManager.GetAllAccountlimited();
            var getPowerWorks = _powerWorkManager.GetAllPowerWorks();
           
            if (id > 0)
            {
                var findPowerwork = _powerWorkManager.GetSinglePowerWorkAccess(id);
                ViewBag.PowerWorkId = new SelectList(getPowerWorks, "PowerWorkId", "MethodName",
                    findPowerwork.PowerWorkId);
                 ViewBag.AccountId = new SelectList(getAccounts, "AccountId", "Name", "AccountsRole.AccountType",findPowerwork.AccountId);
                return PartialView(findPowerwork);
            }
            ViewBag.PowerWorkId = new SelectList(getPowerWorks, "PowerWorkId", "MethodName", 0);
             ViewBag.AccountId = new SelectList(getAccounts, "AccountId", "Name", "AccountsRole.AccountType", 0);
            var newPowerWork = new PowerWorkAccess()
            {
                PcIpAddress = "",IpRestrictions = false
            };
            return PartialView(newPowerWork);
        }
       
        public string UpdatePowerWorkAccessCommit(PowerWorkAccess powerWork, string save)
        {

            string msg="";
            try
            {
                if (ModelState.IsValid)
                {
                    switch (save)
                    {
                        case "Add":
                            _powerWorkManager.InsertPowerWorkAccess(powerWork);
                            msg = "Insert Completed";
                            break;
                        case "Update":
                            _powerWorkManager.UpdatePowerWorkAccess(powerWork);
                            msg = "Update Completed";
                            break;
                        case "Delete":
                            _powerWorkManager.DeletePowerWorkAccess(powerWork.PowerWorkAccessId);
                            msg = "Delete Completed";
                            break;
                    }  
                }
                else
                {
                    msg = "Update Failed";
                }
            }
            catch (DbUpdateException)
            {

                msg = "Internal Entity exception occured.";
            }

            return msg;
        }
        public string DeletePowerWorkAccessCommit(int id)
        {
            string msg;
            try
            {
                if (id > 0)
                {
                    _powerWorkManager.DeletePowerWorkAccess(id);
                    msg = "Delete Completed";
                }
                else
                {
                    msg = "Delete Failed";
                }
            }
            catch (DbUpdateException)
            {

                msg = "Internal Entity exception occured.";
            }

            return msg;
        }

        //-------Power Work Access----------------

    }
}