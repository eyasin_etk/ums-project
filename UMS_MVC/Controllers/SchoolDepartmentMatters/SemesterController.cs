﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.Controllers.SchoolDepartmentMatters
{
    public class SemesterController : Controller
    {
        private UmsDbContext db = new UmsDbContext();

        public ActionResult Index()
        {
            if (Session["Role"] != null && Session["User"] != null)
            {
                string role = Session["Role"].ToString();
                if (role == "Admin")
                {
                    //////////////// main code start /////////////////////////////
                    return View(db.Semesters.ToList());
                    ////////////////main code end////////////////////////////////
                }
                return RedirectToAction("NotPermitted", "Home");
            }
            return RedirectToAction("LogInresultSubmit", "Home");
           
        }

        
        public ActionResult Details(int? id)
        {
            if (Session["Role"] != null && Session["User"] != null)
            {
                string role = Session["Role"].ToString();
                if (role == "Admin")
                {
                    //////////////// main code start /////////////////////////////
                    if (id == null)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    Semester semester = db.Semesters.Find(id);
                    if (semester == null)
                    {
                        return HttpNotFound();
                    }
                    return View(semester);
                    ////////////////main code end////////////////////////////////
                }
                return RedirectToAction("NotPermitted", "Home");
            }
            return RedirectToAction("LogInresultSubmit", "Home");
        

           
        }

        
        public ActionResult Create()
        {
            if (Session["Role"] != null && Session["User"] != null)
            {
                string role = Session["Role"].ToString();
                if (role == "Admin")
                {
                    //////////////// main code start /////////////////////////////
                    ViewBag.SemesterInfoId = new SelectList(db.SemesterInfos, "SemesterName", "SemesterName");
                    return View();
                    ////////////////main code end////////////////////////////////
                }
                return RedirectToAction("NotPermitted", "Home");
            }
            return RedirectToAction("LogInresultSubmit", "Home");
        
            
        }

       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SemesterId,SemesterNYear,ActiveSemester")] Semester semester)
        {
            if (ModelState.IsValid)
            {
                db.Semesters.Add(semester);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SemesterInfoId = new SelectList(db.SemesterInfos, "SemesterName", "SemesterName");
            return View(semester);
        }

       
        public ActionResult Edit(int? id)
        {
            if (Session["Role"] != null && Session["User"] != null)
            {
                string role = Session["Role"].ToString();
                if (role == "Admin")
                {
                    //////////////// main code start /////////////////////////////

                    if (id == null)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    Semester semester = db.Semesters.Find(id);
                    if (semester == null)
                    {
                        return HttpNotFound();
                    }
                    ViewBag.SemesterInfoId = new SelectList(db.SemesterInfos, "SemesterName", "SemesterName");
                    return View(semester);
                    ////////////////main code end////////////////////////////////
                }
                return RedirectToAction("NotPermitted", "Home");
            }
            return RedirectToAction("LogInresultSubmit", "Home");
        

        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SemesterId,SemesterNYear,ActiveSemester,MidTerm,FinalTerm")] Semester semester)
        {
            var getSemesters = db.Semesters;
            if (ModelState.IsValid)
            {
                db.Entry(semester).State = EntityState.Modified;
                db.SaveChanges();
                if (semester.ActiveSemester)
                {
                    foreach (var semester1 in getSemesters)
                    {
                        if (semester1 != semester)
                        {
                            semester1.ActiveSemester = false;
                            db.Entry(semester1).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        
                    }
                }
                return RedirectToAction("Index");
            }

            ViewBag.SemesterInfoId = new SelectList(db.SemesterInfos, "SemesterName", "SemesterName");
            return View(semester);
        }

       
        public ActionResult Delete(int? id)
        {
            if (Session["Role"] != null && Session["User"] != null)
            {
                string role = Session["Role"].ToString();
                if (role == "Admin")
                {
                    //////////////// main code start /////////////////////////////
                    if (id == null)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    Semester semester = db.Semesters.Find(id);
                    if (semester == null)
                    {
                        return HttpNotFound();
                    }
                    return View(semester);
                    ////////////////main code end////////////////////////////////
                }
                return RedirectToAction("NotPermitted", "Home");
            }
            return RedirectToAction("LogInresultSubmit", "Home");
        

        }

        
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Semester semester = db.Semesters.Find(id);
            db.Semesters.Remove(semester);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
