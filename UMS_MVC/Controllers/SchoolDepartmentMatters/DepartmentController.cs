﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO;

namespace UMS_MVC.Controllers.SchoolDepartmentMatters
{
    public class DepartmentController : Controller
    {
        private UmsDbContext db = new UmsDbContext();
        private readonly SchoolManager  _schoolManager=new SchoolManager();
        private readonly DepartmentManager _departmentManager=new DepartmentManager();

        // GET: /Department/
        public ActionResult Index()
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin" && role != "Admission" && role != "Advisor" && role != "Accounts Officer" && role != "Ps" && role != "Coordinator" && role != "Support")
                return RedirectToAction("NotPermitted", "Home");
            var firstSchool = (_schoolManager.GetAllSchools().OrderBy(s => s.SchoolId)).First();
            var departments = _departmentManager.GetAllDepartment().Where(s => s.SchoolId == firstSchool.SchoolId);
            ViewBag.SchoolId = new SelectList(_schoolManager.GetAllSchools(), "SchoolId", "SchoolName", firstSchool.SchoolId);
            if (role == "Admin")
            {
                ViewBag.role = "Admin";
            }
            return View(departments);
        }

        public PartialViewResult SortedDepartmentList(int? schoolId)
        {
            var getAlldepartments = _departmentManager.GetAllDepartment();
            if (schoolId > 0)
            {
                var department = getAlldepartments.Where(s => s.SchoolId == schoolId);
                return PartialView("_DepartmentSortedListPartial",department.ToList());
            }
            return PartialView("_DepartmentSortedListPartial", getAlldepartments.ToList());
        }

        public ActionResult Details(int? id)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin" && role != "Admission" && role != "Ps" && role != "Coordinator"&&role != "Support")
                return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Department department = db.Departments.Find(id);
            if (department == null)
            {
                return HttpNotFound();
            }
            return View(department);
            ////////////////main code end////////////////////////////////
        }

        
        public ActionResult Create()
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            ViewBag.SchoolId = new SelectList(db.Schools, "SchoolId", "SchoolName");
            return View();
            ////////////////main code end////////////////////////////////
        }

       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DepartmentId,SchoolId,DepartmentName,DepartmentCode,TotalSemester,TotalCredit,AddmissionFee,PerCreditCost,LabFeePerSemester,OtherFeePerSemister,TotalCost,DateCreated,DepartmentTitle,RequiredCredit")] Department department)
        {
            
            var checkDepartment =
               _departmentManager.GetAllDepartment().Count(
                    x => x.DepartmentName == department.DepartmentName || x.DepartmentCode == department.DepartmentCode) ==
                0;
            if (checkDepartment)
            {
                if (ModelState.IsValid)
                {
                    db.Departments.Add(department);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            ViewBag.msg = "Department Name or Code already exists try another.";
            ViewBag.SchoolId = new SelectList(_schoolManager.GetAllSchools(), "SchoolId", "SchoolName", department.SchoolId);
            return View(department);
        }

        // GET: /Department/Edit/5
        public ActionResult Edit(int? id)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Department department = db.Departments.Find(id);
            if (department == null)
            {
                return HttpNotFound();
            }
            ViewBag.SchoolId = new SelectList(_schoolManager.GetAllSchools(), "SchoolId", "SchoolName", department.SchoolId);
            return View(department);
            ////////////////main code end////////////////////////////////
        }

       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DepartmentId,SchoolId,DepartmentName,DepartmentCode,TotalSemester,TotalCredit,AddmissionFee,PerCreditCost,LabFeePerSemester,OtherFeePerSemister,TotalCost,DateCreated,DepartmentTitle,RequiredCredit")] Department department)
        {
            if (ModelState.IsValid)
            {
                db.Entry(department).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SchoolId = new SelectList(_schoolManager.GetAllSchools(), "SchoolId", "SchoolName", department.SchoolId);
            return View(department);
        }

        // GET: /Department/Delete/5
        public ActionResult Delete(int? id)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin") return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Department department = db.Departments.Find(id);
            if (department == null)
            {
                return HttpNotFound();
            }
            return View(department);
            ////////////////main code end////////////////////////////////
        }

        // POST: /Department/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin") return RedirectToAction("NotPermitted", "Home");
            Department department = db.Departments.Find(id);
            db.Departments.Remove(department);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
