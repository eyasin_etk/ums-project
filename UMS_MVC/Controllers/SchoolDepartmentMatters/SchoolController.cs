﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.Controllers.SchoolDepartmentMatters
{
    public class SchoolController : Controller
    {
        private UmsDbContext db = new UmsDbContext();

       
        public ActionResult Index()
        {
            if (Session["Role"] != null && Session["User"] != null)
            {
                string role = Session["Role"].ToString();
                if (role == "Admin")
                {
                    //////////////// main code start /////////////////////////////
                    return View(db.Schools.ToList());
                    ////////////////main code end////////////////////////////////
                }
                return RedirectToAction("NotPermitted", "Home");
            }
            return RedirectToAction("LogInresultSubmit", "Home");           
        }

       
        public ActionResult Details(int? id)
        {

            if (Session["Role"] != null && Session["User"] != null)
            {
                string role = Session["Role"].ToString();
                if (role == "Admin")
                {
                    //////////////// main code start /////////////////////////////
                    if (id == null)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    School school = db.Schools.Find(id);
                    if (school == null)
                    {
                        return HttpNotFound();
                    }
                    return View(school);
                    ////////////////main code end////////////////////////////////
                }
                return RedirectToAction("NotPermitted", "Home");
            }
            return RedirectToAction("LogInresultSubmit", "Home");
        
           
           
        }

        
        public ActionResult Create()
        {
            if (Session["Role"] != null && Session["User"] != null)
            {
                string role = Session["Role"].ToString();
                if (role == "Admin")
                {
                    //////////////// main code start /////////////////////////////
                    return View();
                    ////////////////main code end////////////////////////////////
                }
                return RedirectToAction("NotPermitted", "Home");
            }
            return RedirectToAction("LogInresultSubmit", "Home");

        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="SchoolId,SchoolName,DateCreated")] School school)
        {
            if (ModelState.IsValid)
            {
                db.Schools.Add(school);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(school);
        }

        // GET: /School/Edit/5
        public ActionResult Edit(int? id)
        {
            if (Session["Role"] != null && Session["User"] != null)
            {
                string role = Session["Role"].ToString();
                if (role == "Admin")
                {
                    //////////////// main code start /////////////////////////////
                    if (id == null)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    School school = db.Schools.Find(id);
                    if (school == null)
                    {
                        return HttpNotFound();
                    }
                    return View(school);
                    ////////////////main code end////////////////////////////////
                }
                return RedirectToAction("NotPermitted", "Home");
            }
            return RedirectToAction("LogInresultSubmit", "Home");

        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="SchoolId,SchoolName,DateCreated")] School school)
        {
            if (ModelState.IsValid)
            {
                db.Entry(school).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(school);
        }

        // GET: /School/Delete/5
        public ActionResult Delete(int? id)
        {
            if (Session["Role"] != null && Session["User"] != null)
            {
                string role = Session["Role"].ToString();
                if (role == "Admin")
                {
                    //////////////// main code start /////////////////////////////
                    if (id == null)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    School school = db.Schools.Find(id);
                    if (school == null)
                    {
                        return HttpNotFound();
                    }
                    return View(school);
                    ////////////////main code end////////////////////////////////
                }
                return RedirectToAction("NotPermitted", "Home");
            }
            return RedirectToAction("LogInresultSubmit", "Home");
            
        }

        // POST: /School/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            School school = db.Schools.Find(id);
            db.Schools.Remove(school);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
