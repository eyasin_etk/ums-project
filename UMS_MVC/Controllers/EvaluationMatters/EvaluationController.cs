﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.DataModel.Models.TeacherEvaluation;
using UMS_MVC.Manager.DAL.BLL.TeachersEvaluationManager;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.Controllers.EvaluationMatters
{
    public class EvaluationController : Controller
    {

        private readonly EvaluationConfigurationLogicManager _configurationLogicManager;
        private readonly EvaluationResultLogicManager _resultLogicManager;
        private readonly SurveyAssesmentLogicManager _assesmentLogicManager;
        private readonly SurveyQuestionsLogicManager _questionsLogicManager;
        private readonly SemesterManager _semesterManager;
        private readonly CourseForStudentManger _courseForStudentManger;
        private readonly SectionManager _sectionManager;
        private readonly StudentManager _studentManager;
        private readonly CourseForDepartmentManager _courseForDepartmentManager;

        public EvaluationController()
        {

            _configurationLogicManager = new EvaluationConfigurationLogicManager();
            _resultLogicManager = new EvaluationResultLogicManager();
            _assesmentLogicManager = new SurveyAssesmentLogicManager();
            _questionsLogicManager = new SurveyQuestionsLogicManager();
            _semesterManager = new SemesterManager();
            _courseForStudentManger = new CourseForStudentManger();
            _sectionManager = new SectionManager();
            _studentManager = new StudentManager();
            _courseForDepartmentManager = new CourseForDepartmentManager();
        }
        //survey answer logic manager is static
        // GET: Evaluation
        public ActionResult Index()
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin") return RedirectToAction("NotPermitted", "Home");
            var config =
                _configurationLogicManager.GetAllEvaluationConfigurations().Include(s => s.Semester).OrderByDescending(s => s.EvaluationConfigurationId);
            var allSemesterForDropdown = _semesterManager.GetAllSemesterFiltered();
            ViewBag.semesterId = new SelectList(allSemesterForDropdown, "SemesterId", "SemesterNYear");
            return View(config.ToList());
        }

        public PartialViewResult ConfigDataPartial(int id = 0)
        {
            var allSemesterForDropdown = _semesterManager.GetAllSemesterFiltered();
            var evaluationConfiguration = new EvaluationConfiguration { SemesterId = 1 };
            if (id != 0)
            {
                evaluationConfiguration = _configurationLogicManager.FindConfiguration(id);
            }
            TempData["semesterId"] = new SelectList(allSemesterForDropdown, "SemesterId", "SemesterNYear", evaluationConfiguration.SemesterId);
            return PartialView("_configDataPartial", evaluationConfiguration);
        }

        public string UpdateEvaluation(EvaluationConfiguration configuration)
        {
            var account = (Account)Session["UserObj"];
            if (configuration.EvaluationConfigurationId != 0)
            {
                var evaluationConfiguration = _configurationLogicManager.FindConfiguration(configuration.EvaluationConfigurationId);
                evaluationConfiguration.SemesterId = configuration.SemesterId;
                evaluationConfiguration.StartDate = configuration.StartDate;
                evaluationConfiguration.EndDate = configuration.EndDate;
                evaluationConfiguration.RemoveEvaluationDate = DateTime.Now;
                evaluationConfiguration.RemovedBy = account.Name;
                evaluationConfiguration.Remarks = configuration.Remarks;

                _configurationLogicManager.UpdateConfiguration(evaluationConfiguration);
                return "Successfully Updated";
            }
            if (configuration.EvaluationConfigurationId == 0)
            {
                _configurationLogicManager.InsertConfiguration(configuration);
                return "Successfully Added";
            }
            return "Error in Update Process";
        }

        public PartialViewResult AssessmentDataPartial(int semesterId = 0)
        {
            try
            {
                if (semesterId > 0)
                {
                    //var assesmentQuery = _assesmentLogicManager.GetAllSurveyAssesment().AsQueryable();

                    var getSections =
                      _sectionManager.GetAllSectionBySemester(semesterId)
                          .Include(s => s.CourseForStudentsAcademics)
                          .Include(s => s.Semester)
                          .Select(s => new DataAssessmentViewModel
                          {

                              SectionId = s.SectionId,
                              CourseName = s.CourseForDepartment.CourseName,
                              SemesterName = s.Semester.SemesterNYear,
                              TotalStudent = s.CourseForStudentsAcademics.Count(),
                              UncompletedSurvey = s.CourseForStudentsAcademics.Count(x => !x.EvaluationComplete),
                              CompletedSurvey = s.CourseForStudentsAcademics.Count(x => x.EvaluationComplete)

                          }).ToList();

                    return PartialView("_assessmentDataPartial", getSections);
                }

            }
            catch (Exception exception)
            {

            }

            return PartialView("_assessmentDataPartial", null);
        }
        public string RemoveAssData(int id)
        {
            if (id <= 0) return "An error occured during removing !!";
            try
            {
                var getStudentsAcaId = _courseForStudentManger.GetAllCourseForStudentsAcademicsFiltered().Where(s => s.SectionId == id).ToList();
                foreach (var i in getStudentsAcaId)
                {
                    var getAcaIdWiseDataAssessment =
                        _assesmentLogicManager.GetAllSurveyAssesment()
                            .Where(s => s.CourseForStudentsAcademicId == i.CourseForStudentsAcademicId);
                    foreach (var j in getAcaIdWiseDataAssessment.ToList())
                    {
                        _assesmentLogicManager.DeleteAssesment(j.SurveryAssesmentId);
                    }
                }
                _assesmentLogicManager.Save();
                return "Removed Successfully";
            }
            catch (Exception exception)
            {
                return "Evaluation Incomplete. Error:" + exception;
            }

        }
    }
}