﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UMS_MVC.DataModel.Models;
using UMS_MVC.Manager.DAL.BLL.TeachersEvaluationManager;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.Controllers.EvaluationMatters
{
    public class EvaluationResultController : Controller
    {
        // GET: EvaluationResult
        private readonly EvaluationResultLogicManager _evaluationResultLogicManager;
        private readonly TeacherManager _teacherManager;
        private readonly SemesterManager _semesterManager;

        public EvaluationResultController()
        {
            _evaluationResultLogicManager = new EvaluationResultLogicManager();
            _teacherManager = new TeacherManager();
            _semesterManager = new SemesterManager();
        }

        public ActionResult Index()
        {

            return View();
        }

        public PartialViewResult EvaluationFilteredResult()
        {
            return PartialView("~/views/evaluationresult/_evaluationFilteredResultPartial.cshtml");
        }

        public ActionResult GeneratedresultOfEvaluation()
        {


            TempData["TeacherId"] = new SelectList(_teacherManager.GetAllTeacherFiltered().Include(s => s.Account).Include(s => s.Department), "TeacherId", "Account.Name", "Department.DepartmentName", 0);
            TempData["SemesterId"] = new SelectList(_semesterManager.GetAllSemesterForDropdown(), "SemesterId", "SemesterNYear", 0);
            return View();
        }

        public PartialViewResult GeneratedresultOfEvaluationPartial(int teacherId = 0, int semesterId = 0)
        {

            if (teacherId > 0 && semesterId > 0)
            {
                TempData["EvaluatedResultWithPartialInfo"] = _evaluationResultLogicManager.GetResultOfEvaluatedSections(semesterId, teacherId);
            }
            return PartialView("_genetatedResultOfEvaluationPartial");
        }

        public ActionResult GeneratePrintResult(int teacherId = 0, int semesterId = 0)
        {
            if (teacherId > 0 && semesterId > 0)
            {
                TempData["EvaluatedResultWithPartialInfo"] = _evaluationResultLogicManager.GetResultOfEvaluatedSections(semesterId, teacherId);
            }
            return new Rotativa.ViewAsPdf("GeneratePrintResult");
        }

      
    }
}