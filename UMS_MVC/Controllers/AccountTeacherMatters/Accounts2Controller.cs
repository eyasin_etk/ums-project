﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using PagedList;
using UMS_MVC.Controllers.OtherSpecialControllers;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO;

namespace UMS_MVC.Controllers.AccountTeacherMatters
{
    public class Accounts2Controller : Controller
    {
        private readonly UmsDbContext db;
        private readonly AccountManager _accountManager;
        private readonly PicController _picController;

        public Accounts2Controller()
        {
            db = new UmsDbContext();
            _accountManager = new AccountManager();
            _picController = new PicController();
        }

        public ActionResult Index(int? AccountsRoleId, int? page, string searchString)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account2 = (Account)Session["UserObj"];
            if (account2.AccountsRole.AccountType != "Admin" && account2.AccountsRole.AccountType != "Support")
                return RedirectToAction("NotPermitted", "Home");

            //////////////// main code start /////////////////////////////
            IQueryable<AccountsRole> accountDroppings;
            bool masterAccess = false;
            var getAllAccount = (from account in _accountManager.GetAllAccounts().Where(s=>s.AccountId!=1).OrderBy(s => s.LoginIdentity)
                                 select account).AsEnumerable();
            if (account2.AccountsRoleId == 1)
            {
                accountDroppings = _accountManager.GetAllAccountsRoles();
                masterAccess = true;
            }
            else
            {
                getAllAccount = getAllAccount.Where(s => s.AccountsRoleId != 1).ToList();
                accountDroppings = _accountManager.AccountRoleForSystemSupport();
            }
            TempData["masterAccess"] = masterAccess;
            TempData["AccountsRoleId"] = new SelectList(accountDroppings, "AccountsRoleId", "AccountType", AccountsRoleId);
            if (Request.HttpMethod != "GET")
            {
                page = 1;
            }
            int pageSize = 50;
            int pageNumber = (page ?? 1);
            if (!string.IsNullOrEmpty(searchString))
            {
                getAllAccount = getAllAccount.Where(s => s.LoginIdentity.Contains(searchString) || s.Name.Contains(searchString));
                return View(getAllAccount.ToPagedList(pageNumber, pageSize));
            }

            if (AccountsRoleId > 0)
            {
                ViewBag.AccountsRole = AccountsRoleId;
                getAllAccount = getAllAccount.Where(s => s.AccountsRoleId == AccountsRoleId);
                
                return View(getAllAccount.ToPagedList(pageNumber, pageSize));
            }

            TempData["AccountsRoleId"] = new SelectList(accountDroppings, "AccountsRoleId", "AccountType", 3);
            return View(getAllAccount.Where(s => s.AccountsRoleId == 3).ToPagedList(pageNumber, pageSize));

            ////////////////main code end////////////////////////////////
        }

        public JsonResult GetAccountsName(string term)
        {
            List<string> accounts = _accountManager.GetAllAccountsFiltered()
                .Where(s => s.Name.Contains(term))
                .Select(y => y.Name).ToList();
            
                
            return Json(accounts, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SetAllAdvisorDisable(string accType, bool stat)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var role = Session["Role"].ToString();
            if (role != "Admin") return RedirectToAction("NotPermitted", "Home");
            try
            {
                var getAllAccount = db.Accounts.Where(s => s.AccountsRoleId == 7);
                foreach (var account in getAllAccount)
                {
                    account.Status = stat;
                    db.Entry(account).State = EntityState.Modified;
                }
                db.SaveChanges();
                TempData["Msg"] = "All " + accType + " Is Updated";
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["Msg"] = "Modification Failed !!!";
                return RedirectToAction("Index");
            }
        }

        public ActionResult ChangeAllAccountsExceptAdmin(bool stat)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin") return RedirectToAction("NotPermitted", "Home");
            try
            {
                var getAllAccount = db.Accounts.Where(s => s.AccountsRoleId != 1);
                foreach (var account in getAllAccount)
                {
                    account.Status = stat;
                    db.Entry(account).State = EntityState.Modified;
                }
                db.SaveChanges();
                TempData["Msg"] = "All Accounts are updated.";
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["Msg"] = "Modification Failed !!!";
                return RedirectToAction("Index");
            }
        }

        public ActionResult ChangeAllAccountButTeacherAdvisor(bool stat)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            string role = Session["Role"].ToString();
            if (role != "Admin") return RedirectToAction("NotPermitted", "Home");
            try
            {
                var getAllAccount = db.Accounts.Where(s => s.AccountsRoleId == 5 || s.AccountsRoleId == 7);
                foreach (var account in getAllAccount)
                {
                    account.Status = stat;
                    db.Entry(account).State = EntityState.Modified;
                }
                db.SaveChanges();
                TempData["Msg"] = "All Accounts are updated.";
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["Msg"] = "Modification Failed !!!";
                return RedirectToAction("Index");
            }
        }

        public ActionResult Details(int id = 0)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account2 = (Account)Session["UserObj"];
            if (account2.AccountsRole.AccountType != "Admin" && account2.AccountsRole.AccountType != "Support") return RedirectToAction("NotPermitted", "Home");
            Account account = _accountManager.GetSingleAccount(id);
            if (account != null)
            {
                return PartialView(account);
            }
            return PartialView(new Account());
        }

        // GET: /Accounts2/Create
        public ActionResult Create(int type = 1)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account2 = (Account)Session["UserObj"];
            if (account2.AccountsRole.AccountType != "Admin" && account2.AccountsRole.AccountType != "Support")
                return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            if (account2.AccountsRoleId == 1)
            {
                ViewBag.AccountsRoleId = new SelectList(_accountManager.GetAllAccountsRoles(), "AccountsRoleId",
                    "AccountType");

            }
            else
            {
                ViewBag.AccountsRoleId = new SelectList(_accountManager.AccountRoleForSystemSupport(), "AccountsRoleId",
                    "AccountType");
            }

            //var pg = new Passgenerator();
            string accountId = GenerateAccountId();
            string password = Passgenerator.GeneratePassword();
            var account = new Account
            {
                LoginIdentity = accountId,
                Password = password
            };
            ViewBag.userId = accountId;
            ViewBag.password = password;

            if (type == 1)
            {
                TempData["layoutCtrl"] = true;
                return View(account);
            }
            TempData["layoutCtrl"] = false;
            return PartialView(account);
            ////////////////main code end////////////////////////////////
        }

        public string GenerateAccountId()
        {
            string year = DateTime.Now.ToString("yy");
            var getAllAccountbyYear = _accountManager.GetAllAccounts().Where(s => s.LoginIdentity.StartsWith(year));
            //int totalMembers = _accountManager.GetAllAccounts().Count(s=>s.LoginIdentity.StartsWith(year));
            int memberId = Convert.ToInt32(year + getAllAccountbyYear.Count().ToString("D4"));
            int lastmember = Convert.ToInt32(year + "9999");
            var allmember = _accountManager.GetAllAccounts();
            string newId = string.Empty;
            for (int i = memberId; i < lastmember; i++)
            {
                string member = Convert.ToString(i);
                if (allmember.Count(s => s.LoginIdentity == member) == 0)
                {
                    newId = member;
                    break;
                }
            }
            return newId;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public string CreateConfirm( Account account)
        {
            if (Session["Role"] == null || Session["User"] == null) return "Access Denied";
            string role = Session["Role"].ToString();
            if (role != "Admin" && role != "Support") return "Access Denied";
            var account1 = db.Accounts.Count(s => s.Email == account.Email) == 0;
            if (account1)
            {
                if (ModelState.IsValid)
                {
                    account.CreatedDate=DateTime.Now;
                    db.Accounts.Add(account);
                    db.SaveChanges();
                    return "Success";

                }
            }
            //return View(account);
            //System.Threading.Thread.Sleep(3000);
            return "Not Done";
        }

        // GET: /Accounts2/Edit/5
        public ActionResult Edit(int id = 0)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account2 = (Account)Session["UserObj"];
            if (account2.AccountsRole.AccountType != "Admin" && account2.AccountsRole.AccountType != "Support")
                return RedirectToAction("NotPermitted", "Home");
            Account account = _accountManager.GetSingleAccount(id);
            if (account != null)
            {
                if (account2.AccountsRoleId == 1)
                {
                    TempData["roleId"] = new SelectList(_accountManager.GetAllAccountsRoles(), "AccountsRoleId",
                        "AccountType", account.AccountsRoleId);
                    return PartialView(account);
                }
                TempData["roleId"] = new SelectList(_accountManager.AccountRoleForSystemSupport(), "AccountsRoleId",
                    "AccountType", account.AccountsRoleId);
                return PartialView(account);
            }
            TempData["roleId"] = new SelectList(_accountManager.GetAllAccountsRoles(), "AccountsRoleId",
                        "AccountType", 0);
            TempData["notPermittedMSg"] = "You are not permitted to edit other admins profile";
            return PartialView(new Account());
            ////////////////main code end////////////////////////////////
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public string Edit([Bind(Include = "AccountId,Name,Email,PhoneNo,TeleExchange,Designation,DepartmentSection,Password,LoginIdentity,RoleType,Pic,Status,AccountsRoleId,PartimeTeacher,ExpireDate,Deactivate,CauseOfDeactivate")] Account account)
        {
            string msg = "You cann't change data.";
            var account2 = (Account)Session["UserObj"];
            if (account2.AccountsRole.AccountType == "Admin" || account2.AccountsRole.AccountType == "Support")
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        db.Entry(account).State = EntityState.Modified;
                        db.SaveChanges();
                        msg = "Account Updated";
                    }
                }
                catch (DbException exception)
                {
                    msg = "Exception !!";

                }
            }
            return msg;
        }

        // GET: /Accounts2/Delete/5
        public ActionResult Delete(int? id)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account2 = (Account)Session["UserObj"];
            if (account2.AccountsRole.AccountType != "Admin" && account2.AccountsRole.AccountType != "Support")
                return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Account account = db.Accounts.Find(id);
            if (account.AccountsRoleId == account2.AccountsRoleId)
            {
                return RedirectToAction("Index");
            }
            if (account2.AccountsRoleId == 14 && account.AccountsRoleId == 1)
            {
                return RedirectToAction("Index");
            }
            return View(account);
            ////////////////main code end////////////////////////////////
        }


        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account2 = (Account)Session["UserObj"];
            if (account2.AccountsRole.AccountType != "Admin" && account2.AccountsRole.AccountType != "Support")
                return RedirectToAction("NotPermitted", "Home");
            //////////////// main code start /////////////////////////////
            if (id != 1)
            {
                Account account = db.Accounts.Find(id);
                db.Accounts.Remove(account);
                db.SaveChanges();
            }
           return RedirectToAction("Index");
        }
        //Password Changer
        [HttpGet]
        public ActionResult ChangePassword()
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(string oldPass, string newPass)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];

            if (!string.IsNullOrEmpty(oldPass) && !string.IsNullOrEmpty(newPass))
            {
                if (account.Password == oldPass)
                {
                    if (oldPass != newPass)
                    {
                        var findMyAccount = _accountManager.GetSingleAccount(account.AccountId);
                        findMyAccount.Password = newPass;
                        if (_accountManager.UpdateAccount(findMyAccount))
                        {
                            Session["UserObj"] = findMyAccount;
                            ViewData["msg"] = "Your Password has been successfully changed.";
                            return View();
                        }
                    }
                    ViewData["msg"] = "Old Password and new Password can not be same.";
                    return View();
                }
            }
            ViewData["msg"] = "Please type Passwords correctly.";
            return View();
        }
        ///Account Pic
        [HttpGet]
        public ActionResult UploadPictureOfUser()
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            return View();
        }

        [HttpPost]
        public ActionResult UploadPictureOfUser(HttpPostedFileBase file)
        {
            if (Session["UserObj"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (file != null && _picController.CheckImageType(file))
            {
                string imageName = account.LoginIdentity + ".jpg";
                var findMyAccount = _accountManager.GetSingleAccount(account.AccountId);

                if (findMyAccount.Pic == null)
                {
                    ResizeImageAndSaveUserProfile(file, imageName);
                    findMyAccount.Pic = imageName;
                    if (_accountManager.UpdateAccount(findMyAccount))
                    {
                        Session["UserObj"] = findMyAccount;
                        TempData["msgsucc"] = "Picture uploaded successfully";
                        return View();
                    }
                }
                if (findMyAccount.Pic != null && account.AccountsRole.AccountType == "Admin")
                {
                    ResizeImageAndSaveUserProfile(file, imageName);
                    findMyAccount.Pic = imageName;
                    if (_accountManager.UpdateAccount(findMyAccount))
                    {
                        Session["UserObj"] = findMyAccount;
                        TempData["msgsucc"] = "Picture uploaded successfully";
                        return View();
                    }
                }
                TempData["msgfailed"] =
                    "Sorry,You are not allowed to upload picture twice ,if you wish to change your picture please contact With Software Engineering Section";
                return View();
            }
            TempData["msgfailed"] = "No picture for upload or your picture is not in jpg format.";
            return View();
        }
        public void ResizeImageAndSaveUserProfile(HttpPostedFileBase file, string imageName)
        {
            WebImage img = new WebImage(file.InputStream);
            if (img.Height > 450)
                img.Resize(350, 450);
            string physicalPath = Server.MapPath("~/Image/AccountPic/" + imageName);
            img.Save(physicalPath);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
                _accountManager.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
