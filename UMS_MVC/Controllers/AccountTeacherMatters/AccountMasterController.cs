﻿using System;
using System.Linq;
using System.Web.Mvc;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.AccountExt;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.Controllers.AccountTeacherMatters
{
    public class AccountMasterController : Controller
    {
        private readonly AccountManager _accountManager;
        private readonly AccountMetaInfoManager _metaInfoManager;
        private readonly AccountMetaProfManager _metaProfManager;
        private readonly AccountExtProfessionalManager _accountExtProfessionalManager;
        private readonly AccountExtEducationManager _accountExtEducationManager;
        private readonly AccountExtProjectManager _accountExtProjectManager;
        private readonly AccountExtPublicationManager _accountExtPublicationManager;
        private readonly CommonAttributeManager _commonAttributeManager;

        public AccountMasterController()
        {

            _accountManager = new AccountManager();
            _metaInfoManager=new AccountMetaInfoManager();
            _metaProfManager=new AccountMetaProfManager();
            _accountExtPublicationManager=new AccountExtPublicationManager();
            _accountExtEducationManager=new AccountExtEducationManager();
            _accountExtProfessionalManager=new AccountExtProfessionalManager();
            _accountExtProjectManager=new AccountExtProjectManager();
            _commonAttributeManager=new CommonAttributeManager();
        }

        // GET: AccountMaster
        public ActionResult Index()
        {
            //Security
            if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
            var account2 = (Account)Session["UserObj"];
            if (account2.AccountsRole.AccountType != "Admin" && account2.AccountsRole.AccountType != "Support")
                return RedirectToAction("NotPermitted", "Home");
            //Main Code Starts Here

            return View();
        }

        public PartialViewResult AccountIndexPartial()
        {
            if (Session["Role"] == null || Session["User"] == null) return PartialView("_SessionOutPartial");
            var account2 = (Account)Session["UserObj"];
            if (account2.AccountsRole.AccountType != "Admin" && account2.AccountsRole.AccountType != "Support")
                return PartialView("_notPermittedPartial");
            var allAccountList = _accountManager.GetAllAccounts();
              return PartialView("_AccountIndexPartial",allAccountList.ToList());
        }

        public AccountViewModel GetMeAccount(int id)
        {
            AccountViewModel accountViewModel=new AccountViewModel();
            var user = _accountManager.GetSingleAccount(id);
            var metainfo = _metaInfoManager.GetAllMetaInfo().FirstOrDefault(s => s.AccountId == id);
            var getMetaProfInfo =
                    _metaProfManager.GetAccountMetaProfFnD(id);
            
            accountViewModel.Account = user;
            accountViewModel.AccountMetaInformation = metainfo;
            accountViewModel.AccountMetaProfessional = getMetaProfInfo;
            return accountViewModel;
        }
        public PartialViewResult AccountDetailsPartial(int id = 0)
        {
            if (Session["Role"] == null || Session["User"] == null) return PartialView("_SessionOutPartial");
            var account2 = (Account)Session["UserObj"];
            if (account2.AccountsRole.AccountType != "Admin" && account2.AccountsRole.AccountType != "Support")
                return PartialView("_notPermittedPartial");
            AccountViewModel accountViewModel=new AccountViewModel();
            if (id > 0)
            {
                var getData = GetMeAccount(id);
                if (getData != null)
                {
                    return PartialView("_AccountDetailSecondViewPartial",getData);
                }
            }


            return PartialView("_AccountDetailSecondViewPartial");
        }
        //public PartialViewResult AccountDetailsPartial(int id = 0)
        //{
        //    if (Session["Role"] == null || Session["User"] == null) return PartialView("_SessionOutPartial");
        //    var account2 = (Account)Session["UserObj"];
        //    if (account2.AccountsRole.AccountType != "Admin" && account2.AccountsRole.AccountType != "Support")
        //        return PartialView("_notPermittedPartial");

        //    var accountMetaProfessional = new AccountMetaProfessional
        //    {
        //        AccountId = id,
        //        AccountMetaProfessionalId = 0
        //    };
        //    var extprofessional = new AccountExtProfessionalActivity{AccountMetaProfessionalId = 0};
        //    var extProject = new AccountExtProject{AccountExtProjectId = 0};
        //    var extPublication = new AccountExtPublication{AccountExtPublicationId = 0};
        //    var exteducation = new AccountExtEducation{AccountExtEducationId = 0};
        //    var accountMetaInformation = new AccountMetaInformation
        //    {
        //        AccountId = id,
        //        AccountMetaInformationId = 0
        //    };
        //    if (id > 0)
        //    {
        //        var user = _accountManager.GetSingleAccount(id);
        //        var metainfo = _metaInfoManager.GetAllMetaInfo().FirstOrDefault(s => s.AccountId == id);
        //        if (metainfo != null)
        //        {
        //            accountMetaInformation = metainfo;
        //        }
        //        var getMetaProfInfo =
        //            _metaProfManager.GetAllAccountMetaProfessionals().FirstOrDefault(s => s.AccountId == id);

        //        if (getMetaProfInfo != null)
        //        {
        //            accountMetaProfessional = getMetaProfInfo;
        //            var getExtprofessional =
        //                _accountExtProfessionalManager.GetAllAccountExtProfessionalActivity()
        //                    .FirstOrDefault(
        //                        s => s.AccountMetaProfessionalId == accountMetaProfessional.AccountMetaProfessionalId);
        //             var getExtProject =
        //                _accountExtProjectManager.GetAllAccountExtProjects()
        //                    .FirstOrDefault(
        //                        s => s.AccountMetaProfessionalId == accountMetaProfessional.AccountMetaProfessionalId);
        //             var getExtPublication =
        //                 _accountExtPublicationManager.GetAllAccountExtPublications()
        //                    .FirstOrDefault(
        //                        s => s.AccountMetaProfessionalId == accountMetaProfessional.AccountMetaProfessionalId);
        //            var getExteducation =
        //                _accountExtEducationManager.GetAllAccountExtEducations()
        //                    .FirstOrDefault(
        //                        s => s.AccountMetaProfessionalId == accountMetaProfessional.AccountMetaProfessionalId);
        //            if (getExtprofessional != null)
        //            {
        //                extprofessional = getExtprofessional;
        //            }
        //            if (getExtProject!=null)
        //            {
        //                extProject = getExtProject;
        //            }
        //            if (getExtPublication!=null)
        //            {
        //                extPublication = getExtPublication;
        //            }
        //            if (getExteducation!=null)
        //            {
        //                exteducation = getExteducation;
        //            }

        //        }

        //        ViewData["extProf"] = extprofessional;
        //        ViewData["extProj"] = extProject;
        //        ViewData["extPub"] = extPublication;
        //        ViewData["extEdu"] = exteducation;
        //        ViewData["metaProf"] = accountMetaProfessional;
        //        ViewData["metaInfo"] = accountMetaInformation;
        //        return PartialView("_AccountDetailsPartial",user);
        //    }


        //    return PartialView("_AccountDetailsPartial");
        //}

        public PartialViewResult AccountMetaInfoPartial(int id=0,int metaId=0)
        {
            if (Session["Role"] == null || Session["User"] == null) return PartialView("_SessionOutPartial");
            var account2 = (Account)Session["UserObj"];
            if (account2.AccountsRole.AccountType != "Admin" && account2.AccountsRole.AccountType != "Support")
                return PartialView("_notPermittedPartial");

            var makeAccountMetaObj = new AccountMetaInformation
            {
                AccountId = id,
                BloodGroupsId = 1,
                MaritalStatusId = 1,
                GenderId = 1
            };
            if (id != 0 && metaId!=0)
            {
                var metaInformation = _metaInfoManager.GetSingleMetaInformations(metaId);
                if (metaInformation != null)
                {
                    makeAccountMetaObj = metaInformation;

                }
            }
            ViewBag.BloodGroupsId = new SelectList(_commonAttributeManager.GetAllBloodGroups(), "BloodGroupsId", "Name", makeAccountMetaObj.BloodGroupsId);
            ViewBag.GenderId = new SelectList(_commonAttributeManager.GetAllGenders(), "GenderId", "GenderName", makeAccountMetaObj.GenderId);
            ViewBag.MaritalStatusId = new SelectList(_commonAttributeManager.GetAllMaritalStatuses(), "MaritalStatusId", "MaritalStat", makeAccountMetaObj.MaritalStatusId);
            return PartialView("_AccountMetaInfoPartial", makeAccountMetaObj);
        }

        [ValidateAntiForgeryToken]
        public string AccountMetaInfoPartialSubmit(AccountMetaInformation accountMetaInformation)
        {
            var account2 = (Account) Session["UserObj"];
            if (account2 != null)
            {
                if (accountMetaInformation.AccountMetaInformationId != 0)
                {
                    accountMetaInformation.EntryBy = account2.LoginIdentity + " " + account2.Name + " (Update)";
                    accountMetaInformation.EntryDate = DateTime.Now;
                    _metaInfoManager.Update(accountMetaInformation);
                }
                else
                {
                    accountMetaInformation.EntryBy = account2.LoginIdentity + " " + account2.Name+" (Add)";
                    accountMetaInformation.EntryDate=DateTime.Now;
                    _metaInfoManager.Insert(accountMetaInformation);
                }
                return _metaInfoManager.SaveData();
            }
            return "Communication Lost.";
        }

        public PartialViewResult AccountMetaProfessionalPartial(int id=0,int metaId=0)
        {
            if (Session["Role"] == null || Session["User"] == null) return PartialView("_SessionOutPartial");
            var account2 = (Account)Session["UserObj"];
            if (account2.AccountsRole.AccountType != "Admin" && account2.AccountsRole.AccountType != "Support")
                return PartialView("_notPermittedPartial");
            var accountProfessional = new AccountMetaProfessional
            {
                AccountId = id
            };
            if (id != 0 && metaId != 0)
            {
                var metaProfessional = _metaProfManager.GetSingleAccountMetaProfessional(metaId);
                if (metaProfessional != null)
                {
                    accountProfessional = metaProfessional;

                }
            }
           
            return PartialView("_AccountMetaProfPartial",accountProfessional);
        }

        [ValidateAntiForgeryToken]
        public string AccountMetaProfPartialSubmit(AccountMetaProfessional accountMetaProfessional)
        {
            var account2 = (Account)Session["UserObj"];
            if (account2 != null)
            {
                if (accountMetaProfessional.AccountMetaProfessionalId != 0)
                {
                    accountMetaProfessional.EntryBy = account2.LoginIdentity + " " + account2.Name + " (Update)";
                    accountMetaProfessional.EntryDate = DateTime.Now;
                    _metaProfManager.Update(accountMetaProfessional);
                }
                else
                {
                    accountMetaProfessional.EntryBy = account2.LoginIdentity + " " + account2.Name + " (Add)";
                    accountMetaProfessional.EntryDate = DateTime.Now;
                    _metaProfManager.Insert(accountMetaProfessional);
                }
                return _metaProfManager.SaveData();
            }
            return "Communication Lost.";
        }

        public PartialViewResult ExtProfessionalPartial(int id=0,int extProfId=0)
        {
            if (Session["Role"] == null || Session["User"] == null) return PartialView("_SessionOutPartial");
            var account2 = (Account)Session["UserObj"];
            if (account2.AccountsRole.AccountType != "Admin" && account2.AccountsRole.AccountType != "Support")
                return PartialView("_notPermittedPartial");
            
            var accountExtProfessional = new AccountExtProfessionalActivity
            {
                AccountMetaProfessionalId = id,
                AccountExtProfessionalActivityId = 0,
                

            };
            if (extProfId!=0)
            {
                var extprof =
                    _accountExtProfessionalManager.GetSinglExtProfessionalActivity(extProfId);
                if (extprof!=null)
                {
                    accountExtProfessional = extprof;
                }
            }
            TempData["attendType"] = new SelectList(_commonAttributeManager.GetAllAccountActivitiesTypes(), "Name", "Name", accountExtProfessional.AttendType);
            return PartialView("_AccountExtProfessionalPartial",accountExtProfessional);
        }

        public string ExtProfessionalPartialSubmit(AccountExtProfessionalActivity extProfessionalActivity)
        {
            var account2 = (Account)Session["UserObj"];
            if (account2 != null)
            {
                if (extProfessionalActivity.AccountExtProfessionalActivityId != 0)
                {
                    extProfessionalActivity.EntryBy = account2.LoginIdentity + " " + account2.Name + " (Update)";
                    extProfessionalActivity.EntryDate = DateTime.Now;
                    _accountExtProfessionalManager.Update(extProfessionalActivity);
                }
                else
                {
                    extProfessionalActivity.EntryBy = account2.LoginIdentity + " " + account2.Name + " (Add)";
                    extProfessionalActivity.EntryDate = DateTime.Now;
                    _accountExtProfessionalManager.Insert(extProfessionalActivity);
                }
                return _accountExtProfessionalManager.SaveData();
            }
            return "Communication Lost.";
        }
        public PartialViewResult ExtEducationPartial(int id = 0, int extEduId = 0)
        {
            if (Session["Role"] == null || Session["User"] == null) return PartialView("_SessionOutPartial");
            var account2 = (Account)Session["UserObj"];
            if (account2.AccountsRole.AccountType != "Admin" && account2.AccountsRole.AccountType != "Support")
                return PartialView("_notPermittedPartial");
            var accountExtEducation = new AccountExtEducation()
            {
                AccountMetaProfessionalId = id,
                AccountExtEducationId = 0
            };
            if (extEduId != 0)
            {
                var extEdu =
                    _accountExtEducationManager.GetSinglEducation(extEduId);
                if (extEdu != null)
                {
                    accountExtEducation = extEdu;
                }
            }
            return PartialView("_AccountExtEducationPartial", accountExtEducation);
        }
        [ValidateAntiForgeryToken]
        public string ExtEducationPartialSubmit(AccountExtEducation accountExtEducation)
        {
            var account2 = (Account)Session["UserObj"];
            if (account2 != null)
            {
                if (accountExtEducation.AccountExtEducationId != 0)
                {
                    accountExtEducation.EntryBy = account2.LoginIdentity + " " + account2.Name + " (Update)";
                    accountExtEducation.EntryDate = DateTime.Now;
                    _accountExtEducationManager.Update(accountExtEducation);
                }
                else
                {
                    accountExtEducation.EntryBy = account2.LoginIdentity + " " + account2.Name + " (Add)";
                    accountExtEducation.EntryDate = DateTime.Now;
                    _accountExtEducationManager.Insert(accountExtEducation);
                }
                return _accountExtEducationManager.SaveData();
            }
            return "Communication Lost.";
        }

        public PartialViewResult ExtProjectPartial(int id = 0, int extProjId = 0)
        {
            if (Session["Role"] == null || Session["User"] == null) return PartialView("_SessionOutPartial");
            var account2 = (Account)Session["UserObj"];
            if (account2.AccountsRole.AccountType != "Admin" && account2.AccountsRole.AccountType != "Support")
                return PartialView("_notPermittedPartial");
            var accountExtProject = new AccountExtProject()
            {
                AccountMetaProfessionalId = id,
                AccountExtProjectId = 0
            };
            if (extProjId != 0)
            {
                var extProj =
                    _accountExtProjectManager.GetSingleAccountExtProject(extProjId);
                if (extProj != null)
                {
                    accountExtProject = extProj;
                }
            }
            return PartialView("_AccountExtProjectPartial", accountExtProject);
        }
        [ValidateAntiForgeryToken]
        public string ExtProjectPartialSubmit(AccountExtProject accountExtProject)
        {
            var account2 = (Account)Session["UserObj"];
            if (account2 != null)
            {
                if (accountExtProject.AccountExtProjectId != 0)
                {
                    accountExtProject.EntryBy = account2.LoginIdentity + " " + account2.Name + " (Update)";
                    accountExtProject.EntryDate = DateTime.Now;
                    _accountExtProjectManager.Update(accountExtProject);
                }
                else
                {
                    accountExtProject.EntryBy = account2.LoginIdentity + " " + account2.Name + " (Add)";
                    accountExtProject.EntryDate = DateTime.Now;
                    _accountExtProjectManager.Insert(accountExtProject);
                }
                return _accountExtProjectManager.SaveData();
            }
            return "Communication Lost.";
        }

        public PartialViewResult ExtPublicationPartial(int id = 0, int extPubId = 0)
        {
            if (Session["Role"] == null || Session["User"] == null) return PartialView("_SessionOutPartial");
            var account2 = (Account)Session["UserObj"];
            if (account2.AccountsRole.AccountType != "Admin" && account2.AccountsRole.AccountType != "Support")
                return PartialView("_notPermittedPartial");
            var accExtPublication = new AccountExtPublication
            {
                AccountMetaProfessionalId = id,
                AccountExtPublicationId = 0
            };
            if (extPubId != 0)
            {
                var extPub = _accountExtPublicationManager.GetSinglExtPublication(extPubId);
                if (extPub != null)
                {
                    accExtPublication = extPub;
                }
            }
            TempData["publicationType"] = new SelectList(_commonAttributeManager.GetAllAccountExtededPublicationTypes(), "Name", "Name", accExtPublication.Type);
            return PartialView("_AccountExtPublicationPartial", accExtPublication);
        }
        [ValidateAntiForgeryToken]
        public string ExtPublicationPartialSubmit(AccountExtPublication accountExtPublication)
        {
            var account2 = (Account)Session["UserObj"];
            if (account2 != null)
            {
                if (accountExtPublication.AccountExtPublicationId != 0)
                {
                    accountExtPublication.EntryBy = account2.LoginIdentity + " " + account2.Name + " (Update)";
                    accountExtPublication.EntryDate = DateTime.Now;
                    _accountExtPublicationManager.Update(accountExtPublication);
                }
                else
                {
                    accountExtPublication.EntryBy = account2.LoginIdentity + " " + account2.Name + " (Add)";
                    accountExtPublication.EntryDate = DateTime.Now;
                    _accountExtPublicationManager.Insert(accountExtPublication);
                }
                return _accountExtPublicationManager.SaveData();
            }
            return "Communication Lost.";
        }

        public string DeleteMst(int id, int optType)
        {
            string msg = "";
            switch (optType)
            {
                case 1:
                    _metaInfoManager.Delete(id);
                    _metaInfoManager.SaveData();
                    msg = "Meta Information Deleted";
                    break;
                case 2:
                    _metaProfManager.Delete(id);
                    _metaProfManager.SaveData();
                    msg = "Meta Professional Information Deleted";
                    break;
                case 3:
                    _accountExtProfessionalManager.Delete(id);
                    _accountExtProfessionalManager.SaveData();
                    msg = "Ext Professional Information Deleted";
                    break;
                case 4:
                    _accountExtProjectManager.Delete(id);
                    _accountExtProjectManager.SaveData();
                    msg = "Ext Project Information Deleted";
                    break;
                case 5:
                    _accountExtPublicationManager.Delete(id);
                    _accountExtPublicationManager.SaveData();
                    msg = "Ext Publication Information Deleted";
                    break;
                case 6:
                    _accountExtEducationManager.Delete(id);
                    _accountExtEducationManager.SaveData();
                    msg = "Ext Educational Information Deleted";
                    break;
            }
            return msg;
        }
    }
}