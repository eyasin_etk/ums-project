﻿using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO;

namespace UMS_MVC.Controllers.AccountTeacherMatters
{
    public class TeacherController : Controller
    {
        private UmsDbContext db = new UmsDbContext();
        private readonly SemesterManager _semesterManager=new SemesterManager();
        private readonly TeacherManager _teacherManager=new TeacherManager();
        private readonly AccountManager _accountManager=new AccountManager();
        private readonly DepartmentManager _departmentManager=new DepartmentManager();
        private readonly CommonAttributeManager _commonAttributeManager=new CommonAttributeManager();
        private readonly SectionManager _sectionManager=new SectionManager();

        public ActionResult SyncTeacherWithAccount()
        {
            if (Session["UserObj"] == null)
                return RedirectToAction("LogInresultSubmit", "Home");
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin"&&account.AccountsRole.AccountType!="Support") return RedirectToAction("NotPermitted", "Home");
            // main code start..
            var getAccoaunts =
                _accountManager.GetAllAccounts().Where(
                    s => s.AccountsRole.AccountType == "Teacher" || s.AccountsRole.AccountType == "Advisor");
            var getAllteacher = _teacherManager.GetAllTeacher();
            ViewData["accounts"] = getAccoaunts;
            ViewData["teachers"] = getAllteacher;
            return View();

        }
        
        public ActionResult Index()
        {
           if (Session["Role"] == null || Session["User"] == null) return RedirectToAction("LogInresultSubmit", "Home");
           string role = Session["Role"].ToString();
           if (role != "Admin" && role != "Ps" && role != "Coordinator"&& role!="Support") return RedirectToAction("NotPermitted", "Home");
           //////////////// main code start /////////////////////////////
           TempData["departments"] = new SelectList(_departmentManager.GetAllDepartment(), "DepartmentId", "DepartmentName", "ASchool.SchoolName", 0);
            return View();
            ////////////////main code end////////////////////////////////
        }

        public PartialViewResult IndexPartial(string searchString, int departmentId = 0)
        {
            
            if (departmentId > 0)
            {
                var teachersFlt = _teacherManager.GetAllTeachersByDepartmentFiltered(departmentId).ToList();
                ViewData["totalTeacher"] = teachersFlt.Count();
                return PartialView("_TeacherIndexPartial",teachersFlt);
            }
            if (!string.IsNullOrEmpty(searchString))
            {

                var teachers = _teacherManager.GetAllTeacherFiltered().Include(s => s.Account).Include(s => s.Department).Where(s => s.Account.Name.Contains(searchString) || s.LoginId.Contains(searchString)).ToList();
                ViewData["totalTeacher"] = teachers.Count();
                return PartialView("_TeacherIndexPartial", teachers);
            }
            return PartialView("_TeacherIndexPartial",new List<Teacher>());
        }

        // GET: /Teacher/Details/5
        public ActionResult Details(int? id)
        {
            if (Session["Role"] != null && Session["User"] != null)
            {
                string role = Session["Role"].ToString();
                if (role == "Admin" && role != "Ps" && role != "Coordinator" && role == "Support")
                {
                    //////////////// main code start /////////////////////////////

                    if (id == null)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    Teacher teacher = db.Teachers.Find(id);
                    if (teacher == null)
                    {
                        return HttpNotFound();
                    }
                    return View(teacher);
                    ////////////////main code end////////////////////////////////
                }
                return RedirectToAction("NotPermitted", "Home");
            }
            return RedirectToAction("LogInresultSubmit", "Home");

        }

        // GET: /Teacher/Create
        public PartialViewResult AlterTeacher(int id=0)
        {
            if (Session["Role"] == null || Session["User"] == null) return PartialView("_SessionOutPartial");
            var account2 = (Account)Session["UserObj"];
            if (account2.AccountsRole.AccountType != "Admin" && account2.AccountsRole.AccountType != "Support")
                return PartialView("_notPermittedPartial");
            //////////////// main code start /////////////////////////////
            Teacher teacher=new Teacher();
            if (id != 0)
            {
                 teacher = db.Teachers.Find(id);
            }
            TempData["DepartmentId"] = new SelectList(_departmentManager.GetAllDepartmentFiltered(), "DepartmentId", "DepartmentName",teacher.DepartmentId);
            TempData["accountId"] = new SelectList(_accountManager.GetAllAccountFilteredForDrpDown(), "AccountId", "Name", teacher.LoginId);
            return PartialView("_AlterTeacher",teacher);
            ////////////////main code end////////////////////////////////
        }

 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public string AlterTeacherSubmit(Teacher teacher)
        {
            if (Session["UserObj"] == null)
                 return "Log in first.";
            var account = (Account)Session["UserObj"];
            if (account.AccountsRole.AccountType != "Admin" && account.AccountsRole.AccountType != "Support") return "Access Denied";
            
            //////////////// main code start /////////////////////////////
            string msg = "";
            try
            {
                if (ModelState.IsValid)
                {
                    var findAccount = db.Accounts.Find(teacher.AccountId);
                    teacher.LoginId = findAccount.LoginIdentity;
                    
                   
                    if (teacher.TeacherId == 0)
                    {
                        var findteacher =
                        _teacherManager.GetAllTeacherFiltered().Count(s => s.AccountId == findAccount.AccountId) != 0;
                        if (findteacher)
                        {
                            return "Teacher's profile already exists.";
                        }
                        db.Teachers.Add(teacher);
                    }
                    else
                    {
                        db.Entry(teacher).State = EntityState.Modified;
                    }
                    db.SaveChanges();
                    msg = "Teacher's Information Saved";
                }
            }
            catch (DbException)
            {

                msg = "Information can't be saved";
            }
            return msg;
            ////////////////main code end////////////////////////////////
        }

        // GET: /Teacher/Edit/5
       

        // GET: /Teacher/Delete/5
        public ActionResult Delete(int? id)
        {
            if (Session["Role"] != null && Session["User"] != null)
            {
                string role = Session["Role"].ToString();
                if (role == "Admin")
                {
                    //////////////// main code start /////////////////////////////
                    if (id == null)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    Teacher teacher = db.Teachers.Find(id);
                    if (teacher == null)
                    {
                        return HttpNotFound();
                    }
                    return View(teacher);
                    ////////////////main code end////////////////////////////////
                }
                return RedirectToAction("NotPermitted", "Home");
            }
            return RedirectToAction("LogInresultSubmit", "Home");

          
        }

        // POST: /Teacher/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var getAllSections = _sectionManager.GetAllSections().Where(s=>s.TeacherId==id);
            if (getAllSections.Any())
            {
                foreach (var section in db.Sections.Where(s=>s.TeacherId==id))
                {
                    section.TeacherId = null;
                    db.Entry(section).State = EntityState.Modified;
                }
                db.SaveChanges();
            }
            Teacher teacher = db.Teachers.Find(id);
            db.Teachers.Remove(teacher);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
