﻿using System;
using System.Linq;
using System.Web.Mvc;
using UMS_MVC.DataModel.Connection;

namespace UMS_MVC.Controllers.AccountTeacherMatters
{
    public class TeacherByController : Controller
    {
        private UmsDbContext db=new UmsDbContext();
        public ActionResult TeacherProfile()
        {
            String loginid = Session["User"].ToString();
                var teacherprofile = db.Teachers.Single(s => s.LoginId == loginid);
                return View(teacherprofile);
        }

        public ActionResult CourseTaken()
        {
            return View();
        }
    }
}