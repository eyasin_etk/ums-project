﻿using System.Web.Mvc;
using System.Web.Routing;

namespace UMS_MVC
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
           

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "LogInresultSubmit", id = UrlParameter.Optional },
                namespaces: new[] { "UMS_MVC.Controllers" }
            );
        }
    }
}
