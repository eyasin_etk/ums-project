﻿
angular.module('umspInitial').controller('postOnlineAdmissionController', [
    '$scope', '$routeParams', 'umsAdmissionService', 'postOnlineAdmService',
    function ($scope, $routeParams, umsAdmissionService, postOnlineAdmService) {
        function init() {
            $scope.msg = $routeParams.id;
        }

        var paramId = $routeParams.id;

        //$scope.generateIdMethod = function () {
        //    $scope.generatedId = {};
        //    umsAdmissionService.generateIdForUms(paramId).then(function (response) {
        //        $scope.generatedId = response.Data;
        //        //alert("Success");
        //    });
        //}
        $scope.getSpecificStudentPstAdmission= function() {
            $scope.specificPostStudent = {};
            umsAdmissionService.specificPostAdmStudent(paramId).then(function (response) {
                $scope.specificPostStudent = response.Data;
                Console.log(response.Data);
                //alert("Success");

            });
        }
        $scope.getSpecificStudentAcaInfo = function () {
            $scope.studentAca = {};
            umsAdmissionService.getspecificStudentAcaWithGuid(paramId).then(function (response) {
                if (response.IsSuccess) {
                    console.log(response.Data);
                    $scope.studentAca = response.Data;
                } else {
                    alert('No Data');
                }

            });
        }
        $scope.insertToUms= function() {
            console.log($scope.specificPostStudent);
            umsAdmissionService.insertOnlineStudentToUms($scope.specificPostStudent).then(function(response) {
                if (response.IsSuccess) {
                    $scope.onlineStudentToUms = {};
                    $scope.onlineStudentToUms = response.Data;
                    console.log(response.Data);
                    alert('Success');
                } else {
                    alert('No Data');
                }
            });
        }
        //$scope.back= function() {
        //    umsAdmissionService.getspecificStudentWithGuid(paramId).then(function (response) {
        //        $scope.student = {};
        //        if (response.IsSuccess) {
        //            $scope.student = response.Data;
        //        } else {
        //            alert('No Data');
        //        }

        //    });
        //}
        init();
        //$scope.generateIdMethod();
        $scope.getSpecificStudentPstAdmission();
        $scope.getSpecificStudentAcaInfo();
    }
]);