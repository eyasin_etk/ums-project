﻿


angular.module('umspInitial').controller('semesterController', [
    '$scope', 'semesterservice', function ($scope, semesterservice) {
        $scope.pageName = 'Semester Master';
       
        function init() {
            $scope.semesters = [];
            $scope.semester = {};
        }
        $scope.checkTerms= function() {
            var semObj = $scope.semester;
            if (semObj.MidTerm && semObj.FinalTerm) {
                semObj.MidTerm = false;
                semObj.FinalTerm = false;
                alert("Both term cann't declared at the same time.");
            }
        }
        $scope.getAllSemester = function () {
            semesterservice.getAllSemesters()
                .then(function (response) {
                    if (response.IsSuccess) {
                        $scope.semesters = response.Data;
                    } else {
                        console.log(response);
                    }
                    
                });
        }

        $scope.getSingle= function(id) {
            semesterservice.findSingleById(id)
                .then(function (response) {
                    console.log(id);
                    if (response.IsSuccess) {
                        $scope.semester = response.Data;
                        console.log(response.Data);
                        var gradeuploadDeadLine = response.Data.SpecialGradeuploadDeadLine;
                        if (gradeuploadDeadLine !== '' || gradeuploadDeadLine!==null) {
                            $scope.semester.SpecialGradeuploadDeadLine = new Date(gradeuploadDeadLine);
                        } 
                    } else {
                        console.log(response);
                    }
                   
                });
        }

        $scope.save= function() {
            semesterservice.save($scope.semester).then(function (response) {
                $scope.msgFromServer = response.Message;
               if (response.IsSuccess) {
                   $scope.semester = response.Data;
                   $scope.getAllSemester();
               } else {
                        console.log(response);
                    }
                   
                });
        }
        $scope.delete= function() {
            semesterservice.deleteSemester($scope.semester).then(function (response) {
                $scope.msgFromServer = response.Message;
                if (response.IsSuccess) {
                   $scope.semester = {};
                    $scope.getAllSemester();
                } else {
                    console.log(response.Data);
                }

            });
        }
        $scope.clear= function() {
            $scope.semester = {};
        }
        init();
        $scope.getAllSemester();
    }]);
