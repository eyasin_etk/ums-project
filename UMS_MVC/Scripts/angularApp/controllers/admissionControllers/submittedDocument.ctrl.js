﻿
angular.module('umspInitial').controller('submittedDocumentController', [
    '$scope', '$location', 'studentInitializer', 'submittedDocumentService' , 'imgUploadService', function ($scope, $location, studentInitializer, submittedDocumentService, imgUploadService) {

        $scope.pageName = "Attacthed Documents";
        $scope.btnOperationName = "Save";
        $scope.cnfmId = false;
        var initialObject = studentInitializer;

        var errorFunction = function (error) {
            console.log(error);
        }
        $scope.init = function () {
            console.log(initialObject);

            $scope.emptyStudentObj();
           
        }
        $scope.diplayStudentInitials = function () {
            $scope.displayGuidId = initialObject.StudentGuidId;
            $scope.displayIdentity = initialObject.StudentIdentificationId;
            $scope.displayId = initialObject.StudentId;
            $scope.studentPicture = initialObject.studentPicture;
            console.log("hi", $scope.studentPicture);
        }
        
        $scope.emptyStudentObj=  function(){
            $scope.studenDocumentObj = {
                StudentIdentificationId: initialObject.StudentIdentificationId,
                StudentId: initialObject.StudentId,
                DocumentAddingId:0,
                SscCertificate: false,
                SscMarkSheet: false,
                HscCertificate: false,
                HscMarkSheet: false,
                DiplomaCertificate: false,
                DiplomaMarkSheet: false,
                Gce5Olavel: false,
                Gce5Alavel: false,
                BaBsCertificate: false,
                BaBsMarkSheet: false,
                MaMsCertificate: false,
                MaMsMarkSheet: false,
                TwoLettersofRecommendation: false,
                TwoStampSizePhotoGraph: false,
                SscTestimonial: false,
                HscTestimonial: false
            };
        }
        function changeBtnName() {
            $scope.btnOperationName = "Update";
            $scope.cnfmId = true;
        }

        $scope.getSingeDocumentAddings= function() {
            submittedDocumentService.findSingleByStudentId(initialObject.StudentId)
                .then(function (response) {
                    $scope.emptyStudentObj();
                    if (response.IsSuccess) {
                        $scope.studenDocumentObj = response.Data;
                        $scope.studentPicture = $scope.studenDocumentObj.StudentId;
                        
                        changeBtnName();
                    } else {
                        console.log(response);
                    }
                },errorFunction());
        }
        $scope.saveNUpdate = function () {
            submittedDocumentService.addSubmittedDocuments($scope.studenDocumentObj).then(function (response) {
                $scope.serverMsg = response.Message;
                if (response.IsSuccess) {
                    $scope.emptyStudentObj();
                    console.log(response);
                    $scope.studenDocumentObj = response.Data;
                    changeBtnName();
                } else {
                    console.log(response);
                }
            }, errorFunction);
        }


        $scope.delete = function () {

            if (confirm("Are you sure?")) {
                var docuObj = $scope.studenDocumentObj;
                submittedDocumentService.deleteSubmittedDocument(docuObj.DocumentAddingId).then(function (response) {
                    $scope.serverMsg = response.Message;
                    if (response.IsSuccess) {
                        $scope.studenDocumentObj = response.Data;
                    } else {
                        console.log(response);
                    }

                }, errorFunction);

            }
        }


        $scope.unlock = function () {
            studentInitializer.StudentGuidId = undefined;
            studentInitializer.StudentId = '';
            studentInitializer.StudentIdentificationId = 0;
            $scope.diplayStudentInitials();
            console.log(studentInitializer);
            $location.path('/studentIdentity');
        }
        $scope.init();
        $scope.diplayStudentInitials();
        $scope.getSingeDocumentAddings();




        // for file upload  ***start here

        $scope.init = function () {
            if (initialObject.StudentIdentificationId !== 0) {
                // console.log(initialObject.StudentGuidId);
                 $scope.getImage();
                 $scope.file();
                
            } else {
                $scope.loadEmptyImage();
            }
            //if (initialObject.StudentId !== '') {
            //    $scope.getSingleStudentInfoByStudentId();
            //}
        }


        $scope.loadEmptyImage = function () {
            $scope.image_source =
                imgUploadService.getEmptyImageUrl();
        }



        $scope.getImage = function () {
            var stamp = new Date().getTime();
            console.log('Getting image');
            $scope.image_source = "";
            
            $scope.image_source = imgUploadService.getImageFinalUrl(initialObject.StudentId, 3, stamp);
            

           // console.log("Server returns " + $scope.image_source);
        }


        

        $scope.Downloadfile=function(){
            var stamp = new Date().getTime();
            $scope.image_source = "";

            $scope.image_source = imgUploadService.getImageFinalUrl(initialObject.studentPicture, 3, stamp);
        }



        $scope.file = function () {

            var stamp = new Date().getTime();
            $scope.check="000";
            $scope.check = imgUploadService.getfilecheck(initialObject.StudentId)
                           .then(function (response) {
                             //  var responseObj = response.data;
                               console.log("new get" + response.data.Message);
                               $scope.checkValue = response.data.Message;
                               //if (responseObj.IsSuccess) {
                               //    $scope.msgFromServ = responseObj.Message;
                               //    initialObject.studentPicture = responseObj.Data;
                               //} else {

                               //    console.log("Server returns " + responseObj.Message);
                               //    $scope.msgFromServ = responseObj.Message;
                               //}
                           });

        }




        $scope.uploadFile = function () {

            var file = $scope.myFile;
            var file1 = $scope.myFile.name;

            var index = file1.lastIndexOf(".");
            var strsubstring = file1.substring(index, file1.length);
            $scope.FileMessage = '';


              if (strsubstring == '.pdf') {
                  console.log('File Uploaded sucessfully');
                  console.log(strsubstring);




                  var submitBy = "";
                  var caption = initialObject.StudentIdentificationId;
                  imgUploadService.uploadFileToUmsUrl(submitBy, caption, file)
                      .then(function (response) {
                          var responseObj = response.data;
                          console.log(response);
                          if (responseObj.IsSuccess) {
                              $scope.msgFromServ = responseObj.Message;
                              initialObject.studentPicture = responseObj.Data;
                              $scope.file();
                          } else {

                              console.log("Server returns " + responseObj.Message);
                              $scope.msgFromServ = responseObj.Message;
                          }
                      });


              }

              else {
                  $scope.theFile = '';
                  $scope.FileMessage = 'only pdf file is allowed to upload';
              }


        }




        //$scope.downloadFile = function () {


        //}

        $scope.init();

    }]);


