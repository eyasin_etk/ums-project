﻿




angular.module('umspInitial').controller('uploadImageController', [
    '$scope', '$location', 'studentInitializer', 'imgUploadService', function ($scope, $location, studentInitializer, imgUploadService) {

        $scope.pageName = "Upload Image";
        var initialObject = studentInitializer;
        
         $scope.init = function () {
             if (initialObject.StudentIdentificationId !== 0) {
               // console.log(initialObject.StudentGuidId);
                $scope.getImage();
            } else {
                $scope.loadEmptyImage();
            }
             //if (initialObject.StudentId !== '') {
            //    $scope.getSingleStudentInfoByStudentId();
            //}
        }


        $scope.loadEmptyImage = function () {
            $scope.image_source =
                imgUploadService.getEmptyImageUrl();
        }
        $scope.getImage = function () {
            var stamp = new Date().getTime();
            console.log('Getting image');
            $scope.image_source = "";
            $scope.image_source = imgUploadService.getImageFinalUrl(initialObject.studentPicture,1, stamp);
        }
        $scope.uploadFile = function () {
            var file = $scope.myFile;
            var submitBy = "";
            var caption = initialObject.StudentIdentificationId;
            imgUploadService.uploadFileToUmsUrl(submitBy, caption, file)
                .then(function (response) {
                  var responseObj = response.data;
                    console.log(response);
                    if (responseObj.IsSuccess) {
                        $scope.msgFromServ = responseObj.Message;
                        initialObject.studentPicture = responseObj.Data;
                    } else {
                      
                        console.log("Server returns " + responseObj.Message);
                        $scope.msgFromServ = responseObj.Message;
                    }
                });

        }
        $scope.unlock = function () {
            studentInitializer.StudentGuidId = undefined;
            studentInitializer.StudentId = '';
            studentInitializer.StudentIdentificationId = 0;
            $scope.diplayStudentInitials();
            console.log(studentInitializer);
            $location.path('/studentIdentity');
        }

        $scope.init();
       
    }]);