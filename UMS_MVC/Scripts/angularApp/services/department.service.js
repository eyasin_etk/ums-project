﻿servApp.service('departmentservice', ['$resource', '$q', 'baseUrl',
    function ($resource, $q, baseUrl) {

        var getAllDepartments = function () {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'DepartmentService');
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }



        return {
            getAllDepartments: getAllDepartments
        };

    }]);