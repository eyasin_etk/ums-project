﻿servApp.service('onlineStudentsUmsInsertService', ['$resource', '$q', 'baseUrl',
    function ($resource, $q, baseUrl) {

        var specificPostAdmIdenStudent= function(id) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'FinalOnlineAdmService?id=' + id + "&type=" + 1);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        var specificPostAdmInfoStudent = function (id) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'FinalOnlineAdmService?id=' + id + "&type=" + 2);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        var getspecificStudentAcaWithGuid = function (id) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'FinalOnlineAdmService?id=' + id + "&type=" + 3);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        return {
            specificPostAdmIdenStudent: specificPostAdmIdenStudent,
            specificPostAdmInfoStudent:specificPostAdmInfoStudent,
            getspecificStudentAcaWithGuid: getspecificStudentAcaWithGuid
        };

    }]);