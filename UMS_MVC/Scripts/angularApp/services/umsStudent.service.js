﻿

servApp.service('umsStudentService', ['$resource', '$q', 'baseUrl',
    function ($resource, $q, baseUrl) {
        var outerUrl = baseUrl;
        baseUrl += "StudentIdentityService";

        var getAllStudent = function (pageObj, filterObj) {
            var defer = $q.defer();
            var url2 = baseUrl ;
            //var resource = $resource(url2 ,
            //{
            //    search: filterObj,
            //    sorting: pageObj
            //});
            var resource = $resource(url2,
         {
             search: filterObj,
             sorting: pageObj
         });
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }


        var getDataToEdit = function (acaId) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + '?id=' + null + '&acaId=' + acaId + "&type=" + false);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }

        var getPreFetchData = function (nameParam, idParam,type) {
            var defer = $q.defer();
            var resource = $resource(outerUrl + 'AdmNameIdFilter?nameParam=' + nameParam + "&idParam="+idParam+"&type="+type);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }

        var getImageUrl = function (id, type, stamp) {
            var uploadUrl = outerUrl + "ProfileImageTransferService";
            return uploadUrl + "?imageName=" + id + "&type=" + type + "&stamp=" + stamp;
        }

        return {
            getAllStudent: getAllStudent,
            getDataToEdit: getDataToEdit,
            getPreFetchData: getPreFetchData,
            getImageUrl: getImageUrl

        };

    }]);