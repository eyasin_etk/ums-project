﻿

servApp.service('studentCourseservice', ['$resource', '$q', 'baseUrl',
    function ($resource, $q, baseUrl) {

        var getAllCoursesForStudent = function (studentId) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'CourseForStudentUpdated?id=' + studentId);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }


        return {
            getAllCoursesForStudent: getAllCoursesForStudent
        };

    }]);