﻿servApp.service('studentRebateService', ['$resource', '$q', 'baseUrl',
    function ($resource, $q, baseUrl) {


        var workUrl = baseUrl + 'StudentRebateService';


        var getAllById = function (studentId) {
            var defer = $q.defer();
            var resource = $resource(workUrl + "?id=" + studentId + "&rebateId=" + 0);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }

        var saveRebateInfo = function (rebateObj) {
            var defer = $q.defer();
            var resource = $resource(workUrl);
            resource.save(rebateObj, function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }


        var updateRebateInfo = function (id) {
            var defer = $q.defer();
            var resource = $resource(workUrl + "?id=" + id);
            resource.save(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        var getSingleByInfoId = function (studentId,infoId) {
            var defer = $q.defer();
            var resource = $resource(workUrl + "?id=" + studentId + "&rebateId=" + infoId);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }

        var deleteStudentsRebateInfo = function (id) {
            var defer = $q.defer();
            var resource = $resource(workUrl + "?id=" + id);
            resource.delete(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }

        return {
            getAllById: getAllById,
            saveRebateInfo: saveRebateInfo,
            updateRebateInfo: updateRebateInfo,
            getSingleByInfoId: getSingleByInfoId,
            deleteStudentsRebateInfo:deleteStudentsRebateInfo,


        };

    }]);