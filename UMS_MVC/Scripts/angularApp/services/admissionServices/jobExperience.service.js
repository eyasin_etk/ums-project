﻿
servApp.service('jobExperienceService', ['$resource', '$q', 'baseUrl',
    function ($resource, $q, baseUrl) {
        var workUrl = baseUrl + 'StudentJobInfoService';

        

        var saveInfo = function (jobObj) {
            var defer = $q.defer();
            var resource = $resource(workUrl);
            resource.save(jobObj, function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        var updateInfo = function (id) {
            var defer = $q.defer();
            var resource = $resource(workUrl + "?id=" + id);
            resource.save(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }


        var getAllByInfoId = function (studentId) {
            var defer = $q.defer();
            var resource = $resource(workUrl + "?identificationId=" + studentId + "&id=" + 0 + "&fullType=" + true);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }


        var getSingleByInfoId = function (infoId) {
            var defer = $q.defer();
            var resource = $resource(workUrl + "?identificationId=" + 0 + "&id=" + infoId + "&fullType=" + false);
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }

        var deleteJobInfo = function (id) {
            var defer = $q.defer();
            var resource = $resource(workUrl + "?id=" + id);
            resource.delete(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }

        //var deleteStudentsAcaInfo = function (id) {
        //    var defer = $q.defer();
        //    var resource = $resource(workUrl + "?id=" + id);
        //    resource.delete(function (response) {
        //        defer.resolve(response);
        //    }, function (error) {
        //        return defer.reject(error);
        //    });
        //    return defer.promise;
        //}


        return {
          
            saveInfo: saveInfo,
            updateInfo: updateInfo,
            getAllByInfoId: getAllByInfoId,
            getSingleByInfoId: getSingleByInfoId,
            deleteJobInfo: deleteJobInfo

        };

    }]);