﻿

servApp.service('evaluationResultService', ['$resource', '$q', 'baseUrl',
    function ($resource, $q, baseUrl) {

        var getResultofEvaluationByTeacher = function (tId, semId) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'EvaluationResultService/CalculateEvaluationResult', { tId: tId, semId: semId });
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }
        var getResultofEvaluationBySection = function (secId) {
            var defer = $q.defer();
            var resource = $resource(baseUrl + 'EvaluationResultService/CalculateEveryEvaluation', { id: secId });
            resource.get(function (response) {
                defer.resolve(response);
            }, function (error) {
                return defer.reject(error);
            });
            return defer.promise;
        }


        return {
            getResultofEvaluationByTeacher: getResultofEvaluationByTeacher,
            getResultofEvaluationBySection: getResultofEvaluationBySection

        };

    }]);