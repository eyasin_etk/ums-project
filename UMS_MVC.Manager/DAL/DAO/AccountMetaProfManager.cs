﻿using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.AccountExt;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IAccount;
using UMS_MVC.Manager.DAL.DAO.Repository.RAccount;

namespace UMS_MVC.Manager.DAL.DAO
{
    public class AccountMetaProfManager
    {
        private readonly IAccountMetaProfessionRepository _accountMetaProfessionRepository;

        public AccountMetaProfManager()
        {
            var context=new UmsDbContext();
            context.Configuration.ProxyCreationEnabled = false;
            context.Configuration.LazyLoadingEnabled = false;
            _accountMetaProfessionRepository = new AccountMetaProfessionRepository(context);
        }

        public IQueryable<AccountMetaProfessional> GetAllAccountMetaProfessionals()
        {
            return _accountMetaProfessionRepository.GetAllAccountMetaInformationRepositories()
                .Include(s=>s.AccountExtEducations)
                .Include(s=>s.AccountExtProjects)
                .Include(s=>s.AccountExtProfessionalActivities)
                .Include(s=>s.AccountExtPublications);
        }

        public AccountMetaProfessional GetSingleAccountMetaProfessional(int id)
        {
            return _accountMetaProfessionRepository.GetSingleAccountMetaInformationRepository(id);
        }

        public AccountMetaProfessional GetAccountMetaProfFnD(int id)
        {
          return  GetAllAccountMetaProfessionals()
                .Where(s => s.AccountId == id)
                .AsEnumerable()
                .Select(s => new AccountMetaProfessional
                {
                   AccountMetaProfessionalId =s.AccountMetaProfessionalId,
                   PhdSuperVision = s.PhdSuperVision,

                   AccountExtEducations = s.AccountExtEducations.AsEnumerable().Select(i=>new AccountExtEducation
                   {
                       AccountExtEducationId = i.AccountExtEducationId,
                       NameOfExamination = i.NameOfExamination
                   }).ToList(),
                   AccountExtProfessionalActivities = s.AccountExtProfessionalActivities.AsEnumerable().Select(i=>new AccountExtProfessionalActivity
                   {
                       AccountExtProfessionalActivityId = i.AccountExtProfessionalActivityId,
                       AttendType = i.AttendType
                   }).ToList(),
                   AccountExtProjects = s.AccountExtProjects.AsEnumerable().Select(d=>new AccountExtProject
                   {
                       AccountExtProjectId = d.AccountExtProjectId,
                       ProjectTitle = d.ProjectTitle
                   }).ToList(),
                   AccountExtPublications = s.AccountExtPublications.AsEnumerable().Select(f=>new AccountExtPublication
                   {
                       AccountExtPublicationId = f.AccountExtPublicationId,
                       Subject = f.Subject
                   }).ToList()
                })
                .ToList().FirstOrDefault();
        }

        public void Insert(AccountMetaProfessional accountMetaProfessional)
        {
             _accountMetaProfessionRepository.Insert(accountMetaProfessional);
        }

        public void Update(AccountMetaProfessional accountMetaProfessional)
        {
            _accountMetaProfessionRepository.Update(accountMetaProfessional);
        }
        public string SaveData()
        {
            string msg;
            try
            {
                _accountMetaProfessionRepository.Save();
                msg = "Success";
            }
            catch (DbException)
            {

                msg = "Failed";
            }
            return msg;
        }
        public string Delete(int id)
        {
            
            string msg;
            try
            {
                _accountMetaProfessionRepository.Delete(id);
                msg = "Success";
            }
            catch (DbException)
            {

                msg = "Failed";
            }
            return msg;
        }
    }
}