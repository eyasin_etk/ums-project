﻿using System.Data.Common;
using System.Linq;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.AccountExt;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IAccount;
using UMS_MVC.Manager.DAL.DAO.Repository.RAccount;

namespace UMS_MVC.Manager.DAL.DAO
{
    public class AccountExtPublicationManager
    {
        private readonly IAccountExtPublicationRepository _accountExtPublicationRepository;

        public AccountExtPublicationManager()
        {
            _accountExtPublicationRepository=new AccountExtPublicationRepository(new UmsDbContext());
        }

        public IQueryable<AccountExtPublication> GetAllAccountExtPublications()
        {
            return _accountExtPublicationRepository.GetAllAccountExtPublications();
        }

        public AccountExtPublication GetSinglExtPublication(int id)
        {
            return _accountExtPublicationRepository.GetSingle(id);
        }

        public void Insert(AccountExtPublication publication)
        {
            _accountExtPublicationRepository.Insert(publication);
        }
        public void Update(AccountExtPublication publication)
        {
            _accountExtPublicationRepository.Update(publication);
        }
        public string SaveData()
        {
            string msg;
            try
            {
                _accountExtPublicationRepository.Save();
                msg = "Success";
            }
            catch (DbException)
            {

                msg = "Failed";
            }
            return msg;
        }
        public string Delete(int id)
        {

            string msg;
            try
            {
                _accountExtPublicationRepository.Delete(id);
                msg = "Success";
            }
            catch (DbException)
            {

                msg = "Failed";
            }
            return msg;
        }
    }
}