﻿using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.DataModel.Models.UniversalTypes;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.Manager.DAL.DAO
{
    public class SemesterManager
    {
        private UmsDbContext db;
        
        public SemesterManager()
        {
            db=new UmsDbContext();
        }

        public IQueryable<Semester> GetAllSemestersDefault()
        {
            return db.Semesters;
        }

        public IQueryable<Semester> GetAllSemesterFiltered()
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.Configuration.LazyLoadingEnabled = false;

            return db.Semesters;
        }

        public IQueryable<Semester> GetAllSemester()
        {
            return GetAllSemestersDefault().OrderByDescending(s => s.SemesterId);
        }

        public List<SemesterDropDownViewModel> GetSemesterDropDownUpdated()
        {
            var semesters = GetAllSemesterFiltered().AsEnumerable().Select(s => new SemesterDropDownViewModel
            {
                SemesterId = s.SemesterId,
                BatchNo = s.BatchNo,
                ActiveSemester = s.ActiveSemester,
                SemesterNYear = s.SemesterNYear,

            }).ToList();
            return semesters;
        }

        public List<Semester> GetAllSemesterForDropdown()
        {
            //db.Configuration.ProxyCreationEnabled = false;
            //db.Configuration.LazyLoadingEnabled = false;
            var semesters = GetAllSemesterFiltered().AsEnumerable().Select(s => new Semester
            {
                SemesterId = s.SemesterId,
                BatchNo = s.BatchNo,
                ActiveSemester = s.ActiveSemester,
                SemesterNYear = s.SemesterNYear,
                
            }).ToList();
            return semesters;
        }


        public Semester GetActiveSemester()
        {
            return GetAllSemestersDefault().Single(s => s.ActiveSemester);
        }

        public Semester GetActiveSemesterFiltered()
        {
            return GetAllSemesterFiltered().Single(s => s.ActiveSemester);
        }

        public Semester GetSingleSemesterFiltered(int semesterId)
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.Configuration.LazyLoadingEnabled = false;
            return db.Semesters.Find(semesterId);
        }

        public Semester GetSingleSemester(int semesterId)
        {
            return db.Semesters.Find(semesterId);
        }
        public Semester GetSingleSemesterNullable(int? semesterId)
        {
            return db.Semesters.Find(semesterId);
        }
        public Semester GetSepecificSemester(string sem)
        {
            return GetAllSemestersDefault().FirstOrDefault(s => s.SemesterNYear == sem);
        }

        //public Semester GetSemesterForDeptWiseCourseAdvising()
        //{
        //    return GetAllSemestersDefault().FirstOrDefault(s => s.DepartmentId !=null);
        //}

        public Semester GetAdvisingActivatedSemester()
        {
            var getAdvisingSemester=GetAllSemester().FirstOrDefault(s => s.CourseAdvising);
            return getAdvisingSemester;
        }

        ////year..........
        public IQueryable<Year> GetallYears()
        {
         return  db.Years;
        }

        public IQueryable<SerializedSemester> GetAllSerializedSemesters()
        {
            return db.SerializedSemesters;
        }

        public bool UpdateSemester(Semester semester)
        {
            try
            {
                db.Entry(semester).State = EntityState.Modified;
                
               return true;
            }
            catch (DbException)
            {
                return false;
            }
        }

        public void SaveAll()
        {
            db.SaveChanges();
        }


        //private bool disposed = false;
        //protected virtual void Dispose(bool disposing)
        //{
        //    if (disposed)
        //        return;

        //    if (disposing)
        //    {
        //        //Free any other managed objects here. 
        //        db.Dispose();
        //    }

        //    // Free any unmanaged objects here. 
        //    disposed = true;
        //}
        //public void Dispose()
        //{
        //    Dispose(true);
        //    GC.SuppressFinalize(this);
        //}

    }
}