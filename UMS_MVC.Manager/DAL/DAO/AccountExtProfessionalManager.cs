﻿using System.Data.Common;
using System.Linq;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.AccountExt;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IAccount;
using UMS_MVC.Manager.DAL.DAO.Repository.RAccount;

namespace UMS_MVC.Manager.DAL.DAO
{
    public class AccountExtProfessionalManager
    {
        private readonly IAccountExtProfessionalActivityRepository _accountExtProfessionalActivityRepository;

        public AccountExtProfessionalManager()
        {
            _accountExtProfessionalActivityRepository=new AccountExtProfessionalActivityRepository(new UmsDbContext());
        }

        public IQueryable<AccountExtProfessionalActivity> GetAllAccountExtProfessionalActivity()
        {
            return _accountExtProfessionalActivityRepository.GetAllAccountExtProfessionalActivities();
        }

        public AccountExtProfessionalActivity GetSinglExtProfessionalActivity(int id)
        {
            return _accountExtProfessionalActivityRepository.GetSingleAccountExtProfessionalActivity(id);
        }

        public void Insert(AccountExtProfessionalActivity accountExtProfessionalActivity)
        {
            _accountExtProfessionalActivityRepository.Insert(accountExtProfessionalActivity);
        }
        public void Update(AccountExtProfessionalActivity accountExtProfessionalActivity)
        {
            _accountExtProfessionalActivityRepository.Update(accountExtProfessionalActivity);
        }
        public string SaveData()
        {
            string msg;
            try
            {
                _accountExtProfessionalActivityRepository.Save();
                msg = "Success";
            }
            catch (DbException)
            {

                msg = "Failed";
            }
            return msg;
        }
        public string Delete(int id)
        {

            string msg;
            try
            {
                _accountExtProfessionalActivityRepository.Delete(id);
                msg = "Success";
            }
            catch (DbException)
            {

                msg = "Failed";
            }
            return msg;
        }
    }
}