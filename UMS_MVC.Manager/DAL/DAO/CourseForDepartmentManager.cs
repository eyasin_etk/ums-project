﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.Manager.DAL.DAO
{
    public class CourseForDepartmentManager
    {
        private UmsDbContext db;

        public CourseForDepartmentManager()
        {
            db = new UmsDbContext();
        }

        public CourseForDepartment GetSingleCourseForDepartment(int id)
        {
            return db.CourseForDepartments.Find(id);
        }

        public IQueryable<CourseForDepartment> GetAllCourseForDepartments()
        {
            return db.CourseForDepartments;
        }

        public IQueryable<CourseForDepartment> GetAllCourseForDepartmentFiltered()
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.Configuration.LazyLoadingEnabled = false;
            return db.CourseForDepartments;
        }

        public CourseForDepartment GetSingleCourseFiltered(int id)
        {
            return GetAllCourseForDepartmentFiltered().FirstOrDefault(s => s.CourseForDepartmentId==id);
        }

        public IQueryable<CourseForDepartment> GetAllCourseForDepartmentsByAscOrder()
        {
            return GetAllCourseForDepartments().OrderBy(s => s.CourseForDepartmentId);
        }

        public IQueryable<CourseForDepartment> GetAllCourseForDepartmentsByDepartment(int departmentId)
        {
            return GetAllCourseForDepartments().Where(s => s.DepartmentId == departmentId);
        }

        public IQueryable<CourseForDepartment> GetCourseforDeptBydeptIdFiltered(int id)
        {
            return GetAllCourseForDepartmentFiltered().Where(s => s.DepartmentId == id).Include(s=>s.SerializedSemester);
        }

        public List<CourseForDepartment> GetAllCourseForDepartmentsWithCode(int departmentId)
        {
            var getCourseList =
                GetCourseforDeptBydeptIdFiltered(departmentId)
                    .AsEnumerable()
                    .Select(s => new CourseForDepartment
                    {
                        CourseForDepartmentId = s.CourseForDepartmentId,
                        CourseName = "(" + s.CourseCode + ") " + s.CourseName,
                        SerializedSemester = new SerializedSemester
                        {
                            SerializedSemesterId = s.SerializedSemesterId,
                            SemesterName = s.SerializedSemester.SemesterName
                        },
                        SerializedSemesterId = s.SerializedSemesterId
                    })
                    .ToList();

            return getCourseList;
        }

        public IQueryable<CourseForDepartment> GetAllCourseForDepartmentByCourseCode(int courseCode)
        {
            return GetAllCourseForDepartments().Where(s => s.CourseForDepartmentId == courseCode);
        }

    }
}