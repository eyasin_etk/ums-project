﻿using System.Linq;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Notice_Matter;
using UMS_MVC.Manager.DAL.DAO.Intefaces;
using UMS_MVC.Manager.DAL.DAO.Repository;

namespace UMS_MVC.Manager.DAL.DAO
{
    public class NoticeBoardDetailsManager
    {
       private readonly INoticeBoardDetailsRepository _detailsRepository;

        public NoticeBoardDetailsManager()
        {
           
            _detailsRepository = new NoticeBoardDetailsRepository();

        }

        public IQueryable<NoticeBoardDetail> GetAllDetails()
        {
            return _detailsRepository.GetAll();
        }

        public NoticeBoardDetail GetSingleDetail(int id)
        {
            return _detailsRepository.FindSingle(id);
        }

        public int InsertUpdate(NoticeBoardDetail detail)
        {
            if (detail.NoticeBoardDetailId != 0)
            {
                _detailsRepository.Update(detail);
            }
            else
            {
                _detailsRepository.Add(detail);
            }
            _detailsRepository.Save();
            return detail.NoticeBoardDetailId;

        }

        public void Update(NoticeBoardDetail detail)
        {
            _detailsRepository.Update(detail);
        }

        public bool Delete(int id)
        {
            var boardDetail = _detailsRepository.FindSingle(id);
            if (boardDetail != null)
            {
                _detailsRepository.Delete(boardDetail);
                _detailsRepository.Save();
                return true;
            }
            return false;

        }

        public void Save()
        {
            _detailsRepository.Save();
        }
    }
}