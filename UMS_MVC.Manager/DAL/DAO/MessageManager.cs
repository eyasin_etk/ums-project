﻿using System.Linq;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.Manager.DAL.DAO
{
    public class MessageManager
    {
        private UmsDbContext _dbContext;
        public MessageManager()
        {
           _dbContext=new UmsDbContext();
        }

        public IQueryable<InternalMessage> GetAllInternalMessages()
        {
            return _dbContext.InternalMessages.OrderByDescending(s=>s.InternalMessageId);
        }
        
        public InternalMessage GetSingleMessage(int id)
        {
            return _dbContext.InternalMessages.Find(id);
        }

        public IQueryable<InternalMessage> GetSentMessagesForSingleUser(int accountId = 0)
        {
            return GetAllInternalMessages().Where(s => s.SenderAccountId == accountId && s.SenderDeleteStatus == false).Take(500);
        }

        public IQueryable<InternalMessage> GetReceivedMessageForSingleUser(int accountId = 0)
        {
            return GetAllInternalMessages().Where(s => s.ReceiverAccountId == accountId && s.ReceiverDeleteStatus == false).Take(500);
        }

    }
}