﻿using System.Linq;
using System.Net.NetworkInformation;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.DataModel.Models.UniversalTypes;

namespace UMS_MVC.Manager.DAL.DAO
{
    public class UserActivityManager
    {
        private UmsDbContext _db;

        public UserActivityManager()
        {
            _db=new UmsDbContext();
        }
        public IQueryable<UserActivityType> GetAllUserActivityTypes()
        {
            return _db.UserActivityTypes;
        }
        public IQueryable<UserActivity> GetAllUserActivity()
        {
            return _db.UserActivities;
        }

        public UserActivity GetSingleUserActivity(int activityId)
        {
            return _db.UserActivities.Find(activityId);
        }

        public void DeleteUserActivity(int activityId)
        {
            var getActivity = _db.UserActivities.Find(activityId);
            _db.UserActivities.Remove(getActivity);
            _db.SaveChanges();
        }

       
       
        public string GetMacAddress()
        {
            const int MIN_MAC_ADDR_LENGTH = 12;
            string macAddress = string.Empty;
            long maxSpeed = -1;

            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                string tempMac = nic.GetPhysicalAddress().ToString();
                if (nic.Speed > maxSpeed &&
                    !string.IsNullOrEmpty(tempMac) &&
                    tempMac.Length >= MIN_MAC_ADDR_LENGTH)
                {
                    maxSpeed = nic.Speed;
                    macAddress = tempMac;
                }
            }
            return macAddress;
        }

      

        public void InsertActivity(UserActivity userActivity)
        {
            //userActivity.ComputerIp = ClassifiedServices.
            _db.UserActivities.Add(userActivity);
            _db.SaveChanges();
        }

    }
}