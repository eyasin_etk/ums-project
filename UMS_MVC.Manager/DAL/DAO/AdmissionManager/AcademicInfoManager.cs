﻿using System;
using System.Data.Common;
using System.Linq;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IAdmission;
using UMS_MVC.Manager.DAL.DAO.Repository.RAdmission;

namespace UMS_MVC.Manager.DAL.DAO.AdmissionManager
{
   public class AcademicInfoManager
    {
        private readonly IAcademicInfoRepository _academicInfo;
       private readonly StudentManager _studentManager;
        public AcademicInfoManager()
        {
            _academicInfo = new AcademicInfoRepository();
            _studentManager=new StudentManager();
        }

        public StudentAcademicInfo GetSingle(int id)
        {
            return _academicInfo.FindSingle(id);
        }
        public IQueryable<StudentAcademicInfo> GetAll()
       {
            return _academicInfo.GetAll();
       }


       public void Insert(StudentAcademicInfo acaInfo)
        {
           
            if (acaInfo.StudentId != null)
            {
                _academicInfo.Add(acaInfo);
            }
        }

        public void Update(StudentAcademicInfo acaInfo)
        {
            var check = _studentManager.GetAllStudentInfos().Count(s => s.StudentId == acaInfo.StudentId) == 1;
            if (check && acaInfo.StudentId != null)
            {
                _academicInfo.Update(acaInfo);
            }
            
        }
        public string Delete(int id)
        {
            string msg;
            try
            {
                var studeAcaInfo = _academicInfo.FindSingle(id);
                _academicInfo.Delete(studeAcaInfo);
                _academicInfo.Save();
                msg = "Success";
            }
            catch (Exception)
            {

                msg = "Failed";
            }
            return msg;
        }
        public string SaveData()
        {
            string msg;
            try
            {
                _academicInfo.Save();
                msg = "Success";
            }
            catch (DbException)
            {

                msg = "Failed";
            }
            return msg;
        }
    }
}
