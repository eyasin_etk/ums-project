﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IAdmission;
using UMS_MVC.Manager.DAL.DAO.Repository.RAdmission;

namespace UMS_MVC.Manager.DAL.DAO.AdmissionManager
{
    public class StudentJobInfoManager
    {
        private readonly IStudentJobInfoRepository _studentJobInfo;
        public readonly StudentManager _studentManager;
        public readonly StudentIdentityManager _studentIdentificationManager;
        public StudentJobInfoManager()
        {
            _studentJobInfo = new StudentJobInfoRepository();
            _studentManager = new StudentManager();
        }


        public StudentAccountExtProfessionalActivitys GetSingle(int id)
        {
            return _studentJobInfo.FindSingle(id);
        }

        public IQueryable<StudentAccountExtProfessionalActivitys> GetAll()
        {
            return _studentJobInfo.GetAll();
        }

        public void Insert(StudentAccountExtProfessionalActivitys info)
        {

            if (info.AccountId != null)
            {
                _studentJobInfo.Add(info);
            }
        }


        public void Update(StudentAccountExtProfessionalActivitys info)
        {
            var check = _studentJobInfo.GetAll().Count(xx => xx.AccountId == info.AccountId) == 1;
            if (check && info.AccountId != null)
            {
                _studentJobInfo.Update(info);
            }

        }


        public string Delete(int id)
        {
            string msg;
            try
            {
                var stdJobInfo = _studentJobInfo.FindSingle(id);
                _studentJobInfo.Delete(stdJobInfo);
                _studentJobInfo.Save();
                msg = "Success";
            }
            catch (Exception)
            {

                msg = "Failed";
            }
            return msg;
        }
        public string SaveData()
        {
            string msg;
            try
            {
                _studentJobInfo.Save();
                msg = "Success";
            }
            catch (DbException)
            {

                msg = "Failed";
            }
            return msg;
        }


    }
}
