﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IAdmission;
using UMS_MVC.Manager.DAL.DAO.Repository.RAdmission;

namespace UMS_MVC.Manager.DAL.DAO.AdmissionManager
{
    public class StudentRebateManager
    {
        private readonly IStudentRebateRepository _studentRebate;
        private readonly StudentManager _studentManager;
        private readonly SemesterManager _semesterManager;

        public StudentRebateManager()
        {
            _studentRebate = new StudentRebateRepository();
            _studentManager = new StudentManager();
            _semesterManager = new SemesterManager();
        }


        public Semester GetSingleActiveSemester()
        {
            return _semesterManager.GetActiveSemesterFiltered();
        }

        public Rebate GetSingle(int id)
        {
            return _studentRebate.FindSingle(id);
        }
        public IQueryable<Rebate> GetAll()
        {
            return _studentRebate.GetAll();
        }


        public void Insert(Rebate rebate)
        {

            if (rebate.StudentIdentificationId > 0)
            {
                _studentRebate.Add(rebate);
            }
        }

        public void Update(Rebate rebate)
        {
            var check = _studentManager.GetAllStudentIdentifications().Count(s => s.StudentIdentificationId == rebate.StudentIdentificationId) == 1;
            if (check && rebate.RebateCategoryId > 0)
            {
                _studentRebate.Update(rebate);
            }

        }
        public string Delete(int id)
        {
            string msg;
            try
            {
                var rebateInfo = _studentRebate.FindSingle(id);
                _studentRebate.Delete(rebateInfo);
                _studentRebate.Save();
                msg = "Success";
            }
            catch (Exception)
            {

                msg = "Failed";
            }
            return msg;
        }
        public string SaveData()
        {
            string msg;
            try
            {
                _studentRebate.Save();
                msg = "Success";
            }
            catch (DbException)
            {

                msg = "Failed";
            }
            return msg;
        }

    }
}
