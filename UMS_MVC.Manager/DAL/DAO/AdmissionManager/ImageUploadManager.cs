﻿using System;
using System.IO;
using System.Web;
using System.Web.Helpers;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.BLL;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IAdmission;
using UMS_MVC.Manager.DAL.DAO.Repository.RAdmission;
//using static System.Net.WebRequestMethods;

namespace UMS_MVC.Manager.DAL.DAO.AdmissionManager
{
    public class ImageUploadManager
    {
        private readonly IStudentIdentityRepository _studentIdentityRepository;

        public ImageUploadManager()
        {
            _studentIdentityRepository = new StudentIdentityRepository();
        }

        public string SaveStudentImage(int studentId, HttpPostedFile img, string fileExte)
        {
            try
            {
                //if (img.Height > 450)
                //    img.Resize(350, 450);
                StudentIdentification identification = _studentIdentityRepository.FindSingle(studentId);
                string makeFileName = identification.StudentId + fileExte;
                if (fileExte == ".pdf")
                {
                    string filelocation1 = FileCheckingManager.UmsFileLocation();
                    // var kk = img;
                    img.SaveAs(filelocation1 + makeFileName);
                   // identification.StudentInfofile = makeFileName;
                   // _studentIdentityRepository.Update(identification);
                   // _studentIdentityRepository.Save();
                    return makeFileName;
                }
                string filelocation = FileCheckingManager.UmsImageLocation();
                // var kk = img;
                img.SaveAs(filelocation+makeFileName);
                identification.StudentPicture = makeFileName;
                _studentIdentityRepository.Update(identification);
                _studentIdentityRepository.Save();
                return makeFileName;
            }
            catch (Exception e)
            {
                return "";
            }
        }

        public void ImageTransfer(string imageName, int studentIdentityId) // Guid type Image name
        {

            string sourcePath = FileCheckingManager.OnlineAdmissionFileLocation() + imageName;
            var fileExte = Path.GetExtension(sourcePath);
            var identification = _studentIdentityRepository.FindSingle(studentIdentityId);
            var newImageName = identification.StudentId + fileExte;
            string desinationPath = FileCheckingManager.UmsImageLocation() + newImageName;
            //string file = Path.GetFileNameWithoutExtension(sourcePath);

            if (File.Exists(sourcePath))
            {
                //File.Move(sourcePath, desinationPath);
                File.Copy(sourcePath, desinationPath);
                identification.StudentPicture = newImageName;
                _studentIdentityRepository.Save();
            }

        }

    }
}
