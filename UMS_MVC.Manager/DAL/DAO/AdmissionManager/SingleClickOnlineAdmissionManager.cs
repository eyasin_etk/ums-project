﻿using System;
using System.Linq;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO.Intefaces;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IAdmission;
using UMS_MVC.Manager.DAL.DAO.Repository;
using UMS_MVC.Manager.DAL.DAO.Repository.RAdmission;

namespace UMS_MVC.Manager.DAL.DAO.AdmissionManager
{
    public class SingleClickOnlineAdmissionManager
    {
        private readonly StudentIdentityManager _identityManager;
        private readonly IPreAdmissionStudentInfoRepository _infoRepository;
        private readonly IPreAdmissionAcademicRepository _academicRepository;
        //private readonly 
        private readonly IStudentIdentityRepository _studentIdentityRepository;
        private readonly IPersonalInfoRepository _personalInfoRepository;
        private readonly IAcademicInfoRepository _academicInfo;
        private readonly ImageUploadManager _imageUploadManager;


        public SingleClickOnlineAdmissionManager()
        {
            _identityManager = new StudentIdentityManager();
            _infoRepository = new PreAdmissionStudentInfoRepository();
            _academicRepository = new PreAdmissionAcademicRepository();
            _studentIdentityRepository = new StudentIdentityRepository();
            _personalInfoRepository = new PersonalInfoRepository();
            _academicInfo = new AcademicInfoRepository();
            _imageUploadManager=new ImageUploadManager();
        }

        public string InsertStudentInfo(Guid? studentGuid, StudentIdentification studentObj)
        {
            var studentInfoObj = _infoRepository.FinSingleGuid(studentGuid);


            if (studentInfoObj != null)
            {

                StudentInfo info = new StudentInfo
                {
                    StudentId = studentObj.StudentId,
                    StudentName = studentInfoObj.StudentName,
                    FathersName = studentInfoObj.FathersName,
                    MothersName = studentInfoObj.MothersName,
                    PhoneNo = studentInfoObj.PersonalContactNo,
                    MobileNo = studentInfoObj.PhoneNoSecond,
                    EmailAddress = studentInfoObj.EmailAddress,
                    BloodGroupsId = studentInfoObj.BloodGroupsId,
                    GenderId = studentInfoObj.GenderId,
                    MaritalStatusId = studentInfoObj.MaritalStatusId,
                    DateOfBirth = studentInfoObj.DateOfBirth,
                    SkillInOtherfield = studentInfoObj.SkillInOtherfield,
                    Nationality = studentInfoObj.Nationality,
                    PresentAddress = studentInfoObj.PresentAddress,
                    PresentDistrict = studentInfoObj.PresentDistrict,
                    PresentPostalCode = studentInfoObj.PresentPostalCode,
                    ParmanentAddress = studentInfoObj.ParmanentAddress,
                    ParmanentDistrict = studentInfoObj.ParmanentDistrict,
                    ParmanentPostalCode = studentInfoObj.ParmanentPostalCode,
                    LocalGuardianName = studentInfoObj.LocalGuardianName,
                    LocalGuardianAddress = studentInfoObj.LocalGuardianAddress,
                    LocalGuardianContact = studentInfoObj.LocalGuardianContact,
                    LocalGuardianRelationship = studentInfoObj.LocalGuardianRelationship
                };

                _personalInfoRepository.Add(info);
                _personalInfoRepository.Save();
                _studentIdentityRepository.StudentIdSync(info.StudentInfoId, studentObj.StudentIdentificationId);
                _studentIdentityRepository.Save();
                studentInfoObj.AdmissionOfficeConfirmed = true;
                _infoRepository.Save();
                if (!string.IsNullOrEmpty(studentInfoObj.StudentImage))
                {
                    _imageUploadManager.ImageTransfer(studentInfoObj.StudentImage, studentObj.StudentIdentificationId);

                }
                return "successfully";
            }
            else
            {
                return "failed";
            }

        }
        

        public string InsertAcademics(Guid? studentGuid, string createdId)
        {
            try
            {
                var academic =
                    _academicRepository.GetAll().Where(s => s.PreAdmissionStudentInfoId == studentGuid).ToList();
                if (academic.Any())
                {
                    foreach (var a in academic)
                    {
                        var aca = new StudentAcademicInfo
                        {
                            StudentId = createdId,
                            Group = a.Group,
                            NameOfExamination = a.NameOfExamination,
                            PassingYear = a.PassingYear,
                            Result = a.Result,
                            StartingSession = a.StartingSession,
                            UniversityBoard = a.UniversityBoard
                           
                        };
                        _academicInfo.Add(aca);

                    }
                    _academicInfo.Save();
                    return "successfully";
                }
                else
                {
                    return "no record";
                }
               
            }
            catch (Exception)
            {

                return "failed";
            }
        }
        public StudentIdentification InsertUpdateIdentity(StudentIdentification identification, bool type)
        {
            try
            {
                var studentIdentification = new StudentIdentification();

                var shortDateString = DateTime.Now.ToShortDateString();
                switch (type)
                {
                    case true:
                        var createdId = _identityManager.GetGeneratedId(identification.DepartmentId, identification.SemesterId);
                        string password = _identityManager.GeneratePassword(3);
                        studentIdentification.SchoolId = 5;
                        studentIdentification.DepartmentId = identification.DepartmentId;
                        studentIdentification.SemesterInfoId = 4;
                        studentIdentification.SemesterAndYear = "";
                        studentIdentification.Validation = true;
                        studentIdentification.SemesterId = identification.SemesterId;
                        studentIdentification.AddedDate = DateTime.Now;
                        studentIdentification.CreditTransfer = identification.CreditTransfer;
                        studentIdentification.DiplomaStudent = identification.DiplomaStudent;
                        studentIdentification.Remark = identification.Remark;
                        studentIdentification.StudentId = createdId;
                        studentIdentification.Password = password;
                        studentIdentification.EntryBy = "";
                        studentIdentification.StudentGroupId = 1;
                        studentIdentification.StudentGuid = identification.StudentGuid;
                        studentIdentification.Batch = identification.Batch;
                        studentIdentification.RegularStudent = identification.RegularStudent;
                        studentIdentification.FridayStudent = identification.FridayStudent;
                        studentIdentification.EveningStudent = identification.EveningStudent;
                       
                        studentIdentification.EntryBy = identification.EntryBy+"_"+shortDateString;
                        _studentIdentityRepository.Add(studentIdentification);
                       
                        break;
                    case false:
                        studentIdentification = _studentIdentityRepository.FindSingle(identification.StudentIdentificationId);
                        studentIdentification.CreditTransfer = identification.CreditTransfer;
                        studentIdentification.DiplomaStudent = identification.DiplomaStudent;
                        studentIdentification.Remark = identification.Remark;
                        studentIdentification.EntryBy +=" & updated by "+identification.EntryBy + "_" + shortDateString;
                        _studentIdentityRepository.Update(studentIdentification);
                        break;
                }
                _studentIdentityRepository.Save();
                return studentIdentification;
            }
            catch (Exception e)
            {

                return null;
            }

        }

        
    }

}
