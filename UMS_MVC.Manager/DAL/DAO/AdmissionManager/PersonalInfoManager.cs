﻿using System;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IAdmission;
using UMS_MVC.Manager.DAL.DAO.Repository.RAdmission;

namespace UMS_MVC.Manager.DAL.DAO.AdmissionManager
{
    public class PersonalInfoManager
    {
        private readonly IStudentIdentityRepository _identityRepository;
        private readonly IPersonalInfoRepository _personalInfoRepository;
        private readonly StudentManager _studentManager = new StudentManager();

        public PersonalInfoManager()
        {
            var context = new UmsDbContext();
            context.Configuration.ProxyCreationEnabled = false;
            context.Configuration.LazyLoadingEnabled = false;
            _personalInfoRepository = new PersonalInfoRepository();
            _identityRepository = new StudentIdentityRepository();
        }
        public StudentInfo GetSingle(int id)
        {
            return _personalInfoRepository.FindSingle(id);
        }
        public void Insert(StudentInfo info)
        {
            var check = _studentManager.GetAllStudentInfos().Count(s => s.StudentId == info.StudentId) == 0;
            var idenId = _identityRepository.GetAll().AsNoTracking().FirstOrDefault(s => s.StudentId == info.StudentId);
            if (check && info.StudentId != null)
            {
                _personalInfoRepository.Add(info);
                _personalInfoRepository.Save();
                _identityRepository.StudentIdSync(info.StudentInfoId, idenId.StudentIdentificationId);
                _identityRepository.Save();
            }
        }

        public void Update(StudentInfo info)
        {
            var check = _studentManager.GetAllStudentInfos().Count(s => s.StudentId == info.StudentId) != 0;
            var idenId = _identityRepository.GetAll().AsNoTracking().FirstOrDefault(s => s.StudentId == info.StudentId);
            if (check && info.StudentId != null)
            {
                _personalInfoRepository.Update(info);
                _personalInfoRepository.Save();
                _identityRepository.StudentIdSync(info.StudentInfoId, idenId.StudentIdentificationId);
                _identityRepository.Save();
            }

        }
        public string Delete(int id)
        {
            string msg;
            try
            {
                var studepersonalInfo = _studentManager.GetSingleStudentInfo(id);
                _personalInfoRepository.Delete(studepersonalInfo);
                _personalInfoRepository.Save();
                msg = "Success";
            }
            catch (Exception)
            {

                msg = "Failed";
            }
            return msg;
        }
        public string SaveData()
        {
            string msg;
            try
            {
                _personalInfoRepository.Save();
                msg = "Success";
            }
            catch (DbException)
            {

                msg = "Failed";
            }
            return msg;
        }


    }
}
