﻿using System;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IAdmission;
using UMS_MVC.Manager.DAL.DAO.Repository.RAdmission;

namespace UMS_MVC.Manager.DAL.DAO.AdmissionManager
{
   public class SubmittedDocumentsManager
    {
        private readonly ISubmittedDocumentRepository _submittedDocumentRepository;
        private readonly StudentManager _studentManager = new StudentManager();

        public SubmittedDocumentsManager()
        {
            var context = new UmsDbContext();
            context.Configuration.ProxyCreationEnabled = false;
            context.Configuration.LazyLoadingEnabled = false;
            _submittedDocumentRepository = new SubmittedDocumentRepository();
        }

        public bool CheckExistingData(string id)
        {
            return _submittedDocumentRepository.GetAll().AsNoTracking().Count(s=>s.StudentId==id)==1;
        }

        public DocumentAdding GetSingle(int id)
        {
            return _submittedDocumentRepository.FindSingle(id);
        }
        public IQueryable<DocumentAdding> GetAll()
        {
            return _submittedDocumentRepository.GetAll();
        }
        public void Insert(DocumentAdding documentAdding)
       {
           var check=_studentManager.GetAllStudentDocument().Count(s => s.StudentId == documentAdding.StudentId) == 0;
           if (check)
           {
               _submittedDocumentRepository.Add(documentAdding);
           }
       }
        
        public void Update(DocumentAdding documentAdding)
        {
            _submittedDocumentRepository.Update(documentAdding);
        }
        public string Delete(int id)
        {
            string msg;
            try
            {
                var studeDocumentInfo = _submittedDocumentRepository.FindSingle(id);
                _submittedDocumentRepository.Delete(studeDocumentInfo);
                _submittedDocumentRepository.Save();
                msg = "Success";
            }
            catch (Exception)
            {

                msg = "Failed";
            }
            return msg;
        }
        public string SaveData()
        {
            string msg;
            try
            {
                _submittedDocumentRepository.Save();
                msg = "Success";
            }
            catch (DbException)
            {

                msg = "Failed";
            }
            return msg;
        }

    }
}
