﻿using System;
using System.Data.Common;
using System.Linq;
using System.Text;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IAdmission;
using UMS_MVC.Manager.DAL.DAO.Repository.RAdmission;

namespace UMS_MVC.Manager.DAL.DAO.AdmissionManager
{
    public class StudentIdentityManager
    {
        private readonly IStudentIdentityRepository _identityRepository;
        private readonly StudentManager _studentManager = new StudentManager();
        private readonly SemesterManager _semesterManager = new SemesterManager();
        private readonly DepartmentManager _departmentManager = new DepartmentManager();
        public StudentIdentityManager()
        {
            var context = new UmsDbContext();
            context.Configuration.ProxyCreationEnabled = false;
            context.Configuration.LazyLoadingEnabled = false;
            _identityRepository = new StudentIdentityRepository();
        }
        public StudentIdentification GetSingle(int id)
        {
            return _identityRepository.FindSingle(id);
        }
        public void Insert(StudentIdentification studentIdentification)
        {
            _identityRepository.Add(studentIdentification);
        }

        public void Update(StudentIdentification studentIdentification)
        {
            _identityRepository.Update(studentIdentification);
        }

        public string Delete(int id)
        {
            string msg;
            try
            {
                var studeAcaInfo = _identityRepository.FindSingle(id);
                _identityRepository.Delete(studeAcaInfo);
                _identityRepository.Save();
                msg = "Success";
            }
            catch (Exception)
            {

                msg = "Failed";
            }
            return msg;
        }
        public string SaveData()
        {
            string msg;
            try
            {
                _identityRepository.Save();
                msg = "Success";
            }
            catch (DbException)
            {

                msg = "Failed";
            }
            return msg;
        }
        public string GetGeneratedId(int depId, int semId)
        {
            if (depId > 0 && semId > 0)
            {
                int countStudent = _studentManager.GetAllStudentByDepartment(depId).Count(s => s.SemesterId == semId);
                var getSemesterInfo = _semesterManager.GetSingleSemester(semId);
                var departmentCode = _departmentManager.GetSingleDepartment(depId).DepartmentCode;
                if (countStudent > 0)
                {
                    string identity;
                    int nextId = countStudent;
                    do
                    {
                        nextId++;
                        identity = getSemesterInfo.BatchNo + nextId.ToString("D3") + departmentCode.ToString("D3");
                    } while (_studentManager.StudentExistanceByStudentId(identity));

                    return identity;
                }
                if (countStudent == 0)
                {
                    const int studentSerial = 1;
                    string identity = getSemesterInfo.BatchNo + studentSerial.ToString("D3") + departmentCode.ToString("D3");
                    return identity;
                }
            }
            return "------";
        }

        //For Generating Password
        private static int RandomNumber(int min, int max)
        {
            var random = new Random();
            return random.Next(min, max);
        }
        private static string RandomString(int size, bool lowerCase)
        {
            var builder = new StringBuilder();
            var random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }
        public string GeneratePassword(int passChar = 2)
        {
            var bd = new StringBuilder();
            bd.Append(RandomString(passChar, true));
            bd.Append(RandomNumber(100, 9999));
            //bd.Append(RandomString(2, false));
            return bd.ToString();

        }

       
    }
}
