﻿using System;
using System.Collections.Generic;
using System.Linq;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.Manager.DAL.DAO
{
    public class RebateManager
    {
        private UmsDbContext db;

        public RebateManager()
        {
            db = new UmsDbContext();
        }

        public IEnumerable<Rebate> GetAllRebate()
        {
            return db.Rebates;
        }

        public IQueryable<Rebate> GetAllRebatesFiltered()
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.Configuration.LazyLoadingEnabled = false;
            return db.Rebates;
        }

        public IEnumerable<RebateCategory> GetAllRebateCategories()
        {
            return db.RebateCategories;
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                //Free any other managed objects here. 
                db.Dispose();
            }

            // Free any unmanaged objects here. 
            disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}