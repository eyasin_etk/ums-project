﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace UMS_MVC.Manager.DAL.DAO.GenericService
{
    public abstract class GenericRepository<C, T> : IGenericRepository<T> where T : class where C : DbContext, new()
    {
        //private C _entities = new C();
        private C _entities = new C {  };
        protected C Context
        {
            get { return _entities; }
            set { _entities = value; }
        }
       
        public virtual IQueryable<T> GetAll()
        {
            IQueryable<T> query = _entities.Set<T>().AsQueryable();
            return query;
        }
        public virtual async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _entities.Set<T>().ToListAsync();
        }
        public IQueryable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
           IQueryable<T> query = _entities.Set<T>().Where(predicate);
            return query;
        }
        public virtual async Task<IEnumerable<T>> FindByAsync(Expression<Func<T, bool>> predicate)
        {
            return await _entities.Set<T>().Where(predicate).ToListAsync();
        }

        public T FindSingle(int id)
        {
           return _entities.Set<T>().Find(id);
        }
        public async Task<T> FindSingleAsync(int id)
        {
            return await _entities.Set<T>().FindAsync(id);
        }
        public virtual void Add(T entity)
        {

            _entities.Set<T>().Add(entity);
        }
        public void Update(T entity)
        {
            _entities.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Delete(T entity)
        {

            _entities.Set<T>().Remove(entity);
        }


        public virtual void Save()
        {

            _entities.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {

            if (!this.disposed)
                if (disposing)
                    _entities.Dispose();

            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}