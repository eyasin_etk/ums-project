﻿using System;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Login_History;

namespace UMS_MVC.Manager.DAL.DAO
{
    public class LoginHistoryManager
    {
        private UmsDbContext db;

        public LoginHistoryManager()
        {
            db = new UmsDbContext();
        }

        public IQueryable<LoginHistory> GetAlLoginHistories()
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.Configuration.LazyLoadingEnabled = false;
            return db.LoginHistories.Include(s=>s.Account);
        }

        public IQueryable<LoginHistory> GetAllLoginHistoriesByTime(string dateTime)
        {
            return GetAlLoginHistories().Where(s => s.EntryTime.Contains(dateTime));
        }

        public bool InsertLoginHistory(LoginHistory loga)
        {
            try
            {
                db.LoginHistories.Add(loga);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public bool DeleteLoginHistory(int loginHistoryId)
        {
           
            try
            {
                var findLoginHistory = db.LoginHistories.Find(loginHistoryId);
                db.LoginHistories.Remove(findLoginHistory);
                db.SaveChanges();
                return true;
            }
            catch (DbException)
            {

                return false;
            }
        }

        public int GetTotalLogins(int accountId)
        {
            return db.LoginHistories.Count(s => s.AccountId == accountId);
        }

        public IQueryable<LoginHistoryStudent> GetAlLoginHistoryStudents()
        {
            return db.LoginHistoryStudents;
        }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
        public IQueryable<LoginHistoryStudent> GetLoginHistoryStudentsByTime(string time)
        {
            return db.LoginHistoryStudents.Where(s => s.EntryTime.Contains(time)).OrderByDescending(s=>s.EntryTime);
        }

        public bool InsertLoginHistoryOfrStudent(LoginHistoryStudent loginHistoryStudent)
        {
            try
            {
                db.LoginHistoryStudents.Add(loginHistoryStudent);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                //Free any other managed objects here. 
                db.Dispose();
            }

            // Free any unmanaged objects here. 
            disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}