﻿using System.Linq;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO.Intefaces;
using UMS_MVC.Manager.DAL.DAO.Repository;

namespace UMS_MVC.Manager.DAL.DAO
{
    public class GradingSystemManager
    {
        private readonly UmsDbContext _umsDbContext;
        private readonly SectionManager _sectionManager;
        private readonly IGradingSystemRepository _gradingSystemRepository;

        public GradingSystemManager()
        {
            _umsDbContext=new UmsDbContext();
            _umsDbContext.Configuration.ProxyCreationEnabled = false;
            _umsDbContext.Configuration.LazyLoadingEnabled = false;
            _sectionManager=new SectionManager();
            _gradingSystemRepository=new GradingSystemRepository();
        }

        public IQueryable<GradingSystem> GetAllGradingSystems()
        {
            return _gradingSystemRepository.GetAll();
        }

        public GradingSystem FindGradingSystem(int id)
        {
            return _gradingSystemRepository.FindSingle(id);
        }
        public GradingSystem GetGradingSystemOfSpecificAdvising(CourseForStudentsAcademic academic)
        {
            GradingSystem findGradingSystem;
            if (academic.GradingSystemId != null)
            {
                findGradingSystem = FindGradingSystem((int)academic.GradingSystemId);
            }
            else
            {
                var findSection = _sectionManager.GetSingleSection((int)academic.SectionId);
                findGradingSystem = FindGradingSystem((int)findSection.GradingSystemId);
            }
            return findGradingSystem;
        }
        //public GradingSystem FindGradingSystemBySection(int id)
        //{

        //}

        public void Insert(GradingSystem gradingSystem)
        {
            _gradingSystemRepository.Add(gradingSystem);
        }

        public void Update(GradingSystem gradingSystem)
        {
            _gradingSystemRepository.Update(gradingSystem);
        }

        public void Delete(int id)
        {
            var gradingSystem = _gradingSystemRepository.FindSingle(id);
            _gradingSystemRepository.Delete(gradingSystem);
        }

        public void Save()
        {
            _gradingSystemRepository.Save();
        }
    }
}