﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.Manager.DAL.DAO
{
    public class AccountManager:IDisposable
    {
        private UmsDbContext db;

        public AccountManager()
        {
            db = new UmsDbContext();
        }

        public IQueryable<Account> GetAllAccountsFiltered()
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.Configuration.LazyLoadingEnabled = false;
            return db.Accounts;
        }   

        public Account FindAccountFilterd(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.Configuration.LazyLoadingEnabled = false;
            return db.Accounts.Find(id);
        }

        public List<Account> GetAllAccountFilteredForDrpDown()
        {
            return GetAllAccountsFiltered().AsEnumerable().Select(s => new Account
            {
                AccountId = s.AccountId,
                Name = s.LoginIdentity+" - "+s.Name
                
            }).ToList();
        }

        public async Task<Account> FindAccountByLogInId(LoginInitial loginInitial)
        {
            return await 
                GetAllAccountsFiltered().Include(s=>s.AccountsRole)
                    .FirstOrDefaultAsync(s => s.LoginIdentity == loginInitial.LoginId && s.Password == loginInitial.Password);
        }

        public IQueryable<Account> GetAllAccounts()
        {
            return db.Accounts;
        }

        public IEnumerable<Account> GetAllAccountlimited()
        {
            var accounts = db.Accounts.AsEnumerable();
            var sortedResult = accounts.Select(s => new Account()
            {
                AccountId = s.AccountId,
                   Name =s.LoginIdentity+"--"+ s.Name,
                   LoginIdentity = s.LoginIdentity,
                   AccountsRoleId = s.AccountsRoleId,
                   Password = s.Password,
                   AccountsRole = new AccountsRole()
                   {
                       AccountsRoleId = s.AccountsRole.AccountsRoleId,
                       AccountType = s.AccountsRole.AccountType
                   }
                   
            });
            return sortedResult;
        }

        public IQueryable<AccountsRole> GetAllAccountsRoles()
        {
            return db.AccountsRoles;
        }
        public IQueryable<AccountsRole> AccountRoleForSupAdmin()
        {
            return GetAllAccountsRoles();
        }
        public IQueryable<AccountsRole> AccountRoleForAdmin()
        {
            int[] rolses = new[] {1};
            return GetAllAccountsRoles().Where(s => !rolses.Contains(s.AccountsRoleId));
        }
        public IQueryable<AccountsRole> AccountRoleForSystemSupport()
        {
            int[] rolses = new[] { 1,14 };
            return GetAllAccountsRoles().Where(s => !rolses.Contains(s.AccountsRoleId));
        }
        public Account GetSingleAccount(int id)
        {
            return db.Accounts.Find(id);
        }

        public bool UpdateAccount(Account account)
        {
            try
            {
                var findAccount = db.Accounts.Find(account.AccountId);
                db.Entry(findAccount).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch (DbException)
            {

                return false;
            }
            
        }

        public static bool AccountAccessRole(string actionName,Account account)
        {
            switch (actionName)
            {
                    //.... Section ......
                    //.. Sample
                    //if (Session["UserObj"] == null)  return RedirectToAction("LogInresultSubmit", "Home");
                    //var account =(Account) Session["UserObj"];
                    //if (!AccountManager.AccountAccessRole("SectionIndex", account)) return RedirectToAction("NotPermitted", "Home");
                case "SectionIndex":
                    var accounts = new[] { "Admin", "Ps", "Coordinator", "Exam","ExamCtrl" };
                    if (accounts.Contains(account.AccountsRole.AccountType)) return true; 
                   break;
                case "SectionDetails":
                   var accounts2 = new[] { "Admin", "Coordinator" };
                    if (accounts2.Contains(account.AccountsRole.AccountType)) return true; 
                   break;
                case "SectionCreate":
                   var accounts3 = new[] { "Admin"};
                   if (accounts3.Contains(account.AccountsRole.AccountType)) return true;
                   break;
                case "SectionEdit":
                   var accounts4 = new[] { "Admin" };
                   if (accounts4.Contains(account.AccountsRole.AccountType)) return true;
                   break;
                case "SectionDelete":
                   var accounts5 = new[] { "Admin" };
                   if (accounts5.Contains(account.AccountsRole.AccountType)) return true;
                   break;

            }
            return false;
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                //Free any other managed objects here. 
                db.Dispose();
            }

            // Free any unmanaged objects here. 
            disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}