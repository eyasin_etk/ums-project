﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO.Intefaces;
using UMS_MVC.Manager.DAL.DAO.Repository;

namespace UMS_MVC.Manager.DAL.DAO
{
    public class NoteUploadManager
    {
        private readonly INoteUploadRepository _noteUploadRepository;

        public NoteUploadManager()
        {
            _noteUploadRepository=new NoteUploadRepository();
        }

        public IQueryable<NoteUpload> GetAllUploadedNote()
        {
            return _noteUploadRepository.GetAll();
        }
        public IQueryable<NoteUpload> GetAllNoteUploadBySection(int secId)
        {
            return _noteUploadRepository.GetAll().Where(s => s.SectionId == secId);
        }
        public NoteUpload FindSingleNote(int id)
        {
            return _noteUploadRepository.FindSingle(id);
            
        }

        public void InsertNote(NoteUpload noteUpload)
        {
            _noteUploadRepository.Add(noteUpload);
            
        }

        public void Update(NoteUpload noteUpload)
        {
            _noteUploadRepository.Update(noteUpload);
            
        }

        public void Delete(int id)
        {
            var findSingle = _noteUploadRepository.FindSingle(id);
            _noteUploadRepository.Delete(findSingle);
            
        }

        public void Save()
        {
            _noteUploadRepository.Save();
        }

    }
}
