﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Ajax.Utilities;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.DataModel.Models.UniversalTypes;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.Manager.DAL.DAO
{
    public class PaymentManager
    {
        private UmsDbContext db;
        private readonly CourseForStudentManger _courseForStudentManger=new CourseForStudentManger();

        public PaymentManager()
        {
            db = new UmsDbContext();
        }

        public IQueryable<PaymentRegistration> GetAllPaymentRegistration()
        {
            return db.PaymentRegistrations;
        }

        public IQueryable<PaymentRegistration> GetAllPaymentRegistrationsFiltered()
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.Configuration.LazyLoadingEnabled = false;
           return db.PaymentRegistrations;
        }

        public PaymentRegistration GetSinglePaymentRegistration(int paymentId)
        {
            return db.PaymentRegistrations.Find(paymentId);
        }

        public Payment GetSinglePayment(int paymentId)
        {
            return db.Payments.Find(paymentId);
        }

        public IQueryable<Payment> GetAllPayments()
        {
            return db.Payments;
        }

        public IQueryable<PaymentType> GetAllPaymentTypes()
        {
            return db.PaymentTypes;
        }

        public IEnumerable<NewRegisteredStudents> GetAllAdvisedStudent()
        {
            var getNewPaymentRegistrations =
                GetAllPaymentRegistration().Where(s => s.Semester.ActiveSemester || s.Semester.CourseAdvising);
            //var advisedList = getNewPaymentRegistrations.Select(newPaymentRegistration => new NewRegisteredStudents
            //{
            //    DepartmentName = newPaymentRegistration.StudentIdentification.Department.DepartmentName,
            //    PaymentRegistrationNumber = newPaymentRegistration.PaymentRegistrationId, 
            //    NoOfRegisteredCourses = newPaymentRegistration.CourseForStudentsAcademics.Count, 
            //    RegisteredBy = newPaymentRegistration.PrintedBy,
            //    StudentId = newPaymentRegistration.StudentIdentification.StudentId,
            //    StudentIdentityId = newPaymentRegistration.StudentIdentificationId,
            //    Registrationtime = newPaymentRegistration.PrintDate
            //}).ToList();

            var getSorting =
                from takenCourses in
                    getNewPaymentRegistrations.Where(s => s.Semester.ActiveSemester || s.Semester.CourseAdvising).DistinctBy(s => s.PaymentRegistrationId)
                    select new NewRegisteredStudents
                    {
                        DepartmentName = takenCourses.StudentIdentification.Department.DepartmentName,
                        PaymentRegistrationNumber = takenCourses.PaymentRegistrationId,
                        NoOfRegisteredCourses = takenCourses.CourseForStudentsAcademics.Count,
                        RegisteredBy = takenCourses.PrintedBy,
                        StudentId = takenCourses.StudentIdentification.StudentId,
                        StudentIdentityId = takenCourses.StudentIdentificationId,
                        Registrationtime = takenCourses.PrintDate
                    };
            return getSorting;

        }

        //private bool disposed = false;
        //protected virtual void Dispose(bool disposing)
        //{
        //    if (disposed)
        //        return;

        //    if (disposing)
        //    {
        //        //Free any other managed objects here. 
        //        db.Dispose();
        //    }

        //    // Free any unmanaged objects here. 
        //    disposed = true;
        //}
        //public void Dispose()
        //{
        //    Dispose(true);
        //    GC.SuppressFinalize(this);
        //}
    }
}