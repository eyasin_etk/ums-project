﻿using System.Linq;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.Manager.DAL.DAO
{
    public class DepartmentChangingManager
    {
        private UmsDbContext _db;
        public DepartmentChangingManager()
        {
            _db=new UmsDbContext();
        }

        public IQueryable<DepartmentChange> GetAllDepartmentChanges()
        {
            return _db.DepartmentChanges;
        }
    }
}