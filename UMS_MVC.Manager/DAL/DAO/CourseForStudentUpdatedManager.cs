﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO.Intefaces;
using UMS_MVC.Manager.DAL.DAO.Repository;

namespace UMS_MVC.Manager.DAL.DAO
{
    public class CourseForStudentUpdatedManager
    {
        private readonly ICourseForStudentRepository _courseForStudentRepository;

        public CourseForStudentUpdatedManager()
        {
            _courseForStudentRepository=new CourseForStudentRepository();
        }

        public IQueryable<CourseForStudentsAcademic> GetAllCourseForStudent()
        {
            return _courseForStudentRepository.GetAll();
        }

        public CourseForStudentsAcademic FindCourseForStudentsAcademic(int id)
        {
            return _courseForStudentRepository.FindSingle(id);
        }

        public IQueryable<CourseForStudentsAcademic> GetAllAdvisingBySection(int secId)
        {
            return _courseForStudentRepository.GetAll().Where(s=>s.SectionId==secId).AsQueryable();
        }

        public IQueryable<CourseForStudentsAcademic> GetAllCourseByStudentId(int id)
        {
            return _courseForStudentRepository.GetAll().Where(s=>s.StudentIdentificationId==id).AsQueryable();
        }

        public void Insert(CourseForStudentsAcademic academic)
        {
            _courseForStudentRepository.Add(academic);
        }

        public int InsertToGetId(CourseForStudentsAcademic academic)
        {
            return _courseForStudentRepository.InsertToGetId(academic);
        }

        public void Update(CourseForStudentsAcademic academic)
        {
            _courseForStudentRepository.Update(academic);
        }

        public void Delete(int id)
        {
            var courseForStudentsAcademic = _courseForStudentRepository.FindSingle(id);
            _courseForStudentRepository.Delete(courseForStudentsAcademic);
        }

        public void Save()
        {
            _courseForStudentRepository.Save();
        }
    }
}
