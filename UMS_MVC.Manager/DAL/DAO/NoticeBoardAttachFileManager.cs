﻿using System.Linq;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Notice_Matter;
using UMS_MVC.Manager.DAL.DAO.Intefaces;
using UMS_MVC.Manager.DAL.DAO.Repository;

namespace UMS_MVC.Manager.DAL.DAO
{
    public class NoticeBoardAttachFileManager
    {
        private readonly INoticeBoardRepository _fileRepository;

        public NoticeBoardAttachFileManager()
        {
           
            _fileRepository=new NoticeBoardRepository();
        }

        public IQueryable<NoticeBoardAttatchedFiles> GetAllAttatchedFiles()
        {
            return _fileRepository.GetAll();
        }

        public NoticeBoardAttatchedFiles GetSingleFile(int id)
        {
            return _fileRepository.FindSingle(id);
        }

        public void Insert(NoticeBoardAttatchedFiles file)
        {
            _fileRepository.Add(file);
        }

        public void Update(NoticeBoardAttatchedFiles file)
        {
            _fileRepository.Update(file);
        }

        public void Delete(int id)
        {
            var attatchedFiles = _fileRepository.FindSingle(id);
            _fileRepository.Delete(attatchedFiles);
        }

        public void Save()
        {
            _fileRepository.Save();
        }
    }
}