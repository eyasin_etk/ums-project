﻿using System;
using System.Data.Common;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IAdmission;
using UMS_MVC.Manager.DAL.DAO.Repository.RAdmission;

namespace UMS_MVC.Manager.DAL.DAO
{
   public class UpdatedAdmissionManager
    {
        private readonly IStudentIdentityRepository _updatedAdmissionRepository;

        public UpdatedAdmissionManager()
        {
            var context = new UmsDbContext();
            context.Configuration.ProxyCreationEnabled = false;
            context.Configuration.LazyLoadingEnabled = false;
            _updatedAdmissionRepository = new StudentIdentityRepository();
        }
        
        public void Insert(StudentIdentification studentIdentification)
        {
            _updatedAdmissionRepository.Add(studentIdentification);
        }

        public void Update(StudentIdentification studentIdentification)
        {
            _updatedAdmissionRepository.Update(studentIdentification);
        }

       

        public string SaveData()
        {
            string msg;
            try
            {
                _updatedAdmissionRepository.Save();
                msg = "Success";
            }
            catch (DbException)
            {

                msg = "Failed";
            }
            return msg;
        }
       
    }
}

