﻿using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.CreditTransfer;
using UMS_MVC.Manager.DAL.DAO.GenericService;
using UMS_MVC.Manager.DAL.DAO.Intefaces;

namespace UMS_MVC.Manager.DAL.DAO.Repository
{
    public class CreditTrasnsferStudentRepository:GenericRepository<UmsDbContext,CreditTransferStudent>,ICreditTrasnsferStudentRepository
    {}
    public class CreditTransferCoursesRepository : GenericRepository<UmsDbContext, CreditTransferAcceptedCourse>,
        ICreditTransferCoursesRepository
    {}
}
