﻿using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Notice_Matter;
using UMS_MVC.Manager.DAL.DAO.GenericService;
using UMS_MVC.Manager.DAL.DAO.Intefaces;

namespace UMS_MVC.Manager.DAL.DAO.Repository
{
    public class NoticeBoardRepository : GenericRepository<UmsDbContext, NoticeBoardAttatchedFiles>,
        INoticeBoardRepository
    {

    }

    public class NoticeBoardCategoryRepository : GenericRepository<UmsDbContext, NoticeBoardCategory>,
        INoticeBoardCategoryRepository
    {
    }

    public class NoticeBoardDetailsRepository : GenericRepository<UmsDbContext, NoticeBoardDetail>,
        INoticeBoardDetailsRepository
    {

    }
}