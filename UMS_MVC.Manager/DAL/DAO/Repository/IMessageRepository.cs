﻿using System;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.Manager.DAL.DAO.Repository
{
    public interface IMessageRepository:IDisposable
    {
       void GetAllMessage();
        InternalMessage GetSingleMessage(int id);
        void CreateMessage();
    }
}