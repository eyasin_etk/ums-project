﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.PreAdmission;
using UMS_MVC.Manager.DAL.DAO.GenericService;
using UMS_MVC.Manager.DAL.DAO.Intefaces;

namespace UMS_MVC.Manager.DAL.DAO.Repository
{
    public class PreAdmissionStudentInfoRepository : GenericRepository<UmsDbContext, PreAdmissionStudentInfo>,
        IPreAdmissionStudentInfoRepository
    {
        public async Task<PreAdmissionStudentInfo> FindSingleInfoGuidAsync(Guid id)
        {
            return await Context.PreAdmissionStudentInfos.FindAsync(id);
        }

        public PreAdmissionStudentInfo FinSingleGuid(Guid? id)
        {
            return  Context.PreAdmissionStudentInfos.Find(id);
        }

        public bool CheckAdmitted(Guid? id)
        {
            return Context.PreAdmissionStudentInfos.Count(s => s.PreAdmissionStudentInfoId == id) == 1;
        }

        public void ConfirmAdmission(Guid? studentGuid)
        {
            PreAdmissionStudentInfo info = FinSingleGuid(studentGuid);
            info.AdmissionOfficeConfirmed = true;
            Update(info);
            Save();
        }
        public string GetExistStudentImage(Guid id)
        {
            return Context.PreAdmissionStudentInfos.Find(id).StudentImage;
        }

        public async Task<bool> CheckExistingStudent(Guid id)
        {
            return await Context.PreAdmissionStudentInfos.AsNoTracking().CountAsync(s => s.PreAdmissionStudentInfoId == id) == 1;
        }

        public async Task<bool> CheckDuplicateByPhone(string phoneNo)
        {
            return await Context.PreAdmissionStudentInfos.AsNoTracking().CountAsync(s => s.PersonalContactNo.Equals(phoneNo)) == 0;
        }

    }

    public class PreAdmissionAcademicRepository : GenericRepository<UmsDbContext, PreAdmissionAcademic>,
        IPreAdmissionAcademicRepository
    {
        public PreAdmissionAcademic FindSingleGuid(Guid id)
        {
            return Context.PreAdmissionAcademics.Find(id);
        }
        public async Task<bool> CheckDuplicateExamination(Guid preAdmissionStudentInfoId, string nameOfExam)
        {
            return await Context.PreAdmissionAcademics.AsNoTracking().CountAsync(s => s.PreAdmissionStudentInfoId == preAdmissionStudentInfoId && s.NameOfExamination.Equals(nameOfExam)) == 0;
        }

        public async Task<int> CountAcademicInfoes(Guid preAdmissionStudentInfoId)
        {
            return await
                Context.PreAdmissionAcademics.AsNoTracking()
                    .CountAsync(s => s.PreAdmissionStudentInfoId == preAdmissionStudentInfoId);
        }
    }

   
}
