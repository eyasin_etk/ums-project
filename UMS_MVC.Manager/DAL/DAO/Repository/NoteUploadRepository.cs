﻿using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO.GenericService;
using UMS_MVC.Manager.DAL.DAO.Intefaces;

namespace UMS_MVC.Manager.DAL.DAO.Repository
{
    public class NoteUploadRepository:GenericRepository<UmsDbContext,NoteUpload>,INoteUploadRepository
    {
       
    }
}
