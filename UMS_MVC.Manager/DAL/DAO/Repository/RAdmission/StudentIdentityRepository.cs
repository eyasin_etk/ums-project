﻿using System.Data.Entity;
using System.Threading.Tasks;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO.GenericService;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IAdmission;

namespace UMS_MVC.Manager.DAL.DAO.Repository.RAdmission
{
    public class StudentIdentityRepository : GenericRepository<UmsDbContext, StudentIdentification>, IStudentIdentityRepository
    {
        public string GetGeneratedId(StudentIdentification identification)
        {
            return "";
        }

        public async Task<bool> CheckStudentExitanceByIdentity(int studentId)
        {
            return await Context.StudentIdentifications.AsNoTracking().CountAsync(s=>s.StudentIdentificationId==studentId)==1;
        }

        public async Task<bool> CheckStudentExitanceById(string studentId)
        {
            return await Context.StudentIdentifications.AsNoTracking().CountAsync(s => s.StudentId == studentId) == 1;
        }

        public void StudentIdSync(int infoStudentInfoId, int identityId)
        {
            StudentIdentification findSingle = FindSingle(identityId);
            findSingle.StudentInfoId = infoStudentInfoId;
            Update(findSingle);
        }
    }
}
