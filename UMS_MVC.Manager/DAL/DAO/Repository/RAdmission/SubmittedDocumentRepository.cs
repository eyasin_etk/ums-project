﻿using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO.GenericService;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IAdmission;

namespace UMS_MVC.Manager.DAL.DAO.Repository.RAdmission
{
    public class SubmittedDocumentRepository:GenericRepository<UmsDbContext, DocumentAdding>, ISubmittedDocumentRepository
    {

    
    }
}
