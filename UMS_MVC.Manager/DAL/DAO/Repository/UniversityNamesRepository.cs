﻿using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.UniversalTypes;
using UMS_MVC.Manager.DAL.DAO.GenericService;
using UMS_MVC.Manager.DAL.DAO.Intefaces;

namespace UMS_MVC.Manager.DAL.DAO.Repository
{
    public class UniversityNamesRepository:GenericRepository<UmsDbContext, UniversityName>, IUniversityNamesRepository
    {
    }
}
