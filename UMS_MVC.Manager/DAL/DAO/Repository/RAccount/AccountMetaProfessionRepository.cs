﻿using System;
using System.Data.Entity;
using System.Linq;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.AccountExt;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IAccount;

namespace UMS_MVC.Manager.DAL.DAO.Repository.RAccount
{
    public class AccountMetaProfessionRepository:IAccountMetaProfessionRepository
    {
        private readonly UmsDbContext _dbContext;

        public AccountMetaProfessionRepository(UmsDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<AccountMetaProfessional> GetAllAccountMetaInformationRepositories()
        {
            return _dbContext.AccountMetaProfessionals;
        }

        public AccountMetaProfessional GetSingleAccountMetaInformationRepository(int id)
        {
            return _dbContext.AccountMetaProfessionals.Find(id);
        }

        public void Insert(AccountMetaProfessional accountMetaProfessional)
        {
            _dbContext.AccountMetaProfessionals.Add(accountMetaProfessional);
        }

        public void Update(AccountMetaProfessional accountMetaProfessional)
        {
            _dbContext.Entry(accountMetaProfessional).State=EntityState.Modified;
        }

        public void Delete(int id)
        {
            _dbContext.AccountMetaProfessionals.Remove(_dbContext.AccountMetaProfessionals.Find(id));
        }

        public void Save()
        {
            _dbContext.SaveChanges();
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}