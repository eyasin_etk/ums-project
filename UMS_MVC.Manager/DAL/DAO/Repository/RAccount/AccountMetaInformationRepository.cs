﻿using System;
using System.Data.Entity;
using System.Linq;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.AccountExt;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IAccount;

namespace UMS_MVC.Manager.DAL.DAO.Repository.RAccount
{
    public class AccountMetaInformationRepository:IAccountMetaInformationRepository
    {
        private readonly UmsDbContext _dbContext;

        public AccountMetaInformationRepository(UmsDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<AccountMetaInformation> GetAllAccountMetaInformations()
        {
            return _dbContext.AccountMetaInformations;
        }

        public AccountMetaInformation GetSingleAccountMetaInformation(int id)
        {
            return _dbContext.AccountMetaInformations.Find(id);
        }

        public void Insert(AccountMetaInformation accountMetaInformation)
        {
            _dbContext.AccountMetaInformations.Add(accountMetaInformation);
        }

        public void Update(AccountMetaInformation accountMetaInformation)
        {
            _dbContext.Entry(accountMetaInformation).State=EntityState.Modified;
        }

        public void Delete(int id)
        {
            _dbContext.AccountMetaInformations.Remove(_dbContext.AccountMetaInformations.Find(id));
        }

        public void Save()
        {
            _dbContext.SaveChanges();
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}