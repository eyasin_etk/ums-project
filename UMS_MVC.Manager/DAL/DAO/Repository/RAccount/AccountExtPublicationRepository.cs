﻿using System;
using System.Data.Entity;
using System.Linq;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.AccountExt;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IAccount;

namespace UMS_MVC.Manager.DAL.DAO.Repository.RAccount
{
    public class AccountExtPublicationRepository:IAccountExtPublicationRepository
    {
        private readonly UmsDbContext _dbContext;

        public AccountExtPublicationRepository(UmsDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<AccountExtPublication> GetAllAccountExtPublications()
        {
            return _dbContext.AccountExtPublications;
        }

        public AccountExtPublication GetSingle(int id)
        {
            return _dbContext.AccountExtPublications.Find(id);
        }

        public void Insert(AccountExtPublication accountExtPublication)
        {
            _dbContext.AccountExtPublications.Add(accountExtPublication);
        }

        public void Update(AccountExtPublication accountExtPublication)
        {
            _dbContext.Entry(accountExtPublication).State=EntityState.Modified;
        }

        public void Delete(int id)
        {
            _dbContext.AccountExtPublications.Remove(_dbContext.AccountExtPublications.Find(id));
        }

        public void Save()
        {
            _dbContext.SaveChanges();
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}