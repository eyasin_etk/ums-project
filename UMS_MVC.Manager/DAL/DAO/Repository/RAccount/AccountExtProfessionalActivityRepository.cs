﻿using System;
using System.Data.Entity;
using System.Linq;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.AccountExt;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IAccount;

namespace UMS_MVC.Manager.DAL.DAO.Repository.RAccount
{
    public class AccountExtProfessionalActivityRepository:IAccountExtProfessionalActivityRepository
    {
        private readonly UmsDbContext _dbContext;

        public AccountExtProfessionalActivityRepository(UmsDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<AccountExtProfessionalActivity> GetAllAccountExtProfessionalActivities()
        {
            return _dbContext.AccountExtProfessionalActivities;
        }

        public AccountExtProfessionalActivity GetSingleAccountExtProfessionalActivity(int id)
        {
            return _dbContext.AccountExtProfessionalActivities.Find(id);
        }

        public void Insert(AccountExtProfessionalActivity accountExtProfessionalActivity)
        {
            _dbContext.AccountExtProfessionalActivities.Add(accountExtProfessionalActivity);
        }

        public void Update(AccountExtProfessionalActivity accountExtProfessionalActivity)
        {
            _dbContext.Entry(accountExtProfessionalActivity).State=EntityState.Modified;
        }

        public void Delete(int id)
        {
            _dbContext.AccountExtProfessionalActivities
                .Remove(_dbContext.AccountExtProfessionalActivities.Find(id));
        }

        public void Save()
        {
            _dbContext.SaveChanges();
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}