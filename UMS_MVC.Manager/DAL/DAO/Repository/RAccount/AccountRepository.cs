﻿using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO.GenericService;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IAccount;

namespace UMS_MVC.Manager.DAL.DAO.Repository.RAccount
{
    public class AccountRepository:GenericRepository<UmsDbContext,Account>,IAccountRepository
    {
        //private readonly UmsDbContext _dbContext;

        //public AccountRepository(UmsDbContext dbContext)
        //{
        //    _dbContext = dbContext;
        //}

        //public IQueryable<Account> GetAllAccounts()
        //{
        //    return _dbContext.Accounts;
        //}

        //public Account GetSingleAccount(int id)
        //{
        //   return _dbContext.Accounts.Find(id);
        //}

        //public void Update(Account account)
        //{
        //    _dbContext.Entry(account).State=EntityState.Modified;
        //}

        //public void Insert(Account account)
        //{
        //    _dbContext.Accounts.Add(account);
        //}

        //public void Delete(int id)
        //{
        //    _dbContext.Accounts.Remove(_dbContext.Accounts.Find(id));
        //}

        //public void Save()
        //{
        //    _dbContext.SaveChanges();
        //}
        //private bool disposed = false;
        //protected virtual void Dispose(bool disposing)
        //{
        //    if (!this.disposed)
        //    {
        //        if (disposing)
        //        {
        //            _dbContext.Dispose();
        //        }
        //    }
        //    this.disposed = true;
        //}

        //public void Dispose()
        //{
        //    Dispose(true);
        //    GC.SuppressFinalize(this);
        //}
    }
}