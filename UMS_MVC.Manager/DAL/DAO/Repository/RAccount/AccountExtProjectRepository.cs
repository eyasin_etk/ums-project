﻿using System;
using System.Data.Entity;
using System.Linq;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.AccountExt;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IAccount;

namespace UMS_MVC.Manager.DAL.DAO.Repository.RAccount
{
    public class AccountExtProjectRepository:IAccountExtProjectRepository
    {
        private readonly UmsDbContext _dbContext;

        public AccountExtProjectRepository(UmsDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<AccountExtProject> GetAllAccountExtProjects()
        {
            return _dbContext.AccountExtProjects;
        }

        public AccountExtProject GetSingleAccountExtProject(int id)
        {
            return _dbContext.AccountExtProjects.Find(id);
        }

        public void Insert(AccountExtProject accountExtProject)
        {
            _dbContext.AccountExtProjects.Add(accountExtProject);
        }

        public void Update(AccountExtProject accountExtProject)
        {
            _dbContext.Entry(accountExtProject).State=EntityState.Modified;
        }

        public void Delete(int id)
        {
            _dbContext.AccountExtProjects.Remove(_dbContext.AccountExtProjects.Find(id));
        }

        public void Save()
        {
            _dbContext.SaveChanges();
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}