﻿using System;
using System.Data.Entity;
using System.Linq;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.AccountExt;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IAccount;

namespace UMS_MVC.Manager.DAL.DAO.Repository.RAccount
{
    public class AccountExtEducationRepository:IAccountExtEducationRepository
    {
        private readonly UmsDbContext _dbContext;

        public AccountExtEducationRepository(UmsDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<AccountExtEducation> GetAllAccountExtEducations()
        {
            return _dbContext.AccountExtEducations;
        }

        public AccountExtEducation GetSinglExtEducation(int id)
        {
            return _dbContext.AccountExtEducations.Find(id);
        }

        public void Insert(AccountExtEducation accountExtEducation)
        {
            _dbContext.AccountExtEducations.Add(accountExtEducation);
        }

        public void Update(AccountExtEducation accountExtEducation)
        {
            _dbContext.Entry(accountExtEducation).State=EntityState.Modified;
        }

        public void Delete(int id)
        {
            _dbContext.AccountExtEducations.Remove(_dbContext.AccountExtEducations.Find(id));
        }

        public void Save()
        {
            _dbContext.SaveChanges();
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}