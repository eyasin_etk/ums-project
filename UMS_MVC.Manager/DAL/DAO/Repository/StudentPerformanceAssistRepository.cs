﻿using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.Student_Tags;
using UMS_MVC.Manager.DAL.DAO.GenericService;
using UMS_MVC.Manager.DAL.DAO.Intefaces;

namespace UMS_MVC.Manager.DAL.DAO.Repository
{
    class StudentPerformanceAssistRepository:GenericRepository<UmsDbContext,StudentPerformanceAssist>, IStudentPerformanceAssistRepository
    {
    }
}
