﻿using System.Data.Entity;
using System.Linq;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO.GenericService;
using UMS_MVC.Manager.DAL.DAO.Intefaces;

namespace UMS_MVC.Manager.DAL.DAO.Repository
{
    public class SemesterRepository:GenericRepository<UmsDbContext,Semester>,ISemesterRepository
    {
        public Semester GetActiveSemester()
        {
            return GetAll().AsNoTracking().FirstOrDefault(s => s.ActiveSemester);
        }

        public Semester GetOnlineAdmSemester()
        {
            return GetAll().AsNoTracking().FirstOrDefault(s => s.OnlineAdmisssionStatus);
        }

        public Semester GetAdvisingSemester()
        {
            return GetAll().AsNoTracking().FirstOrDefault(s=>s.CourseAdvising);
        }
    }
}
