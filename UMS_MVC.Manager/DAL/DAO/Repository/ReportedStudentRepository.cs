﻿using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.Reported_Case;
using UMS_MVC.Manager.DAL.DAO.GenericService;
using UMS_MVC.Manager.DAL.DAO.Intefaces;

namespace UMS_MVC.Manager.DAL.DAO.Repository
{
    public class ReportedStudentRepository:GenericRepository<UmsDbContext,ReportedStudent>,IReportedStudentRepository
    {   
    }

    public class ReportedPunishmentRepositoryRepository : GenericRepository<UmsDbContext, ReportedPunishment>,IReportedPunishmentRepository
    {
    }
}
