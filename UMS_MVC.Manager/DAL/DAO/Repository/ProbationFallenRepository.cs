﻿using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.Probation;
using UMS_MVC.Manager.DAL.DAO.GenericService;
using UMS_MVC.Manager.DAL.DAO.Intefaces;

namespace UMS_MVC.Manager.DAL.DAO.Repository
{
    public class ProbationFallenRepository:GenericRepository<UmsDbContext, ProbationFallenStudent>, IProbationFallenRepository
    {
    }

    public class ProbationCategoryRepository : GenericRepository<UmsDbContext, ProbationCategory>, IProbationCategoryRepository
    {
    }
}
