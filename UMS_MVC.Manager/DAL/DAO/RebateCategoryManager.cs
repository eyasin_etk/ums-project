﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO.Intefaces;
using UMS_MVC.Manager.DAL.DAO.Repository;

namespace UMS_MVC.Manager.DAL.DAO
{
    public class RebateCategoryManager
    {
        private readonly IRebateCategoryRepository _rebateCategory;
        private readonly StudentManager _studentManager;
        private readonly RebateManager _rebateManager;

        public RebateCategoryManager()
        {
            _studentManager = new StudentManager();
            _rebateCategory = new RebateCategoryRepository();
            _rebateManager = new RebateManager();
        }


        public RebateCategory GetSingle(int id)
        {
            return _rebateCategory.FindSingle(id);
        }
        public IQueryable<RebateCategory> GetAll()
        {
            return _rebateCategory.GetAll();
        }


        public void Insert(RebateCategory rebateCategory)
        {

            if (rebateCategory.RebateCategoryId > 0)
            {
                _rebateCategory.Add(rebateCategory);
            }
        }

        public void Update(RebateCategory rebateCategory)
        {
            var check = _rebateManager.GetAllRebatesFiltered().Count(s => s.RebateCategoryId == rebateCategory.RebateCategoryId) == 1;
            if (check && rebateCategory.RebateCategoryId > 0)
            {
                _rebateCategory.Update(rebateCategory);
            }

        }
        public string Delete(int id)
        {
            string msg;
            try
            {
                var rebateInfo = _rebateCategory.FindSingle(id);
                _rebateCategory.Delete(rebateInfo);
                _rebateCategory.Save();
                msg = "Success";
            }
            catch (Exception)
            {

                msg = "Failed";
            }
            return msg;
        }
        public string SaveData()
        {
            string msg;
            try
            {
                _rebateCategory.Save();
                msg = "Success";
            }
            catch (DbException)
            {

                msg = "Failed";
            }
            return msg;
        }
    }
}
