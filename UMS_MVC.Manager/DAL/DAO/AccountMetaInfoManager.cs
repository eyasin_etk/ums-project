﻿using System.Data.Common;
using System.Linq;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.AccountExt;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IAccount;
using UMS_MVC.Manager.DAL.DAO.Repository.RAccount;

namespace UMS_MVC.Manager.DAL.DAO
{
    public class AccountMetaInfoManager
    {
        private readonly IAccountMetaInformationRepository _metaInformationRepository;

        public AccountMetaInfoManager()
        {
            _metaInformationRepository =new AccountMetaInformationRepository(new UmsDbContext());
        }

        public IQueryable<AccountMetaInformation> GetAllMetaInfo()
        {
            return _metaInformationRepository.GetAllAccountMetaInformations();
        }

        public AccountMetaInformation GetSingleMetaInformations(int id)
        {
            return _metaInformationRepository.GetSingleAccountMetaInformation(id);
        }

        public void Insert(AccountMetaInformation accountMeta)
        {
            _metaInformationRepository.Insert(accountMeta);
            
        }

        public string SaveData()
        {
            string msg;
            try
            {
                _metaInformationRepository.Save();
                msg = "Success";
            }
            catch (DbException)
            {

                msg = "Failed";
            }
            return msg;
        }

        public void Update(AccountMetaInformation accountMeta)
        {
            _metaInformationRepository.Update(accountMeta);
        }

        public string Delete(int id)
        {

            string msg;
            try
            {
                _metaInformationRepository.Delete(id);
                msg = "Success";
            }
            catch (DbException)
            {

                msg = "Failed";
            }
            return msg;
        }

       
    }
}