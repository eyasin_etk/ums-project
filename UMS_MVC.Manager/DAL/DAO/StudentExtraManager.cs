﻿using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.Manager.DAL.DAO
{
    public class StudentExtraManager
    {
        private readonly DepartmentManager _departmentManager = new DepartmentManager();
        private readonly SemesterManager _semesterManager = new SemesterManager();

        public AdmittedStudent GenerateViewStudent(StudentIdentification studentIdentification)
        {
            var viewStudent = new AdmittedStudent
            {
                StudentId = studentIdentification.StudentId,
                DepartmentName = _departmentManager.GetSingleDepartment(studentIdentification.DepartmentId).DepartmentName,
                CreditTransfer = studentIdentification.CreditTransfer,
                CreatedDate = studentIdentification.AddedDate,
                Diploma = studentIdentification.DiplomaStudent,
                Password = studentIdentification.Password,
                Semester = _semesterManager.GetSingleSemesterNullable(studentIdentification.SemesterId).SemesterNYear
            };
            return viewStudent;
        }
    }
}