﻿using System.Data.Common;
using System.Linq;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.AccountExt;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IAccount;
using UMS_MVC.Manager.DAL.DAO.Repository.RAccount;

namespace UMS_MVC.Manager.DAL.DAO
{
    public class AccountExtProjectManager
    {
        private readonly IAccountExtProjectRepository _accountExtProjectRepository;

        public AccountExtProjectManager()
        {
            _accountExtProjectRepository=new AccountExtProjectRepository(new UmsDbContext());
        }

        public IQueryable<AccountExtProject> GetAllAccountExtProjects()
        {
            return _accountExtProjectRepository.GetAllAccountExtProjects();
        }

        public AccountExtProject GetSingleAccountExtProject(int id)
        {
            return _accountExtProjectRepository.GetSingleAccountExtProject(id);
        }

        public void Insert(AccountExtProject accountExtProject)
        {
            _accountExtProjectRepository.Insert(accountExtProject);
        }

        public void Update(AccountExtProject accountExtProject)
        {
            _accountExtProjectRepository.Update(accountExtProject);
        }
        public string SaveData()
        {
            string msg;
            try
            {
                _accountExtProjectRepository.Save();
                msg = "Success";
            }
            catch (DbException)
            {

                msg = "Failed";
            }
            return msg;
        }
        public string Delete(int id)
        {

            string msg;
            try
            {
                _accountExtProjectRepository.Delete(id);
                msg = "Success";
            }
            catch (DbException)
            {

                msg = "Failed";
            }
            return msg;
        }


    }
}