﻿using System.Data.Entity;
using System.Linq;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO.Intefaces;
using UMS_MVC.Manager.DAL.DAO.Repository;

namespace UMS_MVC.Manager.DAL.DAO
{
    public class PowerWorkManager
    {
        private readonly IPowerWorkTypeRepository _powerWorkTypeRepository;
        private readonly IPowerWorkRepository _powerWorkAccessRepository;

        public PowerWorkManager()
        {
            
            //_dbContext.Configuration.ProxyCreationEnabled = false;
            _powerWorkAccessRepository = new PowerWorkRepository();
            _powerWorkTypeRepository = new PowerWorkTypeRepository();
        }

        public IQueryable<PowerWork> GetAllPowerWorks()
        {
            return _powerWorkTypeRepository.GetAll();
        }

        public PowerWork GetSinglePowerWork(int id)
        {
            return _powerWorkTypeRepository.FindSingle(id);
        }

        public void InsertPowerWork(PowerWork powerWork)
        {
            _powerWorkTypeRepository.Add(powerWork);
            _powerWorkTypeRepository.Save();
        }

        public void UpdatePowerWork(PowerWork powerWork)
        {
            _powerWorkTypeRepository.Update(powerWork);
            _powerWorkTypeRepository.Save();
        }

        public void DeletePowerwork(int id)
        {
            var findSingle = _powerWorkTypeRepository.FindSingle(id);
            _powerWorkTypeRepository.Delete(findSingle);
            _powerWorkTypeRepository.Save();
        }

        public IQueryable<PowerWorkAccess> GetAllPowerWorkAccesses()
        {
            return _powerWorkAccessRepository.GetAll().Include(s=>s.PowerWork).Include(s=>s.Account);
        }

        public PowerWorkAccess GetSinglePowerWorkAccess(int id)
        {
            return _powerWorkAccessRepository.FindSingle(id);
        }

        public void InsertPowerWorkAccess(PowerWorkAccess powerWorkAccess)
        {
            _powerWorkAccessRepository.Add(powerWorkAccess);
            _powerWorkAccessRepository.Save();
        }

        public void UpdatePowerWorkAccess(PowerWorkAccess powerWorkAccess)
        {
            _powerWorkAccessRepository.Update(powerWorkAccess);
            _powerWorkAccessRepository.Save();
        }

        public void DeletePowerWorkAccess(int id)
        {
            var powerWorkAccess = _powerWorkAccessRepository.FindSingle(id);
            _powerWorkAccessRepository.Delete(powerWorkAccess);
            _powerWorkAccessRepository.Save();
        }


    }
}