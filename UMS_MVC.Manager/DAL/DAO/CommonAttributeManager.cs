﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.DataModel.Models.UniversalTypes;
using UMS_MVC.ViewModels.ViewModels;
using UMS_MVC.ViewModels.ViewModels.Account;
using CourseStatus = UMS_MVC.DataModel.Models.UniversalTypes.CourseStatus;


namespace UMS_MVC.Manager.DAL.DAO
{
    public class CommonAttributeManager
    {
        private UmsDbContext db;

        public CommonAttributeManager()
        {
            db = new UmsDbContext();
        }

        public IEnumerable<Gender> GetAllGenders()
        {
            return db.Genders;
        }

        public IEnumerable<BloodGroups> GetAllBloodGroups()
        {
            return db.BloodGroupses;
        }

        public async Task<List<BloodGroups>> GetAllBloodGroupsesFiltered()
        {
            using (var db2 = new UmsDbContext())
            {
                db2.Configuration.ProxyCreationEnabled = false;
                db2.Configuration.LazyLoadingEnabled = false;
                return await db2.BloodGroupses.ToListAsync();
            }
        }
        public async Task<List<Gender>> GetAllGenderFiltered()
        {
            using (var db2 = new UmsDbContext())
            {
                db2.Configuration.ProxyCreationEnabled = false;
                db2.Configuration.LazyLoadingEnabled = false;
                return await db2.Genders.ToListAsync();
            }
        }
        public async Task<List<MaritalStatus>> GetAllMaritalStatusesFiltered()
        {
            using (var db2 = new UmsDbContext())
            {
                db2.Configuration.ProxyCreationEnabled = false;
                db2.Configuration.LazyLoadingEnabled = false;
                return await db2.MaritalStatuses.ToListAsync();
            }
        }
        public IEnumerable<MaritalStatus> GetAllMaritalStatuses()
        {
            return db.MaritalStatuses;
        }

        public IEnumerable<Year> GetAllYears()
        {
            return db.Years;
        }

        public IQueryable<CourseStatus> GetallCourseStatuses()
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.Configuration.LazyLoadingEnabled = false;
            return db.CourseStatuses;
        }

        public IEnumerable<GradingSystem> GetAllGradingSystems()
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.Configuration.LazyLoadingEnabled = false;
            return db.GradingSystems.ToList();
        }

        public GradingSystem GetSpecificGradingSystem(int gradingCode)
        {
            return db.GradingSystems.Find(gradingCode);
        }

        public List<AssociatedMarksByCourseStatus> GetAllGradesForExtraCourseStatus()
        {
            var statuslist = new List<AssociatedMarksByCourseStatus>
            {
                new AssociatedMarksByCourseStatus{CustGradeId=1,StatusId = 4,CourseStatus="Drop",CustomGrade = "W",ShortName = "Withdraw",CancelSection = true},
                new AssociatedMarksByCourseStatus{CustGradeId=2,StatusId = 8,CourseStatus="Incomplete",CustomGrade = "I",ShortName = "Incomplete"},
                new AssociatedMarksByCourseStatus{CustGradeId=3,StatusId = 7,CourseStatus="Non_credit",CustomGrade = "NCP",ShortName = "Non-Cr Pass"},
                new AssociatedMarksByCourseStatus{CustGradeId=4,StatusId = 7,CourseStatus="Non_credit",CustomGrade = "NCF",ShortName = "Non-Cr Failed"}
            };
            return statuslist;
        }

        public List<Terms> GetAllTerms()
        {
            var term = new List<Terms>
            {
                new Terms {TermId = 1, Term = "Mid Term"},
                new Terms {TermId = 2, Term = "Final Term"}
            };
            return term;
        }
        public List<ExtPublicationOrActivityType> GetAllAccountExtededPublicationTypes()
        {
            var pubTypes = new List<ExtPublicationOrActivityType>
            {
                new ExtPublicationOrActivityType {Name = "Journal Article"},
                new ExtPublicationOrActivityType {Name = "Book"},
                new ExtPublicationOrActivityType {Name = "Chapter"},
                new ExtPublicationOrActivityType {Name = "Conference"},
                new ExtPublicationOrActivityType {Name = "Report"}

            };
            return pubTypes;
        }


        public List<ExtPublicationOrActivityType> GetAllAccountActivitiesTypes()
        {
            var pubTypes = new List<ExtPublicationOrActivityType>
            {
                new ExtPublicationOrActivityType{Name = "Events/Conferences Attended"},
                new ExtPublicationOrActivityType{Name = "Other Activities"}
                ,
            };
            return pubTypes;
        }



    }
}