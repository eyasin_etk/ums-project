﻿using UMS_MVC.DataModel.Models.CreditTransfer;
using UMS_MVC.Manager.DAL.DAO.GenericService;

namespace UMS_MVC.Manager.DAL.DAO.Intefaces
{
    public interface ICreditTrasnsferStudentRepository:IGenericRepository<CreditTransferStudent>
    {
    }

    public interface ICreditTransferCoursesRepository:IGenericRepository<CreditTransferAcceptedCourse>
    {
         
    }
}
