﻿using UMS_MVC.DataModel.Models.Reported_Case;
using UMS_MVC.Manager.DAL.DAO.GenericService;

namespace UMS_MVC.Manager.DAL.DAO.Intefaces
{
    public interface IReportedStudentRepository:IGenericRepository<ReportedStudent>
    {
    }

    public interface IReportedPunishmentRepository:IGenericRepository<ReportedPunishment>
    {
    }
}
