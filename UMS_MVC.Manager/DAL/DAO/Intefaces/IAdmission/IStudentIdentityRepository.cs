﻿using System.Threading.Tasks;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO.GenericService;

namespace UMS_MVC.Manager.DAL.DAO.Intefaces.IAdmission
{
    public interface IStudentIdentityRepository : IGenericRepository<StudentIdentification>
    {
        string GetGeneratedId(StudentIdentification studentIdentification);
        Task<bool> CheckStudentExitanceByIdentity(int studentId);
        Task<bool> CheckStudentExitanceById(string studentId);

        void StudentIdSync(int infoStudentInfoId, int identityId);
    }
}
