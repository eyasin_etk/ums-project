﻿using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO.GenericService;

namespace UMS_MVC.Manager.DAL.DAO.Intefaces
{
    interface IGradingSystemRepository: IGenericRepository<GradingSystem>
    {
        //IQueryable<GradingSystem> GetAllGradingSystems();
        //GradingSystem FindGradingSystem(int id);
        //void Insert(GradingSystem gradingSystem);
        //void Update(GradingSystem gradingSystem);
        //void Delete(int id);
        //void Save();
    }
}
