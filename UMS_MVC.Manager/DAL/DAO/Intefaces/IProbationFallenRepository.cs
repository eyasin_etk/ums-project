﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UMS_MVC.DataModel.Models.Probation;
using UMS_MVC.Manager.DAL.DAO.GenericService;

namespace UMS_MVC.Manager.DAL.DAO.Intefaces
{
    public interface IProbationFallenRepository:IGenericRepository<ProbationFallenStudent>
    {
    }

    public interface IProbationCategoryRepository:IGenericRepository<ProbationCategory>
    {
         
    }
}
