﻿using System;
using System.Threading.Tasks;
using UMS_MVC.DataModel.Models.PreAdmission;
using UMS_MVC.Manager.DAL.DAO.GenericService;

namespace UMS_MVC.Manager.DAL.DAO.Intefaces
{
    public interface IPreAdmissionStudentInfoRepository:IGenericRepository<PreAdmissionStudentInfo>
    {
        Task<PreAdmissionStudentInfo> FindSingleInfoGuidAsync(Guid id);
        PreAdmissionStudentInfo FinSingleGuid(Guid? id);
        bool CheckAdmitted(Guid? id);
        void ConfirmAdmission(Guid? studentGuid);

        string GetExistStudentImage(Guid id);
        Task<bool> CheckExistingStudent(Guid id);
        Task<bool> CheckDuplicateByPhone(string phoneNo);
    }

    public interface IPreAdmissionAcademicRepository : IGenericRepository<PreAdmissionAcademic>
    {
        //int Alter(PreAdmissionAcademic academic);
        Task<bool> CheckDuplicateExamination(Guid preAdmissionStudentInfoId, string nameOfExam);
        Task<int> CountAcademicInfoes(Guid preAdmissionStudentInfoId);
    }

}
