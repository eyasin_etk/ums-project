﻿using UMS_MVC.DataModel.Models.Student_Tags;
using UMS_MVC.Manager.DAL.DAO.GenericService;

namespace UMS_MVC.Manager.DAL.DAO.Intefaces
{
    public interface IStudentPerformanceAssistRepository:IGenericRepository<StudentPerformanceAssist>
    {
    }
}
