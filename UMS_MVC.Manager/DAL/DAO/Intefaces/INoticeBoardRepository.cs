﻿using UMS_MVC.DataModel.Models.Notice_Matter;
using UMS_MVC.Manager.DAL.DAO.GenericService;

namespace UMS_MVC.Manager.DAL.DAO.Intefaces
{
    interface INoticeBoardRepository: IGenericRepository<NoticeBoardAttatchedFiles>
    {
        
    }

    interface INoticeBoardCategoryRepository : IGenericRepository<NoticeBoardCategory>
    {

    }

    internal interface INoticeBoardDetailsRepository : IGenericRepository<NoticeBoardDetail>
    {

    }

}