﻿using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO.GenericService;

namespace UMS_MVC.Manager.DAL.DAO.Intefaces
{
    public interface IControlFeatureRepository:IGenericRepository<ControlFeatures>
    {
        
    }

}
