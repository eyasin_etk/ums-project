﻿using UMS_MVC.DataModel.Models.UniversalTypes;
using UMS_MVC.Manager.DAL.DAO.GenericService;

namespace UMS_MVC.Manager.DAL.DAO.Intefaces
{
    public interface IStudentGroupRepository:IGenericRepository<StudentGroup>
    {
    }
}
