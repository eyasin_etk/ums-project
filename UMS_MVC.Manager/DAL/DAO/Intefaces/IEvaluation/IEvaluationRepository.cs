﻿using UMS_MVC.DataModel.Models.TeacherEvaluation;
using UMS_MVC.Manager.DAL.DAO.GenericService;

namespace UMS_MVC.Manager.DAL.DAO.Intefaces.IEvaluation
{
    interface IEvaluationRepository : IGenericRepository<EvaluationConfiguration>
    {
       
    }
    interface IEvaluationResultRepository : IGenericRepository<EvaluationResult>
    {
    }
}
