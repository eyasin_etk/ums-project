﻿using UMS_MVC.DataModel.Models.TeacherEvaluation;
using UMS_MVC.Manager.DAL.DAO.GenericService;

namespace UMS_MVC.Manager.DAL.DAO.Intefaces.IEvaluation
{
     interface ISurveyAnswerRepository : IGenericRepository<SurveyAnswer>
    {
        
    }

    interface ISurveyAssesmentRepository : IGenericRepository<SurveryAssesment>
    {

    }

    interface ISurveyQuestionRepository : IGenericRepository<SurveyQuestion>
    {

    }
}
