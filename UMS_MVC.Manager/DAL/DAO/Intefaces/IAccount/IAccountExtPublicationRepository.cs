﻿using System;
using System.Linq;
using UMS_MVC.DataModel.Models.AccountExt;

namespace UMS_MVC.Manager.DAL.DAO.Intefaces.IAccount
{
    interface IAccountExtPublicationRepository:IDisposable
    {
        IQueryable<AccountExtPublication> GetAllAccountExtPublications();
        AccountExtPublication GetSingle(int id);
        void Insert(AccountExtPublication accountExtPublication);
        void Update(AccountExtPublication accountExtPublication);
        void Delete(int id);
        void Save();
    }
}
