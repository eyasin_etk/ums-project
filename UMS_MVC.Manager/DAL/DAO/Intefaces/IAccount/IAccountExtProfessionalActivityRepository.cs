﻿using System;
using System.Linq;
using UMS_MVC.DataModel.Models.AccountExt;

namespace UMS_MVC.Manager.DAL.DAO.Intefaces.IAccount
{
    interface IAccountExtProfessionalActivityRepository:IDisposable
    {
        IQueryable<AccountExtProfessionalActivity> GetAllAccountExtProfessionalActivities();
        AccountExtProfessionalActivity GetSingleAccountExtProfessionalActivity(int id);
        void Insert(AccountExtProfessionalActivity accountExtEducation);
        void Update(AccountExtProfessionalActivity accountExtEducation);
        void Delete(int id);
        void Save();
    }
}
