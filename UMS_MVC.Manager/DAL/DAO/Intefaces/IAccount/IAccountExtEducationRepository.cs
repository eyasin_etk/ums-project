﻿using System;
using System.Linq;
using UMS_MVC.DataModel.Models.AccountExt;

namespace UMS_MVC.Manager.DAL.DAO.Intefaces.IAccount
{
    interface IAccountExtEducationRepository:IDisposable
    {
        IQueryable<AccountExtEducation> GetAllAccountExtEducations();
        AccountExtEducation GetSinglExtEducation(int id);
        void Insert(AccountExtEducation accountExtEducation);
        void Update(AccountExtEducation accountExtEducation);
        void Delete(int id);
        void Save();
    }
}