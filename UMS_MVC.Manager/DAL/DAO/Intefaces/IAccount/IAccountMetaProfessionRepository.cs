﻿using System;
using System.Linq;
using UMS_MVC.DataModel.Models.AccountExt;

namespace UMS_MVC.Manager.DAL.DAO.Intefaces.IAccount
{
    interface IAccountMetaProfessionRepository:IDisposable
    {
        IQueryable<AccountMetaProfessional> GetAllAccountMetaInformationRepositories();
        AccountMetaProfessional GetSingleAccountMetaInformationRepository(int id);
        void Insert(AccountMetaProfessional accountMetaInformation);
        void Update(AccountMetaProfessional accountMetaInformation);
        void Delete(int id);
        void Save();
    }
}
