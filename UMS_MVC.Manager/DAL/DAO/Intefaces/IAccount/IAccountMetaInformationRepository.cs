﻿using System;
using System.Linq;
using UMS_MVC.DataModel.Models.AccountExt;

namespace UMS_MVC.Manager.DAL.DAO.Intefaces.IAccount
{
    interface IAccountMetaInformationRepository:IDisposable
    {
        IQueryable<AccountMetaInformation> GetAllAccountMetaInformations();
        AccountMetaInformation GetSingleAccountMetaInformation(int id);
        void Insert(AccountMetaInformation accountMetaInformation);
        void Update(AccountMetaInformation accountMetaInformation);
        void Delete(int id);
        void Save();
    }
}
