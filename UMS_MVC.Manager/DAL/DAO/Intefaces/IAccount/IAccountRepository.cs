﻿using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO.GenericService;

namespace UMS_MVC.Manager.DAL.DAO.Intefaces.IAccount
{
    public interface IAccountRepository:IGenericRepository<Account>
    {
    //    IQueryable<Account> GetAllAccounts();
    //   Account GetSingleAccount(int id);
    //    void Update(Account account);
    //    void Insert(Account account);
    //    void Delete(int id);
    //    void Save();
    }
}