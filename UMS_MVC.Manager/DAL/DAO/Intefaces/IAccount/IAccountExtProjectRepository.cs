﻿using System;
using System.Linq;
using UMS_MVC.DataModel.Models.AccountExt;

namespace UMS_MVC.Manager.DAL.DAO.Intefaces.IAccount
{
    interface IAccountExtProjectRepository:IDisposable
    {
        IQueryable<AccountExtProject> GetAllAccountExtProjects();
        AccountExtProject GetSingleAccountExtProject(int id);
        void Insert(AccountExtProject accountExtProject);
        void Update(AccountExtProject accountExtProject);
        void Delete(int id);
        void Save();
    }
}
