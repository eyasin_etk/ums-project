﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO.Intefaces;
using UMS_MVC.Manager.DAL.DAO.Repository;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.Manager.DAL.DAO
{
    public class GradeManager
    {
        private UmsDbContext db;
        private readonly CourseForStudentManger _courseForStudentManger;
        private readonly SemesterManager _semesterManager;
        private readonly CommonAttributeManager _commonAttributeManager;
        private readonly ICourseForStudentRepository _courseForStudentRepository;
        public GradeManager()
        {
           db=new UmsDbContext();
           _courseForStudentManger = new CourseForStudentManger();
           _semesterManager = new SemesterManager();
           _commonAttributeManager = new CommonAttributeManager();
            _courseForStudentRepository=new CourseForStudentRepository();

        }

        public CourseForStudentsAcademic CalculateGradeUpdated(CourseForStudentsAcademic courseForStudentsAcademic, int gradingCode = 1)
        {
            double calculateTotal = 0;
            var initialMarks = FixMarks(courseForStudentsAcademic,gradingCode);
            var calculateAcademic = UpdatedActualMark(initialMarks,gradingCode);
            calculateTotal = calculateAcademic.TotalMark;
            double grade = 0;
            string gradeLetter = string.Empty;
            if (calculateTotal >= 0 && calculateTotal < 40)
            {
                grade = 0.0;
                gradeLetter = "F";
            }
            else if (calculateTotal >= 40 && calculateTotal < 45)
            {
                grade = 2.0;
                gradeLetter = "D";
            }
            else if (calculateTotal >= 45 && calculateTotal < 50)
            {
                grade = 2.25;
                gradeLetter = "C";
            }
            else if (calculateTotal >= 50 && calculateTotal < 55)
            {
                grade = 2.5;
                gradeLetter = "C+";
            }
            else if (calculateTotal >= 55 && calculateTotal < 60)
            {
                grade = 2.75;
                gradeLetter = "B-";
            }
            else if (calculateTotal >= 60 && calculateTotal < 65)
            {
                grade = 3.0;
                gradeLetter = "B";
            }
            else if (calculateTotal >= 65 && calculateTotal < 70)
            {
                grade = 3.25;
                gradeLetter = "B+";
            }
            else if (calculateTotal >= 70 && calculateTotal < 75)
            {
                grade = 3.5;
                gradeLetter = "A-";
            }
            else if (calculateTotal >= 75 && calculateTotal < 80)
            {
                grade = 3.75;
                gradeLetter = "A";
            }
            else if (calculateTotal >= 80)
            {
                grade = 4.0;
                gradeLetter = "A+";
            }
            courseForStudentsAcademic.Attendance = calculateAcademic.Attendance;
            courseForStudentsAcademic.ClassTest = calculateAcademic.ClassTest;
            courseForStudentsAcademic.Midterm = calculateAcademic.Midterm;
            courseForStudentsAcademic.FinalTerm = calculateAcademic.FinalTerm;

            var getCourseInfo = db.CourseForDepartments.Find(courseForStudentsAcademic.CourseForDepartmentId).Credit;
            courseForStudentsAcademic.TotalMark = calculateTotal;
            courseForStudentsAcademic.Grade = grade;
            courseForStudentsAcademic.LetterGrade = gradeLetter;
            courseForStudentsAcademic.TotalGrade = grade * getCourseInfo;
            courseForStudentsAcademic.GradingSystemId = gradingCode;
            return courseForStudentsAcademic;
        }
        
        public CourseForStudentsAcademic FixMarks(CourseForStudentsAcademic courseForStudentsAcademic, int gradingCode)
        {
            var getCourseGradings = _commonAttributeManager.GetSpecificGradingSystem(gradingCode);
            if (getCourseGradings.Attendance == 0)
            {
                courseForStudentsAcademic.Attendance = 0;
            }
            if (getCourseGradings.ClassTest == 0)
            {
                courseForStudentsAcademic.ClassTest = 0;
            }
            if (getCourseGradings.MidTerm == 0)
            {
                courseForStudentsAcademic.Midterm = 0;
            }
            return courseForStudentsAcademic;
        }

        public CourseForStudentsAcademic UpdatedActualMark(CourseForStudentsAcademic academic, int gradingCode)
        {
            var getCourseGradings = _commonAttributeManager.GetSpecificGradingSystem(gradingCode);
            double att = 0;
            double classTest = 0;
            double mid = 0;
            double final = 0;
            if (academic.Attendance > 0)
            {
                att = (academic.Attendance / getCourseGradings.Attendance) * getCourseGradings.AttendancePercent;
                if (double.IsNaN(att))
                {
                    att = 0;
                }
            }
            if (academic.ClassTest > 0)
            {
                classTest = (academic.ClassTest / getCourseGradings.ClassTest) * getCourseGradings.ClassTestPercent;
                if (double.IsNaN(classTest))
                {
                    classTest = 0;
                }
            }
            if (academic.Midterm > 0)
            {
                mid = (academic.Midterm / getCourseGradings.MidTerm) * getCourseGradings.MidTermPercent;
                if (double.IsNaN(mid))
                {
                    mid = 0;
                }
            }
            if (academic.FinalTerm > 0)
            {
                final = (academic.FinalTerm / getCourseGradings.FinalTerm) * getCourseGradings.FinalTermPercent;
                if (double.IsNaN(final))
                {
                    final = 0;
                }
            }
            var totalMark = att + classTest + mid + final;
            //academic.Attendance = att;
            //academic.ClassTest = classTest;
            //academic.Midterm = mid;
            //academic.FinalTerm = final;
            academic.TotalMark = Math.Round(totalMark, 0, MidpointRounding.AwayFromZero);
           // return att + classTest + mid + final;
            return academic;

        }
        
        public CourseForStudentsAcademic CalculateGrade(CourseForStudentsAcademic courseForStudentsAcademic,string courseType)
        {
            double calculateTotal;
            switch (courseType)
            {
                case "LAB":
                    calculateTotal = LabActualMark(courseForStudentsAcademic);
                    break;
                case "OTHER":
                    calculateTotal = OtherActualMark(courseForStudentsAcademic);
                    break;
                case "PHARMA_CORE":
                    calculateTotal = PharmacyCoreActualMark(courseForStudentsAcademic);
                    break;
                case "MBA_CORE":
                    calculateTotal = PharmacyCoreActualMark(courseForStudentsAcademic);
                    break;
                case "CORE_PAU":
                    calculateTotal = PharmacyCoreActualMark(courseForStudentsAcademic);
                    break;
                default:
                    calculateTotal = ActualMark(courseForStudentsAcademic);
                    break;
            }
            calculateTotal = Math.Round(calculateTotal, 0, MidpointRounding.AwayFromZero);
            double grade = 0;
            string gradeLetter = string.Empty;
            if (calculateTotal >= 0 && calculateTotal < 40)
            {
                grade = 0.0;
                gradeLetter = "F";
            }
            else if (calculateTotal >=40  && calculateTotal < 45)
            {
                grade = 2.0;
                gradeLetter = "D";
            }
            else if (calculateTotal >= 45 && calculateTotal < 50)
            {
                grade = 2.25;
                gradeLetter = "C";
            }
            else if (calculateTotal >= 50 && calculateTotal <55)
            {
                grade = 2.5;
                gradeLetter = "C+";
            }
            else if (calculateTotal >= 55 && calculateTotal <60)
            {
                grade = 2.75;
                gradeLetter = "B-";
            }
            else if (calculateTotal >= 60 && calculateTotal <65)
            {
                grade = 3.0;
                gradeLetter = "B";
            }
            else if (calculateTotal >= 65 && calculateTotal <70)
            {
                grade = 3.25;
                gradeLetter = "B+";
            }
            else if (calculateTotal >= 70 && calculateTotal < 75)
            {
                grade = 3.5;
                gradeLetter = "A-";
            }
            else if (calculateTotal >= 75 && calculateTotal < 80)
            {
                grade = 3.75;
                gradeLetter = "A";
            }
            else if (calculateTotal >= 80 )
            {
                grade = 4.0;
                gradeLetter = "A+";
            }
            var getCourseInfo = db.CourseForDepartments.Find(courseForStudentsAcademic.CourseForDepartmentId).Credit;
            courseForStudentsAcademic.TotalMark = calculateTotal;
            courseForStudentsAcademic.Grade = grade;
            courseForStudentsAcademic.LetterGrade=gradeLetter;
            courseForStudentsAcademic.TotalGrade = grade*getCourseInfo;
            return courseForStudentsAcademic;
        }
        
        public double ActualMark(CourseForStudentsAcademic courseForStudentsAcademic)
        {
            double att = 0;
            double classTest = 0;
            double mid = 0;
            double final = 0;
            if (courseForStudentsAcademic.Attendance>0)
            {
                att = (courseForStudentsAcademic.Attendance/10)*5;
            }
            if (courseForStudentsAcademic.ClassTest > 0)
            {
                classTest = (courseForStudentsAcademic.ClassTest/50)*25;
            }
            if (courseForStudentsAcademic.Midterm > 0)
            {
                mid = (courseForStudentsAcademic.Midterm/100)*20;
            }
            if (courseForStudentsAcademic.FinalTerm > 0)
            {
                final = (courseForStudentsAcademic.FinalTerm/100)*50;
            }
            return att + classTest + mid + final;
        }

        public double PharmacyCoreActualMark(CourseForStudentsAcademic courseForStudentsAcademic)
        {
            double att = 0;
            double classTest = 0;
            double mid = 0;
            double final = 0;
            if (courseForStudentsAcademic.Attendance > 0)
            {
                att = courseForStudentsAcademic.Attendance;
            }
            if (courseForStudentsAcademic.ClassTest > 0)
            {
                classTest = courseForStudentsAcademic.ClassTest ;
            }
            if (courseForStudentsAcademic.Midterm > 0)
            {
                mid = courseForStudentsAcademic.Midterm;
            }
            if (courseForStudentsAcademic.FinalTerm > 0)
            {
                final = courseForStudentsAcademic.FinalTerm ;
            }
            return att + classTest + mid + final;
        }

        public double LabActualMark(CourseForStudentsAcademic courseForStudentsAcademic)
        {
            double mid = 0;
            double final = 0;
            
            if (courseForStudentsAcademic.Midterm > 0)
            {
                mid = (courseForStudentsAcademic.Midterm / 100) * 40;
            }
            if (courseForStudentsAcademic.FinalTerm > 0)
            {
                final = (courseForStudentsAcademic.FinalTerm / 100) * 60;
            }
            return mid + final;
        }

        public double OtherActualMark(CourseForStudentsAcademic courseForStudentsAcademic)
        {
            double final = 0;
           
            if (courseForStudentsAcademic.FinalTerm > 0)
            {
                final = courseForStudentsAcademic.FinalTerm;
            }
            return final;
        }
        
        //public CumulativeScores CalculateCgpaofAStudent(int studentIdenId = 0)
        //{
        //    var getAllCourseOfThisStudent =_courseForStudentManger
        //        .GetAllCourseByStudent(studentIdenId);
        //    double cgpaCal = 0, totalCredit = 0, totalGrade = 0.0;
        //    double x = 0, y = 0;
        //    double creditEarned = 0, takenCredit = 0;
        //    double courseCompleted = 0;
            
        //    foreach (var courseForStudentsAcademic in getAllCourseOfThisStudent)
        //    {
        //        var creditVal = courseForStudentsAcademic.CourseForDepartment.Credit;
        //        takenCredit += creditVal;
        //        if (courseForStudentsAcademic.CourseStatusId == 2)
        //        {
                   
        //            x = x + creditVal;
        //            y = y + (courseForStudentsAcademic.Grade*creditVal);
        //            if (courseForStudentsAcademic.LetterGrade == "F") continue;
        //            creditEarned += creditVal;
        //            courseCompleted += 1;
        //        }
        //        else
        //        {
        //            x = x + 0;
        //            y = y + 0;
        //        }
        //    }
        //    totalCredit += x;
        //    totalGrade += y;
        //    cgpaCal = Math.Round(totalGrade / totalCredit, 2, MidpointRounding.AwayFromZero);
        //    if (double.IsNaN(cgpaCal))
        //    {
        //        cgpaCal = 0;
        //    }
        //    var cumulatives = new CumulativeScores
        //    {
        //        Credits = takenCredit,
        //        CreditEarned = creditEarned,
        //        Cgpa = cgpaCal,
        //        CourseCompleted = courseCompleted,
        //        TotalCredit = x,
        //        TotalGrade = y,
        //        StudentId = studentIdenId
        //    };
        //    return cumulatives;
        //}
        public CumulativeScores CalculateCgpaofAStudent(int studentIdenId = 0)
        {
            var courseForStudentsAcademics = _courseForStudentRepository
                .GetAll().Include(s=>s.CourseForDepartment).Where(s => s.StudentIdentificationId == studentIdenId).AsNoTracking();
            var getAllCourseOfThisStudent = (from course in courseForStudentsAcademics
                select new
                {
                    course.CourseStatusId,
                    course.Grade,
                    course.LetterGrade,
                    course.CourseForDepartment.Credit
                }).ToList();
            
            double cgpaCal = 0, totalCredit = 0, totalGrade = 0.0;
            double x = 0, y = 0;
            double creditEarned = 0, takenCredit = 0;
            double courseCompleted = 0;

            foreach (var courseForStudentsAcademic in getAllCourseOfThisStudent)
            {
                var creditVal = courseForStudentsAcademic.Credit;
                takenCredit += creditVal;
                if (courseForStudentsAcademic.CourseStatusId == 2)
                {

                    x = x + creditVal;
                    y = y + (courseForStudentsAcademic.Grade * creditVal);
                    if (courseForStudentsAcademic.LetterGrade == "F") continue;
                    creditEarned += creditVal;
                    courseCompleted += 1;
                }
                else
                {
                    x = x + 0;
                    y = y + 0;
                }
            }
            totalCredit += x;
            totalGrade += y;
            cgpaCal = Math.Round(totalGrade / totalCredit, 2, MidpointRounding.AwayFromZero);
            if (double.IsNaN(cgpaCal))
            {
                cgpaCal = 0;
            }
            var cumulatives = new CumulativeScores
            {
                Credits = takenCredit,
                CreditEarned = creditEarned,
                Cgpa = cgpaCal,
                CourseCompleted = courseCompleted,
                TotalCredit = x,
                TotalGrade = y,
                StudentId = studentIdenId
            };
            return cumulatives;
        }

        public CumulativeScores CalulateScoresBySemester(int studentIdenId = 0, int semesterId = 0)
        {
            var getAllCgpas = CalculateCgpaAllSemester(studentIdenId).FirstOrDefault(s => s.SemesterId == semesterId);
            return getAllCgpas;
            //var getAllCourseOfThisStudent = _courseForStudentManger
            //    .GetAllCourseByStudent(studentIdenId).Where(s => s.SemesterId == semesterId);
            //var getCgpaOfThisStudent = CalculateCgpaofAStudent(studentIdenId);

            //double x = 0, y = 0, z = 0;
            //double creditEarned = 0, takenCredit = 0;
            //double courseCompleted = 0;
            //foreach (var courseForStudentsAcademic in getAllCourseOfThisStudent)
            //{
            //    double creditVal = courseForStudentsAcademic.CourseForDepartment.Credit;
            //    takenCredit += creditVal;
            //    if (courseForStudentsAcademic.CourseStatusId == 2)
            //    {
            //        x = x + creditVal;
            //        y = y + courseForStudentsAcademic.TotalGrade;
            //        if (courseForStudentsAcademic.LetterGrade == "F") continue;
            //        creditEarned += creditVal;
            //        courseCompleted += 1;
            //    }
            //    else
            //    {
            //        x = x + 0;
            //        y = y + 0;
            //    }
            //}

            //z = Math.Round(y / x, 2);
            //if (double.IsNaN(z))
            //{
            //    z = 0;
            //}
            //var cumulatives = new CumulativeScores
            //{
            //    Credits = takenCredit,
            //    CreditEarned = creditEarned,
            //    SemesterGpa = z,
            //    Cgpa = getCgpaOfThisStudent.Cgpa,
            //    CourseCompleted = courseCompleted,
            //    SemesterId = semesterId,
            //    TotalCredit = x,
            //    TotalGrade = y,
            //    StudentId = studentIdenId
            //};
            //return cumulatives;
        }
        //public CumulativeScores CalulateScoresBySemester(int studentIdenId = 0, int semesterId = 0)
        //{
        //    var getAllCourseOfThisStudent = _courseForStudentManger.GetAllCourseByStudent(studentIdenId).Where(s => s.SemesterId == semesterId);
        //    double credits = 0, grades = 0, creditEarned = 0, courseCompleted = 0;

        //    foreach (var courseForStudentsAcademic in getAllCourseOfThisStudent)
        //    {
        //        if (courseForStudentsAcademic.CourseStatusId == 2)
        //        {
        //            credits = credits + courseForStudentsAcademic.CourseForDepartment.Credit;
        //            grades = grades + courseForStudentsAcademic.TotalGrade;
        //            if (courseForStudentsAcademic.LetterGrade != "F")
        //            {
        //                creditEarned += courseForStudentsAcademic.CourseForDepartment.Credit;
        //                courseCompleted += 1;
        //            }
        //        }
        //    }
        //    var cgpa = Math.Round(grades / credits, 2);
        //    if (double.IsNaN(cgpa))
        //    {
        //        cgpa = 0;
        //    }
        //    var cumulatives = new CumulativeScores
        //    {
        //        Cgpa = cgpa,
        //        Credits = credits,
        //        CreditEarned = creditEarned,
        //        CourseCompleted = courseCompleted
        //    };
        //    return cumulatives;
        //}
        //public List<CumulativeScores> CalculateCgpaAllSemester(int studentIdenId = 0)
        //{
        //    var cumulativeSemesters = new List<CumulativeScores>();
        //    var getAllCourseOfThisStudent = _courseForStudentManger.GetAllCourseByStudent(studentIdenId);
        //    double cgpaCal = 0, totalCredit = 0.0, totalGrade = 0.0, cumulativeCredits = 0;
        //    foreach (var cgpaSemester in getAllCourseOfThisStudent.Select(a => a.Semester.SemesterId).Distinct())
        //    {
        //        double x = 0, y = 0, z = 0;
        //        double creditEarned = 0, takenCredit = 0;
        //        double courseCompleted = 0;
        //        foreach (
        //            var courseForStudentsAcademic in getAllCourseOfThisStudent.Where(s => s.SemesterId == cgpaSemester))
        //        {
        //            double creditVal =0;
        //            // for dropped Course, invalid, non credit, incomplete
        //            int courseStatusId = courseForStudentsAcademic.CourseStatusId;
        //            if ( courseStatusId == 4)
        //            {
        //                takenCredit += 0;
        //            }
        //            else
        //            {
        //                creditVal = courseForStudentsAcademic.CourseForDepartment.Credit;
        //                takenCredit += creditVal;
        //            }
        //            if (courseForStudentsAcademic.CourseStatusId == 2)
        //            {

        //                x = x + creditVal;
        //                y = y + courseForStudentsAcademic.TotalGrade;
        //                string letterMark = courseForStudentsAcademic.LetterGrade;
        //                if (letterMark != "F" && !string.IsNullOrEmpty(letterMark))
        //                {
        //                    creditEarned += creditVal;
        //                    courseCompleted += 1;
        //                }
        //            }
        //            else
        //            {
        //                x = x + 0;
        //                y = y + 0;
        //            }

        //        }
        //        cumulativeCredits += creditEarned;
        //        totalCredit += x;
        //        totalGrade += y;
        //        z = Math.Round(y / x, 2, MidpointRounding.AwayFromZero);
        //        if (double.IsNaN(z))
        //        {
        //            z = 0;
        //        }
        //        cgpaCal = Math.Round(totalGrade / totalCredit, 2, MidpointRounding.AwayFromZero);
        //        if (double.IsNaN(cgpaCal))
        //        {
        //            cgpaCal = 0;
        //        }
        //        var cumulatives = new CumulativeScores
        //        {
        //            Credits = takenCredit,
        //            CreditEarned = creditEarned,
        //            SemesterGpa = z,
        //            Cgpa = cgpaCal,
        //            CourseCompleted = courseCompleted,
        //            SemesterId = cgpaSemester,
        //            TotalCredit = x,
        //            TotalGrade = y,
        //            StudentId = studentIdenId,
        //            CumulativeCredits = cumulativeCredits
        //        };
        //        cumulativeSemesters.Add(cumulatives);
        //    }
        //    return cumulativeSemesters;
        //}
        //public List<CumulativeScores> CalculateCgpaAllSemester(int studentIdenId = 0)
        //{
        //    var cumulativeSemesters = new List<CumulativeScores>();
        //    var getAllCourseOfThisStudent = _courseForStudentManger.GetAllCourseByStudent(studentIdenId);
        //    double cgpaCal = 0, totalCredit = 0.0, totalGrade = 0.0, cumulativeCredits = 0;
        //    double allCreditTaken = 0, allGrades = 0, allCreditEarned = 0, allcreditToBeCount = 0;
        //    foreach (var cgpaSemester in getAllCourseOfThisStudent.Select(a => a.Semester.SemesterId).Distinct())
        //    {
        //        double creditEarnedIndvSemester = 0, takenCreditIndvSemester = 0;
        //        double gradesIndvSemester = 0;
        //        double courseCompleted = 0;
        //        double currentGpa = 0;
        //        double creditToBeCount = 0;
        //        foreach (
        //            var courseForStudentsAcademic in getAllCourseOfThisStudent.Where(s => s.SemesterId == cgpaSemester))
        //        {
        //            var creditVal = courseForStudentsAcademic.CourseForDepartment.Credit;
        //            int courseStatusId = courseForStudentsAcademic.CourseStatusId;

        //#region Taken Credit
        //            if (courseStatusId == 4)
        //            {
        //                takenCreditIndvSemester += 0;
        //            }
        //            else
        //            {
        //                takenCreditIndvSemester += creditVal;
        //            }
        //#endregion

        //            if (courseForStudentsAcademic.CourseStatusId == 2)
        //            {
        //                gradesIndvSemester = gradesIndvSemester + (courseForStudentsAcademic.Grade*creditVal);
        //                creditToBeCount += creditVal;
        //                string letterMark = courseForStudentsAcademic.LetterGrade;
        //                if (letterMark != "F" && !string.IsNullOrEmpty(letterMark))
        //                {
        //                    creditEarnedIndvSemester += creditVal;
        //                    courseCompleted += 1;
        //                 }
        //            }
        //        }
        //        currentGpa = Math.Round(gradesIndvSemester / creditToBeCount, 2, MidpointRounding.AwayFromZero);
        //        if (double.IsNaN(currentGpa))
        //        {
        //            currentGpa = 0;
        //        }

        //        allGrades += gradesIndvSemester;
        //        allCreditEarned += creditEarnedIndvSemester;
        //        allCreditTaken += takenCreditIndvSemester;
        //        allcreditToBeCount += creditToBeCount;
        //        cgpaCal = Math.Round(allGrades / allcreditToBeCount, 2, MidpointRounding.AwayFromZero);
        //        if (double.IsNaN(cgpaCal))
        //        {
        //            cgpaCal = 0;
        //        }
        //        var cumulatives = new CumulativeScores
        //        {
        //            Credits = takenCreditIndvSemester,
        //            CreditEarned = creditEarnedIndvSemester,
        //            SemesterGpa = currentGpa,
        //            Cgpa = cgpaCal,
        //            CourseCompleted = courseCompleted,
        //            SemesterId = cgpaSemester,
        //            TotalCredit = allCreditTaken,
        //            TotalGrade = gradesIndvSemester,
        //            StudentId = studentIdenId,
        //            CumulativeCredits = allCreditEarned
        //        };
        //        cumulativeSemesters.Add(cumulatives);
        //    }
        //    return cumulativeSemesters;
        //}
        public List<CumulativeScores> CalculateCgpaAllSemester(int studentIdenId = 0)
        {
            var cumulativeSemesters = new List<CumulativeScores>();

            var courseForStudentsAcademics = _courseForStudentRepository
               .GetAll().Include(s => s.CourseForDepartment).Where(s => s.StudentIdentificationId == studentIdenId).AsNoTracking();
            var getAllCourseOfThisStudent = (from course in courseForStudentsAcademics
                                             select new
                                             {
                                                 course.CourseStatusId,
                                                 course.Grade,
                                                 course.LetterGrade,
                                                 course.CourseForDepartment.Credit,
                                                 course.SemesterId,
                                             }).ToList();
           
            double cgpaCal = 0, totalCredit = 0.0, totalGrade = 0.0, cumulativeCredits = 0;
            double allCreditTaken = 0, allGrades = 0, allCreditEarned = 0, allcreditToBeCount = 0;
            foreach (var cgpaSemester in getAllCourseOfThisStudent.Select(a => a.SemesterId).Distinct())
            {
                double creditEarnedIndvSemester = 0, takenCreditIndvSemester = 0;
                double gradesIndvSemester = 0;
                double courseCompleted = 0;
                double currentGpa = 0;
                double creditToBeCount = 0;
                foreach (
                    var courseForStudentsAcademic in getAllCourseOfThisStudent.Where(s => s.SemesterId == cgpaSemester))
                {
                    var creditVal = courseForStudentsAcademic.Credit;
                    int courseStatusId = courseForStudentsAcademic.CourseStatusId;

                    #region Taken Credit
                    if (courseStatusId == 4)
                    {
                        takenCreditIndvSemester += 0;
                    }
                    else
                    {
                        takenCreditIndvSemester += creditVal;
                    }
                    #endregion

                    if (courseForStudentsAcademic.CourseStatusId == 2)
                    {
                        gradesIndvSemester = gradesIndvSemester + (courseForStudentsAcademic.Grade * creditVal);
                        creditToBeCount += creditVal;
                        string letterMark = courseForStudentsAcademic.LetterGrade;
                        if (letterMark != "F" && !string.IsNullOrEmpty(letterMark))
                        {
                            creditEarnedIndvSemester += creditVal;
                            courseCompleted += 1;
                        }
                    }
                }
                currentGpa = Math.Round(gradesIndvSemester / creditToBeCount, 2, MidpointRounding.AwayFromZero);
                if (double.IsNaN(currentGpa))
                {
                    currentGpa = 0;
                }

                allGrades += gradesIndvSemester;
                allCreditEarned += creditEarnedIndvSemester;
                allCreditTaken += takenCreditIndvSemester;
                allcreditToBeCount += creditToBeCount;
                cgpaCal = Math.Round(allGrades / allcreditToBeCount, 2, MidpointRounding.AwayFromZero);
                if (double.IsNaN(cgpaCal))
                {
                    cgpaCal = 0;
                }
                var cumulatives = new CumulativeScores
                {
                    Credits = takenCreditIndvSemester,
                    CreditEarned = creditEarnedIndvSemester,
                    SemesterGpa = currentGpa,
                    Cgpa = cgpaCal,
                    CourseCompleted = courseCompleted,
                    SemesterId = cgpaSemester,
                    TotalCredit = allCreditTaken,
                    TotalGrade = gradesIndvSemester,
                    StudentId = studentIdenId,
                    CumulativeCredits = allCreditEarned
                };
                cumulativeSemesters.Add(cumulatives);
            }
            return cumulativeSemesters;
        }
        public CumulativeScores CalculateCgpaSingleSemester(int studentIdenId = 0, int semesterId = 0)
        {
            //var getAllCourseOfThisStudent = _courseForStudentManger.GetAllCourseByStudent(studentIdenId);
            var courseForStudentsAcademics = _courseForStudentRepository
               .GetAll().Include(s => s.CourseForDepartment).Where(s => s.StudentIdentificationId == studentIdenId).AsNoTracking();
            var getAllCourseOfThisStudent = (from course in courseForStudentsAcademics
                                             select new
                                             {
                                                 course.CourseStatusId,
                                                 course.Grade,
                                                 course.LetterGrade,
                                                 course.CourseForDepartment.Credit,
                                                 course.SemesterId
                                             }).ToList();
            double takenCredit = 0, grades = 0, creditEarned = 0, courseCompleted = 0, sgpa=0;
            double creditToBecount = 0;
            foreach (var courseForStudentsAcademic in getAllCourseOfThisStudent.Where(s => s.SemesterId == semesterId))
            {
                var creditVal = courseForStudentsAcademic.Credit;
                int courseStatusId = courseForStudentsAcademic.CourseStatusId;
                if (courseStatusId==4 ) // updated this method for tuhin pervez gpa calculation update
                {
                    takenCredit += 0;
                }
                else
                {
                    takenCredit += creditVal;
                }
                if (courseForStudentsAcademic.CourseStatusId == 2 || courseForStudentsAcademic.CourseStatusId== 4 ||
                    courseForStudentsAcademic.CourseStatusId == 5 || courseForStudentsAcademic.CourseStatusId == 6 &&
                    !string.IsNullOrEmpty(courseForStudentsAcademic.LetterGrade))
                {
                    creditToBecount += creditVal;
                    grades = grades + (courseForStudentsAcademic.Grade*creditVal);
                    if (courseForStudentsAcademic.LetterGrade == "F") continue;
                    creditEarned += creditVal;
                    courseCompleted += 1;
                }
            }
            sgpa = Math.Round(grades / creditToBecount, 2, MidpointRounding.AwayFromZero);
            if (double.IsNaN(sgpa))
            {
                sgpa = 0;
            }
            var cumulatives = new CumulativeScores
            {
                SemesterGpa = sgpa,
                Cgpa = Math.Round(grades / creditToBecount, 2, MidpointRounding.AwayFromZero),
                Credits = takenCredit,
                CreditEarned = creditEarned,
                CourseCompleted = courseCompleted
            };
            return cumulatives;
        }

        public List<CumulativeScores> CalculateCumulativeCreditsForInitials(int studentIdenId = 0)
        {
                    var cumulativeSemesters = new List<CumulativeScores>();
            var getAllCourseOfThisStudent = _courseForStudentManger.GetAllCourseByStudent(studentIdenId);
            double cgpaCal = 0, totalCredit = 0.0, totalGrade = 0.0, cumulativeCredits = 0;
            foreach (var cgpaSemester in getAllCourseOfThisStudent.Select(a => a.Semester.SemesterId).Distinct())
            {
                double x = 0, y = 0, z = 0;
                double creditEarned = 0, takenCredit = 0;
                double courseCompleted = 0;
                foreach (
                    var courseForStudentsAcademic in getAllCourseOfThisStudent.Where(s => s.SemesterId == cgpaSemester))
                {

                    double creditVal = courseForStudentsAcademic.CourseForDepartment.Credit;
                    var courseStatIdSpec = courseForStudentsAcademic.CourseStatusId;
                    if (courseStatIdSpec == 4)
                    {
                        takenCredit += 0;
                    }
                    else
                    {
                        takenCredit += creditVal;
                    }
                    if (courseStatIdSpec != 1 && courseStatIdSpec != 4 && courseStatIdSpec != 7)
                    //if (courseForStudentsAcademic.CourseStatusId != 1 )
                    {
                        x = x + creditVal;
                        y = y + (courseForStudentsAcademic.Grade*creditVal);
                        string letterMark = courseForStudentsAcademic.LetterGrade;
                        if (letterMark != "F" && !string.IsNullOrEmpty(letterMark))
                        {
                            creditEarned += creditVal;
                            courseCompleted += 1;
                        }
                    }
                    else
                    {
                        x = x + 0;
                        y = y + 0;
                    }
                }
                cumulativeCredits += creditEarned;
                totalCredit += x;
                totalGrade += y;
                z = Math.Round(y / x, 2, MidpointRounding.AwayFromZero);
                if (double.IsNaN(z))
                {
                    z = 0;
                }
                cgpaCal = Math.Round(totalGrade / totalCredit, 2, MidpointRounding.AwayFromZero);
                if (double.IsNaN(cgpaCal))
                {
                    cgpaCal = 0;
                }
                var cumulatives = new CumulativeScores
                {
                    Credits = takenCredit,
                    CreditEarned = creditEarned,
                    SemesterGpa = z,
                    Cgpa = cgpaCal,
                    CourseCompleted = courseCompleted,
                    SemesterId = cgpaSemester,
                    TotalCredit = x,
                    TotalGrade = y,
                    StudentId = studentIdenId,
                    CumulativeCredits = cumulativeCredits
                };
                cumulativeSemesters.Add(cumulatives);
            }
            return cumulativeSemesters;
        }

        public CumulativeScores GetCgpaSingleSemesterInitial(int studentIdenId = 0, int semesterId = 0)
        {
            var getAllCgpas = CalculateCumulativeCreditsForInitials(studentIdenId).FirstOrDefault(s => s.SemesterId == semesterId);
            return getAllCgpas;
        }

        public IQueryable<GradingSystem> GetAllGradingSystems()
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.Configuration.LazyLoadingEnabled = false;
            return db.GradingSystems.AsQueryable();
        }

        public GradingSystem GetSpecificGradingSystem(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.Configuration.LazyLoadingEnabled = false;
            return db.GradingSystems.Find(id);
        }

        //private bool disposed = false;
        //protected virtual void Dispose(bool disposing)
        //{
        //    if (disposed)
        //        return;

        //    if (disposing)
        //    {
        //        //Free any other managed objects here. 
        //        db.Dispose();
        //    }

        //    // Free any unmanaged objects here. 
        //    disposed = true;
        //}
        //public void Dispose()
        //{
        //    Dispose(true);
        //    GC.SuppressFinalize(this);
        //}
       }
    }
