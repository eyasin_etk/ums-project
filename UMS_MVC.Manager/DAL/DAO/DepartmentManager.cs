﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.Manager.DAL.DAO
{
    public class DepartmentManager
    {
        private UmsDbContext db;
        private readonly StudentManager _studentManager=new StudentManager();
        private readonly SemesterManager _semesterManager=new SemesterManager();
       
        public DepartmentManager()
        {
            db = new UmsDbContext();
        }

        public IQueryable<Department> GetAllDepartment()
        {
            return db.Departments;
        }

        public IQueryable<Department> GetAllDepartmentFiltered()
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.Configuration.LazyLoadingEnabled = false;
            //return db.Departments.AsQueryable();
            return db.Departments.AsQueryable();
        }
        public Department GetSingleDeptFiltered(int deptId)
        {
            return GetAllDepartment().Include(s => s.ASchool).FirstOrDefault(s => s.DepartmentId == deptId);
        }
        public List<DepartmentViewModel> GetAllDepartmentForDropdownSimple()
        {
            var departments = GetAllDepartmentFiltered().Include(s => s.ASchool).Select(s => new DepartmentViewModel
            {
                DepartmentId = s.DepartmentId,
                DepartmentName =  s.DepartmentName,
                DepartmentTitle = s.DepartmentTitle,
                DepartmentCode = s.DepartmentCode,
                SchoolName = s.ASchool.SchoolName,
              
            }).ToList();
            return departments;
        }
        public List<Department> GetAllDepartmentForDropdown()
        {
            //db.Configuration.ProxyCreationEnabled = false;
            //db.Configuration.LazyLoadingEnabled = false;
            var departments = Enumerable.AsEnumerable<Department>(GetAllDepartmentFiltered().Include(s => s.ASchool)).Select(s => new Department
            {
                DepartmentId = s.DepartmentId,
                DepartmentName ="[Dept Code:"+ s.DepartmentCode.ToString("D3")+"] "+s.DepartmentName,
                DepartmentTitle = s.DepartmentTitle,
                DepartmentCode = s.DepartmentCode,
                ASchool = new School
                {
                    SchoolId = s.ASchool.SchoolId,
                    SchoolName = s.ASchool.SchoolName
                }
                
            }).ToList();
            return departments;
        }

        public List<Department> GetAllDepartmentWithEmptyPicCalculation()
        {
            var departments = Enumerable.AsEnumerable<Department>(GetAllDepartmentFiltered().Include(s => s.ASchool)).Select(s => new Department
            {
                DepartmentId = s.DepartmentId,
                DepartmentName = "[EPT:" + (from student in _studentManager.GetAllStudentIdentificationsFiltered().Where(j => j.DepartmentId == s.DepartmentId)
                                            where string.IsNullOrEmpty(student.StudentPicture)
                                            select student).Count().ToString("D3") + "] " + s.DepartmentName,
                DepartmentTitle = s.DepartmentTitle,
                DepartmentCode = s.DepartmentCode,
                ASchool = new School
                {
                    SchoolId = s.ASchool.SchoolId,
                    SchoolName = s.ASchool.SchoolName
                }

            }).ToList();
           return departments;
        }

        public Department GetSingleDepartment(int departmentId=0)
        {
            return db.Departments.Find(departmentId);
        }

        

        public  List<StudentCountByDepartment> GetStudentCountByDepartments(int semesterFinder)
        {
            var studentCounts = new List<StudentCountByDepartment>();
            foreach (var a in GetAllDepartmentFiltered())
            {
                var getStudentByDepartment = _studentManager.GetAllSemesterDepartmentStatusForStudentCount(a.DepartmentId);
                var studentCounter = new StudentCountByDepartment
                {
                    DepartmentName = a.DepartmentName,
                    CurrentSemesterCount = getStudentByDepartment.Count(s=>s.SemesterId==semesterFinder),
                    TotalCount = getStudentByDepartment.Count()
                };
                studentCounts.Add(studentCounter);
            }
            return studentCounts;
        }

        public IQueryable<Department> GetAllDepartmentFinal()
        {
            return GetAllDepartment().Where(s => s.DepartmentId != 20 && s.DepartmentId != 21);
        }

        //private bool disposed = false;
        //protected virtual void Dispose(bool disposing)
        //{
        //    if (disposed)
        //        return;

        //    if (disposing)
        //    {
        //        //Free any other managed objects here. 
        //        db.Dispose();
        //    }

        //    // Free any unmanaged objects here. 
        //    disposed = true;
        //}
        //public void Dispose()
        //{
        //    Dispose(true);
        //    GC.SuppressFinalize(this);
        //}
    }
}