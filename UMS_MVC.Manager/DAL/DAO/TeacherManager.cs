﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.DataModel.Models.UniversalTypes;

namespace UMS_MVC.Manager.DAL.DAO
{
    public class TeacherManager
    {
        private UmsDbContext db;

        public TeacherManager()
        {
            db = new UmsDbContext();
            
        }
        public IQueryable<Teacher> GetAllTeacher()
        {
            return db.Teachers.Include(s=>s.Account).OrderBy(s=>s.Account.Name);
        }

       

        public IQueryable<Teacher> GetAllTeacherFiltered()
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.Configuration.LazyLoadingEnabled = false;
            return db.Teachers.Include(s=>s.Account);
        }

        public Teacher FindTeacherFirstAndDef(int id)
        {
            return GetAllTeacherFiltered().Include(s => s.Department).Include(s => s.Account).FirstOrDefault(s => s.TeacherId == id);
        }

        public IQueryable<Teacher> GetAllTeachersByDepartment(int deptId)
        {
            return GetAllTeacher().Where(s => s.DepartmentId == deptId);
        }
        public Teacher FindTeacherByAccId(int id)
        {
            return GetAllTeacher().FirstOrDefault(s => s.AccountId == id);
        }
        public IEnumerable<Teacher> GetAllTeachersByDepartmentFiltered(int deptId)
        {
            return GetAllTeacherFiltered().Include(s=>s.Account).Include(s=>s.Department).Where(s => s.DepartmentId == deptId && s.AccountId!=null).AsEnumerable().Select(s=>new Teacher
            {
                TeacherId = s.TeacherId,
                DepartmentId = s.DepartmentId,
                LoginId = s.LoginId,
                Account = new Account
                {
                    AccountId = (int) s.AccountId,
                    Name = s.Account.Name
                },
                Department = new Department
                {
                    DepartmentId = s.DepartmentId,
                    DepartmentName = s.Department.DepartmentName
                    
                }
            });
        }
        public IQueryable<TeacherDesignations> GetAllTeacherDesignationses()
        {
            return db.TeacherDesignations;
        }

        public Teacher FindeTeacherFiltered(int teacherId)
        {
            db.Configuration.LazyLoadingEnabled = false;
            db.Configuration.ProxyCreationEnabled = false;
            return db.Teachers.Find(teacherId);
        }

        public Teacher GetSingleTeacher(int? teacherId)
        {
            return db.Teachers.Find(teacherId);

        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                //Free any other managed objects here. 
                db.Dispose();
            }

            // Free any unmanaged objects here. 
            disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}