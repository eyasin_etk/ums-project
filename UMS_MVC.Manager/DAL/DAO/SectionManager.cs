﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Microsoft.Ajax.Utilities;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.Manager.DAL.DAO
{
    public class SectionManager
    {
        private UmsDbContext db;
        private CourseForStudentManger _courseForStudentManger;

        public SectionManager()
        {
            db = new UmsDbContext();
            _courseForStudentManger=new CourseForStudentManger();
        }

        public Section GetSingleSection(int id)
        {
            return db.Sections.Find(id);
        }
        public double CountAllStudentInSection(int secId)
        {
            return _courseForStudentManger.GetAllCourseForStudentFiltered().Count(s => s.SectionId == secId);
        }
        public IQueryable<Section> GetAllSectionsFiltered()
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.Configuration.LazyLoadingEnabled = false;
            return db.Sections;
        }

        public IQueryable<Section> GetAllSections()
        {
            return db.Sections.AsQueryable();
        }
        public Section FindSectionFirstDefForGrading(int id)
        {
            return GetAllSectionsFiltered().Include(s => s.CourseForDepartment).Include(s => s.Semester).FirstOrDefault(s => s.SectionId == id);
        }

        public IQueryable<Section> GetAllHighLightedSection()
        {
            return GetAllSections().Where(s => s.MidTermEx || s.FinalTermEx);
        }

        public IEnumerable<Section> GetAllSectionsByCourseCodewise()
        {
            var sections = new List<Section>();
            foreach (var allSection in GetAllSections())
            {
                var section = new Section
                {
                    SectionId = allSection.SectionId,
                    SectionName = allSection.CourseForDepartment.CourseCode + " [" + allSection.SectionName + "]",
                    SemesterId = allSection.SemesterId,
                    Status = allSection.Status,
                    TeacherId = allSection.TeacherId,
                    CourseForDepartmentId = allSection.CourseForDepartmentId
                };
               
                sections.Add(section);
            }
            return sections;
        }
        public List<Semester> GetAllSemesterbySectionFaculty(int teacherId)
        {
            var takesSections = GetAllSections().Where(s => s.TeacherId == teacherId).AsEnumerable().ToList();
            var sortSemesters = takesSections.DistinctBy(s => s.SemesterId);
            var getSectionsCount = sortSemesters.Select(s => new Semester()
            {
                SemesterId = s.SemesterId,
                SemesterNYear = "[" + takesSections.Count(i => i.SemesterId == s.SemesterId).ToString("D3") + "] " + s.Semester.SemesterNYear
            }).ToList();

            return getSectionsCount;
        }
        public List<Section> GetAllSectionForTeachers(string teacherId)
        {
            var sections = new List<Section>();
            foreach (var allSection in (GetAllSections().Where(s=>s.Teacher.LoginId==teacherId)))
            {
                var section = new Section
                {
                    SectionId = allSection.SectionId,
                    SectionName = allSection.CourseForDepartment.CourseCode + " [" + allSection.SectionName + "]",
                    SemesterId = allSection.SemesterId,
                    Status = allSection.Status,
                    TeacherId = allSection.TeacherId,
                    Semester = allSection.Semester,
                    CourseForDepartmentId = allSection.CourseForDepartmentId
                };

                sections.Add(section);
            }
            return sections;
        }

        public IQueryable<Section> GetAllSectionsByDepartment(int department)
        {
            return GetAllSections().Where(s => s.CourseForDepartment.DepartmentId == department);
        }

        public IQueryable<Section> GetAllSectionByCourseFiltered(int id)
        {
            return GetAllSectionsFiltered().Where(s => s.CourseForDepartmentId == id);
        }
        

        public IQueryable<Section> GetAllSectionByCourseCodeAndSemester(int courseCode, int semesterId)
        {
            return GetAllSections().Where(s => s.CourseForDepartmentId == courseCode && s.SemesterId == semesterId);
        }
        public IQueryable<Section> GetAllSectionByCourseCodeAndSemesterFiltered(int courseCode, int semesterId)
        {
            return GetAllSectionsFiltered().Where(s => s.CourseForDepartmentId == courseCode && s.SemesterId == semesterId);
        }
        public IQueryable<Section> GetAllSectionByCourseCode(int courseCode)
        {
            return GetAllSections().Where(s => s.CourseForDepartmentId == courseCode);
        }
        public IQueryable<Section> GetAllSectionBySemester(int semesterId)
        {
            return GetAllSectionsFiltered().Where(s=>s.SemesterId==semesterId).AsQueryable();
        }

        public void UpdateSection(Section section)
        {
            db.Entry(section).State=EntityState.Modified;
        }

        public void Commit()
        {
            db.SaveChanges();
        }

        //private bool disposed = false;
        //protected virtual void Dispose(bool disposing)
        //{
        //    if (disposed)
        //        return;

        //    if (disposing)
        //    {
        //        //Free any other managed objects here. 
        //        db.Dispose();
        //    }

        //    // Free any unmanaged objects here. 
        //    disposed = true;
        //}
        //public void Dispose()
        //{
        //    Dispose(true);
        //    GC.SuppressFinalize(this);
        //}

    }
}