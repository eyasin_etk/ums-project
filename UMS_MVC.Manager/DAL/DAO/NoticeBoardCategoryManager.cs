﻿using System.Linq;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Notice_Matter;
using UMS_MVC.Manager.DAL.DAO.Intefaces;
using UMS_MVC.Manager.DAL.DAO.Repository;

namespace UMS_MVC.Manager.DAL.DAO
{
    public class NoticeBoardCategoryManager
    {
        
        private readonly INoticeBoardCategoryRepository _categoryRepository;

        public NoticeBoardCategoryManager()
        {
           _categoryRepository=new NoticeBoardCategoryRepository();

        }
        public IQueryable<NoticeBoardCategory> GetAllCategories()
        {
            return _categoryRepository.GetAll();
        }

        public NoticeBoardCategory GetSinglecCategory(int id)
        {
            return _categoryRepository.FindSingle(id);
        }

        public int InsertUpdate(NoticeBoardCategory category)
        {
            NoticeBoardCategory noticeBoardCategory;
            if (category.NoticeBoardCategoryId != 0)
            {
                noticeBoardCategory = GetSinglecCategory(category.NoticeBoardCategoryId);
                noticeBoardCategory.CategoryName = category.CategoryName;
                noticeBoardCategory.NoticeBoardDetails = category.NoticeBoardDetails;
                noticeBoardCategory.Remarks = category.Remarks;
                _categoryRepository.Update(category);
            }
            else
            {
                noticeBoardCategory = category;
                _categoryRepository.Add(noticeBoardCategory);
            }
            _categoryRepository.Save();
            return noticeBoardCategory.NoticeBoardCategoryId;

        }

        public void Update(NoticeBoardCategory category)
        {
            _categoryRepository.Update(category);
        }

        public bool Delete(int id)
        {
            var category = _categoryRepository.FindSingle(id);
            if (category != null)
            {
                _categoryRepository.Delete(category);
                _categoryRepository.Save();
                return true;
            }
            return false;

        }

        public void Save()
        {
            _categoryRepository.Save();
        }
    }
}