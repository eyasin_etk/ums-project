﻿using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.TeacherEvaluation;
using UMS_MVC.Manager.DAL.DAO.GenericService;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IEvaluation;

namespace UMS_MVC.Manager.DAL.DAO.RServey
{
    class SurveyAnswerRepository: GenericRepository<UmsDbContext, SurveyAnswer>, ISurveyAnswerRepository
    {
       
    }

    class SurveyAssesmentRepository : GenericRepository<UmsDbContext, SurveryAssesment>, ISurveyAssesmentRepository
    {

    }
    class SurveyQuestionRepository : GenericRepository<UmsDbContext, SurveyQuestion>, ISurveyQuestionRepository
    {

    }
}
