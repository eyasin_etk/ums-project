﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.Manager.DAL.DAO
{
    public class CourseForStudentManger
    {
        private UmsDbContext db;
        private readonly StudentManager _studentManager;
        private readonly CourseForDepartmentManager _courseForDepartmentManager;
        private readonly SemesterManager _semesterManager;

        public CourseForStudentManger()
        {
            db=new UmsDbContext();
            _studentManager = new StudentManager();
            _courseForDepartmentManager = new CourseForDepartmentManager();
            _semesterManager = new SemesterManager();
        }

        public IQueryable<CourseForStudentsAcademic> GetAllCourseForStudentsAcademics()
        {
            return db.CourseForStudentsAcademics.OrderBy(s => s.StudentIdentification.StudentId);
        }

        public IQueryable<CourseForStudentsAcademic> GetAllCourseForStudentFiltered()
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.Configuration.LazyLoadingEnabled = false;
            return db.CourseForStudentsAcademics.AsQueryable();
        }

        public IQueryable<CourseForStudentsAcademic> GetAllCourseForStudentsAcademicsFiltered()
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.Configuration.LazyLoadingEnabled = false;
            return db.CourseForStudentsAcademics;
        }

        public CourseForStudentsAcademic GetSingleCourseForStudentAcademic(int courseId = 0)
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.Configuration.LazyLoadingEnabled = false;
            return db.CourseForStudentsAcademics.Find(courseId);
        }

        public CourseForStudentsAcademic GetAdvisingFull(int advisingId)
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.Configuration.LazyLoadingEnabled = false;
            return db.CourseForStudentsAcademics.Include(s=>s.CourseForDepartment.Department).Include(s=>s.CourseStatus).FirstOrDefault(s=>s.CourseForStudentsAcademicId==advisingId);
        }

        public IQueryable<CourseForStudentsAcademic> GetAllCourseForStudentsAcademicsBysection(int sectionId)
        {
            return db.CourseForStudentsAcademics.Include(s=>s.StudentIdentification.StudentInfo).Where(s => s.SectionId == sectionId);
        }
        
        public IQueryable<CourseForStudentsAcademic> GetAllCourseForStudentsAcademicsByCourseId(int courseId)
        {
            return GetAllCourseForStudentsAcademics().Where(s => s.CourseForDepartmentId == courseId);
        }

        public IQueryable<CourseForStudentsAcademic> GetAllCourseForStudentsAcademicsByDepartment(int deptId)
        {
            return GetAllCourseForStudentsAcademics().Where(s => s.CourseForDepartment.DepartmentId == deptId);
        }

        public IQueryable<CourseForStudentsAcademic> GetAllCourseForStudentsAcademicsByDeptAndSemester(int deptId,int semesterId)
        {
            return
                GetAllCourseForStudentsAcademics()
                    .Where(s => s.CourseForDepartment.DepartmentId == deptId && s.SemesterId == semesterId).Include(s=>s.StudentIdentification);
        }

        public IQueryable<CourseForStudentsAcademic> GetAllCourseForStudentsAcademicsBySemester(int semesterId=0)
        {
            return GetAllCourseForStudentsAcademics().Where(s => s.SemesterId == semesterId);
        }

        public IQueryable<CourseForStudentsAcademic> GetAllCoursesFromBatchToBatch(int fromBatch, int toBatch)
        {
            //var getBatches =
            //    _semesterManager.GetAllSemester().Where(s => s.SemesterId >= fromBatch && s.SemesterId <= toBatch);
            //return GetAllCourseForStudentsAcademics().Where(p=>getBatches.Any(k=>k.SemesterId==p.StudentIdentification.SemesterId));
            var getBatches = new HashSet<int>(_semesterManager.GetAllSemester().Where(s=>s.SemesterId>=fromBatch && s.SemesterId<=toBatch).Select(s => s.SemesterId));
            var result =
                GetAllCourseForStudentsAcademics().Where(s => getBatches.Contains((int) s.StudentIdentification.SemesterId));
            return result;
        }

        public IQueryable<CourseForStudentsAcademic> GetAllCoursesByCourseCodeandSemester(int courseCode,int semesterId)
        {
            return
                GetAllCourseForStudentsAcademics()
                    .Where(s => s.CourseForDepartmentId == courseCode && s.SemesterId == semesterId);
        }

        //public IQueryable<CourseForStudentsAcademic> GetAllCourseForStudentsAcademicsBySemester()
        //{
        //    return GetAllCourseForStudentsAcademics().Where(s => s.Semester.CourseAdvising);
        //}

        //public IQueryable<CourseForStudentsAcademic> GetAllForStudentsAcademicsForCurrentPayments()
        //{

        //}
        public IQueryable<CourseForStudentsAcademic> GetallAdvisedCoursesByPaymentRegis(int paymentRegiId=0)
        {
            return GetAllCourseForStudentsAcademics().Where(s => s.PaymentRegistrationId == paymentRegiId);
        }

        public IQueryable<CourseForStudentsAcademic> GetAllCourseByStudent(int studentId)
        {
            return GetAllCourseForStudentsAcademics().Where(s => s.StudentIdentificationId == studentId);
        }
        
        public bool UpdateCourseForStudentAcademic(CourseForStudentsAcademic findCourse1)
        {
            try
            {
                db.Entry(findCourse1).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                throw new DbUpdateException();
            }
            
        }
        public void UpdateCourseForStudentAcademicSecond(CourseForStudentsAcademic findCourse1)
        {
            db.Entry(findCourse1).State = EntityState.Modified;
        }


        public void Commit()
        {
            db.SaveChanges();
        }
        public IQueryable<CourseForStudentsAcademic> GetAllCompletedCourseOfAStudent(int studentId)
        {
            return GetAllCourseForStudentsAcademics().Where(s => s.StudentIdentificationId == studentId && s.CourseStatusId==2);
        }

        public IEnumerable<CourseForDepartment> GetAllOfferedCoursesList(int studentId)
        {
            var getStudentDepartment = _studentManager.GetSingleStudentByStudentIdentification(studentId);
            var getAllCourseByDepartment =
                _courseForDepartmentManager.GetAllCourseForDepartmentsByDepartment(getStudentDepartment.DepartmentId);
            var getAllTakenCourses = GetAllCompletedCourseOfAStudent(studentId);
            HashSet<int> courseIds = new HashSet<int>(getAllTakenCourses.Select(s => s.CourseForDepartmentId));
            var offerdCourseList = getAllCourseByDepartment.Where(m => !courseIds.Contains(m.CourseForDepartmentId));
            return offerdCourseList;
            //var offerdCourses = from courseForDepartment in getAllCourseByDepartment
            //    where !getAllTakenCourses.Any(s => s.CourseForDepartmentId == courseForDepartment.CourseForDepartmentId)
            //    select courseForDepartment;
            //return offerdCourses.ToList();
        }

        public IEnumerable<CourseForStudentsAcademic> GetAllOtherAndFailedCourses(int studentId)
        {
            return GetAllCourseByStudent(studentId).Where(s => s.LetterGrade == "F" || s.CourseStatusId != 2);
        }

        public IEnumerable<CourseForStudentsAcademic> GetCoursesForImprovement(int studentId)
        {
            return
                GetAllCourseByStudent(studentId)
                    .Where(
                        s =>
                            s.LetterGrade == "B-" || s.LetterGrade == "C+" || s.LetterGrade == "C" ||
                            s.LetterGrade == "D");
        }

        public IQueryable<CourseForStudentsAcademic> GetAllOtherCoursesOfAStudent(int studentId)
        {
            return GetAllCourseByStudent(studentId).Where(s => s.CourseStatusId != 2);
        }

        public IQueryable<CourseForStudentsAcademic> GetAllNewAdvisedCoursesOfAStudent(int studentId,int semesterId)
        {
            
            return GetAllCourseByStudent(studentId).Where(s => s.CourseStatusId == 1);
        }

        public IQueryable<CourseForStudentsAcademic> GetAllFailedCoursesOfAStudent(int studentId)
        {
            return GetAllCourseByStudent(studentId).Where(s => s.LetterGrade == "F");
        }

        public List<CourseForStudentsAcademic> GetCoursesByPaymentSlipId(int paymentId)
        {
            var getCourses=db.CourseForStudentsAcademics.Where(s => s.PaymentRegistrationId == paymentId);
            return getCourses.ToList();
        }
        public double CountAllStudentInSection(int secId)
        {
            return GetAllCourseForStudentFiltered().Count(s => s.SectionId == secId);
        }
        public IQueryable<CourseForStudentsAcademic> GetAllCoursesByIdentificationIdAndSemester(int studenId, int semesterId)
        {
            return
                GetAllCourseForStudentFiltered()
                    .Where(s => s.StudentIdentificationId == studenId && s.SemesterId == semesterId);
        }
        //private bool disposed = false;
        //protected virtual void Dispose(bool disposing)
        //{
        //    if (disposed)
        //        return;

        //    if (disposing)
        //    {
        //        //Free any other managed objects here. 
        //        db.Dispose();
        //    }

        //    // Free any unmanaged objects here. 
        //    disposed = true;
        //}
        //public void Dispose()
        //{
        //    Dispose(true);
        //    GC.SuppressFinalize(this);
        //}
    }
}