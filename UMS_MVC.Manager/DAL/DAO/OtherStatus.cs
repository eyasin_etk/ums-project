﻿using System;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.Manager.DAL.DAO
{
    public class OtherStatus
    {
        private UmsDbContext db;
        private readonly SemesterManager _semesterManager=new SemesterManager();
        public OtherStatus()
        {
            db=new UmsDbContext();
        }

        public static string GetToday()
        {
            return DateTime.Now.ToShortDateString();
        }

        public SemesterStatus GetSemesterStatus()
        {
            var getActiveSemester = _semesterManager.GetActiveSemester();
            var getAdviseActivatedSemester = _semesterManager.GetAdvisingActivatedSemester();
            var overallSemesterStatus = new SemesterStatus {ActiveSemester = getActiveSemester.SemesterNYear};
            if (getActiveSemester.FinalTerm)
            {
                overallSemesterStatus.GradeUploadTerm = "Final Term Activated";
            }
            else if (getActiveSemester.MidTerm)
            {
                overallSemesterStatus.GradeUploadTerm = "Mid Term Activated";
            }
            else
            {
                overallSemesterStatus.GradeUploadTerm = "Grade upload Disabled";
            }
            if (getAdviseActivatedSemester != null)
            {
                overallSemesterStatus.AdvisingSemester=getAdviseActivatedSemester.SemesterNYear + " - Activated";
            }
            else
            {
                overallSemesterStatus.AdvisingSemester = "Disabled";
            }
            return overallSemesterStatus;
        }
    }
}