﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.DataModel.Models.UniversalTypes;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.Manager.DAL.DAO
{
    public class StudentManager
    {


        private UmsDbContext db;
        public StudentManager()
        {
            db = new UmsDbContext();
        }

        public IQueryable<StudentIdentification> GetAllStudentIdentifications()
        {
            return db.StudentIdentifications;
        }

        public IQueryable<StudentIdentification> GetAllStudentIdentificationsFiltered()
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.Configuration.LazyLoadingEnabled = false;
            return db.StudentIdentifications.AsQueryable();
        }

        public IQueryable<StudentInfo> GetAllStudentInfoFiltered()
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.Configuration.LazyLoadingEnabled = false;
            return db.StudentInfos.AsQueryable();
        }

        public StudentIdentification GetSingleStudentFilteredFnD(int id)
        {
            return GetAllStudentIdentificationsFiltered().Include(s => s.Department).Include(s => s.StudentInfo).FirstOrDefault(s => s.StudentIdentificationId == id);
        }

        public IQueryable<StudentIdentification> GetStudentIdentificationIndexType()
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.Configuration.LazyLoadingEnabled = false;
            //var studentSortedList= from VAR in  db.StudentIdentifications.Include(s=>s.Department).Include(s=>s.StudentInfo).Include(s=>s.Semester).AsQueryable()

            var studentIdentifications = db.StudentIdentifications.Include(s => s.Department).Include(s => s.StudentInfo).Include(s => s.Semester).Select(s => new StudentIdentification()
            {
                StudentIdentificationId = s.StudentIdentificationId,
                StudentId = s.StudentId,
                DepartmentId = s.DepartmentId,
                SemesterId = s.SemesterId,
                Remark = s.Remark,
                CreditTransfer = s.CreditTransfer,
                DiplomaStudent = s.DiplomaStudent,
                Validation = s.Validation,
                Password = s.Password,
                StudentPicture = s.StudentPicture,
                AddedDate = s.AddedDate,
                Department = new Department
                {
                    DepartmentId = s.DepartmentId,
                    DepartmentName = s.Department.DepartmentName
                },

                StudentInfo = new StudentInfo
                {
                    EmailAddress = s.StudentInfo.EmailAddress,
                    StudentInfoId = s.StudentInfo.StudentInfoId,
                    PhoneNo = s.StudentInfo.PhoneNo,
                    MobileNo = s.StudentInfo.MobileNo,
                    GenderId = s.StudentInfo.GenderId,
                    StudentName = s.StudentInfo.StudentName,
                },

                Semester = new Semester
                {
                    SemesterNYear = s.Semester.SemesterNYear,
                    BatchNo = s.Semester.BatchNo
                }

            });
            return studentIdentifications;
        }

        public IQueryable<StudentIdentification> GetAllStudentIdentityFromBatchToBatch(int fromBatch, int toBatch)
        {
            return GetAllStudentIdentifications().Where(s => s.SemesterId >= fromBatch && s.SemesterId <= toBatch);
        }

        public IQueryable<StudentIdentification> GetAllStudentIdentityLimited()
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.Configuration.LazyLoadingEnabled = false;
            var getStudents2 =
                db.StudentIdentifications.Include(s => s.StudentInfo)
                    .Include(s => s.Department)
                    .Include(s => s.Semester);
            //var getStudents = from studentIdentification in db.StudentIdentifications.Include(s => s.StudentInfo).Include(s=>s.Department).Include(s=>s.Semester)
            //    select new StudentIdentification
            //    {
            //        StudentIdentificationId = studentIdentification.StudentIdentificationId,
            //        Department = studentIdentification.Department,
            //        DepartmentId = studentIdentification.DepartmentId,
            //        //Semester = studentIdentification.Semester,
            //        SemesterId = studentIdentification.SemesterId,
            //        SemesterInfo = studentIdentification.SemesterInfo,
            //        SemesterInfoId = studentIdentification.SemesterInfoId,
            //        StudentId = studentIdentification.StudentId,
            //        AddedDate =studentIdentification.AddedDate

            //    };
            return getStudents2;

        }

        //public IQueryable<PicOfStudent> GetAllPicOfStudents()
        //{
        //    return db.PicOfStudents;
        //}
        public StudentIdentification FindSingleStudentFiltered(int id)
        {
            using (var db2=new UmsDbContext())
            {
                db2.Configuration.ProxyCreationEnabled = false;
                db2.Configuration.LazyLoadingEnabled = false;
                return db2.StudentIdentifications.Find(id);
            }
        }

        public StudentInfo FindSingleStudentInfoFiltered(int id)
        {
            using (var db2 = new UmsDbContext())
            {
                db2.Configuration.ProxyCreationEnabled = false;
                db2.Configuration.LazyLoadingEnabled = false;
                return db2.StudentInfos.Find(id);
            }
        }

        public StudentIdentification GetSingleStudent(int id)
        {
            return db.StudentIdentifications.Find(id);
        }

        public StudentIdentification GetSingleStudentByStudentId(string studentId)
        {
            return
                GetAllStudentIdentifications()
                    .FirstOrDefault(s => s.StudentId == studentId);
        }

        public StudentIdentification GetSingleStudentByStudentIdentification(int studentIdentity)
        {
            return
                GetAllStudentIdentifications()
                    .FirstOrDefault(s => s.StudentIdentificationId == studentIdentity);
        }

        public bool StudentExistanceByStudentId(string studentId)
        {
            return GetAllStudentIdentifications().Count(s => s.StudentId == studentId) == 1;
        }

        public bool StudentExistanceByStudentIdentity(int studentId)
        {
            return GetAllStudentIdentifications().Count(s => s.StudentIdentificationId == studentId) == 1;
        }

        public IEnumerable<Department> GetallDepartments()
        {
            return db.Departments;
        }

        public IQueryable<StudentInfo> GetAllStudentInfos()
        {
            return db.StudentInfos;
        }

        public StudentInfo GetSingleStudentInfo(int id)
        {
            return db.StudentInfos.Find(id);
        }

        public IQueryable<StudentAcademicInfo> GetAllAcademicInfos()
        {
            return db.StudentAcademicInfos;
        }

        public IQueryable<DocumentAdding> GetAllStudentDocument()
        {
            return db.DocumentsaddAddings;
        }

        public StudentInfo GetStudentInfoByStudentId(string studentId)
        {
            return GetAllStudentInfos().FirstOrDefault(s => s.StudentId == studentId);
        }
        
        public StudentInfo GetStudentInfoByStudentIdFilterd(string studentId)
        {
            return GetAllStudentInfoFiltered().FirstOrDefault(s => s.StudentId == studentId);
        }

        public IQueryable<StudentIdentification> GetAllStudentByDepartment(int deptId = 0)
        {
            var getStudentList = GetAllStudentIdentifications().Where(s => s.DepartmentId == deptId);
            return getStudentList;
        }

        public IQueryable<StudentIdentification> GetAllStudentBySemesterFiltered(int semesterId)
        {
            return GetAllStudentIdentificationsFiltered().Where(s => s.SemesterId == semesterId);
        }

        public IQueryable<StudentIdentification> GetAllSemesterDepartmentStatusForStudentCount(int deptId = 0)
        {
            return GetAllStudentIdentificationsFiltered()
                .Where(s => s.DepartmentId == deptId).AsQueryable();

        }
        public IQueryable<StudentIdentification> GetAllStudentForAdmitCard(int department, int semester = 0)
        {
            return
                GetAllStudentIdentifications().Where(s => s.DepartmentId == department && s.SemesterId == semester);
        }

        public IEnumerable<BloodGroups> GetAllBloodGroups()
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.Configuration.LazyLoadingEnabled = false;
            return db.BloodGroupses;
        }

        public IEnumerable<MaritalStatus> GetAllmMaritalStatuses()
        {
            return db.MaritalStatuses;
        }

        public IEnumerable<Gender> GetAllGender()
        {
            return db.Genders;
        }

        public StudentBlockViewModel StudentBlockCheck(int id)
        {
            using (var db2=new UmsDbContext())
            {
                db2.Configuration.ProxyCreationEnabled = false;
                db.Configuration.LazyLoadingEnabled = false;
                StudentIdentification student = db.StudentIdentifications.Find(id);
               
                    return new StudentBlockViewModel
                    {
                        IsBlocked = true,
                        BlockReason = student.BlockReason,
                        BlockExpireDate = student.BlockExpireDate
                    };
                
            }
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                //Free any other managed objects here. 
                db.Dispose();
            }

            // Free any unmanaged objects here. 
            disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}