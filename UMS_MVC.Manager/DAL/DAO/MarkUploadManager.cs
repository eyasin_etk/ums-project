﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO.Intefaces;
using UMS_MVC.Manager.DAL.DAO.Repository;

namespace UMS_MVC.Manager.DAL.DAO
{
    public class MarkUploadManager
    {
        private readonly ICourseForStudentRepository _courseForStudentRepository;
        public MarkUploadManager()
        {
           
            _courseForStudentRepository=new CourseForStudentRepository();
            
        }

        public IQueryable<CourseForStudentsAcademic> GetAllCourseForStudent()
        {
            return _courseForStudentRepository.GetAll()
                .Include(s=>s.CourseForDepartment)
                .Include(s=>s.Section)
                .Include(s=>s.StudentIdentification.StudentInfo);
        }


        public CourseForStudentsAcademic GetSingleCourseForStudentsAcademic(int id)
        {
            //return _courseForStudentRepository.GetSingleCourseForStudentsAcademic(id);
            var courseForStudentsAcademic = GetAllCourseForStudent().FirstOrDefault(s => s.CourseForStudentsAcademicId == id);
            if (courseForStudentsAcademic!=null)
            {
                return courseForStudentsAcademic;
            }
            return new CourseForStudentsAcademic();
        }

        public string InsertCourseForStudent(CourseForStudentsAcademic courseForStudentsAcademic)
        {
            string msg;
            try
            {
                _courseForStudentRepository.Add(courseForStudentsAcademic);
                _courseForStudentRepository.Save();
                msg = "Insert successfull";
            }
            catch (DbUpdateException)
            {

                msg = "Insert Failed !!";
            }
            return msg;
        }

        public string UpdateCourseForStudent(CourseForStudentsAcademic courseForStudentsAcademic)
        {
            string msg;
            try
            {
                _courseForStudentRepository.Update(courseForStudentsAcademic);
                _courseForStudentRepository.Save();
                msg = "Update successfull";
            }
            catch (DbUpdateException)
            {

                msg = "Update Failed !!";
            }
            return msg;
        }

        public string DeleteCourseForStudent(int id)
        {
            string msg;
            try
            {
                var courseForStudentsAcademic = _courseForStudentRepository.FindSingle(id);
                _courseForStudentRepository.Delete(courseForStudentsAcademic);
                _courseForStudentRepository.Save();
                msg = "Delete successfull";
            }
            catch (DbUpdateException)
            {

                msg = "Delete Failed !!";
            }
            return msg;
        }
        //protected void Dispose(bool disposing)
        //{
        //    _dbContext.Dispose();
        //    Dispose(disposing);
        //}
    }
}