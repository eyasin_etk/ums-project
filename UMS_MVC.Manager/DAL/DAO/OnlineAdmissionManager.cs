﻿using System;
using System.Linq;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.DataModel.Models.PreAdmission;
using UMS_MVC.Manager.DAL.BLL;
using UMS_MVC.Manager.DAL.DAO.Intefaces;
using UMS_MVC.Manager.DAL.DAO.Repository;
using UMS_MVC.ViewModels.ViewModels.OnlineAdmission;


namespace UMS_MVC.Manager.DAL.DAO
{
    public class OnlineAdmissionManager
    {
        private readonly StudentManager _studentManager;
        private readonly IPreAdmissionStudentInfoRepository _infoRepository;
        private readonly PostOnlineAdmissionIdenRepository _postOnlineAdmissionRepository;
        private readonly PreAdmissionStudentInfoRepository _preAdmissionStudent;
        private readonly PreAdmissionAcademicRepository _academicRepository;
        private readonly ISemesterRepository _semesterRepository;
        // private readonly 

        public OnlineAdmissionManager()
        {
            _studentManager = new StudentManager();
            _academicRepository = new PreAdmissionAcademicRepository();
            _infoRepository = new PreAdmissionStudentInfoRepository();
            _preAdmissionStudent = new PreAdmissionStudentInfoRepository();
            _postOnlineAdmissionRepository = new PostOnlineAdmissionIdenRepository();
            _semesterRepository=new SemesterRepository();
        }

        public StudentInfo GetSingleStudentInfo(string id)
        {
            var stuInfoObj = _studentManager.GetStudentInfoByStudentIdFilterd(id);
            if (stuInfoObj != null)
            {
                return stuInfoObj;
            }
            return null;
        }
        public IQueryable<StudentAcademicInfo> GetSingleStudentAcaInfo(string id)
        {
            var stuAcaInfoObj = _studentManager.GetAllAcademicInfos().Where(s => s.StudentId == id);
            return stuAcaInfoObj;
        }

        public OnlineAdmStudentIdentityViewModel GetStudentIdViewModel(Guid id)
        {
            PreAdmissionStudentInfo studentInfoObj = _preAdmissionStudent.FinSingleGuid(id);
            var identityViewModel = new OnlineAdmStudentIdentityViewModel
            {
                DepartmentId = studentInfoObj.EnrolledDepartmentId,
                SemesterId = studentInfoObj.EnrolledSemsterId,
                StudentPicture = studentInfoObj.StudentImage,
                StudentGuid = studentInfoObj.PreAdmissionStudentInfoId,
                StudentInfoId = 0

            };
            return identityViewModel;
        }

        public OnlineAdmStudentInfoViewMOdel GetStudentInfoViewMOdel(Guid id)
        {
            PreAdmissionStudentInfo studentInfoObj = _preAdmissionStudent.FinSingleGuid(id);
            var infoViemodel = new OnlineAdmStudentInfoViewMOdel
            {
                StudentName = studentInfoObj.StudentName,
                FathersName = studentInfoObj.FathersName,
                MothersName = studentInfoObj.MothersName,
                PhoneNo = studentInfoObj.PersonalContactNo,
                MobileNo = studentInfoObj.PhoneNoSecond,
                EmailAddress = studentInfoObj.EmailAddress,
                BloodGroupsId = studentInfoObj.BloodGroupsId,
                GenderId = studentInfoObj.GenderId,
                MaritalStatusId = studentInfoObj.MaritalStatusId,
                DateOfBirth = studentInfoObj.DateOfBirth,
                SkillInOtherfield = studentInfoObj.SkillInOtherfield,
                Nationality = studentInfoObj.Nationality,
                PresentAddress = studentInfoObj.PresentAddress,
                PresentDistrict = studentInfoObj.PresentDistrict,
                PresentPostalCode = studentInfoObj.PresentPostalCode,
                ParmanentAddress = studentInfoObj.ParmanentAddress,
                ParmanentDistrict = studentInfoObj.ParmanentDistrict,
                ParmanentPostalCode = studentInfoObj.ParmanentPostalCode,
                LocalGuardianName = studentInfoObj.LocalGuardianName,
                LocalGuardianAddress = studentInfoObj.LocalGuardianAddress,
                LocalGuardianContact = studentInfoObj.LocalGuardianContact,
                LocalGuardianRelationship = studentInfoObj.LocalGuardianRelationship

            };
            return infoViemodel;
        }
        public OnlineAdmAcademicViewModel AdmAcademicViewModel(Guid id)
        {
            PreAdmissionAcademic studentacaObj = _academicRepository.FindSingleGuid(id);
            var info =
                _studentManager.GetAllStudentIdentifications().Where(s => s.StudentGuid == id);
            var acaViemodel = new OnlineAdmAcademicViewModel
            {
                StudentId = info.Select(s => s.StudentId).ToString(),
                NameOfExamination = studentacaObj.NameOfExamination,
                StartingSession = studentacaObj.StartingSession,
                Group = studentacaObj.Group,
                PassingYear = studentacaObj.PassingYear,
                Result = studentacaObj.Result,
                UniversityBoard = studentacaObj.UniversityBoard
            };
            return acaViemodel;
        }

        public Guid InsertUpdate(PreAdmissionStudentInfo admissionStudentInfo)
        {
            PreAdmissionStudentInfo dbStudentInfo;
            if (admissionStudentInfo.PreAdmissionStudentInfoId != Guid.Empty)
            {
                dbStudentInfo = _infoRepository.FinSingleGuid(admissionStudentInfo.PreAdmissionStudentInfoId);

                dbStudentInfo.StudentName = admissionStudentInfo.StudentName;
                dbStudentInfo.PersonalContactNo = admissionStudentInfo.PersonalContactNo;
                dbStudentInfo.BloodGroupsId = admissionStudentInfo.BloodGroupsId;
                dbStudentInfo.GenderId = admissionStudentInfo.GenderId;
                dbStudentInfo.EmailAddress = admissionStudentInfo.EmailAddress;
                dbStudentInfo.MaritalStatusId = admissionStudentInfo.MaritalStatusId;
                dbStudentInfo.Nationality = admissionStudentInfo.Nationality;
                dbStudentInfo.SkillInOtherfield = admissionStudentInfo.SkillInOtherfield;
                dbStudentInfo.FathersName = admissionStudentInfo.FathersName;
                dbStudentInfo.MothersName = admissionStudentInfo.MothersName;
                dbStudentInfo.PresentAddress = admissionStudentInfo.PresentAddress;
                dbStudentInfo.PresentDistrict = admissionStudentInfo.PresentDistrict;
                dbStudentInfo.PresentPostalCode = admissionStudentInfo.PresentPostalCode;
                dbStudentInfo.ParmanentAddress = admissionStudentInfo.ParmanentAddress;
                dbStudentInfo.ParmanentDistrict = admissionStudentInfo.ParmanentDistrict;
                dbStudentInfo.ParmanentPostalCode = admissionStudentInfo.ParmanentPostalCode;
                dbStudentInfo.PhoneNoSecond = admissionStudentInfo.PhoneNoSecond;
                dbStudentInfo.LocalGuardianName = admissionStudentInfo.LocalGuardianName;
                dbStudentInfo.LocalGuardianRelationship = admissionStudentInfo.LocalGuardianRelationship;
                dbStudentInfo.LocalGuardianAddress = admissionStudentInfo.LocalGuardianAddress;
                dbStudentInfo.LocalGuardianContact = admissionStudentInfo.LocalGuardianContact;
                dbStudentInfo.EnrolledSemsterId = admissionStudentInfo.EnrolledSemsterId;
                dbStudentInfo.EnrolledDepartmentId = admissionStudentInfo.EnrolledDepartmentId;
                dbStudentInfo.StudentImage = admissionStudentInfo.StudentImage;
                dbStudentInfo.DiplomaStudent = admissionStudentInfo.DiplomaStudent;
                dbStudentInfo.AdmissionOfficeConfirmed = admissionStudentInfo.AdmissionOfficeConfirmed;
                dbStudentInfo.InfoConfirmed = admissionStudentInfo.InfoConfirmed;

                _infoRepository.Update(dbStudentInfo);
            }
            else
            {
                dbStudentInfo = admissionStudentInfo;
                admissionStudentInfo.BarCodeSerial = "";

                admissionStudentInfo.EntryTime = DateTime.Now;
                _infoRepository.Add(dbStudentInfo);
            }
            _infoRepository.Save();
            return dbStudentInfo.PreAdmissionStudentInfoId;
        }

        public string Delete(Guid id)
        {
            string msg;
            try
            {
                var preAdmissionStudentInfo = _infoRepository.FinSingleGuid(id);
                _infoRepository.Delete(preAdmissionStudentInfo);
                _infoRepository.Save();
                msg = "Success";
            }
            catch (Exception)
            {

                msg = "Failed";
            }
            return msg;
        }

        public PreAdmissionStudentInfo PostSpecificStudent(Guid id)
        {
            var postAdmissionStudent = _preAdmissionStudent.FinSingleGuid(id);
            return postAdmissionStudent;
        }

     
        public void CommitChanges()
        {
            _postOnlineAdmissionRepository.Save();
        }

        public PreAdmissionStudentInfo InsertOnly(PreAdmissionStudentInfo admissionStudentInfo)
        {
            var admSemester = _semesterRepository.GetOnlineAdmSemester();
           
            var dbStudentInfo = admissionStudentInfo;
            admissionStudentInfo.BarCodeSerial = "";
            dbStudentInfo.EnrolledSemsterId = admSemester.SemesterId;
            admissionStudentInfo.EntryTime = DateTime.Now;
            _infoRepository.Add(dbStudentInfo);
            _infoRepository.Save();
            dbStudentInfo.BarCodeSerial = GuidIdConverter.GuidToBase64(dbStudentInfo.PreAdmissionStudentInfoId);
            _infoRepository.Update(dbStudentInfo);
            _infoRepository.Save();
            return dbStudentInfo;
        }
        public bool StoreImageName(Guid studentToken)
        {
            try
            {
                PreAdmissionStudentInfo studentInfo = _infoRepository.FinSingleGuid(studentToken);
                studentInfo.StudentImage = studentToken + ".jpg";
                _infoRepository.Update(studentInfo);
                _infoRepository.Save();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

    }
}
