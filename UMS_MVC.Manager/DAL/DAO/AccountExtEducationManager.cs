﻿using System.Data.Common;
using System.Linq;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models.AccountExt;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IAccount;
using UMS_MVC.Manager.DAL.DAO.Repository.RAccount;

namespace UMS_MVC.Manager.DAL.DAO
{
    public class AccountExtEducationManager
    {
        private readonly IAccountExtEducationRepository _accountExtEducationRepository;

        public AccountExtEducationManager()
        {
            _accountExtEducationRepository = new AccountExtEducationRepository(new UmsDbContext());
        }

        public IQueryable<AccountExtEducation> GetAllAccountExtEducations()
        {
            return _accountExtEducationRepository.GetAllAccountExtEducations();
        }

        public AccountExtEducation GetSinglEducation(int id)
        {
            return _accountExtEducationRepository.GetSinglExtEducation(id);
        }

        public void Insert(AccountExtEducation education)
        {
            _accountExtEducationRepository.Insert(education);
        }

        public void Update(AccountExtEducation education)
        {
            _accountExtEducationRepository.Update(education);
        }
        public string SaveData()
        {
            string msg;
            try
            {
                _accountExtEducationRepository.Save();
                msg = "Success";
            }
            catch (DbException)
            {

                msg = "Failed";
            }
            return msg;
        }
        public string Delete(int id)
        {

            string msg;
            try
            {
                _accountExtEducationRepository.Delete(id);
                msg = "Success";
            }
            catch (DbException)
            {

                msg = "Failed";
            }
            return msg;
        }



    }
}