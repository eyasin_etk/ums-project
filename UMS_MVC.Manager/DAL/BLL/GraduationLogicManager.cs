﻿using System;
using System.Linq;
using UMS_MVC.DataModel.Models.Student_Tags;
using UMS_MVC.Manager.DAL.DAO.Intefaces;
using UMS_MVC.Manager.DAL.DAO.Repository;

namespace UMS_MVC.Manager.DAL.BLL
{
    public class GraduationLogicManager
    {
        private readonly IGraduationRepository _graduationRepository;

        public GraduationLogicManager()
        {
            _graduationRepository=new GraduationRepository();
        }

        public IQueryable<StudentGraduation> GetAllGraduationList()
        {
            return _graduationRepository.GetAll().AsQueryable();
        }

        public StudentGraduation GetSingleGraduation(int id)
        {
            return _graduationRepository.FindSingle(id);
        }

       
        //Graduation Will generate only from this method
        public int ApplyForGraduation(int studentIdentityId)
        {
            int graduationId = 0;
            var studentGrad = GetAllGraduationList().FirstOrDefault(s=>s.StudentIdentificationId==studentIdentityId);
            if (studentGrad == null)
            {
                var studentGraduation = new StudentGraduation
                {
                    StudentIdentificationId = studentIdentityId,
                    ApplyDate = DateTime.Now,
                    ApplyForGraduationStat = true,
                };
               
            }
            else
            {
                var getGradObj = _graduationRepository.FindSingle(studentGrad.StudentGraduationId);
                getGradObj.StudentIdentificationId = studentIdentityId;
                getGradObj.ApplyDate = DateTime.Now;
                getGradObj.ApplyForGraduationStat = true;
                _graduationRepository.Update(getGradObj);
                graduationId = getGradObj.StudentGraduationId;

            }
            _graduationRepository.Save();
            return graduationId;
        }

        public bool DeleteGraduationProfile(int id)
        {
            var studentGraduation = _graduationRepository.FindSingle(id);
            if (studentGraduation != null)
            {
                _graduationRepository.Delete(studentGraduation);
                _graduationRepository.Save();
                return true;
            }
            return false;
        }
    }
}
