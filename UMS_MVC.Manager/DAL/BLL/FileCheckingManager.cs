﻿using System;

namespace UMS_MVC.Manager.DAL.BLL
{
    public static class FileCheckingManager
    {
        public static string OnlineAdmissionFileLocation(int type = 1)
        {
            string physicalPath = "";
            physicalPath = type == 1 ? @"E:\UMSuploadedFiles\OnlineAdmissionFiles\Images\" : @"E:\UMSuploadedFiles\OnlineAdmissionFiles\No_Images\photo.jpg";

            return physicalPath;
        }

        public static string AccountImageFileLocation()
        {
            return @"E:\UMSuploadedFiles\ProfileImages\AccountImage\";
        }

        public static string UmsImageLocation(int type=1)
        {
            string physicalPath = "";
            physicalPath = type == 1 ? @"E:\UMSuploadedFiles\ProfileImages\StudentImage\" : @"E:\UMSuploadedFiles\OnlineAdmissionFiles\No_Images\photo.jpg";

            return physicalPath;
        }
        public static string UmsFileLocation(int type = 3)
        {
            string physicalPath = "";
            physicalPath = type == 3 ? @"E:\UMSuploadedFiles\StudentInfofiles\" : @"E:\UMSuploadedFiles\OnlineAdmissionFiles\No_Images\photo.jpg";

            return physicalPath;
        }
        public static bool OnlineAdmFileCheck(string fileExt)
        {
            string extension = fileExt.ToLower();
            if (extension == ".jpg" || extension == ".JPG" || extension == ".jpeg"|| extension == ".pdf")
            {
                return true;
            }
            return false;
        }

        

        

        public static string FileUploadLocation()
        {
            string physicalPath = @"E:\UMSuploadedFiles\Notes\";
            return physicalPath;
        }
        public static String BytesToString(long byteCount)
        {
            string[] suf = { "B", "KB", "MB", "GB", "TB", "PB", "EB" };
            if (byteCount == 0)
                return "0" + suf[0];
            long bytes = Math.Abs(byteCount);
            int place = Convert.ToInt32(Math.Floor(Math.Log(bytes, 1024)));
            double num = Math.Round(bytes / Math.Pow(1024, place), 1);
            return (Math.Sign(byteCount) * num).ToString() + suf[place];
        }
        public static bool CheckUploadedFileType(string fileExt)
        {
            string extension = fileExt.ToLower();
            if (extension == ".jpg" || extension == ".doc" || extension == ".docx" ||
                extension == ".pdf" || extension == ".xls" || extension == ".xlsx" ||
                extension == ".ppt" || extension == ".pptx" || extension == ".txt" || extension == ".png" || extension == ".jpeg")
            {
                return true;
            }
            return false;
        }
    }
}
