﻿using System.Collections.Generic;
using System.Linq;
using UMS_MVC.DataModel.Models;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.ViewModels.ViewModels;
using System.Data.Entity;
using Microsoft.Ajax.Utilities;
using UMS_MVC.DataModel.Models.Essential_Types;

namespace UMS_MVC.Manager.DAL.BLL
{
    public class CourseBusinessLogic
    {
        private readonly DepartmentManager _departmentManager ;
        private readonly SemesterManager _semesterManager ;
        private readonly StudentManager _studentManager;
        private readonly CourseForStudentManger _courseForStudentManger;
        private readonly GradeManager _gradeManager;

        public CourseBusinessLogic()
        {
            _gradeManager=new GradeManager();
            _departmentManager = new DepartmentManager();
            _semesterManager = new SemesterManager();
            _studentManager = new StudentManager();
            _courseForStudentManger = new CourseForStudentManger();
        }

        // GET: CourseOtherMatter

        public IEnumerable<DepartmentStatus> GenerateStatisticalData(int fromBatch=35,int toBatch=0)
        {
            var departmentList = _departmentManager.GetAllDepartmentFinal();
            var semesterInfo = _semesterManager.GetActiveSemester();
            var allstudent = _studentManager.GetAllStudentIdentityFromBatchToBatch(fromBatch,toBatch).Where(s => s.StudentInfoId != null).ToList();
            var getAdvisedCourses = _courseForStudentManger.GetAllCoursesFromBatchToBatch(fromBatch,toBatch).Where(s => s.StudentIdentification.StudentInfoId != null);
            var dep = new List<DepartmentStatus>();
            foreach (var department in departmentList.ToList())
            {
                Department department1 = department;
                var sortAllStudent = allstudent.Where(s => s.DepartmentId == department1.DepartmentId).ToList();

                /////existing student//////

                var allExistingStudent = sortAllStudent.Count();
                var existMale =
                    sortAllStudent.Count(s => s.StudentInfo.GenderId == 1);
                var existFemale =
                   sortAllStudent.Count(s => s.StudentInfo.GenderId == 2);
                var courseForStudent =
                    getAdvisedCourses.Where(s => s.CourseForDepartment.DepartmentId == department1.DepartmentId);

                

                //////advised student///// 
                
                var allAdvisedStud = courseForStudent.DistinctBy(s => s.StudentIdentificationId).Count();
                var maleAdv =
                    courseForStudent.DistinctBy(s => s.StudentIdentificationId).Count(d => d.StudentIdentification.StudentInfo.GenderId == 1);
                var femaleAdv =
                    courseForStudent.DistinctBy(s => s.StudentIdentificationId).Count(d => d.StudentIdentification.StudentInfo.GenderId == 2);
                
                //Get GPA of students
                var separateMaleStudents = courseForStudent.Where(s => s.StudentIdentification.StudentInfo.GenderId == 1)
                .OrderBy(s => s.StudentIdentification.StudentId)
                            .DistinctBy(s => s.StudentIdentificationId)
                            .Select(s => s.StudentIdentificationId);
                var separateFemaleStudents = courseForStudent.Where(s => s.StudentIdentification.StudentInfo.GenderId == 2)
                .OrderBy(s => s.StudentIdentification.StudentId)
                            .DistinctBy(s => s.StudentIdentificationId)
                            .Select(s => s.StudentIdentificationId);

                var getGradingofMale = TabulationCreation(separateMaleStudents);
                var getGradingofFeMale = TabulationCreation(separateFemaleStudents);
                

                /////////Failed Student////////
                var allFailedMale = getGradingofMale.Count(s => s.SemesterGpa < 2.0);
                var allFailedFemale = getGradingofFeMale.Count(s => s.SemesterGpa < 2.0);
                var allFailedStudent = allFailedMale+allFailedFemale;
                

                ///////Passed ///////////

                var allPassedMale = getGradingofMale.Count(s => s.SemesterGpa >= 2.0);
                var allPassedFemale = getGradingofFeMale.Count(s => s.SemesterGpa >= 2.0);
                var allPassedStudent = allPassedMale+allPassedFemale;
                ///////////
                var nonGradedStudent =
                    courseForStudent.Where(s => s.LetterGrade == null)
                        .DistinctBy(s => s.StudentIdentificationId)
                        .Count();
                //Get Batches

                var depStatusObj = new DepartmentStatus()
                {
                    DepartmentName = department.DepartmentName,
                    AdvisedFemale = femaleAdv,
                    AdvisedMale = maleAdv,
                    AdvisedTotal = allAdvisedStud,
                    ExistingFemale = existFemale,
                    ExistingMale = existMale,
                    ExistingTotal = allExistingStudent,
                    FailedFemale = allFailedFemale,
                    FailedMale = allFailedMale,
                    FailedTotal = allFailedStudent,
                    PassedFemale = allPassedFemale,
                    PassedMale = allPassedMale,
                    PassedTotal = allPassedStudent,
                    NonGraded = nonGradedStudent
                };
                dep.Add(depStatusObj);
            }
            return dep;
        }

        public List<CumulativeScores> TabulationCreation(IEnumerable<int> studentList)
        {
            var getActiveSemester = _semesterManager.GetActiveSemester().SemesterId;
            var cumulativesList = new List<CumulativeScores>();
            foreach (var i in studentList)
            {
                var getCumulatives = _gradeManager.CalculateCgpaSingleSemester(i, getActiveSemester);
                var cumulativeForStudent = new CumulativeScores
                {
                    StudentId = i,
                    Credits = getCumulatives.Credits,
                    CreditEarned = getCumulatives.CreditEarned,
                    SemesterGpa = getCumulatives.SemesterGpa
                };
                cumulativesList.Add(cumulativeForStudent);
            }
            return cumulativesList;
            

        }
    }
}