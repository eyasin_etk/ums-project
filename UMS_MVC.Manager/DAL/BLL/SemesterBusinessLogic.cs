﻿using System.Collections.Generic;
using System.Linq;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO;

namespace UMS_MVC.Manager.DAL.BLL
{
    public class SemesterBusinessLogic
    {
        private readonly SemesterManager _semesterManager;
        private readonly SectionManager _sectionManager;

        public SemesterBusinessLogic()
        {
            _semesterManager=new SemesterManager();
            _sectionManager=new SectionManager();
        }

        public List<Semester> GetAllHighlightedSectionDropDown()
        {
            var sections = _sectionManager.GetAllHighLightedSection();
            var sortedSemester = (from semester in _semesterManager.GetAllSemestersDefault().AsEnumerable()
                                 select new Semester
                                 {
                                     SemesterId = semester.SemesterId,
                                     SemesterNYear ="["+sections.Count(s=>s.SemesterId==semester.SemesterId).ToString("D3") +"] " +semester.SemesterNYear
                                 }).ToList();
            return sortedSemester;
        }

        public IQueryable<Semester> GetAllSemesterForDropDown()
        {
            var sortedSemester = from semester in _semesterManager.GetAllSemestersDefault()
                                 select new Semester
                                 {
                                     SemesterId = semester.SemesterId,
                                     BatchNo = semester.BatchNo,
                                     SemesterNYear = semester.SemesterNYear
                                 };
            return sortedSemester;
            //return GetAllSemester().Select(o =>
            //    new Semester
            //    {
            //        SemesterId = o.SemesterId,
            //        BatchNo = o.BatchNo,
            //        SemesterNYear = o.SemesterNYear
            //    });
        }
    }
}