﻿using System.Linq;
using UMS_MVC.DataModel.Models.TeacherEvaluation;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IEvaluation;
using UMS_MVC.Manager.DAL.DAO.RServey;

namespace UMS_MVC.Manager.DAL.BLL.TeachersEvaluationManager
{
  public  class SurveyQuestionsLogicManager
  {
      private ISurveyQuestionRepository _surveyQuestionRepository;

      public SurveyQuestionsLogicManager()
      {
         
          _surveyQuestionRepository=new SurveyQuestionRepository();
      }

      public IQueryable<SurveyQuestion> GetAllSurveyQuestions()
      {
          return _surveyQuestionRepository.GetAll();
      }
      public SurveyQuestion FindConfiguration(int id)
      {
          return _surveyQuestionRepository.FindSingle(id);
      }
      public void InsertConfiguration(SurveyQuestion surveyAssesment)
      {
         _surveyQuestionRepository.Add(surveyAssesment);
        
      }
      public void UpdateConfiguration(SurveyQuestion surveyAssesment)
      {
         _surveyQuestionRepository.Update(surveyAssesment);
        
      }
      public void DeleteConfig(int id)
      {
          var surveyQuestion = _surveyQuestionRepository.FindSingle(id);
          _surveyQuestionRepository.Delete(surveyQuestion);
         
      }

      public void Save()
      {
            _surveyQuestionRepository.Save();
        }
  }
}
