﻿using System.Linq;
using UMS_MVC.DataModel.Models.TeacherEvaluation;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IEvaluation;
using UMS_MVC.Manager.DAL.DAO.RServey;

namespace UMS_MVC.Manager.DAL.BLL.TeachersEvaluationManager
{
    public class SurveyAssesmentLogicManager
    {
        private readonly ISurveyAssesmentRepository _surveyAssesment;

        public SurveyAssesmentLogicManager()
        {
           
            _surveyAssesment=new SurveyAssesmentRepository();
        }
        public IQueryable<SurveryAssesment> GetAllSurveyAssesment()
        {
            return  _surveyAssesment.GetAll();
        }
        public SurveryAssesment FindConfiguration(int id)
        {
            return  _surveyAssesment.FindSingle(id);
        }
        public void InsertAssesment(SurveryAssesment surveyAssesment)
        {
             _surveyAssesment.Add(surveyAssesment);
            
        }
        public void UpdateAssesment(SurveryAssesment surveyAssesment)
        {
             _surveyAssesment.Update(surveyAssesment);
             
        }
        public void DeleteAssesment(int id)
        {
            var assesment = _surveyAssesment.FindSingle(id);
            _surveyAssesment.Delete(assesment);
            
        }

        public void Save()
        {
            _surveyAssesment.Save();
        }
    }
}
