﻿using System.Linq;
using UMS_MVC.DataModel.Models.TeacherEvaluation;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IEvaluation;
using UMS_MVC.Manager.DAL.DAO.RServey;

namespace UMS_MVC.Manager.DAL.BLL.TeachersEvaluationManager
{
   static class SurveyAnswerLogicManager
    {
        private static ISurveyAnswerRepository _surveyAnswerRepository;

         static SurveyAnswerLogicManager()
        {
           _surveyAnswerRepository=new SurveyAnswerRepository();
        }
        public static IQueryable<SurveyAnswer> GetAllSurveyAnswers()
        {
            return _surveyAnswerRepository.GetAll();
        }
        public static SurveyAnswer FindConfiguration(int id)
        {
            return _surveyAnswerRepository.FindSingle(id);
        }
        public static void InsertConfiguration(SurveyAnswer surveyAssesment)
        {
            _surveyAnswerRepository.Add(surveyAssesment);
            
        }
        public static void UpdateConfiguration(SurveyAnswer surveyAssesment)
        {
            _surveyAnswerRepository.Update(surveyAssesment);
            
        }
        public static void DeleteConfig(int id)
        {
            var surveyAnswer = _surveyAnswerRepository.FindSingle(id);
            _surveyAnswerRepository.Delete(surveyAnswer);
            
        }

       public static void Save()
       {
          _surveyAnswerRepository.Save();
       }
    }
}
