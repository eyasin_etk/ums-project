﻿using System.Linq;
using UMS_MVC.DataModel.Models.TeacherEvaluation;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IEvaluation;
using UMS_MVC.Manager.DAL.DAO.RServey;

namespace UMS_MVC.Manager.DAL.BLL.TeachersEvaluationManager
{
    public class EvaluationConfigurationLogicManager
    {
        private readonly IEvaluationRepository _evaluationConfigurationRepository;

        public EvaluationConfigurationLogicManager()
        {
            
            _evaluationConfigurationRepository=new EvaluationRepository();
        }

        public IQueryable<EvaluationConfiguration> GetAllEvaluationConfigurations()
        {
            return _evaluationConfigurationRepository.GetAll();
        }

        public EvaluationConfiguration FindConfiguration(int id)
        {
            return _evaluationConfigurationRepository.FindSingle(id);
        }

        public void InsertConfiguration(EvaluationConfiguration evaluationConfiguration)
        {
            _evaluationConfigurationRepository.Add(evaluationConfiguration);
                _evaluationConfigurationRepository.Save();
        }
        public void UpdateConfiguration(EvaluationConfiguration evaluationConfiguration)
        {
            _evaluationConfigurationRepository.Update(evaluationConfiguration);
            _evaluationConfigurationRepository.Save();
        }

        public void DeleteConfig(int id)
        {
            var configuration = _evaluationConfigurationRepository.FindSingle(id);
            _evaluationConfigurationRepository.Delete(configuration);
            _evaluationConfigurationRepository.Save();
        }

    }
}
