﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.DataModel.Models.TeacherEvaluation;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IEvaluation;
using UMS_MVC.Manager.DAL.DAO.RServey;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.Manager.DAL.BLL.TeachersEvaluationManager
{
    public class EvaluationResultLogicManager
    {
        private readonly IEvaluationResultRepository _evaluationResultRepository;
        private readonly SectionManager _sectionManager;
        private readonly CourseForStudentUpdatedManager _studentUpdatedManager;
        private readonly DepartmentManager _departmentManager;
        private readonly TeacherManager _teacherManager;

        public EvaluationResultLogicManager()
        {
            _evaluationResultRepository = new EvaluationResultRepository();
            _sectionManager = new SectionManager();
            _studentUpdatedManager = new CourseForStudentUpdatedManager();
            _departmentManager = new DepartmentManager();
            _teacherManager = new TeacherManager();
        }
        public IQueryable<EvaluationResult> GetAllEvaluationResults()
        {
            return _evaluationResultRepository.GetAll();
        }
        public EvaluationResult FindEvaluationResults(int id)
        {
            return _evaluationResultRepository.FindSingle(id);
        }

        public EvaluationResult FindEvaluationResultBySection(int secId)
        {
            return _evaluationResultRepository.GetAll().FirstOrDefault(s => s.SectionId == secId);
        }

        public IQueryable<Section> GetEvaluationPossibleSectionsByFaculty(int tId, int semId)
        {
            var getQuery = _sectionManager.GetAllSectionsFiltered().Where(s => s.TeacherId == tId && s.SemesterId == semId && s.EvaluationPossible);
            return getQuery.AsQueryable();
        }

        public IQueryable<Section> GetEvaluationPossibleSectionsByDept(int deptId, int semId)
        {
            var getQuery = _sectionManager.GetAllSectionsFiltered().Include(s => s.CourseForDepartment.DepartmentId).Where(s => s.CourseForDepartment.DepartmentId == deptId && s.SemesterId == semId && s.EvaluationPossible);
            return getQuery.AsQueryable();
        }

        public EvaluationResultViewModel CalculateOveralResultOfSemester(int tId, int semId)
        {
            List<int> getQuery = GetEvaluationPossibleSectionsByFaculty(tId, semId).Select(s => s.SectionId).ToList();
            var getResult = GetAllEvaluationResults().Where(s => getQuery.Contains(s.SectionId)).AsQueryable();

            var evaluationResultsSum = new EvaluationResultViewModel
            {
                TotalPointYes = getResult.Sum(s => s.TotalPointYes),
                TotalPointAverage = getResult.Sum(s => s.TotalPointAverage),
                TotalPointNo = getResult.Sum(s => s.TotalPointNo),
                SectionId = 0,
                QuestionId = 0
            };
            return evaluationResultsSum;
        }

        public EvaluationPartialInfo GetPartialInfo(int deptId , int tId )
        {
            var evaluationProjectedResult = new EvaluationPartialInfo();
            if (deptId > 0)
            {
                evaluationProjectedResult.MultipleFaculty = true;
                var department = _departmentManager.GetSingleDeptFiltered(deptId).DepartmentName;
                evaluationProjectedResult.DepartmentName = department;
               
            }
            else
            {
                evaluationProjectedResult.MultipleFaculty = false;
                var teacher = _teacherManager.FindTeacherFirstAndDef(tId);
                evaluationProjectedResult.DepartmentName = teacher.Department.DepartmentName;
                evaluationProjectedResult.FacultyName = teacher.Account.Name;
                evaluationProjectedResult.FacultyImage = teacher.Account.Pic;
                evaluationProjectedResult.FacultyDesignation = teacher.Account.Designation;
            }
            return evaluationProjectedResult;
        }

        public EvaluationProjectedResult GetResultOfEvaluatedSections(int semId, int tId = 0, int deptId = 0)
        {
            var getResult = new List<Section>();
            var evaluationProjectedResult = new EvaluationProjectedResult();
            
            if (deptId > 0)
            {
                getResult = GetEvaluationPossibleSectionsByDept(deptId, semId).Include(s => s.CourseForDepartment).AsEnumerable().ToList();
                evaluationProjectedResult.EvaluationPartialInfo = GetPartialInfo(deptId, 0);
            }
            else
            {
                getResult = GetEvaluationPossibleSectionsByFaculty(tId, semId).Include(s => s.CourseForDepartment).AsEnumerable().ToList();
                evaluationProjectedResult.EvaluationPartialInfo = GetPartialInfo(0, tId);
            }
            var evaluationresults = new List<EvaluationResultViewModel>();
            foreach (var s in getResult.ToList())
            {
                int totalStudent = _studentUpdatedManager.GetAllAdvisingBySection(s.SectionId).Count(i => i.EvaluationComplete);

                var evaluationResultViewModel = new EvaluationResultViewModel
                {
                    QuestionId = 0,
                    SectionId = s.SectionId,
                    CourseName = "[" + s.CourseForDepartment.CourseCode + "] " + s.CourseForDepartment.CourseName,
                    TotalStudent = 0
                };
                if (totalStudent != 0)
                {
                    evaluationResultViewModel.TotalStudent = totalStudent;
                    double totalYes = GetAllEvaluationResults().Where(i => i.SectionId == s.SectionId).Sum(i => i.TotalPointYes);
                    double totalAverage = GetAllEvaluationResults().Where(i => i.SectionId == s.SectionId).Sum(i => i.TotalPointAverage);
                    double totalNo = GetAllEvaluationResults().Where(i => i.SectionId == s.SectionId).Sum(i => i.TotalPointNo);
                    var yesPercent = (totalYes * 100) / totalStudent;
                    var averagePercent = (totalAverage * 100) / totalStudent;
                    var noPercent = (totalNo * 100) / totalStudent;

                    if (!double.IsNaN(yesPercent))
                    {
                        evaluationResultViewModel.TotalPointYes = Math.Round(yesPercent, 0, MidpointRounding.AwayFromZero);
                    }
                    if (!double.IsNaN(averagePercent))
                    {
                        evaluationResultViewModel.TotalPointAverage = Math.Round(averagePercent, 0, MidpointRounding.AwayFromZero);
                    }
                    if (!double.IsNaN(noPercent))
                    {
                        evaluationResultViewModel.TotalPointNo = Math.Round(noPercent, 0, MidpointRounding.AwayFromZero);
                    }
                }
                evaluationresults.Add(evaluationResultViewModel);
            }
            evaluationProjectedResult.EvaluationResultViewModels = evaluationresults;
            return evaluationProjectedResult;
        }

        public void Insert(EvaluationResult evaluationResult)
        {
            _evaluationResultRepository.Add(evaluationResult);
            _evaluationResultRepository.Save();
        }
        public void Update(EvaluationResult evaluationResult)
        {
            _evaluationResultRepository.Update(evaluationResult);
            _evaluationResultRepository.Save();
        }
        public void Delete(int id)
        {
            var evaluationResult = _evaluationResultRepository.FindSingle(id);
            _evaluationResultRepository.Delete(evaluationResult);
            _evaluationResultRepository.Save();
        }
        public int CreateEvaluationById(int sectionId)
        {
            var aresult = new EvaluationResult
            {
                SectionId = sectionId
            };
            Insert(aresult);
            return aresult.EvaluationResultId;
        }
    }
}
