﻿using System.Linq;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO;

namespace UMS_MVC.Manager.DAL.BLL
{
    public class PowerWorkBusinessLogic
    {
        private readonly PowerWorkManager _powerWorkManager;

        public PowerWorkBusinessLogic()
        {
            _powerWorkManager=new PowerWorkManager();                                                                
        }

        public PowerWork GetpowerWork(string methodName)
        {
            var findObj = _powerWorkManager.GetAllPowerWorks().FirstOrDefault(s => s.MethodName == methodName);
            return findObj;
        }

        public bool CheckMethodForAccess(string methodName, int accountId)
        {
            var getMethod =
                _powerWorkManager.GetAllPowerWorkAccesses()
                    .Count(s => s.PowerWork.MethodName == methodName && s.AccountId == accountId)==0;
            return getMethod;
        }
    }
}