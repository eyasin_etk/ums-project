﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.ViewModels.ViewModels;
using UMS_MVC.ViewModels.ViewModels.GradingForSection;

namespace UMS_MVC.Manager.DAL.BLL
{
    public class SectionBusinessLogic
    {
        private readonly TeacherManager _teacherManager;
        private readonly SectionManager _sectionManager;
        private readonly SemesterManager _semesterManager;
        private readonly DepartmentManager _departmentManager;
        private readonly GradingSystemManager _gradingSystemManager;
        private readonly CourseForStudentManger _courseForStudentManger;
        public SectionBusinessLogic()
        {
            _teacherManager=new TeacherManager();
            _sectionManager=new SectionManager();
            _semesterManager=new SemesterManager();
            _departmentManager=new DepartmentManager();
            _gradingSystemManager=new GradingSystemManager();
            _courseForStudentManger=new CourseForStudentManger();
            
        }

        public IEnumerable<Teacher> FilteredTeachers()
        {
            var getCurretSemester = _semesterManager.GetActiveSemester();
            var getAllTeacher = _teacherManager.GetAllTeacherFiltered().Include(s => s.Account).Where(s => s.AccountId != null).ToList();
            var getAllSections = _sectionManager.GetAllSectionsFiltered();
            var sortedTeacher = new List<Teacher>();
            foreach (var teach in getAllTeacher)
            {
                var countSections = getAllSections.Count(s => s.TeacherId == teach.TeacherId);
                var countSectionsCurrentSemester = getAllSections.Count(s => s.TeacherId == teach.TeacherId && s.SemesterId == getCurretSemester.SemesterId);
                var teacher = new Teacher
                {
                    TeacherId = teach.TeacherId,
                    Account = new Account()
                    {
                        Name = teach.Account.Name + " - [ " + countSectionsCurrentSemester + "/" + countSections + " ]",
                        
                    },
                    Department = new Department
                    {
                        DepartmentId = teach.DepartmentId,
                        DepartmentTitle = _departmentManager.GetSingleDeptFiltered(teach.DepartmentId).DepartmentTitle
                    }

                };
                sortedTeacher.Add(teacher);
            }
            return sortedTeacher;
        }

        public List<TeacherSectionFilterViewModel> FilteredTeachersSecind()
        {
            var getCurretSemester = _semesterManager.GetActiveSemester();
            var getAllTeacher =
                _teacherManager.GetAllTeacherFiltered().Include(s => s.Account).Where(s => s.AccountId != null);
            var getAllSections = _sectionManager.GetAllSectionsFiltered();
            var sortTeacher = (from teacher1 in getAllTeacher.ToList()
                let countSectionsCurrentSemester =
                    getAllSections.Count(
                        s => s.TeacherId == teacher1.TeacherId && s.SemesterId == getCurretSemester.SemesterId)
                let countSections = getAllSections.Count(s => s.TeacherId == teacher1.TeacherId)
                select new TeacherSectionFilterViewModel
                {
                    TeacherId = teacher1.TeacherId,

                    Name = teacher1.Account.Name,
                    CurrentSection = countSectionsCurrentSemester,
                    TotalSections = countSections,
                    Designation = teacher1.Account.Designation,
                    Department = _departmentManager.GetSingleDeptFiltered(teacher1.DepartmentId).DepartmentTitle
                }).ToList();
           return sortTeacher;
        }
    

        public SectionStatusModel ManageSectionStatusModel(int id = 0)
        {
            var sectioDetail = _sectionManager.FindSectionFirstDefForGrading(id);
            var departmentName = _departmentManager.GetSingleDeptFiltered(sectioDetail.CourseForDepartment.DepartmentId).DepartmentName;
            if (sectioDetail != null)
            {
                var section = new SectionStatusModel
                {
                    SectionId = sectioDetail.SectionId,
                    SectionName = sectioDetail.SectionName,
                    ProgramName = departmentName,
                    CourseForDepartmentId = sectioDetail.CourseForDepartmentId,
                    CourseForDepartment = new CourseForDepartment
                    {
                        CourseForDepartmentId = sectioDetail.CourseForDepartmentId,
                        CourseCode = sectioDetail.CourseForDepartment.CourseCode,
                        CourseName = sectioDetail.CourseForDepartment.CourseName,
                        CourseType = sectioDetail.CourseForDepartment.CourseType,
                        Credit = sectioDetail.CourseForDepartment.Credit
                    },
                    SemesterId = sectioDetail.SemesterId,
                    Semester = new Semester
                    {
                        SemesterId = sectioDetail.SemesterId,
                        SemesterNYear = sectioDetail.Semester.SemesterNYear,
                        MidTerm = sectioDetail.Semester.MidTerm,
                        FinalTerm = sectioDetail.Semester.FinalTerm
                    },
                    TotalEnrollment = _courseForStudentManger.CountAllStudentInSection(sectioDetail.SectionId),
                    // GradeUploadViewModel = _sectionLogicManager.SectionMarkUploadDefined(id),
                };
                if (sectioDetail.GradingSystemId != null)
                {
                    var findGradingSystem = _gradingSystemManager.FindGradingSystem((int)sectioDetail.GradingSystemId);
                    section.GradingSystem = findGradingSystem;
                }
                if (sectioDetail.TeacherId != null)
                {
                    var teacherName = _teacherManager.FindTeacherFirstAndDef((int)sectioDetail.TeacherId).Account.Name;
                    section.TeacherName = teacherName;
                }
                var getEnrollments =
                    _courseForStudentManger.GetAllCourseForStudentsAcademicsBysection(id);
                if (getEnrollments.Any())
                {
                    //var allAdvisings = getEnrollments.AsEnumerable().Select(s => new SectionAdvisingViewModel
                    //{
                    //    CourseForStudentAcademicId = s.CourseForStudentsAcademicId,
                    //    CourseForDepartmentId = s.CourseForDepartmentId,
                    //    SemesterId = s.SemesterId,
                    //    StudentIdentityId = s.StudentIdentificationId,
                    //    StudentId = s.StudentIdentification.StudentId,
                    //    StudentName = s.StudentIdentification.StudentInfo.StudentName,
                    //    Att = s.Attendance,
                    //    Ct = s.ClassTest,
                    //    Mid = s.Midterm,
                    //    Final = s.FinalTerm,
                    //    Total = s.TotalMark,
                    //    GradePoint = s.Grade,
                    //    LetterGrade = s.LetterGrade,
                    //    TotalGrade = section.CourseForDepartment.Credit * s.Grade,
                    //}).ToList();
                    var allAdvisings = (from s in getEnrollments.ToList()
                        orderby s.StudentIdentification.StudentId, s.StudentIdentification.StudentId
                        select new SectionAdvisingViewModel
                        {
                            CourseForStudentAcademicId = s.CourseForStudentsAcademicId,
                            CourseForDepartmentId = s.CourseForDepartmentId,
                            SemesterId = s.SemesterId,
                            StudentIdentityId = s.StudentIdentificationId,
                            StudentId = s.StudentIdentification.StudentId,
                            StudentName = s.StudentIdentification.StudentInfo.StudentName,
                            Att = s.Attendance,
                            Ct = s.ClassTest,
                            Mid = s.Midterm,
                            Final = s.FinalTerm,
                            Total = s.TotalMark,
                            GradePoint = s.Grade,
                            LetterGrade = s.LetterGrade,
                            TotalGrade = section.CourseForDepartment.Credit * s.Grade,
                        }).ToList();
                    section.Advisings = allAdvisings;
                }
                return section;
            }
            return new SectionStatusModel();
        }

        public static MinifiedGradingViewModel SimplifiedGrading(CourseForStudentsAcademic advising, GradingSystem gradingSystem)
        {

            double atten = ((advising.Attendance / gradingSystem.Attendance) * gradingSystem.AttendancePercent);
            if (double.IsNaN(atten))
            {
                atten = 0;
            }
            double classt = ((advising.ClassTest / gradingSystem.ClassTest) * gradingSystem.ClassTestPercent);
            if (double.IsNaN(classt))
            {
                classt = 0;
            }

            double midt = ((advising.Midterm / gradingSystem.MidTerm) * gradingSystem.MidTermPercent);
            if (double.IsNaN(midt))
            {
                midt = 0;
            }
            double finalT = ((advising.FinalTerm / gradingSystem.FinalTerm) * gradingSystem.FinalTermPercent);
            if (double.IsNaN(finalT))
            {
                finalT = 0;
            }
            return new MinifiedGradingViewModel
            {
                Att = atten,
                Ct = classt,
                Mid = midt,
                Final = finalT
            };
        }
        public SectionStatusModel ManageSectiontatusFormattedModel(int secid = 0)
        {
            var sectioDetail = _sectionManager.FindSectionFirstDefForGrading(secid);
            var departmentName = _departmentManager.GetSingleDeptFiltered(sectioDetail.CourseForDepartment.DepartmentId).DepartmentName;
            if (sectioDetail != null)
            {
                var section = new SectionStatusModel
                {
                    SectionId = sectioDetail.SectionId,
                    SectionName = sectioDetail.SectionName,
                    ProgramName = departmentName,
                    CourseForDepartmentId = sectioDetail.CourseForDepartmentId,
                    CourseForDepartment = new CourseForDepartment
                    {
                        CourseForDepartmentId = sectioDetail.CourseForDepartmentId,
                        CourseCode = sectioDetail.CourseForDepartment.CourseCode,
                        CourseName = sectioDetail.CourseForDepartment.CourseName,
                        CourseType = sectioDetail.CourseForDepartment.CourseType,
                        Credit = sectioDetail.CourseForDepartment.Credit
                    },
                    SemesterId = sectioDetail.SemesterId,
                    Semester = new Semester
                    {
                        SemesterId = sectioDetail.SemesterId,
                        SemesterNYear = sectioDetail.Semester.SemesterNYear,
                        MidTerm = sectioDetail.Semester.MidTerm,
                        FinalTerm = sectioDetail.Semester.FinalTerm
                    },
                    TotalEnrollment = _courseForStudentManger.CountAllStudentInSection(sectioDetail.SectionId),
                    // GradeUploadViewModel = _sectionLogicManager.SectionMarkUploadDefined(id),
                };
                if (sectioDetail.GradingSystemId != null)
                {
                    var findGradingSystem = _gradingSystemManager.FindGradingSystem((int) sectioDetail.GradingSystemId);
                    section.GradingSystem = findGradingSystem;
                }
                if (sectioDetail.TeacherId != null)
                {
                    var teacherName = _teacherManager.FindTeacherFirstAndDef((int) sectioDetail.TeacherId).Account.Name;
                    section.TeacherName = teacherName;
                }
                var getEnrollments =
                    _courseForStudentManger.GetAllCourseForStudentsAcademicsBysection(secid);
                var advisingLists=new List<SectionAdvisingViewModel>();
                if (getEnrollments.Any())
                {
                    foreach (var advs in getEnrollments)
                    {
                        var getgrades = SimplifiedGrading(advs, section.GradingSystem);
                        advisingLists.Add(new SectionAdvisingViewModel
                        {
                            CourseForStudentAcademicId = advs.CourseForStudentsAcademicId,
                            CourseForDepartmentId = advs.CourseForDepartmentId,
                            SemesterId = advs.SemesterId,
                            StudentIdentityId = advs.StudentIdentificationId,
                            StudentId = advs.StudentIdentification.StudentId,
                            StudentName = advs.StudentIdentification.StudentInfo.StudentName,
                            Att = getgrades.Att,
                            Ct = getgrades.Ct,
                            Mid = getgrades.Mid,
                            Final = getgrades.Final,
                            Total = advs.TotalMark,
                            GradePoint = advs.Grade,
                            LetterGrade = advs.LetterGrade,
                            TotalGrade = sectioDetail.CourseForDepartment.Credit * advs.Grade
                        });
                    }

                    section.Advisings = advisingLists;
                }
                return section;
            }
            return new SectionStatusModel();
        }
    }
}