﻿using System;
using System.Data.Entity;
using UMS_MVC.DataModel.Connection;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.ViewModels.ViewModels;
using UMS_MVC.ViewModels.ViewModels.GradingForSection;

namespace UMS_MVC.Manager.DAL.BLL
{
    public class MarkSubmitLogicManager
    {
        private readonly GradingSystemManager _gradingSystemManager;
        //private readonly CourseLogicManger _courseLogicManger;
        private readonly CourseForStudentManger _forStudentUpdatedLogicManager;
        private readonly DepartmentManager _departmentManager;
        private readonly CourseForDepartmentManager _courseForDepartmentManager;
        private readonly SectionManager _sectionManager;
        private readonly UmsDbContext _db;


        public MarkSubmitLogicManager()
        {
            _gradingSystemManager = new GradingSystemManager();
            //_courseLogicManger=new CourseLogicManger();
            _forStudentUpdatedLogicManager = new CourseForStudentManger();
            _departmentManager = new DepartmentManager();
            _courseForDepartmentManager = new CourseForDepartmentManager();
            _sectionManager=new SectionManager();
            _db = new UmsDbContext();

        }

        public static GradePontViewModel GetGradePointCalculation(double calculateTotal = 0)
        {

            double grade = 0;
            string gradeLetter = string.Empty;
            if (calculateTotal >= 0 && calculateTotal < 40)
            {
                grade = 0.0;
                gradeLetter = "F";
            }
            else if (calculateTotal >= 40 && calculateTotal < 45)
            {
                grade = 2.0;
                gradeLetter = "D";
            }
            else if (calculateTotal >= 45 && calculateTotal < 50)
            {
                grade = 2.25;
                gradeLetter = "C";
            }
            else if (calculateTotal >= 50 && calculateTotal < 55)
            {
                grade = 2.5;
                gradeLetter = "C+";
            }
            else if (calculateTotal >= 55 && calculateTotal < 60)
            {
                grade = 2.75;
                gradeLetter = "B-";
            }
            else if (calculateTotal >= 60 && calculateTotal < 65)
            {
                grade = 3.0;
                gradeLetter = "B";
            }
            else if (calculateTotal >= 65 && calculateTotal < 70)
            {
                grade = 3.25;
                gradeLetter = "B+";
            }
            else if (calculateTotal >= 70 && calculateTotal < 75)
            {
                grade = 3.5;
                gradeLetter = "A-";
            }
            else if (calculateTotal >= 75 && calculateTotal < 80)
            {
                grade = 3.75;
                gradeLetter = "A";
            }
            else if (calculateTotal >= 80)
            {
                grade = 4.0;
                gradeLetter = "A+";
            }
            var aGradePoint = new GradePontViewModel
            {
                GradeP = grade,
                GradeLetter = gradeLetter
            };
            return aGradePoint;
        }

        public CourseForStudentsAcademic UpdatedActualMark(CourseForStudentsAcademic academic,
            GradingSystem getCourseGradings)
        {
            double att = 0;
            double classTest = 0;
            double mid = 0;
            double final = 0;
            if (academic.Attendance > 0)
            {
                att = (academic.Attendance/getCourseGradings.Attendance)*getCourseGradings.AttendancePercent;
                if (double.IsNaN(att))
                {
                    att = 0;
                }
            }
            if (academic.ClassTest > 0)
            {
                classTest = (academic.ClassTest / getCourseGradings.ClassTest) * getCourseGradings.ClassTestPercent;
                if (double.IsNaN(classTest))
                {
                    classTest = 0;
                }
            }
            if (academic.Midterm > 0)
            {
                mid = (academic.Midterm/getCourseGradings.MidTerm)*getCourseGradings.MidTermPercent;
                if (double.IsNaN(mid))
                {
                    mid = 0;
                }
            }
            if (academic.FinalTerm > 0)
            {
                final = (academic.FinalTerm/getCourseGradings.FinalTerm)*getCourseGradings.FinalTermPercent;
                if (double.IsNaN(final))
                {
                    final = 0;
                }
            }
            var totalMark = att + classTest + mid + final;
            academic.TotalMark = Math.Round(totalMark, 0, MidpointRounding.AwayFromZero);
            //var gradePontViewModel = GetGradePointCalculation(totalMark);
            var gradePontViewModel = GetGradePointCalculation(academic.TotalMark);
            academic.Grade = gradePontViewModel.GradeP;
            academic.LetterGrade = gradePontViewModel.GradeLetter;
            var getCourseInfo =
                _courseForDepartmentManager.GetSingleCourseFiltered(academic.CourseForDepartmentId).Credit;
            academic.TotalGrade = academic.Grade*getCourseInfo;
            return academic;

        }

        public CourseForStudentsAcademic CalculateGradeForAdvisedCourseUpdated(CourseForStudentsAcademic sectionAdvisingModel, GradingSystem gradingSystem)
        {
            var calculateAcademic = UpdatedActualMark(sectionAdvisingModel, gradingSystem);
            calculateAcademic.CourseStatusId = 2;
            return calculateAcademic;
        }

        public ResponseModel CheckBeforeSave(CourseForStudentsAcademic advisingViewModel, GradingSystem gradingSystem)
        {
            

            if (advisingViewModel.Attendance < 0 || advisingViewModel.Attendance > (gradingSystem.Attendance + 1))
            {
                return new ResponseModel
                {
                    IsSuccess = false,
                    Message = "Invalid Att"
                };
            }
            if (advisingViewModel.ClassTest < 0 || advisingViewModel.ClassTest > (gradingSystem.ClassTest + 1))
            {
                return new ResponseModel
                {
                    IsSuccess = false,
                    Message = "Invalid CT"
                };
            }
            if (advisingViewModel.Midterm < 0 || advisingViewModel.Midterm > (gradingSystem.MidTerm + 1))
            {
                return new ResponseModel
                {
                    IsSuccess = false,
                    Message = "Invalid Mid"
                };
            }
            if (advisingViewModel.FinalTerm < 0 || advisingViewModel.FinalTerm > (gradingSystem.FinalTerm + 1))
            {
                return new ResponseModel
                {
                    IsSuccess = false,
                    Message = "Invalid Final"
                };
            }
            return new ResponseModel();

        }

        public void UpdateMarkModified(CourseForStudentsAcademic courseForStudentsAcademic)
        {
            var getActualCourseForStudent =
              _db.CourseForStudentsAcademics.Find(courseForStudentsAcademic.CourseForStudentsAcademicId);

            getActualCourseForStudent.Attendance = courseForStudentsAcademic.Attendance;
            getActualCourseForStudent.ClassTest = courseForStudentsAcademic.ClassTest;
            getActualCourseForStudent.Midterm = courseForStudentsAcademic.Midterm;
            getActualCourseForStudent.FinalTerm = courseForStudentsAcademic.FinalTerm;
            getActualCourseForStudent.TotalMark = courseForStudentsAcademic.TotalMark;
            getActualCourseForStudent.Grade = courseForStudentsAcademic.Grade;
            getActualCourseForStudent.LetterGrade = courseForStudentsAcademic.LetterGrade;
            getActualCourseForStudent.TotalGrade = courseForStudentsAcademic.TotalGrade;
            getActualCourseForStudent.GradingSystemId = courseForStudentsAcademic.GradingSystemId;
            getActualCourseForStudent.CourseStatusId = courseForStudentsAcademic.CourseStatusId;
            getActualCourseForStudent.SectionId = courseForStudentsAcademic.SectionId;
            try
            {
                _db.Configuration.AutoDetectChangesEnabled = false;
                _db.Entry(getActualCourseForStudent).State = EntityState.Modified;
                _db.SaveChanges();

            }
            finally
            {
                _db.Configuration.AutoDetectChangesEnabled = true;
            }
        }
        public void SaveMarkSubmitBySpecifiedUser(GradingHistory gradingHistory)
        {
            _db.GradingHistories.Add(gradingHistory);
            _db.SaveChanges();
        }

        
    }
}

