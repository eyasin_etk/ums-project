﻿using System;

namespace UMS_MVC.Manager.DAL.BLL
{
   static class GuidIdConverter
    {
        internal static string GuidToBase64(Guid guid)
        {
            return Convert.ToBase64String(guid.ToByteArray()).Replace("/", "-").Replace("+", "_").Replace("=", "");
        }

        internal static Guid Base64ToGuid(string base64)
        {
            Guid guid;
            base64 = base64.Replace("-", "/").Replace("_", "+") + "==";
            try
            {
                guid = new Guid(Convert.FromBase64String(base64));
            }
            catch (Exception ex)
            {
                throw new Exception("Bad Base64 conversion to GUID", ex);
            }

            return guid;
        }
    }
}
