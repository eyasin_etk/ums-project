﻿using System.Web;
using System.Web.Mvc;

namespace UMS_MVC.ApiService
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
