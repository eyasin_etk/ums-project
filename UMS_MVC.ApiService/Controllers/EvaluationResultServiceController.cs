﻿using System;
using System.Linq;
using System.Web.Http;
using UMS_MVC.ApiService.Security;
using UMS_MVC.Manager.DAL.BLL.TeachersEvaluationManager;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers
{
   
    public class EvaluationResultServiceController : ApiController
    {
        private readonly EvaluationResultLogicManager _evaluationResultLogicManager;
        private readonly SectionManager _sectionManager;

        public EvaluationResultServiceController()
        {
            _evaluationResultLogicManager = new EvaluationResultLogicManager();
            _sectionManager = new SectionManager();
        }
        [HttpGet]
        [ActionName("CalculateEveryEvaluation")]
        public ResponseModel CalculateEveryEvaluation(int id) // sectionId
        {
            ResponseModel responseModel;
            try
            {
                var evaluationResults = _evaluationResultLogicManager.GetAllEvaluationResults().Where(s => s.SectionId == id).Select(s => new EvaluationResultViewModel
                {
                    EvaluationResultId = s.EvaluationResultId,
                    SectionId = s.SectionId,
                    QuestionId = s.QuestionId,
                    TotalPointYes = s.TotalPointYes,
                    TotalPointAverage = s.TotalPointAverage,
                    TotalPointNo = s.TotalPointNo,
                    TotalStudent = s.TotalStudent
                }).ToList();
                responseModel = new ResponseModel(evaluationResults);
            }
            catch (Exception exception)
            {
                responseModel = new ResponseModel(isSuccess: true, message: "Error", exception: exception);
            }
            return responseModel;
        }
        [HttpGet]
        [ActionName("CalculateEvaluationResult")]
        public ResponseModel CalculateEvaluationResult(int tId, int semId) //teacherId
        {
            ResponseModel responseModel;
            try
            {
                var resultOfEvaluatedSections = _evaluationResultLogicManager.GetResultOfEvaluatedSections(semId: semId, tId: tId);
                responseModel =new ResponseModel(resultOfEvaluatedSections);
            }
            catch (Exception exception)
            {
                responseModel = new ResponseModel(isSuccess: true, message: "Error", exception: exception);
            }
            return responseModel;

        }
    }
}
