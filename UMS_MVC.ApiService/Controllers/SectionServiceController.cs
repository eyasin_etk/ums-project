﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using UMS_MVC.ApiService.Security;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.ViewModels.ViewModels;
using UMS_MVC.ViewModels.ViewModels.Scrutiny;

namespace UMS_MVC.ApiService.Controllers
{
    
    public class SectionServiceController : ApiController
    {
        private readonly SectionManager _sectionLogicManager;
        private readonly CourseForStudentManger _courseForStudentManger;


        public SectionServiceController()
        {
            _sectionLogicManager = new SectionManager();
            _courseForStudentManger = new CourseForStudentManger();
        }
        [HttpGet]
        [System.Web.Http.ActionName("SectionForScruBySemesterDep")]
        public ResponseModel SectionForScruBySemesterDep(int semId, int courseforDepId, int teacherId, int secId = 0)
        {
            ResponseModel responseModel;
            IQueryable<CourseForStudentsAcademic> getQuery = _courseForStudentManger.GetAllCourseForStudentFiltered().AsQueryable();
            IQueryable<Section> sectionQuery =
                _sectionLogicManager.GetAllSectionsFiltered().Include(s => s.CourseForDepartment).AsQueryable();
            try
            {
                if (semId > 0)
                {
                    sectionQuery = sectionQuery.Where(s => s.SemesterId == semId);
                }
                if (courseforDepId > 0)
                {
                    sectionQuery = sectionQuery.Where(s => s.CourseForDepartmentId == courseforDepId);
                }
                if (teacherId > 0)
                {
                    sectionQuery = sectionQuery.Where(s => s.TeacherId == teacherId);
                }
                if (secId > 0)
                {
                    sectionQuery = sectionQuery.Where(s => s.SectionId == secId);
                }
                if (semId == 0 && courseforDepId == 0 && teacherId == 0 && secId == 0)
                {
                    sectionQuery = sectionQuery.Take(40);
                }
                var getSections = (from sec in
                       sectionQuery.AsEnumerable()
                      select new SectionsScrutinyInfoViewModel
                                   {
                                       SectionId = sec.SectionId,
                                       CourseName = sec.CourseForDepartment.CourseName,
                                       CountLevel1Status = getQuery.Count(i => i.SectionId == sec.SectionId).ToString() + "/" + getQuery.Count(i => i.SectionId == sec.SectionId && i.FirstScrutinized).ToString(),
                                       CountLevel2Status = getQuery.Count(i => i.SectionId == sec.SectionId).ToString() + "/" + getQuery.Count(i => i.SectionId == sec.SectionId && i.SecondScrutinized).ToString()
                                   }).ToList();
                responseModel = getSections.Any() ? new ResponseModel(getSections) : new ResponseModel(isSuccess: false, message: "No Data Found");
            }
            catch (Exception exception)
            {
                responseModel = new ResponseModel(isSuccess: false, message: "Error", exception: exception);
            }
            return responseModel;
        }
        [HttpGet]
        [System.Web.Http.ActionName("SectionBySemDep")]
        public ResponseModel SectionBySemDep(int semId, int deptId)
        {
            ResponseModel responseModel = null;
            try
            {
                if (semId > 0)
                {
                    var getSections =
                        _sectionLogicManager.GetAllSectionsFiltered().Include(s => s.CourseForDepartment).Include(s => s.CourseForDepartment.Department)
                            .Where(a => a.SemesterId == semId && a.CourseForDepartment.DepartmentId == deptId)
                            .AsEnumerable()
                            .Select(s => new SectionDropDownViewModel
                            {
                                SectionId = s.SectionId,
                                SectionName = s.SectionName,
                                CourseForDeptId = s.CourseForDepartmentId,
                                CourseCode = s.CourseForDepartment.CourseCode,
                                CourseNameWithCounter = "[" + _sectionLogicManager.CountAllStudentInSection(s.SectionId).ToString("###") + "-Students] " + s.CourseForDepartment.CourseName,
                                SemesterId = s.SemesterId,
                                SemesterSerilizer = "Semester " + s.CourseForDepartment.SerializedSemesterId

                            });
                    if (getSections.Any())
                    {
                        responseModel = new ResponseModel(getSections);
                    }
                }
                else
                {
                    responseModel = new ResponseModel(isSuccess: false, message: "No data found.");
                }
            }
            catch (Exception exception)
            {
                responseModel = new ResponseModel(isSuccess: true, message: "Error", exception: exception);
            }
            return responseModel;
        }

        [HttpGet]
        [System.Web.Http.ActionName("FindSectionByTeacherId")]
        public ResponseModel FindSectionByTeacherId(int id)// TeacherId
        {
            ResponseModel responseModel = null;
            try
            {
                if (id > 0)
                {
                    var getSections =
                        _sectionLogicManager.GetAllSectionsFiltered().Include(s => s.Semester).Include(s => s.CourseForDepartment)
                            .Where(i => i.TeacherId == id)
                            .AsEnumerable()
                            .Select(s => new SectionDropDownViewModel
                            {
                                SectionId = s.SectionId,
                                SectionName = s.SectionName,
                                CourseForDeptId = s.CourseForDepartmentId,
                                CourseCode = s.CourseForDepartment.CourseCode,
                                CourseNameWithCounter = "[" + _sectionLogicManager.CountAllStudentInSection(s.SectionId).ToString("###") + "-Students] " + s.CourseForDepartment.CourseName,
                                SemesterId = s.SemesterId,
                                SemesterName = s.Semester.SemesterNYear
                            });
                    if (getSections.Any())
                    {
                        responseModel = new ResponseModel(getSections);
                    }
                }
                else
                {
                    responseModel = new ResponseModel(isSuccess: false, message: "No data found.");
                }
            }
            catch (Exception exception)
            {
                responseModel = new ResponseModel(isSuccess: true, message: "Error", exception: exception);
            }
            return responseModel;
        }
        //[HttpGet]
        //[ActionName("FindSection")]
        //public ResponseModel FindSection(int id)
        //{
        //    var s = _sectionLogicManager.GetSingleSection(id);
        //    ResponseModel responseModel;
        //    if (s != null)
        //    {
        //        var section = new Section
        //        {
        //            SectionId = s.SectionId,
        //            SectionName = s.SectionName,
        //            CourseForDepartmentId = s.CourseForDepartmentId,
        //            CourseForDepartment = new CourseForDepartment
        //            {
        //                CourseForDepartmentId = s.CourseForDepartmentId,
        //                CourseCode = s.CourseForDepartment.CourseCode,
        //                CourseName = s.CourseForDepartment.CourseName,
        //                CourseType = s.CourseForDepartment.CourseType
        //            },
        //            SemesterId = s.SemesterId,
        //            Semester = new Semester
        //            {
        //                SemesterId = s.SemesterId,
        //                SemesterNYear = s.Semester.SemesterNYear
        //            }
        //        };
        //        responseModel=new ResponseModel(section);
        //    }
        //    else
        //    {
        //        responseModel = new ResponseModel(isSuccess: false, message: "No enrollment found");
        //    }
        //    return responseModel;
        //}

    }
}
