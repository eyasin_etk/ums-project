﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using UMS_MVC.ApiService.Security;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers
{
   
    public class AdvisingStatusServiceController : ApiController
    {
        private readonly SemesterManager _semesterLogicManager;

        private readonly PaymentManager _paymentRegistration;
        private readonly DepartmentManager _departmentManager;
        public AdvisingStatusServiceController()
        {
            _semesterLogicManager = new SemesterManager();
            _paymentRegistration = new PaymentManager();
            _departmentManager = new DepartmentManager();
        }
        public ResponseModel Get()
        {
            var allSemester = _semesterLogicManager.GetAllSemester().AsEnumerable().Select(s => new SemesterViewModel
            {
                SemesterId = s.SemesterId,
                SemesterNYear = s.SemesterNYear,
                ActiveSemester = s.ActiveSemester,
                MidTerm = s.MidTerm,
                FinalTerm = s.FinalTerm,
                BatchNo = s.BatchNo,
                CourseAdvising = s.CourseAdvising,
                SpecialGradeuploadDeadLine = s.SpecialGradeuploadDeadLine
            });
            return new ResponseModel(allSemester);
        }

        public ResponseModel Get(int id)
        {
            var sem = new Semester();
            //var getActivatedSem =
            if (id == 0)
            {
                sem = _semesterLogicManager.GetActiveSemester();
            }
            else
            {
                sem = _semesterLogicManager.GetSingleSemester(id);
            }
            var studentCount = new List<AdmissionStatusViewModel>();
            foreach (var a in _departmentManager.GetAllDepartmentFiltered())
            {
                var newStatus = new AdmissionStatusViewModel
                {
                    DepartmentId = a.DepartmentId,
                    DepartmentName = a.DepartmentName,
                    SemesterId = sem.SemesterId,
                    SemesterName = sem.SemesterNYear,

                    CurrentAdmittedStudent = _paymentRegistration.GetAllPaymentRegistrationsFiltered().Include(s => s.StudentIdentification.DepartmentId).Count(s => s.SemesterId == sem.SemesterId && s.StudentIdentification.DepartmentId == a.DepartmentId)

                };
                studentCount.Add(newStatus);
            }

            return new ResponseModel { Data = studentCount };
        }
    }
}
