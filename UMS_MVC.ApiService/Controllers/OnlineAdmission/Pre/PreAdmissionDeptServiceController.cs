﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using UMS_MVC.ApiService.Security;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers.OnlineAdmission.Pre
{
    public class PreAdmissionDeptServiceController : ApiController
    {
        private readonly DepartmentManager _departmentManager;
        public PreAdmissionDeptServiceController()
        {
            _departmentManager = new DepartmentManager();
        }

        public ResponseModel Get()
        {
            ResponseModel responseModel;

            try
            {
                var programExceptList = new List<int> { 20, 21, 24 };

                var getAlldepartment = (from dept in _departmentManager.GetAllDepartmentFiltered()
                                        where !programExceptList.Contains(dept.DepartmentId)
                                        select new Department
                                        {
                                            DepartmentId = dept.DepartmentId,
                                            DepartmentName = dept.DepartmentName,
                                            DepartmentTitle = dept.DepartmentTitle,
                                            RequiredCredit = dept.RequiredCredit,
                                            DepartmentCode = dept.DepartmentCode,
                                            TotalSemester = dept.TotalSemester,
                                            TotalCost = dept.TotalCost,
                                            SchoolId = dept.SchoolId
                                        }).ToList();
                responseModel = new ResponseModel(getAlldepartment);
            }
            catch (Exception exception)
            {
                responseModel = new ResponseModel(isSuccess: true, message: "Error", exception: exception);
            }
            return responseModel;
        }

       
    }
}
