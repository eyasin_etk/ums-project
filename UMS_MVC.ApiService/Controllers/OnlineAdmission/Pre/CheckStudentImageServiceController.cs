﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using UMS_MVC.Manager.DAL.BLL;
using UMS_MVC.Manager.DAL.DAO.Intefaces;
using UMS_MVC.Manager.DAL.DAO.Repository;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers.OnlineAdmission.Pre
{
    
    public class CheckStudentImageServiceController : ApiController
    {
        private readonly IPreAdmissionStudentInfoRepository _admissionStudentInfo;

        public CheckStudentImageServiceController()
        {
           _admissionStudentInfo=new PreAdmissionStudentInfoRepository();
        }
        public HttpResponseMessage Get()
        {
            HttpResponseMessage result;

            var localFilePath = FileCheckingManager.OnlineAdmissionFileLocation(2);
            try
            {
              result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(new FileStream(localFilePath, FileMode.Open, FileAccess.Read));
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = "NoImage";
            }
            catch (Exception e)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError);
            }

            return result;
        }
        public ResponseModel Get(Guid? studentToken)
        {
            ResponseModel responseModel;
            try
            {
                if (!studentToken.HasValue || studentToken == Guid.Empty)
                {
                    var msg = "No student token provided.";
                    responseModel=new ResponseModel(message:msg, isSuccess:false);
                }
                else
                {
                    var studentImageName = _admissionStudentInfo.GetExistStudentImage((Guid)studentToken);
                    responseModel=new ResponseModel(studentImageName);
                }
               
            }
            catch (Exception e)
            {
                responseModel=new ResponseModel(isSuccess:false, exception:e);
            }
            return responseModel;
        }
    }
}
