﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;
using UMS_MVC.ApiService.Security;
using UMS_MVC.Manager.DAL.BLL;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.Manager.DAL.DAO.Intefaces;
using UMS_MVC.Manager.DAL.DAO.Repository;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers.OnlineAdmission.Pre
{
    public class ImageUploadServiceController : ApiController
    {
        private readonly IPreAdmissionStudentInfoRepository _admissionStudentInfo;
        private readonly OnlineAdmissionManager _onlineAdmissionManager;

        public ImageUploadServiceController()
        {
            _admissionStudentInfo = new PreAdmissionStudentInfoRepository();
            _onlineAdmissionManager = new OnlineAdmissionManager();
        }
        
        public HttpResponseMessage Get(Guid? studentToken,int stamp=0)
        {
            HttpResponseMessage result;

            var localFilePath = FileCheckingManager.OnlineAdmissionFileLocation();
            try
            {
                if (!studentToken.HasValue || studentToken == Guid.Empty)
                {
                    localFilePath = FileCheckingManager.OnlineAdmissionFileLocation(2);
                }
                else
                {
                    var studentInfo = _admissionStudentInfo.GetExistStudentImage((Guid)studentToken);
                    if (!string.IsNullOrEmpty(studentInfo))
                    {
                        localFilePath += studentInfo;
                    }
                    if (!File.Exists(localFilePath))
                    {
                        localFilePath = FileCheckingManager.OnlineAdmissionFileLocation(2);
                    }
                }
                result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(new FileStream(localFilePath, FileMode.Open, FileAccess.Read));
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = "StudentImage";
            }
            catch (Exception e)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError);
            }

            return result;
        }
        
        public async Task<ResponseModel> Post()
        {
            var httpRequest = HttpContext.Current.Request;
            ResponseModel responseModel = null;
            string fileNames = httpRequest.Form["newFileName"];
            string uploadedBy = httpRequest.Form["sumittedBy"];
            try
            {
                if (!string.IsNullOrEmpty(uploadedBy))
                {
                    var guidStudentToken = new Guid(fileNames);
                    var guidAsync = await _admissionStudentInfo.CheckExistingStudent(guidStudentToken);
                    if (guidAsync)
                    {
                        _onlineAdmissionManager.StoreImageName(guidStudentToken);
                        if (httpRequest.Files.Count > 0)
                        {
                            foreach (string file in httpRequest.Files)
                            {
                                var postedFile = httpRequest.Files[file];
                                WebImage img = new WebImage(postedFile.InputStream);
                                if (img.Height > 450)
                                    img.Resize(350, 450);
                                var fileExte = Path.GetExtension(postedFile.FileName);
                                string makeFileName = guidStudentToken + fileExte;
                                if (FileCheckingManager.OnlineAdmFileCheck(fileExte))
                                {
                                    string filelocation = FileCheckingManager.OnlineAdmissionFileLocation();
                                    img.Save(filelocation + makeFileName);

                                    responseModel = new ResponseModel(message: "File saved sussessfully.");
                                }
                                else
                                {
                                    responseModel = new ResponseModel(isSuccess: false, message: "File type not supported.");
                                }
                            }
                        }
                        else
                        {
                            responseModel = new ResponseModel(isSuccess: false, message: "No File to upload");
                        }
                    }
                }

                else
                {
                    responseModel = new ResponseModel(isSuccess: false, message: "Session out. Contact to admission office.");
                }
            }
            catch (Exception e)
            {
                responseModel = new ResponseModel(isSuccess: false, message: "Error", exception: e);
            }
            return responseModel;
        }
    }
}
