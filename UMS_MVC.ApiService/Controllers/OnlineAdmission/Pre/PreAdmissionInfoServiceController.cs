﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using UMS_MVC.ApiService.Security;
using UMS_MVC.DataModel.Models.PreAdmission;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.Manager.DAL.DAO.Intefaces;
using UMS_MVC.Manager.DAL.DAO.Repository;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers.OnlineAdmission.Pre
{
    
    public class PreAdmissionInfoServiceController : ApiController
    {
        private readonly IPreAdmissionStudentInfoRepository _admissionStudentInfo;
        private readonly OnlineAdmissionManager _admissionManager;
        

        public PreAdmissionInfoServiceController()
        {
            _admissionStudentInfo=new PreAdmissionStudentInfoRepository();
            _admissionManager=new OnlineAdmissionManager();
           
        }
        public ResponseModel Get()
        {
            ResponseModel responseModel;
            var asyncQuery = _admissionStudentInfo.GetAll().ToList();
            try
            {
                responseModel = asyncQuery.Any() ? new ResponseModel(asyncQuery) : new ResponseModel(isSuccess:true, message:"No Data Found!");
            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(exception: es, message: "Error", isSuccess: false);
            }
            return responseModel;
        }

        public async Task<ResponseModel> Get(Guid id)
        {
            ResponseModel responseModel;
            try
            {
                var asyncQuery = await _admissionStudentInfo.FindSingleInfoGuidAsync(id);
                responseModel = asyncQuery != null ? new ResponseModel(asyncQuery) : new ResponseModel(isSuccess:false, message:"No Data Found.");
            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(exception: es, message: "Error", isSuccess: false);
            }
            return responseModel;
        }

        public async Task<ResponseModel> Post(PreAdmissionStudentInfo studentInfo)
        {
            ResponseModel responseModel;
            string personalContactNo = studentInfo.PersonalContactNo;
            var checkDuplicate = await _admissionStudentInfo.CheckDuplicateByPhone(personalContactNo);
            try
            {
                if (checkDuplicate)
                {
                    if (ModelState.IsValid)
                    {
                        if (studentInfo.PreAdmissionStudentInfoId == Guid.Empty)
                        {
                           
                            var studentObj = _admissionManager.InsertOnly(studentInfo);
                            responseModel = new ResponseModel(studentObj);
                        }
                        else
                        {
                            responseModel = new ResponseModel(isSuccess:false, message:"Can not update information.");
                        }
                       }
                    else
                    {
                        var custMsg = ModelState.SelectMany(state => state.Value.Errors).Aggregate(string.Empty, (current, error) => current + (error.ErrorMessage + ','));
                     responseModel = new ResponseModel(isSuccess: false, message: custMsg);
                    }
                }
                else
                {
                    return new ResponseModel(isSuccess: false,
                        message: personalContactNo + ", this Phone Number is already used. Try another.");
                }
            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(exception: es, message: "Error", isSuccess: false);
            }
            return responseModel;
        }
      
        //public ResponseModel Delete(Guid id)
        //{
        //    ResponseModel responseModel;
        //    try
        //    {
        //        _admissionManager.Delete(id);
        //        responseModel = new ResponseModel();
        //    }
        //    catch (Exception es)
        //    {
        //        responseModel = new ResponseModel(isSuccess: false, message: "Error !", exception: es);
        //    }
        //    return responseModel;
        //}
    }
}
