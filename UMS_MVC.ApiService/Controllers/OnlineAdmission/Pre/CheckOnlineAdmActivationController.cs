﻿using System;
using System.Web.Http;
using UMS_MVC.Manager.DAL.DAO.Intefaces;
using UMS_MVC.Manager.DAL.DAO.Repository;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers.OnlineAdmission.Pre
{
    public class CheckOnlineAdmActivationController : ApiController
    {
        private readonly ISemesterRepository _semesterRepository;

        public CheckOnlineAdmActivationController()
        {
            _semesterRepository = new SemesterRepository();
        }

        public ResponseModel Get()
        {
            ResponseModel responseModel;
            try
            {
                var semesterId = _semesterRepository.GetOnlineAdmSemester();
                string msg;
                bool activated;
                if (semesterId == null)
                {
                    activated = false;
                    msg = "Online Admission Will Start Soon.";

                }
                else
                {
                    activated = true;
                    msg = "Online Admission is currently activated.";
                }
                responseModel = new ResponseModel(message: msg, data: activated);
            }
            catch (Exception ex)
            {

                responseModel = new ResponseModel(isSuccess: true, message: "Error", exception: ex);
            }
            return responseModel;
        }
    }
}
