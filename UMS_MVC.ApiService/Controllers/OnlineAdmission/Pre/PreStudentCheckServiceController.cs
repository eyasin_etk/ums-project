﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using UMS_MVC.ApiService.Security;
using UMS_MVC.Manager.DAL.DAO.Intefaces;
using UMS_MVC.Manager.DAL.DAO.Repository;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers.OnlineAdmission.Pre
{
    public class PreStudentCheckServiceController : ApiController
    {
        private readonly IPreAdmissionStudentInfoRepository _admissionStudentInfo;

        public PreStudentCheckServiceController()
        {
            _admissionStudentInfo=new PreAdmissionStudentInfoRepository();
        }
        public async Task<ResponseModel> Get(string phoneNo)
        {
            ResponseModel responseModel;
            try
            {
                var checkDuplicate =await _admissionStudentInfo.CheckDuplicateByPhone(phoneNo);
                responseModel=new ResponseModel(data:checkDuplicate);
            }
            catch (Exception e)
            {
                responseModel=new ResponseModel(isSuccess:false, exception:e);
            }
            return responseModel;
           
        }
    }
}
