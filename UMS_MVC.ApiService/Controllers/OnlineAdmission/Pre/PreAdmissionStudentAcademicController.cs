﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using UMS_MVC.ApiService.Security;
using UMS_MVC.DataModel.Models.PreAdmission;
using UMS_MVC.Manager.DAL.DAO.Intefaces;
using UMS_MVC.Manager.DAL.DAO.Repository;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers.OnlineAdmission.Pre
{
    
    public class PreAdmissionStudentAcademicController : ApiController
    {
        private readonly IPreAdmissionAcademicRepository _preAdmissionAcademic;

        public PreAdmissionStudentAcademicController()
        {
            _preAdmissionAcademic = new PreAdmissionAcademicRepository();
        }
        // [HttpGet]
        //[ActionName("FindAcademicById")]
        // public async Task<ResponseModel> FindAcademicById(int acaId, bool isQuery = false)
        // {
        //     ResponseModel responseModel;

        //     try
        //     {
        //         var asyncQuery = await _preAdmissionAcademic.FindSingleAsync(acaId);
        //         responseModel = asyncQuery != null ? new ResponseModel(asyncQuery) : new ResponseModel(isSuccess: false, message: "No Data found");
        //     }
        //     catch (Exception es)
        //     {
        //         responseModel = new ResponseModel(exception: es, message: "Error", isSuccess: false);
        //     }
        //     return responseModel;

        // }
        [HttpGet]
        public async Task<ResponseModel> Get(Guid? id, int academicId, bool fullType = false)
        {
            ResponseModel responseModel = null;
            try
            {
                switch (fullType)
                {
                    case true:
                        List<PreAdmissionAcademic> studentAcademic = await _preAdmissionAcademic.GetAll().Where(s => s.PreAdmissionStudentInfoId == id).AsNoTracking().ToListAsync();
                        responseModel = studentAcademic.Any() ? new ResponseModel(studentAcademic) : new ResponseModel(isSuccess: false, message: "No Data Found");
                        break;
                    case false:
                        var asyncQuery = await _preAdmissionAcademic.FindSingleAsync(academicId);
                        responseModel = asyncQuery != null ? new ResponseModel(asyncQuery) : new ResponseModel(isSuccess: false, message: "No Data found");
                        break;
                }
            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(isSuccess: false, message: "Error!", exception: es);
            }
            return responseModel;
        }
        //public ResponseModel Get(int id)
        //{
        //    ResponseModel responseModel;
        //    try
        //    {
        //        var findInfo =  _preAdmissionAcademic.FindSingle(id);
        //        responseModel = findInfo!=null ? new ResponseModel(findInfo) : new ResponseModel(isSuccess: false, message: "No Data Found");
        //    }
        //    catch (Exception es)
        //    {
        //        responseModel = new ResponseModel(isSuccess: false, message: "Error!", exception: es);
        //    }
        //    return responseModel;
        //}

        public async Task<ResponseModel> Post(PreAdmissionAcademic academic)
        {
            ResponseModel responseModel;
            try
            {
                var nameOfExamination = academic.NameOfExamination;
                var preAdmissionStudentInfoId = academic.PreAdmissionStudentInfoId;
                var checkDuplicate = await _preAdmissionAcademic.CheckDuplicateExamination(preAdmissionStudentInfoId, nameOfExamination);
                var countAcaInfo = await _preAdmissionAcademic.CountAcademicInfoes(preAdmissionStudentInfoId);
                if (ModelState.IsValid)
                {
                    if (academic.PreAdmissionAcademicId != 0)
                    {
                        _preAdmissionAcademic.Update(academic);
                      responseModel=new ResponseModel(message:  nameOfExamination + " data saved.");
                    }
                    else
                    {
                        if (!checkDuplicate)
                        {
                            responseModel = new ResponseModel(isSuccess: false, message: "! " + nameOfExamination + " data already saved. Try Another.");
                        }
                        else if (countAcaInfo >= 3)
                        {
                            responseModel = new ResponseModel(isSuccess: false, message: "You can insert maximum 3 individual data.");
                        }
                        else
                        {
                            _preAdmissionAcademic.Add(academic);
                            responseModel = new ResponseModel(message: nameOfExamination + " data saved.");
                        }
                     }
                    _preAdmissionAcademic.Save();
                  }
                else
                {
                    var custMsg = ModelState.SelectMany(state => state.Value.Errors)
                        .Aggregate(string.Empty, (current, error) => current + (error.ErrorMessage + ','));
                    responseModel = new ResponseModel(isSuccess: false, message: custMsg);
                }
            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(isSuccess: false, message: "Error!", exception: es);
            }
            return responseModel;
        }

        public ResponseModel Delete(int id)
        {
            ResponseModel responseModel;
            try
            {
                PreAdmissionAcademic findSingle = _preAdmissionAcademic.FindSingle(id);
                _preAdmissionAcademic.Delete(findSingle);
                _preAdmissionAcademic.Save();
                responseModel = new ResponseModel();
            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(isSuccess: false, message: "Error !", exception: es);
            }
            return responseModel;
        }
    }
}
