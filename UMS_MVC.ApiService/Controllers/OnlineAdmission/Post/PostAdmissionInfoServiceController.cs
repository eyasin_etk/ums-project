﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Newtonsoft.Json;
using UMS_MVC.ApiService.Security;
using UMS_MVC.DataModel.Models.PreAdmission;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.Manager.DAL.DAO.Intefaces;
using UMS_MVC.Manager.DAL.DAO.Repository;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers.OnlineAdmission.Post
{
    //[AntiForgeryValidate]
    public class PostAdmissionInfoServiceController : ApiController
    {
        private readonly IPreAdmissionStudentInfoRepository _admissionStudentInfoRepository;
        private readonly OnlineAdmissionManager _admissionManager;

        public PostAdmissionInfoServiceController()
        {
            _admissionStudentInfoRepository = new PreAdmissionStudentInfoRepository();
            _admissionManager = new OnlineAdmissionManager();
        }

        public async Task<ResponseModel> Get( string name, string tokenId,int departmentId,bool admitted, string phone, int gender, string sorting)

        {
            ResponseModel responseModel;

            try
            {
                var sortObj = JsonConvert.DeserializeObject<PaginationViewModel>(sorting);
                IQueryable<PreAdmissionStudentInfo> infos = _admissionStudentInfoRepository.GetAll().AsNoTracking().OrderByDescending(s=>s.EntryTime);
                //Other Filter's 
                var asyncQuery = infos
                    .Select(s => new
                    {
                        s.StudentName,
                        s.PreAdmissionStudentInfoId,
                        s.StudentImage,
                        s.PersonalContactNo,
                        s.EntryTime,
                        s.EnrolledDepartmentId,
                        s.AdmissionOfficeConfirmed,
                        s.BarCodeSerial
                    });

                // Filter--------
                asyncQuery = asyncQuery.Where(s => s.AdmissionOfficeConfirmed==admitted);
                if (!string.IsNullOrEmpty(name))
                {
                    asyncQuery = asyncQuery.Where(s => s.StudentName.Contains(name));
                }
                if (!string.IsNullOrEmpty(tokenId))
                {
                   asyncQuery = asyncQuery.Where(s =>s.BarCodeSerial == tokenId);
                }
                if (!string.IsNullOrEmpty(phone))
                {
                    asyncQuery = asyncQuery.Where(s => s.PersonalContactNo.Contains(phone));
                }
                if (departmentId > 0)
                {
                    asyncQuery = asyncQuery.Where(s => s.EnrolledDepartmentId == departmentId);
                }
                
                //if (!string.IsNullOrEmpty(sortDate))
                //{
                //    DateTime tempDate;
                //    DateTime.TryParse(sortDate, out tempDate);
                //    asyncQuery = asyncQuery.Where(s => s.EntryTime == tempDate);
                //}
                if (gender > 0)
                {

                }
                // Filter--------

                var totalCount = asyncQuery.Count();
                var totalPages = (int)Math.Ceiling((double)totalCount / sortObj.PageSize);
                var pageData = new PaginationViewModel
                {
                    PageNo = sortObj.PageNo,
                    PageSize = sortObj.PageSize,
                    TotalCount = totalCount,
                    TotalPages = totalPages
                };
                var results = await asyncQuery
                    .Skip(sortObj.PageSize * sortObj.PageNo)
                    .Take(sortObj.PageSize)
                    .ToListAsync();
                responseModel = new ResponseModel(results, pageData);
            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(exception: es, message: "Error", isSuccess: false);
            }
            return responseModel;

        }

        public async Task<ResponseModel> Get(Guid id)
        {
            ResponseModel responseModel;
            var asyncQuery = await _admissionStudentInfoRepository.FindSingleInfoGuidAsync(id);
            try
            {
                responseModel = asyncQuery != null ? new ResponseModel(asyncQuery) : new ResponseModel(isSuccess: false, message: "No Data Found.");
            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(exception: es, message: "Error", isSuccess: false);
            }
            return responseModel;
        }

        public ResponseModel Post(PreAdmissionStudentInfo studentInfo)
        {
            ResponseModel responseModel;
            //var asyncQuery =  _admissionStudentInfoRepository.FinSingleGuid(id);
            try
            {
                var addedStudentObj = _admissionManager.InsertUpdate(studentInfo);
                responseModel = new ResponseModel(addedStudentObj);
            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(exception: es, message: "Error", isSuccess: false);
            }
            return responseModel;
        }
        public ResponseModel Delete(Guid id)
        {
            ResponseModel responseModel;
            try
            {
                _admissionManager.Delete(id);
                responseModel = new ResponseModel();
            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(isSuccess: false, message: "Error !", exception: es);
            }
            return responseModel;
        }
    }
}
