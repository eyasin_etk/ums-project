﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using UMS_MVC.Manager.DAL.DAO.Intefaces;
using UMS_MVC.Manager.DAL.DAO.Repository;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers.OnlineAdmission.Post
{
    public class PostAdmNameLoaderController : ApiController
    {
        private readonly IPreAdmissionStudentInfoRepository _infoRepository;

        public PostAdmNameLoaderController()
        {
            _infoRepository = new PreAdmissionStudentInfoRepository();
        }

        public async Task<ResponseModel> Get(string nameParam)
        {
           ResponseModel responseModel;
            try
            {
                if (!string.IsNullOrEmpty(nameParam))
                {
                    var query =await _infoRepository.GetAll().Where(s => s.StudentName.Contains(nameParam)).Select(s => new
                    {
                        s.StudentName
                    }).ToListAsync();
                    responseModel = new ResponseModel(query);
                }
                else
                {
                    responseModel=new ResponseModel(isSuccess:false, message:"No name");
                }
            }
            catch (Exception e)
            {
                responseModel=new ResponseModel(isSuccess:false,exception:e,message:"ERROR!");
            }
           return responseModel;

        }

    }
}
