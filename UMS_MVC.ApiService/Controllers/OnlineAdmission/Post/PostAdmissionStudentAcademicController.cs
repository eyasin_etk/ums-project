﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using UMS_MVC.ApiService.Security;
using UMS_MVC.DataModel.Models.PreAdmission;
using UMS_MVC.Manager.DAL.DAO.Intefaces;
using UMS_MVC.Manager.DAL.DAO.Repository;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers.OnlineAdmission.Post
{
    
    public class PostAdmissionStudentAcademicController : ApiController
    {
        private readonly IPreAdmissionAcademicRepository _preAdmissionAcademic;

        public PostAdmissionStudentAcademicController()
        {
            _preAdmissionAcademic = new PreAdmissionAcademicRepository();
        }

        //public ResponseModel Get()
        //{
        //    ResponseModel responseModel;
        //    try
        //    {
        //        List<PreAdmissionAcademic> studentAcademic = _preAdmissionAcademic.GetAll().ToList();
        //        responseModel = studentAcademic.Any() ? new ResponseModel(studentAcademic) : new ResponseModel(isSuccess: false, message: "No Data Found");
        //    }
        //    catch (Exception es)
        //    {
        //        responseModel = new ResponseModel(isSuccess: false, message: "Error!", exception: es);
        //    }
        //    return responseModel;
        //}
        public ResponseModel Get(Guid? id, int acaId, bool type=false)
        {
            ResponseModel responseModel = null;
            try
            {
                switch (type)
                {
                    case true:
                        List<PreAdmissionAcademic> studentAcademic = _preAdmissionAcademic.GetAll().Where(s => s.PreAdmissionStudentInfoId == id).ToList();
                        responseModel = studentAcademic.Any() ? new ResponseModel(studentAcademic) : new ResponseModel(isSuccess: false, message: "No Data Found");
                        break;
                    case false:
                        var studentAcademicSingle = _preAdmissionAcademic.FindSingle(acaId);
                        responseModel = new ResponseModel(studentAcademicSingle);
                        break;
                }
            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(isSuccess: false, message: "Error!", exception: es);
            }
            return responseModel;
        }

        public ResponseModel Post(PreAdmissionAcademic academic)
        {
            ResponseModel responseModel;
            try
            {
                if (academic.PreAdmissionAcademicId != 0)
                {
                    _preAdmissionAcademic.Update(academic);
                    _preAdmissionAcademic.Save();
                }
                else
                {
                    _preAdmissionAcademic.Add(academic);
                }

                _preAdmissionAcademic.Save();
                responseModel = new ResponseModel();

            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(isSuccess: false, message: "Error!", exception: es);
            }
            return responseModel;
        }

        public ResponseModel Delete(int id)
        {
            ResponseModel responseModel;
            try
            {
                PreAdmissionAcademic findSingle = _preAdmissionAcademic.FindSingle(id);
                _preAdmissionAcademic.Delete(findSingle);
                _preAdmissionAcademic.Save();
                responseModel = new ResponseModel();
            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(isSuccess: false, message: "Error !", exception: es);
            }
            return responseModel;
        }
    }
}
