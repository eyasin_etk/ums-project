﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using UMS_MVC.ApiService.Security;
using UMS_MVC.DataModel.Models.CreditTransfer;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.Manager.DAL.DAO.Intefaces;
using UMS_MVC.Manager.DAL.DAO.Repository;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers
{
    
    public class CreditTransStudentServiceController : ApiController
    {
        private readonly ICreditTrasnsferStudentRepository _trasnsferStudent;
        private readonly StudentManager _studentManager;
        private readonly AccountManager _accountManager;

        public CreditTransStudentServiceController()
        {
            _trasnsferStudent = new CreditTrasnsferStudentRepository();
            _studentManager = new StudentManager();
            _accountManager = new AccountManager();
        }
        public ResponseModel Get()
        {
            ResponseModel responseModel;
            try
            {
                IQueryable<CreditTransferStudent> creditTransferStudents = _trasnsferStudent.GetAll();
                var getAllProbationCategory = from trs in
                    creditTransferStudents
                                              select new CreditTransferStudentViewModel
                                              {
                                                  StudentIdentificationId = trs.StudentIdentificationId,
                                                  StudentName =
                                                      _studentManager.GetAllStudentIdentificationsFiltered()
                                                          .Include(s => s.StudentInfo)
                                                          .FirstOrDefault(s => s.StudentIdentificationId == trs.StudentIdentificationId)
                                                          .StudentInfo.StudentName,
                                                  CreditAccepted = trs.CreditAccepted,
                                                  CreditTransferStudentsId = trs.CreditTransferStudentId,
                                                  InstituteAdmittedProgram = trs.InstituteAdmittedProgram,
                                                  InstituteCreditCompleted = trs.InstituteCreditCompleted,
                                                  InstituteTotalCredit = trs.InstituteTotalCredit,
                                                  PreviousInstituteName = trs.PreviousInstituteName,
                                                  YearAdmitted = trs.YearAdmitted
                                              };
                //select new 
                responseModel = new ResponseModel(getAllProbationCategory.ToList());
            }
            catch (Exception e)
            {
                responseModel = new ResponseModel(exception: e, message: "Error", isSuccess: false);
            }
            return responseModel;
        }

    }
}
