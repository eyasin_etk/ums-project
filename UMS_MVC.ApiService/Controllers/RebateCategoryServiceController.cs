﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers
{
    public class RebateCategoryServiceController : ApiController
    {
        private readonly RebateCategoryManager _rebateCategoryManager;

        public RebateCategoryServiceController()
        {
            _rebateCategoryManager = new RebateCategoryManager();
        }

        [HttpGet]
        public ResponseModel Get()
        {
            ResponseModel responseModel = new ResponseModel();

            try
            {
                //if (id > 0)
                //{
                //    var data = _rebateCategoryManager.GetSingle(id);
                //    RebateCategoryViewModel model = new RebateCategoryViewModel()
                //    {
                //        RebateCategoryId = data.RebateCategoryId,
                //        RebateName = data.RebateName,
                //        RebatePercent = data.RebatePercent
                //    };


                //    responseModel = model != null ? new ResponseModel(model) : new ResponseModel(isSuccess: false, message: "No Data Found");
                //}
                //else
                //{
                    List<RebateCategoryViewModel> rebateCategoryList = _rebateCategoryManager.GetAll()

                                   .Select(x => new RebateCategoryViewModel()
                                   {
                                       RebateCategoryId = x.RebateCategoryId,
                                       RebateName = x.RebateName,
                                       RebatePercent = x.RebatePercent
                                   }).ToList();

                    responseModel = rebateCategoryList.Any() ? new ResponseModel(rebateCategoryList) : new ResponseModel(isSuccess: true, message: "No Data Found");
                //}

            }
            catch (Exception ex)
            {

                responseModel = new ResponseModel(isSuccess: false, message: "Error!", exception: ex);
            }

            return responseModel;
        }
    }
}
