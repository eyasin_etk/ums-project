﻿using System;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Web.Http;
using UMS_MVC.ApiService.Security;
using UMS_MVC.DataModel.Models;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.BLL;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers
{
    
    public class NoteUploadServiceController : ApiController
    {
        private readonly NoteUploadManager _noteUploadLogicManager;

        public NoteUploadServiceController()
        {
            _noteUploadLogicManager=new NoteUploadManager();
        }

        [HttpGet]
        
        [ActionName("NotesBySection")]
        public ResponseModel NotesBySection(int id)
        {
            ResponseModel responseModel;
            var getAllNotesBySecId = _noteUploadLogicManager.GetAllNoteUploadBySection(id);
            var specifiedNotes = getAllNotesBySecId.Select(s => new NoteUpload
            {
                NoteUploadId = s.NoteUploadId,
                SectionId = s.SectionId,
                AccountId = s.AccountId,
                Description = s.Description,
                ExpireDate = s.ExpireDate,
                FileName = s.FileName,
                UploadDate = s.UploadDate,
                Status = s.Status
            }).ToList();
           
            if (getAllNotesBySecId != null)
            {
                responseModel = new ResponseModel(specifiedNotes);
            }
            else
            {
                responseModel = new ResponseModel(isSuccess: false, message: "Nothing uploaded yet!");
            }
            return responseModel;
        }
        
        public ResponseModel Delete(int id)
        {
            ResponseModel responseModel ;
            string msg = "Removed";
            if (id > 0)
            {
                try
                {
                    NoteUpload getFileInfo = _noteUploadLogicManager.FindSingleNote(id);
                    string physicalPath2 = FileCheckingManager.FileUploadLocation() + getFileInfo.FileName;
                   
                    if (File.Exists(physicalPath2))
                    {
                        File.Delete(physicalPath2);
                        msg += " file";

                    }
                    if (getFileInfo.NoteUploadId != 0)
                    {
                        _noteUploadLogicManager.Delete(id);
                        _noteUploadLogicManager.Save();
                        msg += " ,server entry sussessfully.";
                    }
                    responseModel = new ResponseModel(message: msg);
                }
                catch (Exception e)
                {
                    responseModel=new ResponseModel(isSuccess:false, message:"Internal error, file could not be removed.",exception:e);
                }
                
            }
            else
            {
                responseModel=new ResponseModel(isSuccess:false,message:"No file selected.");
            }
            return responseModel;
        }
    }
}
