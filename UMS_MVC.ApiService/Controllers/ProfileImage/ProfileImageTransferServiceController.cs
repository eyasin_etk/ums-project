﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using UMS_MVC.Manager.DAL.BLL;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers.ProfileImage
{
    public class ProfileImageTransferServiceController : ApiController
    {
        public HttpResponseMessage Get(string imageName,int type=0, int stamp = 0)

        {
            var root =  FileCheckingManager.OnlineAdmissionFileLocation(2);
            HttpResponseMessage result;
            ResponseModel responseModel = null;
            switch (type)
            {
                case 0:
                    root = FileCheckingManager.AccountImageFileLocation() + imageName;
                   
                    break;
                case 1:
                    root = FileCheckingManager.UmsImageLocation() + imageName;
                    break;
                case 2:
                    root = FileCheckingManager.OnlineAdmissionFileLocation() + imageName;
                    break;

                case 3:
                    //imageName = imageName.Remove(imageName.LastIndexOf("."));
                    imageName = imageName + ".pdf";
                    root = FileCheckingManager.UmsFileLocation() + imageName;
                    break;
            }
            

            try
            {
                if (!File.Exists(root))
                {
                    root = FileCheckingManager.OnlineAdmissionFileLocation(2);
                }

                //if (!File.Exists(root) && type == 3)
                //{
                //   // root = "no";
                //    responseModel = new ResponseModel(isSuccess: false, message: "No File to upload");
                //    return responseModel;
                //}

                result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(new FileStream(root, FileMode.Open, FileAccess.Read));
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = imageName;
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
            }
            catch (Exception e)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError);
            }

            return result;
        }



        [Route("api/ProfileImageTransferService/check")]
        [HttpGet]
        public HttpResponseMessage check(string imageName)

        {
            HttpResponseMessage result= Request.CreateResponse(HttpStatusCode.OK, new { Message = "no" });
            var root="";

               
                    imageName = imageName.Remove(imageName.LastIndexOf("."));
                    imageName = imageName + ".pdf";
                    root = FileCheckingManager.UmsFileLocation() + imageName;
                   
            


          
                if(!File.Exists(root))
                {
                //root = FileCheckingManager.OnlineAdmissionFileLocation(2);
                        
                    result=Request.CreateResponse(HttpStatusCode.OK,new {Message="ok" });
                    return result;
                 }

            //if (!File.Exists(root) && type == 3)
            //{
            //   // root = "no";
            //    responseModel = new ResponseModel(isSuccess: false, message: "No File to upload");
            //    return responseModel;
            //}




            // result = Request.CreateResponse(HttpStatusCode.InternalServerError);
            return result;

        }










    }
}
