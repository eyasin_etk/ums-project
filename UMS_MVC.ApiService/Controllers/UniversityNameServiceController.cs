﻿using System;
using System.Linq;
using System.Web.Http;
using UMS_MVC.ApiService.Security;
using UMS_MVC.DataModel.Models.UniversalTypes;
using UMS_MVC.Manager.DAL.DAO.Intefaces;
using UMS_MVC.Manager.DAL.DAO.Repository;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers
{
    
    public class UniversityNameServiceController : ApiController
    {
        private readonly IUniversityNamesRepository _universityNames;

        public UniversityNameServiceController()
        {
            _universityNames = new UniversityNamesRepository();
        }

        public ResponseModel Get()
        {
            ResponseModel responseModel;
            try
            {
                var getAllUniNames = _universityNames.GetAll();
                responseModel = new ResponseModel(getAllUniNames.ToList());
            }
            catch (Exception e)
            {
                responseModel = new ResponseModel(exception: e, message: "Error", isSuccess: false);
            }
            return responseModel;
        }
        public ResponseModel Get(int id)
        {
            ResponseModel responseModel;
            try
            {
                var findUniName = _universityNames.FindSingle(id);
                responseModel = new ResponseModel(findUniName);
            }
            catch (Exception e)
            {
                responseModel = new ResponseModel(exception: e, message: "Error", isSuccess: false);
            }
            return responseModel;
        }
        public ResponseModel Post(UniversityName uniName)
        {
            ResponseModel responseModel;
            try
            {
                UniversityName aUniName;
                if (uniName.UniversityNameId != 0)
                {
                    aUniName = _universityNames.FindSingle(uniName.UniversityNameId);
                    aUniName.FirstPhoneNo = uniName.FirstPhoneNo;
                    aUniName.InstituteName = uniName.InstituteName;
                    aUniName.Location = uniName.Location;
                    aUniName.Remarks = uniName.Remarks;
                    aUniName.SecondPhoneNo = uniName.SecondPhoneNo;
                    aUniName.ShortName = uniName.ShortName;
                    aUniName.GoogleMapUrl = uniName.GoogleMapUrl;
                    _universityNames.Update(aUniName);
                }
                else
                {
                    aUniName = uniName;
                    _universityNames.Add(aUniName);
                }
                _universityNames.Save();
                responseModel = new ResponseModel(aUniName.UniversityNameId);
            }
            catch (Exception e)
            {
                responseModel = new ResponseModel(exception: e, message: "Error", isSuccess: false);
            }
            return responseModel;
        }
        public ResponseModel Delete(int id)
        {
            ResponseModel responseModel;
            try
            {
                var dbObj = _universityNames.FindSingle(id);
                if (dbObj != null)
                {
                    _universityNames.Delete(dbObj);
                    _universityNames.Save();
                    responseModel = new ResponseModel(id);
                }
                else
                {
                    responseModel = new ResponseModel(isSuccess: false, message: "Cannot be removed");
                }
            }
            catch (Exception exception)
            {
                responseModel = new ResponseModel { IsSuccess = false, Message = "Exception Found", Exception = exception };
            }
            return responseModel;
        }
    }
}
