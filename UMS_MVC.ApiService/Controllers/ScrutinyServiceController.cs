﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using UMS_MVC.ApiService.Security;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.ViewModels.ViewModels;
using UMS_MVC.ViewModels.ViewModels.Scrutiny;

namespace UMS_MVC.ApiService.Controllers
{
    
    public class ScrutinyServiceController : ApiController
    {
        private readonly SectionManager _sectionLogicManager;
        private readonly CourseForStudentManger _courseForStudentManger;


        public ScrutinyServiceController()
        {
            _sectionLogicManager = new SectionManager();
            _courseForStudentManger = new CourseForStudentManger();
        }

        [HttpGet]
        [ActionName("AllStudentsBySection")]
        public ResponseModel AllStudentsBySection(int sectionId)
        {
            ResponseModel responseModel;
            try
            {
                if (sectionId > 0)
                {
                    var scrutinyObj = new ScrutinyViewModel
                    {
                        Section = new ScrutintSectionViewModel(),
                        Students = new List<StudentsScrutinyInfoViewModel>()
                    };
                    var sectionObj =
                        _sectionLogicManager.GetAllSectionsFiltered()
                            .Include(s => s.Teacher.Account)
                            .Include(s => s.CourseForDepartment)
                            .FirstOrDefault(s => s.SectionId == sectionId);
                    var scrutinyStudentObj =
                        (from adv in
                            _courseForStudentManger.GetAllCourseForStudentsAcademicsBysection(sectionId).AsEnumerable()
                         select new StudentsScrutinyInfoViewModel
                         {
                             CourseForStudentAcademicId = adv.CourseForStudentsAcademicId,
                             StudentId = adv.StudentIdentification.StudentId,
                             StudentName = adv.StudentIdentification.StudentInfo.StudentName,
                             SectionId = sectionId,
                             Lvl1Stat = adv.FirstScrutinized,
                             Lvl2Stat = adv.SecondScrutinized,
                             ScrutinyBy = adv.ScrutinizedBy,
                         }).ToList();

                    if (sectionObj != null)
                    {
                        scrutinyObj.Section = new ScrutintSectionViewModel
                        {
                            SectionId = sectionObj.SectionId,
                            AssignedTeacher = sectionObj.Teacher.Account.Name,
                            CourseName = sectionObj.CourseForDepartment.CourseName,
                            CourseCode = sectionObj.CourseForDepartment.CourseCode,
                            CourseCredit = sectionObj.CourseForDepartment.Credit
                        };
                    }
                    scrutinyObj.Students = scrutinyStudentObj;
                    responseModel = new ResponseModel(scrutinyObj);
                    return responseModel;
                }
                responseModel = new ResponseModel(isSuccess: false, message: "No data found.");
            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(isSuccess: false, message: "Error.", exception: es);
            }
            return responseModel;
        }

        [HttpPost]
        [ActionName("OverAllScrutinySubmit")]
        public ResponseModel OverAllScrutinySubmit(int sectionId, int a)
        {
            ResponseModel responseModel;
            if (sectionId > 0)
            {
                var getQuery =
                     _courseForStudentManger.GetAllCourseForStudentFiltered().Where(s => s.SectionId == sectionId);
                try
                {
                    foreach (var i in getQuery.ToList())
                    {
                        switch (a)
                        {
                            case 1:
                                i.FirstScrutinized = true;
                                break;
                            case 2:
                                i.SecondScrutinized = true;
                                break;
                            case 3:
                                i.FirstScrutinized = false;
                                break;
                            case 4:
                                i.SecondScrutinized = false;
                                break;
                        }
                        _courseForStudentManger.UpdateCourseForStudentAcademicSecond(i);
                    }
                    _courseForStudentManger.Commit();
                    responseModel = new ResponseModel();
                }
                catch (Exception es)
                {
                    responseModel = new ResponseModel(exception: es, message: "Error", isSuccess: false);
                }
            }
            else
            {
                responseModel = new ResponseModel(isSuccess: false, message: "No data found.");
            }
            return responseModel;
        }

        [HttpPost]
        [ActionName("SingleScruSubmit")]
        public ResponseModel SingleScruSubmit(int studentAcaId, int sectionId, int e)
        {
            ResponseModel responseModel;
            try
            {
                CourseForStudentsAcademic studentAcademic = _courseForStudentManger.GetSingleCourseForStudentAcademic(studentAcaId);
                if (studentAcaId > 0 && studentAcademic != null)
                {
                    switch (e)
                    {
                        case 1:
                            studentAcademic.FirstScrutinized = true;
                            break;
                        case 2:
                            studentAcademic.SecondScrutinized = true;
                            break;

                    }
                    _courseForStudentManger.UpdateCourseForStudentAcademicSecond(studentAcademic);
                    _courseForStudentManger.Commit();
                    responseModel = new ResponseModel();
                }
                else
                {
                    responseModel = new ResponseModel(isSuccess: false, message: "No data found.");
                }
            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(isSuccess: false, message: "Error !", exception: es);
            }
            return responseModel;
        }
    }


}
