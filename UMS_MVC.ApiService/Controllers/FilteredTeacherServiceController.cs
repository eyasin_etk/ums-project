﻿using System;
using System.Linq;
using System.Web.Http;
using UMS_MVC.ApiService.Security;
using UMS_MVC.Manager.DAL.BLL;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers
{
    public class FilteredTeacherServiceController : ApiController
    {
        private readonly TeacherManager _teacherManager;
        private readonly SectionBusinessLogic _sectionBusinessLogic;
        private readonly CourseForStudentManger _courseForStudentManger;
        public FilteredTeacherServiceController()
        {
            _teacherManager=new TeacherManager();
            _sectionBusinessLogic=new SectionBusinessLogic();
            _courseForStudentManger=new CourseForStudentManger();

        }
        public ResponseModel Get()
       {
            ResponseModel responseModel;

            try
            {
                var filteredTeachers = _sectionBusinessLogic.FilteredTeachersSecind();
                responseModel = filteredTeachers.Any() ? new ResponseModel(filteredTeachers.ToList()) : new ResponseModel(isSuccess:false, message:"No Data");
            }
            catch (Exception es)
            {
                responseModel= new ResponseModel(isSuccess: false, message: "Error", exception:es);
            }
            return responseModel;
        }
      
    }
}
