﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers
{
    public class CommonServiceController : ApiController
    {

        private readonly CommonAttributeManager _commonAttributeManager;

        public CommonServiceController()
        {
            _commonAttributeManager = new CommonAttributeManager();
        }

        public async Task<ResponseModel> Get(int id = 1)
        {
            ResponseModel responseModel;
            try
            {
                switch (id)
                {
                    case 2:
                        var allBloodGroupsesFiltered =await _commonAttributeManager.GetAllBloodGroupsesFiltered();
                        responseModel = new ResponseModel(allBloodGroupsesFiltered);
                        break;
                    case 3:
                        var allMaritalStatusesFiltered =await _commonAttributeManager.GetAllMaritalStatusesFiltered();
                        responseModel = new ResponseModel(allMaritalStatusesFiltered);
                        break;
                    default:
                        var allGenderFiltered =await _commonAttributeManager.GetAllGenderFiltered();
                        responseModel = new ResponseModel(allGenderFiltered);
                        break;
                }
            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(isSuccess: false, message: "Error", exception: es);
            }
            return responseModel;

        }

       
    }
}
