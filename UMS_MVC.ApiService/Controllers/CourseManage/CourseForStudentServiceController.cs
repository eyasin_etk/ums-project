﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using UMS_MVC.ApiService.Security;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers.CourseManage
{
    //[AntiForgeryValidate]
    public class CourseForStudentServiceController : ApiController
    {
        private readonly CourseForStudentUpdatedManager _courseForStudentManger;

        public CourseForStudentServiceController()
        {
            _courseForStudentManger = new CourseForStudentUpdatedManager();
        }
        public async Task<ResponseModel> Get(int id)
        {
            ResponseModel responseModel;
            try
            {
                var courseForStudentsAcademics =await 
                (from course in
                    _courseForStudentManger.GetAllCourseByStudentId(id)
                        .Include(s => s.CourseForDepartment)
                        .Include(s => s.CourseStatus).Include(s=>s.Semester)
                        .AsNoTracking()
                    select new
                    {
                        course.CourseForDepartment.CourseCode,
                        course.CourseForDepartment.CourseName,
                        course.CourseForDepartment.Credit,
                        course.Attendance,
                        course.ClassTest,
                        course.Midterm,
                        course.FinalTerm,
                        course.CourseStatusId,
                        course.CourseStatus.ShortName,
                        course.Grade,
                        course.TotalGrade,
                        course.TotalMark,
                        course.LetterGrade,
                        course.SemesterId,
                        course.Semester.SemesterNYear
                    }).ToListAsync();
                responseModel = new ResponseModel(data: courseForStudentsAcademics);

            }
            catch (Exception exception)
            {
                responseModel = new ResponseModel(isSuccess: false, message: "Error", exception: exception);
            }
            return responseModel;
        }

        public ResponseModel Post(CourseForStudentsAcademic courseForStudentsAcademic)
        {
            ResponseModel responseModel;
            try
            {
                int advisingId = 0;
                if (courseForStudentsAcademic.CourseForStudentsAcademicId != 0)
                {
                    _courseForStudentManger.Update(courseForStudentsAcademic);
                    _courseForStudentManger.Save();
                }
                else
                {
                    advisingId = _courseForStudentManger.InsertToGetId(courseForStudentsAcademic);
                }
               
                responseModel=new ResponseModel(advisingId);
            }
            catch (Exception exception)
            {
                responseModel=new ResponseModel(isSuccess:false,message:"Error", exception:exception);
            }
            return responseModel;
        }

        public ResponseModel Delete(int id)
        {
            ResponseModel responseModel;
            try
            {
                _courseForStudentManger.Delete(id);
                _courseForStudentManger.Save();
                responseModel=new ResponseModel();
            }
            catch (Exception exception)
            {
                responseModel=new ResponseModel(isSuccess:false,exception:exception);
            }
            return responseModel;

        }
    }
}
