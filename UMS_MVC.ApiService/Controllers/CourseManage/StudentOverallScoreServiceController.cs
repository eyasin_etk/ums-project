﻿using System;
using System.Web.Http;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers.CourseManage
{
    public class StudentOverallScoreServiceController : ApiController
    {
        private readonly GradeManager _gradeManager;

        public StudentOverallScoreServiceController()
        {
            _gradeManager=new GradeManager();
        }

        public ResponseModel Get(int id)
        {
            ResponseModel responseModel;
            try
            {
                var cumulativeScore = _gradeManager.CalculateCgpaofAStudent(id);
                responseModel = new ResponseModel(cumulativeScore);
            }
            catch (Exception ex)
            {

                responseModel = new ResponseModel(isSuccess: false, message: "Error", exception: ex);
            }
            return responseModel;
        }


    }
}
