﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO.AdmissionManager;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers.AdmissionApiController
{
    public class AcademicInfoServiceController : ApiController
    {
        private readonly AcademicInfoManager _academicInfoManager;
        public AcademicInfoServiceController()
        {
            _academicInfoManager = new AcademicInfoManager();
        }

        public ResponseModel Get(string id, int academicId, bool fullType)
        {
            ResponseModel responseModel = null;

            try
            {
                switch (fullType)
                {
                    case true:
                        List<StudentAcademicInfo> studentAcademic = _academicInfoManager.GetAll().Where(s => s.StudentId == id).ToList();

                        responseModel = studentAcademic.Any() ? new ResponseModel(studentAcademic) : new ResponseModel(isSuccess: true, message: "No Data Found");
                        break;
                    case false:
                        var studentAcademicSpecific = _academicInfoManager.GetSingle(academicId);

                        responseModel = studentAcademicSpecific != null ? new ResponseModel(studentAcademicSpecific) : new ResponseModel(isSuccess: false, message: "No Data Found");
                        break;
                }
            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(isSuccess: false, message: "Error!", exception: es);
            }
            return responseModel;
        }

        public ResponseModel Post(StudentAcademicInfo acaInfo)
        {
            ResponseModel responseModel;
            try
            {
                string msg = "";
                if ( !string.IsNullOrEmpty(acaInfo.StudentId))
                {
                    if (acaInfo.StudentAcademicInfoId != 0)
                    {
                        _academicInfoManager.Update(acaInfo);
                        msg = "Academic Information Updated Successfully";

                    }
                    else
                    {
                        _academicInfoManager.Insert(acaInfo);
                        msg = "Academic Information Inserted Successfully";
                    }

                    _academicInfoManager.SaveData();
                }
                else
                {
                    msg = "Insert failed. Student Id not selected";
                }
                return new ResponseModel(message: msg, data: acaInfo);
            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(isSuccess: false, message: "Error !", exception: es);
            }
            return responseModel;
        }
        public ResponseModel Delete(int id)
        {
            ResponseModel responseModel;
            try
            {
                StudentAcademicInfo findSingle = _academicInfoManager.GetSingle(id);
                if (findSingle.StudentAcademicInfoId != 0)
                {
                    _academicInfoManager.Delete(findSingle.StudentAcademicInfoId);
                    _academicInfoManager.SaveData();
                }
                responseModel = new ResponseModel(message: "Academic Information Deleted");
            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(isSuccess: false, message: "Error !", exception: es);
            }
            return responseModel;
        }
    }


}