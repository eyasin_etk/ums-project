﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IAdmission;
using UMS_MVC.Manager.DAL.DAO.Repository.RAdmission;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers.AdmissionApiController
{
    public class AdmNameIdFilterController : ApiController
    {
        private readonly IPersonalInfoRepository _infoRepository;

        public AdmNameIdFilterController()
        {
            _infoRepository = new PersonalInfoRepository();
        }

        public async Task<ResponseModel> Get(string nameParam, string idParam, bool type)
        {
            ResponseModel responseModel=null;
            try
            {
                switch (type)
                {
                    case true:
                        if (!string.IsNullOrEmpty(nameParam))
                        {
                            var query1 = await _infoRepository.GetAll().Where(s => s.
                                           StudentName.Contains(nameParam)).Select(s => new
                                           {
                                              s.StudentName
                                           }).Take(100).ToListAsync();
                            responseModel = new ResponseModel(query1);
                        }
                        else
                        {
                            responseModel = new ResponseModel(isSuccess: false, message: "No Name");
                        }

                        break;
                    case false:
                        if (!string.IsNullOrEmpty(idParam))
                        {
                            var query2 = await _infoRepository.GetAll().Where(s => s.
                                         StudentId.StartsWith(idParam)).Select(s => new
                                         {
                                             s.StudentId,
                                            
                                         }).Take(100).ToListAsync();
                            responseModel = new ResponseModel(query2);
                        }
                        else
                        {
                            responseModel = new ResponseModel(isSuccess: false, message: "No ");
                        }
                        break;
                }
            }
            catch (Exception e)
            {
                responseModel = new ResponseModel(isSuccess: false, exception: e, message: "ERROR!");
            }
            return responseModel;

        }
    }
}
