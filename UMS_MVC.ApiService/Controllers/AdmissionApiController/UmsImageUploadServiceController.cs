﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;
using UMS_MVC.ApiService.Security;
using UMS_MVC.Manager.DAL.BLL;
using UMS_MVC.Manager.DAL.DAO.AdmissionManager;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IAdmission;
using UMS_MVC.Manager.DAL.DAO.Repository.RAdmission;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers.AdmissionApiController
{
    public class UmsImageUploadServiceController : ApiController
    {
        private readonly IStudentIdentityRepository _studentIdentityRepository;
        private readonly ImageUploadManager _imageUploadManager;

        public UmsImageUploadServiceController()
        {
            _studentIdentityRepository = new StudentIdentityRepository();   
            _imageUploadManager = new ImageUploadManager();
        }

        public HttpResponseMessage Get(int studentIdentity = 0, int stamp = 0)
        {
            HttpResponseMessage result;

            var localFilePath = FileCheckingManager.UmsImageLocation();
            try
            {
                if (studentIdentity <= 0)
                {
                    localFilePath = FileCheckingManager.UmsImageLocation(2);
                }
                else
                {
                    var studentInfo = _studentIdentityRepository.FindSingle(studentIdentity).StudentPicture;
                    if (!string.IsNullOrEmpty(studentInfo))
                    {
                        localFilePath += studentInfo;
                    }
                    if (!File.Exists(localFilePath))
                    {
                        localFilePath = FileCheckingManager.UmsImageLocation(2);
                    }
                }
                result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(new FileStream(localFilePath, FileMode.Open, FileAccess.Read));
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = "StudentImage";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
            }
            catch (Exception e)
            {
                result = Request.CreateResponse(HttpStatusCode.InternalServerError);
            }

            return result;
        }
      
        public async Task<ResponseModel> Post()
        {
            var httpRequest = HttpContext.Current.Request;
            ResponseModel responseModel = null;
            int fileNames = Convert.ToInt32(httpRequest.Form["newFileName"]);
            string uploadedBy = httpRequest.Form["sumittedBy"];
            try
            {
                if (fileNames>0)
                {
                    var studentId = fileNames;
                    var guidAsync = await _studentIdentityRepository.CheckStudentExitanceByIdentity(studentId);
                    if (guidAsync)
                    {

                        if (httpRequest.Files.Count > 0)
                        {
                            foreach (string file in httpRequest.Files)
                            {
                                var postedFile = httpRequest.Files[file];
                               
                                
                                var fileExte = Path.GetExtension(postedFile.FileName);
                                //  WebImage img = new WebImage(postedFile.InputStream);
                                 var img = postedFile;
                               // img.SaveAs()
                                if (FileCheckingManager.OnlineAdmFileCheck(fileExte))
                                {
                                   var imageName= _imageUploadManager.SaveStudentImage(studentId, img, fileExte);
                                    responseModel = new ResponseModel(message: "File saved sussessfully.",data: imageName);
                                }

                                else
                                {
                                    responseModel = new ResponseModel(isSuccess: false, message: "File type not supported.");
                                }
                            }
                        }
                        else
                        {
                            responseModel = new ResponseModel(isSuccess: false, message: "No File to upload");
                        }
                    }
                }

                else
                {
                    responseModel = new ResponseModel(isSuccess: false, message: "Session out. Contact to admission office.");
                }
            }
            catch (Exception e)
            {
                responseModel = new ResponseModel(isSuccess: false, message: "Error", exception: e);
            }
            return responseModel;
        }
    }
}
