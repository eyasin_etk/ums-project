﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO.AdmissionManager;
using UMS_MVC.ViewModels.ViewModels; 

namespace UMS_MVC.ApiService.Controllers.AdmissionApiController
{
    public class StudentJobInfoServiceController : ApiController
    {
        private readonly StudentJobInfoManager _studentJobInfoManger;
        public StudentJobInfoServiceController()
        {
            _studentJobInfoManger = new StudentJobInfoManager();
        }

        public ResponseModel Get(int identificationId, int id, bool fullType)
        {
            ResponseModel responseModel = null;

            try
            {
                switch (fullType)
                {
                    case true:
                        List<StudentAccountExtProfessionalActivitys> stdJobInfo = _studentJobInfoManger.GetAll().Where(s => s.AccountId == identificationId).ToList();

                        responseModel = stdJobInfo.Any() ? new ResponseModel(stdJobInfo) : new ResponseModel(isSuccess: true, message: "No Data Found");
                        break;
                    case false:
                        var studentJobSpecific = _studentJobInfoManger.GetSingle(id);

                        responseModel = studentJobSpecific != null ? new ResponseModel(studentJobSpecific) : new ResponseModel(isSuccess: false, message: "No Data Found");
                        break;
                }
            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(isSuccess: false, message: "Error!", exception: es);
            }
            return responseModel;
        }


        public ResponseModel Post(StudentAccountExtProfessionalActivitys info)
        {
            ResponseModel responseModel;
            try
            {
                string msg = "";
                if (info.AccountId > 0)
                {
                    if (info.StudentAccountExtProfessionalActivityId != 0)
                    {
                        _studentJobInfoManger.Update(info);
                        msg = "Job Information Updated Successfully";

                    }
                    else
                    {
                        _studentJobInfoManger.Insert(info);
                        msg = "Job Information Inserted Successfully";
                    }

                    _studentJobInfoManger.SaveData();
                }
                else
                {
                    msg = "Insert failed";
                }
                return new ResponseModel(message: msg, data: info);
            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(isSuccess: false, message: "Error !", exception: es);
            }
            return responseModel;
        }


        public ResponseModel Delete(int id)
        {
            ResponseModel responseModel;
            try
            {
                StudentAccountExtProfessionalActivitys findSingle = _studentJobInfoManger.GetSingle(id);
                if (findSingle.StudentAccountExtProfessionalActivityId != 0)
                {
                    _studentJobInfoManger.Delete(findSingle.StudentAccountExtProfessionalActivityId);
                    _studentJobInfoManger.SaveData();
                }
                responseModel = new ResponseModel(message: "Student Job Information Deleted");
            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(isSuccess: false, message: "Error !", exception: es);
            }
            return responseModel;
        }

    }
}
