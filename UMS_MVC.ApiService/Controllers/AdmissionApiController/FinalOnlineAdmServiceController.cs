﻿using System;
using System.Web.Http;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers.AdmissionApiController
{
    public class FinalOnlineAdmServiceController : ApiController
    {
        private readonly OnlineAdmissionManager _onlineAdmission;


        public FinalOnlineAdmServiceController()
        {
            _onlineAdmission=new OnlineAdmissionManager();
        }

        public ResponseModel Get(Guid id,int type)
        {
            ResponseModel responseModel;
            try
            {
                switch (type)
                {
                    case 1:
                        var identityViewModel = _onlineAdmission.GetStudentIdViewModel(id);
                        responseModel=new ResponseModel(identityViewModel);
                        break;
                    case 2:
                        var infoViewModel = _onlineAdmission.GetStudentInfoViewMOdel(id);
                        responseModel = new ResponseModel(infoViewModel);
                        break;
                    case 3:
                        var acaViewModel = _onlineAdmission.AdmAcademicViewModel(id);
                        responseModel = new ResponseModel(acaViewModel);
                        break;
                    default:
                        responseModel=new ResponseModel();
                        break;
                }
            }
            catch (Exception e)
            {
                responseModel=new ResponseModel(isSuccess:false, exception:e, message:"Error!");
            }
            return responseModel;
        }
    }
}
