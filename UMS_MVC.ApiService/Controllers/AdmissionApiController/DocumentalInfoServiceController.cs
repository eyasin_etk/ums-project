﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO.AdmissionManager;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IAdmission;
using UMS_MVC.Manager.DAL.DAO.Repository.RAdmission;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers.AdmissionApiController
{
    public class DocumentalInfoServiceController : ApiController
    {
        private readonly SubmittedDocumentsManager _submittedDocumentManager;
        private readonly ISubmittedDocumentRepository _submittedDocumentRepository;

        public DocumentalInfoServiceController()
        {
            _submittedDocumentManager = new SubmittedDocumentsManager();
            _submittedDocumentRepository = new SubmittedDocumentRepository();
        }


        public async Task<ResponseModel> Get(string id)
        {
            ResponseModel responseModel;
            try
            {
                if (!string.IsNullOrEmpty(id))
                {
                    var documentAdding = await _submittedDocumentRepository.GetAll().AsNoTracking().FirstOrDefaultAsync(s => s.StudentId == id);
                    responseModel = documentAdding != null ? new ResponseModel(documentAdding) : new ResponseModel(isSuccess:false,message: "No data found");
                }
                else
                {
                    responseModel = new ResponseModel(isSuccess: false, message: "No Student ID");
                }
            }
            catch (Exception e)
            {
                responseModel = new ResponseModel(isSuccess: false, message: "ERROR!", exception: e);
            }
            return responseModel;
        }


        public ResponseModel Post(DocumentAdding documentAdding)
        {
            ResponseModel responseModel;
            try
            {
                string msg = "";
                if (!string.IsNullOrEmpty(documentAdding.StudentId))
                {
                    if (documentAdding.DocumentAddingId != 0)
                    {
                        _submittedDocumentManager.Update(documentAdding);
                        msg = "Documental Information Updated Successfully";
                    }
                    else
                    {
                        _submittedDocumentManager.Insert(documentAdding);
                        msg = "Documental Information Inserted Successfully";

                    }
                    _submittedDocumentManager.SaveData();
                }
                else
                {
                    msg = "Insert failed. Student Id not selected";
                }
                return new ResponseModel(message: msg, data: documentAdding);
            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(isSuccess: false, message: "Error !", exception: es);
            }
            return responseModel;
        }
        public ResponseModel Delete(int id)
        {
            ResponseModel responseModel;
            try
            {
                DocumentAdding findSingle = _submittedDocumentManager.GetSingle(id);
                _submittedDocumentManager.Delete(findSingle.DocumentAddingId);
                _submittedDocumentManager.SaveData();
                responseModel = new ResponseModel(message: "Documental Information Deleted");
            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(isSuccess: false, message: "Error !", exception: es);
            }
            return responseModel;
        }
    }
}