﻿using System;
using System.Linq;
using System.Web.Http;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.Manager.DAL.DAO.AdmissionManager;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers.AdmissionApiController
{
    public class PersonalInfoServiceController : ApiController
    {
        private readonly PersonalInfoManager _personalInfoManager;
        private readonly StudentManager _studentManager;
        private readonly OnlineAdmissionManager _onlineAdmission;
        public PersonalInfoServiceController()
        {
            _personalInfoManager = new PersonalInfoManager();
            _studentManager = new StudentManager();
            _onlineAdmission = new OnlineAdmissionManager();
        }

        public ResponseModel Get(string studentId)
        {
            ResponseModel responseModel;
            try
            {
                if (!string.IsNullOrEmpty(studentId))
                {
                    var infoByStudentIdViewModel = _onlineAdmission.GetSingleStudentInfo(studentId);
                    if (infoByStudentIdViewModel != null)
                    {
                        responseModel = new ResponseModel(infoByStudentIdViewModel);
                    }
                    else
                    {
                        responseModel=new ResponseModel(isSuccess:false);
                    }
                    
                }
                else
                {
                    responseModel = new ResponseModel(isSuccess: false, message: "No Data Found");
                }

            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(isSuccess: false, message: "Error !", exception: es);
            }
            return responseModel;
        }


        public ResponseModel Post(StudentInfo info)
        {
            ResponseModel responseModel;
            try
            {
                
                if (ModelState.IsValid && !string.IsNullOrEmpty(info.StudentId))
                {
                    string msg;
                    if (info.StudentInfoId != 0)
                    {
                        _personalInfoManager.Update(info);
                        msg = "Personal Information Updated Successfully";
                    }
                    else
                    {
                        _personalInfoManager.Insert(info);
                        msg = "Personal Information Created Successfully";
                    }
                    _personalInfoManager.SaveData();
                    return new ResponseModel(message: msg, data: info);
                }
                else
                {
                    var custMsg = ModelState.SelectMany(state => state.Value.Errors).Aggregate(string.Empty, (current, error) => current + (error.ErrorMessage + Environment.NewLine));
                    responseModel = new ResponseModel(isSuccess: false, message: custMsg);
                }
            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(isSuccess: false, message: "Error !", exception: es);
            }
            return responseModel;
        }
        public ResponseModel Delete(int id)
        {
            ResponseModel responseModel;
            try
            {
                StudentInfo findSingle = _personalInfoManager.GetSingle(id);
                if (findSingle.StudentInfoId != 0)
                {
                    _personalInfoManager.Delete(findSingle.StudentInfoId);
                    _personalInfoManager.SaveData();
                }
                responseModel = new ResponseModel(message: "Personal Information Deleted");
            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(isSuccess: false, message: "Error !", exception: es);
            }
            return responseModel;
        }
    }
}