﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO.AdmissionManager;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers.AdmissionApiController
{
    public class StudentRebateServiceController : ApiController
    {
        private readonly StudentRebateManager _studentRebateManager;

        public StudentRebateServiceController()
        {
            _studentRebateManager = new StudentRebateManager();
        }

        public ResponseModel Get(int id, int rebateId)
        {
            ResponseModel responseModel = new ResponseModel();

            try
            {
                if (rebateId > 0)
                {
                    var data = _studentRebateManager.GetSingle(rebateId);
                    StudentRebateViewModel model = new StudentRebateViewModel()
                    {
                        StudentIdentificationId = data.StudentIdentificationId,
                        RebateId = data.RebateId,
                        RebateCategoryId = data.RebateCategoryId,
                        RebateName = data.RebateCategory.RebateName,
                        RebatePercentage = data.RebateCategory.RebatePercent,
                        SemesterId = data.SemesterId,
                        SemesterName = data.Semester.SemesterNYear
                    };


                    responseModel = model != null ? new ResponseModel(model) : new ResponseModel(isSuccess: false, message: "No Data Found");
                }
                else
                {
                    List<StudentRebateViewModel> rebateList = _studentRebateManager.GetAll()
                                   .Where(x => x.StudentIdentificationId == id)
                                   .Select(x => new StudentRebateViewModel()
                                   {
                                       StudentIdentificationId = x.StudentIdentificationId,
                                       RebateId = x.RebateId,
                                       RebateCategoryId = x.RebateCategoryId,
                                       RebateName = x.RebateCategory.RebateName,
                                       RebatePercentage = x.RebateCategory.RebatePercent,
                                       SemesterId = x.SemesterId,
                                       SemesterName = x.Semester.SemesterNYear
                                   }).ToList();

                    responseModel = rebateList.Any() ? new ResponseModel(rebateList) : new ResponseModel(isSuccess: true, message: "No Data Found");
                }

            }
            catch (Exception ex)
            {

                responseModel = new ResponseModel(isSuccess: false, message: "Error!", exception: ex);
            }

            return responseModel;
        }

        public ResponseModel Post(Rebate rebate)
        {
            ResponseModel responseModel;
            try
            {
                string msg = "";
                if (rebate.StudentIdentificationId > 0)
                {
                    if (rebate.RebateId != 0)
                    {
                        _studentRebateManager.Update(rebate);
                        msg = "Student Rebate Updated Successfully";

                    }
                    else
                    {
                        Rebate rebateModel = new Rebate
                        {
                            RebateId = rebate.RebateId,
                            RebateCategoryId = rebate.RebateCategoryId,
                            StudentIdentificationId = rebate.StudentIdentificationId,
                            SemesterId = _studentRebateManager.GetSingleActiveSemester().SemesterId

                        };
                        _studentRebateManager.Insert(rebateModel);
                        msg = "Student Rebate Inserted Successfully";
                    }

                    _studentRebateManager.SaveData();
                }
                else
                {
                    msg = "Insert failed";
                }
                return new ResponseModel(message: msg, data: rebate);
            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(isSuccess: false, message: "Error !", exception: es);
            }
            return responseModel;
        }


        public ResponseModel Delete(int id)
        {
            ResponseModel responseModel;
            try
            {
                Rebate findSingle = _studentRebateManager.GetSingle(id);
                if (findSingle.RebateId != 0)
                {
                    _studentRebateManager.Delete(findSingle.RebateId);
                    _studentRebateManager.SaveData();
                }
                responseModel = new ResponseModel(message: "Rebate Information Deleted");
            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(isSuccess: false, message: "Error !", exception: es);
            }
            return responseModel;
        }
    }
}
