﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Newtonsoft.Json;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO.AdmissionManager;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IAccount;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IAdmission;
using UMS_MVC.Manager.DAL.DAO.Repository.RAccount;
using UMS_MVC.Manager.DAL.DAO.Repository.RAdmission;
using UMS_MVC.ViewModels.ViewModels;
using UMS_MVC.ViewModels.ViewModels.SortingIndex;

namespace UMS_MVC.ApiService.Controllers.AdmissionApiController
{
    public class StudentIdentityServiceController : ApiController
    {
        private readonly IStudentIdentityRepository _studentIdentityRepository;
        private readonly StudentIdentityManager _identityManager;
        private readonly SingleClickOnlineAdmissionManager _clickOnlineAdmission;
        private readonly IAccountRepository _accountRepository;
        public StudentIdentityServiceController()
        {
            _studentIdentityRepository = new StudentIdentityRepository();
            _identityManager = new StudentIdentityManager();
            _clickOnlineAdmission = new SingleClickOnlineAdmissionManager();
            _accountRepository=new AccountRepository();
        }
        //public async Task<ResponseModel> Get(string name, int departmentId, int gender, string sorting)
        public async Task<ResponseModel> Get(string search, string sorting)
        {
            ResponseModel responseModel;
            try
            {
                var sortObj = JsonConvert.DeserializeObject<PaginationViewModel>(sorting);
                var searchObj = JsonConvert.DeserializeObject<StudentIndexApiSorting>(search);
                IQueryable<StudentIdentification> infos = _studentIdentityRepository.GetAll().OrderBy(s => s.StudentId).Include(s => s.StudentInfo).AsNoTracking();
                // Other Filter's 
                //var asyncQuery = infos.Where(s => s.StudentInfoId != null)
                var asyncQuery = infos
                    .Select(s => new
                    {
                        s.StudentInfoId,
                        StudentName=s.StudentInfo.StudentName??"-----------------------",
                        s.StudentIdentificationId,
                        s.StudentPicture,
                        s.DepartmentId,
                       GenderId=(int?)s.StudentInfo.GenderId??3,
                        s.StudentId,
                        s.SemesterId
                    });
                // Filter--------
                switch (searchObj.GenderId)
                {
                    case 1: // male
                        asyncQuery = asyncQuery.Where(s => s.GenderId == 1);
                        break;
                    case 2:
                        asyncQuery = asyncQuery.Where(s => s.GenderId == 2);
                        break;
                    case 3:
                        asyncQuery = asyncQuery.Where(s => s.StudentInfoId == null);
                        break;

                }
                if (!string.IsNullOrEmpty(searchObj.NameSearch))
                {
                    asyncQuery = asyncQuery.Where(s => s.StudentName.Contains(searchObj.NameSearch));
                }
                if (!string.IsNullOrEmpty(searchObj.IdSearch))
                {
                    asyncQuery = asyncQuery.Where(s => s.StudentId.Contains(searchObj.IdSearch));
                }
                if (searchObj.DepartmentId > 0)
                {
                    asyncQuery = asyncQuery.Where(s => s.DepartmentId == searchObj.DepartmentId);
                }
                if (searchObj.SemesterId > 0)
                {
                    asyncQuery = asyncQuery.Where(s => s.SemesterId == searchObj.SemesterId);
                }
                // Filter--------

                var totalCount = asyncQuery.Count();
                var totalPages = (int?)Math.Ceiling((double)totalCount / sortObj.PageSize)??0;
                var pageData = new PaginationViewModel
                {
                    PageNo = sortObj.PageNo,
                    PageSize = sortObj.PageSize,
                    TotalCount = totalCount,
                    TotalPages = totalPages
                };
                var results = await asyncQuery
                    .Skip(sortObj.PageSize * sortObj.PageNo)
                    .Take(sortObj.PageSize)
                    .ToListAsync();
                responseModel = new ResponseModel(results, pageData);
            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(exception: es, message: "Error", isSuccess: false);
            }
            return responseModel;

        }

        public async Task<ResponseModel> Get(int id)
        {
            ResponseModel responseModel;

            try
            {
                StudentIdentification identification = await _studentIdentityRepository.GetAll().AsNoTracking().FirstOrDefaultAsync(s => s.StudentIdentificationId == id);

                responseModel = identification != null ? new ResponseModel(identification) : new ResponseModel(data: new StudentIdentification(), message: "No Identification");
            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(isSuccess: false, message: "Error!", exception: es);
            }
            return responseModel;

        }
        public ResponseModel Post(StudentIdentification identification)
        {
            ResponseModel responseModel;
            try
            {
                string msg;
                StudentIdentification generatedStudentIdentity;
                var headers = Request.Headers;
                if (!headers.Contains("AccountId"))
                {
                    responseModel = new ResponseModel(isSuccess: false, message: "Login First");
                }
                else
                {
                    //int userData = Convert.ToInt32(headers.GetValues("AccountId").First());
                    string loginId = headers.GetValues("UserId").First();
                    identification.EntryBy = loginId;
                    if (identification.StudentIdentificationId == 0)
                    {
                        generatedStudentIdentity = _clickOnlineAdmission.InsertUpdateIdentity(identification, true);
                        if (generatedStudentIdentity != null)
                        {
                            var infoStat = _clickOnlineAdmission.InsertStudentInfo(identification.StudentGuid, generatedStudentIdentity);

                            var academicStat = _clickOnlineAdmission.InsertAcademics(identification.StudentGuid, generatedStudentIdentity.StudentId);

                            msg = "Student Id, Personal Info added " + infoStat + ", Academic Record added " + academicStat;

                        }
                        else
                        {
                            msg = "Student Id created, ";
                        }
                    }
                    else
                    {
                        generatedStudentIdentity = _clickOnlineAdmission.InsertUpdateIdentity(identification, false);
                        msg = "Id Updated Successfully";
                    }
                    responseModel = new ResponseModel(message: msg, data: generatedStudentIdentity);
                }
               
            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(isSuccess: false, message: "Error !", exception: es);
            }
            return responseModel;
        }
        public ResponseModel Delete(int id)
        {
            ResponseModel responseModel;
            try
            {
                StudentIdentification findSingle = _studentIdentityRepository.FindSingle(id);
                _identityManager.Delete(findSingle.StudentIdentificationId);
                _identityManager.SaveData();
                responseModel = new ResponseModel(message: "Id Deleted Successfully");
            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(isSuccess: false, message: "Error !", exception: es);
            }
            return responseModel;
        }
    }

}