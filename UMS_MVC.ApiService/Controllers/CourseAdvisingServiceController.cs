﻿using System.Web.Http;
using UMS_MVC.ApiService.Security;
using UMS_MVC.Manager.DAL.DAO;

namespace UMS_MVC.ApiService.Controllers
{
    
    public class CourseAdvisingServiceController : ApiController
    {
        private readonly SectionManager _sectionLogicManager;
        private readonly CourseForStudentManger _courseForStudentManger;


        public CourseAdvisingServiceController()
        {
            _sectionLogicManager = new SectionManager();
            _courseForStudentManger = new CourseForStudentManger();
        }
       
    }
}
