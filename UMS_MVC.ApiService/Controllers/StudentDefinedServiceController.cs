﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO.Intefaces;
using UMS_MVC.Manager.DAL.DAO.Intefaces.IAdmission;
using UMS_MVC.Manager.DAL.DAO.Repository;
using UMS_MVC.Manager.DAL.DAO.Repository.RAdmission;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers
{
    public class StudentDefinedServiceController : ApiController
    {

        private readonly IStudentIdentityRepository _studentIdentityRepository;
        
        public StudentDefinedServiceController()
        {
            _studentIdentityRepository=new StudentIdentityRepository();
        }

        public ResponseModel Get(int id, string identity, bool type)
        {
            ResponseModel responseModel;
            try
            {
                StudentIdentification identification = null;
                IQueryable<StudentIdentification> identifications = _studentIdentityRepository.GetAll().AsNoTracking();
                switch (type)
                {
                    case true:
                     identification=   identifications.FirstOrDefault(s => s.StudentIdentificationId == id);
                        break;
                    case false:
                        identification = identifications.FirstOrDefault(s => s.StudentId == identity);
                        break;
                }
                responseModel = identification != null ? new ResponseModel(identification) : new ResponseModel(isSuccess:false, message:"No Student Found");
              
            }
            catch (Exception ex)
            {

                responseModel = new ResponseModel(isSuccess: false, message: "Error", exception: ex);
            }
            return responseModel;
        }
    }
}
