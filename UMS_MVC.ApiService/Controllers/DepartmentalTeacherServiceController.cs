﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using UMS_MVC.ApiService.Security;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers
{
    
    public class DepartmentalTeacherServiceController : ApiController
    {
        private readonly TeacherManager _teacherManager;

        public DepartmentalTeacherServiceController()
        {
            _teacherManager=new TeacherManager();
        }
        [HttpGet]
        
        public ResponseModel Get(int id)
        {
            ResponseModel responseModel;

            try
            {
                var teachers = _teacherManager.GetAllTeacherFiltered().Where(s => s.DepartmentId == id).Include(s => s.Department).Include(s => s.Account).Select(s => new TeacherAccountViewModel
                {
                    Name = s.Account.Name,
                    Designation = s.Account.Designation,
                    TeacherId = s.TeacherId,
                    Pic = s.Account.Pic,
                    Department = s.Department.DepartmentName
                });
                responseModel = new ResponseModel(teachers);
            }
            catch (Exception exception)
            {
                responseModel = new ResponseModel(isSuccess: true, message: "Error", exception: exception);
            }
            return responseModel;
        }
    }
}
