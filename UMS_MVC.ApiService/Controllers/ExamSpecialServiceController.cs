﻿using System;
using System.Linq;
using System.Web.Http;
using UMS_MVC.ApiService.Security;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers
{
    
    public class ExamSpecialServiceController : ApiController
    {
        private readonly CourseForStudentManger _studentManger;
        private readonly AccountManager _accountManager;

        public ExamSpecialServiceController()
        {
            _studentManger = new CourseForStudentManger();
            _accountManager=new AccountManager();
        }

        public ResponseModel Get()
        {
            return new ResponseModel();
        }

        public ResponseModel Get(int id, int accountId)
        {
            ResponseModel responseModel;
            Account singleAccount = _accountManager.GetSingleAccount(accountId);
            if (singleAccount.AccountsRoleId != 1 && singleAccount.AccountsRoleId != 13)
                return  new ResponseModel(isSuccess:false, message:"Access Denied");
            try
            {
                IQueryable<CourseForStudentsAcademic> studentsAcademics = _studentManger.GetAllCourseForStudentsAcademicsBysection(id);
                int countUpdate = 0;
                if (studentsAcademics.Any())
                    {
                        foreach (var adv in studentsAcademics.ToList())
                        {
                            adv.LetterGrade = adv.TotalMark >= 40 ? "NCP" : "NCF";
                            adv.CourseStatusId = 7;
                            adv.Grade = 0;
                            adv.TotalGrade = 0;
                            _studentManger.UpdateCourseForStudentAcademicSecond(adv);
                            countUpdate++;
                        }
                        _studentManger.Commit();
                        responseModel = new ResponseModel(message: countUpdate + " Result Updated");
                    }
                    else
                    {
                        responseModel = new ResponseModel(message: "No Data Found");
                    }
                
                
            }
            catch (Exception es)
            {
                responseModel=new ResponseModel(isSuccess:false, message:"Error",exception:es);
            }
            return responseModel;
        }
    }


}
