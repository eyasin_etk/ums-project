﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using UMS_MVC.ApiService.Security;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.ViewModels.ViewModels;
using UMS_MVC.ViewModels.ViewModels.GradingForSection;

namespace UMS_MVC.ApiService.Controllers
{
   public class SectionBlockServiceController : ApiController
    {
        private readonly SectionManager _sectionManager;
        private readonly SemesterManager _semesterManager;
        private readonly DepartmentManager _departmentManager;
        private readonly GradingSystemManager _gradingSystemManager;

        public SectionBlockServiceController()
        {
            _sectionManager = new SectionManager();
            _semesterManager = new SemesterManager();
            _departmentManager = new DepartmentManager();
            _gradingSystemManager = new GradingSystemManager();
        }
        public ResponseModel Get()
        {
            ResponseModel responseModel;
            try
            {
                Semester activeSemester = _semesterManager.GetActiveSemester();

                IQueryable<Section> sections = _sectionManager.GetAllSectionBySemester(activeSemester.SemesterId).Include(s => s.CourseForDepartment);
                var departmentsWithSections = new List<SectionBlockSubmit>();
                foreach (var dept in _departmentManager.GetAllDepartmentFiltered())
                {
                    IQueryable<Section> sortingQuery = sections.Where(s => s.CourseForDepartment.DepartmentId == dept.DepartmentId);
                    var calculateTotal = sortingQuery.Count();
                    var umsCoreCal = sortingQuery.Count(s => s.GradingSystemId == 1);
                    var umsLabCal = sortingQuery.Count(s => s.GradingSystemId == 2);
                    var umsOtherCal = sortingQuery.Count(s => s.GradingSystemId == 3);
                    var pauCoreCal = sortingQuery.Count(s => s.GradingSystemId == 4);
                    var pharmaCoreNew = sortingQuery.Count(s => s.GradingSystemId == 6);
                    var pharmaCoreOld = sortingQuery.Count(s => s.GradingSystemId == 8);
                    var facultySubmitted =
                        sections.Count(
                            s => s.CourseForDepartment.DepartmentId == dept.DepartmentId && s.ConfirmSubmitByFaculty);
                    var calculateHighLighted =
                        sections.Count(s => s.CourseForDepartment.DepartmentId == dept.DepartmentId && s.HighLight);

                    var secDef = new SectionBlockSubmit
                    {
                        DepartmentId = dept.DepartmentId,
                        DepartmentName = dept.DepartmentName,
                        UmsCoreSections = umsCoreCal,
                        UmsLabSections = umsLabCal,
                        UmsOtherSections = umsOtherCal,
                        PauCoreSections = pauCoreCal,
                        PharmaCoreNew = pharmaCoreNew,
                        PharmaCoreOld = pharmaCoreOld,
                        TotalSections = calculateTotal,
                        FacultySubmitted = facultySubmitted,
                        HighLighted = calculateHighLighted,
                    };
                    departmentsWithSections.Add(secDef);
                }
                //var departmentsWithSections = (from deptObj in _departmentManager.GetAllDepartmentFiltered().ToList()
                //                               let calculateTotal = sections.Count(s => s.CourseForDepartment.DepartmentId == deptObj.DepartmentId)
                //                               let facultySubmitted = sections.Count(s => s.CourseForDepartment.DepartmentId == deptObj.DepartmentId && s.ConfirmSubmitByFaculty)
                //                               let calculateHighLighted = sections.Count(s => s.CourseForDepartment.DepartmentId == deptObj.DepartmentId && s.HighLight)
                //                               select new SectionBlockSubmit
                //                               {
                //                                   DepartmentId = deptObj.DepartmentId,
                //                                   DepartmentName = deptObj.DepartmentName,
                //                                   TotalSections = calculateTotal,
                //                                   FacultySubmitted = facultySubmitted,
                //                                   HighLighted = calculateHighLighted
                //                               }).ToList();
                responseModel = departmentsWithSections.Any() ? new ResponseModel(departmentsWithSections) : new ResponseModel(message: "No Data.");
            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(isSuccess: false, exception: es);
            }
            return responseModel;
        }
        //public ResponseModel Get()
        //{
        //    ResponseModel responseModel;
        //    try
        //    {
        //        Semester activeSemester = _semesterManager.GetActiveSemester();
        //        IQueryable<Section> sections = _sectionManager.GetAllSectionBySemester(activeSemester.SemesterId).Include(s => s.CourseForDepartment);
        //        var departmentsWithSections = (from deptObj in _departmentManager.GetAllDepartmentFiltered().ToList()
        //                                       let calculateTotal = sections.Count(s => s.CourseForDepartment.DepartmentId == deptObj.DepartmentId)
        //                                       let facultySubmitted = sections.Count(s => s.CourseForDepartment.DepartmentId == deptObj.DepartmentId && s.ConfirmSubmitByFaculty)
        //                                       let calculateHighLighted = sections.Count(s => s.CourseForDepartment.DepartmentId == deptObj.DepartmentId && s.HighLight)
        //                                       select new SectionBlockSubmit
        //                                       {
        //                                           DepartmentId = deptObj.DepartmentId,
        //                                           DepartmentName = deptObj.DepartmentName,
        //                                           TotalSections = calculateTotal,
        //                                           FacultySubmitted = facultySubmitted,
        //                                           HighLighted = calculateHighLighted
        //                                       }).ToList();
        //        responseModel = departmentsWithSections.Any() ? new ResponseModel(departmentsWithSections) : new ResponseModel(message: "No Data.");
        //    }
        //    catch (Exception es)
        //    {
        //        responseModel = new ResponseModel(isSuccess: false, exception: es);
        //    }
        //    return responseModel;
        //}
        public ResponseModel Get(bool submitAcc)// Will work to unblock sections status
        {
            ResponseModel responseModel;
            try
            {
                int countChanging = 0;
                Semester activeSemester = _semesterManager.GetActiveSemester();
                IQueryable<Section> sections = _sectionManager.GetAllSectionBySemester(activeSemester.SemesterId).Where(s => s.ConfirmSubmitByFaculty && s.GradingSystemId==1);
                if (sections.Any())
                {
                    foreach (var s in sections.ToList())
                    {
                        if (s.HighLight)
                        {
                            string shortNote = "This section was highlighted for ";
                            if (s.MidTermEx)
                            {
                                shortNote += "Mid Term (" + s.ExpireDateTime + ")";
                            }
                            if (s.FinalTermEx)
                            {
                                shortNote += "Final Term (" + s.ExpireDateTime + ")";
                            }
                            s.ShortNote += shortNote;
                        }
                        s.ConfirmSubmitByFaculty = submitAcc;
                        s.HighLight = false;
                        s.MidTermEx = false;
                        s.FinalTermEx = false;
                        s.RevisedStat = false;
                        s.ExpireDateTime = null;
                        _sectionManager.UpdateSection(s);
                        countChanging++;
                    }
                    _sectionManager.Commit();
                    responseModel = new ResponseModel(message: countChanging + " sections status updated.");
                }
                else
                {
                    responseModel = new ResponseModel(message: "No Section found.");
                }
            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(isSuccess: false, message: "Error", exception: es);
            }
            return responseModel;
        }

        public ResponseModel Post(SectionBlockStatusViewModel statusViewModel)
        {
            ResponseModel responseModel;
            try
            {
                int countChanging = 0;
                Semester activeSemester = _semesterManager.GetActiveSemester();
                IQueryable<Section> sections = _sectionManager.GetAllSectionBySemester(activeSemester.SemesterId);
                if (statusViewModel.CoreDo)
                {
                    sections  = sections.Where(s=>s.GradingSystemId==1);
                }
                if (statusViewModel.LabDo)
                {
                    sections = sections.Where(s => s.GradingSystemId == 2);
                }
                if (statusViewModel.OtherDo)
                {
                    sections = sections.Where(s => s.GradingSystemId == 3);
                }
                if (statusViewModel.PauCoreDo)
                {
                    sections = sections.Where(s => s.GradingSystemId == 4);
                }
                if (sections.Any())
                {
                    foreach (var s in sections.ToList())
                    {
                        if (s.HighLight)
                        {
                            string shortNote = "This section was highlighted for ";
                            if (s.MidTermEx)
                            {
                                shortNote += "Mid Term (" + s.ExpireDateTime + ")";
                            }
                            if (s.FinalTermEx)
                            {
                                shortNote += "Final Term (" + s.ExpireDateTime + ")";
                            }
                            s.ShortNote += shortNote;
                        }
                        //s.ConfirmSubmitByFaculty = submitAcc;
                        s.HighLight = false;
                        s.MidTermEx = false;
                        s.FinalTermEx = false;
                        s.RevisedStat = false;
                        s.ExpireDateTime = null;
                        _sectionManager.UpdateSection(s);
                        countChanging++;
                    }
                    _sectionManager.Commit();
                    responseModel = new ResponseModel(message: countChanging + " sections status updated.");
                }
                else
                {
                    responseModel = new ResponseModel(message: "No Section found.");
                }
            }
            catch (Exception es)
            {
                responseModel = new ResponseModel(isSuccess: false, message: "Error", exception: es);
            }
            return responseModel;
        }
    }
}
