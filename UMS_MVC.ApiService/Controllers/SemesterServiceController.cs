﻿using System;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;
using UMS_MVC.ApiService.Security;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO.Intefaces;
using UMS_MVC.Manager.DAL.DAO.Repository;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers
{
   public class SemesterServiceController : ApiController
    {
        private readonly ISemesterRepository _semesterRepository;

        public SemesterServiceController()
        {
            _semesterRepository=new SemesterRepository();
        }

        public async Task<ResponseModel> Get()
        {
            ResponseModel responseModel;
            try
            {
                var semesters =await _semesterRepository.GetAll().AsNoTracking().Select(s=>new
                {
                    s.SemesterId,
                    s.SemesterNYear,
                    s.ActiveSemester,
                    s.BatchNo,
                    s.AcademicYear,
                    s.MidTerm,
                    s.FinalTerm,
                    s.CourseAdvising,
                    s.SpecialGradeuploadDeadLine,
                    s.OnlineAdmisssionStatus,
                    s.AdmissionStartDate,
                    s.AdmissionEndDate
                    }).OrderByDescending(s=>s.SemesterId).ToListAsync();
                responseModel=new ResponseModel(semesters);
            }
            catch (Exception ex)
            {

                responseModel = new ResponseModel(isSuccess: true, message: "Error", exception: ex);
            }
            return responseModel;
             
        }

        public async Task<ResponseModel> Get(int id)
        {
            ResponseModel responseModel;
            try
            {
                var singleAsync =await _semesterRepository.FindSingleAsync(id);
                responseModel=new ResponseModel(singleAsync);
            }
            catch (Exception ex)
            {
                
               responseModel=new ResponseModel(isSuccess:false, message:"Error !", exception:ex);
            }
            return responseModel;
        }

       
        public ResponseModel Post(Semester semester)
        {
            ResponseModel responseModel;
            try
            {
                var msg = "";
                if (semester.SemesterId > 0)
                {
                    if (semester.ActiveSemester)
                    {
                        var activeSemester = _semesterRepository.GetActiveSemester();
                        if (activeSemester.SemesterId != semester.SemesterId)
                        {
                            activeSemester.ActiveSemester = false;
                            activeSemester.MidTerm = false;
                            activeSemester.FinalTerm = false;
                            _semesterRepository.Update(activeSemester);
                        }
                       
                    }
                    if (semester.CourseAdvising)
                    {
                        var advisingSemester = _semesterRepository.GetAdvisingSemester();
                        if (advisingSemester!=null && advisingSemester.SemesterId != semester.SemesterId)
                        {
                            advisingSemester.CourseAdvising = false;
                            _semesterRepository.Update(advisingSemester);
                        }
                        
                    }
                    if (semester.OnlineAdmisssionStatus)
                    {
                        var onlineAdmSemester = _semesterRepository.GetOnlineAdmSemester();
                        if (onlineAdmSemester!=null && onlineAdmSemester.SemesterId!=semester.SemesterId)
                        {
                            onlineAdmSemester.OnlineAdmisssionStatus = false;
                            _semesterRepository.Update(onlineAdmSemester);
                        }
                    }
                    _semesterRepository.Save();
                    _semesterRepository.Update(semester);
                    msg = "Updated";
                }
                else
                {
                    _semesterRepository.Add(semester);
                    msg = "Added";
                }
                _semesterRepository.Save();
                responseModel = new ResponseModel(semester,message:msg);
            }
            catch (Exception ex)
            {

                responseModel = new ResponseModel(isSuccess: true, message: "Error", exception: ex);
            }
            return responseModel;
        }


        //public ResponseModel Delete(int id)
        //{
        //    ResponseModel responseModel;
        //    try
        //    {
        //        var findSingle = _semesterRepository.FindSingle(id);
        //        _semesterRepository.Delete(findSingle);
        //        _semesterRepository.Save();
        //        responseModel = new ResponseModel(message:"Semester removed successfully");
        //    }
        //    catch (Exception ex)
        //    {

        //        responseModel = new ResponseModel(isSuccess: true, message: "Error", exception: ex);
        //    }
        //    return responseModel;
        //}
    }
}
