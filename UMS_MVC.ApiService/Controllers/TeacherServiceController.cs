﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;
using UMS_MVC.ApiService.Security;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers
{
    
    public class TeacherServiceController : ApiController
    {
        private readonly TeacherManager _teacherManager;
        
        public TeacherServiceController()
        {
            _teacherManager=new TeacherManager();
        }
        public ResponseModel Get()
        {
            ResponseModel responseModel;

            try
            {
                var teachers = _teacherManager.GetAllTeacherFiltered().Include(s=>s.Department).Include(s=>s.Account).Select(s=>new TeacherAccountViewModel
                {
                    Name = s.Account.Name,
                    Designation = s.Account.Designation,
                    TeacherId = s.TeacherId,
                    Pic = s.Account.Pic,
                    Department = s.Department.DepartmentName
                });
                responseModel=new ResponseModel(teachers);
            }
            catch (Exception exception)
            {
                responseModel = new ResponseModel(isSuccess: true, message: "Error", exception: exception);
            }
            return responseModel;
            
        }
        
        public ResponseModel Get(int id)
        {
            ResponseModel responseModel;
            try
            {
                var getAccount = _teacherManager.FindTeacherFirstAndDef(id);
                var teacherAccountViewModel = new TeacherAccountViewModel
                {
                    Name = getAccount.Account.Name,
                    Department = getAccount.Department.DepartmentTitle,
                    Designation = getAccount.Account.Designation,
                    PhoneNo = getAccount.Account.PhoneNo,
                    TeacherId = id,
                    TeleExchange = getAccount.Account.TeleExchange,
                    Pic=getAccount.Account.Pic
                };
                responseModel=new ResponseModel(teacherAccountViewModel);
            }
            catch (Exception exception)
            {
                responseModel=new ResponseModel(isSuccess:false, message:"Error",exception:exception);
            }
            return responseModel;
        }
        


    }
}
