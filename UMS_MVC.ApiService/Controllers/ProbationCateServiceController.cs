﻿using System;
using System.Linq;
using System.Web.Http;
using UMS_MVC.ApiService.Security;
using UMS_MVC.DataModel.Models.Notice_Matter;
using UMS_MVC.DataModel.Models.Probation;
using UMS_MVC.Manager.DAL.DAO.Intefaces;
using UMS_MVC.Manager.DAL.DAO.Repository;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers
{
    public class ProbationCateServiceController : ApiController
    {
        private readonly IProbationCategoryRepository _probationCategoryRepository;

        public ProbationCateServiceController()
        {
            _probationCategoryRepository=new ProbationCategoryRepository();
        }

        public ResponseModel Get()
        {
            ResponseModel responseModel;
            try
            {
                var getAllProbationCategory = _probationCategoryRepository.GetAll();
                responseModel = new ResponseModel(getAllProbationCategory.ToList());
            }
            catch (Exception e)
            {
                responseModel = new ResponseModel(exception: e, message: "Error", isSuccess: false);
            }
            return responseModel;
        }

        public ResponseModel Get(int id)
        {
            ResponseModel responseModel;
            try
            {
                var findSingleProbationCategory = _probationCategoryRepository.FindSingle(id);
                responseModel = new ResponseModel(findSingleProbationCategory);
            }
            catch (Exception e)
            {
                responseModel = new ResponseModel(exception: e, message: "Error", isSuccess: false);
            }
            return responseModel;
        }

        public ResponseModel Post(ProbationCategory probCategory)
        {
            ResponseModel responseModel;
            try
            {
                ProbationCategory aProbationCategory;
                if (probCategory.ProbationCategoryId != 0)
                {
                    
                    aProbationCategory = _probationCategoryRepository.FindSingle(probCategory.ProbationCategoryId);
                    aProbationCategory.ProbationName = probCategory.ProbationName;
                    aProbationCategory.Remarks = probCategory.Remarks;
                    _probationCategoryRepository.Update(aProbationCategory);
                }
                else
                {
                    aProbationCategory = probCategory;
                    _probationCategoryRepository.Add(aProbationCategory);
                }
                _probationCategoryRepository.Save();
               responseModel = new ResponseModel(aProbationCategory.ProbationCategoryId);
            }
            catch (Exception e)
            {
                responseModel = new ResponseModel(exception: e, message: "Error", isSuccess: false);
            }
            return responseModel;
        }

        public ResponseModel Delete(int id)
        {
            ResponseModel responseModel;
            try
            {
                var dbObj = _probationCategoryRepository.FindSingle(id);
                if (dbObj != null)
                {
                    _probationCategoryRepository.Delete(dbObj);
                    _probationCategoryRepository.Save();
                    responseModel=new ResponseModel(id);
                }
                else
                {
                    responseModel=new ResponseModel(isSuccess: false, message: "Cannot be removed");
                }
                
            }
            catch (Exception exception)
            {
                responseModel = new ResponseModel { IsSuccess = false, Message = "Exception Found", Exception = exception };
            }
            return responseModel;
        }

    }
}
