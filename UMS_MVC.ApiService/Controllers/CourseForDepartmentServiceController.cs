﻿using System;
using System.Linq;
using System.Web.Http;
using UMS_MVC.ApiService.Security;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers
{
   
    public class CourseForDepartmentServiceController : ApiController
    {
        private readonly CourseForDepartmentManager _courseForDepartmentManager;

        public CourseForDepartmentServiceController()
        {
            _courseForDepartmentManager = new CourseForDepartmentManager();
        }
        [HttpGet]
        public ResponseModel Get(int deptId)
        {
            ResponseModel responseModel;
            try
            {
                var allCourseList =
                    (from course in _courseForDepartmentManager.GetAllCourseForDepartmentsByDepartment(deptId).AsEnumerable()
                        select new CourseForDepartment
                        { 
                            CourseForDepartmentId = course.CourseForDepartmentId,
                            CourseCode = course.CourseCode,
                            CourseName = course.CourseName
                            }).ToList();
                responseModel = new ResponseModel(allCourseList);
            }
            catch (Exception ex)
            {

                responseModel = new ResponseModel(isSuccess: true, message: "Error", exception: ex);
            }
            return responseModel;

        }
    }
}
