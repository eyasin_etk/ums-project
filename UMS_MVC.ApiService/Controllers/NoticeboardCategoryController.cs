﻿using System;
using System.Linq;
using System.Web.Http;
using UMS_MVC.ApiService.Security;
using UMS_MVC.DataModel.Models.Notice_Matter;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers
{
    
    public class NoticeboardCategoryController : ApiController
    {
        private readonly NoticeBoardCategoryManager _noticeBoardCategoryManager;

        public NoticeboardCategoryController()
        {
            _noticeBoardCategoryManager=new NoticeBoardCategoryManager();
        }
        public ResponseModel Get()
        {
            ResponseModel responseModel;
            try
            {
                var getAllNoticeDetails = _noticeBoardCategoryManager.GetAllCategories();
                responseModel = new ResponseModel(getAllNoticeDetails.ToList());
            }
            catch (Exception e)
            {
                responseModel = new ResponseModel(exception: e, message: "Error", isSuccess: false);
            }
            return responseModel;
        }

        public ResponseModel Get(int id)
        {
            ResponseModel responseModel;
            try
            {
                var getAllNoticeDetails = _noticeBoardCategoryManager.GetSinglecCategory(id);
                responseModel = new ResponseModel(getAllNoticeDetails);
            }
            catch (Exception e)
            {
                responseModel = new ResponseModel(exception: e, message: "Error", isSuccess: false);
            }
            return responseModel;
        }

        public ResponseModel Post(NoticeBoardCategory boardCategory)
        {
            ResponseModel responseModel;
            try
            {
                var categoryId = _noticeBoardCategoryManager.InsertUpdate(boardCategory);
                responseModel = new ResponseModel(categoryId);
            }
            catch (Exception e)
            {
                responseModel = new ResponseModel(exception: e, message: "Error", isSuccess: false);
            }
            return responseModel;
        }

        public ResponseModel Delete(int id)
        {
            ResponseModel responseModel;
            try
            {
                var riceDetailId = _noticeBoardCategoryManager.Delete(id);
                responseModel = riceDetailId ? new ResponseModel(data: id) : new ResponseModel(isSuccess: false, message: "Cannot be removed");
            }
            catch (Exception exception)
            {
                responseModel = new ResponseModel { IsSuccess = false, Message = "Exception Found", Exception = exception };
            }
            return responseModel;
        }

    }
}
