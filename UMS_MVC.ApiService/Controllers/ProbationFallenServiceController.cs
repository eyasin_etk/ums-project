﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using UMS_MVC.ApiService.Security;
using UMS_MVC.DataModel.Models.Probation;
using UMS_MVC.Manager.DAL.DAO.Intefaces;
using UMS_MVC.Manager.DAL.DAO.Repository;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers
{
    public class ProbationFallenServiceController : ApiController
    {
        private readonly IProbationFallenRepository _probationFallenRepository;

        public ProbationFallenServiceController()
        {
            _probationFallenRepository=new ProbationFallenRepository();
        }
        public ResponseModel Get()
        {
            ResponseModel responseModel;
            try
            {
                var getAllProbationCategory = _probationFallenRepository.GetAll().Include(s=>s.ProbationCategory);
                responseModel = new ResponseModel(getAllProbationCategory.ToList());
            }
            catch (Exception e)
            {
                responseModel = new ResponseModel(exception: e, message: "Error", isSuccess: false);
            }
            return responseModel;
        }

        public ResponseModel Get(int id)
        {
            ResponseModel responseModel;
            try
            {
                var findObj = _probationFallenRepository.FindSingle(id);
                responseModel = new ResponseModel(findObj);
            }
            catch (Exception e)
            {
                responseModel = new ResponseModel(exception: e, message: "Error", isSuccess: false);
            }
            return responseModel;
        }

        public ResponseModel Post(ProbationFallenStudent probFall)
        {
            ResponseModel responseModel;
            try
            {
                ProbationFallenStudent aProbationFallen;
                if (probFall.ProbationCategoryId != 0)
                {

                    aProbationFallen = _probationFallenRepository.FindSingle(probFall.ProbationCategoryId);

                    aProbationFallen.ActivateSuspend = probFall.ActivateSuspend;
                    aProbationFallen.Cgpa = probFall.Cgpa;
                    aProbationFallen.CreditCompleted = probFall.CreditCompleted;
                    aProbationFallen.ProbationCategoryId = probFall.ProbationCategoryId;
                    aProbationFallen.Remarks = probFall.Remarks;
                    aProbationFallen.SemesterCredits = probFall.SemesterCredits;
                    aProbationFallen.SemesterId = probFall.SemesterId;
                    aProbationFallen.Sgpa = probFall.Sgpa;
                    aProbationFallen.StudentIdentificationId = probFall.StudentIdentificationId;
                    _probationFallenRepository.Update(aProbationFallen);
                }
                else
                {
                    aProbationFallen = probFall;
                    aProbationFallen.CreationDate=DateTime.Now;
                    _probationFallenRepository.Add(aProbationFallen);
                }
                _probationFallenRepository.Save();
                responseModel = new ResponseModel(aProbationFallen.ProbationCategoryId);
            }
            catch (Exception e)
            {
                responseModel = new ResponseModel(exception: e, message: "Error", isSuccess: false);
            }
            return responseModel;
        }

        public ResponseModel Delete(int id)
        {
            ResponseModel responseModel;
            try
            {
                var dbObj = _probationFallenRepository.FindSingle(id);
                if (dbObj != null)
                {
                    _probationFallenRepository.Delete(dbObj);
                    _probationFallenRepository.Save();
                    responseModel = new ResponseModel(id);
                }
                else
                {
                    responseModel = new ResponseModel(isSuccess: false, message: "Cannot be removed");
                }

            }
            catch (Exception exception)
            {
                responseModel = new ResponseModel { IsSuccess = false, Message = "Exception Found", Exception = exception };
            }
            return responseModel;
        }
    }
}
