﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using UMS_MVC.ApiService.Security;
using UMS_MVC.DataModel.Models.Essential_Types;
using UMS_MVC.Manager.DAL.DAO;
using UMS_MVC.ViewModels.ViewModels;

namespace UMS_MVC.ApiService.Controllers
{
    
    public class DepartmentServiceController : ApiController
    {
        private readonly DepartmentManager _departmentManager;
        public DepartmentServiceController()
        {
            _departmentManager = new DepartmentManager();
        }

        public ResponseModel Get()
        {
            ResponseModel responseModel;

            try
            {
                //var getAlldepartment = _departmentManager.GetAllDepartmentFiltered().AsEnumerable().Select(s => new Department()
                //{
                //    DepartmentId = s.DepartmentId,
                //    DepartmentName = s.DepartmentName,
                //    DepartmentTitle = s.DepartmentTitle,
                //    RequiredCredit = s.RequiredCredit,
                //    DepartmentCode = s.DepartmentCode,
                //    TotalSemester = s.TotalSemester,
                //    TotalCost = s.TotalCost,
                //    SchoolId = s.SchoolId,

                //}).ToList();
                  var programExceptList = new List<int> { 20, 21, 24 };

                var getAlldepartment = (from dept in _departmentManager.GetAllDepartmentFiltered().AsEnumerable()
                                        where !programExceptList.Contains(dept.DepartmentId)
                                        select new Department
                                        {
                                            DepartmentId = dept.DepartmentId,
                                            DepartmentName = dept.DepartmentName,
                                            DepartmentTitle = dept.DepartmentTitle,
                                            RequiredCredit = dept.RequiredCredit,
                                            DepartmentCode = dept.DepartmentCode,
                                            TotalSemester = dept.TotalSemester,
                                            TotalCost = dept.TotalCost,
                                            SchoolId = dept.SchoolId
                                        }).ToList();
                responseModel = new ResponseModel(getAlldepartment);
            }
            catch (Exception exception)
            {
                responseModel = new ResponseModel(isSuccess: true, message: "Error", exception: exception);
            }
            return responseModel;
        }
        [HttpGet]
        [ActionName("Get")]
        public ResponseModel Get(int id)
        {
            ResponseModel responseModel;

            try
            {
                var s = _departmentManager.GetSingleDeptFiltered(id);
                if (s != null)
                {
                    var newDepartment = new Department
                    {
                        DepartmentId = s.DepartmentId,
                        DepartmentName = s.DepartmentName,
                        DepartmentTitle = s.DepartmentTitle,
                        RequiredCredit = s.RequiredCredit,
                        DepartmentCode = s.DepartmentCode,
                        TotalCredit = s.TotalCredit,
                        TotalSemester = s.TotalSemester,
                        TotalCost = s.TotalCost,
                        SchoolId = s.SchoolId,

                    };
                    responseModel = new ResponseModel(newDepartment);
                }
                else
                {
                    responseModel = new ResponseModel(isSuccess: false, message: "No Department Found");
                }
            }
            catch (Exception exception)
            {
                responseModel = new ResponseModel(isSuccess: true, message: "Error", exception: exception);
            }
            return responseModel;
        }


    }

}
